package com.hyper.instamour;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import org.jivesoftware.smack.packet.Message;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

import com.hyper.instamour.ProfileView.UploadImageTask;
import com.hyper.instamour.messages.C2DMReceiver;
import com.hyper.instamour.messages.c2dm.C2DMessaging;
import com.hyper.instamour.model.ChatHistoryData;
import com.hyper.instamour.model.DataBaseHelper;
import com.hyper.instamour.model.GCMSender;
import com.hyper.instamour.model.GlobalConsts;
import com.hyper.instamour.model.Model;
import com.quickblox.core.QBCallback;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.result.Result;
import com.quickblox.module.auth.QBAuth;
import com.quickblox.module.chat.utils.QBChatUtils;
import com.quickblox.module.content.QBContent;
import com.quickblox.module.content.result.QBFileUploadTaskResult;
import com.quickblox.module.users.model.QBUser;

public class ChatWindow extends Activity {
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GlobalConsts.isMsgScreenOpen = false;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		GlobalConsts.isMsgScreenOpen = true;

		QBAuth.createSession(me, new QBCallback() {

			@Override
			public void onComplete(Result arg0, Object arg1) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onComplete(Result result) {
				// TODO Auto-generated method stub
				if (result.isSuccess()) {
					C2DMessaging.register(ChatWindow.this, "521258089223");
				} else {

				}
			}
		});
	}

	public int cnt;
	// private static ChatWindow instance;
	private Global global;
	private Function c;
	private String uname;
	private ViewGroup messagesContainer;
	private ScrollView scrollContainer;
	private TextView msgTxt;
	MediaPlayer mMediaPlayer;
	private EditText editText;
	private Intent data;
	String currentusername, loginusername, friendphoto;
	String loginjabberid, fileSrc, receiveImageURL;
	int Friendjabberid;
	String password;

	private QBUser me, friend;
	private MyChatController myChatController;
	private DataBaseHelper databasehelper;
	private ArrayList<ChatHistoryData> arr_chatHistory;
	protected static final int CAMERA_REQUEST = 0;
	protected static final int GALLERY_PICTURE = 1;
	private ProgressDialog progressDialog;
	private boolean isAttachPress = false;
	private Bitmap bitmap_camera;
	final Handler handler = new Handler();
	private ArrayList<Model> records = new ArrayList<Model>();
	private static String attachFile = "(Attach file)";
	private File ReceiveFileImage;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		Log.e("onResume", "OnResume");
		try {
			new VideoSetting(ChatWindow.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
		/*
		 * arr_chatHistory = databasehelper.getChatHistory(
		 * Integer.parseInt(global.getJabberid()), Friendjabberid); if
		 * (arr_chatHistory.size() > 0) { for (int i = 0; i <
		 * arr_chatHistory.size(); i++) { if
		 * (arr_chatHistory.get(i).getSenderId() == Integer
		 * .parseInt(global.getJabberid())) { if
		 * (arr_chatHistory.get(i).getChatContents().length() > 12 &&
		 * arr_chatHistory.get(i).getChatContents() .substring(0,
		 * 13).equals(attachFile)) { //
		 * arr_chatHistory.get(i).getLocalPath().concat( // "      "+
		 * arr_chatHistory.get(i).getChatTime()); showImageMessage(
		 * arr_chatHistory.get(i).getLocalPath(), arr_chatHistory .get(i)
		 * .getChatDate() .concat("  " + arr_chatHistory.get(i) .getChatTime()),
		 * true); } else { showMessage(
		 * arr_chatHistory.get(i).getChatContents(), arr_chatHistory .get(i)
		 * .getChatDate() .concat("  " + arr_chatHistory.get(i) .getChatTime()),
		 * true); }
		 * 
		 * } else { if (arr_chatHistory.get(i).getChatContents().length() > 12
		 * && arr_chatHistory.get(i).getChatContents() .substring(0,
		 * 13).equals(attachFile)) { // String ImageLocalPath = //
		 * arr_chatHistory.get(i).getLocalPath().concat( // "      "+
		 * arr_chatHistory.get(i).getChatTime()); showImageMessage(
		 * arr_chatHistory.get(i).getLocalPath(), arr_chatHistory .get(i)
		 * .getChatDate() .concat("  " + arr_chatHistory.get(i) .getChatTime()),
		 * false); } else { showMessage(
		 * arr_chatHistory.get(i).getChatContents(), arr_chatHistory .get(i)
		 * .getChatDate() .concat("  " + arr_chatHistory.get(i) .getChatTime()),
		 * false); }
		 * 
		 * } } }
		 */
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.e("oncreate called", "oncreate called ");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Runtime.getRuntime().totalMemory();
		Runtime.getRuntime().freeMemory();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_window);

		global = Global.getInstance();
		try {
			databasehelper = new DataBaseHelper(ChatWindow.this);
			data = getIntent();
			global.preferences = getSharedPreferences("LoginPrefrence",
					MODE_PRIVATE);
			global.editor = global.preferences.edit();
			arr_chatHistory = new ArrayList<ChatHistoryData>();
			progressDialog = new ProgressDialog(this);

			// progressDialog.setTitle(getString(R.string.app_name));
			progressDialog.setMessage("Uploading Please Wait....");
			progressDialog.setCancelable(true);
			// instance = this;
			c = new Function();
			me = new QBUser();
			InstantChat.listclicked = true;
			// loginjabberid = global.getJabberid();
			loginjabberid = global.preferences.getString("jabberId", "");
			loginusername = global.getLoginusername();
			Log.e("login jabber id", "--" + loginusername);
			// password = global.preferences.getString("pass", "");
			me.setId(Integer.parseInt(loginjabberid));
			me.setLogin(loginusername);
			Log.e("--------", "------>" + global.getPassword());
			me.setPassword("instamourapp");

			friend = new QBUser();
			currentusername = data.getStringExtra("currentuser");

			Log.e("current user", "----" + currentusername);

			Friendjabberid = data.getIntExtra("jabberid", 0);
			GlobalConsts.JabberID = String.valueOf(Friendjabberid);
			friendphoto = data.getStringExtra("currentuserphoto");
			Log.e("Instant chat details", currentusername + "--"
					+ Friendjabberid + "--" + friendphoto);

			friend.setId(Friendjabberid);
			friend.setLogin(currentusername);
			editText = (EditText) findViewById(R.id.txtbox);
			Log.e("logged in details ", loginjabberid + "--" + loginusername
					+ "--" + global.getPassword());
			Log.e("selected user details", currentusername + "--"
					+ Friendjabberid + "--" + data.getStringExtra("photo"));
			global.setFriendjabberid(Friendjabberid + "");
			msgTxt = (TextView) findViewById(R.id.user_name);
			msgTxt.setText(currentusername);
			messagesContainer = (ViewGroup) findViewById(R.id.messagecontainer);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		scrollContainer = (ScrollView) findViewById(R.id.scrollLayout);

		if (me != null && friend != null) {

			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					// Do something after 100ms
					try {
						String chatLogin = QBChatUtils.getChatLoginShort(me);
						String friendLogin = QBChatUtils
								.getChatLoginFull(friend);
						Log.e("Login Name is  : ", chatLogin);
						Log.e("friend login is : ", friendLogin);
						String password = me.getPassword();
						myChatController = new MyChatController(chatLogin,
								password);
						myChatController.startChat(friendLogin);
						myChatController
								.setOnMessageReceivedListener(onMessageReceivedListener);

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}
			}, 100);

		}
		/*
		 * String chatLogin = QBChatUtils.getChatLoginShort(me);
		 * Log.e("Login Name is  : ", chatLogin);
		 * 
		 * // Our current (me) user's password. String password =
		 * me.getPassword(); if (me != null && friend != null) {
		 * 
		 * // ================= QuickBlox ===== Step 6 ================= // All
		 * chat logic can be implemented by yourself using // ASMACK library
		 * (https://github.com/Flowdalic/asmack/downloads) // -- Android wrapper
		 * for Java XMPP library //
		 * (http://www.igniterealtime.org/projects/smack/). myChatController =
		 * new MyChatController(chatLogin, password); myChatController
		 * .setOnMessageReceivedListener(onMessageReceivedListener);
		 * 
		 * // ================= QuickBlox ===== Step 7 ================= // Get
		 * friend's login based on QuickBlox user account. // Note, that for
		 * your companion you should use full chat login, // that looks like
		 * '17792-1028@chat.quickblox.com' //
		 * (<qb_user_id>-<qb_app_id>@chat.quickblox.com). // Don't use short
		 * login, it String friendLogin = QBChatUtils.getChatLoginFull(friend);
		 * Log.e("friend login is : ", friendLogin);
		 * myChatController.startChat(friendLogin); }
		 */
		Log.e("8", global.getJabberid() + "-");
		databasehelper.updateFriendCount(0, Integer.parseInt(loginjabberid), "");
		arr_chatHistory = databasehelper.getChatHistory(
				Integer.parseInt(global.preferences.getString("jabberId", "")),
				Friendjabberid);
		if (arr_chatHistory.size() > 0) {
			for (int i = 0; i < arr_chatHistory.size(); i++) {
				if (arr_chatHistory.get(i).getSenderId() == Integer
						.parseInt(global.getJabberid())) {
					if (arr_chatHistory.get(i).getChatContents().length() > 12
							&& arr_chatHistory.get(i).getChatContents()
									.substring(0, 13).equals(attachFile)) {
						// String ImageLocalPath =
						// arr_chatHistory.get(i).getLocalPath().concat(
						// "      "+ arr_chatHistory.get(i).getChatTime());
						showImageMessage(
								arr_chatHistory.get(i).getLocalPath(),
								arr_chatHistory
										.get(i)
										.getChatDate()
										.concat("  "
												+ arr_chatHistory.get(i)
														.getChatTime()), true);
					} else {
						showMessage(
								arr_chatHistory.get(i).getChatContents(),
								arr_chatHistory
										.get(i)
										.getChatDate()
										.concat("  "
												+ arr_chatHistory.get(i)
														.getChatTime()), true);
					}

				} else {
					if (arr_chatHistory.get(i).getChatContents().length() > 12
							&& arr_chatHistory.get(i).getChatContents()
									.substring(0, 13).equals(attachFile)) {
						// String ImageLocalPath =
						// arr_chatHistory.get(i).getLocalPath().concat(
						// "      "+ arr_chatHistory.get(i).getChatTime());
						showImageMessage(
								arr_chatHistory.get(i).getLocalPath(),
								arr_chatHistory
										.get(i)
										.getChatDate()
										.concat("  "
												+ arr_chatHistory.get(i)
														.getChatTime()), false);
					} else {
						showMessage(
								arr_chatHistory.get(i).getChatContents(),
								arr_chatHistory
										.get(i)
										.getChatDate()
										.concat("  "
												+ arr_chatHistory.get(i)
														.getChatTime()), false);
					}

				}
			}
		}

	}

	public static void retrieveMessage(final String message) {
		/*
		 * String text = message + "\n" +
		 * retrievedMessages.getText().toString();
		 * retrievedMessages.setText(text);
		 */
		// progressBar.setVisibility(View.INVISIBLE);

		Log.e("Message u receive is  : ", "------>" + message);

	}

	private void sendImageMessage(String attachImage, String localImage) {
		if (attachImage.length() > 0) {
			cnt++;
			try {
				Log.e("locaal image  : ", "------>" + localImage);
				myChatController.sendMessage(attachImage);
				String chatTimeMsg = databasehelper.insert_chatdata(
						Integer.parseInt(global.getJabberid()), Friendjabberid,
						attachImage, localImage, 0, currentusername);
				showImageMessage(localImage, chatTimeMsg, true);
			} catch (Exception e) {
				displayDialog("Error in Sending Image", ChatWindow.this, false);
				Log.e("Error in sending : ", "Error in sending Image ");
			}

		}

	}

	private void showImageMessage(String ImagePath, String dateTime,
			boolean leftSide) {
		String finalImageurl = null;
		// String Imagetime;
		if (leftSide == false) {
			finalImageurl = ImagePath;
			/*
			 * if(DateFormat.is24HourFormat(getApplicationContext()) == false) {
			 * //Imagetime = ImagePath.substring(ImagePath.length()-8).trim();
			 * finalImageurl =
			 * ImagePath.substring(0,ImagePath.length()-8).trim(); } else {
			 * //Imagetime = ImagePath.substring(ImagePath.length()-5).trim();
			 * finalImageurl =
			 * ImagePath.substring(0,ImagePath.length()-5).trim(); }
			 */
		} else {
			finalImageurl = ImagePath;
		}

		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater = LayoutInflater.from(ChatWindow.this);
		final View convertview = layoutInflater.inflate(R.layout.chat_image,
				null);

		LinearLayout ll_right = (LinearLayout) convertview
				.findViewById(R.id.ll_main_img_right);
		LinearLayout ll_left = (LinearLayout) convertview
				.findViewById(R.id.ll_main_img_left);
		if (leftSide == false) {

			ll_left.setVisibility(View.VISIBLE);
			ll_right.setVisibility(View.GONE);

			final ImageView img_chat_left = (ImageView) convertview
					.findViewById(R.id.img_chat_left);
			TextView txt_img_left_time = (TextView) convertview
					.findViewById(R.id.txt_chat_img_left_time);
			txt_img_left_time.setText(dateTime);
			Bitmap bitmap;

			try {
				Log.e("FIle Path is : ", finalImageurl);
				if (finalImageurl != null && finalImageurl != "") {
					File fileImagPath = new File(finalImageurl);
					if (fileImagPath.exists()) {
						int targetW = getResources().getDimensionPixelSize(
								R.dimen.attach_preview_margin);
						int targetH = getResources().getDimensionPixelSize(
								R.dimen.attach_preview_margin);

						// Get the dimensions of the bitmap
						BitmapFactory.Options bmOptions = new BitmapFactory.Options();
						bmOptions.inJustDecodeBounds = true;
						BitmapFactory.decodeFile(
								fileImagPath.getAbsolutePath(), bmOptions);
						int photoW = bmOptions.outWidth;
						int photoH = bmOptions.outHeight;

						// Determine how much to scale down the image
						int scaleFactor = Math.min(photoW / targetW, photoH
								/ targetH);

						// Decode the image file into a Bitmap sized to fill the
						// View
						bmOptions.inJustDecodeBounds = false;
						bmOptions.inSampleSize = scaleFactor;
						bmOptions.inPurgeable = true;

						bitmap = BitmapFactory.decodeFile(
								fileImagPath.getAbsolutePath(), bmOptions);
						img_chat_left.setImageBitmap(bitmap);

					} else {
						Log.e("file no exists : ", "exists");
						// userAttachImage.setImageResource(R.drawable.sh);
					}
				}

			} catch (Exception e) {
				Log.e("Error ", "error");
			} catch (java.lang.OutOfMemoryError e) {
				Log.e("Error ", "error");
			}

		} else {
			ll_right.setVisibility(View.VISIBLE);
			ll_left.setVisibility(View.GONE);
			ImageView img_chat_right = (ImageView) convertview
					.findViewById(R.id.img_chat_right);
			TextView txt_right_time = (TextView) convertview
					.findViewById(R.id.txt_chat_img_right_time);
			txt_right_time.setText(dateTime);
			Bitmap bitmap;

			try {
				Log.e("FIle Path is : ", finalImageurl);
				if (finalImageurl != null && finalImageurl != "") {
					File fileImagPath = new File(finalImageurl);
					if (fileImagPath.exists()) {
						int targetW = getResources().getDimensionPixelSize(
								R.dimen.attach_preview_margin);
						int targetH = getResources().getDimensionPixelSize(
								R.dimen.attach_preview_margin);

						// Get the dimensions of the bitmap
						BitmapFactory.Options bmOptions = new BitmapFactory.Options();
						bmOptions.inJustDecodeBounds = true;
						BitmapFactory.decodeFile(
								fileImagPath.getAbsolutePath(), bmOptions);
						int photoW = bmOptions.outWidth;
						int photoH = bmOptions.outHeight;

						// Determine how much to scale down the image
						int scaleFactor = Math.min(photoW / targetW, photoH
								/ targetH);

						// Decode the image file into a Bitmap sized to fill the
						// View
						bmOptions.inJustDecodeBounds = false;
						bmOptions.inSampleSize = scaleFactor;
						bmOptions.inPurgeable = true;

						bitmap = BitmapFactory.decodeFile(
								fileImagPath.getAbsolutePath(), bmOptions);
						img_chat_right.setImageBitmap(bitmap);

					} else {
						Log.e("file no exists : ", "exists");
						// userAttachImage.setImageResource(R.drawable.sh);
					}
				}

			} catch (Exception e) {
				Log.e("Error ", "error");
			} catch (java.lang.OutOfMemoryError e) {
				Log.e("Error ", "error");
			}

		}

		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				messagesContainer.addView(convertview);

			}
		});
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer = MediaPlayer.create(this, R.raw.messagereceived);
		try {
			mMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		/*
		 * mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC) ;
		 * mMediaPlayer.setLooping(true);
		 */
		mMediaPlayer.start();
		mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub

				mMediaPlayer.stop();
				mMediaPlayer.release();
			}
		});

		scrollDown();

		/*
		 * final RelativeLayout reLay = new RelativeLayout(this);
		 * LinearLayout.LayoutParams rlParams = new
		 * LinearLayout.LayoutParams(getResources
		 * ().getDimensionPixelSize(R.dimen
		 * .imagelyout),getResources().getDimensionPixelSize
		 * (R.dimen.imagelyout)); reLay.setLayoutParams(rlParams);
		 * 
		 * int bgRes = R.drawable.left_chatbox; if (!leftSide) { bgRes =
		 * R.drawable.right_chatbox; rlParams.gravity = Gravity.RIGHT; }
		 * 
		 * final ImageView userAttachImage = new ImageView(ChatWindow.this);
		 * userAttachImage
		 * .setMaxHeight(getResources().getDimensionPixelSize(R.dimen
		 * .attach_preview_margin));
		 * userAttachImage.setMaxWidth(getResources().getDimensionPixelSize
		 * (R.dimen.attach_preview_margin));
		 * userAttachImage.setScaleType(ImageView.ScaleType.FIT_XY);
		 * userAttachImage.setPadding(10, 10, 10, 10);
		 * userAttachImage.setLayoutParams(rlParams);
		 * userAttachImage.setBackgroundResource(bgRes); Bitmap bitmap;
		 * Log.e("FIle Path is : ", finalImageurl);
		 * 
		 * 
		 * 
		 * userAttachImage.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * try { if(finalImageurl!=null && finalImageurl!="") { File
		 * fileImagPath = new File(finalImageurl); if(fileImagPath.exists()) {
		 * Dialog dialog = new Dialog(ChatWindow.this);
		 * dialog.setContentView(R.layout.attach_image);
		 * dialog.setTitle(getString(R.string.app_name));
		 * dialog.setCancelable(true);
		 * dialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
		 * LayoutParams.FILL_PARENT); ImageView imageFull = (ImageView)
		 * dialog.findViewById(R.id.image_full); Uri uri =
		 * Uri.parse(finalImageurl); imageFull.setImageURI(uri); dialog.show();
		 * } else { userAttachImage.setImageResource(R.drawable.noimage); } }
		 * 
		 * } catch (Exception e) {
		 * 
		 * Log.e("Error", "error: " + e.getMessage(), e); }
		 * 
		 * } });
		 * 
		 * 
		 * RelativeLayout.LayoutParams tParams = new RelativeLayout.LayoutParams
		 * (LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		 * tParams.addRule(RelativeLayout.CENTER_HORIZONTAL,
		 * RelativeLayout.TRUE);
		 * tParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
		 * RelativeLayout.TRUE);
		 * tParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,
		 * RelativeLayout.TRUE); TextView text=new TextView(this);
		 * text.setText(dateTime); text.setTextColor(Color.WHITE);
		 * text.setPadding(0,0, 10, 15); text.setLayoutParams(tParams);
		 * reLay.addView(userAttachImage); reLay.addView(text); isAttachPress =
		 * true;
		 */

		/*
		 * runOnUiThread(new Runnable() {
		 * 
		 * @Override public void run() { messagesContainer.addView(reLay);
		 * isAttachPress =false; } });
		 * 
		 * scrollDown();
		 */
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.send:
			/*
			 * if (TextUtils.isEmpty(editText.getText().toString())) { return; }
			 */
			if (c.haveNetworkConnection(getApplicationContext())) {
				try {
					sendMessage();

					mMediaPlayer = new MediaPlayer();
					mMediaPlayer = MediaPlayer.create(this, R.raw.message_sent);
					try {
						mMediaPlayer.prepare();
					} catch (IllegalStateException e) {
						// TODO: handle exception
					} catch (Exception e) {
						// TODO: handle exception
					}
					/*
					 * mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
					 * ; mMediaPlayer.setLooping(true);
					 */
					mMediaPlayer.start();
					mMediaPlayer
							.setOnCompletionListener(new OnCompletionListener() {

								@Override
								public void onCompletion(MediaPlayer mp) {
									// TODO Auto-generated method stub

									mMediaPlayer.stop();
									mMediaPlayer.release();
								}
							});
				} catch (Exception e) {
					// TODO: handle exception
				}
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						ChatWindow.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			break;

		case R.id.cancel:
			finish();
			break;
		case R.id.camera:
			pickImageDialog();
			break;
		default:
			break;
		}
	}

	private void pickImageDialog() {

		AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
		myAlertDialog.setTitle("Change Pictures Option");
		myAlertDialog.setMessage("How do you want to set your picture?");

		myAlertDialog.setPositiveButton("Gallery",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						Intent pictureActionIntent = new Intent(
								Intent.ACTION_GET_CONTENT, null);
						pictureActionIntent.setType("image/*");
						pictureActionIntent.putExtra("return-data", true);
						global.chat_image = true;
						startActivityForResult(pictureActionIntent,
								GALLERY_PICTURE);
					}
				});

		myAlertDialog.setNegativeButton("Camera",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {

						Intent cameraIntent = new Intent(
								android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						global.chat_image = true;
						startActivityForResult(cameraIntent, CAMERA_REQUEST);

					}
				});
		myAlertDialog.show();

	}

	private void showMessage(String message, String chatDateTime, boolean b) {
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater = LayoutInflater.from(ChatWindow.this);
		final View convertview = layoutInflater.inflate(R.layout.chat, null);
		LinearLayout ll_right = (LinearLayout) convertview
				.findViewById(R.id.ll_main_right);
		LinearLayout ll_left = (LinearLayout) convertview
				.findViewById(R.id.ll_main_left);
		Log.e("Sending message is", message + "-");
		if (!b) {

			ll_left.setVisibility(View.VISIBLE);
			ll_right.setVisibility(View.GONE);

			TextView txt_chat_left = (TextView) convertview
					.findViewById(R.id.txt_chat_left);
			TextView txt_left_time = (TextView) convertview
					.findViewById(R.id.txt_chat_left_time);
			txt_left_time.setText(chatDateTime);
			txt_chat_left.setText(message);

			Log.e("message", "---" + txt_chat_left.getText().toString());

		} else {
			ll_right.setVisibility(View.VISIBLE);
			ll_left.setVisibility(View.GONE);
			TextView txt_chat_right = (TextView) convertview
					.findViewById(R.id.txt_chat_right);
			TextView txt_right_time = (TextView) convertview
					.findViewById(R.id.txt_chat_right_time);
			txt_right_time.setText(chatDateTime);
			txt_chat_right.setText(message);

			Log.e("message", "---" + txt_chat_right.getText().toString());

		}

		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				messagesContainer.addView(convertview);

			}
		});

		scrollDown();

	}

	/*
	 * @SuppressLint("SimpleDateFormat") public String setTime() { String
	 * displaytime = null; try { Date date = new Date();
	 * getDayOfMonthSuffix(date.getDay()); SimpleDateFormat fd = new
	 * SimpleDateFormat("MMM dd,yyyy HH:mm"); displaytime = fd.format(date);
	 * 
	 * } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * Log.e("current time is", displaytime + ""); return displaytime;
	 * 
	 * }
	 * 
	 * public String getDayOfMonthSuffix(final int n) { if (n >= 11 && n <= 13)
	 * { return "th"; } switch (n % 10) { case 1: return "st"; case 2: return
	 * "nd"; case 3: return "rd"; default: return "th"; } }
	 */

	private void scrollDown() {
		scrollContainer.post(new Runnable() {

			@Override
			public void run() {
				scrollContainer.fullScroll(View.FOCUSABLES_ALL);

			}
		});
	}

	private void sendMessage() {
		if (editText != null) {

			String messageString = editText.getText().toString();
			if (messageString.length() > 0) {
				String chatTimeMsg = databasehelper.insert_chatdata(
						Integer.parseInt(global.getJabberid()), Friendjabberid,
						messageString, friendphoto, 0, currentusername);

				int jabberExist = databasehelper
						.getJabberIdNoti(Friendjabberid);

				Log.e("Jabbbbeeeeeee", "------>" + jabberExist);

				if (jabberExist == 1) {
					databasehelper.updateFriendCount(0, Friendjabberid,
							messageString);
				} else {
					databasehelper.insertnoti(currentusername, messageString,
							friendphoto, "", 0, Friendjabberid);
				}

				myChatController.sendMessage(messageString);
				editText.setText("");
				showMessage(messageString, chatTimeMsg, true);

				/*
				 * if (cnt == 0) { databasehelper.insertnoti(currentusername,
				 * messageString, global.getPhoto(), chatTimeMsg, ++cnt,
				 * Friendjabberid)
				 */;
				GCMSender gs = new GCMSender();
				// String name = pQBUser.getFullName() != null ?
				// pQBUser.getFullName() : pQBUser.getLogin();
				StringBuilder sb = new StringBuilder();

				sb.append(me.getLogin())
						.append(GlobalConsts.ATTACH_INDICATOR_NOTIFICATION)
						.append(messageString)
						.append(GlobalConsts.ATTACH_INDICATOR_NOTIFICATION)
						.append(global.getJabberid());

				gs.sendPushNotifications(Friendjabberid, sb.toString());
				Log.e("sb", sb.toString());

				/*
				 * } else { databasehelper.updateFriendCount(++cnt,
				 * Integer.parseInt(loginjabberid));
				 * 
				 * GCMSender gs = new GCMSender(); // String name =
				 * pQBUser.getFullName() != null ? // pQBUser.getFullName() :
				 * pQBUser.getLogin(); StringBuilder sb = new StringBuilder();
				 * 
				 * sb.append(me.getLogin()).append(" : ")
				 * .append(messageString).append(" / ")
				 * .append(global.getJabberid());
				 * 
				 * gs.sendPushNotifications(Friendjabberid, sb.toString());
				 * Log.e("sb", sb.toString()); }
				 */

				Model model = new Model();
				model.setChat_name(currentusername);
				model.setChat_jabberid(Friendjabberid + "");
				model.setChat_image(friendphoto);
				model.setChat_date(arr_chatHistory.get(
						arr_chatHistory.size() - 1).getChatDate()
						+ " at "
						+ arr_chatHistory.get(arr_chatHistory.size() - 1)
								.getChatTime());
				model.setMessage(messageString);
				global.Convlist.add(model);
				global.setChatList(global.Convlist);
				global.setFriendjabberid(Friendjabberid + "");
				global.setFriendname(currentusername);

				Log.e("sending details", currentusername + "--"
						+ Friendjabberid + "--" + friendphoto + "--"
						+ messageString);

			}
		}

	}

	private MyChatController.OnMessageReceivedListener onMessageReceivedListener = new MyChatController.OnMessageReceivedListener() {

		@Override
		public void onMessageReceived(Message message) {
			final String messageString = message.getBody();

			String receiverJabberId = message.getFrom();
			int reciverId = Integer.parseInt(receiverJabberId.substring(0,
					receiverJabberId.lastIndexOf("-")));
			// chatMessage =messageStringReceive + "      " + chatTimeMsg;
			Log.e("Receiver Id  : ", "------>" + reciverId + "------>"
					+ Friendjabberid);
			try {

				if (String.valueOf(Friendjabberid).equals(
						String.valueOf(reciverId))) {

					Log.e("Both are same : ", "both are same");
					runOnUiThread(new Runnable() {
						@Override
						public void run() {

							if (messageString.length() > 12
									&& messageString.substring(0, 13).equals(
											attachFile)) {
								// cntDownload++;
								receiveImageURL = messageString;
								String imagerecURL = messageString.substring(
										messageString.lastIndexOf(")") + 1,
										messageString.length());
								new DownLoadTask().execute(imagerecURL);
								mMediaPlayer = new MediaPlayer();
								mMediaPlayer = MediaPlayer.create(
										ChatWindow.this, R.raw.messagereceived);
								try {
									mMediaPlayer.prepare();
								} catch (IllegalStateException e) {
									// TODO: handle exception
								} catch (Exception e) {
									// TODO: handle exception
								}
								/*
								 * mMediaPlayer.setAudioStreamType(AudioManager.
								 * STREAM_MUSIC) ;
								 * mMediaPlayer.setLooping(true);
								 */
								mMediaPlayer.start();
								mMediaPlayer
										.setOnCompletionListener(new OnCompletionListener() {

											@Override
											public void onCompletion(
													MediaPlayer mp) {
												// TODO Auto-generated method
												// stub

												mMediaPlayer.stop();
												mMediaPlayer.release();
											}
										});
							} else {
								String chatTimeMsg = databasehelper
										.insert_chatdata(Friendjabberid,
												Integer.parseInt(global
														.getJabberid()),
												messageString, friendphoto, 0,
												currentusername);

								int jabberExist = databasehelper
										.getJabberIdNoti(Friendjabberid);

								Log.e("Jabbbbeeeeeee", "------>" + jabberExist);

								if (jabberExist == 1) {
									databasehelper.updateFriendCount(0,
											Friendjabberid, messageString);
								} else {
									databasehelper.insertnoti(currentusername,
											messageString, "", "", 0,
											Friendjabberid);
								}
								showMessage(messageString, chatTimeMsg, false);
								mMediaPlayer = new MediaPlayer();
								mMediaPlayer = MediaPlayer.create(
										ChatWindow.this, R.raw.messagereceived);
								try {
									mMediaPlayer.prepare();
								} catch (IllegalStateException e) {
									// TODO: handle exception
								} catch (Exception e) {
									// TODO: handle exception
								}
								/*
								 * mMediaPlayer.setAudioStreamType(AudioManager.
								 * STREAM_MUSIC) ;
								 * mMediaPlayer.setLooping(true);
								 */
								mMediaPlayer.start();
								mMediaPlayer
										.setOnCompletionListener(new OnCompletionListener() {

											@Override
											public void onCompletion(
													MediaPlayer mp) {
												// TODO Auto-generated method
												// stub

												mMediaPlayer.stop();
												mMediaPlayer.release();
											}
										});
							}

						}
					});

				}
			} catch (Exception e) {
				// TODO: handle exception
			}

			/*
			 * Log.e("receiver id is and friend is id : ", "" +
			 * app.getJabber_id() + "------> " + friendjabberid); String
			 * chatTimeMsg = databashelper.insert_chatdata(friendjabberid,
			 * app.getJabber_id(), messageString); chatMessage = messageString +
			 * "      " + chatTimeMsg;
			 */

		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == GALLERY_PICTURE) {
			if (resultCode == RESULT_OK) {
				if (data != null) {

					try {
						final Cursor cursor;
						final int column_index;
						progressDialog.show();
						String[] projection = { MediaStore.Images.Media.DATA };
						cursor = managedQuery(data.getData(), projection, null,
								null, null);
						column_index = cursor
								.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
						cursor.moveToFirst();
						// localPathImage = cursor.getString(column_index);

						QBAuth.createSession(me, new QBCallback() {

							@Override
							public void onComplete(Result arg0, Object arg1) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onComplete(Result result) {

								if (result.isSuccess()) {
									downImage(cursor.getString(column_index));
								} else {

								}

							}
						});

						/*
						 * BitmapDrawable bmpDrawable = null; Uri selectedImage
						 * = data.getData(); String[] filePathColumn = {
						 * MediaStore.Images.Media.DATA }; String sel =
						 * MediaStore.Images.Media._ID + "=?"; Cursor cursor =
						 * getContentResolver().query(selectedImage,
						 * filePathColumn, null, null, null); if (cursor !=
						 * null) { cursor.moveToFirst();
						 * 
						 * int columnIndex = cursor
						 * .getColumnIndex(filePathColumn[0]); fileSrc =
						 * cursor.getString(columnIndex); imageName = fileSrc
						 * .substring(fileSrc.lastIndexOf("/") + 1); try {
						 * bitmap = BitmapFactory.decodeFile(fileSrc); } catch
						 * (Exception e) { // TODO: handle exception
						 * Toast.makeText(getApplicationContext(),
						 * "Sorry, This image can't be uploaded",
						 * Toast.LENGTH_LONG).show(); } catch (OutOfMemoryError
						 * e) { // TODO: handle exception
						 * Toast.makeText(getApplicationContext(),
						 * "Sorry, This image can't be uploaded",
						 * Toast.LENGTH_LONG).show(); } cursor.close();
						 * 
						 * new UploadImageTask().execute();
						 * 
						 * } else {
						 * 
						 * bmpDrawable = new BitmapDrawable(getResources(), data
						 * .getData().getPath()); Log.e("", bmpDrawable + "");
						 * userpic.setImageDrawable(bmpDrawable); }
						 */

					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(getApplicationContext(),
								"Sorry, This image can't be uploaded",
								Toast.LENGTH_LONG).show();
					} catch (OutOfMemoryError e) {
						// TODO: handle exception
						Toast.makeText(getApplicationContext(),
								"Sorry, This image can't be uploaded",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(getApplicationContext(), "Cancelled",
							Toast.LENGTH_SHORT).show();
				}
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(getApplicationContext(), "Cancelled",
						Toast.LENGTH_SHORT).show();
			}
		} else if (requestCode == CAMERA_REQUEST) {

			try {
				if (resultCode == RESULT_OK) {

					bitmap_camera = (Bitmap) data.getExtras().get("data");
					saveImage();
					if (fileSrc != null || !fileSrc.equals("")) {
						if (c.haveNetworkConnection(getApplicationContext())) {
							QBAuth.createSession(me, new QBCallback() {

								@Override
								public void onComplete(Result arg0, Object arg1) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onComplete(Result result) {

									if (result.isSuccess()) {
										progressDialog.show();
										Log.e("File Name is : ", "----->"
												+ fileSrc);
										downImage(fileSrc);
									} else {

									}

								}
							});
						} else {
							AlertDialog.Builder dialog2 = new AlertDialog.Builder(
									ChatWindow.this);
							dialog2.setTitle("Instamour");
							dialog2.setMessage("No Data Connection Avaible");
							dialog2.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub

										}
									});
							AlertDialog alert2 = dialog2.create();
							alert2.show();
						}
					}

				} else if (resultCode == RESULT_CANCELED) {
					Log.e("Canceled", "canceled");
				}
			} catch (Exception e) {
				// TODO: handle exception
				Toast.makeText(getApplicationContext(),
						"Sorry, This image can't be uploaded",
						Toast.LENGTH_LONG).show();
			} catch (OutOfMemoryError e) {
				// TODO: handle exception
				Toast.makeText(getApplicationContext(),
						"Sorry, This image can't be uploaded",
						Toast.LENGTH_LONG).show();
			}

		}

	}

	private void downImage(final String imgPath) {

		Log.e("Image Path in new Function  : ", imgPath);

		synchronized (imgPath) {

			runOnUiThread(new Runnable() {

				@Override
				public void run() {

					QBContent.uploadFileTask(imgPath, true, new QBCallback() {
						@Override
						public void onComplete(Result result) {
							if (result.isSuccess()) {

								QBFileUploadTaskResult qbFileUploadTaskResultq = (QBFileUploadTaskResult) result;
								String attachFileLinkImage = attachFile
										+ qbFileUploadTaskResultq.getFile()
												.getPublicUrl();
								Log.e("File Path is  : ", ""
										+ qbFileUploadTaskResultq.getFile()
												.getPublicUrl()
										+ " appended link is  : "
										+ attachFileLinkImage);
								sendImageMessage(attachFileLinkImage, imgPath);
								progressDialog.dismiss();

							} else {

								Log.e("Hellooooooo ur errror is  : ", ""
										+ result.getErrors());
								progressDialog.dismiss();
								displayDialog(
										"Sorry this Image Cannot be Uploaded",
										ChatWindow.this, false);
							}

						}

						@Override
						public void onComplete(Result result, Object o) {

						}
					});

				}
			});
			imgPath.notify();
		}

	}

	private void saveImage() {

		int counting = 0;
		OutputStream fOut = null;
		Uri outputFileUri;

		try {
			File root = new File(Environment.getExternalStorageDirectory()
					+ "/instamour/");
			if (!root.exists()) {
				root.mkdirs();
			}
			String imageName = "image_" + String.valueOf(counting) + ".jpg";

			File sdImageMainDirectory = new File(root, imageName);
			while (sdImageMainDirectory.exists()) {
				counting++;
				imageName = "image_" + String.valueOf(counting) + ".jpg";
				sdImageMainDirectory = new File(root, imageName);
				fileSrc = sdImageMainDirectory.toString();

			}
			Log.e("captured image", sdImageMainDirectory + "");
			outputFileUri = Uri.fromFile(sdImageMainDirectory);
			fOut = new FileOutputStream(sdImageMainDirectory);
		} catch (Exception e) {
			Log.e("Incident Photo", "Error occured. Please try again later.");
		}
		try {
			bitmap_camera.compress(Bitmap.CompressFormat.JPEG, 90, fOut);

			fOut.flush();
			fOut.close();

		} catch (Exception e) {
		}

	}

	public class DownLoadTask extends AsyncTask<String, Void, Void> {
		String imageURL;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// String messageurl =
			// messageStringReceive.substring(messageStringReceive.lastIndexOf(")")+1,
			// messageStringReceive.length());
			// downLoadImage = messageurl;
		}

		@Override
		protected Void doInBackground(String... params) {
			// Log.e("file url received is  : ", params[0]);
			imageURL = params[0];
			downloadFile(imageURL);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			// dialog.dismiss();
			Log.e("download taskkkkk receive file image is  : ", ""
					+ ReceiveFileImage.getPath());
			String chatTimeMsg = databasehelper.insert_chatdata(Friendjabberid,
					Integer.parseInt(global.getJabberid()), receiveImageURL,
					ReceiveFileImage.getPath(), 0, currentusername);
			// chatMessage =ReceiveFileImage.getPath() + "      " + chatTimeMsg;
			showImageMessage(ReceiveFileImage.getPath(), chatTimeMsg, false);
			// image.setImageDrawable(drawable);
		}

	}

	public void downloadFile(String sourceFileUri) {

		try {
			// File root = android.os.Environment.getExternalStorageDirectory()
			// ;
			File dir = new File(Environment.getExternalStorageDirectory()
					+ "/Instamour/");
			if (dir.exists() == false) {
				dir.mkdirs();
			}

			long startTime = System.currentTimeMillis();
			String fileName = String.valueOf((System.currentTimeMillis()))
					+ ".jpg";
			URL url = new URL(sourceFileUri); // you can write here any link
			// File file = new File(dir, fileName);

			// URL url = new URL ("file://some/path/anImage.png");
			InputStream input = url.openStream();
			try {
				// The sdcard directory e.g. '/sdcard' can be used directly, or
				// more safely abstracted with getExternalStorageDirectory()
				// File storagePath = Environment.getExternalStorageDirectory();
				ReceiveFileImage = new File(dir, fileName);
				OutputStream output = new FileOutputStream(ReceiveFileImage);
				try {
					byte[] buffer = new byte[6000];
					int bytesRead = 0;
					while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
						output.write(buffer, 0, bytesRead);
					}
				} finally {
					output.close();
				}
			} finally {
				input.close();
			}

			Log.e("Download Image Path iss : ", ReceiveFileImage.getPath());
			Log.e("DownloadManager",
					"download ready in "
							+ ((System.currentTimeMillis() - startTime) / 1000)
							+ " sec");
		} catch (IOException e) {
			Log.e("DownloadManager", "Error: " + e);
			// dialog.dismiss();
		}

	}

	public void displayDialog(String msg, final Context context,
			final boolean isFinish) {

		final AlertDialog alertDialog = new AlertDialog.Builder(context)
				.create();
		alertDialog.setMessage(msg);
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (isFinish)
					((Activity) context).finish();

			}
		});
		alertDialog.show();
	}

	@SuppressWarnings("static-access")
	@Override
	protected void onDestroy() {
		super.onDestroy();
		// unregister for gcms
		C2DMessaging.unregister(ChatWindow.this);
		try {
			myChatController.connection.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
		}
		finish();
	}

	@SuppressWarnings("static-access")
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		try {
			myChatController.connection.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
