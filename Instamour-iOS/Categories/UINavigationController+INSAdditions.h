//
//  UINavigationController+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (INSAdditions)

- (void)applyGlobalStyleINS;

@end
