//
//  INSActivityItemProvider.h
//  Instamour
//
//  Created by Brian Slick on 8/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INSActivityItemProvider : UIActivityItemProvider

// Public Properties

@property (nonatomic, copy) NSString *checkOutThisThingText;
// "... Instamour.com" will be appended. So include "from" or "on", etc, in text. Ex: "Check out this link from". Don't add trailing space.


@property (nonatomic, copy) NSString *urlString;

@end
