//
//  INSAudioVideoCallViewController.h
//  Instamour
//
//  Created by Brian Slick on 8/2/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIViewController.h"
@class INSUser;

// Public Constants

//typedef NS_ENUM(NSInteger, INSCallType) {
//    INSCallTypeAudio,
//    INSCallTypeVideo,
//};

// Protocols

@interface INSAudioVideoCallViewController : BTIViewController

// Public Properties
@property (nonatomic, assign) enum QBVideoChatConferenceType callType;
@property (nonatomic, strong) INSUser *otherUser;
@property (nonatomic, assign, getter = isIncomingCall) BOOL incomingCall;
@property (nonatomic, copy) NSString *incomingSessionID;

// Public Methods

@end
