//
//  INSCustomSearchViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSCustomSearchViewController.h"

// Models and other global

// Sub-controllers
#import "INSBrowseProfilesViewController.h"

// Views
#import "TPKeyboardAvoidingTableView.h"
#import "INSPickerInputView.h"
#import "INSLabelAndTextFieldTableCell.h"
#import "INSGenderTableCell.h"

// Private Constants

typedef NS_ENUM(NSInteger, INSAgePickerComponents) {
    INSAgePickerComponentsMinimumAge = 0,
    INSAgePickerComponentsTo,
    INSAgePickerComponentsMaximumAge,
};

NSString *const INSSearchRowIdentifierUserName = @"INSSearchRowIdentifierUserName";
NSString *const INSSearchRowIdentifierChildren = @"INSSearchRowIdentifierChildren";
NSString *const INSSearchRowIdentifierGender = @"INSSearchRowIdentifierGender";
NSString *const INSSearchRowIdentifierBodyType = @"INSSearchRowIdentifierBodyType";
NSString *const INSSearchRowIdentifierAgeRange = @"INSSearchRowIdentifierAgeRange";
NSString *const INSSearchRowIdentifierLookingFor = @"INSSearchRowIdentifierLookingFor";
NSString *const INSSearchRowIdentifierVideo = @"INSSearchRowIdentifierVideo";
NSString *const INSSearchRowIdentifierMiles = @"INSSearchRowIdentifierMiles";
NSString *const INSSearchRowIdentifierLastOnline = @"INSSearchRowIdentifierLastOnline";
NSString *const INSSearchRowIdentifierEthnicity = @"INSSearchRowIdentifierEthnicity";
NSString *const INSSearchRowIdentifierSmoker = @"INSSearchRowIdentifierSmoker";
NSString *const INSSearchRowIdentifierSexualPreference = @"INSSearchRowIdentifierSexualPreference";

@interface INSCustomSearchViewController () <UITextFieldDelegate, INSPickerInputViewDelegate, INSGenderTableCellDelegate>

// Private Properties
@property (nonatomic, strong) INSPickerInputView *pickerInputView;

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *children;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *bodyType;
@property (nonatomic, assign) NSInteger minimumAge;
@property (nonatomic, assign) NSInteger maximumAge;
@property (nonatomic, copy) NSString *lookingFor;
@property (nonatomic, assign) NSInteger video;
@property (nonatomic, copy) NSString *miles;
@property (nonatomic, copy) NSString *lastOnline;
@property (nonatomic, copy) NSString *ethnicity;
@property (nonatomic, copy) NSString *smoker;
@property (nonatomic, copy) NSString *sexualPreference;

@property (nonatomic, strong) UITextField *activeTextField;
@property (nonatomic, copy) NSArray *activePickerList;

@property (nonatomic, copy) NSSet *listPickerRowIdentifiers;
@property (nonatomic, copy) NSSet *textFieldRowIdentifiers;

@property (nonatomic, copy) NSArray *childrenPickerContents;
@property (nonatomic, copy) NSArray *bodyTypePickerContents;
@property (nonatomic, copy) NSArray *ageRangePickerContents;
@property (nonatomic, copy) NSArray *lookingForPickerContents;
@property (nonatomic, copy) NSArray *videoPickerContents;
@property (nonatomic, copy) NSArray *milesPickerContents;
@property (nonatomic, copy) NSArray *lastOnlinePickerContents;
@property (nonatomic, copy) NSArray *ethnicityPickerContents;
@property (nonatomic, copy) NSArray *smokerPickerContents;
@property (nonatomic, copy) NSArray *sexualPreferencePickerContents;

@end

@implementation INSCustomSearchViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters

- (INSPickerInputView *)pickerInputView
{
    if (_pickerInputView == nil)
    {
        _pickerInputView = [INSPickerInputView pickerInputViewMode:INSPickerInputViewModeAllButtons
                                                          delegate:self];
        [_pickerInputView setPreventBlankRowSelection:NO];
    }
    return _pickerInputView;
}

- (NSSet *)listPickerRowIdentifiers
{
    if (_listPickerRowIdentifiers == nil)
    {
        _listPickerRowIdentifiers = [[NSSet alloc] initWithObjects:INSSearchRowIdentifierChildren, INSSearchRowIdentifierBodyType, INSSearchRowIdentifierAgeRange, INSSearchRowIdentifierLookingFor, INSSearchRowIdentifierVideo, INSSearchRowIdentifierMiles, INSSearchRowIdentifierLastOnline, INSSearchRowIdentifierEthnicity, INSSearchRowIdentifierSmoker, INSSearchRowIdentifierSexualPreference, nil];
    }
    return _listPickerRowIdentifiers;
}

- (NSSet *)textFieldRowIdentifiers
{
    if (_textFieldRowIdentifiers == nil)
    {
        NSMutableSet *identifiers = [NSMutableSet setWithSet:[self listPickerRowIdentifiers]];
        [identifiers addObject:INSSearchRowIdentifierUserName];
        _textFieldRowIdentifiers = [[NSSet alloc] initWithSet:identifiers];
    }
    return _textFieldRowIdentifiers;
}

- (NSArray *)childrenPickerContents
{
    if (_childrenPickerContents == nil)
    {
        _childrenPickerContents = @[ INSPickerInputViewBlankValue, INSChildrenStatusYes, INSChildrenStatusNo, INSChildrenStatusNotWithMe ];
    }
    return _childrenPickerContents;
}

- (NSArray *)bodyTypePickerContents
{
    if (_bodyTypePickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allBodyTypesINS]];
        
        _bodyTypePickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _bodyTypePickerContents;
}

- (NSArray *)ageRangePickerContents
{
    if (_ageRangePickerContents == nil)
    {
        NSMutableArray *ages = [NSMutableArray array];
        for (NSInteger age = INSAgeRangeMinimum; age <= INSAgeRangeMaximum; age++)
        {
            [ages addObject:[NSString stringWithFormat:@"%ld", (long)age]];
        }
        
        _ageRangePickerContents = [[NSArray alloc] initWithArray:ages];
    }
    return _ageRangePickerContents;
}

- (NSArray *)lookingForPickerContents
{
    if (_lookingForPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allLookingForsINS]];
        
        _lookingForPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _lookingForPickerContents;
}

- (NSArray *)videoPickerContents
{
    if (_videoPickerContents == nil)
    {
        NSInteger numberOfVideos = 4;
        
        NSMutableArray *videos = [NSMutableArray array];
        [videos addObject:INSPickerInputViewBlankValue];
        
        for (NSInteger index = 1; index <= numberOfVideos; index++)
        {
            [videos addObject:[NSString stringWithFormat:@"At least %ld", (long)index]];
        }
        
        _videoPickerContents = [NSArray arrayWithArray:videos];
    }
    return _videoPickerContents;
}

- (NSArray *)milesPickerContents
{
    if (_milesPickerContents == nil)
    {
        _milesPickerContents = @[ INSPickerInputViewBlankValue, @"5", @"10", @"25", @"50", @"100", @"200" ];
    }
    return _milesPickerContents;
}

- (NSArray *)lastOnlinePickerContents
{
    if (_lastOnlinePickerContents == nil)
    {
        _lastOnlinePickerContents = @[ INSPickerInputViewBlankValue, @"Online Now!", @"Yesterday", @"In the last week", @"In the last month" ];
    }
    return _lastOnlinePickerContents;
}

- (NSArray *)ethnicityPickerContents
{
    if (_ethnicityPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allEthnicitiesINS]];
        
        _ethnicityPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _ethnicityPickerContents;
}

- (NSArray *)smokerPickerContents
{
    if (_smokerPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allSmokersINS]];
        
        _smokerPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _smokerPickerContents;
}

- (NSArray *)sexualPreferencePickerContents
{
    if (_sexualPreferencePickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allSexualPreferencesINS]];
        
        _sexualPreferencePickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _sexualPreferencePickerContents;
}

#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Search"];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    UIBarButtonItem *goButton = [[UIBarButtonItem alloc] initWithTitle:@"Go"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(goButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:goButton];
    
    // Toolbar
    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc] initWithTitle:@"Clear Fields"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(clearButtonPressed:)];
    
    [self setToolbarItems:@[ flexItem, clearButton ]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Toolbar
    
    [[self navigationController] setToolbarHidden:NO animated:YES];
    
    // Analytics
    
    
    // User Interface
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self setUserName:[userDefaults searchTermUserNameINS]];
    [self setChildren:[userDefaults searchTermChildrenINS]];
    [self setGender:[userDefaults searchTermGenderINS]];
    [self setBodyType:[userDefaults searchTermBodyTypeINS]];
    [self setMinimumAge:[userDefaults searchTermMinimumAgeINS]];
    [self setMaximumAge:[userDefaults searchTermMaximumAgeINS]];
    [self setLookingFor:[userDefaults searchTermLookingForINS]];
    [self setVideo:[userDefaults searchTermVideoCountINS]];
    [self setMiles:[userDefaults searchTermMilesINS]];
    [self setLastOnline:[userDefaults searchTermLastOnlineINS]];
    [self setEthnicity:[userDefaults searchTermEthnicityINS]];
    [self setSmoker:[userDefaults searchTermSmokerINS]];
    [self setSexualPreference:[userDefaults searchTermSexualPreferenceINS]];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableContentsManager *manager = [self mainContentsManager];
    [manager reset];
    
    BTITableRowInfo *rowInfo = nil;
    
    {{  // Username
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierUserName];
        [rowInfo setText:@"User Name"];
        [rowInfo setDetailText:[self userName]];
    }}
    
    {{  // Children
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierChildren];
        [rowInfo setText:@"Children"];
        [rowInfo setDetailText:[self children]];
    }}
    
    {{  // Gender
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierGender];
        [rowInfo setText:@"Gender"];
    }}
    
    {{  // Body Type
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierBodyType];
        [rowInfo setText:@"Body Type"];
        [rowInfo setDetailText:[self bodyType]];
    }}
    
    {{  // Age Range
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierAgeRange];
        [rowInfo setText:@"Age Range"];
        [rowInfo setDetailText:[self formattedAgeRangeString]];
    }}
    
    {{  // Looking For
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierLookingFor];
        [rowInfo setText:@"Looking For"];
        [rowInfo setDetailText:[self lookingFor]];
    }}
    
    {{  // Video
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierVideo];
        [rowInfo setText:@"Video"];
        [rowInfo setDetailText:[self formattedVideoCountString]];
    }}
    
    {{  // Miles
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierMiles];
        [rowInfo setText:@"Miles"];
        [rowInfo setDetailText:[self formattedVideoCountString]];
    }}
    
    {{  // Last online
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierLastOnline];
        [rowInfo setText:@"Last online"];
        [rowInfo setDetailText:[self formattedVideoCountString]];
    }}
    
    {{  // Ethnicity
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierEthnicity];
        [rowInfo setText:@"Ethnicity"];
        [rowInfo setDetailText:[self ethnicity]];
    }}
    
    {{  // Smoker
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierSmoker];
        [rowInfo setText:@"Smoker"];
        [rowInfo setDetailText:[self smoker]];
    }}
    
    {{  // Sexual Preference
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSearchRowIdentifierSexualPreference];
        [rowInfo setText:@"Preference"];
        [rowInfo setDetailText:[self sexualPreference]];
    }}
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [INSLabelAndTextFieldTableCell registerNibForTableViewBTI:tableView];
    [INSGenderTableCell registerNibForTableViewBTI:tableView];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers


#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)goButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self activeTextField] resignFirstResponder];
    
    [[INSUserManager sharedManager] clearSearchUsers];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [self userName];
    NSString *children = [self children];
    NSString *gender = [self gender];
    NSString *bodyType = [self bodyType];
    NSString *ageRangeString = [self formattedAgeRangeString];
    NSString *lookingFor = [self lookingFor];
    NSString *video = [self formattedVideoCountString];
    NSString *distance = [self miles];
    NSString *lastOnline = [self lastOnline];
    NSString *ethnicity = [self ethnicity];
    NSString *smoker = [self smoker];
    NSString *sexualPreference = [self sexualPreference];
    
    BOOL isAtLeastOneFieldPopulated = ( [userName isNotEmpty] || [children isNotEmpty] || [gender isNotEmpty] || [bodyType isNotEmpty] || [ageRangeString isNotEmpty] || [lookingFor isNotEmpty] || [video isNotEmpty] || [distance isNotEmpty] || [lastOnline isNotEmpty] || [ethnicity isNotEmpty] || [smoker isNotEmpty] || [sexualPreference isNotEmpty] );
    
    if (!isAtLeastOneFieldPopulated)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instamour"
                                                        message:@"Please select at least one field to search"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No search criteria", self, __PRETTY_FUNCTION__);
        return;
    }
    
    // Save values
    
    [userDefaults setSearchTermUserNameINS:userName];
    [userDefaults setSearchTermChildrenINS:children];
    [userDefaults setSearchTermGenderINS:gender];
    [userDefaults setSearchTermBodyTypeINS:bodyType];
    [userDefaults setSearchTermMinimumAgeINS:[self minimumAge]];
    [userDefaults setSearchTermMaximumAgeINS:[self maximumAge]];
    [userDefaults setSearchTermLookingForINS:lookingFor];
    [userDefaults setSearchTermVideoCountINS:[self video]];
    [userDefaults setSearchTermMilesINS:distance];
    [userDefaults setSearchTermLastOnlineINS:lastOnline];
    [userDefaults setSearchTermEthnicityINS:ethnicity];
    [userDefaults setSearchTermSmokerINS:smoker];
    [userDefaults setSearchTermSexualPreferenceINS:sexualPreference];
    
    [userDefaults synchronize];
    
    INSBrowseProfilesViewController *mainController = [[INSBrowseProfilesViewController alloc] initWithDisplayMode:INSBrowseProfilesDisplayModeCustomSearch];
    
    [[self navigationController] pushViewController:mainController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)clearButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSUserManager sharedManager] clearSearchUsers];
    
    [[NSUserDefaults standardUserDefaults] clearAllSearchCriteriaINS];
    
    [self setUserName:nil];
    [self setChildren:nil];
    [self setGender:nil];
    [self setBodyType:nil];
    [self setMinimumAge:NSNotFound];
    [self setMaximumAge:NSNotFound];
    [self setLookingFor:nil];
    [self setVideo:NSNotFound];
    [self setMiles:nil];
    [self setLastOnline:nil];
    [self setEthnicity:nil];
    [self setSmoker:nil];
    [self setSexualPreference:nil];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (NSString *)formattedAgeRangeString
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger minimumAge = [self minimumAge];
    NSInteger maximumAge = [self maximumAge];
    
    if ( (minimumAge == NSNotFound) || (minimumAge < INSAgeRangeMinimum) || (minimumAge > INSAgeRangeMaximum) )
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid minimum age", self, __PRETTY_FUNCTION__);
        return nil;
    }
    
    if ( (maximumAge == NSNotFound) || (maximumAge < INSAgeRangeMinimum) || (maximumAge > INSAgeRangeMaximum) )
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid maximum age", self, __PRETTY_FUNCTION__);
        return nil;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return [NSString stringWithFormat:@"%ld - %ld", (long)minimumAge, (long)maximumAge];
}

- (NSString *)formattedVideoCountString
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger videoCount = [self video];
    
    if ( (videoCount == NSNotFound) || (videoCount == 0) || (videoCount > [[self videoPickerContents] count]) )
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid count", self, __PRETTY_FUNCTION__);
        return nil;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return [[self videoPickerContents] objectAtIndex:videoCount];
}

- (void)loadPickerViewDataForRowIdentifier:(NSString *)rowIdentifier
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSPickerInputView *pickerInputView = [self pickerInputView];
    [pickerInputView removeContents];
    
    if ([rowIdentifier isEqualToString:INSSearchRowIdentifierAgeRange])
    {
        NSArray *ageList = [self ageRangePickerContents];
        
        NSInteger minimumAge = [self minimumAge];
        NSInteger maximumAge = [self maximumAge];
        
        if (minimumAge == NSNotFound)
        {
            minimumAge = INSAgeRangeMinimum;
            [self setMinimumAge:minimumAge];
        }
        
        if (maximumAge == NSNotFound)
        {
            maximumAge = INSAgeRangeMinimum;
            [self setMaximumAge:maximumAge];
        }
        
        NSString *minimumAgeString = [NSString stringWithFormat:@"%ld", (long)minimumAge];
        NSString *maximumAgeString = [NSString stringWithFormat:@"%ld", (long)maximumAge];
        
        NSInteger minimumAgeRow = [ageList indexOfObject:minimumAgeString];
        NSInteger maximumAgeRow = [ageList indexOfObject:maximumAgeString];
        
        [pickerInputView setPickerViewContents:ageList forComponent:INSAgePickerComponentsMinimumAge preselectingRow:minimumAgeRow];
        [pickerInputView setPickerViewContents:@[ @"To" ] forComponent:INSAgePickerComponentsTo preselectingRow:NSNotFound];
        [pickerInputView setPickerViewContents:ageList forComponent:INSAgePickerComponentsMaximumAge preselectingRow:maximumAgeRow];
        
        return;
    }
    
    if ([rowIdentifier isEqualToString:INSSearchRowIdentifierChildren])
    {
        [self setActivePickerList:[self childrenPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierBodyType])
    {
        [self setActivePickerList:[self bodyTypePickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierLookingFor])
    {
        [self setActivePickerList:[self lookingForPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierVideo])
    {
        [self setActivePickerList:[self videoPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierMiles])
    {
        [self setActivePickerList:[self milesPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierLastOnline])
    {
        [self setActivePickerList:[self lastOnlinePickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierEthnicity])
    {
        [self setActivePickerList:[self ethnicityPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierSmoker])
    {
        [self setActivePickerList:[self smokerPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierSexualPreference])
    {
        [self setActivePickerList:[self sexualPreferencePickerContents]];
    }
    else
    {
        [self setActivePickerList:nil];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not a list field", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *selectedText = [[self activeTextField] text];
    NSInteger preselectedRow = NSNotFound;
    
    if ([selectedText isNotEmpty])
    {
        preselectedRow = [[self activePickerList] indexOfObject:selectedText];
    }
    
    [pickerInputView setPickerViewContents:[self activePickerList] forComponent:0 preselectingRow:preselectedRow];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self mainContentsManager] numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContentsManager] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    UITableViewCell *cellToReturn = nil;
    
    if ([[self textFieldRowIdentifiers] containsObject:rowIdentifier])
    {
        INSLabelAndTextFieldTableCell *textFieldCell = [INSLabelAndTextFieldTableCell dequeueCellFromTableViewBTI:tableView];
        
        UITextField *textField = [textFieldCell textField];
        [textField setDelegate:self];
        
        [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        
        [textField setPlaceholder:[rowInfo text]];
        [textField setText:[rowInfo detailText]];
        
        [textFieldCell setLabelText:[rowInfo text]];
        
        cellToReturn = textFieldCell;
    }
    else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierGender])
    {
        INSGenderTableCell *genderCell = [INSGenderTableCell dequeueCellFromTableViewBTI:tableView];
        [genderCell setDelegate:self];
        
        [genderCell setGender:[self gender]];
        
        cellToReturn = genderCell;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cellToReturn;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setActiveTextField:textField];
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    if ([[self listPickerRowIdentifiers] containsObject:rowIdentifier])
    {
        [textField setInputView:[self pickerInputView]];
        
        [self loadPickerViewDataForRowIdentifier:rowIdentifier];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self tableView] respondsToSelector:@selector(TPKeyboardAvoiding_scrollToActiveTextField)])
    {
        [(TPKeyboardAvoidingTableView *)[self tableView] TPKeyboardAvoiding_scrollToActiveTextField];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    if ([rowIdentifier isEqualToString:INSSearchRowIdentifierUserName])
    {
        NSString *newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        
        if ([newString length] >= INSUserNameMaxLength)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User name too long", self, __PRETTY_FUNCTION__);
            return NO;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [textField resignFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    NSString *text = [textField text];
    
    if ([rowIdentifier isEqualToString:INSSearchRowIdentifierUserName])
    {
        [self setUserName:text];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSPickerInputViewDelegate Methods

- (void)pickerInputView:(INSPickerInputView *)pickerInputView
           didSelectRow:(NSInteger)row
            inComponent:(NSInteger)component
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *textField = [self activeTextField];
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    if ([rowIdentifier isEqualToString:INSSearchRowIdentifierAgeRange])
    {
        if (component == INSAgePickerComponentsTo)
        {
            return;
        }
        
        NSString *text = [[self ageRangePickerContents] objectAtIndex:row];
        
        NSInteger age = [text integerValue];
        
        if (component == INSAgePickerComponentsMinimumAge)
        {
            [self setMinimumAge:age];
        }
        else if (component == INSAgePickerComponentsMaximumAge)
        {
            [self setMaximumAge:age];
        }
        
        [[self activeTextField] setText:[self formattedAgeRangeString]];
    }
    else
    {
        NSString *text = [[self activePickerList] objectAtIndex:row];
        if ([text isEqualToString:INSPickerInputViewBlankValue])
        {
            text = nil;
        }
        
        if ([rowIdentifier isEqualToString:INSSearchRowIdentifierChildren])
        {
            [self setChildren:text];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierBodyType])
        {
            [self setBodyType:text];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierLookingFor])
        {
            [self setLookingFor:text];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierVideo])
        {
            [self setVideo:[text integerValue]];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierMiles])
        {
            [self setMiles:text];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierLastOnline])
        {
            [self setLastOnline:text];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierEthnicity])
        {
            [self setEthnicity:text];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierSmoker])
        {
            [self setSmoker:text];
        }
        else if ([rowIdentifier isEqualToString:INSSearchRowIdentifierSexualPreference])
        {
            [self setSexualPreference:text];
        }
        
        [rowInfo setDetailText:text];
        [textField setText:text];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pickerInputViewDoneButtonPressed:(INSPickerInputView *)pickerInputView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self activeTextField] resignFirstResponder];
    
    [self setActiveTextField:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pickerInputViewPreviousButtonPressed:(INSPickerInputView *)pickerInputView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableContentsManager *manager = [self mainContentsManager];
    
    UITextField *currentTextField = [self activeTextField];
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:currentTextField];
    NSInteger currentIndex = [indexPath row];
    
    NSInteger nextIndex = currentIndex - 1;
    
    if (nextIndex < 0)
    {
        nextIndex = [manager numberOfRowsInSection:0] - 1;
    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:nextIndex inSection:0];
    BTITableRowInfo *nextRowInfo = [manager rowInfoAtIndexPath:nextIndexPath];
    NSString *nextIdentifier = [nextRowInfo identifier];
    
    [self loadPickerViewDataForRowIdentifier:nextIdentifier];
    
    INSLabelAndTextFieldTableCell *cell = (INSLabelAndTextFieldTableCell *)[[self tableView] cellForRowAtIndexPath:nextIndexPath];
    
    if ([cell isKindOfClass:[INSLabelAndTextFieldTableCell class]])
    {
        [[cell textField] becomeFirstResponder];
    }
    else
    {
        [[self activeTextField] resignFirstResponder];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pickerInputViewNextButtonPressed:(INSPickerInputView *)pickerInputView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableContentsManager *manager = [self mainContentsManager];
    
    UITextField *currentTextField = [self activeTextField];
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:currentTextField];
    NSInteger currentIndex = [indexPath row];
    
    NSInteger nextIndex = currentIndex + 1;
    
    if (nextIndex >= [manager numberOfRowsInSection:0])
    {
        nextIndex = 0;
    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:nextIndex inSection:0];
    BTITableRowInfo *nextRowInfo = [manager rowInfoAtIndexPath:nextIndexPath];
    NSString *nextIdentifier = [nextRowInfo identifier];
    
    [self loadPickerViewDataForRowIdentifier:nextIdentifier];
    
    INSLabelAndTextFieldTableCell *cell = (INSLabelAndTextFieldTableCell *)[[self tableView] cellForRowAtIndexPath:nextIndexPath];
    
    if ([cell isKindOfClass:[INSLabelAndTextFieldTableCell class]])
    {
        [[cell textField] becomeFirstResponder];
    }
    else
    {
        [[self activeTextField] resignFirstResponder];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSGenderTableCellDelegate Methods

- (void)genderTableCell:(INSGenderTableCell *)cell
        didSelectGender:(NSString *)gender
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setGender:gender];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
