//
//  INSPushNotificationSettingsViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/20/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIManagerTableViewController.h"

// Public Constants

// Protocols


@interface INSPushNotificationSettingsViewController : BTIManagerTableViewController

// Public Properties

// Public Methods

@end
