package com.hyper.instamour;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.quickblox.core.QBCallback;
import com.quickblox.core.result.Result;
import com.quickblox.module.users.QBUsers;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

public class Setting extends Activity {
	Builder alertDialog = null;
	private RelativeLayout layout;
	Global global;
	String storePass, uid;
	private ImageView privacy, account, support;
	ImageView enable_phone_calling_heart1, enable_phone_calling_heart,
			enable_video_calling_heart, enable_video_calling_heart1,
			disable_account_heart, disable_account_heart1;

	private boolean phone_click = false;
	private boolean video_click = false;
	private boolean disable_account_click = false;
	private ArrayList<NameValuePair> param;
	private Function c;
	private MenuSlideView scrollView;
	private int layoutToSlide;
	private View anapp;
	private SlideMenu sm;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			new VideoSetting(Setting.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		LayoutInflater inflater = LayoutInflater.from(this);
		scrollView = (MenuSlideView) inflater.inflate(
				R.layout.screen_scroll_with_list_menu, null);
		setContentView(scrollView);
		layoutToSlide = R.layout.setting;
		sm = new SlideMenu(getApplicationContext(), scrollView, inflater,
				layoutToSlide);
		alertDialog = new AlertDialog.Builder(Setting.this);
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		global.editor = global.preferences.edit();
		layout = (RelativeLayout) findViewById(R.id.my_layout);
		uid = global.preferences.getString("uid", "");

		c = new Function();
		privacy = (ImageView) findViewById(R.id.privacy);
		account = (ImageView) findViewById(R.id.account);
		support = (ImageView) findViewById(R.id.support);

		View view = getLayoutInflater().inflate(R.layout.account, null);
		layout.addView(view);
		try {
			new VideoSetting(Setting.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.account:
			layout.removeAllViews();
			privacy.setImageResource(R.drawable.privacy);
			account.setImageResource(R.drawable.account_sel);
			support.setImageResource(R.drawable.support);
			View view1 = getLayoutInflater().inflate(R.layout.account, null);
			layout.addView(view1);
			break;
		case R.id.privacy:

			layout.removeAllViews();
			privacy.setImageResource(R.drawable.privacy_sel);
			account.setImageResource(R.drawable.account);
			support.setImageResource(R.drawable.support);

			View view2 = getLayoutInflater().inflate(R.layout.privacy, null);
			enable_phone_calling_heart = (ImageView) view2
					.findViewById(R.id.enable_phone_calling_heart);
			enable_phone_calling_heart1 = (ImageView) view2
					.findViewById(R.id.enable_phone_calling_heart1);

			enable_video_calling_heart = (ImageView) view2
					.findViewById(R.id.enable_video_calling_heart);
			enable_video_calling_heart1 = (ImageView) view2
					.findViewById(R.id.enable_video_calling_heart1);

			disable_account_heart = (ImageView) view2
					.findViewById(R.id.disable_account_heart);
			disable_account_heart1 = (ImageView) view2
					.findViewById(R.id.disable_account_heart1);

			if (global.is_phone_checked == false) {
				enable_phone_calling_heart.setVisibility(View.VISIBLE);
				enable_phone_calling_heart1.setVisibility(View.GONE);
				phone_click = false;
			} else {
				enable_phone_calling_heart.setVisibility(View.GONE);
				enable_phone_calling_heart1.setVisibility(View.VISIBLE);
				phone_click = true;
			}

			if (global.is_video_checked == false) {
				enable_video_calling_heart.setVisibility(View.VISIBLE);
				enable_video_calling_heart1.setVisibility(View.GONE);
				video_click = false;

			} else {
				enable_video_calling_heart.setVisibility(View.GONE);
				enable_video_calling_heart1.setVisibility(View.VISIBLE);
				video_click = true;
			}

			if (global.is_account_checked == false) {
				disable_account_heart.setVisibility(View.VISIBLE);
				disable_account_heart1.setVisibility(View.GONE);
				disable_account_click = false;

			} else {
				disable_account_heart.setVisibility(View.GONE);
				disable_account_heart1.setVisibility(View.VISIBLE);
				disable_account_click = true;
			}

			layout.addView(view2);

			break;
		case R.id.support:
			layout.removeAllViews();
			support.setImageResource(R.drawable.support_sel);
			privacy.setImageResource(R.drawable.privacy);
			account.setImageResource(R.drawable.account);

			View view3 = getLayoutInflater().inflate(R.layout.support, null);

			layout.addView(view3);

			break;
		/*
		 * case R.id.menu: finish(); break;
		 */

		case R.id.edit_profile:
			Intent intent = new Intent(Setting.this, EditProfile.class);
			startActivity(intent);
			break;

		case R.id.email_address:
			UpdateEmailDialog();
			break;
		case R.id.change_password:
			ChangePasswordlDialog();
			break;

		case R.id.update_location:
			try {
				if (c.haveNetworkConnection(Setting.this)) {
					new UpdateLocation().execute();
				} else {
					AlertDialog.Builder dialog2 = new AlertDialog.Builder(
							Setting.this);
					dialog2.setTitle("Instamour");
					dialog2.setMessage("No Data Connection Avaible");
					dialog2.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							});
					AlertDialog alert2 = dialog2.create();
					alert2.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			break;
			
		case R.id.push_notification_textView:
			startActivity(new Intent(Setting.this, PushNotificationActivity.class));
			
			break;
		case R.id.txt_enable_phone_calling:

			if (phone_click == false) {
				global.setIs_phone_checked(true);
				global.editor.putBoolean("is_phone_checked", true);
				global.editor.commit();
				enable_phone_calling_heart.setVisibility(View.GONE);
				enable_phone_calling_heart1.setVisibility(View.VISIBLE);
				phone_click = true;
			} else {
				global.setIs_phone_checked(false);
				global.editor.putBoolean("is_phone_checked", false);
				global.editor.commit();
				enable_phone_calling_heart.setVisibility(View.VISIBLE);
				enable_phone_calling_heart1.setVisibility(View.GONE);
				phone_click = false;
			}

			break;
		case R.id.txt_enable_video_calling:

			if (video_click == false) {
				global.setIs_video_checked(true);
				global.editor.putBoolean("is_video_checked", true);
				global.editor.commit();
				enable_video_calling_heart.setVisibility(View.GONE);
				enable_video_calling_heart1.setVisibility(View.VISIBLE);
				video_click = true;
			} else {
				global.setIs_video_checked(false);
				global.editor.putBoolean("is_video_checked", false);
				global.editor.commit();
				enable_video_calling_heart.setVisibility(View.VISIBLE);
				enable_video_calling_heart1.setVisibility(View.GONE);
				video_click = false;
			}

			break;
		case R.id.txt_disable_account:

			if (disable_account_click == false) {
				global.setIs_account_checked(true);
				global.editor.putBoolean("is_account_checked", true);
				global.editor.commit();
				disable_account_heart.setVisibility(View.GONE);
				disable_account_heart1.setVisibility(View.VISIBLE);
				disable_account_click = true;
			} else {
				global.setIs_account_checked(false);
				global.editor.putBoolean("is_account_checked", false);
				global.editor.commit();
				disable_account_heart.setVisibility(View.VISIBLE);
				disable_account_heart1.setVisibility(View.GONE);
				disable_account_click = false;
			}

			break;

		case R.id.rate_app:

			break;

		case R.id.send_feedback:
			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			String[] recipients = new String[] { "dating@instamour.com", "", };

			emailIntent
					.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);

			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Instamour");

			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
					"Feedback from a member");

			emailIntent.setType("text/plain");

			startActivity(Intent.createChooser(emailIntent,
					"Send Feedback Via..."));

			break;

		case R.id.delete_account:
			deleteaccount();
			break;

		case R.id.about_instamour:
			Intent intent2 = new Intent(Setting.this, AboutInstamour.class);
			startActivity(intent2);
			finish();
			break;

		case R.id.privacy_policy:
			Intent intent3 = new Intent(Setting.this, PrivacyPolicy.class);
			startActivity(intent3);
			finish();
			break;

		case R.id.term_service:
			Intent intent4 = new Intent(Setting.this, TermsConditions.class);
			startActivity(intent4);
			finish();
			break;
		}

	}

	@SuppressLint("NewApi")
	public void emailu(String email) {
		if (android.os.Build.VERSION.SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}
		Log.e("mail", email);
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "user-change-email"));
		param.add(new BasicNameValuePair("sessionId", uid));
		param.add(new BasicNameValuePair("email", email));
		if (c.haveNetworkConnection(getApplicationContext())) {
			JSONObject json = c.makeHttpRequest(c.link, "POST", param);
			Log.e("Response email", "----" + json);

		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(Setting.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

	}

	@SuppressLint("NewApi")
	public void changepassword(String pass)

	{

		if (android.os.Build.VERSION.SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "user-change-pwd"));
		param.add(new BasicNameValuePair("sessionId", uid));
		param.add(new BasicNameValuePair("pwd", pass));

		if (c.haveNetworkConnection(getApplicationContext())) {

			JSONObject json = c.makeHttpRequest(c.link, "POST", param);
			Log.e("Response password", "----" + json);
		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(Setting.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

		global.editor.putString("password", pass);
		global.editor.commit();
		QBUser user = new QBUser();
		String loginjabberid = global.getJabberid();
		user.setId(Integer.parseInt(global.getJabberid()));
		Log.e("own pass", global.getPassword());
		user.setOldPassword(global.getPassword());
		user.setPassword(pass);
		global.setPassword(pass);

		QBUsers.updateUser(user, new QBCallback() {

			@Override
			public void onComplete(Result arg0, Object arg1) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onComplete(Result arg0) {
				// TODO Auto-generated method stub
				if (arg0.isSuccess()) {
					Log.e("result success", "Success");
				} else {
					Log.e("result fail", "Fail");

				}
			}
		});
	}

	@SuppressLint("NewApi")
	public void deleteaccount() {

		if (android.os.Build.VERSION.SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "user-delete"));
		param.add(new BasicNameValuePair("sessionId", uid));
		if (c.haveNetworkConnection(getApplicationContext())) {
			JSONObject json = c.makeHttpRequest(c.link, "POST", param);
			Log.e("Response delete account", "----" + json);
			String state = null;
			try {
				state = json.getString("state");
				if (state.equals("success")) {

					Intent intent = new Intent(Setting.this, HomeScreen.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(Setting.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

	}

	@SuppressWarnings("null")
	public void UpdateEmailDialog() {
		alertDialog.setTitle("Update Email");
		alertDialog.setCancelable(false);
		alertDialog.setMessage("Enter Email-id");
		final EditText input = new EditText(getApplicationContext());
		input.setTextColor(Color.BLACK);
		input.setHint("Enter email id");
		alertDialog.setView(input);
		final ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String srt = input.getEditableText().toString();
						runOnUiThread(new Runnable() {
							public void run() {
								if (!TextUtils.isEmpty(input.getText()
										.toString())) {
									emailu(input.getText().toString());
								} else {

									input.setError("Enter email");
									input.setFocusable(true);
									Toast.makeText(getApplicationContext(),
											"Email Not updated",
											Toast.LENGTH_SHORT).show();
								}

							}
						});

					}
				});

		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();

					}
				});
		alertDialog.show();

	}

	@SuppressWarnings("null")
	public void ChangePasswordlDialog() {
		alertDialog.setTitle("Change Password");
		alertDialog.setCancelable(false);
		alertDialog.setMessage("Enter your new password");
		final EditText input = new EditText(getApplicationContext());
		input.setTextColor(Color.BLACK);
		input.setHint("Enter new password");
		alertDialog.setView(input);

		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						final String srt = input.getEditableText().toString();
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method
								// stub
								if (!TextUtils.isEmpty(input.getText()
										.toString())) {

									changepassword(srt);

								} else {

									input.setError("Enter email");
									input.setFocusable(true);
									Toast.makeText(getApplicationContext(),
											"Email Not updated",
											Toast.LENGTH_SHORT).show();
								}
							}
						});
					}
				});
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();

					}
				});
		alertDialog.show();

	}

	public class UpdateLocation extends AsyncTask<Void, Void, Void> {
		private ProgressDialog pd;
		GPSTracker gps;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(Setting.this);
			pd.setMessage("Updating location");
			pd.setCancelable(false);

			gps = new GPSTracker(Setting.this);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			if (gps.canGetLocation()) {
				double latitude = 0;
				double longitude = 0;
				latitude = gps.getLatitude();
				longitude = gps.getLongitude();

				global.editor.putString("lat", String.valueOf(latitude));
				global.editor.putString("lng", String.valueOf(longitude));

				global.editor.commit();
				// \n is for new line
				try {
					Geocoder gcd = new Geocoder(getApplicationContext(),
							Locale.getDefault());
					List<Address> addresses = gcd.getFromLocation(latitude,
							longitude, 1);
					if (addresses.size() > 0)
						System.out.println(addresses.get(0).getLocality());

					String city;
					city = addresses.get(0).getLocality() + ","
							+ addresses.get(0).getCountryCode();
					param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair("m", "user-edit"));
					param.add(new BasicNameValuePair("sessionId", uid));
					param.add(new BasicNameValuePair("city", city));
					if (c.haveNetworkConnection(getApplicationContext())) {
						JSONObject json = c.makeHttpRequest(c.link, "POST",
								param);
						Log.e("response city", json + "");
					} else {
						AlertDialog.Builder dialog2 = new AlertDialog.Builder(
								Setting.this);
						dialog2.setTitle("Instamour");
						dialog2.setMessage("No Data Connection Avaible");
						dialog2.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

									}
								});
						AlertDialog alert2 = dialog2.create();
						alert2.show();
					}

				} catch (Exception e) {
					e.printStackTrace();

				}

			} else {

				gps.showSettingsAlert();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
		}
	}
	//the directions for FlurryAnalytics call for this
			//(FLJ, 5/6/14)
			 // The Activity's onStart method
			@Override 
			protected void onStart() { // Set up the Flurry session
				super.onStart();
				FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
				
			}
			@Override
			protected void onStop() {
				super.onStop();
				FlurryAgent.onEndSession(this);
			}
}
