//
//  INSSocialMediaViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/5/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSSocialMediaViewController.h"

// Models and other global

// Sub-controllers

// Views

// Private Constants

@interface INSSocialMediaViewController ()

// Private Propertes

@end

@implementation INSSocialMediaViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activityIndicator setHidesWhenStopped:YES];
    [self setActivityIndicatorView:activityIndicator];
    
    UIBarButtonItem *activityButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    
    [[self navigationItem] setRightBarButtonItem:activityButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
    
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    
    // User Interface
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSUser *user = [self user];
    
    NSString *urlString = @"";
    
    switch ([self platform])
    {
        case INSSocialMediaPlatformInstagram:
        {
            [self setTitle:@"Instagram"];
            
            urlString = @"http://www.instagram.com/";
            
            if ([[user instagramName] isNotEmpty])
            {
                urlString = [urlString stringByAppendingString:[user instagramName]];
            }
        }
            break;
        case INSSocialMediaPlatformVine:
        {
            [self setTitle:@"Vine"];
            
            urlString = @"https://vine.co/";
            
            if ([[user vineName] isNotEmpty])
            {
                urlString = [urlString stringByAppendingString:[user vineName]];
            }
        }
            break;
        default:
        {
            [self setTitle:@"Instamour"];
            
            urlString = @"http://www.instamour.com/";
        }
            break;
    }

    NSURL *url = [NSURL URLWithString:urlString];
    
    [self loadURL:url];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers


#pragma mark - UI Response Methods


#pragma mark - Misc Methods


#pragma mark - UIWebViewDelegate Methods

@end
