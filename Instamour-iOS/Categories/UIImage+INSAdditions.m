//
//  UIImage+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/15/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "UIImage+INSAdditions.h"

@implementation UIImage (INSAdditions)

+ (UIImage *)maleButtonImageCheckedINS:(BOOL)isChecked
{
    if (isChecked)
    {
        return [UIImage imageNamed:@"male-checked"];
    }
    
    return [UIImage imageNamed:@"male-unchecked"];
}

+ (UIImage *)femaleButtonImageCheckedINS:(BOOL)isChecked
{
    if (isChecked)
    {
        return [UIImage imageNamed:@"female-checked"];
    }
    
    return [UIImage imageNamed:@"female-unchecked"];
}

+ (UIImage *)placeholderAvatarImageINS
{
    return [UIImage imageNamed:@"placeHolder"];
}

+ (UIImage *)playButtonImageINS
{
    return [UIImage imageNamed:@"play_button"];
}

+ (UIImage *)pauseButtonImageINS
{
    return [UIImage imageNamed:@"pause_button"];
}

+ (UIImage *)navigationBarLogoINS
{
    return [UIImage imageNamed:@"instamour_logo_clear"];
}

- (UIImage *)croppedProfileImageINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *unrotatedImage = [self imageByRemovingOrientationINS];
    
    UIImage *squareImage = [unrotatedImage squareCroppedImageINS];
    
    UIImage *reducedImage = [squareImage imageByScalingAspectFitToSizeINS:CGSizeMake(640.0, 640.0)];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return reducedImage;
}

- (UIImage *)chatThumbnailImageINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    UIImage *unrotatedImage = [self imageByRemovingOrientationINS];
    
    UIImage *squareImage = [unrotatedImage squareCroppedImageINS];
    
    UIImage *reducedImage = [squareImage imageByScalingAspectFitToSizeINS:CGSizeMake(180.0, 180.0)];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return reducedImage;
}

- (UIImage *)squareCroppedImageINS
{
    CGRect croppedFrame = CGRectZero;

    CGSize originalImageSize = [self size];
    CGFloat originalImageWidth = originalImageSize.width;
    CGFloat originalImageHeight = originalImageSize.height;

    if (originalImageWidth > originalImageHeight)		// Landscape
	{
		CGFloat trimBorderWidth = (originalImageWidth - originalImageHeight) / 2.0;
		
		croppedFrame = CGRectMake(trimBorderWidth, 0.0, originalImageHeight, originalImageHeight);
	}
	else if (originalImageWidth < originalImageHeight)	// Portrait
	{
		CGFloat trimBorderWidth = (originalImageHeight - originalImageWidth) / 2.0;
		
		croppedFrame = CGRectMake(0.0, trimBorderWidth, originalImageWidth, originalImageWidth);
	}
	else												// Square
	{
		croppedFrame = CGRectMake(0.0, 0.0, originalImageWidth, originalImageWidth);
	}

    return [self croppedImage:croppedFrame];
}

- (UIImage *)imageByRemovingOrientationINS
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
	UIImage *imageToReturn = nil;
    
	CGSize originalImageSize = [self size];
	CGFloat originalImageWidth = originalImageSize.width;
	CGFloat originalImageHeight = originalImageSize.height;
    
	CGRect newRect = CGRectMake(0.0, 0.0, originalImageWidth, originalImageHeight);
    
	UIGraphicsBeginImageContext(newRect.size);
    
    [self drawInRect:newRect];
    
    imageToReturn = UIGraphicsGetImageFromCurrentImageContext();
    
	UIGraphicsEndImageContext();
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
	return imageToReturn;
}

- (UIImage *)imageByScalingAspectFitToSizeINS:(CGSize)size
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
	UIImage *imageToReturn = nil;
    
	CGSize originalImageSize = [self size];
	CGFloat originalImageWidth = originalImageSize.width;
	CGFloat originalImageHeight = originalImageSize.height;
    
	CGFloat maximumWidth = size.width;
    CGFloat maximumHeight = size.height;
    
	if ( (originalImageWidth <= maximumWidth) && (originalImageHeight <= maximumHeight) )
	{
		BTITrackingLog(@"<<< Leaving %s >>> EARLY - Image is smaller than target", __PRETTY_FUNCTION__);
		return self;
	}
    
	// Redraw the image to remove the direction information
    
	UIImage *image = [self imageByRemovingOrientationINS];
    
	// Resize the image
    
    CGSize scaledSize = [self aspectFitSizeThatFitsSizeINS:size];
    
	CGRect rect = CGRectMake(0.0, 0.0, rintf(scaledSize.width), rintf(scaledSize.height));
    
	UIGraphicsBeginImageContext(rect.size);
	[image drawInRect:rect];
	imageToReturn = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
	return imageToReturn;
}

- (CGSize)aspectFitSizeThatFitsSizeINS:(CGSize)size
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    CGSize originalImageSize = [self size];
	CGFloat originalImageWidth = originalImageSize.width;
	CGFloat originalImageHeight = originalImageSize.height;
    
	CGFloat maximumWidth = size.width;
    CGFloat maximumHeight = size.height;
    
	if ( (originalImageWidth <= maximumWidth) && (originalImageHeight <= maximumHeight) )
	{
		BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Image is smaller than target", self, __PRETTY_FUNCTION__);
		return size;
	}
    
    CGFloat widthRatio = maximumWidth / originalImageWidth;
    CGFloat heightRatio = maximumHeight / originalImageHeight;
    
    CGFloat scaleRatio = MIN(heightRatio, widthRatio);
    
    CGFloat scaledWidth = scaleRatio * originalImageWidth;
    CGFloat scaledHeight = scaleRatio * originalImageHeight;
    
	CGSize sizeToReturn = CGSizeMake(scaledWidth, scaledHeight);
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return sizeToReturn;
}

- (BOOL)isSquareINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    CGFloat width = [self size].width;
    CGFloat height = [self size].height;
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return (width == height);
}

@end
