//
//  INSShareViewController.h
//  Instamour
//
//  Created by Brian Slick on 8/5/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIManagerTableViewController.h"

// Public Constants

// Protocols

@interface INSShareViewController : BTIManagerTableViewController

// Public Properties

// Public Methods

@end
