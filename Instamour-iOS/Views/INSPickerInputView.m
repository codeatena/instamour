//
//  INSPickerInputView.m
//  Instamour
//
//  Created by Brian Slick on 7/14/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSPickerInputView.h"

// Private Constants
NSString *const INSPickerInputViewBlankValue = @"-- No Selection --";


@interface INSPickerInputView () <UIPickerViewDataSource, UIPickerViewDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;

@property (nonatomic, strong) IBOutlet UIToolbar *toolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *previousButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *nextButton;

@property (nonatomic, strong) NSMutableDictionary *pickerContents;

@end

@implementation INSPickerInputView

#pragma mark - Custom Getters and Setters

- (NSMutableDictionary *)pickerContents
{
    if (_pickerContents == nil)
    {
        _pickerContents = [[NSMutableDictionary alloc] init];
    }
    return _pickerContents;
}

#pragma mark - Initialization

+ (instancetype)pickerInputViewMode:(INSPickerInputViewMode)mode
                           delegate:(id<INSPickerInputViewDelegate>)delegate
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
    NSArray *nibContents = [nib instantiateWithOwner:self options:nil];
    
    INSPickerInputView *view = nil;
    for (id nibItem in nibContents)
    {
        if ([nibItem isKindOfClass:[self class]])
        {
            view = (INSPickerInputView *)nibItem;
            break;
        }
    }
    
    [view layoutSubviewsForMode:mode];
    [view setDelegate:delegate];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return view;
}

#pragma mark - UI Response Methods

- (IBAction)previousButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(pickerInputViewPreviousButtonPressed:)])
    {
        [[self delegate] pickerInputViewPreviousButtonPressed:self];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)nextButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(pickerInputViewNextButtonPressed:)])
    {
        [[self delegate] pickerInputViewNextButtonPressed:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)doneButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(pickerInputViewDoneButtonPressed:)])
    {
        [[self delegate] pickerInputViewDoneButtonPressed:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)layoutSubviewsForMode:(INSPickerInputViewMode)mode
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    switch (mode)
    {
        case INSPickerInputViewModeDoneButtonOnly:
        {
            NSMutableArray *buttonItems = [[[self toolbar] items] mutableCopy];
            [buttonItems removeObject:[self previousButton]];
            [buttonItems removeObject:[self nextButton]];
            
            [[self toolbar] setItems:buttonItems];
        }
            break;
        case INSPickerInputViewModeNoButtons:
        {
            [[self toolbar] removeFromSuperview];
            
            [self setFrame:[[self pickerView] bounds]];
        }
        case INSPickerInputViewModeAllButtons:
        default:
            break;
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setPreviousButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self previousButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setNextButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self nextButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setPickerViewContents:(NSArray *)contents
                 forComponent:(NSInteger)component
              preselectingRow:(NSInteger)row
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (contents == nil)
    {
        return;
    }
    
    // Make sure there are enough contents in the dictionary
    
    for (NSInteger index = 0; index < component; index++)
    {
        NSNumber *key = @(index);
        
        NSArray *componentContents = [[self pickerContents] objectForKey:key];
        if (componentContents == nil)
        {
            [[self pickerContents] setObject:@[ ] forKey:key];
        }
    }
    
    // Add new stuff
    
    UIPickerView *pickerView = [self pickerView];
    
    [[self pickerContents] setObject:contents forKey:@(component)];
    
    [pickerView reloadAllComponents];
    
    if (row != NSNotFound)
    {
        [pickerView selectRow:row inComponent:component animated:NO];
    }
    else
    {
        [pickerView selectRow:0 inComponent:0 animated:NO];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)reloadPickerView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self pickerView] reloadAllComponents];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)removeContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self pickerContents] removeAllObjects];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UIPickerViewDataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return [[self pickerContents] count];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    NSArray *componentArray = [[self pickerContents] objectForKey:@(component)];
    
    return [componentArray count];
}

#pragma mark - UIPickerViewDelegate Methods

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    NSArray *componentArray = [[self pickerContents] objectForKey:@(component)];

    return [componentArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isPreventingBlankRowSelection])
    {
        NSArray *componentArray = [[self pickerContents] objectForKey:@(component)];
        NSString *rowContents = [componentArray objectAtIndex:row];
        
        if ([rowContents isEqualToString:INSPickerInputViewBlankValue])
        {
            if (row < [componentArray count] - 1)
            {
                row++;
                
                [pickerView selectRow:row inComponent:component animated:YES];
            }
        }
    }
    
    if ([[self delegate] respondsToSelector:@selector(pickerInputView:didSelectRow:inComponent:)])
    {
        [[self delegate] pickerInputView:self didSelectRow:row inComponent:component];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
