//
//  NSMutableArray+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/19/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (INSAdditions)

- (void)safelyAddPopulatedStringINS:(NSString *)string;

@end
