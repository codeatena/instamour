package com.hyper.instamour;

import com.flurry.android.FlurryAgent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

public class HomeScreen extends Activity {
	private ImageView sliding;
	Thread timer;
	int imagearay[] = { R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d };
	float width;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		sliding = (ImageView) findViewById(R.id.slider);
		final Handler handler = new Handler();
		Runnable runnable = new Runnable() {
			int i = 0;

			@Override
			public void run() {
				// TODO Auto-generated method stub
				sliding.setImageResource(imagearay[i]);
				i++;
				if (i > imagearay.length - 1) {
					i = 0;
				}
				handler.postDelayed(this, 3000); // for interval...

			}
		};
		handler.postDelayed(runnable, 1000); // for initial delay..
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.home_signup:
			Intent i = new Intent(this, Signup.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(i);
			

			break;
		case R.id.home_login:
			Intent i1 = new Intent(this, Login.class);
			i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(i1);
			

			break;

		case R.id.preview:
			Intent intent = new Intent(HomeScreen.this, SampleActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			finish();
			break;

		}
	}
	
	//the directions for FlurryAnalytics call for this
	//(FLJ, 5/6/14)
	 // The Activity's onStart method
	@Override 
	protected void onStart() { // Set up the Flurry session
		super.onStart();
		FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
		
	}
	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
}
