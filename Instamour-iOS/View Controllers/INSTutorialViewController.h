//
//  INSTutorialViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/15/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIViewController.h"

// Public Constants

// Protocols
@protocol INSTutorialViewControllerDelegate;

@interface INSTutorialViewController : BTIViewController

// Public Properties
@property (nonatomic, weak) id<INSTutorialViewControllerDelegate> delegate;

// Public Methods
- (void)setCloseButtonVisible:(BOOL)isVisible;      // Default is YES.

- (void)setTutorialPageInfos:(NSArray *)contents;

- (void)beginTutorial;

@end

@protocol INSTutorialViewControllerDelegate <NSObject>

@required
- (void)tutorialViewControllerDidFinish:(INSTutorialViewController *)tutorialViewController;

@end
