//
//  INSActivityItemProvider.m
//  Instamour
//
//  Created by Brian Slick on 8/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSActivityItemProvider.h"

@implementation INSActivityItemProvider

- (id)activityViewController:(UIActivityViewController *)activityViewController
         itemForActivityType:(NSString *)activityType
{
    NSString *companyText = @"Instamour.com";
    
    if ([activityType isEqualToString:UIActivityTypePostToTwitter])
    {
        companyText = @"@Instamour";
    }
    
    NSString *message = [self checkOutThisThingText];
    if (![message isNotEmpty])
    {
        message = @"Check out this link from";
    }
    
    NSString *link = [self urlString];
    if (![link isNotEmpty])
    {
        link = @"http://apple.instamour.com";
    }
    
    NSString *text = [NSString stringWithFormat:@"%@ %@!", message, companyText];
    
    if ([activityType isEqualToString:UIActivityTypeMail])
    {
        // http://stackoverflow.com/questions/19460460/send-a-link-in-an-email-using-uiactivityviewcontroller
        // <html><body><b>This is a bold string</b><br\\>Check out this amazing site: <a href='http://apple.com'>Apple</a></body></html>

        text = [NSString stringWithFormat:@"<html><body><p>%@</p><br\\><a href='%@'>Instamour.com</a></body></html>", text, link];
    }
    else
    {
        text = [text stringByAppendingFormat:@" - %@", link];
    }
    
    return text;
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return @"";
}

@end
