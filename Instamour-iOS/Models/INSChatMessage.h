//
//  INSChatMessage.h
//  Instamour
//
//  Created by Brian Slick on 8/7/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Classes and Forward Declarations
#import "BTIObject.h"
@class QBChatMessage;
@class INSChatConversation;

// Public Constants

typedef NS_ENUM(NSInteger, INSChatMessageType) {
    INSChatMessageTypeText,
    INSChatMessageTypeImage,
    INSChatMessageTypeVideo,
};

// Protocols

@interface INSChatMessage : BTIObject <NSCoding>

// Public Properties
@property (nonatomic, weak) INSChatConversation *chatConversation;

@property (nonatomic, copy) NSString *amourUserID;
@property (nonatomic, copy) NSDate *dateSent;

@property (nonatomic, assign, getter = isToCurrentUser) BOOL toCurrentUser;

@property (nonatomic, assign) INSChatMessageType messageType;
@property (nonatomic, copy) NSString *messageBody;
@property (nonatomic, copy) NSString *imageFileName;
@property (nonatomic, copy) NSString *videoFileName;
@property (nonatomic, copy) NSURL *videoURL;

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *thumbnailImage;

@property (nonatomic, assign, getter = isUnread) BOOL unread;

@property (nonatomic, assign, getter = isUploading) BOOL uploading;
@property (nonatomic, assign, getter = isDownloading) BOOL downloading;
@property (nonatomic, assign, getter = isGeneratingThumbnail) BOOL generatingThumbnail;

// Not intended to be stored permanently. Just for retrieval in the event of send failure.
@property (nonatomic, strong) QBChatMessage *quickBloxChatMessage;

// Public Methods
- (BOOL)isBusy;
- (NSString *)thumbnailFileName;

- (void)saveImage;
- (void)saveThumbnailImage;

@end
