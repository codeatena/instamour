//
//  INSCommentsViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/30/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSCommentsViewController.h"

// Models and other global

// Sub-controllers

// Views
#import "INSCommentTableCell.h"

// Private Constants

@interface INSCommentsViewController () <UITextViewDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIView *inputContainerView;
@property (nonatomic, strong) IBOutlet UITextView *inputTextView;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *distanceFromBottomConstraint;

@property (nonatomic, strong) INSCommentTableCell *measurementCell;

@property (nonatomic, weak) NSTimer *refreshTimer;

@property (nonatomic, strong) INSComment *commentPendingAction;

@property (nonatomic, strong) NSMutableDictionary *commentsToDelete;

@end

@implementation INSCommentsViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters

- (NSMutableDictionary *)commentsToDelete
{
    if (_commentsToDelete == nil)
    {
        _commentsToDelete = [[NSMutableDictionary alloc] init];
    }
    return _commentsToDelete;
}

#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Comments"];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    
    
    // User Interface
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidAppear:animated];
    
    [self startTimer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillDisappear:animated];
    
    [self stopTimer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self mainContents] removeAllObjects];
    
    [[self mainContents] addObjectsFromArray:[[self user] comments]];
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:UIKeyboardWillShowNotification
                                   selector:@selector(keyboardWillShowNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:UIKeyboardWillHideNotification
                                   selector:@selector(keyboardWillHideNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerAddCommentDidFinishNotification
                                   selector:@selector(addCommentDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerAddCommentFailedNotification
                                   selector:@selector(addCommentFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerEditCommentDidFinishNotification
                                   selector:@selector(editCommentDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerEditCommentFailedNotification
                                   selector:@selector(editCommentFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerDeleteCommentDidFinishNotification
                                   selector:@selector(deleteCommentDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerDeleteCommentFailedNotification
                                   selector:@selector(deleteCommentFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSUserManagerDidChangeCurrentUserDetailsNotification
                                   selector:@selector(currentUserDidChangeNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerGetUserDidFinishNotification
                                   selector:@selector(getUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerGetUserFailedNotification
                                   selector:@selector(getUserFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerBlockUserDidFinishNotification
                                   selector:@selector(blockUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerBlockUserFailedNotification
                                   selector:@selector(blockUserFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [INSCommentTableCell registerNibForTableViewBTI:tableView];
    
    INSCommentTableCell *cell = [INSCommentTableCell dequeueCellFromTableViewBTI:tableView];
    [self setMeasurementCell:cell];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)keyboardWillShowNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
	CGRect keyboardFrameEnd = [(NSValue *)[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	CGRect adjustedKeyboardFrameEnd = [[self view] convertRect:keyboardFrameEnd fromView:nil];
	CGFloat animationDuration = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat animationCurve = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue];
    
    [[self view] layoutIfNeeded];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         
                         [[self distanceFromBottomConstraint] setConstant:CGRectGetHeight(adjustedKeyboardFrameEnd)];
                         [[self view] layoutIfNeeded];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         NSInteger numberOfRows = [[self mainContents] count];
                         if (numberOfRows > 0)
                         {
                             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:numberOfRows - 1 inSection:0];
                             [[self tableView] scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                         }
                         
                     }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)keyboardWillHideNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
	CGFloat animationDuration = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat animationCurve = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue];
    
    [[self view] layoutIfNeeded];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         
                         [[self distanceFromBottomConstraint] setConstant:0];
                         [[self view] layoutIfNeeded];
                         
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)addCommentDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] hideProgressIndicator];
        
        NSString *errorMessage = [jsonDictionary errorMessageINS];
        
        if ([errorMessage isEqualToString:@"Upgrade Required"])
        {
            [self promptUserForUnlimitedMode];
            
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Unlimited Mode Error", self, __PRETTY_FUNCTION__);
            return;
        }
        
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Add Comment Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Add Comment Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self inputTextView] setText:nil];
    
    [self refreshUser];
    
    [[INSPushNotificationManager sharedManager] sendNewCommentMessageToUser:[self user]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)addCommentFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Add Comment Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)editCommentDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] hideProgressIndicator];
        
        NSString *errorMessage = [jsonDictionary errorMessageINS];
        
        if ([errorMessage isEqualToString:@"Upgrade Required"])
        {
            [self promptUserForUnlimitedMode];
            
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Unlimited Mode Error", self, __PRETTY_FUNCTION__);
            return;
        }
        
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Edit Comment Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Edit Comment Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self commentPendingAction] setBody:[[self inputTextView] text]];
    [self setCommentPendingAction:nil];
    [[self inputTextView] setText:nil];
    
    [[self tableView] reloadData];
    
    [self refreshUser];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)editCommentFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Edit Comment Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteCommentDidSucceedNotification:(NSNotification *)notification
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSString *commentIdentifier = [userInfo objectForKey:INSAPIKeyIdentifier];
    
    if ([commentIdentifier isNotEmpty])
    {
        [[self commentsToDelete] removeObjectForKey:commentIdentifier];
    }
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteCommentFailedNotification:(NSNotification *)notification
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSDictionary *userInfo = [notification userInfo];
    
    NSString *commentIdentifier = [userInfo objectForKey:INSAPIKeyIdentifier];
    
    // TODO: Try to delete it again?
    
    if ([commentIdentifier isNotEmpty])
    {
        [[self commentsToDelete] removeObjectForKey:commentIdentifier];
    }
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)currentUserDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)getUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Get User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ([[INSUserManager sharedManager] isCurrentUser:[self user]])
    {
        // Let the INSUserManager handle it
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Current user", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSDictionary *jsonUser = [jsonDictionary currentUserINS];
    
    [[self user] loadWithJSONUser:jsonUser];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)getUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)blockUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"User Block Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Block User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSComment *comment = [self commentPendingAction];
    
    [[INSUserManager sharedManager] removeUserWithIDFromAllLists:[comment senderID]];
    
    [self setCommentPendingAction:nil];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"User Blocked"
                                                    message:@"Block added successfully!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)blockUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Block User Failed!"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (IBAction)sendButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextView *textView = [self inputTextView];
    UITableView *tableView = [self tableView];
    
    [textView resignFirstResponder];
    
    NSString *text = [textView text];
    
    NSInteger index = [[self mainContents] indexOfObject:[self commentPendingAction]];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    if (![text isNotEmpty])
    {
        [self setCommentPendingAction:nil];
        
        if (index == NSNotFound)
        {
            [tableView reloadData];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No text entered", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ([self commentPendingAction] != nil)
    {
        // Edit Comment
        
        INSComment *comment = [self commentPendingAction];
        NSString *oldBody = [comment body];
        NSString *newBody = [textView text];
        
        if ([newBody isEqualToString:oldBody])
        {
            [textView setText:nil];
            [self setCommentPendingAction:nil];
            
            if (index == NSNotFound)
            {
                [tableView reloadData];
            }
            else
            {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
            
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Comment did not change", self, __PRETTY_FUNCTION__);
            return;
        }
        
        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please Wait..."];
        
        [[INSServerAPIManager sharedManager] editCommentWithID:[comment identifier]
                                                     toNewBody:newBody];
    }
    else
    {
        // Add comment
        
        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please Wait..."];
        
        INSUser *user = [self user];
        
        [[INSServerAPIManager sharedManager] addComment:text
                                               toUserID:[user userID]];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)startTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self stopTimer];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(timerFireMethod:)
                                                    userInfo:nil
                                                     repeats:YES];
    [self setRefreshTimer:timer];
    
    [timer fire];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self refreshTimer] invalidate];
    
    [self setRefreshTimer:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)timerFireMethod:(NSTimer *)timer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForSelectedRow];
    
    [[self tableView] reloadData];
    
    if (indexPath != nil)
    {
        [[self tableView] selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)refreshUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    INSUser *user = [self user];
    
    if ([userManager isCurrentUser:user])
    {
        [userManager refreshCurrentUserDataRightNow:YES];
    }
    else
    {
        [[INSServerAPIManager sharedManager] getUserWithUserName:nil
                                                        orUserID:[user userID]];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)promptUserForUnlimitedMode
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [Flurry logEvent:@"Comment In App Purchase Prompt"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Coming Soon!"
                                                    message:@"Purchase the ability to send unlimited comments!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)promptUserForMyOwnCommentAction
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // You can't block yourself. All other actions are available.
    
    __weak INSCommentsViewController *weakSelf = self;
    INSComment *comment = [weakSelf commentPendingAction];
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    [cancelButton setAction:^{
        
        [weakSelf setCommentPendingAction:nil];
        
        [[weakSelf tableView] deselectRowAtIndexPath:[[weakSelf tableView] indexPathForSelectedRow] animated:YES];
        
    }];
    
    RIButtonItem *deleteButton = [RIButtonItem item];
    [deleteButton setLabel:@"Delete"];
    [deleteButton setAction:^{

        [weakSelf deleteComment:comment];
        
        [weakSelf setCommentPendingAction:nil];

//        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please Wait..."];
//        
//        [[INSServerAPIManager sharedManager] deleteCommentWithID:[comment identifier]];
        
    }];
    
    RIButtonItem *editButton = [RIButtonItem item];
    [editButton setLabel:@"Edit"];
    [editButton setAction:^{
        
        [[weakSelf inputTextView] setText:[comment body]];
        [[weakSelf inputTextView] becomeFirstResponder];
        
    }];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                               cancelButtonItem:cancelButton
                                          destructiveButtonItem:deleteButton
                                               otherButtonItems:editButton, nil];
    [sheet showInView:[self view]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)promptUserForOtherPersonCommentAction
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // You can delete any comment if this is your own page. Otherwise all you can do is block.
    
    __weak INSCommentsViewController *weakSelf = self;
    INSComment *comment = [weakSelf commentPendingAction];
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    [cancelButton setAction:^{
        
        [weakSelf setCommentPendingAction:nil];
        
        [[weakSelf tableView] deselectRowAtIndexPath:[[weakSelf tableView] indexPathForSelectedRow] animated:YES];
        
    }];
    
    RIButtonItem *blockButton = [RIButtonItem item];
    [blockButton setLabel:@"Block User"];
    [blockButton setAction:^{
        
        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please Wait..."];
        
        [[INSServerAPIManager sharedManager] blockUserForUserID:[comment senderID]];
        
    }];
    
    RIButtonItem *deleteButton = nil;
    
    if ([[INSUserManager sharedManager] isCurrentUser:[self user]])
    {
        // Current user page. Delete comment is allowed.
        
        deleteButton = [RIButtonItem item];
        [deleteButton setLabel:@"Delete"];
        [deleteButton setAction:^{
            
            [weakSelf deleteComment:comment];
            
            [weakSelf setCommentPendingAction:nil];
            
//            [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please Wait..."];
//            
//            [[INSServerAPIManager sharedManager] deleteCommentWithID:[comment identifier]];
            
        }];
    }
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                               cancelButtonItem:cancelButton
                                          destructiveButtonItem:blockButton
                                               otherButtonItems:deleteButton, nil];
    
    [sheet showInView:[self view]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteComment:(INSComment *)comment
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSInteger row = [[self mainContents] indexOfObject:comment];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    
    [[self mainContents] removeObject:comment];
    [[[self user] comments] removeObject:comment];
    
    [[self tableView] deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    NSString *commentIdentifier = [comment identifier];
    
    [[self commentsToDelete] setObject:comment forKey:commentIdentifier];
    
    [[INSServerAPIManager sharedManager] deleteCommentWithID:commentIdentifier];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContents] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSCommentTableCell *cell = [INSCommentTableCell dequeueCellFromTableViewBTI:tableView];
    
    INSComment *comment = [[self mainContents] objectAtIndex:[indexPath row]];
    
    [[cell userNameLabel] setText:[comment senderUserName]];
    [[cell dateLabel] setText:[NSDate shortTimeAgoSinceDate:[comment dateAdded]]];
    [[cell messageLabel] setText:[comment body]];
    
    __weak UIImageView *avatarImageView = [cell avatarImageView];
    
    UIImage *avatarImage = [comment avatarImage];
    if (avatarImage == nil)
    {
        [comment setAvatarDownloadCompletionBlock:^(UIImage *image){
            
            [avatarImageView setImage:image];
            
        }];
        
        avatarImage = [UIImage placeholderAvatarImageINS];
    }
    else
    {
        [comment setAvatarDownloadCompletionBlock:nil];
    }
    
    [avatarImageView setImage:avatarImage];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSComment *comment = [[self mainContents] objectAtIndex:[indexPath row]];
    
    INSCommentTableCell *cell = [self measurementCell];
    
    [[cell userNameLabel] setText:@"Anything"];
    [[cell dateLabel] setText:@"Anything"];
    [[cell messageLabel] setText:[comment body]];
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    height += 1;
    
    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSComment *comment = [[self mainContents] objectAtIndex:[indexPath row]];
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    [self setCommentPendingAction:comment];
    
    BOOL isMyComment = [[comment senderID] isEqualToString:[currentUser userID]];
    
    if (isMyComment)
    {
        [self promptUserForMyOwnCommentAction];
    }
    else
    {
        [self promptUserForOtherPersonCommentAction];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextViewDelegate Methods

- (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    else
    {
        NSString *newString = [[textView text] stringByReplacingCharactersInRange:range withString:text];
        
        if ([newString length] > 140)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Too long", self, __PRETTY_FUNCTION__);
            return NO;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}


@end
