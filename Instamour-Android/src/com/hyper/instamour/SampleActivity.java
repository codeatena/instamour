
package com.hyper.instamour;

import java.text.Bidi;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.flurry.android.FlurryAgent;
import com.hyper.instamour.messages.C2DMReceiver;
import com.hyper.instamour.messages.c2dm.C2DMessaging;
import com.hyper.instamour.model.ImageLoader;
import com.hyper.instamour.model.Model;
import com.quickblox.core.QBCallback;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.result.Result;
import com.quickblox.module.auth.QBAuth;
import com.quickblox.module.messages.QBMessages;
import com.quickblox.module.messages.model.QBEnvironment;
import com.quickblox.module.messages.result.QBSubscribeToPushNotificationsResult;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

public class SampleActivity extends Activity {

    static ListView listView;
    private String result;
    private ImageLoader imageLoader;
    private List<NameValuePair> param;
    String a;
    private QBUser userLogin;
    private ArrayList<Model> records;
    private int currentPage = 0;
    protected boolean pauseOnScroll = false;
    protected boolean pauseOnFling = true;

    private ProgressDialog pd;
    private ImageView img_slider;
    private int page;

    Function c;

    LazyAdapter adapter;
    Global global;
    LazyAdapter1 adaptervideo;

    private ImageView btnuserview;
    private ImageView btnuservideo;

    GridView gridView;

    private MenuSlideView scrollView;
    private int layoutToSlide;
    private View anapp;
    private SlideMenu sm;
    private QBUser me;
    String uid;

    private String flagResult;
    boolean b;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        Log.e("oncreate called ", "oncreate called");
        LayoutInflater inflater = LayoutInflater.from(this);
        scrollView = (MenuSlideView) inflater.inflate(R.layout.screen_scroll_with_list_menu, null);
        setContentView(scrollView);
        layoutToSlide = R.layout.sample;
        sm = new SlideMenu(this, scrollView, inflater, layoutToSlide);
        anapp = sm.menu;
        img_slider = (ImageView) anapp.findViewById(R.id.img_slider);
        page = 1;
        c = new Function();
        b = c.haveNetworkConnection(SampleActivity.this);
        if (b) {

            // setContentView(R.layout.sample);

            global = Global.getInstance();
            global.preferences = getSharedPreferences("LoginPrefrence",
                    MODE_PRIVATE);
            global.editor = global.preferences.edit();
            uid = global.preferences.getString("uid", "");
            listView = (ListView) findViewById(R.id.userList);
            gridView = (GridView) findViewById(R.id.gridView1);
            me = new QBUser();

            btnuserview = (ImageView) findViewById(R.id.sample_btnuserview);
            imageLoader = new ImageLoader(getApplicationContext());
            records = new ArrayList<Model>();

            btnuservideo = (ImageView) findViewById(R.id.sample_btnuservideo);
            pd = new ProgressDialog(SampleActivity.this);

            Log.e("my pass ", "----->>" + global.getPassword());
            try {
                Log.e("list size is ", "----->>" + global.getList().size());
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            Log.e("value of search is ", "---" + "");

            try {
                global.setList(new ArrayList<Model>());
                if (global.getList().size() > 0) {
                    records.clear();
                    records = new ArrayList<Model>();

                    records = global.getList();
                    Log.e("my searach list size", "--"
                            + global.getList().size());
                    adapter = new LazyAdapter(SampleActivity.this, records);
                    gridView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {/*
                         * AlertDialog.Builder builder = new
                         * AlertDialog.Builder( SampleActivity.this);
                         * builder.setTitle("Instamour"); builder.setMessage(
                         * "No Data found from the search,It would display previous list."
                         * ) .setCancelable(false) .setPositiveButton("OK", new
                         * DialogInterface.OnClickListener() { public void
                         * onClick( DialogInterface dialog, int id) { } });
                         * AlertDialog alert = builder.create(); alert.show();
                         */

                    // new AsyncAction().execute();
                }
            } catch (Exception e) {
                // TODO: handle exception
                // Log.e("searched data size is ",global.getList().size()+"");
                new AsyncAction().execute();
                e.printStackTrace();
            }

        } else {
            AlertDialog.Builder dialog2 = new AlertDialog.Builder(
                    SampleActivity.this);
            dialog2.setTitle("Instamour");
            dialog2.setMessage("No Data Connection Avaible");
            dialog2.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                        }
                    });
            AlertDialog alert2 = dialog2.create();
            alert2.show();
            finish();
        }
        Log.e("my pass ", "----->>" + global.getPassword());
        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
                // TODO Auto-generated method stub
                Log.e("selected", "------>" + arg2 + "");
                gridView.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);

                btnuservideo.setImageResource(R.drawable.videoview_sel);
                btnuserview.setImageResource(R.drawable.photoview);
                adaptervideo = new LazyAdapter1(SampleActivity.this, records);
                listView.setAdapter(adaptervideo);
                listView.setSelection(arg2);
                pos = arg2;

            }
        });
        gridView.setOnScrollListener(new EndlessScrollListener());

    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_slider:
                Log.e("click", "click");
                break;
            case R.id.sample_btnuservideo:
                btnuservideo.setImageResource(R.drawable.videoview_sel);
                btnuserview.setImageResource(R.drawable.photoview);
                gridView.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                try {
                    adaptervideo = new LazyAdapter1(SampleActivity.this, records);
                    listView.smoothScrollToPosition(gridView
                            .getSelectedItemPosition());
                    listView.setAdapter(adaptervideo);
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;
            case R.id.sample_btnuserview:
                gridView.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                btnuserview.setImageResource(R.drawable.photoview_sel);
                btnuservideo.setImageResource(R.drawable.videoview);
                try {
                    if (global.is_searched) {
                        adapter = new LazyAdapter(SampleActivity.this,
                                global.getList());
                        gridView.setAdapter(adapter);
                    } else {
                        adapter = new LazyAdapter(SampleActivity.this, records);
                        gridView.setAdapter(adapter);

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.search:
                Intent intent = new Intent(SampleActivity.this, Search.class);
                startActivity(intent);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
        Log.e("onrestart called ", "onrestart called");
        try {
            if (global.getList().size() > 0) {

                records = new ArrayList<Model>();
                records = global.getList();
                Log.e("my searach list size", "--" + records.size());
                adapter = new LazyAdapter(SampleActivity.this, records);
                gridView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                /*
                 * AlertDialog.Builder builder = new AlertDialog.Builder(
                 * SampleActivity.this); builder.setTitle("Instamour");
                 * builder.setMessage("No Data found") .setCancelable(false)
                 * .setPositiveButton("OK", new
                 * DialogInterface.OnClickListener() { public void
                 * onClick(DialogInterface dialog, int id) { } }); AlertDialog
                 * alert = builder.create(); alert.show();
                 */

            }

        } catch (Exception e) {
            // TODO: handle exception
            // Log.e("searched data size is ",global.getList().size()+"");
            // new AsyncAction().execute();
            e.printStackTrace();
        }
    }

    public class AsyncAction extends AsyncTask<Void, Void, Void> {
        public boolean status = false;

        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pd.setMessage("loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // TODO Auto-generated method stub
            try {

                filllistdata();
                status = true;

            } catch (Exception e) {
                // TODO: handle exception
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (b) {
                new UserGetTask().execute();
            }

        }
    }

    @SuppressLint("NewApi")
    public void filllistdata() {

        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        Log.e("uid", uid);
        String lat = global.preferences.getString("lat", "");
        String lngs = global.preferences.getString("lngd", "");

        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("m", "search"));
        param.add(new BasicNameValuePair("page", String
                .valueOf(currentPage + 1)));
        param.add(new BasicNameValuePair("sessionId", uid));
        JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
        // Log.e("response of search", json.toString());

        try {  
            JSONArray user = json.getJSONArray("users");
            Log.e("size ", "--" + user.length());
            for (int i = 0; i < user.length(); i++) {
                String description = null;
                JSONObject index = user.getJSONObject(i);
                String uid = index.getString("uid");
                Log.e("uid", uid);
                String uname = index.getString("uname");
                String date_added = index.getString("date_added");
                // Log.e("date added ", date_added);

                JSONObject setting = index.getJSONObject("settings");
                String gender = setting.getString("gender");
                String dob = setting.getString("dob");
                String ethnicity = setting.getString("ethnicity");
                String city = setting.getString("city");
                String state = setting.getString("state");
                String country = setting.getString("country");

                // Log.e("country", country);
                String photo = setting.getString("photo");
                // Log.e("photo", "-->" + photo);

                String video1 = setting.getString("video1");
                String video2 = setting.getString("video2");
                String video3 = setting.getString("video3");
                String video4 = setting.getString("video4");

                String merge = setting.getString("video_merge");
                String video_count = setting.getString("video_count");
                // Log.e("video count", video_count);

                String chat = setting.getString("chat");
                String video_chat = setting.getString("video_chat");
                String chatid = setting.getString("chatid");
                String identity = setting.getString("identity");
                String height = setting.getString("height");
                String body_type = setting.getString("body_type");
                String sexual_preference = setting
                        .getString("sexual_preference");
                String looking_for = setting.getString("looking_for");
                String smoker = setting.getString("smoker");
                String about_me = setting.getString("about_me");
                Calendar c1 = Calendar.getInstance();
                System.out.println("Current time => " + c1.getTime());
                Log.e("Current time => ", "" + c1.getTime());

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                String formattedTodayDate = df.format(c1.getTime());

                Date today = df.parse(formattedTodayDate);
                Date bdate = df.parse(dob);
                long diff = ((today.getTime() - bdate.getTime()) / (24 * 60 * 60 * 1000)) / 365;
                Log.e("age", "-->" + diff / 365);

                if (diff > 0) {
                    description = Long.toString(diff);
                } else {
                    description = "Sorry Incorect Value";
                }

                if (height.equals("null") || height.equals("Choose")
                        || height.equals("")) {
                    height = "";
                } else {
                    description += "/ " + height;
                }
                if (identity.equals("null") || identity.equals("Choose")
                        || identity.equals("")) {
                    identity = "";
                } else {
                    description += "/ " + identity;
                }

                if (body_type.equals("null") || body_type.equals("Choose")
                        || body_type.equals("")) {
                    body_type = "";
                } else {
                    description += "/ " + body_type;
                }

                if (sexual_preference.equals("null")
                        || sexual_preference.equals("Choose")
                        || sexual_preference.equals("")) {
                    sexual_preference = "";
                } else {
                    description += "/ " + sexual_preference;
                }

                if (looking_for.equals("null") || looking_for.equals("Choose")
                        || looking_for.equals("")) {
                    looking_for = "";
                }
                Log.e("description", "--" + description);
                Model model = new Model();
                String link = c.dolink.trim();
                model.setUid(uid);
                model.setUname(uname);
                model.setDateadded(date_added);
                model.setDob(dob);
                model.setGender(gender);
                model.setEthnicity(ethnicity);
                model.setCity(city);

                model.setUserphoto(link + photo);
                model.setData(description);

                if (!merge.equalsIgnoreCase("null")) {
                    model.setVideomerge(link + merge);
                } else {
                    model.setVideomerge(merge);
                }

                Log.e("video file is  : ", "------>" + link + merge);

                records.add(model);

            }
        }

        catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    "Internet connection is too slow", Toast.LENGTH_LONG)
                    .show();
        }

    }

    public class LazyAdapter extends BaseAdapter {
        private Activity activity;
        private ArrayList<Model> data;
        private LayoutInflater inflater;

        @Override
        public void notifyDataSetChanged() {
            // TODO Auto-generated method stub
            super.notifyDataSetChanged();
        }

        public LazyAdapter(Activity a, ArrayList<Model> listArr) {

            activity = a;
            data = listArr;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return data.size();
        }

        public Object getItem(int position) {
            return data.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("NewApi")
        public View getView(final int position, View convertView,
                ViewGroup parent) {
            Log.e("----", "--" + data.size());

            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.userprofilepic1, null);
            final ImageView imageView = (ImageView) vi
                    .findViewById(R.id.userleft);

            imageLoader.DisplayImage(data.get(position).getUserphoto(),
                    imageView);
            /*
             * imageView.setOnClickListener(new OnClickListener() {
             * @Override public void onClick(View v) { // TODO Auto-generated
             * method stub gridView.setVisibility(View.GONE);
             * listView.setVisibility(View.VISIBLE); adaptervideo = new
             * LazyAdapter1(SampleActivity.this, records);
             * Log.e("",""+gridView.getItemIdAtPosition(position));
             * listView.setSelection(gridView.getSelectedItemPosition());
             * listView.setAdapter(adaptervideo); } });
             */
            return vi;
        }
    }

    public static Bitmap getResizedBitmap(Bitmap image, int newHeight,
            int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    public class LazyAdapter1 extends BaseAdapter {

        private Activity activity;
        private ArrayList<Model> data;
        private LayoutInflater inflater;
        public ImageLoader imageLoader;

        final List<String> videoPathes = new ArrayList<String>();
        int videocount = -1;
        int totalvideo = -1;

        @Override
        public void notifyDataSetChanged() {
            // TODO Auto-generated method stub
            super.notifyDataSetChanged();
        }

        public LazyAdapter1(Activity a, ArrayList<Model> listArr) {

            activity = a;
            data = listArr;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            imageLoader = new ImageLoader(SampleActivity.this);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                ViewGroup parent) {
            Log.e("----", "--" + data.size());
            // TODO Auto-generated method stub

            View vi = convertView;
            if (convertView == null)

                // totalvideo=-1;
                vi = inflater.inflate(R.layout.uservideoview, null);
            TextView txtuname = (TextView) vi.findViewById(R.id.username);
            txtuname.setText(data.get(position).getUname());
            txtuname.setOnClickListener(new OnClickListener() {

                @SuppressLint("NewApi")
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    listView.smoothScrollToPosition(position);
                }
            });
            ImageView cross = (ImageView) vi.findViewById(R.id.cross);
            cross.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    records.remove(position);
                    // ary_user_right.remove(position);
                    new RemoveTask().execute();
                }
            });

            ImageView gift = (ImageView) vi.findViewById(R.id.gift);
            gift.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            SampleActivity.this);
                    builder.setTitle("Instamour");
                    builder.setMessage(
                            "Gifting will be here in our next update ,so sit tight- thanks")
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {

                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                }
            });

            final ImageView uservideopic = (ImageView) vi
                    .findViewById(R.id.uservideopic);
            imageLoader.DisplayImage(data.get(position).getUserphoto(),
                    uservideopic);

            String txtDesc = "";
            try {
                if (!data.get(position).getHeight().equalsIgnoreCase("null")
                        && data.get(position).getIdentity()
                                .equalsIgnoreCase("null")
                        && data.get(position).getBodytype()
                                .equalsIgnoreCase("null")
                        && data.get(position).getLookingfor()
                                .equalsIgnoreCase("null")) {
                    txtDesc = data.get(position).getHeight() + "/"
                            + data.get(position).getIdentity() + "/"
                            + data.get(position).getBodytype() + "/"
                            + data.get(position).getLookingfor() + "/"
                            + data.get(position).getCity();
                } else {
                    txtDesc = data.get(position).getHeight() + "/"
                            + data.get(position).getIdentity() + "/"
                            + data.get(position).getBodytype() + "/"
                            + data.get(position).getLookingfor() + "/"
                            + data.get(position).getCity();
                }
            }

            catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            TextView txtextra = (TextView) vi.findViewById(R.id.txtdesc);

            txtextra.setText(data.get(position).getData());

            TextView txtaboutme = (TextView) vi.findViewById(R.id.txtaboutme);
            txtaboutme.setText(data.get(position).getAboutme());

            final ImageView heart = (ImageView) vi.findViewById(R.id.heart);
            Log.e("uid", data.get(position).getUid());

            heart.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Log.e("uid for the heart", uid);
                    Log.e("selected user",
                            String.valueOf(data.get(position).getUid()));
                    sendreq(uid, String.valueOf(data.get(position).getUid()));
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            SampleActivity.this);
                    builder.setTitle("Love is in the air!");
                    if (data.get(position).getGender()
                            .equalsIgnoreCase("female")) {

                        builder.setMessage("You pushed her heart.")
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int id) {
                                                heart.setImageDrawable(getResources()
                                                        .getDrawable(
                                                                R.drawable.heart_sel));
                                                heart.setEnabled(false);
                                            }
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        builder.setMessage("You pushed his heart.")
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int id) {

                                                heart.setImageDrawable(getResources()
                                                        .getDrawable(
                                                                R.drawable.heart_sel));
                                                heart.setEnabled(false);
                                            }
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }

                }

            });

            ImageView img_flag = (ImageView) vi.findViewById(R.id.flag);
            img_flag.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                            SampleActivity.this);

                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                            SampleActivity.this,
                            android.R.layout.select_dialog_singlechoice);

                    arrayAdapter.add("Nudity");
                    arrayAdapter.add("Racism");
                    arrayAdapter.add("marriage");
                    arrayAdapter.add("FoulLanguage");
                    arrayAdapter.add("JustCreepy");
                    /*
                     * builderSingle.setNegativeButton("cancel", new
                     * DialogInterface.OnClickListener() {
                     * @Override public void onClick(DialogInterface dialog, int
                     * which) { dialog.dismiss(); } });
                     */

                    builderSingle.setAdapter(arrayAdapter,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    String strName = arrayAdapter
                                            .getItem(which);

                                    new AsyncActionFlag().execute(uid, data
                                            .get(position).getUid(), strName
                                            .toString());
                                    dialog.dismiss();

                                }
                            });
                    builderSingle.show();

                }
            });
            final VideoView videoview = (VideoView) vi
                    .findViewById(R.id.uservideoview_sample);
            final ImageView videoplaybtn = (ImageView) vi
                    .findViewById(R.id.video_icon);

            // Log.e("height + width", h + " + " + w);

            // videoview.setLayoutParams(new RelativeLayout.LayoutParams(w, h));

            videoplaybtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    MediaController mc;
                    mc = new MediaController(SampleActivity.this);
                    mc.setAnchorView(videoview);
                    mc.setMediaPlayer(videoview);

                    /*
                     * DisplayMetrics displaymetrics = new DisplayMetrics();
                     * getWindowManager
                     * ().getDefaultDisplay().getMetrics(displaymetrics);
                     * android.widget.RelativeLayout.LayoutParams params =
                     * (android.widget.RelativeLayout.LayoutParams)
                     * videoview.getLayoutParams(); final int h =
                     * displaymetrics.heightPixels; final int w =
                     * displaymetrics.widthPixels; params.width =
                     * displaymetrics.widthPixels; params.height =
                     * displaymetrics.heightPixels; params.leftMargin = 0;
                     * videoview.setLayoutParams(params);
                     */

                    Uri uri = Uri.parse(data.get(position).getVideomerge());
                    Log.e("Video path is : ", "---->" + uri.getPath()
                            + "----->" + data.get(position).getVideomerge());
                    // videoview.setLayoutParams(new LayoutParams(w, h));
                    videoview.setMediaController(mc);

                    if (data.get(position).getVideomerge().equals(c.dolink)) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                SampleActivity.this);
                        builder.setMessage(
                                "Sorry, this member hasn't created a video yet.")
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int id) {

                                                dialog.dismiss();
                                            }
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {

                        videoview.setVideoPath(uri.toString());
                        // videoview.setLayoutParams(new LayoutParams(w, h));
                        videoview.start();
                        /*
                         * int he = videoview.getHeight(); int we =
                         * videoview.getWidth(); Log.e("video size is  ", he +
                         * "=" + we);
                         */
                        uservideopic.setVisibility(View.GONE);
                        videoplaybtn.setVisibility(View.INVISIBLE);
                    }
                }
            });
            videoview.setOnCompletionListener(new OnCompletionListener() {

                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    videoview.setVisibility(View.GONE);
                    uservideopic.setVisibility(View.VISIBLE);
                    videoplaybtn.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.GONE);

                    listView.setVisibility(View.VISIBLE);
                    listView.setSelection(pos);

                }
            });

            return vi;
        }
    }

    public class RemoveTask extends AsyncTask<Void, Void, Void> {
        public boolean status = false;
        private ProgressDialog pd;

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    gridView.setVisibility(View.GONE);
                    adaptervideo = new LazyAdapter1(SampleActivity.this,
                            records);
                    listView.smoothScrollToPosition(gridView
                            .getSelectedItemPosition());
                    listView.setAdapter(adaptervideo);

                }
            });

            return null;
        }

        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pd = new ProgressDialog(SampleActivity.this);
            pd.setMessage("Removing...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(Void result) {

            pd.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    SampleActivity.this);
            builder.setTitle("Instamour");
            builder.setMessage("User removed successfully")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int id) {

                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();

        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.e("onresume called ", "onresume called");
        try {/*
              * if (global.getList().size() > 0) { records.clear(); records =
              * new ArrayList<Model>(); records = global.getList();
              * Log.e("my searach list size", "--" + global.getList().size());
              * adapter = new LazyAdapter(SampleActivity.this, records);
              * gridView.setAdapter(adapter); adapter.notifyDataSetChanged(); }
              * else { AlertDialog.Builder builder = new AlertDialog.Builder(
              * SampleActivity.this); builder.setTitle("Instamour");
              * builder.setMessage("No Data found") .setCancelable(false)
              * .setPositiveButton("OK", new DialogInterface.OnClickListener() {
              * public void onClick(DialogInterface dialog, int id) { } });
              * AlertDialog alert = builder.create(); alert.show(); }
              */

            try {
                if (global.getList().size() > 0) {
                    records = global.getList();
                    Log.e("my searach list size", "--"
                            + global.getList().size());
                    adapter = new LazyAdapter(SampleActivity.this, records);
                    gridView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    records.clear();
                    records = new ArrayList<Model>();
                    currentPage = 0;
                    new VideoSetting(SampleActivity.this);
                    new AsyncAction().execute();

                }
            } catch (Exception e) {
                // TODO: handle exception
                new AsyncAction().execute();
            }
        } catch (Exception e) {
            // TODO: handle exception
            // Log.e("searched data size is ",global.getList().size()+"");

        }

    }

    @SuppressLint("NewApi")
    private void sendreq(String sender, String receiver) {
        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("m", "friends-status"));
        param.add(new BasicNameValuePair("sessionId", sender));
        param.add(new BasicNameValuePair("uid", receiver));
        param.add(new BasicNameValuePair("status", "pending"));

        JSONObject json = c.makeHttpRequest(c.link, "POST", param);
        Log.e("pushed in heart", "" + json);
    }

    public class AsyncActionFlag extends AsyncTask<String, Void, String> {
        public boolean status = false;
        private ProgressDialog pd;

        protected String doInBackground(String... arg0) {
            // TODO Auto-generated method stub
            try {

                flaguser(arg0[0].toString(), arg0[1].toString(),
                        arg0[2].toString());
                status = true;
                flagResult = arg0[2].toString();

            } catch (Exception e) {
                // TODO: handle exception
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            pd.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    SampleActivity.this);
            builder.setTitle("Message!");
            builder.setMessage("Flag raised successfully")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int id) {

                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();

            Toast.makeText(getApplicationContext(), flagResult,
                    Toast.LENGTH_LONG).show();

        }

        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pd = new ProgressDialog(SampleActivity.this);
            pd.setMessage("loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @SuppressLint("NewApi")
    public void flaguser(String sender, String receiver, String flag) {

        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("m", "flag"));
        param.add(new BasicNameValuePair("sessionId", sender));
        param.add(new BasicNameValuePair("uid", receiver));
        param.add(new BasicNameValuePair("reason", flag));

        JSONObject json = c.makeHttpRequest(c.link, "POST", param);
        Log.e("Flag raised", "" + json);

    }

    @SuppressLint("NewApi")
    private void userGet() {

        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        // TODO Auto-generated method stub
        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("m", "user-get"));
        param.add(new BasicNameValuePair("sessionId", uid));
        JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
        Log.e("user get response", "--" + json.toString());

        try {
            String link = c.dolink.trim();
            JSONObject user = json.getJSONObject("user");
            String uid = user.getString("uid");
            String uname = user.getString("uname");
            Log.e("user ", "" + uname);
            String email = user.getString("email");
            String fbid = user.getString("fbid");
            String date_added = user.getString("date_added");
            String disable_account = user.getString("disable_account");
            // String qbpass = user.getString("qbpass");

            JSONObject setting = user.getJSONObject("settings");
            String pid = setting.getString("pid");
            String gender = setting.getString("gender");
            String dob = setting.getString("dob");
            String ethnicity = setting.getString("ethnicity");
            String city = setting.getString("city");
            String state = setting.getString("state");
            String country = setting.getString("country");

            String ltd = setting.getString("ltd");
            String lngd = setting.getString("lngd");
            String photo = setting.getString("photo");
            Log.e("photo ", "-------------" + photo);

            String video1 = setting.getString("video1");
            String video2 = setting.getString("video2");
            String video3 = setting.getString("video3");
            String video4 = setting.getString("video4");

            Model model = new Model();
            if (!video1.equalsIgnoreCase("null")
                    || !video1.equalsIgnoreCase("")) {
                global.editor.putString("video1", link + video1);
            } else {
                global.editor.putString("video1", video1);
            }
            if (!video2.equalsIgnoreCase("null")
                    || !video2.equalsIgnoreCase("")) {
                global.editor.putString("video2", link + video2);
            } else {
                global.editor.putString("video2", video2);
            }

            if (!video3.equalsIgnoreCase("null")
                    || !video3.equalsIgnoreCase("")) {
                global.editor.putString("video3", link + video3);
            } else {
                global.editor.putString("video3", video3);
            }
            if (!video4.equalsIgnoreCase("null")
                    || !video4.equalsIgnoreCase("")) {
                global.editor.putString("video4", link + video4);
            } else {
                global.editor.putString("video4", video4);
            }

            String video_merge = setting.getString("video_merge");
            if (!video_merge.equalsIgnoreCase("null")) {
                global.editor.putString("video_merge", link + video_merge);
            } else {
                global.editor.putString("video_merge", video_merge);
            }

            String video_count = setting.getString("video_count");

            String chat = setting.getString("chat");
            String video_chat = setting.getString("video_chat");
            String chatid = setting.getString("chatid");
            String identity = setting.getString("identity");
            String height = setting.getString("height");
            String body_type = setting.getString("body_type");
            String sexual_preference = setting.getString("sexual_preference");
            String looking_for = setting.getString("looking_for");
            String smoker = setting.getString("smoker");
            String about_me = setting.getString("about_me");
            String uresh = "748232";
            String deep = "748360";
            
            // push notification parsing
            
            JSONObject jsonPushNotification = user.getJSONObject("pushNotifications");
            
            String strPushEnabled = jsonPushNotification.getString("enabled");
            String strHearPushed = jsonPushNotification.getString("heart_pushed");
            String strWatchedVideo = jsonPushNotification.getString("watched_video");
            String strKissedMe = jsonPushNotification.getString("kissed");
            String strNewComment = jsonPushNotification.getString("new_comment");
            String strNewAmour = jsonPushNotification.getString("new_amour");
            String strInstantChat = jsonPushNotification.getString("instant_chat");
            String strVideoCall = jsonPushNotification.getString("video_call");
            String strPhoneCall = jsonPushNotification.getString("phone_call");
            String strReceivedGift = jsonPushNotification.getString("received_gift");
            
            global.editor.putString("push_enabled", strPushEnabled);
            global.editor.putString("heart_pushed", strHearPushed);
            global.editor.putString("watched_video", strWatchedVideo);
            global.editor.putString("kissed", strKissedMe);
            global.editor.putString("new_comment", strNewComment);
            global.editor.putString("new_amour", strNewAmour);
            global.editor.putString("instant_chat", strInstantChat);
            global.editor.putString("video_call", strVideoCall);
            global.editor.putString("phone_call", strPhoneCall);
            global.editor.putString("received_gift", strReceivedGift);

            Log.e("my jabberid", chatid);
            Log.e("my username", uname);
            global.editor.putString("userid", uid);
            global.editor.putString("jabberId", chatid);
            global.editor.putString("uname", uname);
            global.editor.commit();

            global.setLoginusername(uname);
            global.setJabberid(chatid);
            // global.setPassword(qbpass);

            global.setPhoto(photo);

            // QBLogin(uname);

            Log.e("---------------------------------------",
                    "-------------------------");
        }

        catch (Exception e) {
            e.printStackTrace();

        }

    }

    public class UserGetTask extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            /*
             * pd = new ProgressDialog(SampleActivity.this);
             * pd.setMessage("loading..."); pd.setIndeterminate(true);
             * pd.setCancelable(false); pd.show();
             */
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            userGet();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            listView.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
            try {
                sm.txt_uName.setText(global.getLoginusername());
                imageLoader.DisplayImage(
                        "http://s3.instamourapp.com/user_files/"
                                + global.getPhoto(), sm.img_pro_pic);
                adapter = new LazyAdapter(SampleActivity.this, records);
                gridView.setAdapter(adapter);
                Log.e("come here", "come here" + global.getLoginusername());
                QBLogin(global.getLoginusername());
                pd.dismiss();
            } catch (Exception e) {
                // TODO: handle exception
            }

        }

    }

    private void QBLogin(String username) {

        Log.e("pass", global.preferences.getString("pass", ""));
        userLogin = new QBUser(username, "instamourapp");

        /*
         * try { userLogin = new QBUser(); //
         * userLogin.setId(Integer.parseInt(global.getJabberid()));
         * Log.e("user name in QB", username + "<--");
         * userLogin.setLogin(username); userLogin.setPassword("instamourapp");
         * } catch (Exception e) { // TODO: handle exception
         * e.printStackTrace(); }
         */
        QBAuth.createSession(userLogin, new QBCallback() {

            @Override
            public void onComplete(Result arg0, Object arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onComplete(Result result) {

                if (result.isSuccess()) {
                    C2DMessaging.register(SampleActivity.this, "521258089223");
                    final String registrationId = C2DMessaging
                            .getRegistrationId(SampleActivity.this);

                    Log.e("SAMPLE id", "onRegistered() registrationId is "
                            + registrationId);

                    // TODO Auto-generated method stub
                    String deviceId = ((TelephonyManager) getBaseContext()
                            .getSystemService(Context.TELEPHONY_SERVICE))
                            .getDeviceId();
                    Log.e("before subscription1", "before1");
                    QBMessages.subscribeToPushNotificationsTask(registrationId,
                            deviceId, QBEnvironment.DEVELOPMENT,
                            new QBCallbackImpl() {
                                @Override
                                public void onComplete(Result result) {
                                    if (result.isSuccess()) {
                                        QBSubscribeToPushNotificationsResult subscribeToPushNotificationsResult = (QBSubscribeToPushNotificationsResult) result;
                                        Log.e("in subscription mode",
                                                "in subscription mode");
                                        Log.e(">>> subscription created",
                                                "----->"
                                                        + subscribeToPushNotificationsResult
                                                                .getSubscriptions()
                                                                .toString());
                                    } else {
                                        Log.e("SAMPLE",
                                                "Errro: in subscription"
                                                        + result.getErrors());
                                    }
                                }
                            });

                    VideoSetting setting = new VideoSetting(SampleActivity.this);

                }
            }
        });

    }

    public class EndlessScrollListener implements OnScrollListener {
        private int visibleThreshold = 5;

        private int previousTotal = 0;
        private boolean loading = true;
        ProgressDialog progressDialog;

        public EndlessScrollListener() {
        }

        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                int visibleItemCount, int totalItemCount) {
            // TODO Auto-generated method stub
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading
                    && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                // new.execute(currentPage + 1);
                filllistdata();
                loading = true;
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            // TODO Auto-generated method stub

        }

    }

    @Override
    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        super.finalize();
    }

    //the directions for FlurryAnalytics call for this
    //(FLJ, 5/6/14)
    // The Activity's onStart method
    @Override
    protected void onStart() { // Set up the Flurry session
        super.onStart();
        FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");

    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
