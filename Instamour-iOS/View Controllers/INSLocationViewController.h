//
//  INSLocationViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIViewController.h"

// Public Constants

// Protocols
@protocol INSLocationViewControllerDelegate;

@interface INSLocationViewController : BTIViewController

// Public Properties
@property (nonatomic, weak) id<INSLocationViewControllerDelegate> delegate;

// Public Methods
- (instancetype)initWithCity:(NSString *)city
                       state:(NSString *)state
                     country:(NSString *)country
                    delegate:(id<INSLocationViewControllerDelegate>)delegate;

@end

@protocol INSLocationViewControllerDelegate <NSObject>

@required
- (void)locationViewController:(INSLocationViewController *)viewController didUpdateCity:(NSString *)city state:(NSString *)state country:(NSString *)country;

@end