//
//  INSPushNotificationManager.m
//  Instamour
//
//  Created by Brian Slick on 8/3/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSPushNotificationManager.h"

// Models and Other Global
#import <Quickblox/Quickblox.h>

// Private Constants
NSString *const INSPushNotificationComponentMessageTypeKey = @"TYPE";
NSString *const INSPushNotificationComponentSenderIDKey = @"ID";
NSString *const INSPushNotificationComponentSenderUserNameKey = @"Name";

NSString *const INSPushNotificationTypeSomeoneKissedMe = @"Kiss";
NSString *const INSPushNotificationTypeNewAmour = @"Amours";
NSString *const INSPushNotificationTypeHeartPush = @"Heart Push";
NSString *const INSPushNotificationTypeMyVideoWatched = @"Video";
NSString *const INSPushNotificationTypeNewComment = @"Comment";
NSString *const INSPushNotificationTypeMissedCall = @"Missed Call";
NSString *const INSPushNotificationTypeUserBusy = @"User Busy";
NSString *const INSPushNotificationTypeChatMessage = @"Chat";

@interface INSPushNotificationManager () <QBActionStatusDelegate>

// Private Properties

@end

@implementation INSPushNotificationManager

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Management


#pragma mark - Initialization


#pragma mark - Notification Handlers


#pragma mark - Misc Methods

- (void)sendPushNotificationToUser:(INSUser *)toUser
              withNotificationInfo:(NSMutableDictionary *)notificationInfo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    if ([notificationInfo objectForKey:QBMPushMessageSoundKey] == nil)
    {
        [notificationInfo setObject:@"default" forKey:QBMPushMessageSoundKey];
    }
    
    if ([notificationInfo objectForKey:INSPushNotificationComponentSenderIDKey] == nil)
    {
        [notificationInfo setObject:[currentUser userID] forKey:INSPushNotificationComponentSenderIDKey];
    }
    
    if ([notificationInfo objectForKey:INSPushNotificationComponentSenderUserNameKey] == nil)
    {
        [notificationInfo setObject:[currentUser userName] forKey:INSPushNotificationComponentSenderUserNameKey];
    }
    
    NSString *quickBloxID = [toUser quickBloxID];
    
    if (![quickBloxID isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Missing quickBloxID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ([quickBloxID integerValue] <= 0)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid quickBloxID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    [payload setObject:notificationInfo forKey:QBMPushMessageApsKey];
    
    QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
    
    [QBMessages TSendPush:pushMessage
                  toUsers:quickBloxID
                 delegate:self];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendSomeoneKissedMeMessageToUser:(INSUser *)toUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (![toUser isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has push notifications disabled", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![toUser isSomeoneKissedMeNotificationEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has this notification disabled", self, __PRETTY_FUNCTION__);
        return;
    }

    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];

    NSString *notificationText = [NSString stringWithFormat:@"%@ just kissed you %@", [currentUser userName], INSKissingLipsEmoji];
    
    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeSomeoneKissedMe forKey:INSPushNotificationComponentMessageTypeKey];

    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendNewAmourMessageToUser:(INSUser *)toUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (![toUser isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has push notifications disabled", self, __PRETTY_FUNCTION__);
        return;
    }

    if (![toUser isNewAmourNotificationEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has this notification disabled", self, __PRETTY_FUNCTION__);
        return;
    }

    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];

    NSString *notificationText = [NSString stringWithFormat:@"%@ is now one of your Amours %@", [currentUser userName], INSFriendsEmoji];
    
    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeNewAmour forKey:INSPushNotificationComponentMessageTypeKey];
    
    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendHeartPushedMessageToUser:(INSUser *)toUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![toUser isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has push notifications disabled", self, __PRETTY_FUNCTION__);
        return;
    }

    if (![toUser isMyHeartPushedNotificationEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has this notification disabled", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    NSString *notificationText = [NSString stringWithFormat:@"%@ just pushed your heart %@", [currentUser userName], INSHeartEmoji];
    
    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeHeartPush forKey:INSPushNotificationComponentMessageTypeKey];
    
    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendMyVideoWatchedMessageToUser:(INSUser *)toUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![toUser isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has push notifications disabled", self, __PRETTY_FUNCTION__);
        return;
    }

    if (![toUser isMyVideoWatchedNotificationEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has this notification disabled", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    NSString *notificationText = [NSString stringWithFormat:@"%@ just watched your video %@", [currentUser userName], INSVideoCameraEmoji];
    
    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeMyVideoWatched forKey:INSPushNotificationComponentMessageTypeKey];
    
    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendNewCommentMessageToUser:(INSUser *)toUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![toUser isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has push notifications disabled", self, __PRETTY_FUNCTION__);
        return;
    }

    if (![toUser isNewCommentNotificationEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has this notification disabled", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    NSString *notificationText = [NSString stringWithFormat:@"%@ commented on your profile %@", [currentUser userName], INSPageFacingUpEmoji];
    
    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeNewComment forKey:INSPushNotificationComponentMessageTypeKey];
    
    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendMissedCallMessageToUser:(INSUser *)toUser
                        forCallType:(enum QBVideoChatConferenceType)callType
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![toUser isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has push notifications disabled", self, __PRETTY_FUNCTION__);
        return;
    }

    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    NSString *userName = [currentUser userName];
    if (![userName isNotEmpty])
    {
        userName = @"a friend";
    }
    
    NSString *notificationText = nil;
    
    switch (callType)
    {
        case QBVideoChatConferenceTypeAudioAndVideo:
            notificationText = [NSString stringWithFormat:@"Missed video call from %@", userName];
            break;
        case QBVideoChatConferenceTypeAudio:
            notificationText = [NSString stringWithFormat:@"Missed audio call from %@", userName];
            break;
        case QBVideoChatConferenceTypeUndefined:
        default:
            notificationText = [NSString stringWithFormat:@"Missed call from %@", userName];
            break;
    }
    
    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeMissedCall forKey:INSPushNotificationComponentMessageTypeKey];
    
    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendBusyCallMessageToUser:(INSUser *)toUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    NSString *currentUserName = [currentUser userName];
    if (![currentUserName isNotEmpty])
    {
        currentUserName = @"Your friend";
    }
    
    NSString *notificationText = [NSString stringWithFormat:@"%@ is busy with another call. Please try again later...", currentUserName];

    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeUserBusy forKey:INSPushNotificationComponentMessageTypeKey];
    
    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendChatMessageMessageToUser:(INSUser *)toUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![toUser isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has push notifications disabled", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![toUser isTextChatNotificationEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has this notification disabled", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    NSString *currentUserName = [currentUser userName];
    if (![currentUserName isNotEmpty])
    {
        currentUserName = @"Your friend";
    }

    NSString *notificationText = [NSString stringWithFormat:@"%@ just sent you an instant message", currentUserName];
    
    NSMutableDictionary *notificationInfo = [NSMutableDictionary dictionary];
    
    [notificationInfo setObject:notificationText forKey:QBMPushMessageAlertKey];
    [notificationInfo setObject:INSPushNotificationTypeChatMessage forKey:INSPushNotificationComponentMessageTypeKey];
    
    [self sendPushNotificationToUser:toUser
                withNotificationInfo:notificationInfo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - QBActionStatusDelegate Methods

- (void)completedWithResult:(Result *)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([result isKindOfClass:[QBMSendPushTaskResult class]])
    {
        NSLog(@"result: %@", result);
        NSLog(@"Push Notification Successful: %@", [result success] ? @"YES" : @"NO");
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Singleton

+ (instancetype)sharedManager
{
	static dispatch_once_t pred;
	static INSPushNotificationManager *shared = nil;
    
	dispatch_once(&pred, ^{
        
		shared = [[[self class] alloc] init];
        
	});
    
	return shared;
}

@end
