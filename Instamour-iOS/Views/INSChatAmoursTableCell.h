//
//  INSChatAmoursTableCell.h
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols

@interface INSChatAmoursTableCell : BTITableViewCell

// Public Properties
@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *messageLabel;
@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) IBOutlet UIImageView *thumbnailImageView;

// Public Methods
- (void)setCount:(NSInteger)count;

@end
