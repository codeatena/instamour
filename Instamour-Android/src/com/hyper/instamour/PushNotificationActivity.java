package com.hyper.instamour;

import com.appspheregroup.android.swichview.SwitchView;
import com.appspheregroup.android.swichview.SwitchView.OnSwitchChangeListener;
import com.han.network.UserAsyncTask;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PushNotificationActivity extends Activity {
	
	TextView txtBackButton;
	
	SwitchView switchPushNotification;
	
	RelativeLayout rlytMyHeart;
	RelativeLayout rlytSomeoneWatches;
	RelativeLayout rlytSomeoneKissed;
	RelativeLayout rlytNewComment;
	RelativeLayout rlytNewAmour;
	RelativeLayout rlytInstantChat;
	RelativeLayout rlytVideoCall;
	RelativeLayout rlytPhoneCall;
	RelativeLayout rlytReceiveGift;
	
	ImageView imgMyHeart;
	ImageView imgSomeoneWatches;
	ImageView imgSomeoneKissed;
	ImageView imgNewComment;
	ImageView imgNewAmour;
	ImageView imgInstantChat;
	ImageView imgVideoCall;
	ImageView imgPhoneCall;
	ImageView imgReceiveGift;
	
	boolean isPushEnabled;
	int nStatusMyHeart;
	int nStatusSomeoneWatches;
	int nStatusSomeoneKissed;
	int nStatusNewComment;
	int nStatusNewAmour;
	int nStatusInstantChat;
	int nStatusVideoCall;
	int nStatusPhoneCall;
	int nStatusReceiveGift;
	
	Global global;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_notification);
        
        initWidget();
        initValue();
        initEvent();
	}
	
	private void initWidget() {
		txtBackButton = (TextView) findViewById(R.id.back_button_textView);
		
		switchPushNotification = (SwitchView) findViewById(R.id.push_notification_switchView);
		
		rlytMyHeart = (RelativeLayout) findViewById(R.id.my_heart_relativeLayout);
		rlytSomeoneWatches = (RelativeLayout) findViewById(R.id.someone_watches_relativeLayout);
		rlytSomeoneKissed = (RelativeLayout) findViewById(R.id.someone_kissed_relativeLayout);
		rlytNewComment = (RelativeLayout) findViewById(R.id.new_comment_relativeLayout);
		rlytNewAmour = (RelativeLayout) findViewById(R.id.new_amour_relativeLayout);
		rlytInstantChat = (RelativeLayout) findViewById(R.id.instant_chat_relativeLayout);
		rlytVideoCall = (RelativeLayout) findViewById(R.id.video_call_relativeLayout);
		rlytPhoneCall = (RelativeLayout) findViewById(R.id.phone_call_relativeLayout);
		rlytReceiveGift = (RelativeLayout) findViewById(R.id.reveive_gift_relativeLayout);
		
		imgMyHeart = (ImageView) findViewById(R.id.my_heart_imageView);
		imgSomeoneWatches = (ImageView) findViewById(R.id.someone_watches_imageView);
		imgSomeoneKissed = (ImageView) findViewById(R.id.someone_kissed_imageView);
		imgNewComment = (ImageView) findViewById(R.id.new_comment_imageView);
		imgNewAmour = (ImageView) findViewById(R.id.new_amour_imageView);
		imgInstantChat = (ImageView) findViewById(R.id.instant_chat_imageView);
		imgVideoCall = (ImageView) findViewById(R.id.video_call_imageView);
		imgPhoneCall = (ImageView) findViewById(R.id.phone_call_imageView);
		imgReceiveGift = (ImageView) findViewById(R.id.receive_gift_imageView);
	}

	private void initValue() {
        drawPushNotificationFeature();
	}
	
	private void initEvent() {
		txtBackButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
		rlytMyHeart.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "heart_pushed");
			}
        });
		rlytSomeoneWatches.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "watched_video");
			}
        });
		rlytSomeoneKissed.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "kissed");
			}
        });
		rlytNewComment.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "new_comment");
			}
        });
		rlytNewAmour.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "new_amour");
			}
        });
		rlytInstantChat.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "instant_chat");
			}
        });
		rlytVideoCall.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "video_call");
			}
        });
		rlytPhoneCall.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "phone_call");
			}
        });
		rlytReceiveGift.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "received_gift");
			}
        });
		switchPushNotification.setOnSwitchChangeListener(new OnSwitchChangeListener() {
			@Override
			public void onSwitchChanged(View view, boolean isOn) {
				new UserAsyncTask(PushNotificationActivity.this).execute(UserAsyncTask.ACTION_SWITCH_PUSH, "enabled");
			}
		});
	}
	
	private void drawPushNotificationFeature() {
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence", MODE_PRIVATE);
        
		isPushEnabled = (global.preferences.getString("push_enabled", "1").equals("1")) ? true : false;
		
		nStatusMyHeart = (global.preferences.getString("heart_pushed", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusSomeoneWatches = (global.preferences.getString("watched_video", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusSomeoneKissed = (global.preferences.getString("kissed", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusNewComment = (global.preferences.getString("new_comment", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusNewAmour = (global.preferences.getString("new_amour", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusInstantChat = (global.preferences.getString("instant_chat", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusVideoCall = (global.preferences.getString("video_call", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusPhoneCall = (global.preferences.getString("phone_call", "1").equals("1")) ? View.VISIBLE : View.GONE;
		nStatusReceiveGift = (global.preferences.getString("received_gift", "1").equals("1")) ? View.VISIBLE : View.GONE;
		
		switchPushNotification.setSwitchOn(isPushEnabled);
		
		imgMyHeart.setVisibility(nStatusMyHeart);
		imgSomeoneWatches.setVisibility(nStatusSomeoneWatches);
		imgSomeoneKissed.setVisibility(nStatusSomeoneKissed);
		imgNewComment.setVisibility(nStatusNewComment);
		imgNewAmour.setVisibility(nStatusNewAmour);
		imgInstantChat.setVisibility(nStatusInstantChat);
		imgVideoCall.setVisibility(nStatusVideoCall);
		imgPhoneCall.setVisibility(nStatusPhoneCall);
		imgReceiveGift.setVisibility(nStatusPhoneCall);

		if (!isPushEnabled) {
			rlytMyHeart.setVisibility(View.GONE);
			rlytSomeoneWatches.setVisibility(View.GONE);
			rlytSomeoneKissed.setVisibility(View.GONE);
			rlytNewComment.setVisibility(View.GONE);
			rlytNewAmour.setVisibility(View.GONE);
			rlytInstantChat.setVisibility(View.GONE);
			rlytVideoCall.setVisibility(View.GONE);
			rlytPhoneCall.setVisibility(View.GONE);
			rlytReceiveGift.setVisibility(View.GONE);
		} else {
			rlytMyHeart.setVisibility(View.VISIBLE);
			rlytSomeoneWatches.setVisibility(View.VISIBLE);
			rlytSomeoneKissed.setVisibility(View.VISIBLE);
			rlytNewComment.setVisibility(View.VISIBLE);
			rlytNewAmour.setVisibility(View.VISIBLE);
			rlytInstantChat.setVisibility(View.VISIBLE);
			rlytVideoCall.setVisibility(View.VISIBLE);
			rlytPhoneCall.setVisibility(View.VISIBLE);
			rlytReceiveGift.setVisibility(View.VISIBLE);
		}
	}
	
	public void successSwitchPushNotification() {
		drawPushNotificationFeature();
	}
	
	protected void onResume() {
		super.onResume();
		Log.e("test onResume", "true");
	}
}
