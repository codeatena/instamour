package com.han.network;

import org.json.JSONObject;

import com.han.source.GlobalData;
import com.han.utility.PreferenceUtility;
import com.hyper.instamour.Global;
import com.hyper.instamour.model.CreateVideoData;

import android.content.SharedPreferences;
import android.util.Log;

public class UserParser extends Parser {
	
	public static Integer parseLogin(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			String strState = jsonObj.getString("state");
			if (!strState.equals("success")) {
				return FAIL;
			}
			
			String sessionID = jsonObj.getString("sessionId");
			SharedPreferences.Editor editor = PreferenceUtility.sharedPreferences.edit();
            editor.putString("sessionId", String.valueOf(sessionID));
            editor.commit();

            Log.e("sessionID", sessionID);
            
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public static Integer parseUserInf(String strResponse) {
		try {
			
			Global global = Global.getInstance();
			
			SharedPreferences.Editor editor = PreferenceUtility.sharedPreferences.edit();

			JSONObject jsonObj = new JSONObject(strResponse);
			
			String strState = jsonObj.getString("state");
			if (!strState.equals("success")) {
				return FAIL;
			}
			
			JSONObject jsonUser = jsonObj.getJSONObject("user");
			
            GlobalData.currentUser.userID = jsonUser.getString("uid");
            GlobalData.currentUser.userName = jsonUser.getString("uname");
            GlobalData.currentUser.email = jsonUser.getString("email");
            
            JSONObject jsonSettings = jsonUser.getJSONObject("settings");
            GlobalData.currentUser.photoFileUrl = GlobalData.URL_USER_FILE_PATH + jsonSettings.getString("photo");
            
            for (int i = 0; i < 10; i++) {
            	
            	CreateVideoData videoData = GlobalData.currentUser.arrVideoData.get(i);
            	videoData.videoUrl = jsonSettings.getString("video" + Integer.toString(i + 1));
            	videoData.thumbUrl = jsonSettings.getString("thumb" + Integer.toString(i + 1));
            	
            	if (!videoData.videoUrl.equals("null")) {
            		videoData.videoUrl = GlobalData.URL_USER_FILE_PATH + videoData.videoUrl;
            		Log.e("video" + Integer.toString(i + 1) + " url", videoData.videoUrl);
            	} else {
            		videoData.videoUrl = null;
            	}
            	if (!videoData.thumbUrl.equals("null")) {
            		videoData.thumbUrl = GlobalData.URL_THUMB_VID_FILE_PATH + videoData.thumbUrl;
            		Log.e("thumb" + Integer.toString(i + 1) + " url", videoData.thumbUrl);
            	} else {
            		videoData.thumbUrl = null;
            	}
            }
            
            JSONObject jsonPushNotification = jsonUser.getJSONObject("pushNotifications");
            
            String strPushEnabled = jsonPushNotification.getString("enabled");
            String strHearPushed = jsonPushNotification.getString("heart_pushed");
            String strWatchedVideo = jsonPushNotification.getString("watched_video");
            String strKissedMe = jsonPushNotification.getString("kissed");
            String strNewComment = jsonPushNotification.getString("new_comment");
            String strNewAmour = jsonPushNotification.getString("new_amour");
            String strInstantChat = jsonPushNotification.getString("instant_chat");
            String strVideoCall = jsonPushNotification.getString("video_call");
            String strPhoneCall = jsonPushNotification.getString("phone_call");
            String strReceivedGift = jsonPushNotification.getString("received_gift");
            
            global.editor.putString("push_enabled", strPushEnabled);
            global.editor.putString("heart_pushed", strHearPushed);
            global.editor.putString("watched_video", strWatchedVideo);
            global.editor.putString("kissed", strKissedMe);
            global.editor.putString("new_comment", strNewComment);
            global.editor.putString("new_amour", strNewAmour);
            global.editor.putString("instant_chat", strInstantChat);
            global.editor.putString("video_call", strVideoCall);
            global.editor.putString("phone_call", strPhoneCall);
            global.editor.putString("received_gift", strReceivedGift);
            
            global.editor.commit();
            
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
}
