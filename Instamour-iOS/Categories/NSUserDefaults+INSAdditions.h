//
//  NSUserDefaults+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/4/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (INSAdditions)

#pragma mark - App Settings Web Service

@property (assign, getter = appSettingsRefreshDateINS, setter = setAppSettingsRefreshDateINS:) NSDate *appSettingsRefreshDateINS;
@property (assign, getter = serverAvatarImagesFilePathINS, setter = setServerAvatarImagesFilePathINS:) NSString *serverAvatarImagesFilePathINS;
@property (assign, getter = serverVideoThumbnailImagesFilePathINS, setter = setServerVideoThumbnailFilePathINS:) NSString *serverVideoThumbnailImagesFilePathINS;
@property (assign, getter = serverVideosFilePathINS, setter = setServerVideosFilePathINS:) NSString *serverVideosFilePathINS;
@property (assign, getter = serverLowQualityVideosFilePathINS, setter = setServerLowQualityVideosFilePathINS:) NSString *serverLowQualityVideosFilePathINS;

// Each of the following is an array of strings
@property (assign, getter = allBodyTypesINS, setter = setAllBodyTypesINS:) NSArray *allBodyTypesINS;
@property (assign, getter = allEthnicitiesINS, setter = setAllEthnicitiesINS:) NSArray *allEthnicitiesINS;
@property (assign, getter = allFlagsINS, setter = setAllFlagsINS:) NSArray *allFlagsINS;
@property (assign, getter = allGendersINS, setter = setAllGendersINS:) NSArray *allGendersINS;
@property (assign, getter = allIdentitiesINS, setter = setAllIdentitiesINS:) NSArray *allIdentitiesINS;
@property (assign, getter = allLookingForsINS, setter = setAllLookingForsINS:) NSArray *allLookingForsINS;
@property (assign, getter = allSexualPreferencesINS, setter = setAllSexualPreferencesINS:) NSArray *allSexualPreferencesINS;
@property (assign, getter = allSmokersINS, setter = setAllSmokersINS:) NSArray *allSmokersINS;

#pragma mark - Search

@property (assign, getter = browserProfilesSearchGenderINS, setter = setBrowserProfilesSearchGenderINS:) NSString *browserProfilesSearchGenderINS;
@property (assign, getter = searchTermUserNameINS, setter = setSearchTermUserNameINS:) NSString *searchTermUserNameINS;
@property (assign, getter = searchTermChildrenINS, setter = setSearchTermChildrenINS:) NSString *searchTermChildrenINS;
@property (assign, getter = searchTermBodyTypeINS, setter = setSearchTermBodyTypeINS:) NSString *searchTermBodyTypeINS;
@property (assign, getter = searchTermGenderINS, setter = setSearchTermGenderINS:) NSString *searchTermGenderINS;
@property (assign, getter = searchTermMinimumAgeINS, setter = setSearchTermMinimumAgeINS:) NSInteger searchTermMinimumAgeINS;
@property (assign, getter = searchTermMaximumAgeINS, setter = setSearchTermMaximumAgeINS:) NSInteger searchTermMaximumAgeINS;
@property (assign, getter = searchTermLookingForINS, setter = setSearchTermLookingForINS:) NSString *searchTermLookingForINS;
@property (assign, getter = searchTermVideoCountINS, setter = setSearchTermVideoCountINS:) NSInteger searchTermVideoCountINS;
@property (assign, getter = searchTermMilesINS, setter = setSearchTermMilesINS:) NSString *searchTermMilesINS;
@property (assign, getter = searchTermLastOnlineINS, setter = setSearchTermLastOnlineINS:) NSString *searchTermLastOnlineINS;
@property (assign, getter = searchTermEthnicityINS, setter = setSearchTermEthnicityINS:) NSString *searchTermEthnicityINS;
@property (assign, getter = searchTermSmokerINS, setter = setSearchTermSmokerINS:) NSString *searchTermSmokerINS;
@property (assign, getter = searchTermSexualPreferenceINS, setter = setSearchTermSexualPreferenceINS:) NSString *searchTermSexualPreferenceINS;

#pragma mark - Misc

@property (assign, getter = isCorporateAlertsEnabledINS, setter = setCorporateAlertsEnabledINS:) BOOL corporateAlertsEnabledINS;
@property (assign, getter = isVideoNeededAlertEnabledINS, setter = setVideoNeededAlertEnabledINS:) BOOL videoNeededAlertEnabledINS;
@property (assign, getter = isCreateVideo1AlertEnabledINS, setter = setCreateVideo1AlertEnabledINS:) BOOL createVideo1AlertEnabledINS;
@property (assign, getter = isCreateVideo2AlertEnabledINS, setter = setCreateVideo2AlertEnabledINS:) BOOL createVideo2AlertEnabledINS;
@property (assign, getter = isCreateVideo3AlertEnabledINS, setter = setCreateVideo3AlertEnabledINS:) BOOL createVideo3AlertEnabledINS;
@property (assign, getter = isCreateVideo4AlertEnabledINS, setter = setCreateVideo4AlertEnabledINS:) BOOL createVideo4AlertEnabledINS;

#pragma mark - Misc Methods

- (void)initializeValuesForNewUserINS;
- (void)initializeValuesForLoginINS;

- (void)deleteAllValuesINS;

- (void)loadWithGetUserJSONResultINS:(NSDictionary *)jsonDictionary;

- (void)clearAllSearchCriteriaINS;

- (void)setBrowserGenderSearchCriteriaIfNecessaryINS;

@end
