package com.hyper.instamour;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.hyper.instamour.model.ImageLoader;
import com.hyper.instamour.model.Model;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.result.Result;
import com.quickblox.module.auth.QBAuth;
import com.quickblox.module.auth.result.QBSessionResult;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.service.QBVideoChatService;
import com.quickblox.module.videochat.model.listeners.OnQBVideoChatListener;
import com.quickblox.module.videochat.model.objects.CallState;
import com.quickblox.module.videochat.model.objects.CallType;
import com.quickblox.module.videochat.model.objects.VideoChatConfig;
import com.quickblox.module.videochat.model.utils.Debugger;

public class YourAmours extends Activity {
	private ArrayList<Model> records;
	private Global global;
	private Function c;
	private ImageLoader imageLoader;
	private ListView listView;
	private QBUser qbUser;
	private List<NameValuePair> param;
	ImageView amour, pushedheart, pending, amour1, pushedheart1, pending1;
	String uid, chatid;
	String loginjabberid, selecteduserjabberid;
	Adapter adapter;
	Adapter1 adapter1;
	Adapter2 adapter2;
	private ProgressDialog progressDialog;
	private String image_profile;
	RelativeLayout layout;
	private boolean isCanceledVideoCall;
	private VideoChatConfig videoChatConfig;

	private MenuSlideView scrollView;
	private int layoutToSlide;
	private String selecteduser;
	private SlideMenu sm;
	private TextView edit;
	private boolean calling = false;
	private TextView notification;
	private int total_pending;
	private boolean audio_call = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.your_amours);
		LayoutInflater inflater = LayoutInflater.from(this);
		scrollView = (MenuSlideView) inflater.inflate(
				R.layout.screen_scroll_with_list_menu, null);
		setContentView(scrollView);
		layoutToSlide = R.layout.your_amours;
		sm = new SlideMenu(getApplicationContext(), scrollView, inflater,
				layoutToSlide);
		// d_videoCalling = new Dialog(YourAmours.this);
		progressDialog = new ProgressDialog(YourAmours.this);
		progressDialog.setCancelable(true);
		progressDialog.setMessage("please wait....");
		layout = (RelativeLayout) findViewById(R.id.my_layout);
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		global.editor = global.preferences.edit();
		c = new Function();
		uid = global.preferences.getString("uid", uid);

		imageLoader = new ImageLoader(getApplicationContext());
		amour = (ImageView) findViewById(R.id.amour);
		pending = (ImageView) findViewById(R.id.img_pending);
		pushedheart = (ImageView) findViewById(R.id.heartpushed);
		notification = (TextView) findViewById(R.id.notify);

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "friends-pending-receiver"));
		param.add(new BasicNameValuePair("sessionId", uid));

		JSONObject json = c.makeHttpRequest(c.link, "POST", param);
		Log.e("Response pending", "" + json);
		try {
			JSONArray user = json.getJSONArray("users");
			if (user.length() > 0) {
				Log.e("-", "-" + user.length());	
				total_pending = user.length();
				notification.setVisibility(View.VISIBLE);
				notification.setTextColor(Color.BLACK);
				notification.setText(total_pending + "");
			} else {

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		listView = (ListView) findViewById(R.id.listview);

		edit = (TextView) findViewById(R.id.edit);
		if (c.haveNetworkConnection(getApplicationContext())) {
			new YourAmourTask().execute();
		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(
					YourAmours.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

		// initViews();

	}

	private void initViews() {

		progressDialog
				.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialogInterface) {
						if (isCanceledVideoCall) {
							QBVideoChatService.getService().stopCalling(
									videoChatConfig);
						}
					}
				});
	}

	public void onClick(View view) {
		switch (view.getId()) {

		case R.id.edit:
			if (c.haveNetworkConnection(getApplicationContext())) {
				new EditTask().execute();
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						YourAmours.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			break;
		case R.id.amour:

			edit.setVisibility(View.VISIBLE);
			amour.setImageResource(R.drawable.amors_sel);
			pending.setImageResource(R.drawable.pending);
			pushedheart.setImageResource(R.drawable.heart_pushed);
			if (c.haveNetworkConnection(getApplicationContext())) {
				new YourAmourTask().execute();
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						YourAmours.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			break;
		case R.id.img_pending:
			// listView = (ListView) findViewById(R.id.listview);
			Log.e("Hello u r in a pending", "Pending");
			edit.setVisibility(View.GONE);
			pending.setImageResource(R.drawable.pending_sel);
			amour.setImageResource(R.drawable.amors);
			pushedheart.setImageResource(R.drawable.heart_pushed);
			if (c.haveNetworkConnection(getApplicationContext())) {
				try {
					Log.e("Hello u r in a pending try", "Pending try");
					new YourPendingTask().execute();
				} catch (Exception e) { // TODO:
					e.printStackTrace();
				}
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						YourAmours.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			break;

		case R.id.heartpushed:
			edit.setVisibility(View.GONE);
			pushedheart.setImageResource(R.drawable.heart_pushed_sel);
			amour.setImageResource(R.drawable.amors);
			pending.setImageResource(R.drawable.pending);
			if (c.haveNetworkConnection(getApplicationContext())) {
				try {
					new YourHeartPushedTask().execute();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						YourAmours.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			break;

		}
	}

	private OnQBVideoChatListener qbVideoChatListener = new OnQBVideoChatListener() {

		@Override
		public void onVideoChatStateChange(final CallState state,
				final VideoChatConfig receivedVideoChatConfig) {

			// TODO Auto-generated method stub
			Log.e("Video Listener : ", "Video ListenerVideo Listener");
			try {

				videoChatConfig = receivedVideoChatConfig;
				isCanceledVideoCall = false;

				Log.e("State Name isc : ", "------>" + state.name());

				switch (state) {
				case ON_CALLING:
					Log.e("hiiii", "iiiiiiiii");

					Log.e("Calling is : -----> ", "---->" + calling);
					if (calling == false) {
						Log.e("Calling inside if is : -----> ", "---->"
								+ calling);
						calling = true;
						showCallDialog();
					}

					break;
				case ON_ACCEPT_BY_USER:
					calling = false;
					progressDialog.dismiss();
					if (audio_call == true) {
						Log.e("ON_START_CONNECTING : Audio", "Audio");
						startAudioActivity();
					} else {
						Log.e("ON_START_CONNECTING : Video", "Video");
						startVideoChatActivity();
					}

					break;
				case ON_REJECTED_BY_USER:
					calling = false;

					progressDialog.dismiss();
					AlertDialog.Builder dialog2 = new AlertDialog.Builder(
							YourAmours.this);
					dialog2.setTitle("Instamour");
					dialog2.setMessage("User rejected answered");
					dialog2.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
								}
							});
					AlertDialog alert2 = dialog2.create();
					alert2.show();

					break;
				case ON_DID_NOT_ANSWERED:
					Log.e("hiiii", "Did not answered");
					calling = false;
					progressDialog.dismiss();
					AlertDialog.Builder dialog1 = new AlertDialog.Builder(
							YourAmours.this);
					dialog1.setTitle("Instamour");
					dialog1.setMessage("Sorry, but this member didn't answer. Try again later or send an instant chat.");
					dialog1.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							});
					AlertDialog alert1 = dialog1.create();
					alert1.show();
					// showCallDialog();
					break;
				case ON_CANCELED_CALL:
					calling = false;
					isCanceledVideoCall = true;
					videoChatConfig = null;
					AlertDialog.Builder dialog = new AlertDialog.Builder(
							YourAmours.this);
					dialog.setTitle("Instamour");
					dialog.setMessage("Sorry, but this member didn't answer. Try again later or send an instant chat.");
					dialog.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
								}
							});
					AlertDialog alert = dialog.create();
					alert.show();
					dialog.setMessage("User canceled called").create().show();
					break;
				case ON_START_CONNECTING:
					calling = false;
					progressDialog.dismiss();
					if (audio_call == true) {
						Log.e("ON_START_CONNECTING : Audio", "Audio");
						startAudioActivity();
					} else {
						Log.e("ON_START_CONNECTING : Video", "Video");
						startVideoChatActivity();
					}
					break;
				}

			} catch (Exception e) {
				// TODO: handle exception

				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						YourAmours.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("Sorry!! Unable to catch Oppenent, Please Try Again.");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								// d_videoCalling.dismiss();
							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();

			}

		}
	};

	OnQBVideoChatListener qbVideoChatListenerAudio = new OnQBVideoChatListener() {

		@Override
		public void onVideoChatStateChange(CallState callState,
				VideoChatConfig arg1) {
			// TODO Auto-generated method stub

			switch (callState) {
			case ON_CALL_START:
				Toast.makeText(getBaseContext(),
						getString(R.string.call_start_txt), Toast.LENGTH_SHORT)
						.show();

				break;
			case ON_CANCELED_CALL:
				Toast.makeText(getBaseContext(),
						getString(R.string.call_canceled_txt),
						Toast.LENGTH_SHORT).show();
				finish();
				break;
			case ON_CALL_END:
				Toast.makeText(getBaseContext(), "Call end", Toast.LENGTH_SHORT)
						.show();
				break;
			}

		}
	};

	private void startVideoChatActivity() {

		Log.e("new Activity : ", "New Activity");
		try {

			Intent intent = new Intent(getBaseContext(),
					ActivityVideoChat.class);
			intent.putExtra("selectedusername", selecteduser);
			intent.putExtra(VideoChatConfig.class.getCanonicalName(),
					videoChatConfig);
			startActivity(intent);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void startAudioActivity() {

		try {
			QBVideoChatService.getService().startVideoChat(videoChatConfig);
			QBVideoChatService.getService().setQbVideoChatListener(
					qbVideoChatListenerAudio);

		} catch (Exception e) {
			// TODO: handle exception
		}
		Log.e("Audio Activity : ", "Audio Activity");

	}

	private void showCallDialog() {
		Log.e("hi", "hi" + "");

		final Dialog d_videoCalling = new Dialog(YourAmours.this);
		d_videoCalling.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d_videoCalling.setContentView(R.layout.videochat);
		d_videoCalling.setCancelable(true);
		d_videoCalling.show();

		ImageView img_ans = (ImageView) d_videoCalling
				.findViewById(R.id.imagviewshow);
		Button btn_ans = (Button) d_videoCalling.findViewById(R.id.btnAns);
		Button btn_decline = (Button) d_videoCalling
				.findViewById(R.id.btndecline);

		// if (image_profile != null) {
		// Log.e("", msg)
		if (c.haveNetworkConnection(getApplicationContext())) {
			imageLoader.DisplayImage(c.dolink + global.getFriendphoto(),
					img_ans);
		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(
					YourAmours.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();

		}

		// }

		btn_ans.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (videoChatConfig == null) {
					Toast.makeText(getApplicationContext(),
							getString(R.string.call_canceled_txt),
							Toast.LENGTH_SHORT).show();
					return;
				}

				Log.e("Call accept  : ", "Call accept ");
				QBVideoChatService.getService().acceptCall(videoChatConfig);
				calling = false;
				d_videoCalling.dismiss();

			}
		});

		btn_decline.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (videoChatConfig == null) {
					Toast.makeText(getApplicationContext(),
							getString(R.string.call_canceled_txt),
							Toast.LENGTH_SHORT).show();
					return;
				}
				Log.e("call reject", "call decline");
				QBVideoChatService.getService().rejectCall(videoChatConfig);
				calling = false;
				d_videoCalling.dismiss();
			}
		});
	}

	@Override
	public void onDestroy() {
		/*
		 * stopService(new Intent(getApplicationContext(),
		 * QBVideoChatService.class));
		 */
		super.onDestroy();
		finish();
	}

	@Override
	public void onResume() {

		Log.e("On Resume Called  : ", "On Resume");
		calling = false;

		try {
			QBVideoChatService.getService().setQbVideoChatListener(
					qbVideoChatListener);
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		super.onResume();
	}

	public class EditTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("m", "friends-get"));
			param.add(new BasicNameValuePair("sessionId", uid));
			param.add(new BasicNameValuePair("type", "accept"));
			JSONObject json = c.makeHttpRequest(c.link, "POST", param);
			Log.e("Response Amours", "----" + json);

			String link = c.dolink.trim();
			try {

				JSONArray user = json.getJSONArray("users");
				Log.e("size of json array:-", "" + user.length());
				for (int i = 0; i < user.length(); i++) {
					JSONObject user1 = user.getJSONObject(i);
					String uid = user1.getString("uid");
					String uname = user1.getString("uname");
					String email = user1.getString("email");
					String fbid = user1.getString("fbid");
					String date_added = user1.getString("date_added");
					String disable_account = user1.getString("disable_account");

					JSONObject setting = user1.getJSONObject("settings");
					String pid = setting.getString("pid");
					String gender = setting.getString("gender");
					String dob = setting.getString("dob");
					String ethnicity = setting.getString("ethnicity");
					String city = setting.getString("city");
					String state = setting.getString("state");
					String country = setting.getString("country");

					String ltd = setting.getString("ltd");
					String lngd = setting.getString("lngd");
					String photo = setting.getString("photo");

					String video1 = setting.getString("video1");
					String video2 = setting.getString("video2");
					String video3 = setting.getString("video3");
					String video4 = setting.getString("video4");

					String video_merge = setting.getString("video_merge");
					String video_count = setting.getString("video_count");

					String chat = setting.getString("chat");
					String video_chat = setting.getString("video_chat");
					String chatid = setting.getString("chatid");
					String identity = setting.getString("identity");
					String height = setting.getString("height");
					String body_type = setting.getString("body_type");
					String sexual_preference = setting
							.getString("sexual_preference");
					String looking_for = setting.getString("looking_for");
					String smoker = setting.getString("smoker");
					String about_me = setting.getString("about_me");
					Model model = new Model();
					model.setUname(uname);
					model.setJid(chatid);

					global.editor.commit();
					model.setUserphoto(photo);
					model.setUid(uid);
					records.add(model);

				}

				for (int i = 0; i < records.size(); i++) {
					for (int j = i + 1; j < records.size(); j++) {
						if (records.get(i).getUid()
								.equals(records.get(j).getUid())) {
							records.remove(j);
						}
					}

				}
				Log.e("record size for the amours", records.size() + "");

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			adapter2 = new Adapter2(YourAmours.this, records);
			listView.setAdapter(adapter2);
			adapter2.notifyDataSetChanged();
		}
	}

	public class Adapter2 extends BaseAdapter {
		private ArrayList<Model> mRecords;
		private Activity mActivity;
		private LayoutInflater inflater;

		public Adapter2(Activity activity, ArrayList<Model> records) {
			// TODO Auto-generated constructor stub
			mRecords = records;
			mActivity = activity;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			imageLoader = new ImageLoader(YourAmours.this);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mRecords.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return records.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = convertView;

			if (view == null)
				view = inflater.inflate(R.layout.delete, null);
			ImageView delete = (ImageView) view.findViewById(R.id.deleteb);
			ImageView userPhoto = (ImageView) view.findViewById(R.id.userPhoto);
			TextView name = (TextView) view.findViewById(R.id.name);

			name.setText(mRecords.get(position).getUname());
			imageLoader.DisplayImage(c.dolink
					+ mRecords.get(position).getUserphoto(), userPhoto);

			delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Log.e("selected user is", mRecords.get(position).getUid());
					if (c.haveNetworkConnection(getApplicationContext())) {
						delete(mRecords.get(position).getUid());
					} else {

					}

				}
			});

			return view;
		}
	}

	public class Adapter extends BaseAdapter {
		private ArrayList<Model> mRecords;
		private Activity mActivity;
		private LayoutInflater inflater;

		public Adapter(Activity activity, ArrayList<Model> records) {
			// TODO Auto-generated constructor stub
			mRecords = records;
			mActivity = activity;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			imageLoader = new ImageLoader(YourAmours.this);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mRecords.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return records.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = convertView;

			if (view == null)
				view = inflater.inflate(R.layout.raw_amours, null);
			final ImageView userPhoto = (ImageView) view
					.findViewById(R.id.userPhoto);
			final TextView name = (TextView) view.findViewById(R.id.name);
			final ImageView chat = (ImageView) view.findViewById(R.id.chat);
			final ImageView video = (ImageView) view.findViewById(R.id.video);
			final ImageView avtar = (ImageView) view.findViewById(R.id.avtar);

			if (global.is_phone_checked == true) {
				avtar.setVisibility(View.INVISIBLE);
			} else {
				avtar.setVisibility(View.VISIBLE);
			}

			if (global.is_video_checked == true) {
				video.setVisibility(View.INVISIBLE);
			} else {
				video.setVisibility(View.VISIBLE);
			}

			name.setText(mRecords.get(position).getUname());
			imageLoader.DisplayImage(c.dolink
					+ mRecords.get(position).getUserphoto(), userPhoto);

			chat.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					/*
					 * try { Intent cha = new Intent(YourAmours.this,
					 * ChatWindow.class); String currentuser = null, chatid =
					 * null; currentuser = mRecords.get(position).getUname();
					 * chatid = mRecords.get(position).getJid();
					 * cha.putExtra("currentuser", currentuser);
					 * cha.putExtra("jabberid", Integer.parseInt(chatid));
					 * startActivity(cha);
					 * 
					 * } catch (Exception e) { // TODO: handle exception }
					 */

					try {
						String currentuser = null, chatid = null;
						currentuser = mRecords.get(position).getUname();
						chatid = mRecords.get(position).getJid();
						global.editor.putString("friendphoto",
								global.getPhoto());
						global.editor.putString("frindjabberid", chatid);
						global.setFriendjabberid(chatid);
						global.setFriendphoto(mRecords.get(position)
								.getUserphoto());
						global.editor.commit();

						/*
						 * Model model=new Model();
						 * model.setChat_name(currentusername);
						 * model.setChat_jabberid(Friendjabberid+"");
						 * model.setChat_image(friendphoto);
						 * model.setMessage(messageString); records.add(model);
						 * global.setChatList(records);
						 */

						Intent cha = new Intent(YourAmours.this,
								ChatWindow.class);
						currentuser = mRecords.get(position).getUname();
						chatid = mRecords.get(position).getJid();
						cha.putExtra("currentuser", currentuser);
						cha.putExtra("jabberid", Integer.parseInt(chatid));
						cha.putExtra("currentuserphoto", mRecords.get(position)
								.getUserphoto());
						startActivity(cha);

					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});
			video.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (c.haveNetworkConnection(getApplicationContext())) {
						// TODO Auto-generated method stub
						try {

							Log.e("selected user jabber id ",
									mRecords.get(position).getJid() + "-");
							if (progressDialog != null
									&& !progressDialog.isShowing()) {
								progressDialog.show();
							}
							audio_call = false;
							calling = false;
							qbUser = new QBUser(Integer.parseInt(mRecords.get(
									position).getJid()));

							image_profile = mRecords.get(position)
									.getUserphoto();
							selecteduser = mRecords.get(position).getUname();
							global.setFriendphoto(mRecords.get(position)
									.getUserphoto());
							videoChatConfig = QBVideoChatService.getService()
									.callUser(qbUser, CallType.VIDEO_AUDIO,
											null);

						} catch (Exception e) {
							// TODO: handle exception
						}
					} else {
						AlertDialog.Builder dialog2 = new AlertDialog.Builder(
								YourAmours.this);
						dialog2.setTitle("Instamour");
						dialog2.setMessage("No Data Connection Avaible");
						dialog2.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

									}
								});
						AlertDialog alert2 = dialog2.create();
						alert2.show();
					}
				}
			});
			avtar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {/*
											 * if (c.haveNetworkConnection(
											 * getApplicationContext())) { try {
											 * Log.e("fjlsdj",
											 * mRecords.get(position).getJid() +
											 * "-"); if (progressDialog != null
											 * && !progressDialog.isShowing()) {
											 * progressDialog.show(); }
											 * 
											 * audio_call = true; qbUser = new
											 * QBUser
											 * (Integer.parseInt(mRecords.get(
											 * position).getJid()));
											 * 
											 * videoChatConfig =
											 * QBVideoChatService.getService()
											 * .callUser(qbUser, CallType.AUDIO,
											 * null);
											 * 
											 * } catch (Exception e) { // TODO:
											 * handle exception
											 * Log.e("Error in Audio : ",
											 * "Audio"); e.printStackTrace(); }
											 * 
											 * } else { AlertDialog.Builder
											 * dialog2 = new
											 * AlertDialog.Builder(
											 * YourAmours.this);
											 * dialog2.setTitle("Instamour");
											 * dialog2.setMessage(
											 * "No Data Connection Avaible");
											 * dialog2.setPositiveButton("Ok",
											 * new
											 * DialogInterface.OnClickListener()
											 * {
											 * 
											 * @Override public void
											 * onClick(DialogInterface dialog,
											 * int which) { // TODO
											 * Auto-generated method stub
											 * 
											 * } }); AlertDialog alert2 =
											 * dialog2.create(); alert2.show();
											 * }
											 */
				}
			});

			return view;
		}
	}

	private void createSession(String login, final String password) {

		// Create QuickBlox session with user
		//

		Log.e("LOgin Name is : ", login + "----->" + password + "-----> "
				+ global.getJabberid());

		QBAuth.createSession(login, password, new QBCallbackImpl() {
			@Override
			public void onComplete(Result result) {

				try {
					if (result.isSuccess()) {
						// save current user
						Log.e("Result is sucesss : ", "sucesssss");

						DataHolder.getInstance().setCurrentQbUser(
								((QBSessionResult) result).getSession()
										.getUserId(), password);
						QBUser currentQbUser = DataHolder.getInstance()
								.getCurrentQbUser();

						Log.e("QBUSER Name is  :",
								"------>" + currentQbUser.getId() + "------>"
										+ currentQbUser.getPassword());

						Debugger.logConnection("setQBVideoChatListener: "
								+ (currentQbUser == null));
						try {
							QBVideoChatService.getService()
									.setQBVideoChatListener(currentQbUser,
											qbVideoChatListener);
						} catch (Exception e) {
							Log.e("Error : ",
									"Error ----->" + result.getErrors());
						}

					} else {
						Log.e("Result : ", "------->" + result.getErrors());
					}
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		});

	}

	public class Adapter1 extends BaseAdapter {
		private ArrayList<Model> mRecords;
		private Activity mActivity;
		private LayoutInflater inflater;

		public Adapter1(Activity activity, ArrayList<Model> records) {
			// TODO Auto-generated constructor stub
			mRecords = records;
			mActivity = activity;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			imageLoader = new ImageLoader(YourAmours.this);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mRecords.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return records.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = convertView;
			if (view == null)
				view = inflater.inflate(R.layout.raw_pending, null);

			final ImageView userPhoto = (ImageView) view
					.findViewById(R.id.userprofilepic);
			final TextView name = (TextView) view
					.findViewById(R.id.txtusername);
			Log.e("name user", mRecords.get(position).getUname());
			Log.e("photo user", mRecords.get(position).getUserphoto());
			name.setText(mRecords.get(position).getUname());
			imageLoader.DisplayImage(c.dolink + ""
					+ mRecords.get(position).getUserphoto(), userPhoto);

			return view;
		}

	}

	@SuppressLint("NewApi")
	public void accept(String reciever) {
		// TODO Auto-generated method stub
		// int SDK_INT = ;
		if (android.os.Build.VERSION.SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "friends-status"));
		param.add(new BasicNameValuePair("sessionId", uid));
		param.add(new BasicNameValuePair("uid", reciever));
		param.add(new BasicNameValuePair("status", "accept"));

		JSONObject json = c.makeHttpRequest(c.link, "POST", param);
		Log.e("Response accept", "----" + json);

	}

	@SuppressLint("NewApi")
	public void delete(String reciever) {
		// TODO Auto-generated method stub
		// int SDK_INT = ;
		ProgressDialog pd;
		pd = new ProgressDialog(YourAmours.this);

		pd.setMessage("deleting friends");
		pd.setCancelable(false);
		pd.show();
		if (android.os.Build.VERSION.SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "friends-status"));
		param.add(new BasicNameValuePair("sessionId", uid));
		param.add(new BasicNameValuePair("uid", reciever));
		param.add(new BasicNameValuePair("status", "deleted"));

		JSONObject json = c.makeHttpRequest(c.link, "POST", param);
		Log.e("Response delete", "----" + json);
		pd.dismiss();
		new EditTask().execute();

	}

	private class YourAmourTask extends AsyncTask<Void, Void, Void> {
		private ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			records = new ArrayList<Model>();
			pd = new ProgressDialog(YourAmours.this);
			pd.setMessage("Loading...");
			pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... params) {

			// TODO Auto-generated method stub
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("m", "friends-get"));
			param.add(new BasicNameValuePair("sessionId", uid));
			param.add(new BasicNameValuePair("type", "accept"));
			JSONObject json = c.makeHttpRequest(c.link, "POST", param);
			Log.e("Response Amours", "----" + json);

			String link = c.dolink.trim();
			try {

				JSONArray user = json.getJSONArray("users");
				Log.e("size of json array:-", "" + user.length());
				for (int i = 0; i < user.length(); i++) {
					JSONObject user1 = user.getJSONObject(i);
					String uid = user1.getString("uid");
					String uname = user1.getString("uname");
					String email = user1.getString("email");
					String fbid = user1.getString("fbid");
					String date_added = user1.getString("date_added");
					String disable_account = user1.getString("disable_account");

					JSONObject setting = user1.getJSONObject("settings");
					String pid = setting.getString("pid");
					String gender = setting.getString("gender");
					String dob = setting.getString("dob");
					String ethnicity = setting.getString("ethnicity");
					String city = setting.getString("city");
					String state = setting.getString("state");
					String country = setting.getString("country");

					String ltd = setting.getString("ltd");
					String lngd = setting.getString("lngd");
					String photo = setting.getString("photo");

					String video1 = setting.getString("video1");
					String video2 = setting.getString("video2");
					String video3 = setting.getString("video3");
					String video4 = setting.getString("video4");

					String video_merge = setting.getString("video_merge");
					String video_count = setting.getString("video_count");

					String chat = setting.getString("chat");
					String video_chat = setting.getString("video_chat");
					String chatid = setting.getString("chatid");
					String identity = setting.getString("identity");
					String height = setting.getString("height");
					String body_type = setting.getString("body_type");
					String sexual_preference = setting
							.getString("sexual_preference");
					String looking_for = setting.getString("looking_for");
					String smoker = setting.getString("smoker");
					String about_me = setting.getString("about_me");
					Model model = new Model();
					model.setUname(uname);
					model.setJid(chatid);

					global.editor.commit();
					model.setUserphoto(photo);
					model.setUid(uid);
					records.add(model);

				}

				for (int i = 0; i < records.size(); i++) {
					for (int j = i + 1; j < records.size(); j++) {
						if (records.get(i).getUid()
								.equals(records.get(j).getUid())) {
							records.remove(j);
						}
					}

				}
				Log.e("record size for the amours", records.size() + "");

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			adapter = new Adapter(YourAmours.this, records);
			listView.setAdapter(adapter);
			adapter.notifyDataSetChanged();
			pd.dismiss();
			try {
				if (global.from_fb == true) {
					Log.e("global.getFbLoginUsername()",
							global.getFbLoginUsername());
					createSession(global.getFbLoginUsername(),
							global.getPassword());
				} else {
					Log.e("global.getLoginUsername()",
							global.getLoginusername());
					createSession(global.getLoginusername(), "instamourapp");
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			// initViews();
		}
	}

	private class YourPendingTask extends AsyncTask<Void, Void, Void> {
		private ProgressDialog pd;

		String description = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Log.e("Hello u r in a pending pre", "Pending pre");
			records = new ArrayList<Model>();
			pd = new ProgressDialog(YourAmours.this);
			pd.setMessage("Loading...");
			pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("m", "friends-pending-receiver"));
			param.add(new BasicNameValuePair("sessionId", uid));

			JSONObject json = c.makeHttpRequest(c.link, "POST", param);
			Log.e("Response pending", "" + json);
			try {
				JSONArray user = json.getJSONArray("users");
				for (int i = 0; i < user.length(); i++) {
					JSONObject user1 = user.getJSONObject(i);
					String uid = user1.getString("uid");
					String uname = user1.getString("uname");
					Log.e("user ", "" + uname);
					String email = user1.getString("email");
					String fbid = user1.getString("fbid");
					String date_added = user1.getString("date_added");
					String disable_account = user1.getString("disable_account");

					JSONObject setting = user1.getJSONObject("settings");
					String pid = setting.getString("pid");
					String gender = setting.getString("gender");
					String dob = setting.getString("dob");
					String ethnicity = setting.getString("ethnicity");
					String city = setting.getString("city");
					String state = setting.getString("state");
					String country = setting.getString("country");

					String ltd = setting.getString("ltd");
					String lngd = setting.getString("lngd");
					String photo = setting.getString("photo");
					Log.e("photo", photo);
					Log.e("userid", uid);

					String video1 = setting.getString("video1");
					String video2 = setting.getString("video2");
					String video3 = setting.getString("video3");
					String video4 = setting.getString("video4");

					String video_merge = setting.getString("video_merge");
					String video_count = setting.getString("video_count");

					String chat = setting.getString("chat");
					String video_chat = setting.getString("video_chat");
					String chatid = setting.getString("chatid");
					String identity = setting.getString("identity");
					String height = setting.getString("height");
					String body_type = setting.getString("body_type");
					String sexual_preference = setting
							.getString("sexual_preference");
					String looking_for = setting.getString("looking_for");
					String smoker = setting.getString("smoker");
					String about_me = setting.getString("about_me");
					Calendar c1 = Calendar.getInstance();
					System.out.println("Current time => " + c1.getTime());
					Log.e("Current time => ", "" + c1.getTime());

					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

					String formattedTodayDate = df.format(c1.getTime());

					Date today = df.parse(formattedTodayDate);
					Date bdate = df.parse(dob);
					long diff = ((today.getTime() - bdate.getTime()) / (24 * 60 * 60 * 1000)) / 365;
					Log.e("age", "-->" + diff / 365);

					if (diff > 0) {
						description = Long.toString(diff);
					} else {
						description = "Sorry Incorect Value";
					}

					if (height.equals("null") || height.equals("Choose")
							|| height.equals("")) {
						height = "";
					} else {
						description += "/ " + height;
					}
					if (identity.equals("null") || identity.equals("Choose")
							|| identity.equals("")) {
						identity = "";
					} else {
						description += "/ " + identity;
					}

					if (body_type.equals("null") || body_type.equals("Choose")
							|| body_type.equals("")) {
						body_type = "";
					} else {
						description += "/ " + body_type;
					}

					if (sexual_preference.equals("null")
							|| sexual_preference.equals("Choose")
							|| sexual_preference.equals("")) {
						sexual_preference = "";
					} else {
						description += "/ " + sexual_preference;
					}

					if (looking_for.equals("null")
							|| looking_for.equals("Choose")
							|| looking_for.equals("")) {
						looking_for = "";
					}
					Log.e("description", "--" + description);

					Model model = new Model();
					model.setUid(uid);
					model.setUname(uname);
					model.setDateadded(date_added);
					model.setDob(dob);
					model.setGender(gender);
					model.setEthnicity(ethnicity);
					model.setCity(city);
					model.setUserphoto(photo);
					model.setData(description);

					if (!video_merge.equalsIgnoreCase("null")) {
						model.setVideomerge(c.dolink + video_merge);
					} else {
						model.setVideomerge(video_merge);
					}

					Log.e("video file is  : ", "------>" + c.dolink
							+ video_merge);

					records.add(model);
					Log.e("record size for the pending", records.size() + "");

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			// TODO Auto-generated method stub
			super.onPostExecute(result);

			adapter1 = new Adapter1(YourAmours.this, records);
			listView.setAdapter(adapter1);
			adapter1.notifyDataSetChanged();
			if (pd != null && pd.isShowing()) {
				pd.dismiss();

			}

			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) { // TODO Auto-generated method
					// stub

					accept(records.get(arg2).getUid());
					Intent i = new Intent(YourAmours.this, UserProfile.class);
					i.putExtra("username", records.get(arg2).getUname());
					i.putExtra("photo", records.get(arg2).getUserphoto());
					/*
					 * i.putExtra("height", records.get(arg2).getHeight());
					 * i.putExtra("identity", records.get(arg2).getIdentity());
					 * 
					 * i.putExtra("bodytype", records.get(arg2).getBodytype());
					 * i.putExtra("lookingfor",
					 * records.get(arg2).getLookingfor()); i.putExtra("city",
					 * records.get(arg2).getCity());
					 * 
					 * i.putExtra("aboutme", records.get(arg2).getAboutme());
					 */
					i.putExtra("userdata", records.get(arg2).getData());

					i.putExtra("uid", records.get(arg2).getUid());
					i.putExtra("videomerge", records.get(arg2).getVideomerge());

					startActivity(i);

				}
			});

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		stopService(new Intent(getApplicationContext(),
				QBVideoChatService.class));
		finish();
	}

	private class YourHeartPushedTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog pd;
		String description = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			records = new ArrayList<Model>();
			pd = new ProgressDialog(YourAmours.this);
			pd.setMessage("Loading...");
			pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			param = new ArrayList<NameValuePair>();
			/*
			 * param.add(new BasicNameValuePair("m", "friends-pending-sender"));
			 * param.add(new BasicNameValuePair("sessionId", uid));
			 */
			param.add(new BasicNameValuePair("m", "friends-get"));
			param.add(new BasicNameValuePair("sessionId", uid));
			param.add(new BasicNameValuePair("type", "pending"));
			JSONObject json = c.makeHttpRequest(c.link, "POST", param);
			Log.e("Response heart", "" + json);
			try {
				JSONArray user = json.getJSONArray("users");
				for (int i = 0; i < user.length(); i++) {

					JSONObject user1 = user.getJSONObject(i);
					String uid = user1.getString("uid");
					String uname = user1.getString("uname");
					Log.e("user ", "" + uname);
					String email = user1.getString("email");
					String fbid = user1.getString("fbid");
					String date_added = user1.getString("date_added");
					String disable_account = user1.getString("disable_account");

					JSONObject setting = user1.getJSONObject("settings");
					String pid = setting.getString("pid");
					String gender = setting.getString("gender");
					String dob = setting.getString("dob");
					String ethnicity = setting.getString("ethnicity");
					String city = setting.getString("city");
					String state = setting.getString("state");
					String country = setting.getString("country");

					String ltd = setting.getString("ltd");
					String lngd = setting.getString("lngd");
					String photo = setting.getString("photo");
					Log.e("photo", photo);
					Log.e("userid", uid);

					String video1 = setting.getString("video1");
					String video2 = setting.getString("video2");
					String video3 = setting.getString("video3");
					String video4 = setting.getString("video4");

					String video_merge = setting.getString("video_merge");
					String video_count = setting.getString("video_count");

					String chat = setting.getString("chat");
					String video_chat = setting.getString("video_chat");
					String chatid = setting.getString("chatid");
					String identity = setting.getString("identity");
					String height = setting.getString("height");
					String body_type = setting.getString("body_type");
					String sexual_preference = setting
							.getString("sexual_preference");
					String looking_for = setting.getString("looking_for");
					String smoker = setting.getString("smoker");
					String about_me = setting.getString("about_me");
					Calendar c1 = Calendar.getInstance();
					System.out.println("Current time => " + c1.getTime());
					Log.e("Current time => ", "" + c1.getTime());

					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

					String formattedTodayDate = df.format(c1.getTime());

					Date today = df.parse(formattedTodayDate);
					Date bdate = df.parse(dob);
					long diff = ((today.getTime() - bdate.getTime()) / (24 * 60 * 60 * 1000)) / 365;
					Log.e("age", "-->" + diff / 365);

					if (diff > 0) {
						description = Long.toString(diff);
					} else {
						description = "Sorry Incorect Value";
					}

					if (height.equals("null") || height.equals("Choose")
							|| height.equals("")) {
						height = "";
					} else {
						description += "/ " + height;
					}
					if (identity.equals("null") || identity.equals("Choose")
							|| identity.equals("")) {
						identity = "";
					} else {
						description += "/ " + identity;
					}

					if (body_type.equals("null") || body_type.equals("Choose")
							|| body_type.equals("")) {
						body_type = "";
					} else {
						description += "/ " + body_type;
					}

					if (sexual_preference.equals("null")
							|| sexual_preference.equals("Choose")
							|| sexual_preference.equals("")) {
						sexual_preference = "";
					} else {
						description += "/ " + sexual_preference;
					}

					if (looking_for.equals("null")
							|| looking_for.equals("Choose")
							|| looking_for.equals("")) {
						looking_for = "";
					}
					Log.e("description", "--" + description);
					Model model = new Model();
					model.setUid(uid);
					model.setUname(uname);
					model.setDateadded(date_added);
					model.setDob(dob);
					model.setGender(gender);
					model.setEthnicity(ethnicity);
					model.setCity(city);
					model.setUserphoto(photo);
					model.setData(description);

					if (!video_merge.equalsIgnoreCase("null")) {
						model.setVideomerge(c.dolink + video_merge);
					} else {
						model.setVideomerge(video_merge);
					}

					records.add(model);
					Log.e("record size for the pending", records.size() + "");

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			listView.clearFocus();
			adapter1 = new Adapter1(YourAmours.this, records);
			listView.setAdapter(adapter1);
			adapter1.notifyDataSetChanged();
			if (pd != null && pd.isShowing()) {
				pd.dismiss();

			}

			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) { // TODO Auto-generated method
					// stub

					accept(records.get(arg2).getUid());
					Intent i = new Intent(YourAmours.this, UserProfile.class);
					i.putExtra("username", records.get(arg2).getUname());
					i.putExtra("photo", records.get(arg2).getUserphoto());
					/*
					 * i.putExtra("height", records.get(arg2).getHeight());
					 * i.putExtra("identity", records.get(arg2).getIdentity());
					 * 
					 * i.putExtra("bodytype", records.get(arg2).getBodytype());
					 * i.putExtra("lookingfor",
					 * records.get(arg2).getLookingfor()); i.putExtra("city",
					 * records.get(arg2).getCity());
					 * 
					 * i.putExtra("aboutme", records.get(arg2).getAboutme());
					 */
					i.putExtra("userdata", records.get(arg2).getData());
					i.putExtra("uid", records.get(arg2).getUid());
					i.putExtra("videomerge", records.get(arg2).getVideomerge());

					startActivity(i);

				}
			});

		}
	}
	//the directions for FlurryAnalytics call for this
			//(FLJ, 5/6/14)
			 // The Activity's onStart method
			@Override 
			protected void onStart() { // Set up the Flurry session
				super.onStart();
				FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
				
			}
			@Override
			protected void onStop() {
				super.onStop();
				FlurryAgent.onEndSession(this);
			}
}
