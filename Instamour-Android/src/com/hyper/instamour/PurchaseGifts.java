package com.hyper.instamour;

import java.io.ObjectOutputStream.PutField;

import com.quickblox.module.videochat.core.service.QBVideoChatService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

public class PurchaseGifts extends Activity {
	private RelativeLayout layout;
	Thread timer;
	int imagearay[] = { R.drawable.screen1, R.drawable.screen2,
			R.drawable.screen3, R.drawable.screen4, R.drawable.screen5 };
	float width;
	private MenuSlideView scrollView;
	private int layoutToSlide;
	private View anapp;
	private SlideMenu sm;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			new VideoSetting(PurchaseGifts.this);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.purchase_gifts);

		LayoutInflater inflater = LayoutInflater.from(this);
		scrollView = (MenuSlideView) inflater.inflate(
				R.layout.screen_scroll_with_list_menu, null);
		setContentView(scrollView);
		layoutToSlide = R.layout.purchase_gifts;
		sm = new SlideMenu(getApplicationContext(), scrollView, inflater,
				layoutToSlide);

		layout = (RelativeLayout) findViewById(R.id.nameLayout);
		final Handler handler = new Handler();
		Runnable runnable = new Runnable() {
			int i = 0;

			@Override
			public void run() {
				// TODO Auto-generated method stub
				layout.setBackgroundResource(imagearay[i]);
				i++;
				if (i > imagearay.length - 1) {
					i = 0;
				}
				handler.postDelayed(this, 1000); // for interval...

			}
		};
		handler.postDelayed(runnable, 500); // for initial delay..
		new VideoSetting(PurchaseGifts.this);

	}

	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.img_slider:
			finish();
			break;

		default:
			break;
		}
	}
}
