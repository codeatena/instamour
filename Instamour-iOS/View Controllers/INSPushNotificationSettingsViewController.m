//
//  INSPushNotificationSettingsViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/20/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSPushNotificationSettingsViewController.h"

// Models and other global

// Sub-controllers

// Views

// Private Constants

@interface INSPushNotificationSettingsViewController ()

// Private Properties
@property (nonatomic, copy) NSString *lastSelectedService;

@end

@implementation INSPushNotificationSettingsViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"instamour_logo_clear"]];
    [[self navigationItem] setTitleView:imageView];
    
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    
    // User Interface
    
    [self populateContents];
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillDisappear:animated];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    BTITableContentsManager *manager = [self mainContentsManager];
    [manager reset];
    
    BTITableRowInfo *rowInfo = nil;
    
    {{  // Push Notifications
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceAllNotifications];
        [rowInfo setText:@"Push Notifications"];
    }}
    
    if (![user isPushNotificationsEnabled])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Push Notifcations are not enabled", self, __PRETTY_FUNCTION__);
        return;
    }
    
    UIImage *checkMarkImage = [UIImage imageNamed:@"check-mark"];
    
    {{  // My heart gets pushed
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceMyHeartGetsPushed];
        [rowInfo setText:@"My heart gets pushed"];
        [rowInfo setImage:([user isMyHeartPushedNotificationEnabled]) ? checkMarkImage : nil];
    }}
    
    {{  // Someone watches my video
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceSomeoneWatchesMyVideo];
        [rowInfo setText:@"Someone watches my video"];
        [rowInfo setImage:([user isMyVideoWatchedNotificationEnabled]) ? checkMarkImage : nil];
    }}
    
    {{  // Someone kissed me
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceSomeoneKissedMe];
        [rowInfo setText:@"Someone kissed me"];
        [rowInfo setImage:([user isSomeoneKissedMeNotificationEnabled]) ? checkMarkImage : nil];
    }}
    
    {{  // New comment posted
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceNewCommentPosted];
        [rowInfo setText:@"New comment posted"];
        [rowInfo setImage:([user isNewCommentNotificationEnabled]) ? checkMarkImage : nil];
    }}
    
    {{  // New Amour added
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceNewAmourAdded];
        [rowInfo setText:@"New Amour added"];
        [rowInfo setImage:([user isNewAmourNotificationEnabled]) ? checkMarkImage : nil];
    }}
    
    {{  // Instant chat request
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceInstantChatRequest];
        [rowInfo setText:@"Instant chat request"];
        [rowInfo setImage:([user isTextChatNotificationEnabled]) ? checkMarkImage : nil];
    }}
    
    {{  // Video chat request
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceVideoCallRequest];
        [rowInfo setText:@"Video chat request"];
        if ([user isVideoChatEnabled])
        {
            [rowInfo setDetailText:nil];
            [rowInfo setImage:([user isVideoChatNotificationEnabled]) ? checkMarkImage : nil];
        }
        else
        {
            [rowInfo setDetailText:[NSString stringWithFormat:@" %@ Video chat is not enabled", INSWarningSignEmoji]];
            [rowInfo setImage:nil];
        }
    }}
    
    {{  // Phone call request
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceAudioCallRequest];
        [rowInfo setText:@"Phone call request"];
        if ([user isAudioChatEnabled])
        {
            [rowInfo setDetailText:nil];
            [rowInfo setImage:([user isAudioChatNotificationEnabled]) ? checkMarkImage : nil];
        }
        else
        {
            [rowInfo setDetailText:[NSString stringWithFormat:@" %@ Phone calling is not enabled", INSWarningSignEmoji]];
            [rowInfo setImage:nil];
        }
    }}
    
    {{  // I receive a gift
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSServerAPIManagerPushNotificationServiceIReceiveAGift];
        [rowInfo setText:@"I receive a gift"];
        [rowInfo setImage:([user isGiftReceivedNotificationEnabled]) ? checkMarkImage : nil];
    }}
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerTogglePushNotificationsDidFinishNotification
                                   selector:@selector(pushNotificationDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerTogglePushNotificationsFailedNotification
                                   selector:@selector(pushNotificationFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)pushNotificationDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Setting Change Failed"
                                                        message:[jsonDictionary errorMessageINS]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Setting Change Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    NSString *selectedService = [self lastSelectedService];
    
    if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceAllNotifications])
    {
        [user setPushNotificationsEnabled:![user isPushNotificationsEnabled]];
        
        BOOL isNotificationsEnabled = [user isPushNotificationsEnabled];
        
        [self toggleTableRows];
        
        [[INSQuickBloxManager sharedManager] setRegisteredForNotifications:isNotificationsEnabled];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceMyHeartGetsPushed])
    {
        [user setMyHeartPushedNotificationEnabled:![user isMyHeartPushedNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceSomeoneWatchesMyVideo])
    {
        [user setMyVideoWatchedNotificationEnabled:![user isMyVideoWatchedNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceSomeoneKissedMe])
    {
        [user setSomeoneKissedMeNotificationEnabled:![user isSomeoneKissedMeNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceNewCommentPosted])
    {
        [user setNewCommentNotificationEnabled:![user isNewCommentNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceNewAmourAdded])
    {
        [user setNewAmourNotificationEnabled:![user isNewAmourNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceInstantChatRequest])
    {
        [user setTextChatNotificationEnabled:![user isTextChatNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceVideoCallRequest])
    {
        [user setVideoChatNotificationEnabled:![user isVideoChatNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceAudioCallRequest])
    {
        [user setAudioChatNotificationEnabled:![user isAudioChatNotificationEnabled]];
    }
    else if ([selectedService isEqualToString:INSServerAPIManagerPushNotificationServiceIReceiveAGift])
    {
        [user setGiftReceivedNotificationEnabled:![user isGiftReceivedNotificationEnabled]];
    }
    
    [self populateContents];
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pushNotificationFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Setting Change Failed"
                                                    message:[(NSError *)[notification object] localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)masterSwitchValueChanged:(UISwitch *)aSwitch
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setLastSelectedService:INSServerAPIManagerPushNotificationServiceAllNotifications];
    
    [self setEnabledForLastSelectedService:[aSwitch isOn]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)toggleTableRows
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableSet *indexPathsBefore = [NSMutableSet setWithArray:[[self mainContentsManager] allIndexPaths]];
    
    [self populateContents];
    
    NSMutableSet *indexPathsAfter = [NSMutableSet setWithArray:[[self mainContentsManager] allIndexPaths]];
    
    if ( [indexPathsAfter count] > [indexPathsBefore count] )
    {
        // Insert Rows
        
        [indexPathsAfter minusSet:indexPathsBefore];
        
        [[self tableView] insertRowsAtIndexPaths:[indexPathsAfter allObjects] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else
    {
        // Remove Rows
        
        [indexPathsBefore minusSet:indexPathsAfter];
        
        [[self tableView] deleteRowsAtIndexPaths:[indexPathsBefore allObjects] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setEnabledForLastSelectedService:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:NO];
    
    //    NSLog(@"Service: %@ enabled: %@", [self lastSelectedService], (isEnabled) ? @"Yes" : @"No");
    
    [[INSServerAPIManager sharedManager] setPushNotificationEnabled:isEnabled
                                                         forService:[self lastSelectedService]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setUserInterfaceEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self tableView] setUserInteractionEnabled:isEnabled];
    [[[self navigationItem] leftBarButtonItem] setEnabled:isEnabled];
    
    if (isEnabled)
    {
        [[AppDelegate sharedDelegate] hideProgressIndicator];
    }
    else
    {
        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContentsManager] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    NSString *detailText = [rowInfo detailText];
    
    [[cell textLabel] setText:[rowInfo text]];
    [[cell detailTextLabel] setText:detailText];
    
    if ([rowIdentifier isEqualToString:INSServerAPIManagerPushNotificationServiceAllNotifications])
    {
        UISwitch *aSwitch = [[UISwitch alloc] init];
        [aSwitch setOnTintColor:[UIColor switchOnTintColorINS]];
        [aSwitch setOn:[[[INSUserManager sharedManager] currentUser] isPushNotificationsEnabled]];
        [aSwitch addTarget:self action:@selector(masterSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        [cell setAccessoryView:aSwitch];
    }
    else
    {
        UIImage *image = [rowInfo image];
        if (image == nil)
        {
            [cell setAccessoryView:nil];
        }
        else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            [cell setAccessoryView:imageView];
        }
    }
    
    // If the row has detail text, it should be disabled
    
    if ([detailText isNotEmpty])
    {
        [[cell textLabel] setTextColor:[UIColor grayColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    else
    {
        [[cell textLabel] setTextColor:[UIColor blackColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPathToReturn = indexPath;
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    
    // If the row has detail text, it should be disabled
    
    if ([[rowInfo detailText] isNotEmpty])
    {
        indexPathToReturn = nil;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return indexPathToReturn;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([rowIdentifier isEqualToString:INSServerAPIManagerPushNotificationServiceAllNotifications])
    {
        UISwitch *aSwitch = (UISwitch *)[cell accessoryView];
        
        [aSwitch setOn:![aSwitch isOn] animated:YES];
        
        [self masterSwitchValueChanged:aSwitch];
    }
    else
    {
        // TODO: Come up with better logic.
        // A row with an accessory view is currently "ON"
        
        [self setLastSelectedService:rowIdentifier];
        
        BOOL isCurrentlyEnabled = ([cell accessoryView] != nil);
        
        [self setEnabledForLastSelectedService:!isCurrentlyEnabled];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
