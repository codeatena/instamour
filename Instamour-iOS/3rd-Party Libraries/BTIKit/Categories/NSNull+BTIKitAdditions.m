//
//  NSNull+BTIKitAdditions.m
//  BTIKit
//  v1.1
//
//  Created by Brian Slick in August 2014
//  Copyright (c) 2014 BriTer Ideas LLC. All rights reserved.
//  https://github.com/BriTerIdeas/BTIKit
//

#import "NSNull+BTIKitAdditions.h"

@implementation NSNull (BTIKitAdditions)

- (BOOL)isNotEmptyBTI
{
    return NO;
}

@end
