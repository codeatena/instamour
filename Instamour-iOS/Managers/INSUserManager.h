//
//  INSCurrentUserManager.h
//  Instamour
//
//  Created by Brian Slick on 7/28/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//


// Libraries

// Forward Declarations and Classes
#import <Foundation/Foundation.h>
@class INSUser;

// Public Constants
FOUNDATION_EXPORT NSString *const INSUserManagerDidChangeCurrentUserDetailsNotification;

FOUNDATION_EXPORT NSString *const INSUserManagerDidChangeYourAmoursNotification;
FOUNDATION_EXPORT NSString *const INSUserManagerDidChangePendingAmoursNotification;
FOUNDATION_EXPORT NSString *const INSUserManagerDidChangeHeartsPushedAmoursNotification;
FOUNDATION_EXPORT NSString *const INSUserManagerDidChangeBrowserUsersNotification;
FOUNDATION_EXPORT NSString *const INSUserManagerDidChangeSearchUsersNotification;

// Protocols

@interface INSUserManager : NSObject

// Public Properties
@property (nonatomic, strong, readonly) INSUser *currentUser;

@property (nonatomic, strong, readonly) NSMutableArray *yourAmoursUsers;
@property (nonatomic, strong, readonly) NSMutableArray *pendingAmoursUsers;
@property (nonatomic, strong, readonly) NSMutableArray *heartsPushedUsers;
@property (nonatomic, strong, readonly) NSMutableArray *browserUsers;
@property (nonatomic, strong, readonly) NSMutableArray *searchUsers;

@property (nonatomic, assign, readonly, getter = isDownloadingBrowserUsers) BOOL downloadingBrowserUsers;
@property (nonatomic, assign, readonly, getter = isDownloadingSearchUsers) BOOL downloadingSearchUsers;

// Public Methods
+ (instancetype)sharedManager;

#pragma mark Master Methods

- (void)loginWithSessionID:(NSString *)sessionID;
- (void)loadUserWithGetUserJSONResult:(NSDictionary *)jsonDictionary;
- (void)logout;
- (BOOL)isLoggedIn;

#pragma mark Current User Methods

- (void)saveUser;
- (void)refreshCurrentUserDataRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
- (BOOL)isCurrentUser:(INSUser *)user;

#pragma mark Your Amours Methods

- (void)saveYourAmoursUsers;
- (void)refreshYourAmoursRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
- (INSUser *)amourWithQuickBloxID:(NSInteger)quickBloxID;
- (INSUser *)amourWithUserID:(NSString *)userID;
- (BOOL)isAmourUser:(INSUser *)user;

#pragma mark Pending Amours Methods

- (void)savePendingAmoursUsers;
- (void)refreshPendingAmoursRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
- (BOOL)isPendingAmourUser:(INSUser *)user;
- (void)movePendingAmourUserToAmours:(INSUser *)user;

#pragma mark Hearts Pushed Methods

- (void)saveHeartsPushedUsers;
- (void)refreshHeartsPushedRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
- (BOOL)isHeartPushedUser:(INSUser *)user;

#pragma mark Browser User Methods

- (void)clearBrowserUsers;
- (void)downloadMoreBrowserUsers;
- (void)saveBrowserUsers;

#pragma mark Search User Methods

- (void)clearSearchUsers;
- (void)downloadMoreSearchUsers;
- (void)saveSearchUsers;

#pragma mark Misc Methods

- (void)removeUserFromAllLists:(INSUser *)userToRemove;
- (void)removeUserWithIDFromAllLists:(NSString *)userID;

@end
