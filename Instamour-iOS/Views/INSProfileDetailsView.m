//
//  INSProfileDetailsView.m
//  Instamour
//
//  Created by Brian Slick on 7/16/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSProfileDetailsView.h"

@interface INSProfileDetailsView ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) IBOutlet UILabel *kissesLabel;
@property (nonatomic, strong) IBOutlet UILabel *comment1Label;
@property (nonatomic, strong) IBOutlet UILabel *comment2Label;
@property (nonatomic, strong) IBOutlet UILabel *comment3Label;
@property (nonatomic, strong) IBOutlet UIButton *allCommentsButton;
@property (nonatomic, strong) IBOutlet UIButton *moreButton;

@end

@implementation INSProfileDetailsView

#pragma mark - Custom Getters and Setters


#pragma mark - Initialization

+ (instancetype)profileDetailsViewWithDelegate:(id<INSProfileDetailsViewDelegate>)delegate
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
    NSArray *nibContents = [nib instantiateWithOwner:self options:nil];
    
    INSProfileDetailsView *view = nil;
    for (id nibItem in nibContents)
    {
        if ([nibItem isKindOfClass:[self class]])
        {
            view = (INSProfileDetailsView *)nibItem;
            break;
        }
    }
    
    [view setDelegate:delegate];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return view;
}

#pragma mark - UI Response Methods

- (IBAction)allCommentsButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(profileDetailsViewAllCommentsButtonPressed:)])
    {
        [[self delegate] profileDetailsViewAllCommentsButtonPressed:self];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)kissButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(profileDetailsViewKissButtonPressed:)])
    {
        [[self delegate] profileDetailsViewKissButtonPressed:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)commentButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(profileDetailsViewCommentButtonPressed:)])
    {
        [[self delegate] profileDetailsViewCommentButtonPressed:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)moreButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(profileDetailsViewMoreButtonPressed:)])
    {
        [[self delegate] profileDetailsViewMoreButtonPressed:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setProfileDescription:(NSString *)profileDescription
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self descriptionLabel] setText:profileDescription];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setNumberOfKisses:(NSInteger)numberOfKisses
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSString *kissString = @"1 Kiss";
    if (numberOfKisses != 1)
    {
        kissString = [NSString stringWithFormat:@"%ld Kisses", (long)numberOfKisses];
    }
    
    [[self kissesLabel] setText:kissString];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setComments:(NSArray *)comments
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateAdded" ascending:NO];
    NSArray *sortedComments = [comments sortedArrayUsingDescriptors:@[ dateDescriptor ]];
    
    NSArray *labels = @[ [self comment1Label], [self comment2Label], [self comment3Label] ];

    NSInteger numberOfComments = [sortedComments count];
    
    [labels enumerateObjectsUsingBlock:^(UILabel *label, NSUInteger index, BOOL *stop) {
        
        [label setText:nil];
        
        if (index >= numberOfComments)
        {
            return;
        }
        
        INSComment *comment = [sortedComments objectAtIndex:index];
        
        NSString *userName = [comment senderUserName];
        NSString *body = [comment body];
        
        if ( [userName isNotEmpty] && [body isNotEmpty] )
        {
            [label setAttributedText:[self attributedStringForUserName:userName commentBody:body]];
        }
        
    }];

    [self setTotalNumberOfComments:numberOfComments];
    
    if (numberOfComments == 0)
    {
        [[self comment1Label] setText:@"No comments"];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setTotalNumberOfComments:(NSInteger)numberOfComments
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIButton *commentsButton = [self allCommentsButton];
    
    if (numberOfComments <= 3)
    {
        [commentsButton setHidden:YES];
    }
    else
    {
        [commentsButton setHidden:NO];
        
        [commentsButton setTitle:[NSString stringWithFormat:@"View all %ld comments", (long)numberOfComments] forState:UIControlStateNormal];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setMoreButtonHidden:(BOOL)isHidden
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self moreButton] setHidden:isHidden];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (NSAttributedString *)attributedStringForUserName:(NSString *)userName
                                        commentBody:(NSString *)comment
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // User name is red
    // comment is gray
    
    NSString *fullCommentString = [NSString stringWithFormat:@"%@ %@", userName, comment];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:fullCommentString];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor redColor]
                             range:NSMakeRange(0, [userName length])];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor profileCommentColorINS]
                             range:NSMakeRange([userName length] + 1, [comment length])];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return attributedString;
}

@end
