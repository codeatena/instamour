//
//  NSString+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/4/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "NSString+INSAdditions.h"

@implementation NSString (INSAdditions)

- (BOOL)isNotEmpty
{
    if ([self length] == 0)
    {
        return NO;
    }
    
    NSSet *stringsToTreatAsEmpty = [NSSet setWithObjects:@"", @"<>", @"<null>", @"(null)", nil];
    
    if ([stringsToTreatAsEmpty containsObject:self])
    {
        return NO;
    }
    
    return YES;
}

// http://www.cocoanetics.com/2014/06/e-mail-validation/
- (BOOL)isValidEmailAddress
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([self length] == 0)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Empty String", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    NSRange entireRange = NSMakeRange(0, [self length]);
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                               error:NULL];
    NSArray *matches = [detector matchesInString:self options:0 range:entireRange];
    
    // should only a single match
    if ([matches count] != 1)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid number of matches", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    NSTextCheckingResult *result = [matches firstObject];
    
    // result should be a link
    if (result.resultType != NSTextCheckingTypeLink)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Non-link result", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    // result should be a recognized mail address
    if (![result.URL.scheme isEqualToString:@"mailto"])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not a mailto address", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    // match must be entire string
    if (!NSEqualRanges(result.range, entireRange))
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid range", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    // but schould not have the mail URL scheme
    if ([self hasPrefix:@"mailto:"])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - mailto: prefix", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    // no complaints, string is valid email address

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

+ (NSString *)heartsPushedSitTightMessageForRecipientGenderINS:(NSString *)gender
{
    NSString *message = nil;
    
    if ([[gender lowercaseString] isEqualToString:[INSMaleString lowercaseString]])
    {
        message = @"You pushed his heart, now just sit tight while he checks you out!";
    }
    else if ([[gender lowercaseString] isEqualToString:[INSFemaleString lowercaseString]])
    {
        message = @"You pushed her heart, now just sit tight while she checks you out!";
    }
    else
    {
        message = @"You pushed their heart, now just sit tight while they check you out!";
    }
    
    return message;
}

+ (NSString *)pendingAmoursEncouragementMessageForRecipientGenderINS:(NSString *)gender
{
    NSString *message = nil;
    
    if ([[gender lowercaseString] isEqualToString:[INSMaleString lowercaseString]])
    {
        message = @"He pushed your heart, now check out his video and push his heart too!";
    }
    else if ([[gender lowercaseString] isEqualToString:[INSFemaleString lowercaseString]])
    {
        message = @"She pushed your heart, now check out her video and push her heart too!";
    }
    else
    {
        message = @"They pushed your heart, now check out their video and push their heart too!";
    }
    
    return message;
}

@end
