package com.hyper.instamour;

import java.util.ArrayList;


public class Config {
	
	static ArrayList<MenuItemList> address = null;

	public static ArrayList<MenuItemList> createAddress() {
		address = new ArrayList<MenuItemList>();
		address.add(new MenuItemList(R.drawable.amours,"Your Amours"));
		address.add(new MenuItemList(R.drawable.browseprof,"Browse Profiles"));
		address.add(new MenuItemList(R.drawable.chat,"Instant chat"));
		address.add(new MenuItemList(R.drawable.video,"Create Videos"));
		address.add(new MenuItemList(R.drawable.share,"Share Instamour"));
		address.add(new MenuItemList(R.drawable.gift,"Purchase Gifts"));
		address.add(new MenuItemList(R.drawable.settings,"Settings"));
		address.add(new MenuItemList(R.drawable.logout,"Logout"));
		return address;

	}

}