//
//  INSChatTableHeaderView.h
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewHeaderFooterView.h"

// Public Constants

// Protocols

@interface INSChatTableHeaderView : BTITableViewHeaderFooterView

// Public Properties
@property (nonatomic, strong) IBOutlet UILabel *headerLabel;

// Public Methods

@end
