//
//  INSInfoViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIWebViewController.h"

// Public Constants

// Protocols

@interface INSInfoViewController : BTIWebViewController

// Public Properties
@property (nonatomic, copy) NSString *infoTitle;

// Public Methods

@end
