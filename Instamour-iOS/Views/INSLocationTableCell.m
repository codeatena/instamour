//
//  INSLocationTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSLocationTableCell.h"

@interface INSLocationTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *locationDetailLabel;

@end

@implementation INSLocationTableCell

#pragma mark - Misc Methods

- (void)setLocation:(NSString *)location
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    UILabel *label = [self locationDetailLabel];
    
    if ([location isNotEmpty])
    {
        [label setText:location];
        [label setTextColor:[UIColor darkGrayColor]];
    }
    else
    {
        [label setText:@"-- Location not available --"];
        [label setTextColor:[UIColor lightGrayColor]];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
