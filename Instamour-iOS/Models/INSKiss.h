//
//  INSKiss.h
//  Instamour
//
//  Created by Brian Slick on 7/29/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "BTIObject.h"

@interface INSKiss : BTIObject <NSCoding>

// Public Properties
@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *senderID;
@property (nonatomic, copy) NSDate *dateAdded;

// Public Methods
- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDictionary;

@end
