//
//  INSChatMessage.m
//  Instamour
//
//  Created by Brian Slick on 8/7/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatMessage.h"

// Private Constants
NSString *const INSChatMessageAmourUserIDKey = @"INSChatMessageAmourUserIDKey";
NSString *const INSChatMessageDateSentKey = @"INSChatMessageDateSentKey";

NSString *const INSChatMessageToCurrentUserKey = @"INSChatMessageToCurrentUserKey";

NSString *const INSChatMessageMessageTypeKey = @"INSChatMessageMessageTypeKey";
NSString *const INSChatMessageMessageBodyKey = @"INSChatMessageMessageBodyKey";
NSString *const INSChatMessageImageFileNameKey = @"INSChatMessageImageFileNameKey";
NSString *const INSChatMessageVideoFileNameKey = @"INSChatMessageVideoFileNameKey";
NSString *const INSChatMessageVideoURLKey = @"INSChatMessageVideoURLKey";

NSString *const INSChatMessageUnreadKey = @"INSChatMessageUnreadKey";

NSString *const INSChatMessageQuickBloxMessageKey = @"INSChatMessageQuickBloxMessageKey";

@interface INSChatMessage ()

// Private Properties

@end

@implementation INSChatMessage

#pragma mark - Dealloc and Memory Management

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Getters and Setters

- (UIImage *)image
{
    if (_image == nil)
    {
        [self loadImage];
    }
    
    return _image;
}

- (UIImage *)thumbnailImage
{
    if (_thumbnailImage == nil)
    {
        [self loadThumbnailImage];
    }
    
    return _thumbnailImage;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveMemoryWarningNotification:)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
    }
    return self;
}

#pragma mark - NSCoding Methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self amourUserID] forKey:INSChatMessageAmourUserIDKey];
    [aCoder encodeObject:[self dateSent] forKey:INSChatMessageDateSentKey];
    
    [aCoder encodeBool:[self isToCurrentUser] forKey:INSChatMessageToCurrentUserKey];
    
    [aCoder encodeInteger:[self messageType] forKey:INSChatMessageMessageTypeKey];
    [aCoder encodeObject:[self messageBody] forKey:INSChatMessageMessageBodyKey];
    [aCoder encodeObject:[self imageFileName] forKey:INSChatMessageImageFileNameKey];
    [aCoder encodeObject:[self videoFileName] forKey:INSChatMessageVideoFileNameKey];
    [aCoder encodeObject:[self videoURL] forKey:INSChatMessageVideoURLKey];
    
    [aCoder encodeBool:[self isUnread] forKey:INSChatMessageUnreadKey];
    
    [aCoder encodeObject:[self quickBloxChatMessage] forKey:INSChatMessageQuickBloxMessageKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self)
    {
        [self setAmourUserID:[aDecoder decodeObjectForKey:INSChatMessageAmourUserIDKey]];
        [self setDateSent:[aDecoder decodeObjectForKey:INSChatMessageDateSentKey]];
        
        [self setToCurrentUser:[aDecoder decodeBoolForKey:INSChatMessageToCurrentUserKey]];
        
        [self setMessageType:[aDecoder decodeIntegerForKey:INSChatMessageMessageTypeKey]];
        [self setMessageBody:[aDecoder decodeObjectForKey:INSChatMessageMessageBodyKey]];
        [self setImageFileName:[aDecoder decodeObjectForKey:INSChatMessageImageFileNameKey]];
        [self setVideoFileName:[aDecoder decodeObjectForKey:INSChatMessageVideoFileNameKey]];
        [self setVideoURL:[aDecoder decodeObjectForKey:INSChatMessageVideoURLKey]];
        
        [self setUnread:[aDecoder decodeBoolForKey:INSChatMessageUnreadKey]];
        
        [self setQuickBloxChatMessage:[aDecoder decodeObjectForKey:INSChatMessageQuickBloxMessageKey]];
    }
    return self;
}

#pragma mark - Misc Methods

- (BOOL)isBusy
{
    return ( [self isUploading] || [self isDownloading] || [self isGeneratingThumbnail] );
}

- (NSString *)thumbnailFileName
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSString *thumbnailFileName = nil;
    
    if ([self messageType] == INSChatMessageTypeImage)
    {
        NSString *imageFileName = [self imageFileName];
        
        if (![imageFileName isNotEmpty])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No image file name", self, __PRETTY_FUNCTION__);
            return nil;
        }
        
        NSString *fileExtension = [imageFileName pathExtension];
        NSString *rootFileName = [imageFileName stringByDeletingPathExtension];
        
        thumbnailFileName = [[rootFileName stringByAppendingString:@"_thumb"] stringByAppendingPathExtension:fileExtension];
    }
    else if ([self messageType] == INSChatMessageTypeVideo)
    {
        NSURL *videoURL = [self videoURL];
        
        if (videoURL == nil)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No video URL", self, __PRETTY_FUNCTION__);
            return nil;
        }
        
        NSString *fileName = [videoURL lastPathComponent];
        
        NSString *rootFileName = [fileName stringByDeletingPathExtension];

        thumbnailFileName = [[rootFileName stringByAppendingString:@"_thumb"] stringByAppendingPathExtension:@"png"];
    }
    
    NSLog(@"thumbnailFileName: %@", thumbnailFileName);
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return thumbnailFileName;
}

- (void)saveImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    UIImage *image = [self image];
    if (image == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No image", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *fileName = [self imageFileName];
    if (![fileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];
    
    NSData *data = UIImagePNGRepresentation(image);
    
    [data writeToURL:fileURL atomically:YES];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveThumbnailImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *image = [self thumbnailImage];
    if (image == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No image", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *fileName = [self thumbnailFileName];
    if (![fileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];

    NSData *data = UIImagePNGRepresentation(image);

    [data writeToURL:fileURL atomically:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *fileName = [self imageFileName];
    
    if (![fileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No local file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    UIImage *image = [[UIImage alloc] initWithData:data];
    
    [self setImage:image];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadThumbnailImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *fileName = [self thumbnailFileName];
    
    if (![fileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No local file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    UIImage *image = [[UIImage alloc] initWithData:data];
    
    [self setThumbnailImage:image];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)didReceiveMemoryWarningNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setImage:nil];
    [self setThumbnailImage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
