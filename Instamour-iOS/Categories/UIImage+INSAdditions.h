//
//  UIImage+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/15/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (INSAdditions)

+ (UIImage *)maleButtonImageCheckedINS:(BOOL)isChecked;
+ (UIImage *)femaleButtonImageCheckedINS:(BOOL)isChecked;
+ (UIImage *)placeholderAvatarImageINS;

+ (UIImage *)playButtonImageINS;
+ (UIImage *)pauseButtonImageINS;

+ (UIImage *)navigationBarLogoINS;

- (UIImage *)croppedProfileImageINS;
- (UIImage *)chatThumbnailImageINS;

- (UIImage *)imageByRemovingOrientationINS;

- (UIImage *)imageByScalingAspectFitToSizeINS:(CGSize)size;

- (BOOL)isSquareINS;

@end
