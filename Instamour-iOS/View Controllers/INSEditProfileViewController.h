//
//  INSEditProfileViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIManagerTableViewController.h"

// Public Constants

// Protocols

@interface INSEditProfileViewController : BTIManagerTableViewController

// Public Properties

// Public Methods

@end
