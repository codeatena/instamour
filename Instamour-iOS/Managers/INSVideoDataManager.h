//
//  INSVideoDataManager.h
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

// Forward Declarations and Classes
@class INSVideoInfo;

// Public Constants
FOUNDATION_EXPORT NSString *const INSVideoDataManagerDidChangeVideoListNotification;


@interface INSVideoDataManager : NSObject

// Public Properties
@property (nonatomic, strong, readonly) NSArray *videoInfoList;
@property (nonatomic, assign, readonly, getter = isListReordering) BOOL listReordering;

// Public Methods

+ (instancetype)sharedManager;

#pragma mark Data Managing Methods

- (void)loadVideoList;
- (void)saveVideoList;
- (void)deleteAllData;
- (void)populateWithGetUserJSONDictionary:(NSDictionary *)jsonDictionary;

#pragma mark Interrogation Methods

- (INSVideoInfo *)mergeVideo;
- (BOOL)isAnyVideoUploading;
- (BOOL)isAnyVideoPopulated;
- (BOOL)isAnyVideoDeleting;
- (BOOL)isAnyVideoTrimming;

#pragma mark Server Call Methods

- (void)uploadVideoAtURL:(NSURL *)fileURL forServerSlotNumber:(NSInteger)serverSlotNumber withServerRotation:(INSServerAPIManagerVideoRotation)serverRotation;
- (void)moveVideoAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;
- (void)deleteVideoAtIndex:(NSInteger)index;
- (void)refreshMergeVideoIfNecessary;

@end
