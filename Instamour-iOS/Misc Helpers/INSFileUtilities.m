//
//  INSFileUtilities.m
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSFileUtilities.h"

@interface INSFileUtilities ()

// Private Properties

@end

@implementation INSFileUtilities

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Management


#pragma mark - Custom Getters and Setters


#pragma mark - Initialization


#pragma mark - Directories

+ (NSURL *)imageDirectoryURL
{
    NSURL *targetURL = [[BTIFileUtilities cacheDirectoryURL] URLByAppendingPathComponent:@"Images"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:[targetURL path]])
    {
        [fileManager createDirectoryAtPathBTI:[targetURL path]
                  withIntermediateDirectories:YES
                                   attributes:nil];
    }
    
	return targetURL;
}

+ (NSURL *)videoDirectoryURL
{
    NSURL *targetURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:@"Videos"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:[targetURL path]])
    {
        [fileManager createDirectoryAtPathBTI:[targetURL path]
                  withIntermediateDirectories:YES
                                   attributes:nil];
    }
    
	return targetURL;
}

+ (NSURL *)chatDataDirectoryURL
{
    NSURL *targetURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:@"ChatData"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:[targetURL path]])
    {
        [fileManager createDirectoryAtPathBTI:[targetURL path]
                  withIntermediateDirectories:YES
                                   attributes:nil];
    }
    
	return targetURL;
}

+ (NSURL *)uniqueNewVideoURL
{
    NSURL *urlToReturn = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyyMMddkkmmss"];

    NSInteger documentCounter = 1;
    BOOL isValidURL = NO;
    
    while (!isValidURL)
    {
        NSString *fileName = [NSString stringWithFormat:@"%@_%ld.mp4", [dateFormatter stringFromDate:[NSDate date]], (long)documentCounter];
        documentCounter++;
        
        urlToReturn = [[INSFileUtilities videoDirectoryURL] URLByAppendingPathComponent:fileName];
        
        isValidURL = ![fileManager fileExistsAtPath:[urlToReturn path]];
    }
    
    return urlToReturn;
}

+ (NSURL *)uniqueNewImageURL
{
    NSURL *urlToReturn = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyyMMddkkmmss"];
    
    NSInteger documentCounter = 1;
    BOOL isValidURL = NO;
    
    while (!isValidURL)
    {
        NSString *fileName = [NSString stringWithFormat:@"%@_%ld.png", [dateFormatter stringFromDate:[NSDate date]], (long)documentCounter];
        documentCounter++;
        
        urlToReturn = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];
        
        isValidURL = ![fileManager fileExistsAtPath:[urlToReturn path]];
    }
    
    return urlToReturn;
}

@end
