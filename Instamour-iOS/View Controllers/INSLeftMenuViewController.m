//
//  INSLeftMenuViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSLeftMenuViewController.h"

// Models and other global

// Sub-controllers
#import "INSCurrentUserProfileViewController.h"
#import "INSAmoursViewController.h"
#import "INSBrowseProfilesViewController.h"
#import "INSShareViewController.h"
#import "INSGiftPurchaseViewController.h"
#import "INSSettingsViewController.h"
#import "INSVideoListViewController.h"
#import "INSChatAmoursViewController.h"

// Views
#import "INSLeftMenuCell.h"
#import "INSMenuProfileTableCell.h"

// Private Constants
NSString *const INSMenuRowIdentifierEditProfile = @"INSMenuRowIdentifierEditProfile";
NSString *const INSMenuRowIdentifierYourAmours = @"INSMenuRowIdentifierYourAmours";
NSString *const INSMenuRowIdentifierBrowseProfiles = @"INSMenuRowIdentifierBrowseProfiles";
NSString *const INSMenuRowIdentifierInstantChat = @"INSMenuRowIdentifierInstantChat";
NSString *const INSMenuRowIdentifierCreateVideos = @"INSMenuRowIdentifierCreateVideos";
NSString *const INSMenuRowIdentifierShareInstamour = @"INSMenuRowIdentifierShareInstamour";
NSString *const INSMenuRowIdentifierPurchaseGifts = @"INSMenuRowIdentifierPurchaseGifts";
NSString *const INSMenuRowIdentifierSettings = @"INSMenuRowIdentifierSettings";
NSString *const INSMenuRowIdentifierLogout = @"INSMenuRowIdentifierLogout";

@interface INSLeftMenuViewController ()

// Private Properties

@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, copy) NSIndexPath *selectedIndexPath;

@property (nonatomic, assign) CGFloat profileRowHeight;
@property (nonatomic, assign) CGFloat standardRowHeight;

@end

@implementation INSLeftMenuViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    // Register table cells
    
    UITableView *tableView = [self tableView];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // SLICK
    // This was interfering with the browser's ability to scroll to the top in response to a status bar touch.
    // Disabling it in this view allows the browser to work properly. Don't need it here anyway.
    [tableView setScrollsToTop:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
    
    [self refreshContentsAndRestoreSelectedRow];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewDidDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidDisappear:animated];
    
    [[self mainContentsManager] reset];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    AppDelegate *appDelegate = [AppDelegate sharedDelegate];
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    INSUser *currentUser = [userManager currentUser];
    
    BTITableContentsManager *contentsManager = [self mainContentsManager];
    [contentsManager reset];
    
    BTITableRowInfo *rowInfo = nil;
    
    {{  // Edit Profile
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierEditProfile];
        
        [rowInfo setText:[currentUser userName]];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSCurrentUserProfileViewController *nextViewController = [[INSCurrentUserProfileViewController alloc] init];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Your Amours
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierYourAmours];
        
        NSInteger numberOfPendingAmours = [[userManager pendingAmoursUsers] count];
        
        [rowInfo setText:@"Your Amours"];
        [rowInfo setDetailText:[NSString stringWithFormat:@"%ld", (long)numberOfPendingAmours]];
        [rowInfo setImageName:@"heart-icon"];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSAmoursViewController *nextViewController = [[INSAmoursViewController alloc] init];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Browse Profiles
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierBrowseProfiles];
        
        [rowInfo setText:@"Browse Profiles"];
        [rowInfo setDetailText:@"0"];
        [rowInfo setImageName:@"profiles-icon"];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSBrowseProfilesViewController *nextViewController = [[INSBrowseProfilesViewController alloc] initWithDisplayMode:INSBrowseProfilesDisplayModeBrowse];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Instant Chat
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierInstantChat];
        
        [rowInfo setText:@"Instant Chat"];
        [rowInfo setDetailText:[NSString stringWithFormat:@"%ld", (long)[[INSChatDataManager sharedManager] numberOfUnreadMessages]]];
        [rowInfo setImageName:@"chat_icon_left_nav"];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSChatAmoursViewController *nextViewController = [[INSChatAmoursViewController alloc] init];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Create Videos
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierCreateVideos];
        
        [rowInfo setText:@"Create Videos"];
        [rowInfo setDetailText:@"0"];
        [rowInfo setImageName:@"videos-icon"];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSVideoListViewController *nextViewController = [[INSVideoListViewController alloc] init];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Share Instamour
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierShareInstamour];
        
        [rowInfo setText:@"Share Instamour"];
        [rowInfo setDetailText:@"0"];
        [rowInfo setImageName:@"share-icon"];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSShareViewController *nextViewController = [[INSShareViewController alloc] init];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Purchase Gifts
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierPurchaseGifts];
        
        [rowInfo setText:@"Purchase Gifts"];
        [rowInfo setDetailText:@"0"];
        [rowInfo setImageName:@"gifts-icon"];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSGiftPurchaseViewController *nextViewController = [[INSGiftPurchaseViewController alloc] init];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Settings
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierPurchaseGifts];
        
        [rowInfo setText:@"Settings"];
        [rowInfo setDetailText:@"0"];
        [rowInfo setImageName:@"settings-icon"];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSSettingsViewController *nextViewController = [[INSSettingsViewController alloc] init];
            
            [appDelegate showViewControllerInCenter:nextViewController
                              closeMenuWhenFinished:YES];
            
        }];
    }}
    
    {{  // Logout
        rowInfo = [contentsManager dequeueReusableRowInfo];
        [contentsManager addRowInfo:rowInfo makeNewSection:NO];
        [rowInfo setIdentifier:INSMenuRowIdentifierPurchaseGifts];
        
        [rowInfo setText:@"Logout"];
        [rowInfo setDetailText:@"0"];
        [rowInfo setImageName:@"logout"];
        
        [rowInfo setRowSelectionBlock:^{
            
            [appDelegate logoutUser];
            
        }];
    }}
    
    [self refreshProfileAndBackgroundImages];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addLifetimeNotificationInfoForName:MFSideMenuStateNotificationEvent
                                    selector:@selector(sideMenuStateChangedNotification:)
                                      object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSChatDataManagerDidChangeAllConversationsNotification
                                   selector:@selector(allChatConversationsDidChangeNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [INSMenuProfileTableCell registerNibForTableViewBTI:tableView];
    [INSLeftMenuCell registerNibForTableViewBTI:tableView];
    
    INSMenuProfileTableCell *menuProfileTableCell = [INSMenuProfileTableCell dequeueCellFromTableViewBTI:tableView];
    [self setProfileRowHeight:CGRectGetHeight([menuProfileTableCell frame])];
    
    INSLeftMenuCell *leftMenuCell = [INSLeftMenuCell dequeueCellFromTableViewBTI:tableView];
    [self setStandardRowHeight:CGRectGetHeight([leftMenuCell frame])];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)sideMenuStateChangedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    MFSideMenuStateEvent menuState = [[userInfo objectForKey:@"eventType"] intValue];
    
    // SLICK: I hate hate HATE doing this, but I need these events to trigger. And this MF menu controller sucks.
    
    switch (menuState)
    {
        case MFSideMenuStateEventMenuWillOpen:
        {
            [self viewWillAppear:YES];
        }
            break;
        case MFSideMenuStateEventMenuDidOpen:
        {
            [self viewDidAppear:YES];
        }
            break;
        case MFSideMenuStateEventMenuWillClose:
        {
            [self viewWillDisappear:YES];
        }
            break;
        case MFSideMenuStateEventMenuDidClose:
        {
            [self viewDidDisappear:YES];
        }
            break;
        default:
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)allChatConversationsDidChangeNotification:(NSNotification *)notification;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self refreshContentsAndRestoreSelectedRow];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods


#pragma mark - Misc Methods

- (void)refreshContentsAndRestoreSelectedRow
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self populateContents];
    
    [[self tableView] reloadData];
    
    if ([self selectedIndexPath] == nil)
    {
        UIViewController *centerViewController = [(UINavigationController *)[[[AppDelegate sharedDelegate] sideMenuContainerViewController] centerViewController] topViewController];
        
        NSString *rowIdentifier = INSMenuRowIdentifierBrowseProfiles;
        
        if ([centerViewController isKindOfClass:[INSVideoListViewController class]])
        {
            rowIdentifier = INSMenuRowIdentifierCreateVideos;
        }
        
        NSIndexPath *indexPath = [[self mainContentsManager] indexPathOfRowIdentifier:rowIdentifier];
        [self setSelectedIndexPath:indexPath];
    }
    
    [[self tableView] selectRowAtIndexPath:[self selectedIndexPath] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (UIImage *)blurredImageForView:(UIView *)view
{
    // x, y and size variables below are only examples.
    // You will want to calculate this in code based on the view you will be presenting.
    float x = 0;
    float y = 0;
    //    CGSize size = CGSizeMake(300,[UIScreen mainScreen].bounds.size.height);
    CGSize size = [view frame].size
    ;
    UIGraphicsBeginImageContext(size);
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(c, -x, -y);
    [view.layer renderInContext:c]; // view is the view you are grabbing the screen shot of. The view that is to be blurred.
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1); // convert to jpeg
    image = [UIImage imageWithData:imageData];
    return image;
}

- (void)refreshProfileAndBackgroundImages
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImageView *backgroundImageView = [self backgroundImageView];
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    UIImage *avatarImage = [currentUser avatarImage];
    if (avatarImage == nil)
    {
        __weak INSLeftMenuViewController *weakSelf = self;
        
        [currentUser setAvatarDownloadCompletionBlock:^(UIImage *image){
            
            [weakSelf refreshProfileAndBackgroundImages];
            
        }];
        
        avatarImage = [UIImage placeholderAvatarImageINS];
    }
    else
    {
        [currentUser setAvatarDownloadCompletionBlock:nil];
    }
    
    // SLICK
    // This is a quickly cleaned up version of what was here. There's probably a better way.
    UIImageView *blurImageView = [[UIImageView alloc] initWithImage:avatarImage];
    [blurImageView setContentMode:UIViewContentModeScaleToFill];
    
    UIImage *blurImage = [self blurredImageForView:blurImageView];
    UIImage *darkImage = [blurImage applyDarkEffect];
    
    [backgroundImageView setImage:darkImage];
    
    NSIndexPath *indexPath = [[self mainContentsManager] indexPathOfRowIdentifier:INSMenuRowIdentifierEditProfile];
    if (indexPath == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Contents not loaded yet", self, __PRETTY_FUNCTION__);
        return;
    }
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    
    [rowInfo setImage:avatarImage];
    
    [[self tableView] reloadRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self mainContentsManager] numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContentsManager] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    UITableViewCell *cellToReturn = nil;
    
    if ([rowIdentifier isEqualToString:INSMenuRowIdentifierEditProfile])
    {
        INSMenuProfileTableCell *profileCell = [INSMenuProfileTableCell dequeueCellFromTableViewBTI:tableView];
        
        [[profileCell avatarImageView] setImage:[rowInfo image]];
        [[profileCell userNameLabel] setText:[rowInfo text]];
        
        cellToReturn = profileCell;
    }
    else
    {
        INSLeftMenuCell *menuCell = [INSLeftMenuCell dequeueCellFromTableViewBTI:tableView];
        [[menuCell iconImageView] setImage:[UIImage imageNamed:[rowInfo imageName]]];
        [[menuCell titleLabel] setText:[rowInfo text]];
        [menuCell setCount:[[rowInfo detailText] integerValue]];
        
        cellToReturn = menuCell;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cellToReturn;
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    CGFloat height = [self standardRowHeight];
    
    if ([rowIdentifier isEqualToString:INSMenuRowIdentifierEditProfile])
    {
        height = [self profileRowHeight];
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    
    [rowInfo safelyPerformRowSelectionBlock];
    
    [self setSelectedIndexPath:indexPath];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
