//
//  NSDictionary+INSJSONAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "NSDictionary+INSJSONAdditions.h"

#import "NSString+HTML.h"

@implementation NSDictionary (INSJSONAdditions)

- (BOOL)isStatusSuccessfulINS
{
    const NSString *key = @"state";

    NSString *status = [self objectForKey:key];
    
    BOOL didSucceed = ([status isEqualToString:@"success"]);

    return didSucceed;
}

- (NSString *)errorMessageINS
{
    const NSString *key = @"msg";
    const NSString *altKey = @"message";

    NSString *object = [self objectForKey:key];
    if (![object isNotEmpty])
    {
        object = [self objectForKey:altKey];
    }
        
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)sessionIdentifierINS
{
    const NSString *key = @"sessionId";

    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)chatFileURLStringINS
{
    const NSString *key = @"url";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSDictionary *)currentUserINS
{
    const NSString *key = @"user";

    id object = [self objectForKey:key];
    
    if (![object isKindOfClass:[NSDictionary class]])
    {
        return nil;
    }
    
    return object;
}

- (NSDictionary *)settingsForUserINS
{
    const NSString *key = @"settings";

    id object = [self objectForKey:key];
    
    if (![object isKindOfClass:[NSDictionary class]])
    {
        return nil;
    }
    
    return object;
}

- (NSString *)identifierForUserINS
{
    const NSString *key = @"uid";
    
    NSString *object = [self objectForKey:key];
    
    if ([object isNotEmpty])
    {
        return object;
    }
    
    NSDictionary *settings = [self settingsForUserINS];
    
    object = [settings objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)userNameForUserINS
{
    const NSString *key = @"uname";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)emailForUserINS
{
    const NSString *key = @"email";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)facebookIDForUserINS
{
    const NSString *key = @"fbid";
    
    NSString *object = [self objectForKey:key];
    
    if ([object isNotEmpty])
    {
        return object;
    }
    
    NSDictionary *settings = [self settingsForUserINS];
    
    object = [settings objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)chatIDForUserINS
{
    const NSString *key = @"chatid";
    
    NSString *object = [self objectForKey:key];
    
    if ([object isNotEmpty])
    {
        return object;
    }
    
    NSDictionary *settings = [self settingsForUserINS];
    
    object = [settings objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)quickBloxPasswordForUserINS
{
    const NSString *key = @"qbpass";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSDate *)dateAddedForUserINS
{
    const NSString *key = @"date_added";
    
    NSString *object = [self objectForKey:key];
    NSDate *date = nil;
    
    if ([object isNotEmpty])
    {
        NSDateFormatter *dateFormatter = [[AppDelegate sharedDelegate] serverDateFormatter];
        date = [dateFormatter dateFromString:object];
    }
    
    return date;
}

- (BOOL)isUserAccountDisabledINS
{
    const NSString *key = @"disable_account";
    
    return ([[self objectForKey:key] integerValue] == 1);
}

- (BOOL)isUserOnlineINS
{
    const NSString *key = @"isonline";
    
    return ([[self objectForKey:key] integerValue] == 1);
}

- (NSString *)genderForUserINS
{
    const NSString *key = @"gender";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    if ([object isNotEmpty])
    {
        if ([[object lowercaseString] isEqualToString:[INSMaleString lowercaseString]])
        {
            object = INSMaleString;
        }
        else if ([[object lowercaseString] isEqualToString:[INSFemaleString lowercaseString]])
        {
            object = INSFemaleString;
        }
        else
        {
            object = nil;
        }
    }
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSDate *)dateOfBirthForUserINS
{
    const NSString *key = @"dob";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    NSDate *date = nil;
    
    if ([object isNotEmpty])
    {
        NSDateFormatter *dateFormatter = [[AppDelegate sharedDelegate] serverBirthdayDateFormatter];
        date = [dateFormatter dateFromString:object];
    }
    
    return date;
}

- (NSString *)ethnicityForUserINS
{
    const NSString *key = @"ethnicity";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)cityForUserINS
{
    const NSString *key = @"city";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)stateForUserINS
{
    const NSString *key = @"state";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)countryForUserINS
{
    const NSString *key = @"country";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)latitudeForUserINS       // TODO: Make number object?
{
    const NSString *key = @"ltd";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)longitudeForUserINS       // TODO: Make number object?
{
    const NSString *key = @"lngd";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)profileImageNameForUserINS
{
    const NSString *key = @"photo";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (BOOL)isAudioChatEnabledForUserINS
{
    const NSString *key = @"audio_chat";
    
    return ([[[self settingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isVideoChatEnabledForUserINS
{
    const NSString *key = @"video_chat";
    
    return ([[[self settingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (NSString *)identityForUserINS
{
    const NSString *key = @"identity";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)heightForUserINS
{
    const NSString *key = @"height";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)bodyTypeForUserINS
{
    const NSString *key = @"body_type";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)sexualPreferenceForUserINS
{
    const NSString *key = @"sexual_preference";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)lookingForForUserINS
{
    const NSString *key = @"looking_for";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)smokerForUserINS
{
    const NSString *key = @"smoker";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

//- (NSString *)aboutMeForUserINS
//{
//    const NSString *key = @"about_me";
//    
//    NSString *object = [[self settingsForUserINS] objectForKey:key];
//    
//    return ([object isNotEmpty]) ? object : nil;
//}

- (NSString *)childrenForUserINS
{
    const NSString *key = @"children";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    NSString *children = [INSServerAPIManager childrenStatusForServerCode:object];
        
    return ([children isNotEmpty]) ? object : nil;
}

- (NSString *)instagramForUserINS
{
    const NSString *key = @"instagram";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)vineForUserINS
{
    const NSString *key = @"vine";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)fileNameForUserVideoNumberINS:(NSInteger)videoNumber
{
    NSString *key = [NSString stringWithFormat:@"video%ld", (long)videoNumber];
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)thumbnailFileNameForUserVideoNumberINS:(NSInteger)videoNumber
{
    NSString *key = [NSString stringWithFormat:@"thumb%ld", (long)videoNumber];
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)fileNameForUserVideoMergeINS
{
    const NSString *key = @"video_merge";
    
    NSString *object = [[self settingsForUserINS] objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

#pragma mark - Get User

#pragma mark Videos

- (NSArray *)videosForUserINS;
{
    const NSString *key = @"videos";
    
    id object = [self objectForKey:key];
    
    if (![object isKindOfClass:[NSArray class]])
    {
        object = nil;
    }
    
    return object;
}

- (NSString *)identifierForVideoINS
{
    const NSString *key = @"id";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSNumber *)slotNumberForVideoINS
{
    const NSString *key = @"count";
    
    NSString *object = [self objectForKey:key];
    
    if (![object isNotEmpty])
    {
        return nil;
    }
    
    NSNumber *slotNumber = @([object integerValue]);
    
    return slotNumber;
}

- (NSDate *)dateAddedForVideoINS
{
    const NSString *key = @"date_added";
    
    NSString *object = [self objectForKey:key];
    NSDate *date = nil;
    
    if ([object isNotEmpty])
    {
        NSDateFormatter *dateFormatter = [[AppDelegate sharedDelegate] serverDateFormatter];
        date = [dateFormatter dateFromString:object];
    }
    
    return date;
}

- (NSString *)videoFileNameForVideoINS
{
    const NSString *key = @"filename";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)thumbnailFileNameForVideoINS
{
    const NSString *key = @"thumb";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)userIDForVideoINS
{
    const NSString *key = @"uid";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

- (NSString *)userNameForVideoINS
{
    const NSString *key = @"uname";
    
    NSString *object = [self objectForKey:key];
    
    return ([object isNotEmpty]) ? object : nil;
}

#pragma mark Kisses

- (NSArray *)kissesForUserINS;
{
    const NSString *key = @"kisses";
    
    id object = [self objectForKey:key];
    
    if (![object isKindOfClass:[NSArray class]])
    {
        object = nil;
    }
    
    return object;
}

- (NSString *)identifierForKissINS
{
    const NSString *key = @"id";
    
    return [self objectForKey:key];
}

- (NSString *)senderIDForKissINS
{
    const NSString *key = @"id";
    
    return [self objectForKey:key];
}

- (NSDate *)dateAddedForKissINS
{
    const NSString *key = @"date_added";
    
    NSString *object = [self objectForKey:key];
    NSDate *date = nil;
    
    if ([object isNotEmpty])
    {
        NSDateFormatter *dateFormatter = [[AppDelegate sharedDelegate] serverDateFormatter];
        date = [dateFormatter dateFromString:object];
    }
    
    return date;
}

#pragma mark Comments

- (NSArray *)commentsForUserINS;
{
    const NSString *key = @"comments";
    
    id object = [self objectForKey:key];
    
    if (![object isKindOfClass:[NSArray class]])
    {
        object = nil;
    }
    
    return object;
}

- (NSString *)identifierForCommentINS
{
    const NSString *key = @"id";
    
    return [self objectForKey:key];
}

- (NSString *)senderIDForCommentINS
{
    const NSString *key = @"sender";
    
    return [self objectForKey:key];
}

- (NSString *)senderUserNameForCommentINS
{
    const NSString *key = @"uname";
    
    return [self objectForKey:key];
}

- (NSString *)avatarFileNameForCommentINS
{
    const NSString *key = @"photo";
    
    return [self objectForKey:key];
}

- (NSString *)bodyForCommentINS
{
    const NSString *key = @"body";
    
    NSString *body = [[self objectForKey:key] stringByDecodingHTMLEntities];
    
    return body;
}

- (NSDate *)dateAddedForCommentINS
{
    const NSString *key = @"date_added";
    
    NSString *object = [self objectForKey:key];
    NSDate *date = nil;
    
    if ([object isNotEmpty])
    {
        NSDateFormatter *dateFormatter = [[AppDelegate sharedDelegate] serverDateFormatter];
        date = [dateFormatter dateFromString:object];
    }
    
    return date;
}

#pragma mark Purchased Features

- (NSDictionary *)purchasedFeaturesForUserINS
{
    const NSString *key = @"features";
    
    id object = [self objectForKey:key];
    
    if (![object isKindOfClass:[NSDictionary class]])
    {
        return nil;
    }
    
    return object;
}

- (BOOL)isUnlimitedCommentsEnabledForUserINS
{
    const NSString *key = @"unlimitedComments";
    
    return ([[[self purchasedFeaturesForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isUnlimitedVideosEnabledForUserINS
{
    const NSString *key = @"videos10";
    
    return ([[[self purchasedFeaturesForUserINS] objectForKey:key] integerValue] == 1);
}

#pragma mark Push Notifications

- (NSDictionary *)pushNotificationSettingsForUserINS
{
    const NSString *key = @"pushNotifications";
    
    id object = [self objectForKey:key];
    
    if (![object isKindOfClass:[NSDictionary class]])
    {
        return nil;
    }
    
    return object;
}

- (BOOL)isPushNotificationsEnabledForUserINS
{
    const NSString *key = @"enabled";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isHeartPushNotificationEnabledForUserINS
{
    const NSString *key = @"heart_pushed";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isWatchedVideoPushNotificationEnabledForUserINS
{
    const NSString *key = @"watched_video";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isKissedPushNotificationEnabledForUserINS
{
    const NSString *key = @"kissed";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isNewCommentPushNotificationEnabledForUserINS
{
    const NSString *key = @"new_comment";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isNewAmourPushNotificationEnabledForUserINS
{
    const NSString *key = @"new_amour";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isChatPushNotificationEnabledForUserINS
{
    const NSString *key = @"instant_chat";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isVideoPushNotificationEnabledForUserINS
{
    const NSString *key = @"video_call";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isAudioPushNotificationEnabledForUserINS
{
    const NSString *key = @"phone_call";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

- (BOOL)isReceivedGiftPushNotificationEnabledForUserINS
{
    const NSString *key = @"received_gift";
    
    return ([[[self pushNotificationSettingsForUserINS] objectForKey:key] integerValue] == 1);
}

#pragma mark Search

- (NSArray *)usersINS;
{
    const NSString *key = @"users";
    
    return [self objectForKey:key];
}


@end
