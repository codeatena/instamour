//
//  INSHomeViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIArrayCollectionViewController.h"

// Public Constants

// Protocols

@interface INSHomeViewController : BTIArrayCollectionViewController

// Public Properties

// Public Methods

@end
