package com.hyper.instamour;

import android.annotation.SuppressLint;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;

public class AboutInstamour extends Activity {
	private WebView webView;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			new VideoSetting(AboutInstamour.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.browser_activity);
		Function c = new Function();
		if (c.haveNetworkConnection(getApplicationContext())) {
			webView = (WebView) findViewById(R.id.webView1);
			webView.getSettings().setJavaScriptEnabled(true);
			webView.getSettings().setAppCacheEnabled(true);
			webView.getSettings()
					.setJavaScriptCanOpenWindowsAutomatically(true);
			webView.loadUrl("http://www.instamour.com/corporate/about.htm");
		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(
					AboutInstamour.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancel:
			finish();
			break;

		}
	}

}
