//
//  INSPickerInputView.h
//  Instamour
//
//  Created by Brian Slick on 7/14/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString *const INSPickerInputViewBlankValue;

typedef NS_ENUM(NSInteger, INSPickerInputViewMode) {
    INSPickerInputViewModeAllButtons,
    INSPickerInputViewModeNoButtons,
    INSPickerInputViewModeDoneButtonOnly,
};

@protocol INSPickerInputViewDelegate;

@interface INSPickerInputView : UIView

// Public Properties
@property (nonatomic, weak) id<INSPickerInputViewDelegate> delegate;
@property (nonatomic, assign, getter = isPreventingBlankRowSelection) BOOL preventBlankRowSelection;

// Public Methods
+ (instancetype)pickerInputViewMode:(INSPickerInputViewMode)mode delegate:(id<INSPickerInputViewDelegate>)delegate;

- (void)setPreviousButtonEnabled:(BOOL)isEnabled;
- (void)setNextButtonEnabled:(BOOL)isEnabled;

- (void)setPickerViewContents:(NSArray *)contents forComponent:(NSInteger)component preselectingRow:(NSInteger)row;   // Use NSNotFound for no row preselection
- (void)reloadPickerView;
- (void)removeContents;

@end

@protocol INSPickerInputViewDelegate <NSObject>

@required
- (void)pickerInputView:(INSPickerInputView *)pickerInputView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;

@optional
- (void)pickerInputViewDoneButtonPressed:(INSPickerInputView *)pickerInputView;
- (void)pickerInputViewPreviousButtonPressed:(INSPickerInputView *)pickerInputView;
- (void)pickerInputViewNextButtonPressed:(INSPickerInputView *)pickerInputView;

@end
