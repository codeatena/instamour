//
//  INSTextFieldTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSLabelAndTextFieldTableCell.h"

@interface INSLabelAndTextFieldTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end

@implementation INSLabelAndTextFieldTableCell

#pragma mark - Misc Methods

- (void)setLabelText:(NSString *)text
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (![text isNotEmpty])
    {
        text = @" ";
    }
    
    UILabel *label = [self titleLabel];
    UITextField *textField = [self textField];
    UIView *contentView = [self contentView];
    
    [label setText:text];
    [label sizeToFit];
    
    CGFloat margin = 10.0;
    
    CGFloat x = CGRectGetMaxX([label frame]) + margin;
    CGFloat width = CGRectGetWidth([contentView bounds]) - x- margin;
    
    CGRect frame = [textField frame];
    frame.origin.x = x;
    frame.size.width = width;
    [textField setFrame:frame];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
