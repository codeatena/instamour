//
//  INSChatTableHeaderView.m
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatTableHeaderView.h"

@implementation INSChatTableHeaderView

- (void)awakeFromNib
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self contentView] setBackgroundColor:[UIColor redColor]];
//    [self setBackgroundColor:[UIColor redColor]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
