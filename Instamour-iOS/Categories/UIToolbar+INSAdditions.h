//
//  UIToolbar+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/23/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIToolbar (INSAdditions)

- (void)applyGlobalStyleINS;

@end
