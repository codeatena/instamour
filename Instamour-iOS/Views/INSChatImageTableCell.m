//
//  INSChatImageTableCell.m
//  Instamour
//
//  Created by Brian Slick on 8/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatImageTableCell.h"

@interface INSChatImageTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UIImageView *playImageView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, strong) IBOutlet UIButton *shareButton;

@end

@implementation INSChatImageTableCell

- (void)setPlayImageHidden:(BOOL)isHidden
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self playImageView] setHidden:isHidden];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)shareButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([[self delegate] respondsToSelector:@selector(chatImageTableCellShareButtonPressed:)])
    {
        [[self delegate] chatImageTableCellShareButtonPressed:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setBusy:(BOOL)isBusy
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (isBusy)
    {
        [[self activityView] startAnimating];
        [[self shareButton] setHidden:YES];
    }
    else
    {
        [[self activityView] stopAnimating];
        [[self shareButton] setHidden:NO];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}


@end
