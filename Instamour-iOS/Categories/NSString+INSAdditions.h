//
//  NSString+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/4/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (INSAdditions)

- (BOOL)isNotEmpty;
- (BOOL)isValidEmailAddress;

+ (NSString *)heartsPushedSitTightMessageForRecipientGenderINS:(NSString *)gender;
+ (NSString *)pendingAmoursEncouragementMessageForRecipientGenderINS:(NSString *)gender;

@end
