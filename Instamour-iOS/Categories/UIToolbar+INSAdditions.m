//
//  UIToolbar+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/23/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "UIToolbar+INSAdditions.h"

@implementation UIToolbar (INSAdditions)

- (void)applyGlobalStyleINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([AppDelegate isiOS7OrNewer])
    {
        [self setBarStyle:UIBarStyleDefault];
        [self setTranslucent:NO];
    }
    else
    {
        [self setBarStyle:UIBarStyleDefault];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
