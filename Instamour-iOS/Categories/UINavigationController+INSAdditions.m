//
//  UINavigationController+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "UINavigationController+INSAdditions.h"

@implementation UINavigationController (INSAdditions)

- (void)applyGlobalStyleINS
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self navigationBar] applyGlobalStyleINS];
    [[self toolbar] applyGlobalStyleINS];
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        [[self interactivePopGestureRecognizer] setEnabled:NO];
    }
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
