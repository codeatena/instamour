package com.hyper.instamour;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("NewApi")
public class VideoRecordDemoActivity extends Activity implements
		OnClickListener, SurfaceHolder.Callback {
	/** Called when the activity is first created. */

	MediaRecorder recorder;
	SurfaceHolder holder;
	SurfaceView cameraView;
	private TextView timeElapsed1;
	boolean recording = false;
	public static String mFileName = null;
	private ImageView record, stop, switch_camera;
	private TextView save, cancel;
	Global global;
	private Camera cam;
	private CamcorderProfile camcorderProfile;
	// boolean recording = false;
	boolean usecamera = true;
	boolean previewRunning = false;

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		try {
			recorder.reset();
			recorder.release();
			cam.stopPreview();
			cam.release();
			// cam = null;

		} catch (IllegalStateException e) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		global.From_create_video = false;

		// finish();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*
		 * getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		 * WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 */
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.main);
		Runtime.getRuntime().totalMemory();
		Runtime.getRuntime().freeMemory();

		camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		global.editor = global.preferences.edit();
		recorder = new MediaRecorder();

		// VideoRecordTest();
		// initRecorder();
		timeElapsed1 = (TextView) findViewById(R.id.timeElapsed);
		stop = (ImageView) findViewById(R.id.stop);
		record = (ImageView) findViewById(R.id.recordtogglebutton);
		switch_camera = (ImageView) findViewById(R.id.switch_camera);
		record = (ImageView) findViewById(R.id.record);
		save = (TextView) findViewById(R.id.save);
		cancel = (TextView) findViewById(R.id.cancel);
		cameraView = (SurfaceView) findViewById(R.id.CameraView);

		holder = cameraView.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		cameraView.setClickable(true);
		cameraView.setOnClickListener(this);

		Log.e("oncreate finished", "oncreate finished");

	}

	/*
	 * private void initRecorder() {
	 * recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
	 * recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
	 * recorder.setOrientationHint(90);
	 * 
	 * recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
	 * recorder.setAudioEncoder(AudioEncoder.AMR_NB);
	 * recorder.setVideoEncoder(VideoEncoder.H264);
	 * recorder.setVideoFrameRate(15); recorder.setVideoSize(720, 1280);
	 * 
	 * recorder.setOutputFile(mFileName); recorder.setMaxDuration(8000); // 8
	 * seconds recorder.setMaxFileSize(5000000); // Approximately 5 megabytes //
	 * recorder.start(); // prepareRecorder(); }
	 */

	private void prepareRecorder() {

		try {

			recorder.setPreviewDisplay(holder.getSurface());
			if (usecamera) {
				cam.unlock();
				recorder.setCamera(cam);
			}

			recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			int rotation = getPreviewOrientation(VideoRecordDemoActivity.this,
					0);
			recorder.setOrientationHint(rotation);
			recorder.setMaxDuration(9000);
			// recorder.setOrientationHint(90);
			recorder.setProfile(camcorderProfile);

			// recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			// recorder.setAudioEncoder(AudioEncoder.AMR_NB);
			// recorder.setVideoEncoder(VideoEncoder.H264);
			// recorder.setVideoFrameRate(15);
			// recorder.setVideoSize(480, 480);
			VideoRecordTest();
			recorder.setOutputFile(mFileName);

			recorder.prepare();

			// recorder.start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
			finish();
		} catch (IOException e) {
			e.printStackTrace();
			finish();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void surfaceCreated(SurfaceHolder holder) {
		Log.e("Surface created ", "suraface created");
		if (usecamera) {
			cam = Camera.open();

			try {
				cam.setPreviewDisplay(holder);
				cam.startPreview();
				previewRunning = true;
				cam.setDisplayOrientation(90);
			} catch (IOException e) {
				Log.e("errrrrrr", e.getMessage());
				e.printStackTrace();
			}
		}
		/*
		 * try { cam = Camera.open(); cam.setPreviewDisplay(holder); //
		 * holder.setKeepScreenOn(true); cam.setDisplayOrientation(90); //
		 * prepareRecorder(); // recorder.prepare(); // prepareRecorder(); }
		 * catch (IOException exception) { cam.release(); cam = null; } catch
		 * (IllegalStateException e) { // TODO: handle exception } catch
		 * (RuntimeException e) { // TODO: handle exception }
		 */
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		Log.e("Surface changed ", "surface changed");
		if (!recording && usecamera) {
			if (previewRunning) {
				cam.stopPreview();
			}

			try {
				Camera.Parameters p = cam.getParameters();
				int width1 = p.getPictureSize().width;
				int height1 = p.getPreviewSize().height;
				Log.e("width", width1 + "-" + height1);
				cameraView.getLayoutParams().width = p.getPictureSize().width;
				cameraView.getLayoutParams().height = p.getPictureSize().height;
				p.setPreviewSize(camcorderProfile.videoFrameWidth,
						camcorderProfile.videoFrameHeight);
				p.setPreviewFrameRate(camcorderProfile.videoFrameRate);
				// p.setRotation(180);

				cam.setParameters(p);

				cam.setPreviewDisplay(holder);
				cam.startPreview();
				previewRunning = true;
			} catch (IOException e) {
				Log.e("", e.getMessage());
				e.printStackTrace();
			} catch (RuntimeException e) {
				// TODO: handle exception
			}

			prepareRecorder();
		}
		/*
		 * try { cam.startPreview(); } catch (IllegalStateException e) { //
		 * TODO: handle exception } catch (RuntimeException e) { // TODO: handle
		 * exception } catch (Exception e) { // TODO: handle exception
		 * Log.e("camera errror", "errrrroooorrrrr"); }
		 */

	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.e("surface destroyed", "surfaceDestroyed");
		if (recording) {
			// recorder.stop();
			recording = false;
		}
		// recorder.release();
		if (usecamera) {
			previewRunning = false;
			// camera.lock();
			// cam.release();
		}
		finish();
		/*
		 * try { Log.e("ssssss", "surface destroyyyyy");
		 * 
		 * recorder.stop(); recorder.reset(); recorder.release(); recorder =
		 * null;
		 * 
		 * // timeElapsed1.setText("--:--"); recording = false; } catch
		 * (IllegalStateException e) { // TODO: handle exception } catch
		 * (Exception e) { // TODO: handle exception }
		 */

		/*
		 * recorder.release(); finish();
		 */
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.save:
			global.From_create_video = true;
			global.back_camera = true;
			finish();
			break;
		case R.id.cancel:
			try {
				/*
				 * cam.stopPreview(); cam.release(); cam = null;
				 * recorder.stop(); recorder.release();
				 */
				timer.cancel();

			} catch (IllegalStateException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			}
			global.From_create_video = false;
			// timeElapsed1.setText("--:--");
			/*
			 * recorder.release(); recorder.reset();
			 */
			finish();
			break;

		case R.id.back:
			try {
				/*
				 * cam.stopPreview(); cam.release(); cam = null;
				 * recorder.stop(); recorder.release();
				 */
			} catch (IllegalStateException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			}
			global.From_create_video = false;
			// timeElapsed1.setText("--:--");
			/*
			 * recorder.release(); recorder.reset();
			 */
			finish();
			break;
		case R.id.record:

			/*
			 * if (recording) { try { Log.e("btn stop", "btn stop");
			 * record.setImageResource(R.drawable.btn1); recorder.stop();
			 * 
			 * recorder.release(); recorder.reset();
			 * 
			 * timeElapsed1.setText("--:--");
			 * 
			 * recording = false;
			 * 
			 * } catch (IllegalStateException e) { e.printStackTrace();
			 * finish(); } catch (Exception e) { e.printStackTrace(); finish();
			 * } // recorder.release();
			 * 
			 * // Let's initRecorder so we can record again initRecorder();
			 * prepareRecorder(); } else {
			 */
			/*
			 * try { // Log.e("btn record","btn record");
			 * 
			 * record.setVisibility(View.GONE); switch_camera.setEnabled(false);
			 * stop.setVisibility(View.VISIBLE);
			 * 
			 * // cancel.setVisibility(View.VISIBLE); timer.start();
			 * cam.stopPreview(); cam.unlock(); recorder.setCamera(cam);
			 * initRecorder(); recorder.setPreviewDisplay(holder.getSurface());
			 * recorder.prepare(); recorder.start();
			 * 
			 * recording = true; // global.From_create_video = true;
			 * 
			 * } catch (IllegalStateException e) { e.printStackTrace();
			 * finish(); } catch (Exception e) { e.printStackTrace(); finish();
			 * }
			 * 
			 * }
			 */

			recording = true;
			recorder.start();
			try {
				Log.e("btn record", "btn record");

				record.setVisibility(View.GONE);
				switch_camera.setEnabled(false);
				stop.setVisibility(View.VISIBLE);

				// cancel.setVisibility(View.VISIBLE);
				timer.start();
			} catch (Exception e) {
				// TODO: handle exception
			}
			Log.e("", "Recording Started");

			break;
		case R.id.switch_camera:
			try {
				recorder.reset();
				recorder.release();
				cam.stopPreview();
				cam.release();
				cam = null;
				Intent intent = new Intent(VideoRecordDemoActivity.this,
						Cemara.class);
				startActivity(intent);

				finish();
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("errrrr", "rrrrrrrrr");
			}
			break;
		case R.id.stop:
			try {

				recorder.stop();
				recorder.release();
				cam.stopPreview();
				cam.release();
				cam = null;

				cancel.setVisibility(View.GONE);
			} catch (IllegalStateException e) {
				// TODO: handle exception
			} catch (RuntimeException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			}

			timer.cancel();
			save.setVisibility(View.VISIBLE);
			record.setVisibility(View.VISIBLE);
			stop.setVisibility(View.GONE);
			record.setEnabled(false);

			timeElapsed1.setText("Cancel");
			timeElapsed1.setTextColor(Color.WHITE);
			timeElapsed1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					global.From_create_video = false;
					finish();
				}
			});
			break;

		}

	}

	public void VideoRecordTest() {
		File file;
		if (Build.VERSION.SDK_INT < 19) {
			Log.e("Build version", Build.VERSION.SDK_INT + "-");
			File directory = new File(Environment.getExternalStorageDirectory()
					+ "/instamour");
			if (!directory.exists()) {
				directory.mkdir();
			}
			// Now create the file in the above directory and write the
			// contents
			// into it
			long seconds = System.currentTimeMillis() / 1000l;
			Date lm = new Date();
			String lasmod = new SimpleDateFormat("yyyyMMdd").format(lm);
			String videoname = lasmod + String.valueOf(seconds) + "_video"
					+ ".mp4";
			file = new File(directory, videoname);
		} else {
			Log.e("Build version", Build.VERSION.SDK_INT + "-");
			File directory = new File("/storage/emulated/0" + "/instamour");
			if (!directory.exists()) {
				directory.mkdir();
			}
			// Now create the file in the above directory and write the
			// contents
			// into it
			long seconds = System.currentTimeMillis() / 1000l;
			Date lm = new Date();
			String lasmod = new SimpleDateFormat("yyyyMMdd").format(lm);
			String videoname = lasmod + String.valueOf(seconds) + "_video"
					+ ".mp4";
			file = new File(directory, videoname);
		}

		mFileName = file.getPath();
		global.From_create_video = true;

	}

	/**
	 * CountDownTimer using for counting elapsed time of media file
	 */
	private CountDownTimer timer = new CountDownTimer(9000, 1000) {
		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
			try {
				timeElapsed1.setText(countTime(millisUntilFinished));
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			// stopRecording();
			try {
				save.setVisibility(View.VISIBLE);
				recorder.stop();
				recorder.release();
				cam.stopPreview();
				cam.release();
				cam = null;

				timeElapsed1.setText("--:--");
				cancel.setVisibility(View.GONE);
			} catch (IllegalStateException e) {
				// TODO: handle exception
			} catch (RuntimeException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			}
			// forwardscreen();
		}
	};

	/**
	 * Convert time from milliseconds into minutes and seconds, proper to media
	 * player
	 * 
	 * @param miliseconds
	 *            media content time in milliseconds
	 * @return time in format minutes:seconds
	 */
	private String countTime(long miliseconds) {
		String timeInMinutes = new String();
		long minutes = miliseconds / 60000;
		long seconds = (miliseconds % 60000) / 1000;
		timeInMinutes = minutes + ":"
				+ (seconds < 10 ? "0" + seconds : seconds);
		return timeInMinutes;
	}

	// Calculate the device orientation:
	public int getDeviceOrientation(Context context) {

		int degrees = 0;
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		int rotation = windowManager.getDefaultDisplay().getRotation();

		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		return degrees;
	}

	// Calculate Camera preview rotation:
	public int getPreviewOrientation(Context context, int cameraId) {

		int temp = 0;
		int previewOrientation = 0;

		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, cameraInfo);

		int deviceOrientation = getDeviceOrientation(context);
		temp = cameraInfo.orientation - deviceOrientation + 360;
		previewOrientation = temp % 360;

		return previewOrientation;
	}
}
