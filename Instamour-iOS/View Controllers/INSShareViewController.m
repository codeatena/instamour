//
//  INSShareViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/5/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSShareViewController.h"

// Models and other global
#import <MessageUI/MessageUI.h>

// Sub-controllers

// Views

// Private Constants


@interface INSShareViewController () <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

// Private Properties

@end

@implementation INSShareViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Share Instamour"];
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    
    // User Interface
    
    [self populateContents];
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    __weak INSShareViewController *weakSelf = self;
    
    BTITableContentsManager *manager = [self mainContentsManager];
    [manager reset];
    
    BTITableRowInfo *rowInfo = nil;
    
    {{  // Facebook
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setText:@"Share on Facebook"];
        [rowInfo setRowSelectionBlock:^{
            
            [weakSelf showSharePanel];
            
        }];
    }}
    
    {{  // Twitter
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setText:@"Share on Twitter"];
        [rowInfo setRowSelectionBlock:^{
            
            [weakSelf showSharePanel];
            
        }];
    }}
    
    if ([MFMailComposeViewController canSendMail])
    {
        {{  // Email
            rowInfo = [manager dequeueReusableRowInfo];
            [manager addRowInfo:rowInfo makeNewSection:NO];
            
            [rowInfo setText:@"Share by Email"];
            [rowInfo setRowSelectionBlock:^{
                
                [weakSelf sendEmail];
                
            }];
        }}
    }
    
    if ([MFMessageComposeViewController canSendText])
    {
        {{  // SMS
            rowInfo = [manager dequeueReusableRowInfo];
            [manager addRowInfo:rowInfo makeNewSection:NO];
            
            [rowInfo setText:@"Share by SMS"];
            [rowInfo setRowSelectionBlock:^{
                
                [weakSelf sendTextMessage];
                
            }];
        }}
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides


#pragma mark - Notification Handlers


#pragma mark - UI Response Methods


#pragma mark - Misc Methods

- (void)sendEmail
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
    [composer setMailComposeDelegate:self];
    
    [composer setSubject:@"Check out Instamour"];
    
    NSMutableString *body = [NSMutableString string];
    // add HTML before the link here with line breaks (\n)
    [body appendString:@"<p>Hey! Check out Instamour, the hottest new video dating app that lets you chat, video call and meet people faster than ever! </p>\n"];
    [body appendString:@"<a href=\"http://apple.instamour.com\">Instamour on the App Store!</a>\n"];
    
    [composer setMessageBody:body isHTML:YES];
    
    [self presentViewController:composer
                       animated:YES
                     completion:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendTextMessage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MFMessageComposeViewController *composer = [[MFMessageComposeViewController alloc] init];
    [composer setMessageComposeDelegate:self];
    
    [composer setBody:@"Hey! Check out the hottest new video dating app that lets you chat, video call and meet people faster than ever!\nhttp://apple.instamour.com"];
    
    __weak INSShareViewController *weakSelf = self;
    
    [self presentViewController:composer
					   animated:YES
					 completion:^{
						 
						 if ([[composer viewControllers] lastObject] == nil)
						 {
							 [weakSelf dismissViewControllerAnimated:YES
                                                          completion:^{
                                                              
                                                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                              message:@"Sorry, this device is not configured to send this message. Please verify settings and try again."
                                                                                                             delegate:nil
                                                                                                    cancelButtonTitle:@"Ok"
                                                                                                    otherButtonTitles:nil];
                                                              [alert show];
                                                              
                                                          }];
						 }
						 
					 }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showSharePanel
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *text = @"Hey everyone! Check out the hottest new video dating app that lets you chat, video call and meet people faster than ever! #Instamour";
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[ text ]
                                                                                         applicationActivities:nil];
    [activityViewController setExcludedActivityTypes:@[ UIActivityTypeMessage, UIActivityTypeMail ]];
    
    [self presentViewController:activityViewController
                       animated:YES
                     completion:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContentsManager] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    static NSString *shareCellIdentifier = @"shareCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:shareCellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shareCellIdentifier];
        
        [cell setBackgroundColor:[UIColor clearColor]];
        [[cell contentView] setBackgroundColor:[UIColor clearColor]];
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        [[cell textLabel] setTextColor:[UIColor darkGrayColor]];
    }
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    
    [[cell textLabel] setText:[rowInfo text]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];

    [rowInfo safelyPerformRowSelectionBlock];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - MFMailComposeViewControllerDelegate Methods

- (void)mailComposeController:(MFMailComposeViewController *)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError *)error
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
	[self dismissViewControllerAnimated:YES
							 completion:nil];
    
	if (result == MFMailComposeResultFailed)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can not send mail"
														message:@"Sorry, we were unable to send the email."
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
	}
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - MFMessageComposeViewControllerDelegate Methods

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
    if (result == MessageComposeResultFailed)
    {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can not send message"
														message:@"Sorry, we were unable to send the message."
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
	}

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
