//
//  INSChatAmoursTableCell.m
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatAmoursTableCell.h"

@interface INSChatAmoursTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *countLabel;

@end

@implementation INSChatAmoursTableCell

- (void)awakeFromNib
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
        
    UILabel *countLabel = [self countLabel];
    [countLabel setBackgroundColor:[UIColor instamourPrimaryBackgroundRedColorINS]];
    [countLabel setTextColor:[UIColor whiteColor]];
    
    CALayer *countLabelLayer = [countLabel layer];
    [countLabelLayer setCornerRadius:2.0];
    [countLabelLayer setMasksToBounds:YES];
    
    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setCount:(NSInteger)count;
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UILabel *countLabel = [self countLabel];
    
    [countLabel setHidden:(count == 0)];
    
    [countLabel setText:[NSString stringWithFormat:@"%ld", (long)count]];
    
    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
