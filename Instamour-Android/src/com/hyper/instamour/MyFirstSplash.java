
package com.hyper.instamour;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;

import com.google.gson.JsonSyntaxException;
import com.han.utility.PreferenceUtility;
import com.quickblox.core.QBCallback;
import com.quickblox.core.QBSettings;
import com.quickblox.core.result.Result;
import com.quickblox.module.auth.QBAuth;
import com.quickblox.module.chat.QBChatService;
import com.quickblox.module.chat.listeners.SessionListener;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.service.QBVideoChatService;
import com.quickblox.module.videochat.model.objects.CallType;
import com.quickblox.module.videochat.model.objects.VideoChatConfig;

public class MyFirstSplash extends Activity implements QBCallback {
    private int delay = 1000;
    Global global;
    static String Preversion;
    Function c;
    QBUser qbUser;
    private VideoChatConfig videoChatConfig;

    public void onCreate(Bundle savedInstanceState) throws JsonSyntaxException {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        
    	PreferenceUtility.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_splash);
        try {
            global = Global.getInstance();
            global.preferences = getSharedPreferences("LoginPrefrence", MODE_PRIVATE);

            global.editor = global.preferences.edit();

            try {
                Preversion = loadversion();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        Log.e("previous version", Preversion);

                    }
                });
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            String pkg = getApplicationContext().getPackageName();
            String mVersionNumber = getApplicationContext().getPackageManager()
                    .getPackageInfo(pkg, 0).versionName;
            Log.e("version number", mVersionNumber + "");

            if (Preversion.equalsIgnoreCase(mVersionNumber)) {

                Log.e("version same", "version same");

            } else {
                global.editor.clear();
                global.editor.commit();
                try {
                    writefile(mVersionNumber);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Log.e("different version data cleared",
                        "Different version data cleared ");

            }

        } catch (NumberFormatException e) {
            String mVersionNumber = "?";
            e.printStackTrace();
            Log.e("in catch", "in catch1");
        } catch (NameNotFoundException e) {
            // TODO: handle exception
            e.printStackTrace();
            Log.e("in catch", "in catch2");
        }
        c = new Function();
        if (c.haveNetworkConnection(MyFirstSplash.this)) {

            Log.e("before  : ", "before");
            try {
                QBSettings.getInstance().fastConfigInit(
                        getResources().getString(R.string.quickblox_app_id),
                        getResources().getString(R.string.quickblox_auth_key),
                        getResources()
                                .getString(R.string.quickblox_auth_secret));

                QBAuth.createSession(this);
            } catch (JsonSyntaxException e) {
                Log.e("jsonsysntaxexception", "QB ERROR");
            }
        } else {
            AlertDialog.Builder dialog2 = new AlertDialog.Builder(
                    MyFirstSplash.this);
            dialog2.setTitle("Instamour");
            dialog2.setMessage("No Data Connection Avaible");
            dialog2.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            finish();
                        }
                    });
            AlertDialog alert2 = dialog2.create();
            alert2.show();

        }

    }

    @Override
    public void onComplete(Result result) {
        // TODO Auto-generated method stub
        Log.e("Raw data : ", "------->" + result.getRawBody());

        if (result.isSuccess()) {

            try {
                /*
                 * GCMIntentService intentService=new GCMIntentService();
                 * intentService.startService(getIntent());
                 */

                Timer timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {

                        if (global.preferences.getString("session_login", "")
                                .equalsIgnoreCase("true")) {
                            Intent intent;
                            int isNotify = getIntent().getIntExtra("isNotify", 0);
                            if (isNotify == 1)
                            {
                                intent = new Intent(getApplicationContext(),
                                        InstantChat.class);
                            } else
                            {
                                intent = new Intent(getApplicationContext(),
                                        SampleActivity.class);
                            }

                            Log.e("sessionlogin", "sessionlogin true");

                            global.setFriendjabberid(global.preferences
                                    .getString("friendjabberid", ""));
                            global.setFriendname(global.preferences.getString(
                                    "friendname", ""));

                            global.setFriendphoto(global.preferences.getString(
                                    "friendphoto", ""));

                            global.from_fb = global.preferences.getBoolean(
                                    "from_fb", false);
                            global.setPassword(global.preferences.getString(
                                    "password", ""));
                            global.setJabberid(global.preferences.getString(
                                    "jabberId", ""));
                            Log.e("jfdljdfldjfjjd", global.from_fb + "");
                            if (global.from_fb) {
                                Log.e("fb username", global.preferences
                                        .getString("fbuname", ""));
                                global.setFbLoginUsername(global.preferences
                                        .getString("fbuname", ""));

                            } else {

                                qbUser = new QBUser(global.getLoginusername(),
                                        "instamourapp");

                            }
                            global.setLoginusername(global.preferences
                                    .getString("uname", ""));

                            global.is_phone_checked = global.preferences
                                    .getBoolean("is_phone_checked", false);
                            global.is_video_checked = global.preferences
                                    .getBoolean("is_video_checked", false);
                            global.is_account_checked = global.preferences
                                    .getBoolean("is_account_checked", true);

                            global.editor = global.preferences.edit();
                            global.editor.putString("session_login", "true");
                            global.editor.commit();

                            QBChatService.getInstance().loginWithUser(qbUser,
                                    new SessionListener() {

                                        @Override
                                        public void onLoginSuccess() {
                                            QBChatService.getInstance().startAutoSendPresence(2000);

                                        }

                                        @Override
                                        public void onLoginError() {
                                            Log.e("error", "errrorr");
                                        }

                                        @Override
                                        public void onDisconnectOnError(Exception arg0) {

                                        }

                                        @Override
                                        public void onDisconnect() {

                                        }
                                    });

                            startActivity(intent);
                            finish();

                        } else {

                            Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                            global.editor = global.preferences.edit();
                            global.editor.putString("session_login", "false");
                            global.editor.commit();
                            startActivity(intent);
                            finish();
                        }

                    }

                };

                timer.schedule(task, delay);
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("error", result.getErrors() + "");
            }
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Error occurred. " + result.getErrors()).create()
                    .show();

            try {
                /*
                 * GCMIntentService intentService=new GCMIntentService();
                 * intentService.startService(getIntent());
                 */

                Timer timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        if (global.preferences.getString("session_login", "")
                                .equalsIgnoreCase("true")) {
                            Intent intent = new Intent(getApplicationContext(),
                                    SampleActivity.class);
                            Log.e("sessionlogin", "sessionlogin true");

                            global.setFriendjabberid(global.preferences
                                    .getString("friendjabberid", ""));
                            global.setFriendname(global.preferences.getString(
                                    "friendname", ""));

                            global.setFriendphoto(global.preferences.getString(
                                    "friendphoto", ""));

                            global.from_fb = global.preferences.getBoolean(
                                    "from_fb", false);
                            global.setPassword(global.preferences.getString(
                                    "password", ""));
                            global.setJabberid(global.preferences.getString(
                                    "jabberId", ""));
                            Log.e("jfdljdfldjfjjd", global.from_fb + "");
                            if (global.from_fb) {
                                Log.e("fb username", global.preferences
                                        .getString("fbuname", ""));
                                global.setFbLoginUsername(global.preferences
                                        .getString("fbuname", ""));

                            } else {
                                global.setLoginusername(global.preferences
                                        .getString("uname", ""));
                                qbUser = new QBUser(global.getLoginusername(),
                                        global.getPassword());

                            }

                            global.is_phone_checked = global.preferences
                                    .getBoolean("is_phone_checked", false);
                            global.is_video_checked = global.preferences
                                    .getBoolean("is_video_checked", false);
                            global.is_account_checked = global.preferences
                                    .getBoolean("is_account_checked", true);

                            global.editor = global.preferences.edit();
                            global.editor.putString("session_login", "true");
                            global.editor.commit();

                            QBChatService.getInstance().loginWithUser(qbUser,
                                    new SessionListener() {

                                        @Override
                                        public void onLoginSuccess() {
                                            // TODO Auto-generated method stub
                                            QBChatService
                                                    .getInstance()
                                                    .startAutoSendPresence(2000);

                                        }

                                        @Override
                                        public void onLoginError() {
                                            // TODO Auto-generated method stub
                                            Log.e("error", "errrorr");
                                        }

                                        @Override
                                        public void onDisconnectOnError(
                                                Exception arg0) {
                                            // TODO Auto-generated method stub

                                        }

                                        @Override
                                        public void onDisconnect() {
                                            // TODO Auto-generated method stub

                                        }
                                    });

                            startActivity(intent);
                            finish();

                        } else {

                            Intent intent = new Intent(getApplicationContext(),
                                    HomeScreen.class);

                            global.editor = global.preferences.edit();
                            global.editor.putString("session_login", "false");
                            global.editor.commit();

                            startActivity(intent);
                            finish();
                        }

                    }

                };

                timer.schedule(task, delay);
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("error", result.getErrors() + "");
            }

        }

    }

    @Override
    public void onComplete(Result arg0, Object arg1) {
        // TODO Auto-generated method stub

    }

    @SuppressLint("NewApi")
    public String loadversion() throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        InputStreamReader reader = new InputStreamReader(getAssets().open(
                "version.txt"), "UTF-8");
        BufferedReader br = new BufferedReader(reader);
        String line = br.readLine();
        return line;
    }

    @SuppressLint("NewApi")
    public void writefile(String data) throws IOException {

        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        /*
         * AssetFileDescriptor descriptor = getAssets().openFd("version.txt");
         * FileReader reader = new FileReader(descriptor.getFileDescriptor());
         * FileWriter fileWriter=new FileWriter(descriptor.getFileDescriptor());
         */
        /*
         * Writer writer = new StringWriter(); BufferedReader br = new
         * BufferedReader(new InputStreamReader(
         * getResources().getAssets().open("version.txt"), "UTF-8")); char[]
         * buffer = new char[2048]; try { int n; while ((n = br.read(buffer)) !=
         * -1) { writer.write(data); } } catch (Exception e) { // TODO: handle
         * exception } String text = writer.toString(); Log.e("text",text);
         * return text;
         */
        Uri uri = Uri.fromFile(new File("//assets/version.txt"));
        Writer writer = new StringWriter();
        FileReader fileReader = new FileReader(uri.toString());
        FileWriter fileWriter = new FileWriter(uri.toString());

        fileWriter.write(data);
        fileWriter.close();
    }
}
