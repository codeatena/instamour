package com.hyper.instamour;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.hyper.instamour.messages.c2dm.C2DMessaging;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class LogoutActivity extends Activity {
	private List<NameValuePair> param;
	private Global global;
	private Function c;
	String uid;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		global = Global.getInstance();
		c = new Function();
		uid = global.preferences.getString("uid", "");
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "logout"));
		param.add(new BasicNameValuePair("sessionId", uid));
		try {

			JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
			Log.e("login parameter", json + "");
		} catch (Exception e) {
			// TODO: handle exception
		}
		global.editor = global.preferences.edit();

		global.editor.clear();
		global.editor.commit();
		global.from_next = false;
		global.is_account_checked = false;
		global.is_phone_checked = false;
		global.is_searched = false;
		global.chat_image = false;
		global.is_video_checked = false;
		global.from_fb = false;
		global.from_audio = false;
		global.from_edit = false;
		global.setJabberid("");
		global.setLoginusername("");

		global.setPhoto("");
		global.setPassword("");
		global.setQbuser(null);

		Intent intent = new Intent(LogoutActivity.this, HomeScreen.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);

		finish();
		C2DMessaging.unregister(LogoutActivity.this);
	}

}
