//
//  INSVideoListViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTITableViewController.h"

// Public Constants

// Protocols

@interface INSVideoListViewController : BTITableViewController

// Public Properties

// Public Methods

@end
