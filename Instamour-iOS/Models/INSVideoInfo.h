//
//  INSVideoInfo.h
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Classes and Forward Declarations
#import "BTIObject.h"

// Public Constants
typedef NS_ENUM(NSInteger, INSVideoInfoState) {
    INSVideoInfoStateNormal,
    INSVideoInfoStateTrimming,
    INSVideoInfoStateUploading,
    INSVideoInfoStateDeleting
};

typedef void (^INSVideoInfoThumbnailDownloadCompletionBlock)(UIImage *image);

@interface INSVideoInfo : BTIObject <NSCoding>

// Public Properties

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSNumber *serverSlotNumber;     // 0 == the merged video
@property (nonatomic, copy) NSDate *dateAdded;
@property (nonatomic, copy) NSString *videoFileName;
@property (nonatomic, copy) NSString *thumbnailFileName;
@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSString *userName;

@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic, copy) INSVideoInfoThumbnailDownloadCompletionBlock thumbnailDownloadCompletionBlock;

@property (nonatomic, assign) INSVideoInfoState state;

// Public Methods

- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDictionary;

- (BOOL)isPopulated;

- (BOOL)isMergedVideo;     // 0 == the merged video

//- (NSURL *)localFileURL;
- (NSURL *)serverVideoFileURL;
//- (NSURL *)preferredFileURL;

@end
