package com.hyper.instamour.model;

import android.net.Uri;

public class CreateVideoData {
    public Uri videoFileUri = null;
    
    public String videoUrl = null;
    public String thumbUrl = null;
}