package com.han.utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class CameraUtility {
	
	private static final String IMAGE_DIRECTORY_NAME = "Instamour_Image";
	private static final String VIDEO_DIRECTORY_NAME = "Instamour_Video";

	public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    
	public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    
    public static String PHOTO_FILE_NAME;
    
    private static final String JPEG_FILE_SUFFIX = ".jpg";

	public static Uri getOutputMediaFileUri(int type) {
	    return Uri.fromFile(getOutputMediaFile(type));
	}
	
	public static File getOutputMediaFile(int type) {
		 
	    // External sdcard location
//		try {
			File mediaStorageDir = null;
			if (type == MEDIA_TYPE_IMAGE) {
			    mediaStorageDir = new File(
			            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
			}
			if (type == MEDIA_TYPE_VIDEO) {
				mediaStorageDir = new File(
			            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), VIDEO_DIRECTORY_NAME);
			}

		 
		    // Create the storage directory if it does not exist
		    if (!mediaStorageDir.exists()) {
		        if (!mediaStorageDir.mkdirs()) {
		            Log.e(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
		                    + IMAGE_DIRECTORY_NAME + " directory");
		            return null;
		        }
		    }
		 
		    // Create a media file name
		    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
		    File mediaFile;
		    if (type == MEDIA_TYPE_IMAGE) {
		        mediaFile = new File(mediaStorageDir.getPath() + File.separator
		                + "IMG_" + timeStamp + ".jpg");
		    } else if (type == MEDIA_TYPE_VIDEO) {
		        mediaFile = new File(mediaStorageDir.getPath() + File.separator
		                + "VID_" + timeStamp + ".mp4");
		    } else {
		        return null;
		    }
		    
		    return mediaFile;
//		} catch (Exception e) {
//			return null;
//		}
	}
	
	public static File getPhotoFile() throws IOException {
		 
		  String filePath = Environment.getExternalStorageDirectory().getPath();
		
		  Calendar c = Calendar.getInstance();
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		  String currentDateandTime = sdf.format(c.getTime());
		    
		  String fileName = "image_" + currentDateandTime + JPEG_FILE_SUFFIX;
		  
		  PHOTO_FILE_NAME = fileName;
		    
		  File tempFile = new File(filePath, fileName);
		  
		  return tempFile;
	}
	
	public static boolean checkCameraHardware(Context context) {
	    if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
	        // this device has a camera
	        return true;
	    } else {
	        // no camera on this device
	        return false;
	    }
	}
	
	public static Camera getCameraInstance(){
	    Camera c = null;
	    try {
	        c = Camera.open(); // attempt to get a Camera instance
	    }
	    catch (Exception e){
	        // Camera is not available (in use or does not exist)
	    }
	    return c; // returns null if camera is unavailable
	}
}
