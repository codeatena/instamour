package com.han.network;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.han.source.GlobalData;
import com.han.utility.PreferenceUtility;
import com.hyper.instamour.Global;

import android.util.Log;

public class UserWebService extends WebService {

	public static Integer userLogin(String email, String password) {
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("m", "login"));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("pwd", password));
        
		Log.e("method", "user login");
        String strResponse = WebService.callHttpRequestGeneral(GlobalData.URL_API, "POST", params);
        
        return UserParser.parseLogin(strResponse);
	}
	
	public static Integer userGetInf() {
		
		Global global = Global.getInstance();
		String sessionID = global.preferences.getString("uid", "");
		
		Log.e("session id", sessionID);
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("m", "user-get"));
		params.add(new BasicNameValuePair("sessionId", sessionID));
        
		Log.e("method", "get user inf");
        String strResponse = WebService.callHttpRequestGeneral(GlobalData.URL_API, "POST", params);
        
        return UserParser.parseUserInf(strResponse);
	}
	
	public static Integer paidFeature(String strFeatureType, String strFeatureStatus) {
		
		String sessionId = PreferenceUtility.sharedPreferences.getString("sessionId", "");
	
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("m", "feature"));
		params.add(new BasicNameValuePair("type", strFeatureType));
		params.add(new BasicNameValuePair("status", strFeatureStatus));
		params.add(new BasicNameValuePair("sessionId", sessionId));

		Log.e("method", "paid feature");
        String strResponse = WebService.callHttpRequestGeneral(GlobalData.URL_API, "POST", params);
        
        return Parser.parseOnlyStatus(strResponse);
	}
	
	public static Integer switchPushNotification(String strType) {
		
		Global global = Global.getInstance();
		String sessionID = global.preferences.getString("uid", "");
		
		Log.e("sessionID", sessionID);
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("m", "user-push-notification"));
		params.add(new BasicNameValuePair("type", strType));
		params.add(new BasicNameValuePair("sessionId", sessionID));

		Log.e("method", "action switch push notification");
        String strResponse = WebService.callHttpRequestGeneral(GlobalData.URL_API, "POST", params);
        
		return SUCCESS;
	}
}
