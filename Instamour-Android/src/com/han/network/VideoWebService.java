package com.han.network;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;

import com.han.source.GlobalData;
import com.han.utility.PreferenceUtility;

import android.util.Log;

public class VideoWebService extends WebService {

	public static Integer uploadVideo(String fileUriPath, String strCount) {
		String sessionId = PreferenceUtility.sharedPreferences.getString("sessionId", "");
		Log.e("sessionId", sessionId);
		try {
			MultipartEntity mpEntity = new MultipartEntity();
			
			File file = new File(fileUriPath);
			
			mpEntity.addPart("m", new StringBody("video-add"));
	        mpEntity.addPart("sessionId", new StringBody(sessionId));
	        mpEntity.addPart("count", new StringBody(strCount));
	        mpEntity.addPart("video", new FileBody(file));
	        
	        Log.e("parameter passed ", mpEntity.toString());
	        
	        Log.e("method", "upload video");
	        String strResponse = WebService.callHttpRequestMultiPart(GlobalData.URL_API, mpEntity);
	        return Parser.parseOnlyStatus(strResponse);

		} catch (Exception e) {
			return FAIL;
		}
	}
	
	public static Integer deleteVideo(String strCount) {
		String sessionId = PreferenceUtility.sharedPreferences.getString("sessionId", "");
		
//		Log.e("video count", strCount);
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("m", "video-delete"));
		params.add(new BasicNameValuePair("sessionId", sessionId));
		params.add(new BasicNameValuePair("count", strCount));

		Log.e("method", "delete video");
        String strResponse = WebService.callHttpRequestGeneral(GlobalData.URL_API, "POST", params);
        
        return Parser.parseOnlyStatus(strResponse);
	}
	
	public static Integer sorVideo(String strOrder) {
		String sessionId = PreferenceUtility.sharedPreferences.getString("sessionId", "");
		
		Log.e("String order", strOrder);
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("m", "videos-sort"));
		params.add(new BasicNameValuePair("sessionId", sessionId));
		params.add(new BasicNameValuePair("order", strOrder));

		Log.e("method", "sort video");
        String strResponse = WebService.callHttpRequestGeneral(GlobalData.URL_API, "POST", params);
        
        return Parser.parseOnlyStatus(strResponse);
	}
}