//
//  INSCommentsViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/30/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIArrayTableViewController.h"
@class INSUser;

// Public Constants

// Protocols

@interface INSCommentsViewController : BTIArrayTableViewController

// Public Properties
@property (nonatomic, strong) INSUser *user;

// Public Methods

@end
