//
//  INSComment.h
//  Instamour
//
//  Created by Brian Slick on 7/18/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Classes and Forward Declarations
#import "BTIObject.h"

// Public Constants
typedef void (^INSCommentAvatarDownloadCompletionBlock)(UIImage *image);

// Protocols

@interface INSComment : BTIObject <NSCoding>

// Public Properties
@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *senderID;
@property (nonatomic, copy) NSString *senderUserName;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSDate *dateAdded;

@property (nonatomic, copy) NSString *avatarFileName;
@property (nonatomic, strong) UIImage *avatarImage;
@property (nonatomic, copy) INSCommentAvatarDownloadCompletionBlock avatarDownloadCompletionBlock;

// Public Methods
- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDictionary;

@end
