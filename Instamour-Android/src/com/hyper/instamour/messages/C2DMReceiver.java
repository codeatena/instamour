package com.hyper.instamour.messages;

import android.app.Notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.hyper.instamour.Global;
import com.hyper.instamour.InstantChat;
import com.hyper.instamour.MyFirstSplash;
import com.hyper.instamour.messages.c2dm.C2DMBaseReceiver;
import com.hyper.instamour.model.DataBaseHelper;
import com.hyper.instamour.model.GCMHelper;
import com.hyper.instamour.model.GlobalConsts;
import com.hyper.instamour.model.Model;
import com.quickblox.core.QBCallback;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.result.Result;
import com.quickblox.module.messages.QBMessages;
import com.quickblox.module.messages.model.QBEnvironment;
import com.quickblox.module.messages.result.QBSubscribeToPushNotificationsResult;
import java.io.IOException;

/**
 * Broadcast receiver that handles Android Cloud to Data Messaging (AC2DM)
 * messages, initiated by the JumpNote App Engine server and routed/delivered by
 * Google AC2DM servers. The only currently defined message is 'sync'.
 */
public class C2DMReceiver extends C2DMBaseReceiver {
	private DataBaseHelper baseHelper;
	static final String LOG_TAG = "C2DMRECEIVER";
//	public static int owncounter;

	private String oppName = "";
	public int cntMsg = 0;
	Global global;
	private static String storeJabber = "";

	public C2DMReceiver() {
		super(GCMHelper.SENDER_ID);
		global = Global.getInstance();

	}

	@Override
	public void onError(Context context, String errorId) {
		Log.e(LOG_TAG, "onError: " + errorId);
		Toast.makeText(context, "Messaging registration error: " + errorId,
				Toast.LENGTH_LONG).show();
	}

	@SuppressWarnings({ "deprecation" })
	@Override
	protected void onMessage(Context context, Intent intent) {
		String message = "";
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		global.editor = global.preferences.edit();
		baseHelper = new DataBaseHelper(context);
		final Bundle extras = intent.getExtras();
		message = intent.getExtras().getString("message");
		try {
			Log.e("from : ",
					extras.getString("from") + "-----"
							+ extras.getString("collapse_key"));
			String id = "" + extras.getString("id");
			Log.e("user id is : ", "------>" + id);
			/*
			 * for(int i=0;i<extras.keySet().size();i++) {
			 * Log.e("user id is : ", "------>" + id + "====>" +
			 * extras.keySet());
			 * 
			 * }
			 */

		} catch (Exception e) {
			// TODO: handle exception
		}

		/*
		 * for(String key : intent.getExtras().k){ message = key + ": " +
		 * intent.getExtras().getString(key) + ";\n";
		 * 
		 * }
		 */

		Log.d(LOG_TAG, "onMessage: ----> " + message);

		// post notification
		Intent intent2 = new Intent(this, MyFirstSplash.class);
		intent2.putExtra("message", message);

		intent2.putExtra("isNotify", 1);
		// cntMsg=baseHelper.getChatCount(Integer.parseInt(global.getJabberid()));
		// global.editor.putString("frindjabberid", chatid);

		/*
		 * intent2.putExtra("chatimage", global.getChatList().get(0)
		 * .getChat_image());
		 */

		Log.e("Message receive from : ", "----->" + message);
		String oppnentName = "", jabberid = "", myJabberId = "";
		int storeCount = 0;

		/*
		 * try {
		 */
		String parts[] = message
				.split(GlobalConsts.ATTACH_INDICATOR_NOTIFICATION);

		oppnentName = parts[0].trim();
		message = parts[1];
		jabberid = parts[2].trim();

		//intent2.putExtra("j", jabberid);
		if (TextUtils.isEmpty(GlobalConsts.JabberID)) {
			myJabberId = "tiger";
		} else {
			myJabberId = GlobalConsts.JabberID;
		}

		if (GlobalConsts.isMsgScreenOpen && myJabberId.equals(jabberid)) {

		} else {
			storeCount = baseHelper.getChatCount(Integer.parseInt(jabberid));
			Log.e("Message receive from : ", "----->" + message);
			Vibrator vibrato = (Vibrator) context
					.getSystemService(Context.VIBRATOR_SERVICE);
			vibrato.vibrate(1000);
			NotificationManager mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notification = new Notification(
					android.R.drawable.ic_dialog_info, "Instamour",
					System.currentTimeMillis());
			PendingIntent intentos = PendingIntent.getActivity(context, 0,
					intent2, 0);
			notification.number = ++storeCount;
			//owncounter=storeCount;
			/*
			 * String dateTime = getDateTime(context); String chatdate =
			 * dateTime.substring(0, dateTime.lastIndexOf("#")); String chatTime
			 * =
			 * dateTime.substring(dateTime.lastIndexOf("#")+1,dateTime.length()
			 * );
			 */

			if (message.length() > 12
					&& message.substring(0, 13).equals("(Attach file)")) {
				// databaseHelper.insert_chatImageSenddata(Integer.parseInt(jabberid),global.preferences.getInt("jabberId",
				// 0), message, "", chatdate, chatTime, 0);
				notification.setLatestEventInfo(context, "Instamour",
						oppnentName + " : " + "Image", intentos);
			}

			else {
				// databaseHelper.insert_chatdata(Integer.parseInt(jabberid),global.preferences.getInt("jabberId",
				// 0), message, "", 0);
				baseHelper.insert_chatdata(Integer.parseInt(jabberid),
						Integer.parseInt(global.preferences.getString(
								"jabberId", "")), message, global.getFriendphoto(), 0,oppnentName);
				notification.setLatestEventInfo(context, "Instamour",
						oppnentName + " : " + message, intentos);
			}

			Log.e("jabber id : ", "------->" + jabberid + "----->"
					+ storeJabber);


			int jabberExist= baseHelper.getJabberIdNoti(Integer.parseInt(jabberid));

			if(jabberExist == 1)
			{
				int count = baseHelper.getChatCount(Integer.parseInt(jabberid));
				count += 1;
				baseHelper.updateFriendCount(count, Integer.parseInt(jabberid),message);
				notification.number = count;
			}
			else
			{
				cntMsg++;
				Log.e("store count : ", "----->" + cntMsg);
				storeJabber = jabberid;
				storeCount = cntMsg;
				cntMsg = 0;
				Log.e("store count : ", "----->" + cntMsg + "---->"
						+ storeCount + "------->" + jabberid + "----->"
						+ storeJabber);
				baseHelper.insertnoti(oppnentName, message, "", "", storeCount, Integer.parseInt(jabberid));
				notification.number = storeCount;
			}

			if (jabberid.equals(storeJabber)) {
				
			} else {
				

			}

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults |= Notification.DEFAULT_SOUND;
			mManager.notify(0, notification);

		}

		/*
		 * }catch (ArrayIndexOutOfBoundsException e) { // TODO: handle exception
		 * Log.e("wwwww", "wwwwww"); }catch (Exception e) { // TODO: handle
		 * exception Log.e("errrrr", "rreeerrr"); }
		 */

	}

	@Override
	public void onRegistered(Context context, final String registrationId)
			throws IOException {
		Log.e(LOG_TAG, "onRegistered() registrationId is " + registrationId);

		String deviceId = ((TelephonyManager) getBaseContext()
				.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		QBMessages.subscribeToPushNotificationsTask(registrationId, deviceId,
				QBEnvironment.DEVELOPMENT, new QBCallbackImpl() {
			@Override
			public void onComplete(Result result) {
				if (result.isSuccess()) {
					QBSubscribeToPushNotificationsResult subscribeToPushNotificationsResult = (QBSubscribeToPushNotificationsResult) result;
					Log.e(">>> subscription created", "----->"
							+ subscribeToPushNotificationsResult
							.getSubscriptions().toString());
				} else {
					Log.e(LOG_TAG, "Errro: ");
				}
			}
		});

	}

	@Override
	public void onUnregistered(Context context) {
		Log.d(LOG_TAG, "onUnregistered");
	}
}
