package com.hyper.instamour.model;

import java.util.ArrayList;

public class User {

	public static final int MAX_USER_VIDEO_COUNT = 10;
	public static final int NOR_USER_VIDEO_COUNT = 4;

	public String userID;
	public String userName;
	public String email;
	public String photoFileUrl;
	
	public ArrayList<CreateVideoData> arrVideoData = new ArrayList<CreateVideoData>();
	
	public User() {
		for (int i = 0; i < MAX_USER_VIDEO_COUNT; i++) {
			CreateVideoData videoData = new CreateVideoData();
			arrVideoData.add(videoData);
		}
	}
}
