//
//  INSVideoRecorderViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/22/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSVideoRecorderViewController.h"

// Models and other global
#import <MediaPlayer/MediaPlayer.h>
#import "PBJVision.h"

// Sub-controllers

// Views

// Private Constants

typedef NS_ENUM(NSInteger, INSVideoRecorderMode) {
    INSVideoRecorderModePlanning,
    INSVideoRecorderModeRecording,
    INSVideoRecorderModeReviewing,
};

@interface INSVideoRecorderViewController () <PBJVisionDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIButton *recordButton;
@property (nonatomic, strong) IBOutlet UIButton *doneButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;

@property (nonatomic, strong) IBOutlet UIView *timerLabelContainerView;
@property (nonatomic, strong) IBOutlet UILabel *timerLabel;

@property (nonatomic, strong) IBOutlet UIView *videoContainerView;

@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;

@property (nonatomic, assign) INSVideoRecorderMode mode;

@property (nonatomic, weak) NSTimer *labelUpdateTimer;
@property (nonatomic, copy) NSDate *timerStartDate;

@property (nonatomic, copy) NSURL *videoFileURL;

@end

@implementation INSVideoRecorderViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Record Video"];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(backButtonPressed:)];
    [[self navigationItem] setLeftBarButtonItem:backButton];
    
    UIBarButtonItem *cameraButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"camera-swap"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(cameraSwapButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:cameraButton];
    
    // Misc

    PBJVision *vision = [PBJVision sharedInstance];
    
    AVCaptureVideoPreviewLayer *previewLayer = [vision previewLayer];
    [previewLayer setFrame:[[self videoContainerView] bounds]];
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [[[self videoContainerView] layer] addSublayer:previewLayer];
    
    if (![[self confirmationButtonTitle] isNotEmpty])
    {
        [self setConfirmationButtonTitle:@"Done"];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    [Flurry logEvent:@"Video Record View"];

    // User Interface
    
    [self setMode:INSVideoRecorderModePlanning];

    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewWillDisappear:animated];
    
    PBJVision *vision = [PBJVision sharedInstance];
    [vision setDelegate:nil];
    
    [vision stopPreview];
    
    [self stopTimer];
    
    [self stopPlayingCurrentVideo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIButton *recordButton = [self recordButton];
    UIBarButtonItem *cameraButton = [[self navigationItem] rightBarButtonItem];
    UIButton *cancelButton = [self cancelButton];
    UIButton *doneButton = [self doneButton];
    UIView *timerLabelContainerView = [self timerLabelContainerView];
    
    [doneButton setTitle:[self confirmationButtonTitle] forState:UIControlStateNormal];
    
    switch ([self mode])
    {
        case INSVideoRecorderModePlanning:
        {
            [recordButton setBackgroundImage:[UIImage imageNamed:@"cameraButton"] forState:UIControlStateNormal];
            [recordButton setEnabled:YES];
            [cameraButton setEnabled:YES];
            [cancelButton setHidden:YES];
            [doneButton setHidden:YES];
            [timerLabelContainerView setHidden:YES];
            
            [self stopPlayingCurrentVideo];
            
            [self resetVideoCapture];
            
            [[PBJVision sharedInstance] startPreview];
            
            if (![self isAtLeastOneCameraAvailable])
            {
                [timerLabelContainerView setHidden:NO];
                [recordButton setEnabled:NO];
                [cameraButton setEnabled:NO];
                [[self timerLabel] setText:@"No Cameras"];
            }
        }
            break;
        case INSVideoRecorderModeRecording:
        {
            [recordButton setBackgroundImage:[UIImage imageNamed:@"cameraButtonWithDot"] forState:UIControlStateNormal];
            [recordButton setEnabled:YES];
            [cameraButton setEnabled:NO];
            [cancelButton setHidden:YES];
            [doneButton setHidden:YES];
            [timerLabelContainerView setHidden:NO];
        }
            break;
        case INSVideoRecorderModeReviewing:
        {
            [recordButton setBackgroundImage:[UIImage imageNamed:@"cameraButtonWithDot"] forState:UIControlStateNormal];
            [recordButton setEnabled:NO];
            [cameraButton setEnabled:NO];
            [cancelButton setHidden:NO];
            [doneButton setHidden:NO];
            [timerLabelContainerView setHidden:YES];
            
            [self playCurrentVideo];
        }
            break;
        default:
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers


#pragma mark - UI Response Methods

- (void)backButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    PBJVision *vision = [PBJVision sharedInstance];
    [vision setDelegate:nil];
    [vision stopPreview];
    [vision endVideoCapture];
    
    [self stopTimer];
    
    [self stopPlayingCurrentVideo];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)recordButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self mode] == INSVideoRecorderModePlanning)
    {
        [self setMode:INSVideoRecorderModeRecording];

        [self startVideoCapture];
        
        [self startTimer];
    }
    else
    {
        [self stopTimer];

        [self stopVideoCapture];

        [self setMode:INSVideoRecorderModeReviewing];
    }
    
    [self populateContents];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)doneButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self stopTimer];
    
    [self stopPlayingCurrentVideo];
    
    [[self delegate] videoRecorder:self
               didRecordVideoToURL:[self videoFileURL]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)cancelButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSVideoRecorderModePlanning];
    
    [self populateContents];
    
    NSURL *fileURL = [self videoFileURL];
    
    if (fileURL == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No video file, nothing else to do", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:[fileURL path]])
    {
        [fileManager removeItemAtPathBTI:[fileURL path]];
    }

    [self setVideoFileURL:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)cameraSwapButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    PBJVision *vision = [PBJVision sharedInstance];
    
    [vision setCameraDevice:([vision cameraDevice] == PBJCameraDeviceBack) ? PBJCameraDeviceFront : PBJCameraDeviceBack];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (BOOL)isAtLeastOneCameraAvailable
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    PBJVision *vision = [PBJVision sharedInstance];

    BOOL isFrontCameraAvailable = [vision isCameraDeviceAvailable:PBJCameraDeviceFront];
    BOOL isBackCameraAvailable = [vision isCameraDeviceAvailable:PBJCameraDeviceBack];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return (isFrontCameraAvailable || isBackCameraAvailable);
}

- (void)resetVideoCapture
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    PBJVision *vision = [PBJVision sharedInstance];
    [vision setDelegate:self];
    
    BOOL isFrontCameraAvailable = [vision isCameraDeviceAvailable:PBJCameraDeviceFront];
    BOOL isBackCameraAvailable = [vision isCameraDeviceAvailable:PBJCameraDeviceBack];
    
    if (isFrontCameraAvailable && isFrontCameraAvailable)
    {
        [[[self navigationItem] rightBarButtonItem] setEnabled:YES];
        
        [vision setCameraDevice:PBJCameraDeviceFront];
    }
    else if (isFrontCameraAvailable || isBackCameraAvailable)
    {
        [[[self navigationItem] rightBarButtonItem] setEnabled:NO];
        
        if (isFrontCameraAvailable)
        {
            [vision setCameraDevice:PBJCameraDeviceFront];
        }
        else if (isBackCameraAvailable)
        {
            [vision setCameraDevice:PBJCameraDeviceBack];
        }
    }
    
    [vision setCameraMode:PBJCameraModeVideo];
    [vision setCameraOrientation:PBJCameraOrientationPortrait];
    [vision setFocusMode:PBJFocusModeContinuousAutoFocus];
    [vision setOutputFormat:PBJOutputFormatSquare];
    [vision setVideoRenderingEnabled:YES];
    [vision setAdditionalCompressionProperties:@{ AVVideoProfileLevelKey : AVVideoProfileLevelH264Baseline30 }];
    // AVVideoProfileLevelKey requires specific captureSessionPreset
    // ^^^ SLICK: This is a note from sample app. I have no idea what it means.
    
    // TODO: This (15) probably needs to be related to PBJVision somehow.
    int32_t preferredFrameRate = 15;
    
    // TODO: Needs to be changed for 30-second video option
    Float64 seconds = INSMaxVideoLengthInSeconds;
    
    CMTime maxDuration = CMTimeMakeWithSeconds(seconds, preferredFrameRate);
    
    [vision setVideoFrameRate:preferredFrameRate];
    [vision setMaximumCaptureDuration:maxDuration];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)startVideoCapture
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [[PBJVision sharedInstance] startVideoCapture];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopVideoCapture
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];

    [[PBJVision sharedInstance] endVideoCapture];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)startTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self stopTimer];
    
    [self setTimerStartDate:[NSDate date]];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                      target:self
                                                    selector:@selector(labelUpdateTimerFireMethod:)
                                                    userInfo:nil
                                                     repeats:YES];
    [self setLabelUpdateTimer:timer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self labelUpdateTimer] invalidate];
    [self setLabelUpdateTimer:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)labelUpdateTimerFireMethod:(NSTimer *)timer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:[self timerStartDate]];
    
    // TODO: Needs to be changed for 30-second video option
    if (timeInterval <= 8.09)       // If I use 8.0, never see 8.0 in label, only 7.9.
    {
        NSString *duration = [NSString stringWithFormat:@"%.1f sec", timeInterval];
        
        [[self timerLabel] setText:duration];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)playCurrentVideo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self stopPlayingCurrentVideo];
    
    NSURL *videoURL = [self videoFileURL];
    
    if (videoURL == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No URL", self, __PRETTY_FUNCTION__);
        return;
    }
    
    MPMoviePlayerController *moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
    [self setMoviePlayerController:moviePlayer];
    
    [[moviePlayer view] setFrame:[[self videoContainerView] bounds]];
    [[moviePlayer view] setBackgroundColor:[UIColor clearColor]];
    
    [[moviePlayer backgroundView] setBackgroundColor:[UIColor clearColor]];

    [moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    [moviePlayer setRepeatMode:MPMovieRepeatModeNone];
    [moviePlayer setFullscreen:NO animated:NO];
    
    [[self videoContainerView] addSubview:[moviePlayer view]];
    
    [moviePlayer prepareToPlay];
    [moviePlayer pause];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopPlayingCurrentVideo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *moviePlayer = [self moviePlayerController];
    
    if (moviePlayer == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No movie player", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [moviePlayer stop];
    
    [[moviePlayer view] removeFromSuperview];
    
    [self setMoviePlayerController:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - PBJVisionDelegate

- (void)vision:(PBJVision *)vision
 capturedVideo:(NSDictionary *)videoDict
         error:(NSError *)error
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSVideoRecorderModePlanning];
    
    if (error && [error.domain isEqual:PBJVisionErrorDomain] && error.code == PBJVisionErrorCancelled)
    {
        [self populateContents];
        
        NSLog(@"recording session cancelled");
        return;
    }
    else if (error)
    {
        [self populateContents];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Recording Error"
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];

        NSLog(@"encounted an error in video capture (%@)", error);
        return;
    }

    NSString *incomingVideoPath = [videoDict objectForKey:PBJVisionVideoPathKey];
    NSURL *incomingVideoURL = [NSURL fileURLWithPath:incomingVideoPath];
    
    NSURL *fileURL = [INSFileUtilities uniqueNewVideoURL];
    
    [[NSFileManager defaultManager] moveItemAtURL:incomingVideoURL toURL:fileURL error:nil];
    
    [self setVideoFileURL:fileURL];
    
    [self setMode:INSVideoRecorderModeReviewing];
    
    [self populateContents];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
