//
//  NSMutableDictionary+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/12/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "NSMutableDictionary+INSAdditions.h"

@implementation NSMutableDictionary (INSAdditions)

- (void)setObjectSafelyINS:(id)anObject
                    forKey:(id < NSCopying >)aKey
{
    if ( (aKey == nil) || (anObject == nil) )
    {
        return;
    }
    
    [self setObject:anObject forKey:aKey];
}

- (void)setKeySafelyINS:(id < NSCopying >)aKey
              andObject:(id)anObject
{
    if ( (aKey == nil) || (anObject == nil) )
    {
        return;
    }
    
    [self setObject:anObject forKey:aKey];
}

@end
