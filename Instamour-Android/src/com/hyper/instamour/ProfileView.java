package com.hyper.instamour;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.R.bool;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.hyper.instamour.model.ImageLoader;

public class ProfileView extends Activity {
	private TextView heading;
	private String uid;
	private MultipartEntity entity;
	private Intent pictureActionIntent = null;
	private Bitmap bitmap;
	private ListView listView;
	private String imageName;
	private ImageView userpic;
	private String selectedImagePath = "";
	android.app.AlertDialog.Builder alertDialog;
	protected static final int CAMERA_REQUEST = 0;
	private List<NameValuePair> param;
	private String description;

	protected static final int GALLERY_PICTURE = 1;
	String uname;
	Global global;
	private TextView desc_txt, about_me_txt, desc_txt1;
	private Function c;
	String fileSrc;
	private ImageLoader imageLoader;
	private MenuSlideView scrollView;
	private int layoutToSlide;
	private View anapp;
	private SlideMenu sm;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		int currentOrientation = getResources().getConfiguration().orientation;
		if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
				|| newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
			super.onConfigurationChanged(newConfig);

		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Runtime.getRuntime().totalMemory();
		Runtime.getRuntime().freeMemory();
		if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
			Toast.makeText(this, "Large screen", Toast.LENGTH_LONG).show();

		} else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			Toast.makeText(this, "Normal sized screen", Toast.LENGTH_LONG)
					.show();

		} else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
			Toast.makeText(this, "Small sized screen", Toast.LENGTH_LONG)
					.show();
		} else {
			Toast.makeText(this,
					"Screen size is neither large, normal or small",
					Toast.LENGTH_LONG).show();
		}

		// setContentView(R.layout.view_profile);
		LayoutInflater inflater = LayoutInflater.from(this);
		scrollView = (MenuSlideView) inflater.inflate(
				R.layout.screen_scroll_with_list_menu, null);
		setContentView(scrollView);
		layoutToSlide = R.layout.view_profile;
		sm = new SlideMenu(getApplicationContext(), scrollView, inflater,
				layoutToSlide);
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);

		global.editor = global.preferences.edit();

		c = new Function();
		entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		imageLoader = new ImageLoader(getApplicationContext());
		alertDialog = new AlertDialog.Builder(ProfileView.this);
		uid = global.preferences.getString("uid", "");
		uname = global.getLoginusername();

		Log.e("uname", "---" + uname);
		desc_txt = (TextView) findViewById(R.id.txtdesc);
		desc_txt1 = (TextView) findViewById(R.id.txtdesc1);
		userpic = (ImageView) findViewById(R.id.uservideopic);
		Log.e("frr", userpic.getHeight() + "  -- " + userpic.getWidth());
		// bitmap=get_ninepatch(R.drawable.user_user, 900, 900,
		// ProfileView.this);
		// bitmap=decodeFile(f, size)
		userpic.setImageBitmap(bitmap);
		Log.e("frr", userpic.getHeight() + "  -- " + userpic.getWidth());
		about_me_txt = (TextView) findViewById(R.id.txtaboutme);
		heading = (TextView) findViewById(R.id.uservideoview_topbar_heading);
		heading.setText(uname);
		Log.e("my image ", global.preferences.getString("photo", "") + "--");
		imageLoader.DisplayImage(c.dolink + "" + global.getPhoto(), userpic);
		if (c.haveNetworkConnection(getApplicationContext())) {

			new ProfileViewTask().execute();

		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(
					ProfileView.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		new ProfileViewTask().execute();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			new VideoSetting(ProfileView.this);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		this.finish();
	}

	public void onClick(View view) {
		switch (view.getId()) {

		case R.id.video_play:
			Log.e("fdfad",
					Uri.parse(global.preferences.getString("video_merge", ""))
							+ "");
			if (global.preferences.getString("video_merge", "")
					.equals(c.dolink)) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						ProfileView.this);
				builder.setMessage("Sorry! No video to play.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.dismiss();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();

			} else {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse(global.preferences.getString(
						"video_merge", "")), "video/mp4");
				startActivity(intent);
			}
			break;
		case R.id.edit_photo:
			startDialog();
			break;
		case R.id.view_profile_edit_info:

			Intent intent1 = new Intent(ProfileView.this, EditProfile.class);
			startActivity(intent1);
			break;

		default:
			break;
		}
	}

	public class ProfileViewTask extends AsyncTask<Void, Void, Void> {
		public boolean status = false;
		private ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(ProfileView.this);
			pd.setMessage("Please wait...");
			pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			profileView();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pd != null && pd.isShowing()) {
				imageLoader.DisplayImage(c.dolink + global.getPhoto(), userpic);
				imageLoader.DisplayImage(c.dolink + "" + global.getPhoto(),
						sm.img_pro_pic);
				pd.dismiss();
			}
			try {
				new VideoSetting(ProfileView.this);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
	}

	@SuppressLint("NewApi")
	public void profileView() {

		SharedPreferences pref = getSharedPreferences("editprofile",
				MODE_PRIVATE);
		SharedPreferences.Editor edit = pref.edit();
		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "user-get"));
		param.add(new BasicNameValuePair("sessionId", uid));
		JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
		Log.e("response", "response" + json);

		try {
			JSONObject user = json.getJSONObject("user");
			String uid = user.getString("uid");
			final String uName = user.getString("uname");
			String email = user.getString("email");
			String fbid = user.getString("fbid");
			String date_added = user.getString("date_added");
			String disable_account = user.getString("disable_account");

			JSONObject setting = user.getJSONObject("settings");
			String pid = setting.getString("pid");
			String gender = setting.getString("gender");
			String dob = setting.getString("dob");
			String ethnicity = setting.getString("ethnicity");
			String city = setting.getString("city");
			String state = setting.getString("state");
			String country = setting.getString("country");

			String ltd = setting.getString("ltd");
			Log.e("country", "---" + country);
			String lngd = setting.getString("lngd");
			String photo = setting.getString("photo");
			Log.e("fadf", "-->" + ltd);
			String video1 = setting.getString("video1");
			String video2 = setting.getString("video2");
			String video3 = setting.getString("video3");

			String video4 = setting.getString("video4");
			String video_merge = setting.getString("video_merge");
			String video_count = setting.getString("video_count");
			Log.e("video count", video_count);
			Log.e("", "---" + video_merge);
			String chat = setting.getString("chat");
			String video_chat = setting.getString("video_chat");
			String chatid = setting.getString("chatid");
			String identity = setting.getString("identity");
			String height = setting.getString("height");
			String body_type = setting.getString("body_type");
			String sexual_preference = setting.getString("sexual_preference");
			String looking_for = setting.getString("looking_for");
			final String smoker = setting.getString("smoker");
			String about_me = setting.getString("about_me");
			if (about_me.equals("null")) {
				about_me = " ";
			}

			Calendar c = Calendar.getInstance();
			System.out.println("Current time => " + c.getTime());
			Log.e("Current time => ", "" + c.getTime());

			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			String formattedTodayDate = df.format(c.getTime());

			Date today = df.parse(formattedTodayDate);
			Date bdate = df.parse(dob);
			long diff = ((today.getTime() - bdate.getTime()) / (24 * 60 * 60 * 1000)) / 365;
			Log.e("age", "-->" + diff / 365);

			if (diff > 0) {
				description = Long.toString(diff);
			} else {
				description = "Sorry Incorect Value";
			}
			if (height.equals("null") || height.equals("Choose")
					|| height.equals("")) {
				height = "";
			} else {
				description += "/ " + height;
			}
			if (identity.equals("null") || identity.equals("Choose")
					|| identity.equals("")) {
				identity = "";
			} else {
				description += "/ " + identity;
			}

			if (body_type.equals("null") || body_type.equals("Choose")
					|| body_type.equals("")) {
				body_type = "";
			} else {
				description += "/ " + body_type;
			}

			if (sexual_preference.equals("null")
					|| sexual_preference.equals("Choose")
					|| sexual_preference.equals("")) {
				sexual_preference = "";
			} else {
				description += "/ " + sexual_preference;
			}

			if (looking_for.equals("null") || looking_for.equals("Choose")
					|| looking_for.equals("")) {
				looking_for = "";
			}
			/*
			 * final String description = diff + "/ " + height + "/ " + identity
			 * + "/ " + body_type + "/ " + sexual_preference;
			 */
			final String description1 = "Looking For " + looking_for;

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					desc_txt.setText(description);
					desc_txt1.setText(description1);

					heading.setText(uname);

				}
			});
			global.editor.putString("uname", uName);
			global.setPhoto(photo);
			global.editor.commit();
			edit.putString("identity", identity);
			edit.putString("height", height);
			edit.putString("body_type", body_type);
			edit.putString("looking_for", looking_for);
			edit.putString("smoker", smoker);
			edit.putString("ethnicity", ethnicity);
			edit.putString("sexual_preference", sexual_preference);
			edit.putString("about_me", about_me);
			edit.commit();
			about_me_txt.setText(about_me);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	private void startDialog() {

		AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
		myAlertDialog.setTitle("Change Pictures Option");
		myAlertDialog.setMessage("How do you want to set your picture?");

		myAlertDialog.setPositiveButton("Gallery",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						pictureActionIntent = new Intent(
								Intent.ACTION_GET_CONTENT, null);
						pictureActionIntent.setType("image/*");
						pictureActionIntent.putExtra("return-data", true);
						startActivityForResult(pictureActionIntent,
								GALLERY_PICTURE);
					}
				});

		myAlertDialog.setNegativeButton("Camera",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {

						Intent cameraIntent = new Intent(
								android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						startActivityForResult(cameraIntent, CAMERA_REQUEST);

					}
				});
		myAlertDialog.show();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == GALLERY_PICTURE) {
			if (resultCode == RESULT_OK) {
				if (data != null) {

					try {

						BitmapDrawable bmpDrawable = null;
						Uri selectedImage = data.getData();
						String[] filePathColumn = { MediaStore.Images.Media.DATA };

						Cursor cursor = getContentResolver()
								.query(selectedImage, filePathColumn, null,
										null, null);

						if (cursor != null) {
							cursor.moveToFirst();

							int columnIndex = cursor
									.getColumnIndex(filePathColumn[0]);
							fileSrc = cursor.getString(columnIndex);
							imageName = fileSrc.substring(fileSrc
									.lastIndexOf("/") + 1);
							try {
								bitmap = BitmapFactory.decodeFile(fileSrc);
							} catch (Exception e) {
								// TODO: handle exception
								Toast.makeText(getApplicationContext(),
										"Sorry, This image can't be uploaded",
										Toast.LENGTH_LONG).show();
							} catch (OutOfMemoryError e) {
								// TODO: handle exception
								Toast.makeText(
										getApplicationContext(),
										"Image size is too big ,Sorry, trys with different image ",
										Toast.LENGTH_LONG).show();
							}
							cursor.close();

							new UploadImageTask().execute();

						} else {

							bmpDrawable = new BitmapDrawable(getResources(),
									data.getData().getPath());
							Log.e("", bmpDrawable + "");
							userpic.setImageDrawable(bmpDrawable);
						}

					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(getApplicationContext(),
								"Sorry, This image can't be uploaded",
								Toast.LENGTH_LONG).show();
					} catch (OutOfMemoryError e) {
						// TODO: handle exception
						Toast.makeText(getApplicationContext(),
								"Sorry, This image can't be uploaded",
								Toast.LENGTH_LONG).show();
					}
					return;

				} else {
					Toast.makeText(getApplicationContext(), "Cancelled",
							Toast.LENGTH_SHORT).show();
					return;
				}
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(getApplicationContext(), "Cancelled",
						Toast.LENGTH_SHORT).show();
				return;
			}
		} else if (requestCode == CAMERA_REQUEST) {
			if (Build.VERSION.SDK_INT < 19) {
				try {
					if (resultCode == RESULT_OK) {

						bitmap = (Bitmap) data.getExtras().get("data");
						saveImage();

						new UploadImageTask().execute();

					} else if (resultCode == RESULT_CANCELED) {
						Log.e("Canceled", "canceled");
					}
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(getApplicationContext(),
							"Sorry, This image can't be uploaded",
							Toast.LENGTH_LONG).show();
				} catch (OutOfMemoryError e) {
					// TODO: handle exception
					Toast.makeText(getApplicationContext(),
							"Sorry, This image can't be uploaded",
							Toast.LENGTH_LONG).show();
				}
			} else {

				try {
					Log.e("Above Kitkat ", "Kitkat");
					Uri selectedImageUri = data.getData();
					/*
					 * imageStorePath = getPath(selectedImageUri);
					 * Log.e("Image store", "---" + imageStorePath);
					 */
					final int takeFlags = data.getFlags()
							& (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
					String id = selectedImageUri.getLastPathSegment()
							.split(":")[1];
					final String[] imageColumns = { MediaStore.Images.Media.DATA };
					final String imageOrderBy = null;

					Uri uri = getUri();

					Cursor imageCursor = managedQuery(uri, imageColumns,
							MediaStore.Images.Media._ID + "=" + id, null,
							imageOrderBy);

					if (imageCursor.moveToFirst()) {
						selectedImagePath = imageCursor.getString(imageCursor
								.getColumnIndex(MediaStore.Images.Media.DATA));
					}

					Bitmap bpm = BitmapFactory.decodeFile(selectedImagePath);
					userpic.setImageBitmap(bpm);
					Log.e("path", "----->" + selectedImagePath);

					/*
					 * ParcelFileDescriptor parcelFileDescriptor;
					 * 
					 * parcelFileDescriptor = getContentResolver()
					 * .openFileDescriptor(selectedImageUri, "r");
					 * FileDescriptor fileDescriptor = parcelFileDescriptor
					 * .getFileDescriptor(); Bitmap image = BitmapFactory
					 * .decodeFileDescriptor(fileDescriptor);
					 * parcelFileDescriptor.close();
					 * userpic.setImageBitmap(image);
					 */

				} catch (Exception e) {
					// TODO: handle exception
					Log.e("Null pointer", "Null Pointer");
				}
				return;

			}

		}

	}

	public void saveImage() {

		int counting = 0;
		OutputStream fOut = null;
		Uri outputFileUri;

		try {
			File root = new File(Environment.getExternalStorageDirectory()
					+ "/instamour/");
			if (!root.exists()) {
				root.mkdirs();
			}
			imageName = "image_" + String.valueOf(counting) + ".jpg";

			File sdImageMainDirectory = new File(root, imageName);
			while (sdImageMainDirectory.exists()) {
				counting++;
				imageName = "image_" + String.valueOf(counting) + ".jpg";
				sdImageMainDirectory = new File(root, imageName);
				fileSrc = sdImageMainDirectory.toString();

			}
			Log.e("captured image", sdImageMainDirectory + "");
			outputFileUri = Uri.fromFile(sdImageMainDirectory);
			fOut = new FileOutputStream(sdImageMainDirectory);
		} catch (Exception e) {
			Log.e("Incident Photo", "Error occured. Please try again later.");
		}
		try {
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);

			fOut.flush();
			fOut.close();

		} catch (Exception e) {
		}

	}

	public class UploadImageTask extends AsyncTask<Void, Void, Void> {

		private JSONObject json;
		private ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(ProfileView.this);
			pd.setMessage("Uploading Please wait...");
			pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();

		}

		@SuppressLint("NewApi")
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (c.haveNetworkConnection(getApplicationContext())) {
				uploadImage();

			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						ProfileView.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pd != null && pd.isShowing()) {

				pd.dismiss();
			}
			if (c.haveNetworkConnection(getApplicationContext())) {
				new ProfileViewTask().execute();
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						ProfileView.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

		}

	}

	@SuppressLint("NewApi")
	public void uploadImage() {
		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, 100, bos);
			byte[] data = bos.toByteArray();

			File file = new File(fileSrc);

			Log.e("file ", file.toString());
			entity.addPart("m", new StringBody("image-add"));
			entity.addPart("sessionId", new StringBody(String.valueOf(uid)));
			entity.addPart("image", new ByteArrayBody(data,fileSrc));
			Log.e("parameter is ", " -->" + entity);

			MultipartEntity nameValuePairs = (MultipartEntity) entity;
			String sResponse = getJSONFromUrl(c.link.trim(), nameValuePairs);

			Log.e("Image upload response is  : ", sResponse);
			global.editor.putString("photo", file.toString());
			global.editor.commit();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Log.e("errottt", "---"+e);
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
			Toast.makeText(getApplicationContext(),
					"this image can't be uploaded", Toast.LENGTH_LONG).show();
		}

	}

	public String getJSONFromUrl(String url, MultipartEntity nameValuePairs) {

		InputStream is = null;
		String json = null;
		try {

			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(nameValuePairs);

			HttpResponse response = httpClient.execute(httpPost);
			Log.e("response from post method", "" + httpPost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		} catch (UnsupportedEncodingException e) {
			Log.e("errottt", json);

			e.printStackTrace();
		} catch (ClientProtocolException e) {
			Log.e("errottt", json);
			e.printStackTrace();
		} catch (Exception e) {
			Log.e("errottt", json);
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();

			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("errottt", json);
			e.printStackTrace();
		}
		return json;
	}

	private Uri getUri() {
		String state = Environment.getExternalStorageState();
		if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
			return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

		return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	}
	//the directions for FlurryAnalytics call for this
			//(FLJ, 5/6/14)
			 // The Activity's onStart method
			@Override 
			protected void onStart() { // Set up the Flurry session
				super.onStart();
				FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
				
			}
			@Override
			protected void onStop() {
				super.onStop();
				FlurryAgent.onEndSession(this);
			}
}
