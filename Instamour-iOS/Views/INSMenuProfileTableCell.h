//
//  INSMenuProfileTableCell.h
//  Instamour
//
//  Created by Brian Slick on 7/13/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols

@interface INSMenuProfileTableCell : BTITableViewCell

// Public Properties
@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;

@end
