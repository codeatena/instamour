
package com.hyper.instamour;

import java.io.File;
import java.util.ArrayList;
import java.util.Stack;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hyper.instamour.MenuSlideView.SizeCallback;
import com.hyper.instamour.messages.C2DMReceiver;
import com.hyper.instamour.model.DataBaseHelper;
import com.hyper.instamour.model.ImageLoader;
import com.hyper.instamour.model.Model;

public class SlideMenu {

    MenuSlideView MnuscrollView;
    View Mnumenu;
    MenuSlideView scrollView;
    private String[] lvMenuItems;
    ImageView btnClick;
    private MenuAdapter menuAdapter;
    View menu;
    View app;
    private Global globalApp;
    private ImageLoader imageLoader;
    static boolean menuOut = false;
    private ArrayList<MenuItemList> address = new ArrayList<MenuItemList>();;
    Builder alert;
    boolean isWebHistory = false;
    private Context contxt;
    private SharedPreferences pref;
    private RelativeLayout rl_menu_toplayout;

    public ImageView img_pro_pic;
    public TextView txt_uName, instant_noti;
    public static boolean isClicked = false;

    /**
     * Menu must NOT be out/shown to start with.
     */
    // boolean menuOut = false;

    public SlideMenu(final Context context, MenuSlideView slidemenu,
            LayoutInflater inflater, int layout) {
        super();
        this.contxt = context;
        this.MnuscrollView = slidemenu;
        globalApp = Global.getInstance();
        ImageLoader imageLoader = new ImageLoader(context);

        menu = inflater.inflate(R.layout.menulist, null);
        app = inflater.inflate(layout, null);
        btnClick = (ImageView) app.findViewById(R.id.img_slider);
        final View[] children = new View[] {
                menu, app
        };
        final Stack stack = new Stack();
        // Scroll to app (view[1]) when layout finished.
        int scrollToViewIdx = 1;
        slidemenu.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(
                btnClick));
        //address = new ArrayList<MenuItemList>();
        address = Config.createAddress();
        menuAdapter = new MenuAdapter(context, R.layout.link, address, inflater);
        final ListView listView = (ListView) menu.findViewById(R.id.list_menu);
        img_pro_pic = (ImageView) menu.findViewById(R.id.menu_prof_img);
        rl_menu_toplayout = (RelativeLayout) menu
                .findViewById(R.id.rl_menu_toplayout);

        // img_profile_click = (ImageView) menu.findViewById(R.id.imageView1);
        txt_uName = (TextView) menu.findViewById(R.id.txt_username);

        Log.e("Profile Pic  : ", "------>" + globalApp.getPhoto());
        String uName = globalApp.getLoginusername();
        if (!TextUtils.isEmpty(uName) && !uName.equals("null")) {
            txt_uName.setText(uName);
        }
        try {

            String photoUrl;
            if (globalApp.from_fb == true) {
                photoUrl = Uri.fromFile(new File(globalApp.getPhoto())).getPath();

            } else {
                photoUrl = "http://s3.instamourapp.com/user_files/" + globalApp.getPhoto();
            }

            if (!photoUrl.equals("") || !photoUrl.equals("null")) {
                imageLoader.DisplayImage(photoUrl, img_pro_pic);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //
        // ViewUtils.initListView(this, listView, "Menu ", 8,
        // android.R.layout.simple_list_item_1);
        listView.setAdapter(menuAdapter);
        // scrollWebviw(MnuscrollView, menu);
        // menuOut = false;

        rl_menu_toplayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent(context, ProfileView.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                scrollWebviw(MnuscrollView, menu);
                menuOut = false;

            }
        });

        btnClick.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                listView.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                String session_login = globalApp.preferences.getString(
                        "session_login", "");
                if (session_login.equals("true")) {

                    int menuWidth = menu.getMeasuredWidth();
                    Log.d("===slide==", "Scroll to rightttttttttttttttt");
                    // Ensure menu is visible
                    menu.setVisibility(View.VISIBLE);

                    if (!menuOut) {
                        // Scroll to 0 to reveal menu
                        Log.d("===slide==", "Scroll to right");
                        Log.d("===clicked==", "clicked");
                        int left = 20;
                        MnuscrollView.smoothScrollTo(left, 0);
                    } else {
                        // Scroll to menuWidth so menu isn't on screen.
                        Log.d("===slide==", "Scroll to left");
                        Log.d("===clicked==", "clicked");
                        int left = menuWidth;
                        MnuscrollView.smoothScrollTo(left, 0);
                    }
                    menuOut = !menuOut;

                } else if ((session_login.equals("false"))) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(
                            context);

                    alert.setTitle("Please Sign In");
                    alert.setMessage("You must create an account or sign in to interact with the Amours");
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    Intent intent = new Intent(context,
                                            HomeScreen.class);

                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    context.startActivity(intent);

                                }
                            });

                    alert.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    dialog.cancel();
                                }
                            });
                    alert.show();

                }

            }

        });

        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                Context context = view.getContext();
                isWebHistory = true;
                menuOut = true;
                stack.push(address.get(position).getName());

                if (address.get(position).getName()
                        .equalsIgnoreCase("Your Amours")) {
                    Intent i = new Intent(context, YourAmours.class);
                    // i.setClassName(context, "com.example.instamourupdated." +
                    // address.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                } else if (address.get(position).getName()
                        .equalsIgnoreCase("Browse Profiles")) {
                    globalApp.getList().clear();
                    Intent i = new Intent(context, SampleActivity.class);
                    // i.setClassName(context, "com.example.instamourupdated." +
                    // address.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                } else if (address.get(position).getName()
                        .equalsIgnoreCase("Instant chat")) {

                    Intent i = new Intent(context, InstantChat.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);

                } else if (address.get(position).getName()
                        .equalsIgnoreCase("Create Videos")) {
                    Intent i = new Intent(context, CreateVideos.class);
                    // i.setClassName(context, "com.example.instamourupdated." +
                    // address.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                } else if (address.get(position).getName()
                        .equalsIgnoreCase("Share Instamour")) {
                    Intent i = new Intent(context, ShareInstamour.class);
                    // i.setClassName(context, "com.example.instamourupdated." +
                    // address.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                } else if (address.get(position).getName()
                        .equalsIgnoreCase("Purchase Gifts")) {
                    Intent i = new Intent(context, PurchaseGifts.class);
                    // i.setClassName(context, "com.example.instamourupdated." +
                    // address.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                } else if (address.get(position).getName()
                        .equalsIgnoreCase("Settings")) {
                    Intent i = new Intent(context, Setting.class);
                    // i.setClassName(context, "com.example.instamourupdated." +
                    // address.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                } else if (address.get(position).getName()
                        .equalsIgnoreCase("Logout")) {
                    Intent i = new Intent(context, LogoutActivity.class);
                    // i.setClassName(context, "com.example.instamourupdated." +
                    // address.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                }

                /*
                 * Intent i = new Intent(); i.setClassName(context,
                 * "com.example.instamourupdated." +
                 * address.get(position).getName());
                 * i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                 * //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 * //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                 * context.startActivity(i);
                 */

                /*
                 * if(address.get(position).getName() == "SignOut") { globalApp
                 * = ChatApplication.getInstance(); pref =
                 * PreferenceManager.getDefaultSharedPreferences(context);
                 * SharedPreferences.Editor editor =pref.edit(); editor.clear();
                 * editor.commit(); Log.e("appp before store is : ", ""+
                 * globalApp.getFullname()); globalApp.setJabber_id(0);
                 * globalApp.setUser_id(0); globalApp.setUsername("");
                 * globalApp.setPassword(""); globalApp.setFullname("");
                 * Log.e("appp after store is : ", "------>"+
                 * globalApp.getFullname()); Intent iToregistration = new
                 * Intent(context,LoginActivity.class);
                 * iToregistration.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                 * context.startActivity(iToregistration); } else {
                 */
                /*
                 * if(address.get(position).getName() == "Friends" ||
                 * address.get(position).getName() == "Home" ||
                 * address.get(position).getName() == "Group" ||
                 * address.get(position).getName() == "SignOut") { Intent i =
                 * new Intent(); i.setClassName(context,
                 * "com.moneytree.activities." +
                 * address.get(position).getName());
                 * i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                 * //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 * //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                 * context.startActivity(i); }
                 */
                // }

                scrollWebviw(MnuscrollView, menu);
                menuOut = false;
            }

        });

    }

    public void openDialog() {

        Dialog dialog = new Dialog(contxt);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.setTitle("Hello");
        TextView textViewUser = (TextView) dialog.findViewById(R.id.alert_);
        textViewUser.setText("Hi");
        dialog.show();

    }

    private void scrollWebviw(MenuSlideView scrollView, View menu) {
        Context context = menu.getContext();

        int menuWidth = menu.getMeasuredWidth();

        // Ensure menu is visible
        menu.setVisibility(View.VISIBLE);

        if (!menuOut) {
            // Scroll to 0 to reveal menu
            Log.d("===slide==", "Scroll to right");
            int left = 0;
            scrollView.smoothScrollTo(left, 0);
        } else {
            // Scroll to menuWidth so menu isn't on screen.
            Log.d("===slide==", "Scroll to left");
            int left = menuWidth;
            scrollView.smoothScrollTo(left, 0);

        }
        menuOut = false;
    }

    static class SizeCallbackForMenu implements SizeCallback {
        int btnWidth;
        ImageView btnSlide;

        public SizeCallbackForMenu(ImageView btnSlide) {
            super();
            this.btnSlide = btnSlide;
        }

        @Override
        public void onGlobalLayout() {
            btnWidth = btnSlide.getMeasuredWidth();
            System.out.println("btnWidth=" + btnWidth);
        }

        @Override
        public void getViewSize(int idx, int w, int h, int[] dims) {
            dims[0] = w;
            dims[1] = h;
            final int menuIdx = 0;
            if (idx == menuIdx) {
                dims[0] = w - btnWidth;
            }
        }
    }

}
