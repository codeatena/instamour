//
//  INSCurrentUserManager.m
//  Instamour
//
//  Created by Brian Slick on 7/28/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSUserManager.h"

// Models and Other Global

// Private Constants
NSString *const INSFileNameCurrentUser = @"User.data";
NSString *const INSFileNameYourAmours = @"YourAmours.data";
NSString *const INSFileNamePendingAmours = @"PendingAmours.data";
NSString *const INSFileNameHeartsPushed = @"HeartsPushed.data";
NSString *const INSFileNameBrowserUsers = @"BrowserUsers.data";
NSString *const INSFileNameSearchUsers = @"SearchUsers.data";

NSString *const INSUserManagerDidChangeCurrentUserDetailsNotification = @"INSUserManagerDidChangeCurrentUserDetailsNotification";
NSString *const INSUserManagerDidChangeYourAmoursNotification = @"INSUserManagerDidChangeYourAmoursNotification";
NSString *const INSUserManagerDidChangePendingAmoursNotification = @"INSUserManagerDidChangePendingAmoursNotification";
NSString *const INSUserManagerDidChangeHeartsPushedAmoursNotification = @"INSUserManagerDidChangeHeartsPushedAmoursNotification";
NSString *const INSUserManagerDidChangeBrowserUsersNotification = @"INSUserManagerDidChangeBrowserUsersNotification";
NSString *const INSUserManagerDidChangeSearchUsersNotification = @"INSUserManagerDidChangeSearchUsersNotification";

NSInteger const INSUserManagerRefreshMinutes = 10;

@interface INSUserManager ()

// Private Properties
@property (nonatomic, strong) INSUser *currentUser;

@property (nonatomic, strong) NSMutableArray *yourAmoursUsers;
@property (nonatomic, strong) NSMutableArray *pendingAmoursUsers;
@property (nonatomic, strong) NSMutableArray *heartsPushedUsers;
@property (nonatomic, strong) NSMutableArray *browserUsers;
@property (nonatomic, strong) NSMutableArray *searchUsers;

@property (nonatomic, copy) NSDate *currentUserLastRefreshDate;
@property (nonatomic, copy) NSDate *yourAmoursLastRefreshDate;
@property (nonatomic, copy) NSDate *pendingAmoursLastRefreshDate;
@property (nonatomic, copy) NSDate *heartsPushedLastRefreshDate;

@property (nonatomic, assign, getter = isSavingCurrentUser) BOOL savingCurrentUser;
@property (nonatomic, assign, getter = isSavingYourAmours) BOOL savingYourAmours;
@property (nonatomic, assign, getter = isSavingPendingAmours) BOOL savingPendingAmours;
@property (nonatomic, assign, getter = isSavingHeartsPushed) BOOL savingHeartsPushed;
@property (nonatomic, assign, getter = isSavingBrowserUsers) BOOL savingBrowserUsers;
@property (nonatomic, assign, getter = isSavingSearchUsers) BOOL savingSearchUsers;

@property (nonatomic, assign, getter = isDownloadingBrowserUsers) BOOL downloadingBrowserUsers;
@property (nonatomic, assign, getter = isDownloadingSearchUsers) BOOL downloadingSearchUsers;

@end

@implementation INSUserManager

#pragma mark - Synthesized Properties

- (NSMutableArray *)yourAmoursUsers
{
    if (_yourAmoursUsers == nil)
    {
        _yourAmoursUsers = [[NSMutableArray alloc] init];
    }
    return _yourAmoursUsers;
}

- (NSMutableArray *)pendingAmoursUsers
{
    if (_pendingAmoursUsers == nil)
    {
        _pendingAmoursUsers = [[NSMutableArray alloc] init];
    }
    return _pendingAmoursUsers;
}

- (NSMutableArray *)heartsPushedUsers
{
    if (_heartsPushedUsers == nil)
    {
        _heartsPushedUsers = [[NSMutableArray alloc] init];
    }
    return _heartsPushedUsers;
}

- (NSMutableArray *)browserUsers
{
    if (_browserUsers == nil)
    {
        _browserUsers = [[NSMutableArray alloc] init];
    }
    return _browserUsers;
}

- (NSMutableArray *)searchUsers
{
    if (_searchUsers == nil)
    {
        _searchUsers = [[NSMutableArray alloc] init];
    }
    return _searchUsers;
}

#pragma mark - Dealloc and Memory Management


#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
        [notificationCenter addObserver:self
                               selector:@selector(getUserDidSucceedNotification:)
                                   name:INSServerAPIManagerGetUserDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(yourAmoursDidSucceedNotification:)
                                   name:INSServerAPIManagerYourAmoursDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(pendingAmoursDidSucceedNotification:)
                                   name:INSServerAPIManagerPendingAmoursDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(heartsPushedDidSucceedNotification:)
                                   name:INSServerAPIManagerHeartsPushedDidFinishNotification
                                 object:nil];
        
        
        [notificationCenter addObserver:self
                               selector:@selector(downloadBrowserUsersDidSucceedNotification:)
                                   name:INSServerAPIManagerBrowserUserDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(downloadBrowserUsersFailedNotification:)
                                   name:INSServerAPIManagerBrowserUserFailedNotification
                                 object:nil];
        
        
        [notificationCenter addObserver:self
                               selector:@selector(downloadSearchUsersDidSucceedNotification:)
                                   name:INSServerAPIManagerSearchUserDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(downloadSearchUsersFailedNotification:)
                                   name:INSServerAPIManagerSearchUserFailedNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(editUserProfilePicDidSucceedNotification:)
                                   name:INSServerAPIManagerEditUserProfilePicDidFinishNotification
                                 object:nil];
        
    }
    return self;
}

#pragma mark - Custom Getters and Setters

- (INSUser *)currentUser
{
    if (_currentUser == nil)
    {
        [self loadUser];
        
        if (_currentUser != nil)
        {
            [self loadBrowserUsers];
            
            [self loadYourAmoursUsers];
            [self loadPendingAmoursUsers];
            [self loadHeartsPushedUsers];
            
            [self loadSearchUsers];
        }
    }
    return _currentUser;
}

#pragma mark - Notification Handlers

- (void)getUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    //    NSLog(@"jsonDictionary: %@", jsonDictionary);
    //    NSLog(@"userInfo: %@", [notification userInfo]);
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        NSString *errorMessage = [jsonDictionary errorMessageINS];
        
        if ( ![errorMessage isNotEmpty] || [errorMessage isEqualToString:@"Not logged in."])
        {
            [[AppDelegate sharedDelegate] logoutUser];
        }
        else
        {
//            NSLog(@"error message: %@", errorMessage);
        }
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Get User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setCurrentUserLastRefreshDate:[NSDate date]];
    
    NSDictionary *incomingUser = [jsonDictionary currentUserINS];
    NSString *incomingUserID = [incomingUser identifierForUserINS];
    NSString *existingID = [[self currentUser] userID];
    
    if (![existingID isEqualToString:incomingUserID])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not current user", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self loadUserWithGetUserJSONResult:jsonDictionary];
    
    [self refreshYourAmoursRightNow:NO];
    [self refreshPendingAmoursRightNow:NO];
    [self refreshHeartsPushedRightNow:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)yourAmoursDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Your Amours Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setYourAmoursLastRefreshDate:[NSDate date]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSArray *jsonUsers = [jsonDictionary usersINS];
        
        // Convert to dictionary for easier searching
        
        NSMutableArray *usersRemainingToSort = [NSMutableArray array];
        NSMutableDictionary *newUsersDictionary = [NSMutableDictionary dictionaryWithCapacity:[jsonUsers count]];
        
        for (NSDictionary *jsonUser in jsonUsers)
        {
            INSUser *user = [[INSUser alloc] initWithJSONUser:jsonUser];
            
            [usersRemainingToSort addObject:user];
            
            if ([[user userID] isNotEmpty])
            {
                [newUsersDictionary setObject:user forKey:[user userID]];
            }
        }
        
        // Loop over sorted Users.
        // For each User ID in old list, find the user in new list. If found, add to the list, remove from "remaining" list.
        // Add remaining users to end of the list
        
        NSArray *oldSortedUsers = [NSArray arrayWithArray:[self yourAmoursUsers]];
        NSMutableArray *newSortedUsers = [NSMutableArray array];
        
        for (INSUser *user in oldSortedUsers)
        {
            NSString *identifier = [user userID];
            
            INSUser *newUser = [newUsersDictionary objectForKey:identifier];
            
            if (newUser != nil)
            {
                [newSortedUsers addObject:newUser];
                [usersRemainingToSort removeObject:newUser];
            }
        }
        
        [newSortedUsers addObjectsFromArray:usersRemainingToSort];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [[self yourAmoursUsers] removeAllObjects];
            [[self yourAmoursUsers] addObjectsFromArray:newSortedUsers];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeYourAmoursNotification];
            
            [self saveYourAmoursUsers];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pendingAmoursDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
//    NSLog(@"Pending Amours Incoming: %@", jsonDictionary);
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Pending Amours Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSArray *jsonUsers = [jsonDictionary usersINS];
    
    NSMutableArray *newUsers = [NSMutableArray arrayWithCapacity:[jsonUsers count]];
    
    for (NSDictionary *jsonUser in jsonUsers)
    {
        INSUser *user = [[INSUser alloc] initWithJSONUser:jsonUser];
        
        [newUsers addObject:user];
    }
    
    [[self pendingAmoursUsers] removeAllObjects];
    [[self pendingAmoursUsers] addObjectsFromArray:newUsers];
    
//    NSLog(@"Pending Amours Processed: %@", [self pendingAmoursUsers]);

    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangePendingAmoursNotification];
    
    [self savePendingAmoursUsers];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)heartsPushedDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Hearts Pushed Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSArray *jsonUsers = [jsonDictionary usersINS];
    
    NSMutableArray *newUsers = [NSMutableArray arrayWithCapacity:[jsonUsers count]];
    
    for (NSDictionary *jsonUser in jsonUsers)
    {
        INSUser *user = [[INSUser alloc] initWithJSONUser:jsonUser];
        
        [newUsers addObject:user];
    }
    
    [[self heartsPushedUsers] removeAllObjects];
    [[self heartsPushedUsers] addObjectsFromArray:newUsers];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeHeartsPushedAmoursNotification];
    
    [self saveHeartsPushedUsers];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadBrowserUsersDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setDownloadingBrowserUsers:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeBrowserUsersNotification];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Browse Profiles Error", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSDictionary *userInfo = [notification userInfo];
    NSInteger pageNumber = [[userInfo objectForKey:INSAPIKeyPageNumber] integerValue];
    NSInteger numberOfResults = [[userInfo objectForKey:INSAPIKeyCount] integerValue];
    
    NSMutableArray *existingUsers = [NSMutableArray arrayWithArray:[self browserUsers]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSArray *jsonUsers = [jsonDictionary usersINS];
        NSMutableArray *newUsers = [NSMutableArray arrayWithCapacity:[jsonUsers count]];
        
        for (NSDictionary *jsonUser in jsonUsers)
        {
            INSUser *user = [[INSUser alloc] initWithJSONUser:jsonUser];
            
            [newUsers addObject:user];
        }
        
        NSRange range = [self rangeForPageNumber:pageNumber pageCount:numberOfResults];
        
        if (NSMaxRange(range) >= [existingUsers count])
        {
            [existingUsers addObjectsFromArray:newUsers];
        }
        else
        {
            [existingUsers replaceObjectsInRange:range withObjectsFromArray:newUsers];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setBrowserUsers:existingUsers];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeBrowserUsersNotification];
            
            [self saveBrowserUsers];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadBrowserUsersFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setDownloadingBrowserUsers:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeBrowserUsersNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadSearchUsersDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setDownloadingSearchUsers:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    NSDictionary *jsonDictionary = [notification object];
    
//    NSLog(@"jsonDictionary: %@", jsonDictionary);
//    NSLog(@"parameters: %@", [notification userInfo]);
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeSearchUsersNotification];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Browse Profiles Error", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSDictionary *userInfo = [notification userInfo];
    NSInteger pageNumber = [[userInfo objectForKey:INSAPIKeyPageNumber] integerValue];
    NSInteger numberOfResults = [[userInfo objectForKey:INSAPIKeyCount] integerValue];
    
    NSMutableArray *existingUsers = [NSMutableArray arrayWithArray:[self searchUsers]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSArray *jsonUsers = [jsonDictionary usersINS];
        NSMutableArray *newUsers = [NSMutableArray arrayWithCapacity:[jsonUsers count]];
        
        for (NSDictionary *jsonUser in jsonUsers)
        {
            INSUser *user = [[INSUser alloc] initWithJSONUser:jsonUser];
            
            [newUsers addObject:user];
        }
        
        NSRange range = [self rangeForPageNumber:pageNumber pageCount:numberOfResults];
        
        if (NSMaxRange(range) >= [existingUsers count])
        {
            [existingUsers addObjectsFromArray:newUsers];
        }
        else
        {
            [existingUsers replaceObjectsInRange:range withObjectsFromArray:newUsers];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setSearchUsers:existingUsers];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeSearchUsersNotification];
            
            [self saveSearchUsers];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadSearchUsersFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setDownloadingSearchUsers:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeSearchUsersNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)editUserProfilePicDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Edit User Profile Pic Error", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Master Methods

- (void)loginWithSessionID:(NSString *)sessionID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![sessionID isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No session ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *user = [[INSUser alloc] init];
    [user setSessionID:sessionID];
    
    [self setCurrentUser:user];
    
    [self saveUser];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeCurrentUserDetailsNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadUserWithGetUserJSONResult:(NSDictionary *)jsonDictionary
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *incomingUser = [jsonDictionary currentUserINS];
    
    [[self currentUser] loadWithJSONUser:incomingUser];
    
    [self saveUser];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeCurrentUserDetailsNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)logout
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setCurrentUser:nil];
    
    [self clearBrowserUsers];
    [self clearSearchUsers];
    
    [self setYourAmoursUsers:nil];
    [self setPendingAmoursUsers:nil];
    [self setHeartsPushedUsers:nil];
    
    [self setCurrentUserLastRefreshDate:nil];
    [self setYourAmoursLastRefreshDate:nil];
    [self setPendingAmoursUsers:nil];
    [self setHeartsPushedLastRefreshDate:nil];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *fileNames = @[ INSFileNameCurrentUser, INSFileNameYourAmours, INSFileNamePendingAmours, INSFileNameHeartsPushed, INSFileNameBrowserUsers, INSFileNameSearchUsers ];
    
    for (NSString *fileName in fileNames)
    {
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:fileName];
        
        if ([fileManager fileExistsAtPath:[fileURL path]])
        {
            [fileManager removeItemAtPathBTI:[fileURL path]];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeCurrentUserDetailsNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)isLoggedIn
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return [[[self currentUser] sessionID] isNotEmpty];
}

#pragma mark - Current User Methods

- (void)saveUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self currentUser] == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No user to save", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ([self isSavingCurrentUser])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already saving", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setSavingCurrentUser:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[self currentUser]];
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameCurrentUser];
        
        [data writeToURL:fileURL atomically:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setSavingCurrentUser:NO];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameCurrentUser];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No data file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    INSUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (![user isKindOfClass:[INSUser class]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid saved data", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setCurrentUser:user];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeCurrentUserDetailsNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)refreshCurrentUserDataRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![self isLoggedIn])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSDate *lastDownloadDate = [self currentUserLastRefreshDate];
    NSDate *rightNow = [NSDate date];
    
    NSTimeInterval timeInterval = [rightNow timeIntervalSinceDate:lastDownloadDate];
    
    if ( !isForcedRefresh && (timeInterval < 60 * INSUserManagerRefreshMinutes) )
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Low-priority refresh", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSServerAPIManager sharedManager] getUserWithUserName:nil
                                                    orUserID:[[self currentUser] userID]];
    
    if (isForcedRefresh)
    {
        [self refreshYourAmoursRightNow:YES];
        [self refreshPendingAmoursRightNow:YES];
        [self refreshHeartsPushedRightNow:YES];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)isCurrentUser:(INSUser *)user
{
    INSUser *currentUser = [self currentUser];
    
    return ([[currentUser userID] isEqualToString:[user userID]]);
}

#pragma mark - Your Amours Methods

- (void)refreshYourAmoursRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDate *lastDownloadDate = [self yourAmoursLastRefreshDate];
    NSDate *rightNow = [NSDate date];
    
    NSTimeInterval timeInterval = [rightNow timeIntervalSinceDate:lastDownloadDate];
    
    if ( !isForcedRefresh && (timeInterval < 60 * (INSUserManagerRefreshMinutes - 1)) )     // A little padding to adjust for user-get delays
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Refresh Limit", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSServerAPIManager sharedManager] downloadYourAmours];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveYourAmoursUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isSavingYourAmours])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already saving", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setSavingYourAmours:YES];
    
    NSMutableArray *arrayToSave = [NSMutableArray arrayWithArray:[self yourAmoursUsers]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayToSave];
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameYourAmours];
        
        [data writeToURL:fileURL atomically:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setSavingYourAmours:NO];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadYourAmoursUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameYourAmours];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        if (![fileManager fileExistsAtPath:[fileURL path]])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No data file", self, __PRETTY_FUNCTION__);
            return;
        }
        
        NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
        
        NSMutableArray *users = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
//            NSLog(@"Done loading Your Amours");
            
            [self setYourAmoursUsers:users];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeYourAmoursNotification];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (INSUser *)amourWithQuickBloxID:(NSInteger)quickBloxID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *userToReturn = nil;
    
    NSArray *amours = [NSArray arrayWithArray:[self yourAmoursUsers]];
    
    for (INSUser *user in amours)
    {
        if ([[user quickBloxID] integerValue] == quickBloxID)
        {
            userToReturn = user;
            break;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return userToReturn;
}

- (INSUser *)amourWithUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *userToReturn = nil;
    
    NSArray *amours = [NSArray arrayWithArray:[self yourAmoursUsers]];
    
    for (INSUser *user in amours)
    {
        if ([[user userID] isEqualToString:userID])
        {
            userToReturn = user;
            break;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return userToReturn;
}

- (BOOL)isAmourUser:(INSUser *)user
{
    INSUser *amourUser = [self amourWithUserID:[user userID]];
    
    return (amourUser != nil);
}

#pragma mark - Pending Amours Methods

- (void)refreshPendingAmoursRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDate *lastDownloadDate = [self pendingAmoursLastRefreshDate];
    NSDate *rightNow = [NSDate date];
    
    NSTimeInterval timeInterval = [rightNow timeIntervalSinceDate:lastDownloadDate];
    
    if ( !isForcedRefresh && (timeInterval < 60 * (INSUserManagerRefreshMinutes - 1)) )     // A little padding to adjust for user-get delays
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Refresh Limit", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSServerAPIManager sharedManager] downloadPendingAmours];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)savePendingAmoursUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isSavingPendingAmours])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already saving", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setSavingPendingAmours:YES];
    
    NSMutableArray *arrayToSave = [NSMutableArray arrayWithArray:[self pendingAmoursUsers]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayToSave];
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNamePendingAmours];
        
        [data writeToURL:fileURL atomically:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setSavingPendingAmours:NO];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadPendingAmoursUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNamePendingAmours];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        if (![fileManager fileExistsAtPath:[fileURL path]])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No data file", self, __PRETTY_FUNCTION__);
            return;
        }
        
        NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
        
        NSMutableArray *users = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
//            NSLog(@"Done loading Pending Amours");
            
            [self setPendingAmoursUsers:users];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangePendingAmoursNotification];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)isPendingAmourUser:(INSUser *)user
{
    BOOL isFound = NO;
    
    for (INSUser *heartPushUser in [self pendingAmoursUsers])
    {
        if ([[heartPushUser userID] isEqualToString:[user userID]])
        {
            isFound = YES;
            break;
        }
    }
    
    return isFound;
}

- (void)movePendingAmourUserToAmours:(INSUser *)user
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self yourAmoursUsers] addObject:user];
    
    NSMutableArray *pendingAmoursToRemove = [NSMutableArray array];
    
    for (INSUser *pendingAmourUser in [self pendingAmoursUsers])
    {
        if ([[pendingAmourUser userID] isEqualToString:[user userID]])
        {
            [pendingAmoursToRemove addObject:pendingAmourUser];
        }
    }
    
    [[self pendingAmoursUsers] removeObjectsInArray:pendingAmoursToRemove];

    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeYourAmoursNotification];
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangePendingAmoursNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Hearts Pushed Methods

- (void)refreshHeartsPushedRightNow:(BOOL)isForcedRefresh;      // Use NO to respect a server call frequency limit. YES to call server now.
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDate *lastDownloadDate = [self heartsPushedLastRefreshDate];
    NSDate *rightNow = [NSDate date];
    
    NSTimeInterval timeInterval = [rightNow timeIntervalSinceDate:lastDownloadDate];
    
    if ( !isForcedRefresh && (timeInterval < 60 * (INSUserManagerRefreshMinutes - 1)) )     // A little padding to adjust for user-get delays
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Refresh Limit", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSServerAPIManager sharedManager] downloadHeartsPushed];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveHeartsPushedUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isSavingHeartsPushed])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already saving", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setSavingHeartsPushed:YES];
    
    NSMutableArray *arrayToSave = [NSMutableArray arrayWithArray:[self heartsPushedUsers]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayToSave];
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameHeartsPushed];
        
        [data writeToURL:fileURL atomically:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setSavingHeartsPushed:NO];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadHeartsPushedUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameHeartsPushed];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        if (![fileManager fileExistsAtPath:[fileURL path]])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No data file", self, __PRETTY_FUNCTION__);
            return;
        }
        
        NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
        
        NSMutableArray *users = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
//            NSLog(@"Done loading Hearts Pushed");
            
            [self setHeartsPushedUsers:users];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeHeartsPushedAmoursNotification];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)isHeartPushedUser:(INSUser *)user
{
    BOOL isFound = NO;
    
    for (INSUser *heartPushUser in [self heartsPushedUsers])
    {
        if ([[heartPushUser userID] isEqualToString:[user userID]])
        {
            isFound = YES;
            break;
        }
    }
    
    return isFound;
}

#pragma mark - Browser User Methods

- (void)clearBrowserUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setBrowserUsers:nil];
    
    //    [self saveBrowserUsers];
    
    //    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeBrowserUsersNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadMoreBrowserUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isDownloadingBrowserUsers])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already downloading", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setBrowserGenderSearchCriteriaIfNecessaryINS];
    
    [self setDownloadingBrowserUsers:YES];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSMutableArray *currentUsers = [self browserUsers];
    
    NSInteger currentPageNumber = [self maxPageNumberOfArray:currentUsers forPageCount:INSNumberOfDownloadedProfilesPerPage];
    
//    NSLog(@"Downloading for page: %ld", (long)currentPageNumber + 1);
    
    [[INSServerAPIManager sharedManager] downloadBrowserUsersForPageNumber:currentPageNumber + 1
                                                           numberOfResults:INSNumberOfDownloadedProfilesPerPage];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveBrowserUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isSavingBrowserUsers])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already saving", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setSavingBrowserUsers:YES];
    
    NSMutableArray *arrayToSave = [NSMutableArray arrayWithArray:[self browserUsers]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayToSave];
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameBrowserUsers];
        
        [data writeToURL:fileURL atomically:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setSavingBrowserUsers:NO];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadBrowserUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameBrowserUsers];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        if (![fileManager fileExistsAtPath:[fileURL path]])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No data file", self, __PRETTY_FUNCTION__);
            return;
        }
        
        NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
        
        NSMutableArray *users = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
//            NSLog(@"Done loading Browser Users");
            
            [self setBrowserUsers:users];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeBrowserUsersNotification];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Search User Methods

- (void)clearSearchUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setSearchUsers:nil];
    //    [[self searchUsers] removeAllObjects];
    
    //    [self saveSearchUsers];
    
    //    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeSearchUsersNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadMoreSearchUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isDownloadingSearchUsers])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already downloading", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setDownloadingSearchUsers:YES];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSMutableArray *currentUsers = [self searchUsers];
    
    NSInteger currentPageNumber = [self maxPageNumberOfArray:currentUsers forPageCount:INSNumberOfDownloadedProfilesPerPage];
    
//    NSLog(@"Downloading for page: %ld", (long)currentPageNumber + 1);
    
    [[INSServerAPIManager sharedManager] downloadSearchUsersForPageNumber:currentPageNumber + 1
                                                          numberOfResults:INSNumberOfDownloadedProfilesPerPage];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveSearchUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isSavingSearchUsers])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already saving", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setSavingSearchUsers:YES];
    
    NSMutableArray *arrayToSave = [NSMutableArray arrayWithArray:[self searchUsers]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayToSave];
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameSearchUsers];
        
        [data writeToURL:fileURL atomically:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self setSavingSearchUsers:NO];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadSearchUsers
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSFileNameSearchUsers];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        if (![fileManager fileExistsAtPath:[fileURL path]])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No data file", self, __PRETTY_FUNCTION__);
            return;
        }
        
        NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
        
        NSMutableArray *users = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
//            NSLog(@"Done loading Search Users");
            
            [self setSearchUsers:users];
            
            [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeSearchUsersNotification];
            
        });
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (NSInteger)maxPageNumberOfArray:(NSArray *)array
                     forPageCount:(NSInteger)count
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger total = [array count];
    
    NSInteger numberOfFilledPages = total / count;      // Integer division deliberately
    
    NSInteger pageNumber = numberOfFilledPages + 1;
    
    if ([array count] == 0)
    {
        pageNumber = 0;
    }
    
//    NSLog(@"page number: %ld", (long)pageNumber);
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return pageNumber;
}

- (NSRange)rangeForPageNumber:(NSInteger)pageNumber
                    pageCount:(NSInteger)pageCount
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger indexStart = (pageNumber - 1) * pageCount;
    
    NSRange range = NSMakeRange(indexStart, pageCount);
    
//    NSLog(@"range: %@", NSStringFromRange(range));
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return range;
}

- (void)removeUserFromAllLists:(INSUser *)userToRemove
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self removeUserWithIDFromAllLists:[userToRemove userID]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)removeUserWithIDFromAllLists:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![userID isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No User ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSMutableArray *usersToRemove = [NSMutableArray array];

    NSArray *sourceArrays = @[ [self browserUsers], [self searchUsers], [self yourAmoursUsers], [self pendingAmoursUsers], [self heartsPushedUsers] ];
    
    for (NSMutableArray *sourceArray in sourceArrays)
    {
        for (INSUser *user in sourceArray)
        {
            if ([[user userID] isEqualToString:userID])
            {
                [usersToRemove addObject:user];
            }
        }
    }
    
    for (NSMutableArray *sourceArray in sourceArrays)
    {
        [sourceArray removeObjectsInArray:usersToRemove];
    }
    
    // Comments
    
    NSMutableArray *commentsToRemove = [NSMutableArray array];
    
    NSMutableArray *comments = [[self currentUser] comments];
    
    for (INSComment *comment in comments)
    {
        if ([[comment senderID] isEqualToString:userID])
        {
            [commentsToRemove addObject:comment];
        }
    }
    
    [comments removeObjectsInArray:commentsToRemove];
    
    // Publish changes
    
    if ( ([usersToRemove count] > 0) || ([commentsToRemove count] > 0) )
    {
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSUserManagerDidChangeCurrentUserDetailsNotification];
    }
    
    [[INSChatDataManager sharedManager] removeChatConversationForUserID:userID];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Singleton

+ (instancetype)sharedManager
{
	static dispatch_once_t pred;
	static INSUserManager *shared = nil;
    
	dispatch_once(&pred, ^{
        
		shared = [[[self class] alloc] init];
        
	});
    
	return shared;
}


@end
