//
//  INSYourAmoursTableCell.h
//  Instamour
//
//  Created by Brian Slick on 7/19/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols
@protocol INSINSYourAmoursTableCellDelegate;

@interface INSYourAmoursTableCell : BTITableViewCell

// Public Properties

@property (nonatomic, strong, readonly) UIImageView *avatarImageView;

@property (nonatomic, weak) id<INSINSYourAmoursTableCellDelegate> delegate;

// Public Methods

- (void)setUserName:(NSString *)userName;
- (void)setAudioButtonEnabled:(BOOL)isEnabled;
- (void)setVideoButtonEnabled:(BOOL)isEnabled;

@end

@protocol INSINSYourAmoursTableCellDelegate <NSObject>

@required
- (void)yourAmoursCellChatButtonPressed:(INSYourAmoursTableCell *)yourAmoursCell;
- (void)yourAmoursCellAudioButtonPressed:(INSYourAmoursTableCell *)yourAmoursCell;
- (void)yourAmoursCellVideoButtonPressed:(INSYourAmoursTableCell *)yourAmoursCell;

@end
