
package com.hyper.instamour;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.easy.facebook.android.apicall.GraphApi;
import com.easy.facebook.android.data.User;
import com.easy.facebook.android.error.EasyFacebookError;
import com.easy.facebook.android.facebook.FBLoginManager;
import com.easy.facebook.android.facebook.Facebook;
import com.easy.facebook.android.facebook.LoginListener;
import com.flurry.android.FlurryAgent;
import com.quickblox.core.QBCallback;
import com.quickblox.core.result.Result;
import com.quickblox.internal.core.exception.BaseServiceException;
import com.quickblox.internal.core.server.BaseService;
import com.quickblox.module.auth.model.QBProvider;
import com.quickblox.module.users.QBUsers;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.users.result.QBUserResult;

public class Login extends Activity implements LoginListener {
    private EditText edt_loginemail;
    private EditText edt_loginpass;
    private Boolean checkData = false;
    private List<NameValuePair> param;
    private String signup_city;
    private String Jabberid;
    boolean status = false;
    private ProgressDialog pd_signUp;
    private static boolean islogin = false;
    public final static String KODEFUNFBAPP_ID = "219338071544861";
    Function c = new Function();
    String getEmail, getPassword, passResult;;
    Global global = Global.getInstance();
    private String facebookAcceessToken;
    private String result;
    private FBLoginManager fbLoginManager;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.hyper.instamour", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        global = Global.getInstance();
        setContentView(R.layout.login);
        global.preferences = getSharedPreferences("LoginPrefrence", MODE_PRIVATE);
        global.editor = global.preferences.edit();
        global.editor.putString("session_login", "false");
        global.editor.commit();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        edt_loginemail = (EditText) findViewById(R.id.login_email);
        edt_loginpass = (EditText) findViewById(R.id.login_password);

        // for debugging purpose
        //     edt_loginemail.setText("kiddkevin000@gmail.com");
        //   edt_loginpass.setText("test1234");

        Random rand = new Random();
        int string = rand.nextInt(10);
        Log.e("fjdslkf", Integer.toString(string));

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_cancel:
                finish();
                break;
            case R.id.login_go:
                getEmail = edt_loginemail.getText().toString().trim();
                getPassword = edt_loginpass.getText().toString().trim();
                float s = edt_loginemail.getTextSize();
                Log.e("text size ", String.valueOf(s) + "--");

                if (TextUtils.isEmpty(getEmail)) {
                    Log.e("u r in a ", "emailid");
                    edt_loginemail.setError(getString(R.string.blankEmail));
                    edt_loginemail.requestFocus();
                    checkData = true;

                }
                if (TextUtils.isEmpty(getPassword)) {
                    Log.e("u r in a ", "password");
                    edt_loginpass.setError(getString(R.string.blankPassword));
                    edt_loginpass.requestFocus();
                    checkData = true;

                }
                /*
                 * if (getPassword.length() < 1) { Log.e("u r in a ",
                 * "password");
                 * edt_loginpass.setError(getString(R.string.minimumpwd));
                 * edt_loginpass.requestFocus(); checkData = true; }
                 */
                if (!checkData) {

                    boolean isonline = c
                            .haveNetworkConnection(getApplicationContext());
                    if (isonline == true) {
                        new AsyncAction().execute(null, null, null);

                    } else {
                        AlertDialog.Builder dialog2 = new AlertDialog.Builder(
                                Login.this);
                        dialog2.setTitle("Instamour");
                        dialog2.setMessage("No Data Connection Avaible");
                        dialog2.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        // TODO Auto-generated method stub

                                    }
                                });
                        AlertDialog alert2 = dialog2.create();
                        alert2.show();
                    }
                    Log.e("Login SucessFully Done", "Login SucessFully Done.......");
                    /* } */

                } else {
                    checkData = false;
                    Log.e("please fill all the data", "please fill all the data");
                }

                break;
            case R.id.login_facebook:

                try {
                    // connectToFacebook();
                    if (c.haveNetworkConnection(getApplicationContext())) {
                        myConnectFacebook();
                    } else {
                        AlertDialog.Builder dialog2 = new AlertDialog.Builder(
                                Login.this);
                        dialog2.setTitle("Instamour");
                        dialog2.setMessage("No Data Connection Avaible");
                        dialog2.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        // TODO Auto-generated method stub

                                    }
                                });
                        AlertDialog alert2 = dialog2.create();
                        alert2.show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                break;
            case R.id.forget_password:

                forgetpassword();
                break;
        }

    }

    @SuppressLint("NewApi")
    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void forgetpassword() {

        final AlertDialog.Builder alert = new AlertDialog.Builder(Login.this);
        final EditText input = new EditText(Login.this);
        final String value;
        alert.setView(input);
        alert.setTitle("Forgot Password");
        alert.setMessage("Enter Your Email");
        value = input.getText().toString().trim();
        alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                runOnUiThread(new Runnable() {
                    public void run() {
                        if (TextUtils.isEmpty(value)) {
                            Toast.makeText(getApplicationContext(),
                                    "Email id  empty", Toast.LENGTH_LONG)
                                    .show();
                        }

                    }
                });
                Toast.makeText(getApplicationContext(), value,
                        Toast.LENGTH_SHORT).show();

                new AsyncActionPass()
                        .execute(input.getText().toString().trim());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        alert.show();

    }

    private class AsyncActionPass extends AsyncTask<String, Void, String> {
        public boolean status = false;
        private ProgressDialog pd;

        protected String doInBackground(String... arg0) {
            // TODO Auto-generated method stub
            try {

                password(arg0[0].toString());
                status = true;

            } catch (Exception e) {
                // TODO: handle exception
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            pd.dismiss();

            Toast.makeText(getApplicationContext(), passResult,
                    Toast.LENGTH_LONG).show();

        }

        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pd = new ProgressDialog(Login.this);
            pd.setMessage("loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
        }

    }

    @SuppressLint("NewApi")
    private void password(String email) {
        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("m", "user-reset-pwd"));
        param.add(new BasicNameValuePair("email", email));
        // param.add(new BasicNameValuePair("fbid",fbid));
        JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
        Log.e("Reset password", json + "");

    }

    public void myConnectFacebook() {
        String permissions[] = {

                "user_about_me",
                "user_location",
                "email", "user_photos", "user_interests", "user_online_presence",
                "xmpp_login", "offline_access", "publish_actions",
                "user_photo_video_tags", "user_photos",

                "publish_checkins", "publish_stream"
        };

        fbLoginManager = new FBLoginManager(this, R.layout.login,
                KODEFUNFBAPP_ID, permissions);
        if (fbLoginManager.existsSavedFacebook()) {
            fbLoginManager.loadFacebook();
        } else {
            fbLoginManager.login();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
            android.content.Intent data) {

        if (resultCode == RESULT_OK) {

            fbLoginManager.loginSuccess(data);

        } else if (resultCode == RESULT_CANCELED) {

            Toast.makeText(getApplicationContext(), "Cancelled",
                    Toast.LENGTH_SHORT).show();
            return;

        }

    }

    @SuppressLint("NewApi")
    public void loginSuccess(Facebook facebook) {

        pd_signUp = new ProgressDialog(Login.this);
        pd_signUp.setMessage("Please Wait....");
        pd_signUp.setCancelable(false);
        pd_signUp.show();
        User user = null;

        try {
            GraphApi graphApi = new GraphApi(facebook);
            user = new User();
            user = graphApi.getMyAccountInfo();
            facebookAcceessToken = facebook.getAccessToken();
            Log.e("access token", "--" + facebookAcceessToken);
            Log.e("user info", "--->" + user.toString());
            global.editor.putString("validsignup", "true");

            GPSTracker gps;
            double latitude = 0;
            double longitude = 0;
            gps = new GPSTracker(Login.this);
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                global.editor.putString("lat", String.valueOf(latitude));
                global.editor.putString("lng", String.valueOf(longitude));

                // \n is for new line
                try {
                    Geocoder gcd = new Geocoder(getApplicationContext(),
                            Locale.getDefault());
                    List<Address> addresses = gcd.getFromLocation(latitude,
                            longitude, 1);
                    if (addresses.size() > 0)
                        System.out.println(addresses.get(0).getLocality());
                    /*
                     * Toast.makeText(getApplicationContext(),
                     * addresses.get(0).getLocality(), Toast.LENGTH_LONG)
                     * .show();
                     */
                    signup_city = addresses.get(0).getLocality();
                } catch (Exception e) {

                }

            } else {

                gps.showSettingsAlert();
            }

            final String fbid = user.getId();
            final String email = user.getEmail();
            final String uname = user.getFirst_name() + " " + user.getLast_name();

            getEmail = email;
            getPassword = "instamourapp";

            DownloadImage(fbid);

            Log.e("Facebook details", fbid + "-" + email + "-" + uname);
            try {
                QBUsers.signInUsingSocialProvider(QBProvider.FACEBOOK,
                        facebookAcceessToken, null, new QBCallback() {

                            @Override
                            public void onComplete(Result arg0, Object arg1) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onComplete(Result result) {
                                // TODO Auto-generated method stub
                                if (result.isSuccess()) {

                                    QBUser qbUserResult = ((QBUserResult) result)
                                            .getUser();
                                    try {
                                        qbUserResult.setPassword(BaseService
                                                .getBaseService().getToken());
                                        qbUserResult.setLogin(qbUserResult
                                                .getLogin());

                                        global.setPassword(qbUserResult
                                                .getPassword());
                                        global.setLoginusername(uname);
                                        global.setFbLoginUsername(qbUserResult
                                                .getLogin());

                                        global.from_fb = true;
                                        global.setQbuser(qbUserResult);
                                        Log.e("Passord and login is : ",
                                                "----->"
                                                        + qbUserResult
                                                                .getPassword()
                                                        + "----->"
                                                        + qbUserResult
                                                                .getLogin());
                                        global.editor.putString("fbuname",
                                                qbUserResult.getLogin());
                                        global.editor.putBoolean("from_fb",
                                                true);
                                        global.editor.putString("uname", uname);
                                        global.editor.putString("password",
                                                qbUserResult.getPassword());
                                        global.editor.commit();
                                        /*
                                         * global.setPassword(qbUserResult.
                                         * getPassword ()) ; g
                                         * global.setQbuser(qbUserResult);
                                         */
                                    } catch (BaseServiceException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }

                                    Jabberid = qbUserResult.getId().toString();
                                    global.setJabberid(Jabberid);
                                    global.editor.putString("jabberId",
                                            Jabberid);
                                    global.editor.commit();
                                    Log.e("Jabber id is  : ", "------->"
                                            + Jabberid);
                                    String session_login = global.preferences
                                            .getString("session_login", "");
                                    if (Jabberid.equals("") || Jabberid == "") {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                "Feed action request limit reached",
                                                Toast.LENGTH_SHORT).show();

                                    } else {

                                        pd_signUp.dismiss();
                                        if (session_login
                                                .equalsIgnoreCase("false")) {
                                            param = new ArrayList<NameValuePair>();
                                            param.add(new BasicNameValuePair(
                                                    "m", "user-add"));

                                            param.add(new BasicNameValuePair(
                                                    "pwd", qbUserResult
                                                            .getPassword()));

                                            param.add(new BasicNameValuePair(
                                                    "email", email));
                                            param.add(new BasicNameValuePair(
                                                    "fbid", fbid));
                                            param.add(new BasicNameValuePair(
                                                    "chatid", Jabberid));
                                            param.add(new BasicNameValuePair(
                                                    "uname", uname));

                                            param.add(new BasicNameValuePair(
                                                    "qbpass", "instamour"));

                                            JSONObject json = c
                                                    .makeHttpRequest(
                                                            c.link.trim(),
                                                            "POST", param);
                                            Log.e("state1 fb", json + "");

                                            try {

                                                if (json.get("state").equals(
                                                        "fail")) {
                                                    param = new ArrayList<NameValuePair>();
                                                    param.add(new BasicNameValuePair(
                                                            "m", "login"));
                                                    param.add(new BasicNameValuePair(
                                                            "fbid", fbid));
                                                    Log.e("fbid", fbid);
                                                    JSONObject json1 = c.makeHttpRequest(
                                                            c.link.trim(),
                                                            "POST", param);
                                                    Log.e("state 2", json1 + "");
                                                    if (json1.get("state")
                                                            .equals("success")) {
                                                        String sessionID = json1
                                                                .getString("sessionId");
                                                        Log.e("Session id ",
                                                                json1.get("sessionId"
                                                                        .toString())
                                                                        + "");
                                                        global.editor
                                                                .putString(
                                                                        "validlogin",
                                                                        "true");
                                                        global.editor
                                                                .putString(
                                                                        "uid",
                                                                        String.valueOf(sessionID));
                                                        global.editor
                                                                .putString(
                                                                        "uname",
                                                                        uname);

                                                        global.editor
                                                                .putString(
                                                                        "session_login",
                                                                        String.valueOf("true"));
                                                        global.editor.commit();
                                                        Intent intent = new Intent(
                                                                Login.this,
                                                                SampleActivity.class);
                                                        startActivity(intent);

                                                    } else {
                                                        Toast.makeText(
                                                                getApplicationContext(),
                                                                "Username doesn't exists, Please Sign up or try with different username.",
                                                                Toast.LENGTH_SHORT)
                                                                .show();
                                                        global.editor
                                                                .putString(
                                                                        "validlogin",
                                                                        "false");
                                                        global.editor
                                                                .putString(
                                                                        "session_login",
                                                                        String.valueOf("false"));
                                                        global.editor.commit();
                                                        status = false;

                                                    }
                                                } else if (json.get("state")
                                                        .equals("success")) {

                                                    Log.e("result success",
                                                            "result succcess");
                                                    String sessionID = json
                                                            .getString("sessionId");
                                                    global.editor.putString(
                                                            "validlogin",
                                                            "true");
                                                    global.editor.putString(
                                                            "uid",
                                                            String.valueOf(sessionID));
                                                    global.editor.putString(
                                                            "uname", uname);

                                                    global.editor
                                                            .putString(
                                                                    "session_login",
                                                                    String.valueOf("true"));
                                                    global.editor.commit();
                                                    status = true;
                                                    Intent intent = new Intent(
                                                            Login.this,
                                                            SampleActivity.class);
                                                    startActivity(intent);
                                                }

                                                /*
                                                 * if (status) { Intent intent =
                                                 * new Intent( Login.this,
                                                 * SampleActivity.class);
                                                 * startActivity(intent); }
                                                 */

                                            } catch (JSONException e) {

                                                e.printStackTrace();
                                            }

                                        } else if (session_login
                                                .equalsIgnoreCase("true")) {
                                            Intent intent = new Intent(
                                                    Login.this,
                                                    SampleActivity.class);

                                            startActivity(intent);

                                        }
                                    }

                                } else {
                                    Log.e("Errors", result.getErrors()
                                            .toString());
                                }
                            }
                        });
            } catch (Exception e) {

                // TODO: handle exception
                Toast.makeText(Login.this,
                        "Sorry ! Unable to login using the facebook ",
                        Toast.LENGTH_LONG).show();
                pd_signUp.dismiss();
            }

        } catch (EasyFacebookError e) {
            Log.e("Sorry ! Unable to login using the facebook ", e.toString());
            Toast.makeText(Login.this,
                    "Sorry ! Unable to login using the facebook ",
                    Toast.LENGTH_LONG).show();
            pd_signUp.dismiss();
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Sorry ! Unable to login using the facebook ", e.toString());
            Toast.makeText(Login.this,
                    "Sorry ! Unable to login using the facebook ",
                    Toast.LENGTH_LONG).show();
            pd_signUp.dismiss();
        }

    }

    public void logoutSuccess() {
        fbLoginManager.displayToast("Logout Success!");
    }

    public void loginFail() {
        fbLoginManager.displayToast("Login Epic Failed!");
    }

    public class AsyncAction extends AsyncTask<Void, Void, Void> {

        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pd = new ProgressDialog(Login.this);
            pd.setMessage("Logging in wait...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                getLogin();

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            pd.dismiss();

            if (status) {
                Intent intent = new Intent(Login.this, SampleActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        Login.this);
                builder.setMessage("Invalid login credential")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                            int id) {
                                        dialog.dismiss();
                                        edt_loginemail.setText("");
                                        edt_loginpass.setText("");
                                        edt_loginemail.requestFocus();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();

            }

        }
    }

    public void getLogin() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //        getEmail = edt_loginemail.getText().toString().trim();
        //        getPassword = edt_loginpass.getText().toString().trim();

        Date cDate = new Date();
        String lastOnlineDate = new SimpleDateFormat("yyyy-MM-dd")
                .format(cDate);

        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("m", "login"));
        param.add(new BasicNameValuePair("email", getEmail));
        param.add(new BasicNameValuePair("pwd", getPassword));
        // param.add(new BasicNameValuePair("fbid",fbid));
        JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
        Log.e("login parameter", json + "");
        try {
            String state = json.getString("state");
            String sessionID = json.getString("sessionId");
            Log.e("state", state);
            if (json.get("state").equals("success")) {
                Log.e("Session id ", json.get("sessionId".toString()) + "");
                global.editor.putString("validlogin", "true");
                global.editor.putString("password", getPassword);
                global.setPassword(getPassword);
                global.editor.putString("uid", String.valueOf(sessionID));
                global.editor.putString("session_login", String.valueOf("true"));
                global.is_phone_checked = false;
                global.is_video_checked = false;
                global.is_account_checked = true;
                global.editor.commit();
                status = true;

            } else {
                global.editor.putString("validlogin", "false");
                global.editor.putString("session_login", String.valueOf("false"));
                global.editor.commit();

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void DownloadImage(String fbid) {
        String videoUrlInDownload = null;
        File ReceiveFileVideo = null;
        // downloadFile(params[0]);`
        long startTime = 0;

        try {
            // File root =
            // android.os.Environment.getExternalStorageDirectory() ;
            File dir = new File(Environment.getExternalStorageDirectory()
                    + "/instamour/");
            if (dir.exists() == false) {
                dir.mkdirs();
            }

            startTime = System.currentTimeMillis();
            String fileName = String.valueOf((System.currentTimeMillis()))
                    + ".jpeg";
            URL url = new URL("http://graph.facebook.com/" + fbid
                    + "/picture?type=large"); // you can write here any
            // link
            Log.e("url", url + "");
            // File file = new File(dir, fileName);
            // URL url = new URL ("file://some/path/anImage.png");
            InputStream input = url.openStream();
            try {
                // The sdcard directory e.g. '/sdcard' can be used directly,
                // or
                // more safely abstracted with getExternalStorageDirectory()
                // File storagePath =
                // Environment.getExternalStorageDirectory();
                Log.e("Video is Downloading  : ", "Video is Downloading......");
                ReceiveFileVideo = new File(dir, fileName);
                OutputStream output = new FileOutputStream(ReceiveFileVideo);
                try {
                    byte[] buffer = new byte[5000];
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                } finally {
                    output.close();
                }
            } finally {
                input.close();
            }

        } catch (Exception e) {

            Log.d("Error....", e.toString());
        }
        try {
            global.setPhoto(ReceiveFileVideo.getPath());
            Log.e("Download Image Path is  recieve : ",
                    ReceiveFileVideo.getPath());

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    // the directions for FlurryAnalytics call for this
    // (FLJ, 5/6/14)
    // The Activity's onStart method
    @Override
    protected void onStart() { // Set up the Flurry session
        super.onStart();
        FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");

    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
