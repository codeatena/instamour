//
//  INSServerAPIManagerr.h
//  Instamour
//
//  Created by Brian Slick on 7/12/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

// Public Constants

// NOTE:
// All "Finish" notifications send a JSON dictionary as the notification's object
// All "Fail" notifications send an NSError as the notification's object
// The input parameters are returned as the userInfo dictionary in each case

// ADDITIONAL NOTE:
// "Finish" just means that communications with the server were successful. It does not mean
// that the returned results have successful contents. The returned JSON dictionary will
// need to be further inspected for any server-provided error messages.

// Public Constants

FOUNDATION_EXPORT NSString *const INSAPIKeyAudioChat;
FOUNDATION_EXPORT NSString *const INSAPIKeyBody;
FOUNDATION_EXPORT NSString *const INSAPIKeyBodyType;
FOUNDATION_EXPORT NSString *const INSAPIKeyChatID;
FOUNDATION_EXPORT NSString *const INSAPIChatFileURL;
FOUNDATION_EXPORT NSString *const INSAPIKeyChildren;
FOUNDATION_EXPORT NSString *const INSAPIKeyCity;
FOUNDATION_EXPORT NSString *const INSAPIKeyCount;
FOUNDATION_EXPORT NSString *const INSAPIKeyCountry;
FOUNDATION_EXPORT NSString *const INSAPIKeyDateOfBirth;
FOUNDATION_EXPORT NSString *const INSAPIKeyDistance;
FOUNDATION_EXPORT NSString *const INSAPIKeyEmail;
FOUNDATION_EXPORT NSString *const INSAPIKeyEthnicity;
FOUNDATION_EXPORT NSString *const INSAPIKeyFacebookUserID;
FOUNDATION_EXPORT NSString *const INSAPIKeyGender;
FOUNDATION_EXPORT NSString *const INSAPIKeyHeight;
FOUNDATION_EXPORT NSString *const INSAPIKeyIdentity;
FOUNDATION_EXPORT NSString *const INSAPIKeyIdentifier;
FOUNDATION_EXPORT NSString *const INSAPIKeyInstagram;
FOUNDATION_EXPORT NSString *const INSAPIKeyLatitude;
FOUNDATION_EXPORT NSString *const INSAPIKeyLongitude;
FOUNDATION_EXPORT NSString *const INSAPIKeyLookingFor;
FOUNDATION_EXPORT NSString *const INSAPIKeyMaximumAge;
FOUNDATION_EXPORT NSString *const INSAPIKeyMinimumAge;
FOUNDATION_EXPORT NSString *const INSAPIKeyNumberOfResults;
FOUNDATION_EXPORT NSString *const INSAPIKeyNumberOfVideos;
FOUNDATION_EXPORT NSString *const INSAPIKeyOrder;
FOUNDATION_EXPORT NSString *const INSAPIKeyPageNumber;
FOUNDATION_EXPORT NSString *const INSAPIKeyPassword;
FOUNDATION_EXPORT NSString *const INSAPIKeyQuickBloxPassword;
FOUNDATION_EXPORT NSString *const INSAPIKeyReason;
FOUNDATION_EXPORT NSString *const INSAPIKeyRotate;
FOUNDATION_EXPORT NSString *const INSAPIKeySessionID;
FOUNDATION_EXPORT NSString *const INSAPIKeySexualPreference;
FOUNDATION_EXPORT NSString *const INSAPIKeySmoker;
FOUNDATION_EXPORT NSString *const INSAPIKeySource;
FOUNDATION_EXPORT NSString *const INSAPIKeyState;
FOUNDATION_EXPORT NSString *const INSAPIKeyStatus;
FOUNDATION_EXPORT NSString *const INSAPIKeyType;
FOUNDATION_EXPORT NSString *const INSAPIKeyUserID;
FOUNDATION_EXPORT NSString *const INSAPIKeyUserName;
FOUNDATION_EXPORT NSString *const INSAPIKeyVideo;
FOUNDATION_EXPORT NSString *const INSAPIKeyVideoChat;
FOUNDATION_EXPORT NSString *const INSAPIKeyVine;


@interface INSServerAPIManager : NSObject

// Public Properties

#pragma mark - Misc Methods

+ (instancetype)sharedManager;

+ (BOOL)isNetworkConnectionAvailable;
+ (void)showNoNetworkAlert;
+ (NSString *)childrenStatusForServerCode:(NSString *)serverCode;

#pragma mark - Generic Services

- (void)downloadImageFromURL:(NSURL *)url
                     success:(void (^)(UIImage *image))success
                     failure:(void (^)(NSString *errorMessage))failure;

- (void)downloadFileFromURL:(NSURL *)fromURL
                  toFileURL:(NSURL *)toURL
                    success:(void (^)(void))success
                    failure:(void (^)(NSString *errorMessage))failure;

#pragma mark - Instamour API Services

#pragma mark App Settings

FOUNDATION_EXPORT NSString *const INSServerAPIManagerAppSettingsDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerAppSettingsFailedNotification;

- (void)appSettings;

#pragma mark Login

FOUNDATION_EXPORT NSString *const INSServerAPIManagerLoginDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerLoginFailedNotification;

- (void)loginWithEmail:(NSString *)email
              password:(NSString *)password;

- (void)loginWithFacebookID:(NSString *)facebookID;

#pragma mark Logout

FOUNDATION_EXPORT NSString *const INSServerAPIManagerLogoutDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerLogoutFailedNotification;

- (void)logOut;

#pragma mark Reset Password

FOUNDATION_EXPORT NSString *const INSServerAPIManagerResetPasswordDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerResetPasswordFailedNotification;

- (void)resetPasswordForEmail:(NSString *)email;

#pragma mark Get User

FOUNDATION_EXPORT NSString *const INSServerAPIManagerGetUserDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerGetUserFailedNotification;

- (void)getUserWithUserName:(NSString *)userName
                   orUserID:(NSString *)userID;

#pragma mark Add User

FOUNDATION_EXPORT NSString *const INSServerAPIManagerAddUserDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerAddUserFailedNotification;

- (void)addUserWithUserName:(NSString *)userName
                      email:(NSString *)email
                   password:(NSString *)password
                 facebookID:(NSString *)facebookID
                     gender:(NSString *)gender
                dateOfBirth:(NSDate *)dateOfBirth
                  ethnicity:(NSString *)ethnicity
                  instagram:(NSString *)instagram
                       vine:(NSString *)vine
                       city:(NSString *)city
                      state:(NSString *)state
                    country:(NSString *)country
                   latitude:(NSString *)latitude
                  longitude:(NSString *)longitude
           sexualPreference:(NSString *)sexualPreference;

#pragma mark Edit User

FOUNDATION_EXPORT NSString *const INSServerAPIManagerEditUserDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerEditUserFailedNotification;

- (void)editUserWithUserName:(NSString *)userName
                      height:(NSString *)height
                    identity:(NSString *)identity
                   ethnicity:(NSString *)ethnicity
                    bodyType:(NSString *)bodyType
                  lookingFor:(NSString *)lookingFor
            sexualPreference:(NSString *)sexualPreference
                      smoker:(NSString *)smoker
                    children:(NSString *)children
                   instagram:(NSString *)instagram
                        vine:(NSString *)vine
            audioChatEnabled:(NSNumber *)isAudioChatEnabled     // NSNumber instead of BOOL to allow for "no change" nil value.
            videoChatEnabled:(NSNumber *)isVideoChatEnabled;    // NSNumber instead of BOOL to allow for "no change" nil value.

#pragma mark Change Email

FOUNDATION_EXPORT NSString *const INSServerAPIManagerChangeEmailDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerChangeEmailFailedNotification;

- (void)changeEmailAddress:(NSString *)newEmailAddress;

#pragma mark Change Password

FOUNDATION_EXPORT NSString *const INSServerAPIManagerChangePasswordDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerChangePasswordFailedNotification;

- (void)changePassword:(NSString *)newPassword;

#pragma mark Deactivate User Account

FOUNDATION_EXPORT NSString *const INSServerAPIManagerDeactivateUserAccountDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerDeactivateUserAccountFailedNotification;

- (void)deactivateUserAccount;

#pragma mark Delete User Account

FOUNDATION_EXPORT NSString *const INSServerAPIManagerDeleteUserAccountDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerDeleteUserAccountFailedNotification;

- (void)deleteUserAccount;

#pragma mark Block User

FOUNDATION_EXPORT NSString *const INSServerAPIManagerBlockUserDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerBlockUserFailedNotification;

- (void)blockUserForUserID:(NSString *)userID;

#pragma mark Edit User Profile Pic

FOUNDATION_EXPORT NSString *const INSServerAPIManagerEditUserProfilePicDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerEditUserProfilePicFailedNotification;

- (void)editUserProfilePicWithImage:(UIImage *)image
                             orData:(NSData *)data;

#pragma mark Push Notifications

FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceAllNotifications;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceMyHeartGetsPushed;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceSomeoneWatchesMyVideo;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceSomeoneKissedMe;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceNewCommentPosted;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceNewAmourAdded;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceInstantChatRequest;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceVideoCallRequest;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceAudioCallRequest;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPushNotificationServiceIReceiveAGift;

FOUNDATION_EXPORT NSString *const INSServerAPIManagerTogglePushNotificationsDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerTogglePushNotificationsFailedNotification;

- (void)setPushNotificationEnabled:(BOOL)isEnabled
                        forService:(NSString *)serviceType;

#pragma mark Flag User

FOUNDATION_EXPORT NSString *const INSServerAPIManagerFlagUserDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerFlagUserFailedNotification;

- (void)flagUserWithIdentifier:(NSString *)userID
                        reason:(NSString *)reason;

#pragma mark Comment Adding

FOUNDATION_EXPORT NSString *const INSServerAPIManagerAddCommentDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerAddCommentFailedNotification;

- (void)addComment:(NSString *)comment
          toUserID:(NSString *)userID;

#pragma mark Comment Editing

FOUNDATION_EXPORT NSString *const INSServerAPIManagerEditCommentDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerEditCommentFailedNotification;

- (void)editCommentWithID:(NSString *)commentID
                toNewBody:(NSString *)body;

#pragma mark Comment Deleting

FOUNDATION_EXPORT NSString *const INSServerAPIManagerDeleteCommentDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerDeleteCommentFailedNotification;

- (void)deleteCommentWithID:(NSString *)commentID;

#pragma mark Kiss Add

FOUNDATION_EXPORT NSString *const INSServerAPIManagerKissAddDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerKissAddFailedNotification;

- (void)kissAddForUserID:(NSString *)userID;

#pragma mark Kiss Delete

FOUNDATION_EXPORT NSString *const INSServerAPIManagerKissDeleteDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerKissDeleteFailedNotification;

- (void)kissDeleteForUserID:(NSString *)userID;

#pragma mark Search User

FOUNDATION_EXPORT NSString *const INSServerAPIManagerBrowserUserDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerBrowserUserFailedNotification;

- (void)downloadBrowserUsersForPageNumber:(NSInteger)pageNumber
                          numberOfResults:(NSInteger)numberOfResults;

FOUNDATION_EXPORT NSString *const INSServerAPIManagerSearchUserDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerSearchUserFailedNotification;

- (void)downloadSearchUsersForPageNumber:(NSInteger)pageNumber
                         numberOfResults:(NSInteger)numberOfResults;

#pragma mark Your Amours (Friends List)

FOUNDATION_EXPORT NSString *const INSServerAPIManagerYourAmoursDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerYourAmoursFailedNotification;

- (void)downloadYourAmours;

#pragma mark Pending Amours (Friends Receiver Pending)

FOUNDATION_EXPORT NSString *const INSServerAPIManagerPendingAmoursDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPendingAmoursFailedNotification;

- (void)downloadPendingAmours;

#pragma mark Hearts Pushed (Friends Sender Pending)

FOUNDATION_EXPORT NSString *const INSServerAPIManagerHeartsPushedDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerHeartsPushedFailedNotification;

- (void)downloadHeartsPushed;

#pragma mark Friend Status

FOUNDATION_EXPORT NSString *const INSServerAPIManagerFriendStatusAcceptDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerFriendStatusAcceptFailedNotification;

- (void)setFriendStatusAcceptForUserID:(NSString *)userID;

FOUNDATION_EXPORT NSString *const INSServerAPIManagerFriendStatusPendingDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerFriendStatusPendingFailedNotification;

- (void)setFriendStatusPendingForUserID:(NSString *)userID;

FOUNDATION_EXPORT NSString *const INSServerAPIManagerFriendStatusDeletedDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerFriendStatusDeletedFailedNotification;

- (void)setFriendStatusDeletedForUserID:(NSString *)userID;

#pragma mark Video Upload

typedef NS_ENUM(NSInteger, INSServerAPIManagerVideoRotation) {
    INSServerAPIManagerVideoRotationNone = 0,
    INSServerAPIManagerVideoRotation90Clockwise = 1,
    INSServerAPIManagerVideoRotation90CounterClockwise = 2,
    INSServerAPIManagerVideoRotation180 = 3,
};

FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoUploadDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoUploadFailedNotification;

- (void)uploadVideoFromFileURL:(NSURL *)fileURL
           forServerSlotNumber:(NSNumber *)serverSlotNumber
                      rotation:(INSServerAPIManagerVideoRotation)rotation;

#pragma mark Video Delete

FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoDeleteDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoDeleteFailedNotification;

- (void)deleteVideoForServerSlotNumber:(NSNumber *)serverSlotNumber;

#pragma mark Video Rotate


#pragma mark Video Sort

FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoSortDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoSortFailedNotification;

- (void)sortVideosWithNewSlotOrder:(NSArray *)slotOrder;

#pragma mark Create Merge Video

FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoCreateMergeDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerVideoCreateMergeFailedNotification;

- (void)createMergeVideo;

#pragma mark Chat Files

- (void)uploadChatImage:(UIImage *)image
                success:(void (^)(NSString *urlString))success
                failure:(void (^)(NSString *errorMessage))failure;

- (void)uploadChatVideoAtURL:(NSURL *)fileURL
                     success:(void (^)(NSString *urlString))success
                     failure:(void (^)(NSString *errorMessage))failure;

#pragma mark Feedback

FOUNDATION_EXPORT NSString *const INSServerAPIManagerFeedbackDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerFeedbackFailedNotification;

- (void)sendFeedback:(NSString *)messageBody;

#pragma mark Ping

FOUNDATION_EXPORT NSString *const INSServerAPIManagerPingDidFinishNotification;
FOUNDATION_EXPORT NSString *const INSServerAPIManagerPingFailedNotification;

- (void)startPing;
- (void)stopPing;

@end
