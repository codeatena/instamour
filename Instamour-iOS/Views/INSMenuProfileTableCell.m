//
//  INSMenuProfileTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/13/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSMenuProfileTableCell.h"

@interface INSMenuProfileTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *editProfileLabel;

@end

@implementation INSMenuProfileTableCell

- (void)awakeFromNib
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setSelectedBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SelectionBackground"]]];

    NSArray *labels = @[ [self userNameLabel], [self editProfileLabel] ];
    for (UILabel *label in labels)
    {
        [label setTextColor:[UIColor normalTableCellTextColorINS]];
        [label setHighlightedTextColor:[UIColor highlightedTableCellTextColorINS]];
    }
    
    UIImageView *imageView = [self avatarImageView];
    
    [imageView setImage:[UIImage imageNamed:@"menu_profilephoto"]];
    
    CALayer *imageViewLayer = [imageView layer];
    
    [imageViewLayer setShadowColor:[[UIColor blackColor] CGColor]];
    [imageViewLayer setShadowRadius:10.0];
    [imageViewLayer setShadowOffset:CGSizeMake(0.0, 5.0)];
    [imageViewLayer setShadowOpacity:1.0];
    
    [imageViewLayer setMasksToBounds:YES];
    [imageViewLayer setCornerRadius:CGRectGetHeight([imageView frame]) / 2.0];

    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
