package com.hyper.instamour;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.easy.facebook.android.apicall.GraphApi;
import com.easy.facebook.android.data.User;
import com.easy.facebook.android.error.EasyFacebookError;
import com.easy.facebook.android.facebook.FBLoginManager;
import com.easy.facebook.android.facebook.Facebook;
import com.easy.facebook.android.facebook.LoginListener;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

public class ShareInstamour extends Activity implements LoginListener {
	private MenuSlideView scrollView;
	private int layoutToSlide;
	private View anapp;
	private SlideMenu sm;
	static String TWITTER_CONSUMER_KEY = "KnWMIg1suOw4AfOBT61zA";
	static String TWITTER_CONSUMER_SECRET = "KCwce5P1FKToWZee33q2rvzHy4iQccYiQKp9O3p0pkQ";
	public final static String KODEFUNFBAPP_ID = "464938160278078";
	public static final String REQUEST_URL = "http://api.twitter.com/oauth/request_token";
	public static final String ACCESS_URL = "http://api.twitter.com/oauth/access_token";
	public static final String AUTHORIZE_URL = "http://api.twitter.com/oauth/authorize";

	final public static String CALLBACK_SCHEME = "x-latify-oauth-twitter";
	final public static String CALLBACK_URL = CALLBACK_SCHEME + "://callback";

	static String PREFERENCE_NAME = "twitter_oauth";
	static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
	static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";
	static final String URL_TWITTER_AUTH = "auth_url";
	static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
	static final String a_token = "2217296058-19wVZnuhgxYnAYsyKy61QpyNkrTCTdIvNVNOgVm";
	static final String a_token_secret = "iuVB81e2gVH2Wh95RHRSba9hefLbbu607Ky5YUPUKzW0n";
	private FBLoginManager fbLoginManager;

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			new VideoSetting(ShareInstamour.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.share_instamour);
		LayoutInflater inflater = LayoutInflater.from(this);
		scrollView = (MenuSlideView) inflater.inflate(
				R.layout.screen_scroll_with_list_menu, null);
		setContentView(scrollView);
		layoutToSlide = R.layout.share_instamour;
		sm = new SlideMenu(getApplicationContext(), scrollView, inflater,
				layoutToSlide);
		new VideoSetting(ShareInstamour.this);

	}

	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.facebook_share:
			if (new Function().haveNetworkConnection(getApplicationContext())) {
				myConnectFacebook();
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						ShareInstamour.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}
			break;
		case R.id.twitter_share:

			Intent intent2 = new Intent(ShareInstamour.this,
					TwitterHomeActivity.class);
			startActivity(intent2);

			// tweet();

			break;
		case R.id.email_share:
			if (new Function().haveNetworkConnection(getApplicationContext())) {
				Uri uri = Uri.parse("mailto:");
				Intent myActivity2 = new Intent(Intent.ACTION_SENDTO, uri);
				myActivity2.putExtra(Intent.EXTRA_SUBJECT,
						"You should try Instamour!");
				myActivity2
						.putExtra(
								Intent.EXTRA_TEXT,
								"Hey everyone! Check out the hottest new video dating app that lets you chat, video call and meet people faster than ever! @ www.instamour.com");
				startActivity(myActivity2);
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						ShareInstamour.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			break;
		case R.id.sms_share:
			try {
				sendSMS();
			} catch (Exception e) {

				e.printStackTrace();
			}

			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {

		} else if (resultCode == RESULT_CANCELED) {

			Toast.makeText(getApplicationContext(), "Cancelled",
					Toast.LENGTH_SHORT).show();
			return;

		}
		fbLoginManager.loginSuccess(data);

	}

	public void sendSMS() {

		Intent sendIntent = new Intent(Intent.ACTION_VIEW);
		sendIntent
				.putExtra(
						"sms_body",
						"Hey everyone! Check out the hottest new video dating app that lets you chat, video call and meet people faster than ever! @ www.instamour.com");
		sendIntent.setType("vnd.android-dir/mms-sms");
		startActivity(sendIntent);
	}

	@SuppressLint("NewApi")
	public void myConnectFacebook() {
		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		String permissions[] = { "user_about_me",

		"user_location",

		"email", "user_photos", "user_interests", "user_online_presence",
				"xmpp_login", "offline_access", "publish_actions",
				"user_photo_video_tags", "user_photos",

				"publish_checkins", "publish_stream" };

		fbLoginManager = new FBLoginManager(this, R.layout.login,
				KODEFUNFBAPP_ID, permissions);
		if (fbLoginManager.existsSavedFacebook()) {
			fbLoginManager.loadFacebook();
		} else {
			fbLoginManager.login();
		}

	}

	@SuppressLint("NewApi")
	@Override
	public void loginSuccess(Facebook facebook) {
		// TODO Auto-generated method stub

		try {
			GraphApi graphApi = new GraphApi(facebook);
			/*
			 * User user = new User(); user = graphApi.getMyAccountInfo();
			 */
			graphApi.setStatus("Hey everyone! Check out the hottest new video dating app that lets you chat, video call and meet people faster than ever! @ www.instamour.com");
		} catch (EasyFacebookError e) {
			Log.d("TAG: ", e.toString());
			AlertDialog.Builder dialog1 = new AlertDialog.Builder(
					ShareInstamour.this);
			dialog1.setTitle("Instamour");
			dialog1.setMessage("This status update is identical to the last one you posted. Try posting something different");
			dialog1.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert1 = dialog1.create();
			alert1.show();

		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(ShareInstamour.this,
					"Sorry ! Unable to Share in facebook ", Toast.LENGTH_LONG)
					.show();

		}

	}

	@Override
	public void logoutSuccess() {
		// TODO Auto-generated method stub
		fbLoginManager.displayToast("Logout Success!");

	}

	@Override
	public void loginFail() {
		// TODO Auto-generated method stub
		fbLoginManager.displayToast("Login Epic Failed!");

	}

	@SuppressLint("NewApi")
	public void tweet() {
		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		try {
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true).setOAuthConsumerKey(TWITTER_CONSUMER_KEY)
					.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET)
					.setOAuthAccessToken(a_token)
					.setOAuthAccessTokenSecret(a_token_secret);
			TwitterFactory tf = new TwitterFactory(cb.build());
			Twitter twitter = tf.getInstance();
			twitter4j.Status response = twitter
					.updateStatus("Hey everyone! Check out the hottest new video dating app that lets you chat, video call and meet people faster than ever! #Instamour");
			Log.e("response is", response.getText());

		} catch (TwitterException te) {
			te.printStackTrace();
		}

	}

	public class Constants {
		static final String TWITTER_CONSUMER_KEY = "KnWMIg1suOw4AfOBT61zA";
		static final String TWITTER_CONSUMER_SECRET = "KCwce5P1FKToWZee33q2rvzHy4iQccYiQKp9O3p0pkQ";
		public static final String REQUEST_URL = "http://api.twitter.com/oauth/request_token";
		public static final String ACCESS_URL = "http://api.twitter.com/oauth/access_token";
		public static final String AUTHORIZE_URL = "http://api.twitter.com/oauth/authorize";

		final public static String CALLBACK_SCHEME = "x-latify-oauth-twitter";
		final public static String CALLBACK_URL = CALLBACK_SCHEME
				+ "://callback";

	}

}
