//
//  INSSignUpProfileViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSSignUpProfileViewController.h"

// Models and other global

// Sub-controllers
#import "INSInfoViewController.h"
#import "INSLocationViewController.h"
#import "INSImageEditorViewController.h"

// Views
#import "INSPickerInputView.h"
#import "INSLabelAndTextFieldTableCell.h"
#import "INSGenderTableCell.h"
#import "INSLocationTableCell.h"
#import "TPKeyboardAvoidingTableView.h"

// Private Constants
NSString *const INSProfileRowIdentifierUserName = @"INSProfileRowIdentifierUserName";
NSString *const INSProfileRowIdentifierEmail = @"INSProfileRowIdentifierEmail";
NSString *const INSProfileRowIdentifierGender = @"INSProfileRowIdentifierGender";
NSString *const INSProfileRowIdentifierEthnicity = @"INSProfileRowIdentifierEthnicity";
NSString *const INSProfileRowIdentifierSexualPreference = @"INSProfileRowIdentifierSexualPreference";
NSString *const INSProfileRowIdentifierLocation = @"INSProfileRowIdentifierLocation";
NSString *const INSProfileRowIdentifierDateOfBirth = @"INSProfileRowIdentifierDateOfBirth";

@interface INSSignUpProfileViewController () <INSPickerInputViewDelegate, UITextFieldDelegate, INSGenderTableCellDelegate, INSLocationViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UIButton *avatarButton;
@property (nonatomic, strong) IBOutlet UILabel *avatarLabel;

@property (nonatomic, strong) IBOutlet UIDatePicker *birthdayDatePicker;

@property (nonatomic, strong) UIBarButtonItem *cancelButton;
@property (nonatomic, strong) UIBarButtonItem *goButton;

@property (nonatomic, strong) INSPickerInputView *pickerInputView;

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *facebookID;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *ethnicity;
@property (nonatomic, copy) NSString *sexualPreference;
@property (nonatomic, copy) NSDate *dateOfBirth;
@property (nonatomic, copy) UIImage *avatarImage;

@property (nonatomic, copy) NSString *emailFromPreviousView;

@property (nonatomic, strong) UITextField *activeTextField;
@property (nonatomic, copy) NSArray *activePickerList;

@property (nonatomic, copy) NSSet *textFieldRowIdentifiers;
@property (nonatomic, copy) NSSet *listPickerRowIdentifiers;

@property (nonatomic, copy) NSArray *ethnicityPickerContents;
@property (nonatomic, copy) NSArray *sexualPreferencePickerContents;

@property (nonatomic, copy) NSDateFormatter *birthdayDateFormatter;

@property (nonatomic, assign, getter = isFacebookSignUp) BOOL facebookSignUp;
@property (nonatomic, assign, getter = isFacebookImageDownloaded) BOOL facebookImageDownloaded;

@end

@implementation INSSignUpProfileViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods

- (instancetype)initWithEmail:(NSString *)email
                     userName:(NSString *)userName
                     password:(NSString *)password
                   facebookID:(NSString *)facebookID
                  dateOfBirth:(NSDate *)date
                       gender:(NSString *)gender
                         city:(NSString *)city
                        state:(NSString *)state
                      country:(NSString *)country
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    self = [self init];
    if (self)
    {
        [self setEmail:email];
        [self setEmailFromPreviousView:email];
        [self setUserName:userName];
        [self setPassword:password];
        [self setFacebookID:facebookID];
        [self setDateOfBirth:date];
        [self setGender:gender];
        [self setCity:city];
        [self setState:state];
        [self setCountry:country];
        
        [self setFacebookSignUp:[facebookID isNotEmpty]];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return self;
}

#pragma mark - Custom Getters and Setters

- (INSPickerInputView *)pickerInputView
{
    if (_pickerInputView == nil)
    {
        _pickerInputView = [INSPickerInputView pickerInputViewMode:INSPickerInputViewModeDoneButtonOnly
                                                          delegate:self];
        [_pickerInputView setPreventBlankRowSelection:YES];
    }
    return _pickerInputView;
}

- (NSSet *)textFieldRowIdentifiers
{
    if (_textFieldRowIdentifiers == nil)
    {
        _textFieldRowIdentifiers = [[NSSet alloc] initWithObjects:INSProfileRowIdentifierUserName, INSProfileRowIdentifierEmail, INSProfileRowIdentifierEthnicity, INSProfileRowIdentifierSexualPreference, INSProfileRowIdentifierDateOfBirth, nil];
    }
    return _textFieldRowIdentifiers;
}

- (NSSet *)listPickerRowIdentifiers
{
    if (_listPickerRowIdentifiers == nil)
    {
        _listPickerRowIdentifiers = [[NSSet alloc] initWithObjects:INSProfileRowIdentifierEthnicity, INSProfileRowIdentifierSexualPreference, nil];
    }
    return _listPickerRowIdentifiers;
}

- (NSArray *)ethnicityPickerContents
{
    if (_ethnicityPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allEthnicitiesINS]];
        
        _ethnicityPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _ethnicityPickerContents;
}

- (NSArray *)sexualPreferencePickerContents
{
    if (_sexualPreferencePickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allSexualPreferencesINS]];
        
        _sexualPreferencePickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _sexualPreferencePickerContents;
}

- (NSDateFormatter *)birthdayDateFormatter
{
    if (_birthdayDateFormatter == nil)
    {
        _birthdayDateFormatter = [[NSDateFormatter alloc] init];
        [_birthdayDateFormatter setDateFormat:@"MMM d, yyyy"];
    }
    return _birthdayDateFormatter;
}

#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Sign Up"];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [self setCancelButton:cancelButton];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    UIBarButtonItem *goButton = [[UIBarButtonItem alloc] initWithTitle:@"Go"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(goButtonPressed:)];
    [self setGoButton:goButton];
    [[self navigationItem] setRightBarButtonItem:goButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    [Flurry logEvent:@"Detailed Sign Up View"];
    
    // User Interface
    
    [self populateContents];
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // Date Picker
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-100];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-18];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate options:0];
    
    [[self birthdayDatePicker] setMinimumDate:minDate];
    [[self birthdayDatePicker] setMaximumDate:maxDate];
    
    // Avatar
    
    [self refreshAvatarViews];
    
    if ( ([self isFacebookSignUp]) && (![self isFacebookImageDownloaded]) )
    {
        [self downloadFacebookProfileImage];
    }
    
    // Location
    
    INSLocationManager *locationManager = [INSLocationManager sharedManager];
    
    if (![[self city] isNotEmpty])
    {
        [self setCity:[locationManager city]];
    }
    
    if (![[self state] isNotEmpty])
    {
        [self setState:[locationManager state]];
    }
    
    if (![[self country] isNotEmpty])
    {
        [self setCountry:[locationManager country]];
    }
    
    // Table contents
    
    BTITableContentsManager *manager = [self mainContentsManager];
    [manager reset];
    
    BTITableRowInfo *rowInfo = nil;
    
    if ([self isFacebookSignUp])
    {
        {{  // Username
            rowInfo = [manager dequeueReusableRowInfo];
            [manager addRowInfo:rowInfo makeNewSection:NO];
            
            [rowInfo setIdentifier:INSProfileRowIdentifierUserName];
            [rowInfo setText:@"User Name"];
            [rowInfo setDetailText:[self userName]];
        }}
    }
    
    if (![[self emailFromPreviousView] isNotEmpty])
    {
        {{  // Email
            rowInfo = [manager dequeueReusableRowInfo];
            [manager addRowInfo:rowInfo makeNewSection:NO];
            
            [rowInfo setIdentifier:INSProfileRowIdentifierEmail];
            [rowInfo setText:@"Email"];
            [rowInfo setDetailText:[self email]];
        }}
    }
    
    {{  // Gender
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSProfileRowIdentifierGender];
        [rowInfo setText:@"Gender"];
        [rowInfo setDetailText:[self gender]];
    }}
    
    {{  // Ethnicity
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSProfileRowIdentifierEthnicity];
        [rowInfo setText:@"Ethnicity"];
        [rowInfo setDetailText:[self ethnicity]];
    }}
    
    {{  // Sexual Preference
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSProfileRowIdentifierSexualPreference];
        [rowInfo setText:@"Preference"];
        [rowInfo setDetailText:[self sexualPreference]];
    }}
    
    {{  // Location
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        NSMutableArray *components = [NSMutableArray array];
        [components safelyAddPopulatedStringINS:[self city]];
        [components safelyAddPopulatedStringINS:[self state]];
        [components safelyAddPopulatedStringINS:[self country]];
        
        [rowInfo setIdentifier:INSProfileRowIdentifierLocation];
        [rowInfo setText:@"Location"];
        [rowInfo setDetailText:[components componentsJoinedByString:@", "]];
    }}
    
    {{  // Date of birth
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSProfileRowIdentifierDateOfBirth];
        [rowInfo setText:@"Birthday"];
        [rowInfo setDetailText:[[self birthdayDateFormatter] stringFromDate:[self dateOfBirth]]];
    }}
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerAddUserDidFinishNotification
                                   selector:@selector(addUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerAddUserFailedNotification
                                   selector:@selector(addUserFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerGetUserDidFinishNotification
                                   selector:@selector(getUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerGetUserFailedNotification
                                   selector:@selector(getUserFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [INSLabelAndTextFieldTableCell registerNibForTableViewBTI:tableView];
    [INSGenderTableCell registerNibForTableViewBTI:tableView];
    [INSLocationTableCell registerNibForTableViewBTI:tableView];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)addUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Sign Up Failed"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Login Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setUserInterfaceEnabled:NO];
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:nil];
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    [userManager loginWithSessionID:[jsonDictionary sessionIdentifierINS]];
    
    INSUser *user = [userManager currentUser];
    
    [user setUserName:[self userName]];
    [user setEmail:[self email]];
    
    UIImage *profileImage = [self avatarImage];
    if (profileImage != nil)
    {
        [user setAvatarImage:profileImage];
        
        [[INSServerAPIManager sharedManager] editUserProfilePicWithImage:profileImage
                                                                  orData:nil];
    }
    
    [[INSServerAPIManager sharedManager] getUserWithUserName:nil     // Nil values will use sessionID (current user)
                                                    orUserID:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)addUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Sign Up Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)getUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"User Details Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Get User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    // Save values to user defaults
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults initializeValuesForNewUserINS];
    
    [userDefaults loadWithGetUserJSONResultINS:jsonDictionary];
    [[INSUserManager sharedManager] loadUserWithGetUserJSONResult:jsonDictionary];
    
    [userDefaults synchronize];
    
    AppDelegate *appDelegate = [AppDelegate sharedDelegate];
    
    [appDelegate showVideoListScreenAfterSignUp];
    
    [[INSQuickBloxManager sharedManager] loginCurrentUser];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)getUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Log In Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] logoutFacebook];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)goButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![self isValidFormEntry])
    {
        // isValidFormEntry will show appropriate alerts
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid Entry", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ([INSServerAPIManager isNetworkConnectionAvailable])
    {
        [self promptUserForTermsAgreement];
    }
    else
    {
        [INSServerAPIManager showNoNetworkAlert];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)dateChanged:(UIDatePicker *)datePicker
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDate *date = [datePicker date];
    
    NSIndexPath *indexPath = [[self mainContentsManager] indexPathOfRowIdentifier:INSProfileRowIdentifierDateOfBirth];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    
    [self setDateOfBirth:date];
    [rowInfo setDetailText:[[self birthdayDateFormatter] stringFromDate:date]];
    
    [[self activeTextField] setText:[rowInfo detailText]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)avatarButtonPressed:(UIButton *)button
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    __weak INSSignUpProfileViewController *weakSelf = self;
    
	void(^showImagePicker)(UIImagePickerControllerSourceType sourceType) = ^(UIImagePickerControllerSourceType sourceType){
        
		UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
		[imagePicker setMediaTypes:@[(NSString *)kUTTypeImage]];
		[imagePicker setDelegate:weakSelf];
		[imagePicker setSourceType:sourceType];
        [imagePicker setAllowsEditing:NO];
        
		[weakSelf presentViewController:imagePicker
                               animated:YES
                             completion:nil];
        
	};
    
	NSMutableArray *buttons = [NSMutableArray array];
    
	RIButtonItem *buttonItem = nil;
    
	// Pick from library
    
	buttonItem = [RIButtonItem item];
	[buttonItem setLabel:@"Choose Photo"];
	[buttonItem setAction:^{
        
		showImagePicker(UIImagePickerControllerSourceTypePhotoLibrary);
        
	}];
    
	[buttons addObject:buttonItem];
    
	// Pick from camera
    
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
		buttonItem = [RIButtonItem item];
		[buttonItem setLabel:@"Take Photo"];
		[buttonItem setAction:^{
            
			showImagePicker(UIImagePickerControllerSourceTypeCamera);
            
		}];
        
		[buttons addObject:buttonItem];
	}
    
	// Delete image
    
    if ([self avatarImage] != nil)
    {
		buttonItem = [RIButtonItem item];
		[buttonItem setLabel:@"Delete Photo"];
		[buttonItem setAction:^{
            
            [weakSelf setAvatarImage:nil];
            
            [weakSelf refreshAvatarViews];
            
		}];
        
        [buttons addObject:buttonItem];
    }
    
	// Cancel
    
	buttonItem = [RIButtonItem item];
	[buttonItem setLabel:@"Cancel"];
    
	[buttons addObject:buttonItem];
    
	// Action Sheet
    
	UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
											   cancelButtonItem:nil
										  destructiveButtonItem:nil
											   otherButtonItems:nil];
    
	NSInteger cancelIndex = -1;
	for (RIButtonItem *aButton in buttons)
	{
		cancelIndex = [sheet addButtonItem:aButton];
	}
    
	[sheet setCancelButtonIndex:cancelIndex];
    
	[sheet showInView:[self view]];
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)refreshAvatarViews
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *image = [self avatarImage];
    
    if (image != nil)
    {
        [[self avatarImageView] setImage:image];
        [[self avatarLabel] setText:@"Tap to Edit"];
    }
    else
    {
        [[self avatarImageView] setImage:[UIImage placeholderAvatarImageINS]];
        [[self avatarLabel] setText:@"Tap to Add Profile Photo"];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)isValidFormEntry
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // User name and email address are required for both standard and Facebook login
    
    if (![[self userName] isNotEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"Please Enter a User Name"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid email address", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    NSString *email = [self email];
    BOOL isValidEmail = ( [email isNotEmpty] && [email isValidEmailAddress] );
    
    if (!isValidEmail)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"Please Enter a valid Email Address"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid email address", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    // Need either Facebook ID -or- password, not both. User can't correct this situation on this screen.
    
    BOOL isPasswordInvalid = ![[self password] isNotEmpty];
    BOOL isFacebookIDInvalid = ![[self facebookID] isNotEmpty];
    
    if ( isPasswordInvalid && isFacebookIDInvalid )
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error"
                                                       message:@"No password or Facebook ID was provided"
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No password or Facebook ID", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    // Location. This can be revised, choosing to require city + state or country, so 2 out of 3 fields.
    
    BOOL isCityInvalid = ![[self city] isNotEmpty];
    BOOL isStateInvalid = ![[self state] isNotEmpty];
    BOOL isCountryInvalid = ![[self country] isNotEmpty];
    
    BOOL isInvalidLocation = ( isCityInvalid || ( isStateInvalid && isCountryInvalid ) );
    
    if (isInvalidLocation)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error"
                                                       message:@"Please enter a valid location"
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid location", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    // I'm choosing to be lazy with the remaining criteria. Just make sure they are all filled out, give generic message.
    // This can be revised to include specific messages for each line item.
    
    NSDate *birthday = [self dateOfBirth];
    NSString *ethnicity = [self ethnicity];
    NSString *gender = [self gender];
    NSString *sexualPreference = [self sexualPreference];
    
    BOOL isValidEntry = ( (birthday != nil) && [ethnicity isNotEmpty] && [gender isNotEmpty] && [sexualPreference isNotEmpty] );
    
    if (!isValidEntry)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error"
                                                       message:@"Please enter all the details"
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Missing info", self, __PRETTY_FUNCTION__);
        return NO;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)showDeclinedTermsOfServiceAlert
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Declined"
                                                   message:@"To use Instamour, you MUST agree to the terms."
                                                  delegate:nil
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showTermsOfServiceView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    INSInfoViewController *infoViewController = [[INSInfoViewController alloc] init];
    [infoViewController setInfoTitle:@"Terms of Service"];
    [infoViewController setUrl:[NSURL URLWithString:@"http://www.instamour.com/corporate/terms.htm"]];
    
    [[self navigationController] pushViewController:infoViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)createAccount
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:NO];
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Creating Account..."];
    
    [[self activeTextField] resignFirstResponder];
    [self setActiveTextField:nil];
    
    [self setUserInterfaceEnabled:NO];
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:nil];
    
    INSLocationManager *locationManager = [INSLocationManager sharedManager];
    
    [[INSServerAPIManager sharedManager] addUserWithUserName:[self userName]
                                                       email:[self email]
                                                    password:[self password]
                                                  facebookID:[self facebookID]
                                                      gender:[self gender]
                                                 dateOfBirth:[self dateOfBirth]
                                                   ethnicity:[self ethnicity]
                                                   instagram:nil
                                                        vine:nil
                                                        city:[self city]
                                                       state:[self state]
                                                     country:[self country]
                                                    latitude:[locationManager latitude]
                                                   longitude:[locationManager longitude]
                                            sexualPreference:[self sexualPreference]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadFacebookProfileImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *facebookID = [self facebookID];
    
    if (![facebookID isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No Facebook ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", facebookID];
    NSURL *url = [NSURL URLWithString:urlString];
    
    __weak INSSignUpProfileViewController *weakSelf = self;
    
    [[weakSelf avatarButton] setHidden:YES];
    [[weakSelf avatarLabel] setHidden:YES];
    
    [[INSServerAPIManager sharedManager] downloadImageFromURL:url
                                                      success:^(UIImage *image) {
                                                          
                                                          [weakSelf setFacebookImageDownloaded:YES];
                                                          
                                                          [weakSelf setAvatarImage:image];
                                                          
                                                          [weakSelf refreshAvatarViews];
                                                          
                                                          [[weakSelf avatarButton] setHidden:NO];
                                                          [[weakSelf avatarLabel] setHidden:NO];
                                                          
                                                      }
                                                      failure:^(NSString *errorMessage) {
                                                          
                                                          NSLog(@"Could not download Facebook profile image");
                                                          
                                                          [[weakSelf avatarButton] setHidden:NO];
                                                          [[weakSelf avatarLabel] setHidden:NO];
                                                          
                                                      }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setUserInterfaceEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self cancelButton] setEnabled:isEnabled];
    [[self goButton] setEnabled:isEnabled];
    
    if (isEnabled)
    {
        [[AppDelegate sharedDelegate] hideProgressIndicator];
    }
    else
    {
        [[self activeTextField] resignFirstResponder];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)promptUserForTermsAgreement
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    //    [[AppDelegate sharedDelegate] setIsProcessingNewAccount:YES];
    
    __weak INSSignUpProfileViewController *weakSelf = self;
    
    RIButtonItem *readButton = [RIButtonItem item];
    [readButton setLabel:@"Read"];
    [readButton setAction:^{
        
        [weakSelf showTermsOfServiceView];
        
    }];
    
    RIButtonItem *agreeButton = [RIButtonItem item];
    [agreeButton setLabel:@"Agree"];
    [agreeButton setAction:^{
        
        [weakSelf createAccount];
        
    }];
    
    RIButtonItem *declineButton = [RIButtonItem item];
    [declineButton setLabel:@"Decline"];
    [declineButton setAction:^{
        
        [weakSelf showDeclinedTermsOfServiceAlert];
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Terms of Service Agreement"
                                                    message:@"To use Instamour, you MUST agree to the Terms of Service"
                                           cancelButtonItem:nil
                                           otherButtonItems:readButton, agreeButton, declineButton, nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadPickerViewDataForRowIdentifier:(NSString *)rowIdentifier
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([rowIdentifier isEqualToString:INSProfileRowIdentifierEthnicity])
    {
        [self setActivePickerList:[self ethnicityPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSProfileRowIdentifierSexualPreference])
    {
        [self setActivePickerList:[self sexualPreferencePickerContents]];
    }
    else
    {
        [self setActivePickerList:nil];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not a list field", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSPickerInputView *pickerInputView = [self pickerInputView];
    
    NSString *selectedText = [[self activeTextField] text];
    NSInteger preselectedRow = NSNotFound;
    
    if ([selectedText isNotEmpty])
    {
        preselectedRow = [[self activePickerList] indexOfObject:selectedText];
    }
    
    [pickerInputView setPickerViewContents:[self activePickerList] forComponent:0 preselectingRow:preselectedRow];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self mainContentsManager] numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContentsManager] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    UITableViewCell *cellToReturn = nil;
    
    if ([[self textFieldRowIdentifiers] containsObject:rowIdentifier])
    {
        INSLabelAndTextFieldTableCell *textFieldCell = [INSLabelAndTextFieldTableCell dequeueCellFromTableViewBTI:tableView];
        
        UITextField *textField = [textFieldCell textField];
        [textField setDelegate:self];
        
        [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        
        [textField setPlaceholder:[rowInfo text]];
        [textField setText:[rowInfo detailText]];
        
        [textFieldCell setLabelText:[rowInfo text]];
        
        cellToReturn = textFieldCell;
    }
    else if ([rowIdentifier isEqualToString:INSProfileRowIdentifierGender])
    {
        INSGenderTableCell *genderCell = [INSGenderTableCell dequeueCellFromTableViewBTI:tableView];
        [genderCell setDelegate:self];
        
        [genderCell setGender:[self gender]];
        
        cellToReturn = genderCell;
    }
    else if ([rowIdentifier isEqualToString:INSProfileRowIdentifierLocation])
    {
        INSLocationTableCell *locationCell = [INSLocationTableCell dequeueCellFromTableViewBTI:tableView];
        
        [locationCell setLocation:[rowInfo detailText]];
        
        cellToReturn = locationCell;
    }
    
    if (cellToReturn == nil)
    {
        cellToReturn = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"blankCell"];
    }
    
    [cellToReturn setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cellToReturn;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    // Pushes new view if you tap there from keyboard, so it might be annoying
    //    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    //    NSString *rowIdentifier = [rowInfo identifier];
    //
    //    if ([rowIdentifier isEqualToString:INSProfileRowIdentifierLocation])
    //    {
    //        [self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
    //    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)tableView:(UITableView *)tableView
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSLocationViewController *nextViewController = [[INSLocationViewController alloc] initWithCity:[self city]
                                                                                              state:[self state]
                                                                                            country:[self country]
                                                                                           delegate:self];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setActiveTextField:textField];
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    if ([[self listPickerRowIdentifiers] containsObject:rowIdentifier])
    {
        [textField setInputView:[self pickerInputView]];
        
        [self loadPickerViewDataForRowIdentifier:rowIdentifier];
    }
    else if ([rowIdentifier isEqualToString:INSProfileRowIdentifierDateOfBirth])
    {
        NSDate *date = [self dateOfBirth];
        if (date == nil)
        {
            date = [[self birthdayDatePicker] maximumDate];
        }
        
        [[self birthdayDatePicker] setDate:date];
        
        [textField setInputView:[self birthdayDatePicker]];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self tableView] respondsToSelector:@selector(TPKeyboardAvoiding_scrollToActiveTextField)])
    {
        [(TPKeyboardAvoidingTableView *)[self tableView] TPKeyboardAvoiding_scrollToActiveTextField];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    if ([rowIdentifier isEqualToString:INSProfileRowIdentifierUserName])
    {
        NSString *newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        
        if ([newString length] >= INSUserNameMaxLength)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User name too long", self, __PRETTY_FUNCTION__);
            return NO;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [textField resignFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    NSString *text = [textField text];
    
    if ([rowIdentifier isEqualToString:INSProfileRowIdentifierUserName])
    {
        [self setUserName:text];
    }
    else if ([rowIdentifier isEqualToString:INSProfileRowIdentifierEmail])
    {
        [self setEmail:text];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSPickerInputViewDelegate Methods

- (void)pickerInputView:(INSPickerInputView *)pickerInputView
           didSelectRow:(NSInteger)row
            inComponent:(NSInteger)component
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *textField = [self activeTextField];
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    NSString *selection = [[self activePickerList] objectAtIndex:row];
    if ([selection isEqualToString:INSPickerInputViewBlankValue])
    {
        selection = nil;
    }
    
    if ([rowIdentifier isEqualToString:INSProfileRowIdentifierEthnicity])
    {
        [self setEthnicity:selection];
    }
    else if ([rowIdentifier isEqualToString:INSProfileRowIdentifierSexualPreference])
    {
        [self setSexualPreference:selection];
    }
    
    [rowInfo setDetailText:selection];
    [textField setText:selection];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pickerInputViewDoneButtonPressed:(INSPickerInputView *)pickerInputView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self activeTextField] resignFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSGenderTableCellDelegate Methods

- (void)genderTableCell:(INSGenderTableCell *)cell
        didSelectGender:(NSString *)gender
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setGender:gender];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSLocationViewControllerDelegate Methods

- (void)locationViewController:(INSLocationViewController *)viewController
                 didUpdateCity:(NSString *)city
                         state:(NSString *)state
                       country:(NSString *)country
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setCity:city];
    [self setState:state];
    [self setCountry:country];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
	UIImage *selectedImage = [info objectForKey:UIImagePickerControllerEditedImage];
	if (selectedImage == nil)
	{
		selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
	}
    
    __weak INSSignUpProfileViewController *weakSelf = self;
    
    void (^processImage)(UIImage *image) = ^(UIImage *image) {
      
        UIImage *croppedImage = [image croppedProfileImageINS];

        [weakSelf setAvatarImage:croppedImage];
        
        [weakSelf refreshAvatarViews];
    };
    
    if ([selectedImage isSquareINS])
    {
        NSLog(@"Square Image");
        
        processImage(selectedImage);
        
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
    }
    else
    {
        NSLog(@"Rectangle Image");
        
        INSImageEditorViewController *nextViewController = [[INSImageEditorViewController alloc] initWithNibName:nil bundle:nil];
        
        [nextViewController setSourceImage:selectedImage];
        [nextViewController setDoneCallback:^(UIImage *image, BOOL canceled) {
            
            if (canceled || (image == nil) )
            {
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                processImage(image);
                
                [weakSelf dismissViewControllerAnimated:YES
                                             completion:nil];
            }
        }];
        
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                     [weakSelf presentViewController:nextViewController
                                                            animated:YES
                                                          completion:nil];
                                     
                                 }];
    }

//    UIImage *croppedImage = [selectedImage croppedProfileImageINS];
//    
//    [self setAvatarImage:croppedImage];
//    
//    [self refreshAvatarViews];
//    
//    [self dismissViewControllerAnimated:YES
//                             completion:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
	[self dismissViewControllerAnimated:YES
							 completion:nil];
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
