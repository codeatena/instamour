//
//  UIColor+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/13/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (INSAdditions)

+ (UIColor *)normalTableCellTextColorINS;
+ (UIColor *)highlightedTableCellTextColorINS;

+ (UIColor *)profileCommentColorINS;

+ (UIColor *)switchOnTintColorINS;

+ (UIColor *)commentUserNameColorINS;
+ (UIColor *)commentTimeColorINS;
+ (UIColor *)commentMessageColorINS;

+ (UIColor *)instamourPrimaryBackgroundRedColorINS;

@end
