//
//  INSSettingsViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/20/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSSettingsViewController.h"

// Models and other global

// Sub-controllers
#import "INSPushNotificationSettingsViewController.h"
#import "INSEditProfileViewController.h"
#import "INSInfoViewController.h"
#import "INSLocationViewController.h"
#import "INSFeedbackViewController.h"

// Views

// Private Constants

typedef NS_ENUM(NSInteger, INSSettingsViewControllerMode) {
    INSSettingsViewControllerModeAccount = 1,
    INSSettingsViewControllerModePrivacy,
    INSSettingsViewControllerModeSupport
};

// Acount view
NSString *const INSSettingsRowIdentifierEditProfile = @"INSSettingsRowIdentifierEditProfile";
NSString *const INSSettingsRowIdentifierEmailAddress = @"INSSettingsRowIdentifierEmailAddress";
NSString *const INSSettingsRowIdentifierChangePassword = @"INSSettingsRowIdentifierChangePassword";
NSString *const INSSettingsRowIdentifierUpdateLocation = @"INSSettingsRowIdentifierUpdateLocation";
NSString *const INSSettingsRowIdentifierPushNotifications = @"INSSettingsRowIdentifierPushNotifications";

// Privacy view
NSString *const INSSettingsRowIdentifierEnablePhoneCalling = @"INSSettingsRowIdentifierEnablePhoneCalling";
NSString *const INSSettingsRowIdentifierEnableVideoCalling = @"INSSettingsRowIdentifierEnableVideoCalling";
NSString *const INSSettingsRowIdentifierDisableAlerts = @"INSSettingsRowIdentifierDisableAlerts";
NSString *const INSSettingsRowIdentifierDisableAccount = @"INSSettingsRowIdentifierDisableAccount";

// Support view
NSString *const INSSettingsRowIdentifierRateApp = @"INSSettingsRowIdentifierRateApp";
NSString *const INSSettingsRowIdentifierFeedback = @"INSSettingsRowIdentifierFeedback";
NSString *const INSSettingsRowIdentifierDeleteAccount = @"INSSettingsRowIdentifierDeleteAccount";
NSString *const INSSettingsRowIdentifierAbout = @"INSSettingsRowIdentifierAbout";
NSString *const INSSettingsRowIdentifierPrivacyPolicy = @"INSSettingsRowIdentifierPrivacyPolicy";
NSString *const INSSettingsRowIdentifierTermsOfService = @"INSSettingsRowIdentifierTermsOfService";

@interface INSSettingsViewController () <INSLocationViewControllerDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIButton *accountsButton;
@property (nonatomic, strong) IBOutlet UIButton *privacyButton;
@property (nonatomic, strong) IBOutlet UIButton *supportButton;

@property (nonatomic, assign) INSSettingsViewControllerMode mode;

@property (nonatomic, strong) UITextField *activeTextField;

@property (nonatomic, copy) NSSet *privacyRowIdentifiers;

@property (nonatomic, copy) NSString *pendingEmailAddress;

@end

@implementation INSSettingsViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters

- (NSSet *)privacyRowIdentifiers
{
    if (_privacyRowIdentifiers == nil)
    {
        _privacyRowIdentifiers = [[NSSet alloc] initWithObjects:INSSettingsRowIdentifierEnablePhoneCalling, INSSettingsRowIdentifierEnableVideoCalling, INSSettingsRowIdentifierDisableAlerts, INSSettingsRowIdentifierDisableAccount, nil];
    }
    return _privacyRowIdentifiers;
}

#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];
    
    if ([self mode] == 0)
    {
        [self setMode:INSSettingsViewControllerModeAccount];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
    
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    [Flurry logEvent:@"Settings View"];
    
    // User Interface
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self mainContentsManager] reset];
    
    UIButton *activeButton = nil;
    
    switch ([self mode])
    {
        case INSSettingsViewControllerModeAccount:
        {
            [self setTitle:@"Account"];
            
            activeButton = [self accountsButton];
            
            [self populateAccountContents];
        }
            break;
        case INSSettingsViewControllerModePrivacy:
        {
            [self setTitle:@"Privacy"];
            
            activeButton = [self privacyButton];
            
            [self populatePrivacyContents];
        }
            break;
        case INSSettingsViewControllerModeSupport:
        {
            [self setTitle:@"Support"];
            
            activeButton = [self supportButton];
            
            [self populateSupportContents];
        }
            break;
        default:
            break;
    }
    
    NSArray *allButtons = @[ [self accountsButton], [self privacyButton], [self supportButton] ];
    
    for (UIButton *button in allButtons)
    {
        [button setSelected:(button == activeButton)];
    }
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSLocationManagerDidChangeLocationNotification
                                   selector:@selector(locationUpdateSucceededNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSLocationManagerFailedNotification
                                   selector:@selector(locationUpdateFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerChangeEmailDidFinishNotification
                                   selector:@selector(changeEmailDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerChangeEmailFailedNotification
                                   selector:@selector(changeEmailFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerChangePasswordDidFinishNotification
                                   selector:@selector(changePasswordDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerChangePasswordFailedNotification
                                   selector:@selector(changePasswordFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerDeactivateUserAccountDidFinishNotification
                                   selector:@selector(deactivateAccountDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerDeactivateUserAccountFailedNotification
                                   selector:@selector(deactivateAccountFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerDeleteUserAccountDidFinishNotification
                                   selector:@selector(deleteAccountDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerDeleteUserAccountFailedNotification
                                   selector:@selector(deleteAccountFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}


#pragma mark - Notification Handlers

- (void)locationUpdateSucceededNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    [self populateContents];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                    message:@"Your Location has been updated!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
#warning TODO: Web service: Update user location
    // TODO: Call API service to update location
    // user-location
    // 7/20: Evan says that user-location isn't used anymore. Currently no replacement.  Might be added to user-edit
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)locationUpdateFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // An alert will already be shown by INSLocationManager
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)changeEmailDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Change Email Failed"
                                                        message:[jsonDictionary errorMessageINS]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Login Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[[INSUserManager sharedManager] currentUser] setEmail:[self pendingEmailAddress]];
    
    [self setPendingEmailAddress:nil];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"Email address has been changed"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)changeEmailFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Change Email Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)changePasswordDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Change Password Failed"
                                                        message:[jsonDictionary errorMessageINS]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Login Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"Password has been changed"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)changePasswordFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Change Password Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deactivateAccountDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Deactivate Account Failed"
                                                        message:[jsonDictionary errorMessageINS]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Login Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[[INSUserManager sharedManager] currentUser] setAccountDisabled:YES];
    
    [self populateContents];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"Account has been deactivated"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
#warning TODO: Logout after account deactivated?
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deactivateAccountFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Deactivate Account Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteAccountDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Account Failed"
                                                        message:[jsonDictionary errorMessageINS]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Login Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[AppDelegate sharedDelegate] logoutUser];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"Account has been deleted"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteAccountFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Delete Account Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (IBAction)accountButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSSettingsViewControllerModeAccount];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)privacyButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSSettingsViewControllerModePrivacy];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)supportButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSSettingsViewControllerModeSupport];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)populateAccountContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    BTITableContentsManager *manager = [self mainContentsManager];
    __weak INSSettingsViewController *weakSelf = self;
    
    BTITableRowInfo *rowInfo = nil;
    
    {{  // Edit Profile
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierEditProfile];
        [rowInfo setText:@"Edit Profile"];
        [rowInfo setRowSelectionBlock:^{
            
            INSEditProfileViewController *nextViewController = [[INSEditProfileViewController alloc] init];
            
            [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
            
        }];
    }}
    
    {{  // Email Address
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierEmailAddress];
        [rowInfo setText:@"Email Address"];
        [rowInfo setRowSelectionBlock:^{
            
            RIButtonItem *cancelButton = [RIButtonItem item];
            [cancelButton setLabel:@"Cancel"];
            [cancelButton setAction:^{
                
                [weakSelf setActiveTextField:nil];
                
            }];
            
            RIButtonItem *updateButton = [RIButtonItem item];
            [updateButton setLabel:@"Update"];
            [updateButton setAction:^{
                
                NSString *emailAddress = [[weakSelf activeTextField] text];
                
                if ( ![emailAddress isNotEmpty] || ![emailAddress isValidEmailAddress] )
                {
                    RIButtonItem *okButton = [RIButtonItem item];
                    [okButton setLabel:@"Ok"];
                    [okButton setAction:^{
                        
                        [weakSelf setActiveTextField:nil];
                        
                    }];
                    
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                         message:@"You must enter a valid email address"
                                                                cancelButtonItem:okButton
                                                                otherButtonItems:nil];
                    [errorAlert show];
                    
                    return;
                }
                
                [weakSelf setPendingEmailAddress:emailAddress];
                
                [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
                
                [[INSServerAPIManager sharedManager] changeEmailAddress:emailAddress];
                
                [weakSelf setActiveTextField:nil];
                
            }];
            
            UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:@"Update Email"
                                                                 message:@"Enter your new Email address"
                                                        cancelButtonItem:cancelButton
                                                        otherButtonItems:updateButton, nil];
            [emailAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
            
            UITextField *textField = [emailAlert textFieldAtIndex:0];
            [weakSelf setActiveTextField:textField];
            
            [textField setKeyboardType:UIKeyboardTypeEmailAddress];
            [textField setPlaceholder:@"New Email Address"];
            
            [emailAlert show];
            
        }];
    }}
    
    {{  // Change Password
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierChangePassword];
        [rowInfo setText:@"Change Password"];
        [rowInfo setRowSelectionBlock:^{
            
            RIButtonItem *cancelButton = [RIButtonItem item];
            [cancelButton setLabel:@"Cancel"];
            [cancelButton setAction:^{
                
                [weakSelf setActiveTextField:nil];
                
            }];
            
            RIButtonItem *updateButton = [RIButtonItem item];
            [updateButton setLabel:@"Update"];
            [updateButton setAction:^{
                
                NSString *password = [[weakSelf activeTextField] text];
                
                if (![password isNotEmpty])
                {
                    RIButtonItem *okButton = [RIButtonItem item];
                    [okButton setLabel:@"Ok"];
                    [okButton setAction:^{
                        
                        [weakSelf setActiveTextField:nil];
                        
                    }];
                    
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                         message:@"You must enter a password"
                                                                cancelButtonItem:okButton
                                                                otherButtonItems:nil];
                    [errorAlert show];
                    
                    return;
                }
                
                [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
                
                [[INSServerAPIManager sharedManager] changePassword:password];
                
                [weakSelf setActiveTextField:nil];
                
            }];
            
            UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:@"Update Password"
                                                                 message:@"Enter your new password"
                                                        cancelButtonItem:cancelButton
                                                        otherButtonItems:updateButton, nil];
            [emailAlert setAlertViewStyle:UIAlertViewStyleSecureTextInput];
            
            UITextField *textField = [emailAlert textFieldAtIndex:0];
            [weakSelf setActiveTextField:textField];
            
            [textField setKeyboardType:UIKeyboardTypeEmailAddress];
            [textField setPlaceholder:@"New Password"];
            
            [emailAlert show];
            
        }];
    }}
    
    {{  // Update Location
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierUpdateLocation];
        [rowInfo setText:@"Update Location"];
        [rowInfo setDetailText:[user formattedCityStateCountry]];
        
        [rowInfo setRowSelectionBlock:^{
            
            INSLocationViewController *nextViewController = [[INSLocationViewController alloc] initWithCity:[user city]
                                                                                                      state:[user state]
                                                                                                    country:[user country]
                                                                                                   delegate:weakSelf];
            
            [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
            
        }];
    }}
    
    {{  // Push Notifications
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierPushNotifications];
        [rowInfo setText:@"Push Notifications"];
        [rowInfo setRowSelectionBlock:^{
            
            INSPushNotificationSettingsViewController *nextViewController = [[INSPushNotificationSettingsViewController alloc] init];
            
            [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
            
        }];
    }}
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populatePrivacyContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    BTITableContentsManager *manager = [self mainContentsManager];
    __weak INSSettingsViewController *weakSelf = self;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    BTITableRowInfo *rowInfo = nil;
    
    UIImage *heartImage = [UIImage imageNamed:@"privacyHeart"];
    
    {{  // Enable Phone Calling
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierEnablePhoneCalling];
        [rowInfo setText:@"Enable Phone Calling"];
        [rowInfo setImage:([user isAudioChatEnabled]) ? heartImage : nil];
        [rowInfo setRowSelectionBlock:^{
            
            [user setAudioChatEnabled:![user isAudioChatEnabled]];
            [user setAudioChatNotificationEnabled:[user isAudioChatEnabled]];
            
            [[INSServerAPIManager sharedManager] editUserWithUserName:nil
                                                               height:nil
                                                             identity:nil
                                                            ethnicity:nil
                                                             bodyType:nil
                                                           lookingFor:nil
                                                     sexualPreference:nil
                                                               smoker:nil
                                                             children:nil
                                                            instagram:nil
                                                                 vine:nil
                                                     audioChatEnabled:@([user isAudioChatEnabled])
                                                     videoChatEnabled:nil];
            
            [[INSServerAPIManager sharedManager] setPushNotificationEnabled:[user isAudioChatNotificationEnabled]
                                                                 forService:INSServerAPIManagerPushNotificationServiceAudioCallRequest];
            
            [weakSelf populateContents];
            
        }];
    }}
    
    {{  // Enable Video Chat
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierEnableVideoCalling];
        [rowInfo setText:@"Enable Video Chat"];
        [rowInfo setImage:([user isVideoChatEnabled]) ? heartImage : nil];
        [rowInfo setRowSelectionBlock:^{
            
            [user setVideoChatEnabled:![user isVideoChatEnabled]];
            [user setVideoChatNotificationEnabled:[user isVideoChatEnabled]];
            
            [[INSServerAPIManager sharedManager] editUserWithUserName:nil
                                                               height:nil
                                                             identity:nil
                                                            ethnicity:nil
                                                             bodyType:nil
                                                           lookingFor:nil
                                                     sexualPreference:nil
                                                               smoker:nil
                                                             children:nil
                                                            instagram:nil
                                                                 vine:nil
                                                     audioChatEnabled:nil
                                                     videoChatEnabled:@([user isVideoChatEnabled])];
            
            [[INSServerAPIManager sharedManager] setPushNotificationEnabled:[user isVideoChatNotificationEnabled]
                                                                 forService:INSServerAPIManagerPushNotificationServiceVideoCallRequest];
            
            [weakSelf populateContents];
            
        }];
    }}
    
    {{  // Disable Alerts
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierDisableAlerts];
        [rowInfo setText:@"Disable Alerts"];
        [rowInfo setImage:([userDefaults isCorporateAlertsEnabledINS]) ? nil : heartImage];
        [rowInfo setRowSelectionBlock:^{
            
            [userDefaults setCorporateAlertsEnabledINS:![userDefaults isCorporateAlertsEnabledINS]];
            
            [weakSelf populateContents];
            
        }];
    }}
    
    {{  // Disable Account
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierDisableAccount];
        [rowInfo setText:@"Disable Account"];
        [rowInfo setImage:([user isAccountDisabled]) ? heartImage : nil];
        [rowInfo setRowSelectionBlock:^{
            
            if (![user isAccountDisabled])
            {
                RIButtonItem *cancelButton = [RIButtonItem item];
                [cancelButton setLabel:@"NO"];
                
                RIButtonItem *proceedButton = [RIButtonItem item];
                [proceedButton setLabel:@"YES"];
                [proceedButton setAction:^{
                    
                    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
                    
                    [[INSServerAPIManager sharedManager] deactivateUserAccount];
                    
                }];
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                message:@"Do you really want to disable your account?"
                                                       cancelButtonItem:cancelButton
                                                       otherButtonItems:proceedButton, nil];
                [alert show];
            }
            
        }];
    }}
    
#warning TODO: Possible for user to reactivate account?
    
    // TODO: Possible for user to reactivate account?
    // A: Yes, but there is not currently a service for it.
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateSupportContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableContentsManager *manager = [self mainContentsManager];
    __weak INSSettingsViewController *weakSelf = self;
    
    BTITableRowInfo *rowInfo = nil;
    
    {{  // Rate Our App
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierRateApp];
        [rowInfo setText:@"Rate Our App"];
        [rowInfo setRowSelectionBlock:^{
            
            RIButtonItem *cancelButton = [RIButtonItem item];
            [cancelButton setLabel:@"No thanks"];
            
            RIButtonItem *rateButton = [RIButtonItem item];
            [rateButton setLabel:@"Rate Instamour"];
            [rateButton setAction:^{
                
                NSString *rateAppURLString = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/%@/app/id%@", [[NSLocale preferredLanguages] objectAtIndex:0], @"710173306"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:rateAppURLString]];
                
            }];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rate Instamour"
                                                            message:@"If you enjoy using Instamour, would you mind taking a moment to rate it? It won't take more than a minute. Thanks for your support!"
                                                   cancelButtonItem:cancelButton
                                                   otherButtonItems:rateButton, nil];
            [alert show];
            
        }];
    }}
    
    {{  // Send Us Feedback
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierFeedback];
        [rowInfo setText:@"Send Us Feedback"];
        [rowInfo setRowSelectionBlock:^{
            
            INSFeedbackViewController *nextViewController = [[INSFeedbackViewController alloc] init];
            
            [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
            
        }];
    }}
    
    {{  // Delete Account
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierDeleteAccount];
        [rowInfo setText:@"Delete Account"];
        [rowInfo setRowSelectionBlock:^{
            
            RIButtonItem *cancelButton = [RIButtonItem item];
            [cancelButton setLabel:@"Cancel"];
            
            RIButtonItem *rateButton = [RIButtonItem item];
            [rateButton setLabel:@"Remove"];
            [rateButton setAction:^{
                
                [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
                
                [[INSServerAPIManager sharedManager] deleteUserAccount];
                
            }];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                            message:@"Do you really want to remove your account?"
                                                   cancelButtonItem:cancelButton
                                                   otherButtonItems:rateButton, nil];
            [alert show];
            
        }];
    }}
    
    {{  // About Instamour
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierAbout];
        [rowInfo setText:@"About Instamour"];
        [rowInfo setRowSelectionBlock:^{
            
            INSInfoViewController *infoViewController = [[INSInfoViewController alloc] init];
            [infoViewController setInfoTitle:@"About Instamour"];
            [infoViewController setUrl:[NSURL URLWithString:@"http://www.instamour.com/corporate/about.htm"]];
            
            [[weakSelf navigationController] pushViewController:infoViewController animated:YES];
            
        }];
    }}
    
    {{  // Privacy Policy
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierPrivacyPolicy];
        [rowInfo setText:@"Privacy Policy"];
        [rowInfo setRowSelectionBlock:^{
            
            INSInfoViewController *infoViewController = [[INSInfoViewController alloc] init];
            [infoViewController setInfoTitle:@"Privacy Policy"];
            [infoViewController setUrl:[NSURL URLWithString:@"http://www.instamour.com/corporate/privacy.htm"]];
            
            [[weakSelf navigationController] pushViewController:infoViewController animated:YES];
            
        }];
    }}
    
    {{  // Terms of Service
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSSettingsRowIdentifierTermsOfService];
        [rowInfo setText:@"Terms of Service"];
        [rowInfo setRowSelectionBlock:^{
            
            INSInfoViewController *infoViewController = [[INSInfoViewController alloc] init];
            [infoViewController setInfoTitle:@"Terms of Service"];
            [infoViewController setUrl:[NSURL URLWithString:@"http://www.instamour.com/corporate/terms.htm"]];
            
            [[weakSelf navigationController] pushViewController:infoViewController animated:YES];
            
        }];
    }}
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContentsManager] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    [[cell textLabel] setText:[rowInfo text]];
    [[cell detailTextLabel] setText:[rowInfo detailText]];
    
    if ([[self privacyRowIdentifiers] containsObject:rowIdentifier])
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if ([rowInfo image] == nil)
        {
            [cell setAccessoryView:nil];
        }
        else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[rowInfo image]];
            [cell setAccessoryView:imageView];
        }
    }
    else
    {
        [cell setAccessoryView:nil];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    
    [rowInfo safelyPerformRowSelectionBlock];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSLocationViewControllerDelegate Methods

- (void)locationViewController:(INSLocationViewController *)viewController
                 didUpdateCity:(NSString *)city
                         state:(NSString *)state
                       country:(NSString *)country
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    [user setCity:city];
    [user setState:state];
    [user setCountry:country];
    // TODO: Add latitude and longitude
    
    [self populateContents];
    
#warning TODO: Report change to server. No current API?
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
