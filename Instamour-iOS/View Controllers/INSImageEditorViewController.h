//
//  INSImageEditorViewController.h
//  Instamour
//
//  Created by Brian Slick on 8/20/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "HFImageEditorViewController.h"

// Public Constants

// Protocols

@interface INSImageEditorViewController : HFImageEditorViewController

// Public Properties

// Public Methods

@end
