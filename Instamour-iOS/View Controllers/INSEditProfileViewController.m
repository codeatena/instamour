//
//  INSEditProfileViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSEditProfileViewController.h"

// Models and other global

// Sub-controllers

// Views
#import "TPKeyboardAvoidingTableView.h"
#import "INSPickerInputView.h"
#import "INSLabelAndTextFieldTableCell.h"

// Private Constants
NSString *const INSEditProfileRowIdentifierUserName = @"INSEditProfileRowIdentifierUserName";
NSString *const INSEditProfileRowIdentifierChildren = @"INSEditProfileRowIdentifierChildren";
NSString *const INSEditProfileRowIdentifierIdentity = @"INSEditProfileRowIdentifierIdentity";
NSString *const INSEditProfileRowIdentifierHeight = @"INSEditProfileRowIdentifierHeight";
NSString *const INSEditProfileRowIdentifierBodyType = @"INSEditProfileRowIdentifierBodyType";
NSString *const INSEditProfileRowIdentifierLookingFor = @"INSEditProfileRowIdentifierLookingFor";
NSString *const INSEditProfileRowIdentifierEthnicity = @"INSEditProfileRowIdentifierEthnicity";
NSString *const INSEditProfileRowIdentifierSmoker = @"INSEditProfileRowIdentifierSmoker";
NSString *const INSEditProfileRowIdentifierSexualPreference = @"INSEditProfileRowIdentifierSexualPreference";
NSString *const INSEditProfileRowIdentifierInstagram = @"INSEditProfileRowIdentifierInstagram";
NSString *const INSEditProfileRowIdentifierVine = @"INSEditProfileRowIdentifierVine";

@interface INSEditProfileViewController () <UITextFieldDelegate, INSPickerInputViewDelegate>

// Private Properties
@property (nonatomic, strong) INSPickerInputView *pickerInputView;

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *children;
@property (nonatomic, copy) NSString *identity;
@property (nonatomic, copy) NSString *height;
@property (nonatomic, copy) NSString *bodyType;
@property (nonatomic, copy) NSString *lookingFor;
@property (nonatomic, copy) NSString *ethnicity;
@property (nonatomic, copy) NSString *smoker;
@property (nonatomic, copy) NSString *sexualPreference;
@property (nonatomic, copy) NSString *instagram;
@property (nonatomic, copy) NSString *vine;

@property (nonatomic, strong) UITextField *activeTextField;
@property (nonatomic, copy) NSArray *activePickerList;

@property (nonatomic, copy) NSSet *listPickerRowIdentifiers;

@property (nonatomic, copy) NSArray *childrenPickerContents;
@property (nonatomic, copy) NSArray *identityPickerContents;
@property (nonatomic, copy) NSArray *heightPickerContents;
@property (nonatomic, copy) NSArray *bodyTypePickerContents;
@property (nonatomic, copy) NSArray *lookingForPickerContents;
@property (nonatomic, copy) NSArray *ethnicityPickerContents;
@property (nonatomic, copy) NSArray *smokerPickerContents;
@property (nonatomic, copy) NSArray *sexualPreferencePickerContents;

@end

@implementation INSEditProfileViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters

- (INSPickerInputView *)pickerInputView
{
    if (_pickerInputView == nil)
    {
        _pickerInputView = [INSPickerInputView pickerInputViewMode:INSPickerInputViewModeAllButtons
                                                          delegate:self];
        [_pickerInputView setPreventBlankRowSelection:YES];
    }
    return _pickerInputView;
}

- (NSSet *)listPickerRowIdentifiers
{
    if (_listPickerRowIdentifiers == nil)
    {
        _listPickerRowIdentifiers = [[NSSet alloc] initWithObjects:INSEditProfileRowIdentifierChildren, INSEditProfileRowIdentifierIdentity, INSEditProfileRowIdentifierHeight, INSEditProfileRowIdentifierBodyType, INSEditProfileRowIdentifierLookingFor, INSEditProfileRowIdentifierEthnicity, INSEditProfileRowIdentifierSmoker, INSEditProfileRowIdentifierSexualPreference, nil];
    }
    return _listPickerRowIdentifiers;
}

- (NSArray *)childrenPickerContents
{
    if (_childrenPickerContents == nil)
    {
        _childrenPickerContents = @[ INSPickerInputViewBlankValue, INSChildrenStatusYes, INSChildrenStatusNo, INSChildrenStatusNotWithMe ];
    }
    return _childrenPickerContents;
}

- (NSArray *)identityPickerContents
{
    if (_identityPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allIdentitiesINS]];
        
        _identityPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _identityPickerContents;
}

- (NSArray *)heightPickerContents
{
    if (_heightPickerContents == nil)
    {
        NSLocale *locale = [NSLocale currentLocale];
        NSString *countryCode = [locale objectForKey:NSLocaleCountryCode];
        
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        
        if ([countryCode isEqualToString:@"US"])
        {
            [contents addObjectsFromArray:@[ @"5'", @"5'1\"", @"5'2\"", @"5'3\"", @"5'4\"", @"5'5\"", @"5'6\"", @"5'7\"", @"5'8\"", @"5'9\"", @"5'10\"", @"5'11\"", @"6'", @"6'1\"", @"6'2\"", @"6'3\"", @"6'4\"", @"6'5\"", @"6'6\"", @"6'7\"", @"6'8\"", @"6'9\"", @"6'10\"", @"6'11\"" ]];
        }
        else
        {
            [contents addObjectsFromArray:@[ @"150 cm", @"152 cm", @"154 cm", @"156 cm", @"158 cm", @"160 cm", @"162 cm", @"164 cm", @"166 cm", @"168 cm", @"170 cm", @"172 cm", @"174 cm", @"176 cm", @"178 cm", @"180 cm", @"182 cm", @"184 cm", @"186 cm", @"188 cm", @"190 cm", @"192 cm", @"194 cm", @"196 cm", @"198 cm", @"200 cm", @"202 cm",  @"204 cm",  @"206 cm",  @"208 cm" ]];
        }
        
        _heightPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _heightPickerContents;
}

- (NSArray *)bodyTypePickerContents
{
    if (_bodyTypePickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allBodyTypesINS]];
        
        _bodyTypePickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _bodyTypePickerContents;
}

- (NSArray *)lookingForPickerContents
{
    if (_lookingForPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allLookingForsINS]];
        
        _lookingForPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _lookingForPickerContents;
}

- (NSArray *)ethnicityPickerContents
{
    if (_ethnicityPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allEthnicitiesINS]];
        
        _ethnicityPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _ethnicityPickerContents;
}

- (NSArray *)smokerPickerContents
{
    if (_smokerPickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allSmokersINS]];
        
        _smokerPickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _smokerPickerContents;
}

- (NSArray *)sexualPreferencePickerContents
{
    if (_sexualPreferencePickerContents == nil)
    {
        NSMutableArray *contents = [NSMutableArray arrayWithObject:INSPickerInputViewBlankValue];
        [contents addObjectsFromArray:[[NSUserDefaults standardUserDefaults] allSexualPreferencesINS]];
        
        _sexualPreferencePickerContents = [[NSArray alloc] initWithArray:contents];
    }
    return _sexualPreferencePickerContents;
}

#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    NSString *userName = [[[INSUserManager sharedManager] currentUser] userName];
    if (![userName isNotEmpty])
    {
        userName = @"Edit Profile";
    }
    
    [self setTitle:userName];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(saveButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:saveButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    [Flurry logEvent:@"Edit Profile View"];
    
    // User Interface
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    NSString *children = [user children];
    if ([children length] == 1)
    {
        children = [INSServerAPIManager childrenStatusForServerCode:children];
    }
    
    [self setUserName:[user userName]];
    [self setChildren:children];
    [self setIdentity:[user identity]];
    [self setHeight:[user height]];
    [self setBodyType:[user bodyType]];
    [self setLookingFor:[user lookingFor]];
    [self setEthnicity:[user ethnicity]];
    [self setSmoker:[user smoker]];
    [self setSexualPreference:[user sexualPreference]];
    [self setInstagram:[user instagramName]];
    [self setVine:[user vineName]];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableContentsManager *manager = [self mainContentsManager];
    [manager reset];
    
    BTITableRowInfo *rowInfo = nil;
    
    {{  // Username
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierUserName];
        [rowInfo setText:@"User Name"];
        [rowInfo setDetailText:[self userName]];
    }}
    
    {{  // Children
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierChildren];
        [rowInfo setText:@"Children"];
        [rowInfo setDetailText:[self children]];
    }}
    
    {{  // Identity
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierIdentity];
        [rowInfo setText:@"Identity"];
        [rowInfo setDetailText:[self identity]];
    }}
    
    {{  // Height
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierHeight];
        [rowInfo setText:@"Height"];
        [rowInfo setDetailText:[self height]];
    }}
    
    {{  // Body Type
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierBodyType];
        [rowInfo setText:@"Body Type"];
        [rowInfo setDetailText:[self bodyType]];
    }}
    
    {{  // Looking For
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierLookingFor];
        [rowInfo setText:@"Looking For"];
        [rowInfo setDetailText:[self lookingFor]];
    }}
    
    {{  // Ethnicity
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierEthnicity];
        [rowInfo setText:@"Ethnicity"];
        [rowInfo setDetailText:[self ethnicity]];
    }}
    
    {{  // Smoker
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierSmoker];
        [rowInfo setText:@"Smoker"];
        [rowInfo setDetailText:[self smoker]];
    }}
    
    {{  // Sexual Preference
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierSexualPreference];
        [rowInfo setText:@"Preference"];
        [rowInfo setDetailText:[self sexualPreference]];
    }}
    
    {{  // Instagram
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierInstagram];
        [rowInfo setText:@"Instagram"];
        [rowInfo setDetailText:[self instagram]];
    }}
    
    {{  // Vine
        rowInfo = [manager dequeueReusableRowInfo];
        [manager addRowInfo:rowInfo makeNewSection:NO];
        
        [rowInfo setIdentifier:INSEditProfileRowIdentifierVine];
        [rowInfo setText:@"Vine"];
        [rowInfo setDetailText:[self vine]];
    }}
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerEditUserDidFinishNotification
                                   selector:@selector(editUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerEditUserFailedNotification
                                   selector:@selector(editUserFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [INSLabelAndTextFieldTableCell registerNibForTableViewBTI:tableView];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)editUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Edit User Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Edit User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    INSUser *currentUser = [userManager currentUser];
    
    [currentUser setUserName:[self userName]];
    [currentUser setChildren:[self children]];
    [currentUser setIdentity:[self identity]];
    [currentUser setHeight:[self height]];
    [currentUser setBodyType:[self bodyType]];
    [currentUser setLookingFor:[self lookingFor]];
    [currentUser setEthnicity:[self ethnicity]];
    [currentUser setSmoker:[self smoker]];
    [currentUser setSexualPreference:[self sexualPreference]];
    [currentUser setInstagramName:[self instagram]];
    [currentUser setVineName:[self vine]];
    
    [userDefaults setBrowserProfilesSearchGenderINS:nil];
    
    [userManager clearBrowserUsers];
    [userManager downloadMoreBrowserUsers];
    
    [userDefaults synchronize];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)editUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSError *error = [notification object];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Profile Update Failed"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self activeTextField] resignFirstResponder];
    
    NSString *userName = [self userName];
    
    if (![userName isNotEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"User Name cannot be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Missing user name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self activeTextField] resignFirstResponder];
    
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Updating..."];
    
    [[INSServerAPIManager sharedManager] editUserWithUserName:[self userName]
                                                       height:[self height]
                                                     identity:[self identity]
                                                    ethnicity:[self ethnicity]
                                                     bodyType:[self bodyType]
                                                   lookingFor:[self lookingFor]
                                             sexualPreference:[self sexualPreference]
                                                       smoker:[self smoker]
                                                     children:[self children]
                                                    instagram:[self instagram]
                                                         vine:[self vine]
                                             audioChatEnabled:nil
                                             videoChatEnabled:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)loadPickerViewDataForRowIdentifier:(NSString *)rowIdentifier
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierChildren])
    {
        [self setActivePickerList:[self childrenPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierIdentity])
    {
        [self setActivePickerList:[self identityPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierHeight])
    {
        [self setActivePickerList:[self heightPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierBodyType])
    {
        [self setActivePickerList:[self bodyTypePickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierLookingFor])
    {
        [self setActivePickerList:[self lookingForPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierEthnicity])
    {
        [self setActivePickerList:[self ethnicityPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierSmoker])
    {
        [self setActivePickerList:[self smokerPickerContents]];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierSexualPreference])
    {
        [self setActivePickerList:[self sexualPreferencePickerContents]];
    }
    else
    {
        [self setActivePickerList:nil];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not a list field", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSPickerInputView *pickerInputView = [self pickerInputView];
    
    NSString *selectedText = [[self activeTextField] text];
    NSInteger preselectedRow = NSNotFound;
    
    if ([selectedText isNotEmpty])
    {
        preselectedRow = [[self activePickerList] indexOfObject:selectedText];
    }
    
    [pickerInputView setPickerViewContents:[self activePickerList] forComponent:0 preselectingRow:preselectedRow];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self mainContentsManager] numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContentsManager] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    
    INSLabelAndTextFieldTableCell *textFieldCell = [INSLabelAndTextFieldTableCell dequeueCellFromTableViewBTI:tableView];
    
    UITextField *textField = [textFieldCell textField];
    [textField setDelegate:self];
    
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    
    [textField setPlaceholder:[rowInfo text]];
    [textField setText:[rowInfo detailText]];
    
    [textFieldCell setLabelText:[rowInfo text]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return textFieldCell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setActiveTextField:textField];
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    if ([[self listPickerRowIdentifiers] containsObject:rowIdentifier])
    {
        [textField setInputView:[self pickerInputView]];
        
        [self loadPickerViewDataForRowIdentifier:rowIdentifier];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self tableView] respondsToSelector:@selector(TPKeyboardAvoiding_scrollToActiveTextField)])
    {
        [(TPKeyboardAvoidingTableView *)[self tableView] TPKeyboardAvoiding_scrollToActiveTextField];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierUserName])
    {
        NSString *newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        
        if ([newString length] >= INSUserNameMaxLength)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User name too long", self, __PRETTY_FUNCTION__);
            return NO;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [textField resignFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    NSString *text = [textField text];
    
    if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierUserName])
    {
        [self setUserName:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierInstagram])
    {
        [self setInstagram:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierVine])
    {
        [self setVine:text];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSPickerInputViewDelegate Methods

- (void)pickerInputView:(INSPickerInputView *)pickerInputView
           didSelectRow:(NSInteger)row
            inComponent:(NSInteger)component
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *textField = [self activeTextField];
    
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:textField];
    BTITableRowInfo *rowInfo = [[self mainContentsManager] rowInfoAtIndexPath:indexPath];
    NSString *rowIdentifier = [rowInfo identifier];
    
    NSString *text = [[self activePickerList] objectAtIndex:row];
    if ([text isEqualToString:INSPickerInputViewBlankValue])
    {
        text = nil;
    }
    
    if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierChildren])
    {
        [self setChildren:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierIdentity])
    {
        [self setIdentity:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierHeight])
    {
        [self setHeight:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierBodyType])
    {
        [self setBodyType:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierLookingFor])
    {
        [self setLookingFor:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierEthnicity])
    {
        [self setEthnicity:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierSmoker])
    {
        [self setSmoker:text];
    }
    else if ([rowIdentifier isEqualToString:INSEditProfileRowIdentifierSexualPreference])
    {
        [self setSexualPreference:text];
    }
    
    [rowInfo setDetailText:text];
    [textField setText:text];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pickerInputViewDoneButtonPressed:(INSPickerInputView *)pickerInputView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self activeTextField] resignFirstResponder];
    
    [self setActiveTextField:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pickerInputViewPreviousButtonPressed:(INSPickerInputView *)pickerInputView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableContentsManager *manager = [self mainContentsManager];
    
    UITextField *currentTextField = [self activeTextField];
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:currentTextField];
    NSInteger currentIndex = [indexPath row];
    
    NSInteger nextIndex = currentIndex - 1;
    
    if (nextIndex < 0)
    {
        nextIndex = [manager numberOfRowsInSection:0] - 1;
    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:nextIndex inSection:0];
    BTITableRowInfo *nextRowInfo = [manager rowInfoAtIndexPath:nextIndexPath];
    NSString *nextIdentifier = [nextRowInfo identifier];
    
    [self loadPickerViewDataForRowIdentifier:nextIdentifier];
    
    INSLabelAndTextFieldTableCell *cell = (INSLabelAndTextFieldTableCell *)[[self tableView] cellForRowAtIndexPath:nextIndexPath];
    
    [[cell textField] becomeFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pickerInputViewNextButtonPressed:(INSPickerInputView *)pickerInputView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BTITableContentsManager *manager = [self mainContentsManager];
    
    UITextField *currentTextField = [self activeTextField];
    NSIndexPath *indexPath = [[self tableView] indexPathForRowContainingViewBTI:currentTextField];
    NSInteger currentIndex = [indexPath row];
    
    NSInteger nextIndex = currentIndex + 1;
    
    if (nextIndex >= [manager numberOfRowsInSection:0])
    {
        nextIndex = 0;
    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:nextIndex inSection:0];
    BTITableRowInfo *nextRowInfo = [manager rowInfoAtIndexPath:nextIndexPath];
    NSString *nextIdentifier = [nextRowInfo identifier];
    
    INSLabelAndTextFieldTableCell *cell = (INSLabelAndTextFieldTableCell *)[[self tableView] cellForRowAtIndexPath:nextIndexPath];
    
    [self loadPickerViewDataForRowIdentifier:nextIdentifier];
    
    [[cell textField] becomeFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
