//
//  BrowseProfilesCollectionViewCell.h
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTICollectionViewCell.h"

// Public Constants

// Protocols

@interface INSBrowseProfilesCollectionViewCell : BTICollectionViewCell

// Public Properties
@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UIImageView *onlineStatusImageView;

// Public Methods

@end
