
package com.hyper.instamour;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.flurry.android.FlurryAgent;
import com.han.network.UserAsyncTask;
import com.han.network.VideoAsyncTask;
import com.han.source.CameraActivity;
import com.han.source.CreateVideoDataAdapter;
import com.han.source.GlobalData;
import com.han.utility.CameraUtility;
import com.han.utility.PreferenceUtility;
import com.hyper.instamour.inappbilling.util.IabHelper;
import com.hyper.instamour.inappbilling.util.IabResult;
import com.hyper.instamour.inappbilling.util.Inventory;
import com.hyper.instamour.inappbilling.util.Purchase;
import com.hyper.instamour.model.CreateVideoData;
import com.hyper.instamour.model.User;
import com.mobeta.android.dslv.DragSortListView;

public class CreateVideos extends Activity {

    private Function c;
    private MenuSlideView scrollView;
    private int layoutToSlide;
    private SlideMenu sm;

    Global global;
    String uid;
    
    // Han's Code
	public static final int REQUEST_CODE_CHOOSE_FILE = 1002;

	LinearLayout llytPurchase;
    Button btnPurchase;
    TextView txtDone;
    
	public DragSortListView lstCreateVideo;
	public ArrayList<CreateVideoData> arrCreateVideoData = new ArrayList<CreateVideoData>();
	public CreateVideoDataAdapter createVideoDataAdapter;
	
	public DragSortListView.DropListener onDrop =
	        new DragSortListView.DropListener() {
	            @Override
	            public void drop(int from, int to) {
	                if (from != to) {
	                	CreateVideoData item = createVideoDataAdapter.getItem(from);
	                    createVideoDataAdapter.remove(item);
	                    createVideoDataAdapter.insert(item, to);
	                    lstCreateVideo.moveCheckState(from, to);
//	                    Log.e("DSLV", "Selected item is " + lstCreateVideo.getCheckedItemPosition());
	                    successSort(from, to);
	                    Log.e("DrogDrop", "from " + Integer.toString(from) + " to " + Integer.toString(to));
	                }
	         }
	    };
	    
	public static final String TAG = "com.hyper.instamour.inappbilling";
	IabHelper mHelper;

	public static final String ITEM_SKU = "com.han.billing.cretevideo";
//	public static final String ITEM_SKU = "android.test.purchased";

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isFailure()) {
		      // Handle error
				Log.e("in-app-billing", "OnIabPurchaseFinishedListener fail");
				return;
			}      
			else if (purchase.getSku().equals(ITEM_SKU)) {
				Log.e("in-app-billing", "OnIabPurchaseFinishedListener success");

				consumeItem();
			    btnPurchase.setEnabled(false);
			}
	   }
	};
	
	public void consumeItem() {
		mHelper.queryInventoryAsync(mReceivedInventoryListener);
	}
		
	IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result, Inventory inventory) {   		   
			if (result.isFailure()) {
			  // Handle failure
				Log.e("in-app-billing", "QueryInventoryFinishedListener fail");

			} else {
				Log.e("in-app-billing", "QueryInventoryFinishedListener success");

				mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), mConsumeFinishedListener);
			}
	    }
	};
	
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
//				clickButton.setEnabled(true);
				Log.e("in-app-billing", "OnConsumeFinishedListener success");
				successPurchase();

			} else {
			    // handle error
				Log.e("in-app-billing", "OnConsumeFinishedListener fail");

			}
		}
	};

    private void initWidget() {
    	llytPurchase = (LinearLayout) findViewById(R.id.purchase_linearLayout);
    	
    	lstCreateVideo = (DragSortListView) findViewById(R.id.create_video_listView);
    	lstCreateVideo.setDropListener(onDrop);
    	lstCreateVideo.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    	btnPurchase = (Button) findViewById(R.id.purchase_button);
    	txtDone = (TextView) findViewById(R.id.done_button_textView);
    }
    
    private void initValue() {
    	String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo9Unb9qzaWPAeHH9KRf/Lyimf09qiNeselxluW56BnAOXH7wDTaKrrnhHTMXjNsSPrOMdlA3LE6LA7lAGsebsw66N2dqy6PIV1IFl9iQ8ibB/nqGYhIXymIVWDw6v7Lc7ColAzWIruaEuQnIGtRgjj2Xtg39/fOrOIsNDbfNK2D1yIAkqlRPgWugvLI72VQZzc4xuhVlkS6+nuUMHitWxcL4KF9nEhFgTvdp6VetUwQ7qgNlApXUPy5QCM7ws9oTbn/x3go0eh0IBZ3NhQBGmNb17WpanwOc5Jreo78HBnJYH0rvDeVT5RxjwMd9L1NC3yU+YO8jN7a4Pq0wAKr8WwIDAQAB";
//    	String base64EncodedPublicKey = "my public key";
    	
		mHelper = new IabHelper(this, base64EncodedPublicKey);
		arrCreateVideoData = new ArrayList<CreateVideoData>();
		
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					Log.e(TAG, "In-app Billing setup failed: " + result);
				} else {
				    Log.e(TAG, "In-app Billing is set up OK");
				}
			}
		});
//    	PreferenceUtility.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

		createVideoDataAdapter = new CreateVideoDataAdapter(this, R.layout.row_create_video, arrCreateVideoData);

//		lstCreatVideo.setCheeseList(arrCreateVideoData);
		lstCreateVideo.setAdapter(createVideoDataAdapter);
		
		setSessionID();
		
		new UserAsyncTask(CreateVideos.this).execute(UserAsyncTask.ACTION_USER_GET_INF);
    }
    
    public void setSessionID() {
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence", MODE_PRIVATE);
		String sessionID = global.preferences.getString("uid", "");
		
		SharedPreferences.Editor editor = PreferenceUtility.sharedPreferences.edit();
        editor.putString("sessionId", String.valueOf(sessionID));
        editor.commit();
    }
    
    private void initEvent() {
    	btnPurchase.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.e("HCW", "purchase button click");
	    	    mHelper.launchPurchaseFlow(CreateVideos.this, ITEM_SKU, 10001, 
	       			   mPurchaseFinishedListener, "mypurchasetoken");
			}
        });
    	txtDone.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        try {
            new VideoSetting(CreateVideos.this);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        Runtime.getRuntime().totalMemory();
        Runtime.getRuntime().freeMemory();
        System.gc();

        LayoutInflater inflater = LayoutInflater.from(this);
        scrollView = (MenuSlideView) inflater.inflate(R.layout.screen_scroll_with_list_menu, null);
        setContentView(scrollView);
        layoutToSlide = R.layout.create_videos;
        sm = new SlideMenu(getApplicationContext(), scrollView, inflater, layoutToSlide);
        try {
            new VideoSetting(CreateVideos.this);
        } catch (Exception e) {

        }
        
        initWidget();
        initValue();
        initEvent();

//        global = Global.getInstance();
//        global.preferences = getSharedPreferences("LoginPrefrence", MODE_PRIVATE);
//        global.editor = global.preferences.edit();
//
//        uid = global.preferences.getString("uid", uid);
//        if (global.from_fb == true) {
//            Log.e("fb", global.preferences.getString("fbuname", ""));
//            global.setFbLoginUsername(global.preferences.getString("fbuname", ""));
//        } else {
//            Log.e("Simple signup", global.preferences.getString("uname", ""));
//        }

        c = new Function();

        if (c.haveNetworkConnection(getApplicationContext())) {
//            new UserGetTask().execute();
        } else {

            AlertDialog.Builder dialog2 = new AlertDialog.Builder(CreateVideos.this);
            dialog2.setTitle("Instamour");
            dialog2.setMessage("No Data Connection Avaible");
            dialog2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alert2 = dialog2.create();
            alert2.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {     
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == CameraUtility.CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            	if (resultCode == RESULT_OK) {
            		Log.e("camera_position", Integer.toString(createVideoDataAdapter.camera_position));
            		createVideoDataAdapter.notifyDataSetChanged();
            		CreateVideoData videoData = arrCreateVideoData.get(createVideoDataAdapter.camera_position);
            		videoData.videoFileUri = CameraActivity.videoFileUri;
            		
		        	new VideoAsyncTask(CreateVideos.this).execute(VideoAsyncTask.ACTION_UPLOAD_VIDEO, 
		        			videoData.videoFileUri.getPath(), Integer.toString(createVideoDataAdapter.camera_position + 1));
            	} else {
    		        CreateVideoData videoData = arrCreateVideoData.get(createVideoDataAdapter.upload_position);
    		        videoData.videoFileUri = null;
            	}
            }
            if (requestCode == REQUEST_CODE_CHOOSE_FILE) {
            	if (resultCode == RESULT_OK) {
    		        CreateVideoData videoData = arrCreateVideoData.get(createVideoDataAdapter.upload_position);

    		        String path = getPath(data.getData());
    		        Log.e("path", path);
    		        videoData.videoFileUri = Uri.parse(path);
    		        
    	        	new VideoAsyncTask(CreateVideos.this).execute(VideoAsyncTask.ACTION_UPLOAD_VIDEO, 
    	        			videoData.videoFileUri.getPath(), Integer.toString(createVideoDataAdapter.upload_position + 1));
    	        }
            }
        }
    }
    
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    
    public void successPurchase() {
    	
    	SharedPreferences.Editor editor = PreferenceUtility.sharedPreferences.edit();
    	editor.putBoolean("isPurchased", true);
    	editor.commit();
    	
    	new UserAsyncTask(this).execute(UserAsyncTask.ACTION_PAID_FEATURE, "videos10", "enable");
    	llytPurchase.setVisibility(View.GONE);
    	arrCreateVideoData.clear();
    	
    	for (int i = 0; i < User.MAX_USER_VIDEO_COUNT; i++) {
    		CreateVideoData videoData = GlobalData.currentUser.arrVideoData.get(i);
    		arrCreateVideoData.add(videoData);
    	}
    	
		createVideoDataAdapter.notifyDataSetChanged();
    }
    
    public void successGetUserInf() {
    	
    	boolean isPurchased = PreferenceUtility.sharedPreferences.getBoolean("isPurchased", false);
//    	isPurchased = false;
    	arrCreateVideoData.clear();

    	if (isPurchased) {
        	llytPurchase.setVisibility(View.GONE);
        	for (int i = 0; i < User.MAX_USER_VIDEO_COUNT; i++) {
        		CreateVideoData videoData = GlobalData.currentUser.arrVideoData.get(i);
        		arrCreateVideoData.add(videoData);
        	}
    	} else {
        	llytPurchase.setVisibility(View.VISIBLE);
        	for (int i = 0; i < User.NOR_USER_VIDEO_COUNT; i++) {
        		CreateVideoData videoData = GlobalData.currentUser.arrVideoData.get(i);
        		arrCreateVideoData.add(videoData);
        	}
    	}
    	
    	createVideoDataAdapter.notifyDataSetChanged();
    }
    
    public void successSort(int from, int to) {
    	
    	String strOrder = "";
    	
    	for (int i = 0; i < arrCreateVideoData.size(); i++) {
    		String strValue;
    		if (i == from) {
    			strValue = Integer.toString(to + 1);
    		} else if (i == to) {
    			strValue = Integer.toString(from + 1);
    		} else {
    			strValue = Integer.toString(i + 1);
    		}
    		if (i == 0) {
    			strOrder += strValue;
    		} else {
    			strOrder = strOrder + "," + strValue;
    		}
    	}
    	
        new VideoAsyncTask(CreateVideos.this).execute(VideoAsyncTask.ACTION_SORT_VIDEO, strOrder);
    }

    @Override
    protected void onStart() { // Set up the Flurry session
        super.onStart();
        FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");

    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (mHelper != null) mHelper.dispose();
    	mHelper = null;
    }
}
