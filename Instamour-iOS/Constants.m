#import "Constants.h"

#pragma mark - Server URLs

// NOTE: Paths must end with /
#define INSServerRequestTestPath            @"https://www.instamourapp.com/api.v020/";
#define INSServerRequestLivePath            @"https://www.instamourapp.com/api.v018/";

#ifdef INS_STORE_BUILD

NSString *const INSServerRequestPath = INSServerRequestLivePath;        // Should always be live server path

#else

NSString *const INSServerRequestPath = INSServerRequestLivePath;

#endif


NSString *const INSServerFilePathEmergencyUseOnly = @"http://s3.instamourapp.com/user_files/";

#pragma mark - Misc Values

NSInteger const INSUserNameMaxLength = 20;

NSInteger const INSMaxNumberOfFreeVideos = 4;
NSInteger const INSMaxNumberOfVideos = 10;
NSInteger const INSAgeRangeMinimum = 18;
NSInteger const INSAgeRangeMaximum = 100;
NSInteger const INSNumberOfDownloadedProfilesPerPage = 20;
NSInteger const INSMaxVideoLengthInSeconds = 8.0;

NSString *const INSMaleString = @"Male";
NSString *const INSFemaleString = @"Female";

NSString *const INSChildrenStatusNo = @"No";
NSString *const INSChildrenStatusYes = @"Yes";
NSString *const INSChildrenStatusNotWithMe = @"They don't live with me";

NSInteger const INSMergeVideoServerSlotNumber = 0;

//NSString *const INSQuickBloxPassword = @"instamourapp";

#pragma mark - Emojis

NSString *const INSKissingLipsEmoji = @"💋";
NSString *const INSFriendsEmoji = @"👫";
NSString *const INSHeartEmoji = @"❤️";
NSString *const INSVideoCameraEmoji = @"📹";
NSString *const INSWarningSignEmoji = @"⚠️";
NSString *const INSPageFacingUpEmoji = @"📄";
