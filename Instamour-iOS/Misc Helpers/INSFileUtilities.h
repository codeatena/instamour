//
//  INSFileUtilities.h
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries
#import <Foundation/Foundation.h>

// Classes and Forward Declarations

// Public Constants

// Protocols

@interface INSFileUtilities : NSObject

// Public Properties

// Public Methods

// Directories
+ (NSURL *)imageDirectoryURL;
+ (NSURL *)videoDirectoryURL;
+ (NSURL *)chatDataDirectoryURL;

+ (NSURL *)uniqueNewVideoURL;
+ (NSURL *)uniqueNewImageURL;

@end
