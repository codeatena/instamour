//
//  INSKiss.m
//  Instamour
//
//  Created by Brian Slick on 7/29/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSKiss.h"

// Private Constants
NSString *const INSKissIDKey = @"INSKissIDKey";
NSString *const INSKissSenderIDKey = @"INSKissSenderIDKey";
NSString *const INSKissDateAddedKey = @"INSKissDateAddedKey";

@interface INSKiss ()

// Private Properties

@end

@implementation INSKiss

#pragma mark - Custom Getters and Setters


#pragma mark - Initialization

- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDictionary
{
    self = [super init];
    if (self)
    {
        [self setIdentifier:[jsonDictionary identifierForKissINS]];
        [self setSenderID:[jsonDictionary senderIDForKissINS]];
        [self setDateAdded:[jsonDictionary dateAddedForKissINS]];
    }
    return self;
}

#pragma mark - NSCoding Methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self identifier] forKey:INSKissIDKey];
    [aCoder encodeObject:[self senderID] forKey:INSKissSenderIDKey];
    [aCoder encodeObject:[self dateAdded] forKey:INSKissDateAddedKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self)
    {
        [self setIdentifier:[aDecoder decodeObjectForKey:INSKissIDKey]];
        [self setSenderID:[aDecoder decodeObjectForKey:INSKissSenderIDKey]];
        [self setDateAdded:[aDecoder decodeObjectForKey:INSKissDateAddedKey]];
    }
    return self;
}

@end
