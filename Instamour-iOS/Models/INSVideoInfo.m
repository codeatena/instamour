//
//  INSVideoInfo.m
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSVideoInfo.h"

// Private Constants
NSString *const INSVideoInfoIdentifierKey = @"INSVideoInfoIdentifierKey";
NSString *const INSVideoInfoServerSlotNumberKey = @"INSVideoInfoServerSlotNumberKey";
NSString *const INSVideoInfoDateAddedKey = @"INSVideoInfoDateAddedKey";
NSString *const INSVideoInfoVideoFileNameKey = @"INSVideoInfoVideoFileNameKey";
NSString *const INSVideoInfoThumbnailFileNameKey = @"INSVideoInfoThumbnailFileNameKey";
NSString *const INSVideoInfoUserIDKey = @"INSVideoInfoUserIDKey";
NSString *const INSVideoInfoUserNameKey = @"INSVideoInfoUserNameKey";

@interface INSVideoInfo ()

// Private Properties
@property (nonatomic, assign, getter = isDownloadingThumbnail) BOOL downloadingThumbnail;

@end

@implementation INSVideoInfo

#pragma mark - Dealloc and Memory Management

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Getters and Setters

- (UIImage *)thumbnailImage
{
    if (_thumbnailImage == nil)
    {
        [self loadLocalImage];
        
        if (_thumbnailImage == nil)
        {
            [self downloadServerImage];
        }
    }
    return _thumbnailImage;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveMemoryWarningNotification:)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
    }
    return self;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDictionary
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    self = [self init];
    if (self)
    {
        NSLog(@"jsonDictionary: %@", jsonDictionary);
        
        [self setIdentifier:[jsonDictionary identifierForVideoINS]];
        [self setServerSlotNumber:[jsonDictionary slotNumberForVideoINS]];
        [self setDateAdded:[jsonDictionary dateAddedForVideoINS]];
        [self setVideoFileName:[jsonDictionary videoFileNameForVideoINS]];
        [self setThumbnailFileName:[jsonDictionary thumbnailFileNameForVideoINS]];
        [self setUserID:[jsonDictionary userIDForVideoINS]];
        [self setUserName:[jsonDictionary userNameForVideoINS]];
        
        NSLog(@"image name: %@", [self thumbnailFileName]);
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return self;
}

#pragma mark - NSCoding Methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self identifier] forKey:INSVideoInfoIdentifierKey];
    [aCoder encodeObject:[self serverSlotNumber] forKey:INSVideoInfoServerSlotNumberKey];
    [aCoder encodeObject:[self dateAdded] forKey:INSVideoInfoDateAddedKey];
    [aCoder encodeObject:[self videoFileName] forKey:INSVideoInfoVideoFileNameKey];
    [aCoder encodeObject:[self thumbnailFileName] forKey:INSVideoInfoThumbnailFileNameKey];
    [aCoder encodeObject:[self userID] forKey:INSVideoInfoUserIDKey];
    [aCoder encodeObject:[self userName] forKey:INSVideoInfoUserNameKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self)
    {
        [self setIdentifier:[aDecoder decodeObjectForKey:INSVideoInfoIdentifierKey]];
        [self setServerSlotNumber:[aDecoder decodeObjectForKey:INSVideoInfoServerSlotNumberKey]];
        [self setDateAdded:[aDecoder decodeObjectForKey:INSVideoInfoDateAddedKey]];
        [self setVideoFileName:[aDecoder decodeObjectForKey:INSVideoInfoVideoFileNameKey]];
        [self setThumbnailFileName:[aDecoder decodeObjectForKey:INSVideoInfoThumbnailFileNameKey]];
        [self setUserID:[aDecoder decodeObjectForKey:INSVideoInfoUserIDKey]];
        [self setUserName:[aDecoder decodeObjectForKey:INSVideoInfoUserNameKey]];
    }
    return self;
}

#pragma mark - Misc Methods

- (BOOL)isPopulated
{
    return [[self videoFileName] isNotEmpty];
}

- (BOOL)isMergedVideo
{
    return [[self serverSlotNumber] isEqualToNumber:@(INSMergeVideoServerSlotNumber)];
}

- (NSURL *)serverVideoFileURL
{
    if (![[self videoFileName] isNotEmpty])
    {
        return nil;
    }
    
    NSString *serverURLString = nil;
    
    if ([self isMergedVideo])
    {
        serverURLString = [[NSUserDefaults standardUserDefaults] serverLowQualityVideosFilePathINS];
    }
    else
    {
        serverURLString = INSServerFilePathEmergencyUseOnly;
    }
        
    return [[NSURL URLWithString:serverURLString] URLByAppendingPathComponent:[self videoFileName]];
}

- (void)loadLocalImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[self thumbnailFileName] isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:[self thumbnailFileName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No local file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    UIImage *image = [[UIImage alloc] initWithData:data];
//    NSLog(@"local image: %@", image);
    
    [self setThumbnailImage:image];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadServerImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isDownloadingThumbnail])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already downloading", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *fileName = [self thumbnailFileName];
    
    if (![fileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setDownloadingThumbnail:YES];
    
    NSString *urlString = [[[NSUserDefaults standardUserDefaults] serverVideoThumbnailImagesFilePathINS] stringByAppendingPathComponent:fileName];
    NSURL *url = [NSURL URLWithString:urlString];
    
    __weak INSVideoInfo *weakSelf = self;
    
    [[INSServerAPIManager sharedManager] downloadImageFromURL:url
                                                         success:^(UIImage *image) {
                                                             
//                                                             NSLog(@"Downloaded image");
                                                             
                                                             [weakSelf setThumbnailImage:image];
                                                             
                                                             if ([weakSelf thumbnailDownloadCompletionBlock] != nil)
                                                             {
                                                                 [weakSelf thumbnailDownloadCompletionBlock](image);
                                                                 
                                                                 [weakSelf setThumbnailDownloadCompletionBlock:nil];
                                                             }
                                                             
                                                             NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];
                                                             
                                                             NSData *data = UIImagePNGRepresentation(image);
                                                             
                                                             [data writeToURL:fileURL atomically:YES];
                                                             
                                                             [weakSelf setDownloadingThumbnail:NO];
                                                             
                                                         }
                                                         failure:^(NSString *errorMessage) {
                                                             
//                                                             NSLog(@"Failed to download image");
//                                                             NSLog(@"errorMessage: %@", errorMessage);
//                                                             NSLog(@"url: %@", url);
                                                             [weakSelf setDownloadingThumbnail:NO];
                                                             
                                                         }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)didReceiveMemoryWarningNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setThumbnailImage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
