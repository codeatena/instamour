package com.han.network;

import com.hyper.instamour.CreateVideos;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class VideoAsyncTask extends AsyncTask<String, Integer, Integer> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public static String ACTION_UPLOAD_VIDEO = "action_upload_video";
	public static String ACTION_DELETE_VIDEO = "action_delete_video";
	public static String ACTION_SORT_VIDEO = "action_sort_video";
	
	public String strLoading;
	
	public Context parent;
	public ProgressDialog dlgLoading;
	
	public String curAction;
	public int nResult;
	
	public VideoAsyncTask(Context _parent) {
		parent = _parent;
		strLoading = "\tPlease wait...";
	}
	
	public VideoAsyncTask(Context _parent, String _strLoading) {
		parent = _parent;
		strLoading = _strLoading;
	}
	
	protected Integer doInBackground(String... params) {
		nResult = FAIL;
		
		curAction = params[0];
		if (curAction.equals(ACTION_UPLOAD_VIDEO)) {
			String fileUriPath = params[1];
			String strCount = params[2];
			
			nResult = VideoWebService.uploadVideo(fileUriPath, strCount);
			if (nResult == SUCCESS) {
				nResult = UserWebService.userGetInf();
			}
		}
		if (curAction.equals(ACTION_DELETE_VIDEO)) {
			String strCount = params[1];
			
			nResult = VideoWebService.deleteVideo(strCount);
			if (nResult == SUCCESS) {
				nResult = UserWebService.userGetInf();
			}
		}
		if (curAction.equals(ACTION_SORT_VIDEO)) {
			String strOrder = params[1];
			
			nResult = VideoWebService.sorVideo(strOrder);
			if (nResult == SUCCESS) {
				nResult = UserWebService.userGetInf();
			}
		}
		
		return nResult;
	}
	
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage(strLoading);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}

	protected void onPostExecute(Integer result) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (curAction.equals(ACTION_UPLOAD_VIDEO)) {
			if (nResult == SUCCESS) {
				CreateVideos createVideos = (CreateVideos) parent;
				createVideos.successGetUserInf();
			}
		}
		if (curAction.equals(ACTION_DELETE_VIDEO)) {
			if (nResult == SUCCESS) {
				CreateVideos createVideos = (CreateVideos) parent;
				createVideos.successGetUserInf();
			}
		}
		if (curAction.equals(ACTION_SORT_VIDEO)) {
			if (nResult == SUCCESS) {
				CreateVideos createVideos = (CreateVideos) parent;
				createVideos.successGetUserInf();
			}
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
}
