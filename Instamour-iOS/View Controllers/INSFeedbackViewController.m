//
//  INSFeedbackViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/13/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSFeedbackViewController.h"

// Models and other global

// Sub-controllers

// Views

// Private Constants

@interface INSFeedbackViewController () <UITextViewDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UITextView *inputTextView;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *distanceFromBottomConstraint;

@end

@implementation INSFeedbackViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Feedback"];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithTitle:@"Send"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sendButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:sendButton];

    [[self inputTextView] setText:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewWillAppear:animated];
    
    [[self inputTextView] becomeFirstResponder];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:UIKeyboardWillShowNotification
                                   selector:@selector(keyboardWillShowNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:UIKeyboardWillHideNotification
                                   selector:@selector(keyboardWillHideNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFeedbackDidFinishNotification
                                   selector:@selector(feedbackDidFinishNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFeedbackFailedNotification
                                   selector:@selector(feedbackFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)keyboardWillShowNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
	CGRect keyboardFrameEnd = [(NSValue *)[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	CGRect adjustedKeyboardFrameEnd = [[self view] convertRect:keyboardFrameEnd fromView:nil];
	CGFloat animationDuration = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat animationCurve = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue];
    
    [[self view] layoutIfNeeded];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         
                         [[self distanceFromBottomConstraint] setConstant:CGRectGetHeight(adjustedKeyboardFrameEnd)];
                         [[self view] layoutIfNeeded];
                         
                     }
                     completion:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)keyboardWillHideNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
	CGFloat animationDuration = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat animationCurve = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue];
    
    [[self view] layoutIfNeeded];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         
                         [[self distanceFromBottomConstraint] setConstant:0];
                         [[self view] layoutIfNeeded];
                         
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)feedbackDidFinishNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Send Feedback Failed"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Send Feedback Failed", self, __PRETTY_FUNCTION__);
        return;
    }

    __weak INSFeedbackViewController *weakSelf = self;
    
    RIButtonItem *doneButton = [RIButtonItem item];
    [doneButton setLabel:@"Done"];
    [doneButton setAction:^{
       
        [[weakSelf navigationController] popViewControllerAnimated:YES];
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback Received"
                                                    message:@"Thank you!"
                                           cancelButtonItem:doneButton
                                           otherButtonItems:nil];
    
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)feedbackFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Send Feedback Failed"
                                                      error:[notification object]
                                                  orMessage:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self inputTextView] resignFirstResponder];

    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self inputTextView] resignFirstResponder];
    
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Sending..."];
    
    [[INSServerAPIManager sharedManager] sendFeedback:[[self inputTextView] text]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextViewDelegate Methods

- (void)textViewDidChange:(UITextView *)textView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSString *text = [textView text];
    
    [[[self navigationItem] rightBarButtonItem] setEnabled:([text length] > 0)];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
