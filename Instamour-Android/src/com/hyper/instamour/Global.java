
package com.hyper.instamour;

import java.util.ArrayList;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;

import com.crittercism.app.Crittercism;
import com.hyper.instamour.model.Model;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

@ReportsCrashes(formUri = "http://www.bugsense.com/api/acra?api_key=76243b28", formKey = "")
public class Global extends Application {

    public static final String CRITTERCISM_APP_ID = "53b15ce6bb9475465a000003";

    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    boolean is_phone_checked = false;
    boolean is_video_checked = false;

    private String friendjabberid;
    private static Global instance;
    private String friendphoto;
    private String friendname;
    boolean from_fb;
    String loginusername;
    String fbLoginUsername;
    private String photo;
    String jabberid;
    private ArrayList<Model> chatList;
    private ArrayList<Model> redirectToChat;
    boolean chat_image = false;
    boolean is_account_checked = false;
    boolean from_next = false;
    boolean is_searched = false;
    boolean from_audio = false;
    boolean from_edit = false;
    String password;

    // new added
    boolean From_create_video = false;
    Boolean front_camera = false;
    boolean back_camera = false;

    public ArrayList<Model> list, Convlist;

    public ArrayList<Model> getChatList() {
        return chatList;
    }

    public ArrayList<Model> getRedirectToChat() {
        return redirectToChat;
    }

    public void setRedirectToChat(ArrayList<Model> redirectToChat) {
        this.redirectToChat = redirectToChat;
    }

    public void setChatList(ArrayList<Model> chatList) {
        this.chatList = chatList;
    }

    public String getFriendphoto() {
        return friendphoto;
    }

    public void setFriendphoto(String friendphoto) {
        this.friendphoto = friendphoto;
    }

    public String getFriendname() {
        return friendname;
    }

    public void setFriendname(String friendname) {
        this.friendname = friendname;
    }

    public String getFriendjabberid() {
        return friendjabberid;
    }

    public void setFriendjabberid(String friendjabberid) {
        this.friendjabberid = friendjabberid;
    }

    public boolean isFrom_edit() {
        return from_edit;
    }

    public void setFrom_edit(boolean from_edit) {
        this.from_edit = from_edit;
    }

    public boolean isFrom_fb() {
        return from_fb;
    }

    public void setFrom_fb(boolean from_fb) {
        this.from_fb = from_fb;
    }

    public boolean isFrom_audio() {
        return from_audio;
    }

    public void setFrom_audio(boolean from_audio) {
        this.from_audio = from_audio;
    }

    QBUser qbuser;

    public String getFbLoginUsername() {
        return fbLoginUsername;
    }

    public void setFbLoginUsername(String fbLoginUsername) {
        this.fbLoginUsername = fbLoginUsername;
    }

    public ArrayList<Model> getList() {
        return list;
    }

    public void setList(ArrayList<Model> list) {
        this.list = list;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Global() {

        // TODO Auto-generated constructor stub
        instance = this;

    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        ACRA.init(this);
        Crittercism.initialize(getApplicationContext(), CRITTERCISM_APP_ID);
        chatList = new ArrayList<Model>();
        Convlist = new ArrayList<Model>();
        startService(new Intent(this, QBVideoChatService.class));

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public QBUser getQbuser() {
        return qbuser;
    }

    public void setQbuser(QBUser qbuser) {
        this.qbuser = qbuser;
    }

    public static Global getInstance() {
        return instance;
    }

    public String getJabberid() {
        return jabberid;
    }

    public void setJabberid(String jabberid) {
        this.jabberid = jabberid;
    }

    public String getLoginusername() {
        return loginusername;
    }

    public void setLoginusername(String loginusername) {
        this.loginusername = loginusername;
    }

    public boolean isIs_phone_checked() {
        return is_phone_checked;
    }

    public void setIs_phone_checked(boolean is_phone_checked) {
        this.is_phone_checked = is_phone_checked;
    }

    public boolean isIs_video_checked() {
        return is_video_checked;
    }

    public void setIs_video_checked(boolean is_video_checked) {
        this.is_video_checked = is_video_checked;
    }

    public boolean isIs_account_checked() {
        return is_account_checked;
    }

    public void setIs_account_checked(boolean is_account_checked) {
        this.is_account_checked = is_account_checked;
    }
}
