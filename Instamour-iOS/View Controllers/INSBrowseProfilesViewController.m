//
//  INSBrowseProfilesViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/15/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSBrowseProfilesViewController.h"


// Models and other global
#import "INSTutorialPageInfo.h"

// Sub-controllers
#import "INSCustomSearchViewController.h"
#import "INSTutorialViewController.h"
#import "INSProfileViewController.h"

// Views
#import "INSBrowseProfilesCollectionViewCell.h"

// Private Constants

@interface INSBrowseProfilesViewController () <INSTutorialViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@property (nonatomic, strong) INSTutorialViewController *tutorialViewController;
@property (nonatomic, strong) NSArray *tutorialPages;

@property (nonatomic, assign) INSBrowseProfilesDisplayMode displayMode;

@end

@implementation INSBrowseProfilesViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods

- (instancetype)initWithDisplayMode:(INSBrowseProfilesDisplayMode)displayMode
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    self = [self init];
    if (self)
    {
        [self setDisplayMode:displayMode];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return self;
}

#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage navigationBarLogoINS]];
    [[self navigationItem] setTitleView:imageView];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"]
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(searchButtonPressed:)];
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self setActivityIndicatorView:indicatorView];
    [indicatorView setHidesWhenStopped:YES];
    
    UIBarButtonItem *indicatorButton = [[UIBarButtonItem alloc] initWithCustomView:indicatorView];
    
    [[self navigationItem] setRightBarButtonItems:@[ searchButton, indicatorButton ]];
    
    [self setBackBarButtonTitleBTI:@"Profiles"];
    
    // Other UI Setup
    
    UICollectionView *collectionView = [self collectionView];

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [self setRefreshControl:refreshControl];
    
    [refreshControl setTintColor:[UIColor grayColor]];
    [refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [collectionView addSubview:refreshControl];
    
    [collectionView setScrollsToTop:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    [Flurry logEvent:@"Browse Profiles"];
    
    // User Interface
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setBrowserGenderSearchCriteriaIfNecessaryINS];
    
    NSString *browserSearchGender = [userDefaults browserProfilesSearchGenderINS];
    
    if ([browserSearchGender isNotEmpty])
    {
        [self downloadProfiles];
    }
    else
    {
        [self promptUserToChooseGender];
    }
    
    [[self collectionView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidAppear:animated];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
    if ([userDefaults isCorporateAlertsEnabledINS])
    {
        if ([userDefaults isVideoNeededAlertEnabledINS])
        {
            if (![[INSVideoDataManager sharedManager] isAnyVideoPopulated])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"Don't forget to record a profile video if you want to be seen by other people!"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                [alert show];
                
                [userDefaults setVideoNeededAlertEnabledINS:NO];
            }
        }
    }
    
    if (![INSServerAPIManager isNetworkConnectionAvailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection"
                                                        message:@"In order to properly use Instamour you need an Internet connection; some features won't work properly until you've got one."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSUserManagerDidChangeBrowserUsersNotification
                                   selector:@selector(browserUsersDidChangeNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSUserManagerDidChangeSearchUsersNotification
                                   selector:@selector(searchUsersDidChangeNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTICollectionViewController Overrides

- (void)registerNibsForCollectionView:(UICollectionView *)collectionView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [INSBrowseProfilesCollectionViewCell registerNibForCollectionViewBTI:collectionView];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)browserUsersDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self displayMode] != INSBrowseProfilesDisplayModeBrowse)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Wrong display mode", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self activityIndicatorView] stopAnimating];
    
    [[self mainContents] removeAllObjects];
    [[self mainContents] addObjectsFromArray:[[INSUserManager sharedManager] browserUsers]];
    
    [[self refreshControl] endRefreshing];
    
    [[self collectionView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)searchUsersDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self displayMode] != INSBrowseProfilesDisplayModeCustomSearch)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Wrong display mode", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self activityIndicatorView] stopAnimating];

    [[self mainContents] removeAllObjects];
    [[self mainContents] addObjectsFromArray:[[INSUserManager sharedManager] searchUsers]];
    
    [[self refreshControl] endRefreshing];
    
    [[self collectionView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)refreshControlValueChanged:(UIRefreshControl *)control
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self activityIndicatorView] startAnimating];
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    switch ([self displayMode])
    {
        case INSBrowseProfilesDisplayModeBrowse:
        {
            [userManager clearBrowserUsers];
            [userManager downloadMoreBrowserUsers];
        }
            break;
        case INSBrowseProfilesDisplayModeCustomSearch:
        {
            [userManager clearSearchUsers];
            [userManager downloadMoreSearchUsers];
        }
            break;
        default:
            break;
    }
    
    [[self collectionView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)searchButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSCustomSearchViewController *nextViewController = [[INSCustomSearchViewController alloc] init];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)showTutorialView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableArray *pages = [NSMutableArray array];
    
    INSTutorialPageInfo *pageInfo = nil;
    
    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Press the video icon to watch videos of potential amours!"];
    [pageInfo setImageName:@"video-play"];
    
    [pages addObject:pageInfo];
    
    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Push the Heart Icon if you like what you see & hear. If the amour pushes your heart back, then you are a match, and can Instant Chat, Video Call or in-app Phone call each other!"];
    [pageInfo setImageName:@"heart"];
    
    [pages addObject:pageInfo];
    
    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Hit the X Icon to remove a user from your feed."];
    [pageInfo setImageName:@"reject"];
    
    [pages addObject:pageInfo];
    
    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Click the Gift Icon if you want to buy someone a gift they can actually use!"];
    [pageInfo setImageName:@"gift"];
    
    [pages addObject:pageInfo];
    
    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Flag a user whose video is inappropriate. "];
    [pageInfo setImageName:@"flag"];
    
    [pages addObject:pageInfo];
    
    INSTutorialViewController *nextViewController = [[INSTutorialViewController alloc] init];
    [self setTutorialViewController:nextViewController];
    [nextViewController setDelegate:self];
    
    [nextViewController setTutorialPageInfos:pages];
    
    UIView *view = [nextViewController view];
    
    [view setFrame:[[self view] bounds]];
    
    [[self view] addSubview:view];
    
    [nextViewController beginTutorial];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)promptUserToChooseGender
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    __weak INSBrowseProfilesViewController *weakSelf = self;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    RIButtonItem *femaleButton = [RIButtonItem item];
    [femaleButton setLabel:@"Female"];
    [femaleButton setAction:^{
        
        NSLog(@"Female");
        [userDefaults setBrowserProfilesSearchGenderINS:INSFemaleString];
        
        [weakSelf downloadProfiles];
        
    }];
    
    RIButtonItem *maleButton = [RIButtonItem item];
    [maleButton setLabel:@"Male"];
    [maleButton setAction:^{
        
        NSLog(@"Male");
        [userDefaults setBrowserProfilesSearchGenderINS:INSMaleString];
        
        [weakSelf downloadProfiles];
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Gender"
                                                    message:@"What gender would you like to search for?"
                                           cancelButtonItem:nil
                                           otherButtonItems:femaleButton, maleButton, nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadProfiles
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self activityIndicatorView] startAnimating];
    
    switch ([self displayMode])
    {
        case INSBrowseProfilesDisplayModeCustomSearch:
            [self downloadSearchProfiles];
            break;
        case INSBrowseProfilesDisplayModeBrowse:
        default:
            [self downloadBrowseProfiles];
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadBrowseProfiles
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    [[self mainContents] removeAllObjects];
    [[self mainContents] addObjectsFromArray:[userManager browserUsers]];
    
    if (![userManager isDownloadingBrowserUsers])
    {
        [userManager downloadMoreBrowserUsers];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadSearchProfiles
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    [[self mainContents] removeAllObjects];
    [[self mainContents] addObjectsFromArray:[userManager searchUsers]];
    
    if (![userManager isDownloadingSearchUsers])
    {
        [userManager downloadMoreSearchUsers];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)refreshStatusLabel
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UILabel *statusLabel = [self statusLabel];
    
    if ([[self mainContents] count] == 0)
    {
        [statusLabel setHidden:NO];
        
        INSUserManager *userManager = [INSUserManager sharedManager];
        
        switch ([self displayMode])
        {
            case INSBrowseProfilesDisplayModeCustomSearch:
            {
                if ([userManager isDownloadingSearchUsers])
                {
                    [statusLabel setText:@"Loading..."];
                }
                else
                {
                    [statusLabel setText:@"No Profiles"];
                }
            }
                break;
            case INSBrowseProfilesDisplayModeBrowse:
            {
                if ([userManager isDownloadingBrowserUsers])
                {
                    [statusLabel setText:@"Loading..."];
                }
                else
                {
                    [statusLabel setText:@"No Profiles"];
                }
            }
                break;
            default:
                break;
        }
    }
    else
    {
        [statusLabel setHidden:YES];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSTutorialViewControllerDelegate Methods

- (void)tutorialViewControllerDidFinish:(INSTutorialViewController *)tutorialViewController
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[tutorialViewController view] removeFromSuperview];
    
    [self setTutorialViewController:nil];
    
    [[self collectionView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    [self refreshStatusLabel];
    
    return [[self mainContents] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSBrowseProfilesCollectionViewCell *cell = [INSBrowseProfilesCollectionViewCell dequeueCellFromCollectionViewBTI:collectionView
                                                                                                         forIndexPath:indexPath];
    
    NSInteger row = [indexPath row];
    
    INSUser *user = [[self mainContents] objectAtIndex:row];
    
    UIImageView *statusImageView = [cell onlineStatusImageView];
    [statusImageView setHidden:![user isOnline]];

    __weak UIImageView *avatarImageView = [cell avatarImageView];
    [avatarImageView setImage:nil];
    
    UIImage *avatarImage = [user avatarImage];
    if (avatarImage == nil)
    {
        [user setAvatarDownloadCompletionBlock:^(UIImage *image){
            
            [avatarImageView setImage:image];
            
        }];
        
        avatarImage = [UIImage placeholderAvatarImageINS];
    }
    else
    {
        [user setAvatarDownloadCompletionBlock:nil];
    }
    
    [avatarImageView setImage:avatarImage];
    
    if ([indexPath row] == [[self mainContents] count] - (INSNumberOfDownloadedProfilesPerPage / 2) )
    {
        //        NSLog(@"Download more");
        [self downloadProfiles];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

#pragma mark -  UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *user = [[self mainContents] objectAtIndex:[indexPath row]];
    
    INSProfileViewController *nextViewController = [[INSProfileViewController alloc] init];
    [nextViewController setUser:user];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSBrowseProfilesCollectionViewCell *profileCell = (INSBrowseProfilesCollectionViewCell *)cell;
    
    [[profileCell avatarImageView] setImage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
