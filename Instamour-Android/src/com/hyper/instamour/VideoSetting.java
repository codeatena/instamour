package com.hyper.instamour;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.hyper.instamour.model.ImageLoader;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.result.Result;
import com.quickblox.module.auth.QBAuth;
import com.quickblox.module.auth.result.QBSessionResult;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.service.QBVideoChatService;
import com.quickblox.module.videochat.model.listeners.OnQBVideoChatListener;
import com.quickblox.module.videochat.model.objects.CallState;
import com.quickblox.module.videochat.model.objects.VideoChatConfig;
import com.quickblox.module.videochat.model.utils.Debugger;

public class VideoSetting {

	private VideoChatConfig videoChatConfig;
	private boolean isCanceledVideoCall;
	private boolean calling = false;
	private boolean audio_call = false;
	private Function c;
	private ImageLoader imageLoader;
	Dialog d_videoCalling;
	MediaPlayer mMediaPlayer;
	private QBUser qbUser;
	// private ProgressDialog progressDialog;
	private String image_profile;
	private Global global;
	private Context context;

	public VideoSetting() {
	}

	public VideoSetting(Context mcontext) {
		// TODO Auto-generated constructor stub
		context = mcontext;
		try {
			global = Global.getInstance();
			global.preferences = context.getSharedPreferences("LoginPrefrence",
					0);
			global.editor = global.preferences.edit();
			c = new Function();
			qbUser = new QBUser();
			imageLoader = new ImageLoader(context);
			d_videoCalling = new Dialog(context);

			createSession(global.getLoginusername(), "instamourapp");
		} catch (IllegalStateException e1) {
			// TODO: handle exception
			Toast.makeText(mcontext, "Server busy", Toast.LENGTH_LONG).show();
		} catch (JsonSyntaxException e2) {
			// TODO: handle exception
			Toast.makeText(mcontext, "Server busy", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	private OnQBVideoChatListener qbVideoChatListener = new OnQBVideoChatListener() {

		@Override
		public void onVideoChatStateChange(final CallState state,
				final VideoChatConfig receivedVideoChatConfig) {

			// TODO Auto-generated method stub
			Log.e("Video Listener : ", "Video ListenerVideo Listener");
			try {

				videoChatConfig = receivedVideoChatConfig;
				isCanceledVideoCall = false;

				Log.e("State Name isc : ", "------>" + state.name());

				switch (state) {
				case ON_CALLING:
					Log.e("hiiii", "iiiiiiiii");

					Log.e("Calling is : -----> ", "---->" + calling);
					if (calling == false) {
						Log.e("Calling inside if is : -----> ", "---->"
								+ calling);
						calling = true;
						showCallDialog();
					}

					break;
				case ON_ACCEPT_BY_USER:
					calling = false;
					// progressDialog.dismiss();
					if (audio_call == true) {
						Log.e("ON_START_CONNECTING : Audio", "Audio");
						startAudioActivity();
					} else {
						Log.e("ON_START_CONNECTING : Video", "Video");
						startVideoChatActivity();
					}

					break;
				case ON_REJECTED_BY_USER:
					calling = false;

					// progressDialog.dismiss();
					AlertDialog.Builder dialog2 = new AlertDialog.Builder(
							context);
					dialog2.setTitle("Instamour");
					dialog2.setMessage("User rejected answered");
					dialog2.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
								}
							});
					AlertDialog alert2 = dialog2.create();
					alert2.show();

					break;
				case ON_DID_NOT_ANSWERED:
					Log.e("hiiii", "Did not answered");
					calling = false;
					// progressDialog.dismiss();
					AlertDialog.Builder dialog1 = new AlertDialog.Builder(
							context);
					dialog1.setTitle("Instamour");
					dialog1.setMessage("Sorry, but this member didn't answer."
							+ " Try again later or send an instant chat.");
					dialog1.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									// calling = false;

								}
							});
					AlertDialog alert1 = dialog1.create();
					alert1.show();
					// showCallDialog();
					break;
				case ON_CANCELED_CALL:
					calling = false;
					isCanceledVideoCall = true;
					videoChatConfig = null;
					AlertDialog.Builder dialog = new AlertDialog.Builder(
							context);
					dialog.setTitle("Instamour");
					dialog.setMessage("Sorry, but this member didn't answer. "
							+ "Try again later or send an instant chat");
					dialog.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									// calling = false;
								}
							});
					AlertDialog alert = dialog.create();
					alert.show();
					dialog.setMessage("User canceled called").create().show();
					break;
				case ON_START_CONNECTING:
					calling = false;
					// progressDialog.dismiss();
					if (audio_call == true) {
						Log.e("ON_START_CONNECTING : Audio", "Audio");
						startAudioActivity();
					} else {
						Log.e("ON_START_CONNECTING : Video", "Video");
						startVideoChatActivity();
					}
					break;
				}

			} catch (Exception e) {
				// TODO: handle exception

				AlertDialog.Builder dialog2 = new AlertDialog.Builder(context);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("Sorry!! Unable to catch Oppenent, Please Try Again.");

				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								d_videoCalling.dismiss();
							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();

			}

		}
	};

	private void startVideoChatActivity() {

		Log.e("new Activity : ", "New Activity");
		try {

			Intent intent = new Intent(context, ActivityVideoChat.class);
			intent.putExtra(VideoChatConfig.class.getCanonicalName(),
					videoChatConfig);
			context.startActivity(intent);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void showCallDialog() {
		try {
			mMediaPlayer = new MediaPlayer();
			mMediaPlayer = MediaPlayer.create(context, R.raw.ringing);
			try {
				mMediaPlayer.prepare();
			} catch (IllegalStateException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			}
			/*
			 * mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC) ;
			 */
			mMediaPlayer.setLooping(true);
			mMediaPlayer.start();

			mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub

					mMediaPlayer.stop();

					mMediaPlayer.release();
				}
			});

			Log.e("hi", "hi" + "");

			// calling = false;
			final Dialog d_videoCalling = new Dialog(context);
			d_videoCalling.requestWindowFeature(Window.FEATURE_NO_TITLE);
			d_videoCalling.setContentView(R.layout.videochat);
			d_videoCalling.setCancelable(true);
			d_videoCalling.show();

			ImageView img_ans = (ImageView) d_videoCalling
					.findViewById(R.id.imagviewshow);
			Button btn_ans = (Button) d_videoCalling.findViewById(R.id.btnAns);
			Button btn_decline = (Button) d_videoCalling
					.findViewById(R.id.btndecline);

			// if (image_profile != null) {
			// Log.e("", msg)
			if (c.haveNetworkConnection(context)) {
				Log.e("friend photo", global.getFriendphoto());
				imageLoader.DisplayImage(c.dolink + global.getFriendphoto(),
						img_ans);
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(context);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();

			}

			// }

			btn_ans.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (videoChatConfig == null) {
						Toast.makeText(context, "Call canceled by caller",
								Toast.LENGTH_SHORT).show();

						return;
					}

					Log.e("Call accept  : ", "Call accept ");
					QBVideoChatService.getService().acceptCall(videoChatConfig);
					calling = false;
					d_videoCalling.dismiss();
					mMediaPlayer.stop();

					mMediaPlayer.release();

				}
			});

			btn_decline.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (videoChatConfig == null) {
						Toast.makeText(context, "Call canceled by caller",
								Toast.LENGTH_SHORT).show();
						return;
					}
					Log.e("call reject", "call decline");

					QBVideoChatService.getService().rejectCall(videoChatConfig);
					calling = false;
					d_videoCalling.dismiss();
					mMediaPlayer.stop();

					mMediaPlayer.release();

				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void createSession(String login, final String password) {

		Log.e("LOgin Name is : ", login + "----->" + password + "-----> "
				+ global.getJabberid());

		QBAuth.createSession(login, password, new QBCallbackImpl() {
			@Override
			public void onComplete(Result result) {

				try {
					if (result.isSuccess()) {
						// save current user
						Log.e("Result is sucesss : ", "sucesssss");

						DataHolder.getInstance().setCurrentQbUser(
								((QBSessionResult) result).getSession()
										.getUserId(), password);
						QBUser currentQbUser = DataHolder.getInstance()
								.getCurrentQbUser();

						Log.e("QBUSER Name is  :",
								"------>" + currentQbUser.getId() + "------>"
										+ currentQbUser.getPassword());

						Debugger.logConnection("setQBVideoChatListener: "
								+ (currentQbUser == null));
						try {
							QBVideoChatService.getService()
									.setQBVideoChatListener(currentQbUser,
											qbVideoChatListener);
						} catch (Exception e) {
							Log.e("Error : ",
									"Error ----->" + result.getErrors());
						}

						try {
							calling = false;
							QBVideoChatService
									.getService()
									.setQbVideoChatListener(qbVideoChatListener);
						} catch (NullPointerException ex) {
							ex.printStackTrace();
							Log.e("error resume ", "error resume");
						}

					} else {
						Log.e("Result : ", "------->" + result.getErrors());
					}
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		});

	}

	OnQBVideoChatListener qbVideoChatListenerAudio = new OnQBVideoChatListener() {

		@Override
		public void onVideoChatStateChange(CallState callState,
				VideoChatConfig arg1) {
			// TODO Auto-generated method stub

			switch (callState) {
			case ON_CALL_START:
				Toast.makeText(context, "Call start", Toast.LENGTH_SHORT)
						.show();

				break;
			case ON_CANCELED_CALL:
				Toast.makeText(context, "Call canceled by caller",
						Toast.LENGTH_SHORT).show();

				break;
			case ON_CALL_END:
				Toast.makeText(context, "Call end", Toast.LENGTH_SHORT).show();
				break;
			}

		}
	};

	private void startAudioActivity() {

		try {
			QBVideoChatService.getService().startVideoChat(videoChatConfig);
			QBVideoChatService.getService().setQbVideoChatListener(
					qbVideoChatListenerAudio);

		} catch (Exception e) {
			// TODO: handle exception
		}
		Log.e("Audio Activity : ", "Audio Activity");

	}

}
