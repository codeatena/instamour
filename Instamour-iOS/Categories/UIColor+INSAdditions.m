//
//  UIColor+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/13/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "UIColor+INSAdditions.h"

@implementation UIColor (INSAdditions)

+ (UIColor *)normalTableCellTextColorINS
{
    return [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:248.0/255.0 alpha:1.0];
}

+ (UIColor *)highlightedTableCellTextColorINS
{
    return [UIColor whiteColor];
}

+ (UIColor *)profileCommentColorINS
{
    return [UIColor colorWithRed:102.0/255.0 green:101.0/255.0 blue:99.0/255.0 alpha:1.0];
}

+ (UIColor *)switchOnTintColorINS
{
    return [UIColor colorWithRed:71.0/255.0 green:170.0/255.0 blue:246.0/255.0 alpha:1.0];
}

+ (UIColor *)commentUserNameColorINS
{
    return [UIColor colorWithRed:31.0/255.0 green:98.0/255.0 blue:153.0/255.0 alpha:1.0];
}

+ (UIColor *)commentTimeColorINS
{
    return [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1.0];
}

+ (UIColor *)commentMessageColorINS
{
    return [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
}

+ (UIColor *)instamourPrimaryBackgroundRedColorINS
{
    return [UIColor colorWithRed:239.0/255.0 green:46.0/255.0 blue:55.0/255.0 alpha:1.0];
}

@end
