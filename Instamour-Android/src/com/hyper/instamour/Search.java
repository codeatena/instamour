package com.hyper.instamour;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import com.hyper.instamour.model.Model;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

public class Search extends Activity {
	private ProgressDialog pd;
	Global global;
	private String uid;
	private ArrayList<Model> records;
	private Function c;
	private List<NameValuePair> param;
	private MenuSlideView scrollView;
	private int layoutToSlide;
	private View anapp;

	private Spinner spinner_minage, spinner_maxage, spinner_video,
			spinner_lookingfor, spinner_ethnicity, spinner_smoker,
			spinner_gender, spinner_miles, spinner_lastonline,
			spinner_preference;
	private String gender, min, max, video, lookingfor, ethnicity, preference,
			smoker, miles, lastonline;
	private int min_pos, max_pos, video_pos, lookingfor_pos, ethnicity_pos,
			smoker_pos, gender_pos, miles_pos, lastonline_pos, preference_pos;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			new VideoSetting(Search.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.search);
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		global.editor = global.preferences.edit();

		try {
			uid = global.preferences.getString("uid", "");
			Log.e("search session id", "--" + uid);
		} catch (Exception e) {
			// TODO: handle exception
		}

		c = new Function();

		records = new ArrayList<Model>();
		spinner_lookingfor = (Spinner) findViewById(R.id.looking_for_spinner);
		spinner_ethnicity = (Spinner) findViewById(R.id.ethnicity_spinner);
		spinner_smoker = (Spinner) findViewById(R.id.smoker_spinner);

		spinner_minage = (Spinner) findViewById(R.id.min_range_spinner1);
		spinner_maxage = (Spinner) findViewById(R.id.max_range_spinner2);
		spinner_video = (Spinner) findViewById(R.id.video_spinner);

		spinner_gender = (Spinner) findViewById(R.id.gender_spinner);
		spinner_miles = (Spinner) findViewById(R.id.miles_spinner);
		spinner_lastonline = (Spinner) findViewById(R.id.last_online_spinner);

		spinner_preference = (Spinner) findViewById(R.id.preference_spinner);

		ArrayAdapter myAdapter = (ArrayAdapter) spinner_minage.getAdapter();
		min_pos = myAdapter.getPosition(min);
		spinner_minage.setSelection(min_pos);
		Log.e("pos i", Integer.toString(min_pos));

		ArrayAdapter myAdapter1 = (ArrayAdapter) spinner_maxage.getAdapter();
		max_pos = myAdapter1.getPosition(max);
		spinner_maxage.setSelection(max_pos);
		Log.e("pos h", Integer.toString(max_pos));

		ArrayAdapter myAdapter2 = (ArrayAdapter) spinner_video.getAdapter();
		video_pos = myAdapter2.getPosition(video);
		spinner_video.setSelection(video_pos);
		Log.e("pos b", Integer.toString(video_pos));

		ArrayAdapter myAdapter3 = (ArrayAdapter) spinner_lookingfor
				.getAdapter();
		lookingfor_pos = myAdapter3.getPosition(lookingfor);
		spinner_lookingfor.setSelection(lookingfor_pos);
		Log.e("pos l", Integer.toString(lookingfor_pos));

		ArrayAdapter myAdapter4 = (ArrayAdapter) spinner_ethnicity.getAdapter();
		ethnicity_pos = myAdapter4.getPosition(ethnicity);
		spinner_ethnicity.setSelection(ethnicity_pos);
		Log.e("pos e", Integer.toString(ethnicity_pos));

		ArrayAdapter myAdapter5 = (ArrayAdapter) spinner_smoker.getAdapter();
		smoker_pos = myAdapter5.getPosition(smoker);
		spinner_smoker.setSelection(smoker_pos);

		ArrayAdapter myAdapter6 = (ArrayAdapter) spinner_gender.getAdapter();
		gender_pos = myAdapter6.getPosition(gender);
		spinner_gender.setSelection(gender_pos);

		ArrayAdapter myAdapter7 = (ArrayAdapter) spinner_lastonline
				.getAdapter();
		lastonline_pos = myAdapter7.getPosition(lastonline);
		spinner_lastonline.setSelection(lastonline_pos);
		Log.e("pos e", Integer.toString(lastonline_pos));

		ArrayAdapter myAdapter8 = (ArrayAdapter) spinner_preference
				.getAdapter();
		preference_pos = myAdapter8.getPosition(preference);
		spinner_preference.setSelection(preference_pos);

		ArrayAdapter myAdapter9 = (ArrayAdapter) spinner_miles.getAdapter();
		miles_pos = myAdapter9.getPosition(miles);
		spinner_miles.setSelection(miles_pos);
		try {
			new VideoSetting(Search.this);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.search:

			min = spinner_minage.getSelectedItem().toString();
			min_pos = spinner_minage.getSelectedItemPosition();

			max = spinner_maxage.getSelectedItem().toString();
			max_pos = spinner_maxage.getSelectedItemPosition();

			video = spinner_video.getSelectedItem().toString();
			video_pos = spinner_video.getSelectedItemPosition();

			lookingfor = spinner_lookingfor.getSelectedItem().toString();
			lookingfor_pos = spinner_lookingfor.getSelectedItemPosition();

			ethnicity = spinner_ethnicity.getSelectedItem().toString();
			ethnicity_pos = spinner_ethnicity.getSelectedItemPosition();

			smoker = spinner_smoker.getSelectedItem().toString();
			smoker_pos = spinner_smoker.getSelectedItemPosition();

			gender = spinner_gender.getSelectedItem().toString();
			gender_pos = spinner_gender.getSelectedItemPosition();

			preference = spinner_preference.getSelectedItem().toString();
			preference_pos = spinner_preference.getSelectedItemPosition();

			miles = spinner_miles.getSelectedItem().toString();
			miles_pos = spinner_miles.getSelectedItemPosition();

			lastonline = spinner_lastonline.getSelectedItem().toString();
			lastonline_pos = spinner_lastonline.getSelectedItemPosition();

			if (c.haveNetworkConnection(getApplicationContext())) {
				new SearchTask().execute();
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						Search.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}

			break;
		case R.id.cancel:
			finish();
			break;
		default:
			break;
		}
	}

	public class SearchTask extends AsyncTask<Void, Void, Void> {
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			pd = new ProgressDialog(Search.this);
			pd.setMessage("Searching please wait..");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("m", "search"));
			param.add(new BasicNameValuePair("sessionId", uid));
			if (!lookingfor.equals("Choose")) {
				param.add(new BasicNameValuePair("looking_for[]", lookingfor));
			} else {
				param.add(new BasicNameValuePair("looking_for[]", ""));

			}
			if (!miles.equals("Choose")) {
				param.add(new BasicNameValuePair("distance", miles));
			} else {
				param.add(new BasicNameValuePair("distance", ""));
			}
			if (!min.equals("Choose")) {
				param.add(new BasicNameValuePair("min_age", min));
			} else {
				param.add(new BasicNameValuePair("min_age", ""));
			}
			if (!max.equals("Choose")) {
				param.add(new BasicNameValuePair("max_age", max));
			} else {
				param.add(new BasicNameValuePair("max_age", ""));
			}
			if (!video.equals("Choose")) {
				param.add(new BasicNameValuePair("video_count", video));
			} else {
				param.add(new BasicNameValuePair("video_count", ""));
			}
			if (!gender.equals("Choose")) {
				param.add(new BasicNameValuePair("gender[]", gender));
			} else {
				param.add(new BasicNameValuePair("gender[]", ""));

			}
			if (!preference.equals("Choose")) {
				param.add(new BasicNameValuePair("sexual_preference[]",
						preference));
			} else {
				param.add(new BasicNameValuePair("sexual_preference[]", ""));
			}
			if (!smoker.equals("Choose")) {
				param.add(new BasicNameValuePair("smoker[]", smoker));
			} else {
				param.add(new BasicNameValuePair("smoker[]", ""));
			}

			if (!ethnicity.equals("Choose")) {
				param.add(new BasicNameValuePair("ethnicity[]", ethnicity));
			} else {
				param.add(new BasicNameValuePair("ethnicity[]", ""));
			}
			Log.e("param", "--" + param);
			JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);

			Log.e("response of search screen", "-" + json.toString());

			try {

				String stater = json.getString("state");
				if (stater.equals("success")) {
					global.is_searched = true;
					JSONArray user = json.getJSONArray("users");
					for (int i = 0; i < user.length(); i++) {
						JSONObject index = user.getJSONObject(i);
						String uid = index.getString("uid");

						String uname = index.getString("uname");
						Log.e("uid", uid + "-" + uname);
						String date_added = index.getString("date_added");
						// Log.e("date added ", date_added);

						JSONObject setting = index.getJSONObject("settings");
						String gender = setting.getString("gender");
						String dob = setting.getString("dob");
						String ethnicity = setting.getString("ethnicity");
						String city = setting.getString("city");
						String state = setting.getString("state");
						String country = setting.getString("country");

						// Log.e("country", country);
						String photo = setting.getString("photo");
						// Log.e("search photo", "-->" + photo);

						String video1 = setting.getString("video1");
						String video2 = setting.getString("video2");
						String video3 = setting.getString("video3");
						String video4 = setting.getString("video4");

						String merge = setting.getString("video_merge");
						String video_count = setting.getString("video_count");
						// Log.e("video count", video_count);

						
							String description = null;
						String chat = setting.getString("chat");
						String video_chat = setting.getString("video_chat");
						String chatid = setting.getString("chatid");
						String identity = setting.getString("identity");
						String height = setting.getString("height");
						String body_type = setting.getString("body_type");
						String sexual_preference = setting
								.getString("sexual_preference");
						String looking_for = setting.getString("looking_for");
						String smoker = setting.getString("smoker");
						String about_me = setting.getString("about_me");
						
						

						
						
						Calendar c1 = Calendar.getInstance();
						System.out.println("Current time => " + c1.getTime());
						Log.e("Current time => ", "" + c1.getTime());

						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

						String formattedTodayDate = df.format(c1.getTime());

						Date today = df.parse(formattedTodayDate);
						Date bdate = df.parse(dob);
						long diff = ((today.getTime() - bdate.getTime()) / (24 * 60 * 60 * 1000)) / 365;
						Log.e("age", "-->" + diff / 365);

						if (diff > 0) {
							description = Long.toString(diff);
						} else {
							description = "Sorry Incorect Value";
						}

						if (height.equals("null") || height.equals("Choose")
								|| height.equals("")) {
							height = "";
						} else {
							description += "/ " + height;
						}
						if (identity.equals("null")
								|| identity.equals("Choose")
								|| identity.equals("")) {
							identity = "";
						} else {
							description += "/ " + identity;
						}

						if (body_type.equals("null")
								|| body_type.equals("Choose")
								|| body_type.equals("")) {
							body_type = "";
						} else {
							description += "/ " + body_type;
						}

						if (sexual_preference.equals("null")
								|| sexual_preference.equals("Choose")
								|| sexual_preference.equals("")) {
							sexual_preference = "";
						} else {
							description += "/ " + sexual_preference;
						}

						if (looking_for.equals("null")
								|| looking_for.equals("Choose")
								|| looking_for.equals("")) {
							looking_for = "";
						}
						Log.e("description", "--" + description);

						Model model = new Model();
						String link = c.dolink.trim();
						model.setUid(uid);
						model.setUname(uname);
						model.setDateadded(date_added);
						model.setDob(dob);
						model.setGender(gender);
						model.setEthnicity(ethnicity);
						model.setCity(city);
						model.setUserphoto(link + photo);
						model.setData(description);
						if (!merge.equalsIgnoreCase("null")) {
							model.setVideomerge(link + merge);
						} else {
							model.setVideomerge(merge);
						}
						records.add(model);


						

						global.is_searched = true;
						Log.e("searched size", records.size() + "");
						global.editor.commit();

					}
					global.setList(records);
				} else if (stater.equals("fail")) {
					Toast.makeText(getApplicationContext(), "Not logged in",
							Toast.LENGTH_LONG).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			super.onPostExecute(result);
			Intent intent = new Intent(Search.this, SampleActivity.class);
			startActivity(intent);

		}

	}
}
