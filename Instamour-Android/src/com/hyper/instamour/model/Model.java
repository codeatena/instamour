package com.hyper.instamour.model;

public class Model {
	private String uname, aboutme, bodytype, uid, gender, dob, height,
			ethnicity, lookingfor, city, video1, video2, video3, video4,
			lphoto, rphoto, userphoto, videomerge, dateadded, identity, jid;
	private String data, message;
	String chat_name, chat_jabberid, chat_image;
	String chat_date;
	String redirectjid, redirectname;

	public String getChat_date() {
		return chat_date;
	}

	public String getRedirectjid() {
		return redirectjid;
	}

	public void setRedirectjid(String redirectjid) {
		this.redirectjid = redirectjid;
	}

	public String getRedirectname() {
		return redirectname;
	}

	public void setRedirectname(String redirectname) {
		this.redirectname = redirectname;
	}

	public void setChat_date(String chat_date) {
		this.chat_date = chat_date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getChat_name() {
		return chat_name;
	}

	public void setChat_name(String chat_name) {
		this.chat_name = chat_name;
	}

	public String getChat_jabberid() {
		return chat_jabberid;
	}

	public void setChat_jabberid(String chat_jabberid) {
		this.chat_jabberid = chat_jabberid;
	}

	public String getChat_image() {
		return chat_image;
	}

	public void setChat_image(String chat_image) {
		this.chat_image = chat_image;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getUserphoto() {
		return userphoto;
	}

	public void setUserphoto(String userphoto) {
		this.userphoto = userphoto;
	}

	public String getLphoto() {
		return lphoto;
	}

	public void setLphoto(String lphoto) {
		this.lphoto = lphoto;
	}

	public String getRphoto() {
		return rphoto;
	}

	public void setRphoto(String rphoto) {
		this.rphoto = rphoto;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getDateadded() {
		return dateadded;
	}

	public void setDateadded(String dateadded) {
		this.dateadded = dateadded;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getAboutme() {
		return aboutme;
	}

	public void setAboutme(String aboutme) {
		this.aboutme = aboutme;
	}

	public String getBodytype() {
		return bodytype;
	}

	public void setBodytype(String bodytype) {
		this.bodytype = bodytype;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getLookingfor() {
		return lookingfor;
	}

	public void setLookingfor(String lookingfor) {
		this.lookingfor = lookingfor;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getVideo1() {
		return video1;
	}

	public void setVideo1(String video1) {
		this.video1 = video1;
	}

	public String getVideo2() {
		return video2;
	}

	public void setVideo2(String video2) {
		this.video2 = video2;
	}

	public String getVideo3() {
		return video3;
	}

	public void setVideo3(String video3) {
		this.video3 = video3;
	}

	public String getVideo4() {
		return video4;
	}

	public void setVideo4(String video4) {
		this.video4 = video4;
	}

	public String getVideomerge() {
		return videomerge;
	}

	public void setVideomerge(String videomerge) {
		this.videomerge = videomerge;
	}

}
