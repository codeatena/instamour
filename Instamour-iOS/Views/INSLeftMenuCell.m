//
//  LeftMenuCell.m
//  instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSLeftMenuCell.h"

@interface INSLeftMenuCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *countLabel;

@end

@implementation INSLeftMenuCell

- (void)awakeFromNib
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setSelectedBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SelectionBackground"]]];
 
    [[self titleLabel] setTextColor:[UIColor normalTableCellTextColorINS]];
    [[self titleLabel] setHighlightedTextColor:[UIColor highlightedTableCellTextColorINS]];

    UILabel *countLabel = [self countLabel];
    [countLabel setBackgroundColor:[UIColor instamourPrimaryBackgroundRedColorINS]];
    [countLabel setTextColor:[UIColor whiteColor]];
    
    CALayer *countLabelLayer = [countLabel layer];
    [countLabelLayer setCornerRadius:2.0];
    [countLabelLayer setMasksToBounds:YES];

    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewCell Overrides

- (void)setSelected:(BOOL)selected
           animated:(BOOL)animated
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super setSelected:selected animated:animated];
    
    [[self countLabel] setBackgroundColor:[UIColor instamourPrimaryBackgroundRedColorINS]];

    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setCount:(NSInteger)count;
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    UILabel *countLabel = [self countLabel];
    
    [countLabel setHidden:(count == 0)];
    
    [countLabel setText:[NSString stringWithFormat:@"%ld", (long)count]];

    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
