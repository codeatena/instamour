//
//  INSSignUpProfileViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIManagerTableViewController.h"

// Public Constants

// Protocols

@interface INSSignUpProfileViewController : BTIManagerTableViewController

// Public Properties

// Public Methods

- (instancetype)initWithEmail:(NSString *)email
                     userName:(NSString *)userName
                     password:(NSString *)password
                   facebookID:(NSString *)facebookID
                  dateOfBirth:(NSDate *)date
                       gender:(NSString *)gender
                         city:(NSString *)city
                        state:(NSString *)state
                      country:(NSString *)country;

@end
