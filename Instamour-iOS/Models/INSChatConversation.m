//
//  INSChatConversation.m
//  Instamour
//
//  Created by Brian Slick on 8/7/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatConversation.h"

// Private Constants
NSString *const INSChatConversationAmourUserIDKey = @"INSChatConversationAmourUserIDKey";

NSString *const INSChatConversationChatMessagesKey = @"INSChatConversationChatMessagesKey";
NSString *const INSChatConversationNumberOfUnreadMessagesKey = @"INSChatConversationNumberOfUnreadMessagesKey";

@interface INSChatConversation ()

// Private Properties
@property (nonatomic, strong) NSMutableArray *chatMessages;
@property (nonatomic, assign) NSInteger numberOfUnreadMessages;

@end

@implementation INSChatConversation

#pragma mark - Dealloc and Memory Management


#pragma mark - Custom Getters and Setters

- (INSUser *)amourUser
{
    if (_amourUser == nil)
    {
        _amourUser = [[INSUserManager sharedManager] amourWithUserID:[self amourUserID]];
    }
    return _amourUser;
}

- (NSMutableArray *)chatMessages
{
    if (_chatMessages == nil)
    {
        _chatMessages = [[NSMutableArray alloc] init];
    }
    return _chatMessages;
}

#pragma mark - Initialization


#pragma mark - NSCoding Methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self amourUserID] forKey:INSChatConversationAmourUserIDKey];
    
    [aCoder encodeObject:[self chatMessages] forKey:INSChatConversationChatMessagesKey];
    [aCoder encodeInteger:[self numberOfUnreadMessages] forKey:INSChatConversationNumberOfUnreadMessagesKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self)
    {
        [self setAmourUserID:[aDecoder decodeObjectForKey:INSChatConversationAmourUserIDKey]];

        [self setChatMessages:[aDecoder decodeObjectForKey:INSChatConversationChatMessagesKey]];
        [self setNumberOfUnreadMessages:[aDecoder decodeIntegerForKey:INSChatConversationNumberOfUnreadMessagesKey]];
    }
    return self;
}

#pragma mark - Misc Methods

- (INSChatMessage *)mostRecentChatMessage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

//    NSLog(@"chat messages: %@", [self chatMessages]);
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return [[self chatMessages] lastObject];
}

- (void)addChatMessage:(INSChatMessage *)chatMessage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (chatMessage == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No chat message", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self chatMessages] addObject:chatMessage];
    
    [chatMessage setChatConversation:self];
    
    [self sortChatMessages];

    [self updateUnreadCount];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)markAllMessagesAsRead
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    for (INSChatMessage *message in [self chatMessages])
    {
        [message setUnread:NO];
    }

    [self setNumberOfUnreadMessages:0];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sortChatMessages
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateSent" ascending:YES];
    
    [[self chatMessages] sortUsingDescriptors:@[ sortDescriptor ]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)updateUnreadCount
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSInteger unreadMessageCount = 0;
    
    for (INSChatMessage *chatMessage in [self chatMessages])
    {
        if ([chatMessage isUnread])
        {
            unreadMessageCount++;
        }
    }
    
    [self setNumberOfUnreadMessages:unreadMessageCount];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}


@end
