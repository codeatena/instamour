//
//  INSProfileViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSProfileViewController.h"

// Models and other global
#import <MediaPlayer/MediaPlayer.h>

// Sub-controllers
#import "INSCommentsViewController.h"
#import "INSSocialMediaViewController.h"

// Views
#import "INSProfileDetailsView.h"

// Private Constants


@interface INSProfileViewController () <INSProfileDetailsViewDelegate, UITextFieldDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UIImageView *cameraIconImageView;
@property (nonatomic, strong) IBOutlet UIButton *playButton;

@property (nonatomic, strong) IBOutlet UIView *buttonBarView;
@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property (nonatomic, strong) IBOutlet UIButton *heartButton;
@property (nonatomic, strong) IBOutlet UIButton *crossButton;
@property (nonatomic, strong) IBOutlet UIButton *videoButton;
@property (nonatomic, strong) IBOutlet UIButton *flagButton;

@property (nonatomic, strong) IBOutlet UILabel *onlineStatusLabel;

@property (nonatomic, strong) INSProfileDetailsView *profileDetailsView;

@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;
@property (nonatomic, strong) UITextField *activeTextField;

@property (nonatomic, strong) UIImage *downloadedAvatarImage;

@property (nonatomic, assign, getter = isFullUserDataDownloaded) BOOL fullUserDataDownloaded;

@property (nonatomic, assign, getter = isWatchedVideoNotificationSent) BOOL watchedVideoNotificationSent;

@end

@implementation INSProfileViewController


#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters

- (INSProfileDetailsView *)profileDetailsView
{
    if (_profileDetailsView == nil)
    {
        _profileDetailsView = [INSProfileDetailsView profileDetailsViewWithDelegate:self];
    }
    return _profileDetailsView;
}

#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:[[self user] userName]];

    UILabel *label = [self onlineStatusLabel];
    [label setText:@"offline"];
    [label sizeToFit];
    
    UIBarButtonItem *onlineStatusButton = [[UIBarButtonItem alloc] initWithCustomView:label];
    [[self navigationItem] setRightBarButtonItem:onlineStatusButton];
    
    // User Interface
    
    [[self cameraIconImageView] setImage:[UIImage playButtonImageINS]];
    
    [self arrangeSubviews];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
    
    // Analytics
    
    [Flurry logEvent:@"Profile View"];
    
    // User Interface
    
    [[self scrollView] setContentOffset:CGPointMake(0.0, 0.0)];
    
    [self populateContents];
    
    if (![self isFullUserDataDownloaded])
    {
        [[INSServerAPIManager sharedManager] getUserWithUserName:nil
                                                        orUserID:[[self user] userID]];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidAppear:animated];
    
    [[self scrollView] flashScrollIndicators];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillDisappear:animated];
    
    [self cancelMoviePlayerController:[self moviePlayerController]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *user = [self user];
    
    // Navigation Header View
        
    [[self onlineStatusLabel] setText:([user isOnline]) ? @"online" : @"offline"];
    
    // Image / Video
    
    // Only download the image once.
    
    UIImage *avatarImage = [user avatarImage];
    if (avatarImage == nil)
    {
        avatarImage = [UIImage placeholderAvatarImageINS];
    }
    [[self avatarImageView] setImage:avatarImage];
    
    if ([self moviePlayerController] == nil)
    {
        NSString *videoFileName = [user mergeVideoFileName];
        if ([videoFileName isNotEmpty])
        {
            [self playVideoForFileName:videoFileName];
        }
    }
    
    // Button Bar
    
    [[self userNameLabel] setText:[user userName]];
    
    // Details
    
    INSProfileDetailsView *detailsView = [self profileDetailsView];
    
    NSMutableArray *components = [NSMutableArray array];
    
    [components safelyAddPopulatedStringINS:[user height]];
    [components safelyAddPopulatedStringINS:[user identity]];
    [components safelyAddPopulatedStringINS:[user bodyType]];
    [components safelyAddPopulatedStringINS:[user lookingFor]];
    [components safelyAddPopulatedStringINS:[user city]];
    
    NSString *profileDescription = [components componentsJoinedByString:@" \\ "];
    
    NSArray *kisses = [user kisses];
    NSArray *comments = [user comments];
    
    [detailsView setProfileDescription:profileDescription];
    [detailsView setNumberOfKisses:[kisses count]];
    [detailsView setComments:comments];
    
    NSString *instagramName = [user instagramName];
    NSString *vineName = [user vineName];
    
    [detailsView setMoreButtonHidden:( ![instagramName isNotEmpty] && ![vineName isNotEmpty] )];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    // Lifetime notification for get-user in case user browses comments before results come back
    
    [self addLifetimeNotificationInfoForName:INSServerAPIManagerGetUserDidFinishNotification
                                    selector:@selector(getUserDidSucceedNotification:)
                                      object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFriendStatusAcceptDidFinishNotification
                                   selector:@selector(friendStatusAcceptDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFriendStatusAcceptFailedNotification
                                   selector:@selector(friendStatusAcceptFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFriendStatusPendingDidFinishNotification
                                   selector:@selector(friendStatusPendingDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFriendStatusPendingFailedNotification
                                   selector:@selector(friendStatusPendingFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerKissAddDidFinishNotification
                                   selector:@selector(kissAddDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerKissAddFailedNotification
                                   selector:@selector(kissAddFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerBlockUserDidFinishNotification
                                   selector:@selector(blockUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerBlockUserFailedNotification
                                   selector:@selector(blockUserFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFlagUserDidFinishNotification
                                   selector:@selector(flagUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFlagUserFailedNotification
                                   selector:@selector(flagUserFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)getUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Get User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *user = [self user];
    
    NSDictionary *incomingUser = [jsonDictionary currentUserINS];
    NSString *incomingUserID = [incomingUser identifierForUserINS];
    
    if (![incomingUserID isEqualToString:[user userID]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Wrong user", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setFullUserDataDownloaded:YES];
    
    [user loadWithJSONUser:incomingUser];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)friendStatusAcceptDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Accept Amour Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Amour Accept Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self showMushyPrompt];
    
    [[INSUserManager sharedManager] movePendingAmourUserToAmours:[self user]];
    [[INSPushNotificationManager sharedManager] sendNewAmourMessageToUser:[self user]];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)friendStatusAcceptFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self heartButton] setEnabled:YES];
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Accept Amour Error"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)friendStatusPendingDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Pending Amour Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Amour Pending Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self showMushyPrompt];
    
    [[INSPushNotificationManager sharedManager] sendHeartPushedMessageToUser:[self user]];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)friendStatusPendingFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self heartButton] setEnabled:YES];
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Pending Amour Error"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)kissAddDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Add Kiss Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Kiss Add Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *user = [self user];
    NSArray *kisses = [user kisses];
    
    [[self profileDetailsView] setNumberOfKisses:[kisses count] + 1];
    
    [[INSPushNotificationManager sharedManager] sendSomeoneKissedMeMessageToUser:user];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Kiss"
                                                    message:@"Kiss added successfully!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)kissAddFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Add Kiss Failed!"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)blockUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"User Block Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Block User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    __weak INSProfileViewController *weakSelf = self;
    
    RIButtonItem *popButton = [RIButtonItem item];
    [popButton setLabel:@"Ok"];
    [popButton setAction:^{
        
        [[INSUserManager sharedManager] removeUserFromAllLists:[weakSelf user]];
        
        UINavigationController *navigationController = [weakSelf navigationController];
        if ([[navigationController viewControllers] containsObject:weakSelf])
        {
            [[weakSelf navigationController] popViewControllerAnimated:YES];
        }
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"User Blocked"
                                                    message:@"Block added successfully!"
                                           cancelButtonItem:popButton
                                           otherButtonItems:nil];
    
    [alert show];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)blockUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Block User Failed!"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)flagUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"User Flag Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Flag User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    __weak INSProfileViewController *weakSelf = self;
    
    RIButtonItem *popButton = [RIButtonItem item];
    [popButton setLabel:@"Ok"];
    [popButton setAction:^{
        
        [[INSUserManager sharedManager] removeUserFromAllLists:[weakSelf user]];
        
        UINavigationController *navigationController = [weakSelf navigationController];
        if ([[navigationController viewControllers] containsObject:weakSelf])
        {
            [[weakSelf navigationController] popViewControllerAnimated:YES];
        }
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"User Flagged"
                                                    message:@"Flag added successfully!"
                                           cancelButtonItem:popButton
                                           otherButtonItems:nil];
    
    [alert show];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)flagUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Flag User Failed!"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moviePlayBackDidFinish:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    [[player view] removeFromSuperview];
    
    [self setMoviePlayerController:nil];
    
    [[self avatarImageView] setHidden:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moviePlaybackStateDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *player = [notification object];
    
    switch ([player playbackState])
    {
        case MPMoviePlaybackStatePlaying:
        {
            [[self cameraIconImageView] setImage:[UIImage pauseButtonImageINS]];
        }
            break;
        default:
        {
            [[self cameraIconImageView] setImage:[UIImage playButtonImageINS]];
        }
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (IBAction)heartButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    if (![userManager isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [button setEnabled:NO];
    
    INSUser *user = [self user];
    NSString *userID = [user userID];
    
    BOOL isPendingAmourUser = [userManager isPendingAmourUser:user];
    
    if (isPendingAmourUser)
    {
        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
        
        [[INSServerAPIManager sharedManager] setFriendStatusAcceptForUserID:userID];
    }
    else
    {
        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
        
        [[INSServerAPIManager sharedManager] setFriendStatusPendingForUserID:userID];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)crossButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    if (![userManager isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *user = [self user];
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    
    RIButtonItem *proceedButton = [RIButtonItem item];
    [proceedButton setLabel:@"Ok"];
    [proceedButton setAction:^{
        
        [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
        
        [[INSServerAPIManager sharedManager] blockUserForUserID:[user userID]];
        
    }];
    
    BOOL isAmourUser = [userManager isAmourUser:user];
    
    NSString *message = @"You will not be able to see this member anymore, and they will not be able to see you either.";
    if (isAmourUser)
    {
        message = @"Are you sure you want to delete and block this Amour?";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"X"
                                                    message:message
                                           cancelButtonItem:cancelButton
                                           otherButtonItems:proceedButton, nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)videoButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video"
                                                    message:@"Video messaging will be here soon, so sit tight - thanks!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)flagButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    __weak INSProfileViewController *weakSelf = self;
    
    RIButtonItem *confirmationCancelButton = [RIButtonItem item];
    [confirmationCancelButton setLabel:@"NO"];
    
    RIButtonItem *confirmationProceedButton = [RIButtonItem item];
    [confirmationProceedButton setLabel:@"YES"];
    [confirmationProceedButton setAction:^{
        
        RIButtonItem *reasonCancelButton = [RIButtonItem item];
        [reasonCancelButton setLabel:@"Cancel"];
        
        RIButtonItem *reasonFlagButton = [RIButtonItem item];
        [reasonFlagButton setLabel:@"Flag"];
        [reasonFlagButton setAction:^{
            
            [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
            
            INSUser *user = [weakSelf user];
            NSString *userID = [user userID];
            NSString *reason = [[weakSelf activeTextField] text];
            
            [[INSServerAPIManager sharedManager] flagUserWithIdentifier:userID
                                                                 reason:reason];
            
            [weakSelf setActiveTextField:nil];
            
        }];
        
        RIButtonItem *reasonFlagAndBlockButton = [RIButtonItem item];
        [reasonFlagAndBlockButton setLabel:@"Flag and Block"];
        [reasonFlagAndBlockButton setAction:^{
            
            [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
            
            INSUser *user = [weakSelf user];
            NSString *userID = [user userID];
            NSString *reason = [[weakSelf activeTextField] text];
            
            [[INSServerAPIManager sharedManager] flagUserWithIdentifier:userID
                                                                 reason:reason];
            
            [[INSServerAPIManager sharedManager] blockUserForUserID:userID];
            
            [weakSelf setActiveTextField:nil];
            
        }];
        
        UIAlertView *reasonAlert = [[UIAlertView alloc] init];
        
        [reasonAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [reasonAlert setTitle:@"Please write a short reason why you are flagging this person"];
        [reasonAlert setMessage:nil];
        [reasonAlert addButtonItem:reasonCancelButton];
        [reasonAlert addButtonItem:reasonFlagButton];
        
        UITextField *textField = [reasonAlert textFieldAtIndex:0];
        [weakSelf setActiveTextField:textField];
        [textField setKeyboardType:UIKeyboardTypeDefault];
        [textField setDelegate:weakSelf];
        
        [reasonAlert show];
        
    }];
    
    UIAlertView *confirmationAlert = [[UIAlertView alloc] initWithTitle:@"Flag alert"
                                                                message:@"Are you sure you want to Flag this user?"
                                                       cancelButtonItem:confirmationCancelButton
                                                       otherButtonItems:confirmationProceedButton, nil];
    [confirmationAlert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)playButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *moviePlayer = [self moviePlayerController];
    
    if (moviePlayer != nil)
    {
        if ([moviePlayer playbackState] == MPMoviePlaybackStatePlaying)
        {
            [moviePlayer pause];
        }
        else
        {
            [moviePlayer play];
        }
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Movie already loaded", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *movieFileName = [[self user] mergeVideoFileName];
    
    if ([movieFileName isNotEmpty])
    {
        [self playVideoForFileName:movieFileName];
    }
    else
    {
        if (![self isFullUserDataDownloaded])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Data hasn't downloaded yet", self, __PRETTY_FUNCTION__);
            return;
        }
        
        __weak INSProfileViewController *weakSelf = self;
        
        RIButtonItem *cancelButton = [RIButtonItem item];
        [cancelButton setLabel:@"Cancel"];
        
        RIButtonItem *commentButton = [RIButtonItem item];
        [commentButton setLabel:@"Comment"];
        [commentButton setAction:^{
            
            INSCommentsViewController *nextViewController = [[INSCommentsViewController alloc] init];
            [nextViewController setUser:[weakSelf user]];
            
            [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
            
        }];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Profile Video"
                                                        message:@"This member doesn't have a video yet, want to leave them a comment?"
                                               cancelButtonItem:cancelButton
                                               otherButtonItems:commentButton, nil];
        [alert show];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)arrangeSubviews
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // NOTE: The avatar image view is supposed to be square!
    
    UIScrollView *scrollView = [self scrollView];
    UIImageView *avatarImageView = [self avatarImageView];
    INSProfileDetailsView *detailsView = [self profileDetailsView];
    
    CGRect detailsViewFrame = [detailsView frame];
    
    detailsViewFrame.origin.y = CGRectGetMaxY([avatarImageView frame]);
    [detailsView setFrame:detailsViewFrame];
    [scrollView addSubview:detailsView];
    
    [scrollView setContentSize:CGSizeMake(CGRectGetWidth([avatarImageView frame]), CGRectGetMaxY(detailsViewFrame))];
    
    // Current user: (shouldn't happen) No buttons
    // Amour: X, Video, Flag
    // Anybody else: Heart, X, Video, Flag
    // If heart has already been pushed, disable heart button
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    INSUser *user = [self user];
    
    BOOL isCurrentUser = [userManager isCurrentUser:user];
    BOOL isAmourUser = [userManager isAmourUser:user];
    BOOL isHeartPushedUser = [userManager isHeartPushedUser:user];
    
    [[self flagButton] removeFromSuperview];
    [[self videoButton] removeFromSuperview];
    [[self crossButton] removeFromSuperview];
    [[self heartButton] removeFromSuperview];
    
    NSMutableArray *buttons = [NSMutableArray array];
    CGFloat x = CGRectGetWidth([[self buttonBarView] bounds]) - 15.0;
    
    if (!isCurrentUser)
    {
        [buttons addObject:[self flagButton]];
        [buttons addObject:[self videoButton]];
        [buttons addObject:[self crossButton]];
        
        if (!isAmourUser)
        {
            [buttons addObject:[self heartButton]];
            [[self heartButton] setEnabled:!isHeartPushedUser];
        }
    }
    
    for (UIButton *button in buttons)
    {
        CGRect frame = [button frame];
        CGFloat width = CGRectGetWidth(frame);
        
        x = x - width;
        frame.origin.x = x;
        [button setFrame:frame];
        [[self buttonBarView] addSubview:button];
        
        x = x - 15.0;
    }
    
    CGRect labelFrame = [[self userNameLabel] frame];
    labelFrame.origin.x = 10.0;
    labelFrame.size.width = x - labelFrame.origin.x;
    [[self userNameLabel] setFrame:labelFrame];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showMushyPrompt
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Love is in the air!"
                                                    message:[NSString heartsPushedSitTightMessageForRecipientGenderINS:[[self user] gender]]
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)cancelMoviePlayerController:(MPMoviePlayerController *)moviePlayerController
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (moviePlayerController == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No movie player", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [moviePlayerController stop];
    [[moviePlayerController view] removeFromSuperview];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayerController];
    [notificationCenter removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:moviePlayerController];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)playVideoForFileName:(NSString *)fileName
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [self cancelMoviePlayerController:[self moviePlayerController]];
    [self setMoviePlayerController:nil];
    
    [[self cameraIconImageView] setHidden:YES];
    
    NSString *urlString = [[userDefaults serverLowQualityVideosFilePathINS] stringByAppendingString:fileName];
    
    if (![urlString isNotEmpty])
    {
        NSLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid URL", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![self isWatchedVideoNotificationSent])
    {
        [self setWatchedVideoNotificationSent:YES];
        
        [[INSPushNotificationManager sharedManager] sendMyVideoWatchedMessageToUser:[self user]];
    }
    
    [[self cameraIconImageView] setHidden:NO];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    MPMoviePlayerController *moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    [self setMoviePlayerController:moviePlayer];
    
    [notificationCenter addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    [notificationCenter addObserver:self selector:@selector(moviePlaybackStateDidChangeNotification:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    
    [moviePlayer setMovieSourceType:MPMovieSourceTypeFile];
    [moviePlayer setControlStyle:MPMovieControlStyleNone];
    [moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    [moviePlayer setShouldAutoplay:YES];
    
    UIView *movieView = [moviePlayer view];
    UIImageView *avatarImageView = [self avatarImageView];
    
    [movieView setFrame:[avatarImageView frame]];
    [movieView setAutoresizingMask:[avatarImageView autoresizingMask]];
    
    UIScrollView *scrollView = [self scrollView];
    
    [scrollView insertSubview:movieView aboveSubview:avatarImageView];
    [scrollView bringSubviewToFront:[self playButton]];
    [scrollView bringSubviewToFront:[self cameraIconImageView]];
    [avatarImageView setHidden:YES];
    
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSProfileDetailsViewDelegate Methods

- (void)profileDetailsViewKissButtonPressed:(INSProfileDetailsView *)profileDetailsView;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
    
    [[INSServerAPIManager sharedManager] kissAddForUserID:[[self user] userID]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)profileDetailsViewCommentButtonPressed:(INSProfileDetailsView *)profileDetailsView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSCommentsViewController *nextViewController = [[INSCommentsViewController alloc] init];
    [nextViewController setUser:[self user]];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)profileDetailsViewAllCommentsButtonPressed:(INSProfileDetailsView *)profileDetailsView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSCommentsViewController *nextViewController = [[INSCommentsViewController alloc] init];
    [nextViewController setUser:[self user]];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)profileDetailsViewMoreButtonPressed:(INSProfileDetailsView *)profileDetailsView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self moviePlayerController] pause];
    
    __weak INSProfileViewController *weakSelf = self;
    INSUser *user = [weakSelf user];
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    
    RIButtonItem *instagramButton = [RIButtonItem item];
    [instagramButton setLabel:@"View Instagram Profile"];
    [instagramButton setAction:^{
        
        INSSocialMediaViewController *nextViewController = [[INSSocialMediaViewController alloc] init];
        [nextViewController setUser:user];
        [nextViewController setPlatform:INSSocialMediaPlatformInstagram];
        
        [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
        
    }];
    
    
    RIButtonItem *vineButton = [RIButtonItem item];
    [vineButton setLabel:@"View Vine Profile"];
    [vineButton setAction:^{
        
        INSSocialMediaViewController *nextViewController = [[INSSocialMediaViewController alloc] init];
        [nextViewController setUser:user];
        [nextViewController setPlatform:INSSocialMediaPlatformVine];
        
        [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
        
    }];
    
    NSMutableArray *buttons = [NSMutableArray array];
    
    if ([[user instagramName] isNotEmpty])
    {
        [buttons addObject:instagramButton];
    }
    
    if ([[user vineName] isNotEmpty])
    {
        [buttons addObject:vineButton];
    }
    
    [buttons addObject:cancelButton];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                               cancelButtonItem:nil
                                          destructiveButtonItem:nil
                                               otherButtonItems:nil];
    
    for (RIButtonItem *button in buttons)
    {
        [sheet addButtonItem:button];
    }
    
    [sheet showInView:[self view]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger newTextLength = [textField.text length] - range.length + [string length];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return (newTextLength <= 140);
}

@end
