package com.hyper.instamour;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hyper.instamour.messages.C2DMReceiver;
import com.hyper.instamour.model.ChatHistoryData;
import com.hyper.instamour.model.DataBaseHelper;
import com.hyper.instamour.model.ImageLoader;
import com.hyper.instamour.model.Model;
import com.hyper.instamour.model.Notify;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

public class InstantChat extends Activity {
	private MenuSlideView scrollView;
	private ListView instant_list;
	private int layoutToSlide;
	public static boolean listclicked = false;
	YourAmours amours;
	private View anapp;
	Model model;
	ArrayList<Model> local;

	private ImageLoader imageLoader;
	private SlideMenu sm;
	private DataBaseHelper databasehelper;
	private List<NameValuePair> param;
	private Global global;
	Intent data;
	int id;
	String cname;
	private static ArrayList<Notify> arr_notify;
	private ArrayList<ChatHistoryData> arr_chatHistory;
	String uid;
	String message;
	private Function c;
	Adapter adapter;
	String date;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			// userGet();
			new VideoSetting(InstantChat.this);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.e("on create called ", "on create called");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		LayoutInflater inflater = LayoutInflater.from(this);
		scrollView = (MenuSlideView) inflater.inflate(
				R.layout.screen_scroll_with_list_menu, null);
		setContentView(scrollView);
		layoutToSlide = R.layout.instant_chat;
		local = new ArrayList<Model>();
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		model = new Model();
		global.editor = global.preferences.edit();
		uid = global.preferences.getString("uid", "");
		sm = new SlideMenu(getApplicationContext(), scrollView, inflater,
				layoutToSlide);
		instant_list = (ListView) findViewById(R.id.instant_list);

		imageLoader = new ImageLoader(InstantChat.this);
		c = new Function();
		VideoSetting setting = new VideoSetting(InstantChat.this);

		// userGet();

		databasehelper = new DataBaseHelper(getApplicationContext());
		// arr_chatHistory = new ArrayList<ChatHistoryData>();

		try {
			/*
			 * ArrayList<ChatHistoryData> all = new
			 * ArrayList<ChatHistoryData>(); all =
			 * databasehelper.getAllChatHistory();
			 * Log.e("All chat history size is ", "--" + all.size());
			 */
			arr_notify = databasehelper.getnotificationlist();
			Log.e("private chathistory", arr_notify.size() + "-");
			/*
			 * arr_notify = databasehelper.getnotificationlist();
			 * Log.e("notification list1", arr_notify.size() + "-");
			 */
			/*
			 * arr_chatHistory = databasehelper.getChatHistory(
			 * Integer.parseInt(global.getJabberid()),
			 * Integer.parseInt(global.getFriendjabberid()));
			 * Log.e("freind jabber id", "--" + global.getFriendjabberid());
			 * Log.e("size of histort local data", arr_chatHistory.size() +
			 * "--");
			 * 
			 * if (global.chat_image == false) { message =
			 * arr_chatHistory.get(arr_chatHistory.size() - 1)
			 * .getChatContents(); date =
			 * arr_chatHistory.get(arr_chatHistory.size() - 1) .getChatDate() +
			 * " at " + arr_chatHistory.get(arr_chatHistory.size() - 1)
			 * .getChatTime(); Log.e("chat date", date); } else { message =
			 * "Image"; }
			 */

			/*
			 * try { if (arr_notify.size() > 0) {
			 * 
			 * adapter = new Adapter(InstantChat.this, arr_notify, message);
			 * instant_list.setAdapter(adapter); adapter.notifyDataSetChanged();
			 * } else {
			 * 
			 * }
			 * 
			 * } catch (Exception e) { // TODO: handle exception
			 * Log.e("on create errror ", "on Create errrrr");
			 * e.printStackTrace(); }
			 */

			try {
				if (arr_notify.size() > 0) {
					adapter = new Adapter(InstantChat.this, arr_notify, message);
					instant_list.setAdapter(adapter);
					adapter.notifyDataSetChanged();

				}
			} catch (Exception e) {
				// TODO: handle exception
			}

			instant_list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {

					// TODO Auto-generated method stub
					Intent intent = new Intent(InstantChat.this,
							ChatWindow.class);
					intent.putExtra("jabberid", arr_notify.get(arg2).getJabberid());
					// C2DMReceiver.cntMsg = 0;
					databasehelper.ResetFriendCount(0,arr_notify.get(arg2).getJabberid());
					intent.putExtra("currentuser", arr_notify.get(arg2)
							.getChatname());

					// global.editor.putString("friendphoto",
					// global.getPhoto());
					startActivity(intent);

				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("on create errror 2 ", "on Create errrrr 2");
		}

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		arr_notify = new ArrayList<Notify>();
		arr_notify = databasehelper.getnotificationlist();

		try {
			if (arr_notify.size() > 0) {
				adapter = new Adapter(InstantChat.this, arr_notify, message);
				instant_list.setAdapter(adapter);
				adapter.notifyDataSetChanged();

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	class Adapter extends BaseAdapter {
		private ArrayList<Notify> mRecords;
		private Activity mActivity;
		private LayoutInflater inflater;

		public Adapter(Activity activity, ArrayList<Notify> data, String message) {
			// TODO Auto-generated constructor stub
			// mRecords = records;
			mActivity = activity;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			imageLoader = new ImageLoader(InstantChat.this);
			mRecords = data;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mRecords.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mRecords.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = convertView;

			if (view == null)
				view = inflater.inflate(R.layout.raw_instant, null);
			try {
				final ImageView userPhoto = (ImageView) view
						.findViewById(R.id.userPhoto);
				final TextView last_message = (TextView) view
						.findViewById(R.id.last_message);
				final TextView name = (TextView) view.findViewById(R.id.name);
				final TextView dateTxt = (TextView) view
						.findViewById(R.id.date);
				final TextView txtCount = (TextView) view
						.findViewById(R.id.txt_count);
				// last_message.setText(mRecords.get(position).getMessage());
				last_message.setText(mRecords.get(position).getMessage());
				name.setText(mRecords.get(position).getChatname());
				dateTxt.setText(mRecords.get(position).getChatdatetime());
				String chatimage = mRecords.get(position).getImg();
				Log.e("chat image", mRecords.get(position).getImg());

				imageLoader.DisplayImage(new Function().dolink + chatimage,

				userPhoto);
				
				int chatCnt = databasehelper.getChatCount(mRecords
						.get(position).getJabberid());
				
				if(chatCnt > 0 )
				{
					txtCount.setText(""
							+ databasehelper.getChatCount(mRecords
									.get(position).getJabberid()));
				}
				
				
				/*if (!listclicked) {
					
				} else {
					textView.setText("");
				}*/

				// name.setText(global.getFriendname());`
				// name.setText(opponent);
				id = Integer.parseInt(getIntent().getStringExtra("jid"));/*
																		 * Integer
																		 * .
																		 * parseInt
																		 * (
																		 * mRecords
																		 * .get(
																		 * position
																		 * ).
																		 * getChat_jabberid
																		 * ());
																		 */
				cname = mRecords.get(position).getChatname();

				Model model = new Model();
				model.setRedirectjid(id + "");
				model.setRedirectname(cname);
				local.add(model);
				global.setRedirectToChat(local);
				// Log.e("fdf2", "--" + mRecords.get(position).getImg());

				/*
				 * Log.e("FFFFFFFFFFFFFFFFFFFFFFFFFFFF", chatimage + "==" +
				 * mRecords.get(position).getChat_jabberid() + "==" +
				 * mRecords.get(position).getChat_name());
				 * global.setFriendjabberid(mRecords.get(position)
				 * .getChat_jabberid());
				 */
				try {
					userPhoto.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							// C2DMReceiver.cntMsg = 0;
							/*
							 * databasehelper.updateFriendCount(0,
							 * Integer.parseInt(global.getJabberid()));
							 */

						}
					});
				} catch (Exception e) {
					// TODO: handle exception
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			return view;
		}
	}

	public class UserGetTask extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			/*
			 * pd = new ProgressDialog(SampleActivity.this);
			 * pd.setMessage("loading..."); pd.setIndeterminate(true);
			 * pd.setCancelable(false); pd.show();
			 */
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			userGet();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}

	}

	@SuppressLint("NewApi")
	private void userGet() {

		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		// TODO Auto-generated method stub
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "user-get"));
		param.add(new BasicNameValuePair("sessionId", uid));
		JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
		Log.e("user get response", "--" + json.toString());

		try {
			String link = c.dolink.trim();
			JSONObject user = json.getJSONObject("user");
			String uid = user.getString("uid");
			String uname = user.getString("uname");
			Log.e("user ", "" + uname);
			String email = user.getString("email");
			String fbid = user.getString("fbid");
			String date_added = user.getString("date_added");
			String disable_account = user.getString("disable_account");
			// String qbpass = user.getString("qbpass");

			JSONObject setting = user.getJSONObject("settings");
			String pid = setting.getString("pid");
			String gender = setting.getString("gender");
			String dob = setting.getString("dob");
			String ethnicity = setting.getString("ethnicity");
			String city = setting.getString("city");
			String state = setting.getString("state");
			String country = setting.getString("country");

			String ltd = setting.getString("ltd");
			String lngd = setting.getString("lngd");
			String photo = setting.getString("photo");
			Log.e("photo ", "-------------" + photo);

			String video1 = setting.getString("video1");
			String video2 = setting.getString("video2");
			String video3 = setting.getString("video3");
			String video4 = setting.getString("video4");

			Model model = new Model();
			if (!video1.equalsIgnoreCase("null")
					|| !video1.equalsIgnoreCase("")) {
				global.editor.putString("video1", link + video1);
			} else {
				global.editor.putString("video1", video1);
			}
			if (!video2.equalsIgnoreCase("null")
					|| !video2.equalsIgnoreCase("")) {
				global.editor.putString("video2", link + video2);
			} else {
				global.editor.putString("video2", video2);
			}

			if (!video3.equalsIgnoreCase("null")
					|| !video3.equalsIgnoreCase("")) {
				global.editor.putString("video3", link + video3);
			} else {
				global.editor.putString("video3", video3);
			}
			if (!video4.equalsIgnoreCase("null")
					|| !video4.equalsIgnoreCase("")) {
				global.editor.putString("video4", link + video4);
			} else {
				global.editor.putString("video4", video4);
			}

			String video_merge = setting.getString("video_merge");
			if (!video_merge.equalsIgnoreCase("null")) {
				global.editor.putString("video_merge", link + video_merge);
			} else {
				global.editor.putString("video_merge", video_merge);
			}

			String video_count = setting.getString("video_count");

			String chat = setting.getString("chat");
			String video_chat = setting.getString("video_chat");
			String chatid = setting.getString("chatid");
			String identity = setting.getString("identity");
			String height = setting.getString("height");
			String body_type = setting.getString("body_type");
			String sexual_preference = setting.getString("sexual_preference");
			String looking_for = setting.getString("looking_for");
			String smoker = setting.getString("smoker");
			String about_me = setting.getString("about_me");
			String uresh = "748232";
			String deep = "748360";

			Log.e("my jabberid", chatid);
			Log.e("my username", uname);
			global.editor.putString("userid", uid);
			global.editor.putString("jabberId", chatid);
			global.editor.putString("uname", uname);
			global.editor.commit();

			global.setLoginusername(uname);
			global.setJabberid(chatid);
			// global.setPassword(qbpass);

			global.setPhoto(photo);

			// QBLogin(uname);

			Log.e("---------------------------------------",
					"-------------------------");
		}

		catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	//the directions for FlurryAnalytics call for this
		//(FLJ, 5/6/14)
		 // The Activity's onStart method
		@Override 
		protected void onStart() { // Set up the Flurry session
			super.onStart();
			FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
			
		}
		@Override
		protected void onStop() {
			super.onStop();
			FlurryAgent.onEndSession(this);
		}
}
