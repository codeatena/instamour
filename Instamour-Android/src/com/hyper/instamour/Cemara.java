package com.hyper.instamour;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Cemara extends Activity implements SurfaceHolder.Callback {

	private Camera prCamera;
	private SurfaceHolder prSurfaceHolder;
	private SurfaceView prSurfaceView;
	private int cMaxRecordDurationInMs = 9000;
	private int rotated_angle = 270;
	private long cMaxFileSizeInBytes = 20971520;// 20 Megabyte
	private ToggleButton recordtogglebutton;
	private TextView timeElapsed1;
	boolean recording = false;
	private CamcorderProfile camcorderProfile;

	private TextView save, cancel;
	Global global;

	private boolean prRecordInProcess;
	private MediaRecorder prMediaRecorder;
	private File prRecordedFile;
	private ImageView record, stop, switch_camera;
	private String cVideoFilePath;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		try {
			prMediaRecorder.reset();
			prMediaRecorder.release();
			prCamera.stopPreview();
			prCamera.release();

		} catch (Exception e) {
			// TODO: handle exception
		}
		global.From_create_video = false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.cemara);
		Runtime.getRuntime().totalMemory();
		Runtime.getRuntime().freeMemory();

		// create file in cache or in external storage
		FileCache();
		global = Global.getInstance();
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		global.editor = global.preferences.edit();
		prRecordInProcess = false;
		/*
		 * cVideoFilePath = android.os.Environment.getExternalStorageDirectory()
		 * + "/VRP/";
		 */
		camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
		// camcorderProfile=CamcorderProfile.get(CamcorderProfile.QUALITY_480P);

		prSurfaceView = (SurfaceView) findViewById(R.id.surfaceView1);
		timeElapsed1 = (TextView) findViewById(R.id.timeElapsedc);
		stop = (ImageView) findViewById(R.id.stop);
		record = (ImageView) findViewById(R.id.recordtogglebutton);
		switch_camera = (ImageView) findViewById(R.id.switch_camera);

		save = (TextView) findViewById(R.id.save);
		cancel = (TextView) findViewById(R.id.cancel);
		prSurfaceHolder = prSurfaceView.getHolder();
		prSurfaceHolder.addCallback(this);
		prSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		prMediaRecorder = new MediaRecorder();
		prCamera = null;

	}

	private File cacheDir;

	private void FileCache() {
		// Find the dir to save cached images
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(
					android.os.Environment.getExternalStorageDirectory(), "VRP");
		else
			cacheDir = this.getCacheDir();

		if (!cacheDir.exists())
			cacheDir.mkdirs();

		File card_folder = new File(cacheDir, "cards");
		if (!card_folder.exists())
			card_folder.mkdirs();
	}

	private OnCheckedChangeListener checkedchangelistener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
			// TODO Auto-generated method stub
			/* pause player */
			if (isChecked) {
				System.out.println("start recording....");
				if (startRecording())

					timer.start();
				else
					Toast.makeText(Cemara.this, "Some error in Camera !!",
							Toast.LENGTH_SHORT).show();
			}
			/* play player */
			else {
				System.out.println("stop recording......");
				stopRecording();
				timer.cancel();

			}
		}
	};

	@SuppressLint("NewApi")
	@Override
	public void surfaceChanged(SurfaceHolder _holder, int _format, int _width,
			int _height) {
		// TODO Auto-generated method stub
		Log.e("Surface changed ", "surface changed");

		prCamera.stopPreview();

		try {
			/*
			 * Camera.Parameters p = prCamera.getParameters();
			 * 
			 * p.setPreviewSize(camcorderProfile.videoFrameWidth,
			 * camcorderProfile.videoFrameHeight);
			 * p.setPreviewFrameRate(camcorderProfile.videoFrameRate);
			 * p.setRotation(180);
			 * 
			 * prCamera.setParameters(p);
			 */

			prCamera.setPreviewDisplay(_holder);
			prCamera.startPreview();
			// previewRunning = true;
		} catch (Exception e) {
			Log.e("", e.getMessage());
			e.printStackTrace();
		}
	}

	@SuppressLint("NewApi")
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub

		Log.e("Surface created", "Surface created");
		/* open front camera */
		CameraInfo cameraInfo = new CameraInfo();
		int cameraCount = Camera.getNumberOfCameras();
		if (cameraCount > 1)
			for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); camIdx++) {
				Camera.getCameraInfo(camIdx, cameraInfo);
				if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
					try {
						prCamera = Camera.open(camIdx);
						holder.setKeepScreenOn(true);
						try {

							if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
								prCamera.setDisplayOrientation(90);
							} else {
								prCamera.setDisplayOrientation(0);
							}
							prCamera.setPreviewDisplay(holder);

							// previewRunning = true;

							Parameters parameters = prCamera.getParameters();
							/*
							 * List<Size> sizes =
							 * parameters.getSupportedPreviewSizes();
							 * parameters.setPreviewSize(sizes.get(0).width,
							 * sizes.get(0).height); // mac dinh solution 0
							 * parameters.set("orientation","portrait");
							 * List<Size> size =
							 * parameters.getSupportedPreviewSizes();
							 * parameters.setPreviewSize(size.get(0).width,
							 * size.get(0).height);
							 */
							prSurfaceView.getLayoutParams().width = parameters
									.getPictureSize().width;
							prSurfaceView.getLayoutParams().height = parameters
									.getPictureSize().height;
							parameters.setPreviewSize(
									camcorderProfile.videoFrameWidth,
									camcorderProfile.videoFrameHeight);
							parameters
									.setPreviewFrameRate(camcorderProfile.videoFrameRate);
							// parameters.setRotation(180);

							// parameters.setRotation(90);
							prCamera.setParameters(parameters);
							prCamera.startPreview();

						} catch (IOException e) {
							Log.e("errrrrrr", e.getMessage());
							e.printStackTrace();
						}

					} catch (RuntimeException e) {
						Log.i("Camera failed to open: ",
								e.getLocalizedMessage());
					}
				}
			}
		else
			prCamera = Camera.open();

		if (prCamera == null) {
			Toast.makeText(this, "Camera is not available!", Toast.LENGTH_SHORT)
					.show();
			finish();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		try {
			Log.e("Surface destroy", "surface destroyyyyy");

			timeElapsed1.setText("--:--");
			recording = false;
		} catch (IllegalStateException e) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@SuppressLint("NewApi")
	private boolean startRecording() {
		/*
		 * CameraInfo cameraInfo = new CameraInfo(); int cameraCount =
		 * Camera.getNumberOfCameras(); if (cameraCount > 1) { for (int camIdx =
		 * 0; camIdx < Camera.getNumberOfCameras(); camIdx++) {
		 * Camera.getCameraInfo(camIdx, cameraInfo); if (cameraInfo.facing ==
		 * Camera.CameraInfo.CAMERA_FACING_FRONT) { try { prCamera =
		 * Camera.open(camIdx); prCamera.setDisplayOrientation(90); } catch
		 * (RuntimeException e) { Log.i("Camera failed to open: ",
		 * e.getLocalizedMessage()); } } } } else {
		 */

		// }

		prCamera.stopPreview();
		try {
			prCamera.unlock();

			prMediaRecorder.setCamera(prCamera);
			prMediaRecorder.setPreviewDisplay(prSurfaceHolder.getSurface());
			prMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			prMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			prMediaRecorder.setProfile(CamcorderProfile
					.get(CamcorderProfile.QUALITY_LOW));

			// Orientation hint is for the direction after the cap

			/*
			 * String deviceMan = android.os.Build.MANUFACTURER;
			 * Toast.makeText(getApplicationContext(), deviceMan,
			 * Toast.LENGTH_SHORT).show(); CamcorderProfile
			 * profile=CamcorderProfile
			 * .get(cameraCount,CamcorderProfile.QUALITY_LOW); int
			 * currentapiVersion = android.os.Build.VERSION.SDK_INT; if
			 * (currentapiVersion >= android.os.Build.VERSION_CODES.FROYO){ //
			 * Do something for froyo and above versions boolean
			 * tellbol=CamcorderProfile
			 * .hasProfile(1,CamcorderProfile.QUALITY_HIGH);
			 * if(deviceMan.equals("samsung")){ profile =
			 * CamcorderProfile.get(1, CamcorderProfile.QUALITY_HIGH);
			 * 
			 * } else{ profile = CamcorderProfile.get(1,
			 * CamcorderProfile.QUALITY_HIGH); }
			 * prMediaRecorder.setProfile(profile); }
			 */
			if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
				prMediaRecorder.setOrientationHint(270);
			} else {
				prMediaRecorder.setOrientationHint(0);
			}

			/*
			 * prMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP
			 * ); prMediaRecorder.setAudioEncoder(AudioEncoder.AMR_NB);
			 * prMediaRecorder.setVideoEncoder(VideoEncoder.H264);
			 * prMediaRecorder.setVideoFrameRate(15);
			 * prMediaRecorder.setVideoSize(240, 320);
			 */

			prMediaRecorder.setMaxDuration(cMaxRecordDurationInMs);
			prMediaRecorder.setMaxFileSize(cMaxFileSizeInBytes);

			File file;
			if (Build.VERSION.SDK_INT < 19) {
				Log.e("Build version", Build.VERSION.SDK_INT + "-");
				File directory = new File(
						Environment.getExternalStorageDirectory()
								+ "/instamour");
				if (!directory.exists()) {
					directory.mkdir();
				}
				// Now create the file in the above directory and write the
				// contents
				// into it
				long seconds = System.currentTimeMillis() / 1000l;
				Date lm = new Date();
				String lasmod = new SimpleDateFormat("yyyyMMdd").format(lm);
				String videoname = lasmod + String.valueOf(seconds) + "_video"
						+ ".mp4";
				file = new File(directory, videoname);
			} else {
				Log.e("Build version", Build.VERSION.SDK_INT + "-");
				File directory = new File("/storage/emulated/0" + "/instamour");
				if (!directory.exists()) {
					directory.mkdir();
				}
				// Now create the file in the above directory and write the
				// contents
				// into it
				long seconds = System.currentTimeMillis() / 1000l;
				Date lm = new Date();
				String lasmod = new SimpleDateFormat("yyyyMMdd").format(lm);
				String videoname = lasmod + String.valueOf(seconds) + "_video"
						+ ".mp4";
				file = new File(directory, videoname);
			}
			VideoRecordDemoActivity.mFileName = file.toString();
			global.From_create_video = true;

			prMediaRecorder.setOutputFile(file.getPath());

			prMediaRecorder.prepare();
			record.setVisibility(View.GONE);
			switch_camera.setEnabled(false);
			stop.setVisibility(View.VISIBLE);

			prMediaRecorder.start();
			prRecordInProcess = true;
			return true;
		} catch (IOException _le) {
			_le.printStackTrace();
			return false;
		}/*
		 * catch (IllegalStateException e) { // TODO: handle exception
		 * e.printStackTrace(); return false;
		 * 
		 * }
		 */catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	private void stopRecording() {
		try {
			prCamera.stopPreview();

			prMediaRecorder.stop();
			prMediaRecorder.reset();
			prMediaRecorder.release();
			timeElapsed1.setText("--:--");

			prRecordInProcess = false;
			prCamera.lock();

			prCamera.release();
			prCamera = null;
		} catch (IllegalStateException e) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	/**
	 * CountDownTimer using for counting elapsed time of media file
	 */
	private CountDownTimer timer = new CountDownTimer(cMaxRecordDurationInMs,
			1000) {

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
			timeElapsed1.setText(countTime(millisUntilFinished));
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			// stopRecording();

			try {
				save.setVisibility(View.VISIBLE);
				prMediaRecorder.stop();
				prMediaRecorder.release();
				prCamera.stopPreview();
				prCamera.release();
				prCamera = null;

				timeElapsed1.setText("--:--");
				cancel.setVisibility(View.GONE);
			} catch (IllegalStateException e) {
				// TODO: handle exception
			} catch (RuntimeException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			}
			// forwardscreen();
		}
	};

	/**
	 * Convert time from milliseconds into minutes and seconds, proper to media
	 * player
	 * 
	 * @param miliseconds
	 *            media content time in milliseconds
	 * @return time in format minutes:seconds
	 */
	private String countTime(long miliseconds) {
		String timeInMinutes = new String();
		long minutes = miliseconds / 60000;
		long seconds = (miliseconds % 60000) / 1000;
		timeInMinutes = minutes + ":"
				+ (seconds < 10 ? "0" + seconds : seconds);
		return timeInMinutes;
	}

	private void forwardscreen() {
		/*
		 * Intent intent = new Intent(Cemara.this, PlayVideo.class);
		 * intent.putExtra("path", prRecordedFile.getAbsolutePath());
		 * startActivity(intent);
		 */
		finish();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.save:

			global.From_create_video = true;
			global.front_camera = true;

			finish();
			break;
		case R.id.cancel:
			try {

				// stopRecording();
				timer.cancel();
			} catch (Exception e) {
				// TODO: handle exception
			}
			global.From_create_video = false;
			finish();
			break;

		case R.id.back:
			try {
				Log.e("back", "back");
				prMediaRecorder.reset();
				prMediaRecorder.release();
				prCamera.release();
				prCamera.stopPreview();
				prCamera = null;

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			global.From_create_video = false;
			break;
		case R.id.recordtogglebutton:
			timer.start();
			startRecording();
			// recordtogglebutton.setEnabled(false);

			break;
		case R.id.switch_camera:
			try {
				// stopRecording();
				prMediaRecorder.reset();
				prMediaRecorder.release();
				prCamera.stopPreview();
				prCamera.release();
				prCamera = null;
				Intent intent = new Intent(Cemara.this,
						VideoRecordDemoActivity.class);
				startActivity(intent);

				finish();
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("errrrr", "rrrrrrrrr");
			}

			break;
		case R.id.stop:
			// stopRecording();
			try {

				prMediaRecorder.stop();
				prMediaRecorder.release();
				prCamera.stopPreview();
				prCamera.release();
				prCamera = null;

				cancel.setVisibility(View.GONE);
			} catch (IllegalStateException e) {
				// TODO: handle exception
			} catch (RuntimeException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			}
			timer.cancel();
			save.setVisibility(View.VISIBLE);
			record.setVisibility(View.VISIBLE);
			stop.setVisibility(View.GONE);
			record.setEnabled(false);

			timeElapsed1.setText("Cancel");
			timeElapsed1.setTextColor(Color.WHITE);
			timeElapsed1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					global.From_create_video = false;
					finish();
				}
			});
			break;

		}

	}

}
