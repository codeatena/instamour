//
//  NSNull+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/7/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNull (INSAdditions)

- (BOOL)isNotEmpty;

@end
