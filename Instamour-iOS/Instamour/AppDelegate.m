//
//  AppDelegate.m
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "AppDelegate.h"

// View Controllers
#import "INSHomeViewController.h"
#import "INSLeftMenuViewController.h"
#import "INSBrowseProfilesViewController.h"
#import "INSAudioVideoCallViewController.h"
#import "INSVideoListViewController.h"

// Views
#import "MBProgressHUD.h"

@interface AppDelegate ()

// Private Properties
@property (nonatomic, strong) MBProgressHUD *progressIndicatorView;

@property (nonatomic, assign, getter = isDownloadingAppSettings) BOOL downloadingAppSettings;

@property (nonatomic, strong) NSDateFormatter *serverDateFormatter;
@property (nonatomic, strong) NSDateFormatter *serverBirthdayDateFormatter;

@property (nonatomic, strong) INSAudioVideoCallViewController *callingViewController;

@end

@implementation AppDelegate

#pragma mark - Custom Getters and Setters

- (MBProgressHUD *)progressIndicatorView
{
    if (_progressIndicatorView == nil)
    {
        UIView *rootView = [[[self window] rootViewController] view];
        
        _progressIndicatorView = [[MBProgressHUD alloc] initWithFrame:[rootView frame]];
        [_progressIndicatorView setCenter:[rootView center]];
        [_progressIndicatorView setMinShowTime:1.0];
        [_progressIndicatorView setLabelText:@"Loading..."];
        [_progressIndicatorView setMode:MBProgressHUDModeIndeterminate];
    }
    return _progressIndicatorView;
}

- (NSDateFormatter *)serverDateFormatter
{
    if (_serverDateFormatter == nil)
    {
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        
        _serverDateFormatter = [[NSDateFormatter alloc] init];
        [_serverDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [_serverDateFormatter setTimeZone:timeZone];
    }
    return _serverDateFormatter;
}

- (NSDateFormatter *)serverBirthdayDateFormatter
{
    if (_serverBirthdayDateFormatter == nil)
    {
        _serverBirthdayDateFormatter = [[NSDateFormatter alloc] init];
        [_serverBirthdayDateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return _serverBirthdayDateFormatter;
}

#pragma mark - UIApplicationDelegate Methods

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // Wake up singletons
    [INSLocationManager sharedManager];
    [INSServerAPIManager sharedManager];
    [INSVideoDataManager sharedManager];
    [[INSUserManager sharedManager] currentUser];
    [INSQuickBloxManager sharedManager];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [self registerNotificationListeners];
    
    [Flurry startSession:@"RRH866VJYC4XZBMJC4X2" withOptions:launchOptions];
    [Flurry setEventLoggingEnabled:YES];
    [Flurry setDebugLogEnabled:NO];
    
    [self applyGlobalDefaultAppearance];
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self setWindow:window];
    
    if ([[INSUserManager sharedManager] isLoggedIn])
    {
        [self showLoggedInHomeScreen];
    }
    else
    {
        [self showLoggedOutScreen];
    }
    
    [window makeKeyAndVisible];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSUserManager sharedManager] saveUser];
    
    // TODO: Save more data?
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[NSUserDefaults standardUserDefaults] setVideoNeededAlertEnabledINS:YES];
    
    [[INSVideoDataManager sharedManager] saveVideoList];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self downloadAppSettings];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    // You can add your app-specific url handling code here if needed
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return wasHandled;
}

- (void)application:(UIApplication *)app
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *StrdeviceToken = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString:@"<"withString:@""]
                                 stringByReplacingOccurrencesOfString:@">" withString:@""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    
    NSLog(@"%@",StrdeviceToken);
    
    // TODO: Slick - this token doesn't appear to be read anywhere. I think that would be necessary for push notifications to work.
	[[NSUserDefaults standardUserDefaults] setValue:StrdeviceToken forKey:@"device_Token"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)application:(UIApplication*)application
didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
	
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([application applicationState] != UIApplicationStateActive)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - App is not active", self, __PRETTY_FUNCTION__);
        return;
    }
    
    //    userInfo: {
    //        ID = 13884;
    //        Name = BriTerIdeas;
    //        TYPE = Comment;
    //        aps =     {
    //            alert = "BriTerIdeas commented on your profile \Ud83d\Udcc4";
    //            sound = default;
    //        };
    //    }
    
    //    NSLog(@"userInfo: %@", userInfo);
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    //    NSLog(@"apsInfo: %@", apsInfo);
    
    NSString *notificationType = [userInfo objectForKey:INSPushNotificationComponentMessageTypeKey];
    NSString *senderID = [userInfo objectForKey:INSPushNotificationComponentSenderIDKey];
    //    NSString *senderUserName = [userInfo objectForKey:INSPushNotificationComponentSenderUserNameKey];
    NSString *notificationText = [apsInfo objectForKey:@"alert"];
    
    //    NSLog(@"notificationType: %@", notificationType);
    //    NSLog(@"senderID: %@", senderID);
    //    NSLog(@"senderUserName: %@", senderUserName);
    //    NSLog(@"notificationText: %@", notificationText);
    
    void(^showAlert)(void) = ^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instamour"
                                                        message:notificationText
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
    };
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    INSUser *user = [userManager currentUser];
    
    if ([notificationType isEqualToString:INSPushNotificationTypeSomeoneKissedMe])
    {
        [userManager refreshCurrentUserDataRightNow:YES];
        
        if ( ([user isPushNotificationsEnabled]) && ([user isSomeoneKissedMeNotificationEnabled]) )
        {
            showAlert();
        }
    }
    else if ([notificationType isEqualToString:INSPushNotificationTypeNewAmour])
    {
        [userManager refreshYourAmoursRightNow:YES];
        
        if ( ([user isPushNotificationsEnabled]) && ([user isNewAmourNotificationEnabled]) )
        {
            showAlert();
        }
    }
    else if ([notificationType isEqualToString:INSPushNotificationTypeHeartPush])
    {
        [userManager refreshPendingAmoursRightNow:YES];
        
        if ( ([user isPushNotificationsEnabled]) && ([user isMyHeartPushedNotificationEnabled]) )
        {
            showAlert();
        }
    }
    else if ([notificationType isEqualToString:INSPushNotificationTypeMyVideoWatched])
    {
        if ( ([user isPushNotificationsEnabled]) && ([user isMyVideoWatchedNotificationEnabled]) )
        {
            showAlert();
        }
    }
    else if ([notificationType isEqualToString:INSPushNotificationTypeNewComment])
    {
        [userManager refreshCurrentUserDataRightNow:YES];
        
        if ( ([user isPushNotificationsEnabled]) && ([user isNewCommentNotificationEnabled]) )
        {
            showAlert();
        }
    }
    else if ([notificationType isEqualToString:INSPushNotificationTypeChatMessage])
    {
        if ( ([user isPushNotificationsEnabled]) && ([user isTextChatNotificationEnabled]) )
        {
            NSString *activeChatID = [[INSChatDataManager sharedManager] activeChatOtherUserID];
            
            if ([activeChatID isEqualToString:senderID])
            {
                BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already chatting with this user", self, __PRETTY_FUNCTION__);
                return;
            }
            
            showAlert();
        }
    }
    else if ([notificationType isEqualToString:INSPushNotificationTypeMissedCall])
    {
        showAlert();
    }
    else if ([notificationType isEqualToString:INSPushNotificationTypeUserBusy])
    {
        showAlert();
    }
    else
    {
        showAlert();
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Primary View Display Methods

- (void)showLoggedOutScreen
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[NSUserDefaults standardUserDefaults] deleteAllValuesINS];
    
    [self setPreviewMode:NO];
    [self setSideMenuContainerViewController:nil];
    
    INSHomeViewController *homeViewController = [[INSHomeViewController alloc] init];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self setNavigationController:navigationController];
    [navigationController applyGlobalStyleINS];
    
    [[self window] setRootViewController:navigationController];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setAppSettingsRefreshDateINS:nil];
    
    [self downloadAppSettings];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showLoggedInHomeScreen
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self downloadAppSettings];
    
    [self setPreviewMode:NO];
    
//    INSLeftMenuViewController *leftMenuViewController = [[INSLeftMenuViewController alloc] init];
    
    INSBrowseProfilesViewController *browseProfilesViewController = [[INSBrowseProfilesViewController alloc] initWithDisplayMode:INSBrowseProfilesDisplayModeBrowse];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:browseProfilesViewController];
    [self setNavigationController:navigationController];
    [navigationController applyGlobalStyleINS];
    
    [self setUpMenuControllerWithCenterViewController:navigationController];
    
//    MFSideMenuContainerViewController *menuContainerViewController = [MFSideMenuContainerViewController containerWithCenterViewController:navigationController
//                                                                                                                   leftMenuViewController:leftMenuViewController
//                                                                                                                  rightMenuViewController:nil];
//    [self setSideMenuContainerViewController:menuContainerViewController];
//    
//    [menuContainerViewController setPanMode:MFSideMenuPanModeNone];
//    
//    [[self window] setRootViewController:menuContainerViewController];
//    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showVideoListScreenAfterSignUp
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self downloadAppSettings];
    
    [self setPreviewMode:NO];
    
    INSVideoListViewController *videoListViewController = [[INSVideoListViewController alloc] init];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:videoListViewController];
    [self setNavigationController:navigationController];
    [navigationController applyGlobalStyleINS];

    [self setUpMenuControllerWithCenterViewController:navigationController];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setUpMenuControllerWithCenterViewController:(UIViewController *)centerViewController
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSLeftMenuViewController *leftMenuViewController = [[INSLeftMenuViewController alloc] init];
    
    MFSideMenuContainerViewController *menuContainerViewController = [MFSideMenuContainerViewController containerWithCenterViewController:centerViewController
                                                                                                                   leftMenuViewController:leftMenuViewController
                                                                                                                  rightMenuViewController:nil];
    
    [self setSideMenuContainerViewController:menuContainerViewController];
    
    [menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    [[self window] setRootViewController:menuContainerViewController];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showPreviewScreen
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self downloadAppSettings];
    
    [self setPreviewMode:YES];
    [self setSideMenuContainerViewController:nil];
    
    INSBrowseProfilesViewController *browseProfilesViewController = [[INSBrowseProfilesViewController alloc] initWithDisplayMode:INSBrowseProfilesDisplayModeBrowse];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:browseProfilesViewController];
    [self setNavigationController:navigationController];
    [navigationController applyGlobalStyleINS];
    
    [[self window] setRootViewController:navigationController];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showViewControllerInCenter:(UIViewController *)viewController
             closeMenuWhenFinished:(BOOL)isMenuClosed
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (viewController == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No view controller", self, __PRETTY_FUNCTION__);
        return;
    }
    
    MFSideMenuContainerViewController *menuContainerViewController = [self sideMenuContainerViewController];
    UINavigationController *navigationController = [menuContainerViewController centerViewController];
    
    [navigationController setViewControllers:@[ viewController ]];
    
    if (isMenuClosed)
    {
        [menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showCallingViewForUser:(INSUser *)user
            quickBloxSessionID:(NSString *)quickBloxSessionID
                      callType:(enum QBVideoChatConferenceType)callType
                  incomingCall:(BOOL)isIncomingCall
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self callingViewController] != nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already displaying calling VC", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    if (callType == QBVideoChatConferenceTypeAudioAndVideo)
    {
        if (![currentUser isVideoChatEnabled])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has video chat disabled", self, __PRETTY_FUNCTION__);
            return;
        }
    }
    else
    {
        if (![currentUser isAudioChatEnabled])
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User has audio chat disabled", self, __PRETTY_FUNCTION__);
            return;
        }
    }
    
    INSAudioVideoCallViewController *nextViewController = [[INSAudioVideoCallViewController alloc] init];
    [self setCallingViewController:nextViewController];
    
    [nextViewController setOtherUser:user];
    [nextViewController setCallType:callType];
    [nextViewController setIncomingSessionID:quickBloxSessionID];
    [nextViewController setIncomingCall:isIncomingCall];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    [navController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    
    [[[self window] rootViewController] presentViewController:navController
                                                     animated:YES
                                                   completion:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)dismissCallingView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setCallingViewController:nil];
    
    [[[self window] rootViewController] dismissViewControllerAnimated:YES
                                                           completion:nil];
    
    [[INSQuickBloxManager sharedManager] cleanupAfterCallEnds];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)applyGlobalDefaultAppearance
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIColor *textColor = [UIColor whiteColor];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    [shadow setShadowColor:[UIColor clearColor]];
    [shadow setShadowOffset:CGSizeMake(0.0, 0.0)];
    
    {{  // Navigation Bars
        
        // Removes shadow from navigation bar.
        [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
        
        UIFont *navigationBarTitleFont = [UIFont boldSystemFontOfSize:18.0];
        
        NSMutableDictionary *navigationBarTitleTextAttributes = [NSMutableDictionary dictionary];
        
        if ([AppDelegate isiOS7OrNewer])
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            
            [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"PlainNavigationBarBackground7"] forBarMetrics:UIBarMetricsDefault];
            [[UINavigationBar appearance] setTintColor:textColor];
            
            [navigationBarTitleTextAttributes setObject:navigationBarTitleFont forKey:NSFontAttributeName];
            [navigationBarTitleTextAttributes setObject:textColor forKey:NSForegroundColorAttributeName];
            [navigationBarTitleTextAttributes setObject:shadow forKey:NSShadowAttributeName];
        }
        else
        {
            [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"PlainNavigationBarBackground"] forBarMetrics:UIBarMetricsDefault];
            
            [navigationBarTitleTextAttributes setObject:navigationBarTitleFont forKey:UITextAttributeFont];
            [navigationBarTitleTextAttributes setObject:textColor forKey:UITextAttributeTextColor];
            [navigationBarTitleTextAttributes setObject:[UIColor clearColor] forKey:UITextAttributeTextShadowColor];
            [navigationBarTitleTextAttributes setObject:[NSValue valueWithCGSize:CGSizeMake(0.0, 0.0)] forKey:UITextAttributeTextShadowOffset];
        }
        
        [[UINavigationBar appearance] setTitleTextAttributes:navigationBarTitleTextAttributes];
    }}
    
    {{  // Toolbars
        [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"ToolbarBackground"] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    }}
    
    {{  // Bar Button Items
        UIFont *barButtonItemFont = [UIFont systemFontOfSize:18.0];
        
        [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        NSMutableDictionary *barButtonItemTitleTextAttributes = [NSMutableDictionary dictionary];
        
        if ([AppDelegate isiOS7OrNewer])
        {
            [barButtonItemTitleTextAttributes setObject:barButtonItemFont forKey:NSFontAttributeName];
            [barButtonItemTitleTextAttributes setObject:textColor forKey:NSForegroundColorAttributeName];
            [barButtonItemTitleTextAttributes setObject:shadow forKey:NSShadowAttributeName];
            
            [[UIBarButtonItem appearance] setTintColor:textColor];
        }
        else
        {
            [barButtonItemTitleTextAttributes setObject:barButtonItemFont forKey:UITextAttributeFont];
            [barButtonItemTitleTextAttributes setObject:textColor forKey:UITextAttributeTextColor];
            [barButtonItemTitleTextAttributes setObject:[UIColor clearColor] forKey:UITextAttributeTextShadowColor];
            [barButtonItemTitleTextAttributes setObject:[NSValue valueWithCGSize:CGSizeMake(0.0, 0.0)] forKey:UITextAttributeTextShadowOffset];
            
            // Back buttons
            UIImage *backButtonImage = [[UIImage imageNamed:@"BackButton7"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)];
            [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        }
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonItemTitleTextAttributes forState:UIControlStateNormal];
    }}
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)registerNotificationListeners
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(appSettingsDidDownloadNotification:)
                               name:INSServerAPIManagerAppSettingsDidFinishNotification
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(appSettingsDownloadFailedNotification:)
                               name:INSServerAPIManagerAppSettingsFailedNotification
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(pingDidSucceedNotification:)
                               name:INSServerAPIManagerPingDidFinishNotification
                             object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

+ (BOOL)isiOS7OrNewer
{
    return (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1);
}

- (UIBarButtonItem *)sideMenuBarButtonItem
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"scroll_transparent"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sideMenuBarButtonPressed:)];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return menuButton;
}

- (void)promptUserToLogIn
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    RIButtonItem *loginButton = [RIButtonItem item];
    [loginButton setLabel:@"OK"];
    [loginButton setAction:^{
        
        [self logoutUser];
        
    }];
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Sign In"
                                                    message:@"To interact with the Amours you need to Sign in or Create an account."
                                           cancelButtonItem:cancelButton
                                           otherButtonItems:loginButton, nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)logoutUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSQuickBloxManager sharedManager] logoutCurrentUser];
    
    [[INSServerAPIManager sharedManager] logOut];
    
    [[INSVideoDataManager sharedManager] deleteAllData];
    
    [[INSUserManager sharedManager] logout];
    
    [self logoutFacebook];
    
    [self deleteAllLocalFiles];
    
    [self showLoggedOutScreen];
    
    [self downloadAppSettings];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)logoutFacebook
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // http://stackoverflow.com/questions/12489619/how-to-log-out-from-facebook-using-facebook-sdk-in-ios
    
    FBSession *session = [FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [FBSession setActiveSession:nil];
    
    NSHTTPCookieStorage *cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie *cookie in facebookCookies)
    {
        [cookies deleteCookie:cookie];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showProgressIndicatorWithMessage:(NSString *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self hideProgressIndicator];
    
    MBProgressHUD *progressIndicator = [self progressIndicatorView];
    if (message == nil)
    {
        message = @"Loading...";
    }
    [progressIndicator setLabelText:message];
    
    [[[[self window] rootViewController] view] addSubview:progressIndicator];
    
    [progressIndicator show:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)hideProgressIndicator
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MBProgressHUD *progressIndicator = _progressIndicatorView;
    
    if (progressIndicator == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not currently visible", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [progressIndicator hide:YES];
    [progressIndicator removeFromSuperview];
    
    [self setProgressIndicatorView:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showFailureAlertWithTitle:(NSString *)title
                            error:(NSError *)error
                        orMessage:(NSString *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *messageToDisplay = nil;
    
    if (error != nil)
    {
        messageToDisplay = [error localizedDescription];
    }
    else
    {
        messageToDisplay = message;
    }
    
    [self hideProgressIndicator];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:messageToDisplay
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadAppSettings
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isDownloadingAppSettings])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already downloading", self, __PRETTY_FUNCTION__);
        return;
    }
    
    // Make sure that all server file paths are available. If not, re-download app settings
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *avatarImages = [userDefaults serverAvatarImagesFilePathINS];
    NSString *videoThumbnails = [userDefaults serverVideoThumbnailImagesFilePathINS];
    NSString *videos = [userDefaults serverVideosFilePathINS];
    NSString *lowQualityVideos = [userDefaults serverLowQualityVideosFilePathINS];
    
    if ( ![avatarImages isNotEmpty] || ![videoThumbnails isNotEmpty] || ![videos isNotEmpty] || ![lowQualityVideos isNotEmpty] )
    {
        [userDefaults setAppSettingsRefreshDateINS:nil];
    }
    
    NSDate *lastDownloadDate = [userDefaults appSettingsRefreshDateINS];
    
    if (lastDownloadDate != nil)
    {
        NSDate *now = [NSDate date];
        NSDate *today;
        [[NSCalendar currentCalendar] rangeOfUnit:NSDayCalendarUnit // beginning of this day
                                        startDate:&today
                                         interval:NULL
                                          forDate:now];
        
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        comp.day = -7;      // lets go 7 days back from today
        NSDate *oneWeekBefore = [[NSCalendar currentCalendar] dateByAddingComponents:comp
                                                                              toDate:today
                                                                             options:0];
        
        if ([lastDownloadDate compare:oneWeekBefore] == NSOrderedDescending)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Downloaded within staleness time frame", self, __PRETTY_FUNCTION__);
            return;
        }
    }
    
    [[INSServerAPIManager sharedManager] appSettings];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteAllLocalFiles
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSArray *directoryURLs = @[ [INSFileUtilities imageDirectoryURL], [INSFileUtilities videoDirectoryURL], [INSFileUtilities chatDataDirectoryURL], [BTIFileUtilities libraryApplicationSupportDirectoryURL], [BTIFileUtilities documentsDirectoryURL] ];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    for (NSURL *directoryURL in directoryURLs)
    {
        NSString *directoryPath = [directoryURL path];
        
        NSArray *fileNames = [fileManager contentsOfDirectoryAtPathBTI:directoryPath];
        for (NSString *fileName in fileNames)
        {
            NSString *path = [directoryPath stringByAppendingPathComponent:fileName];
            
            [fileManager removeItemAtPathBTI:path];
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)appSettingsDidDownloadNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setDownloadingAppSettings:NO];
    
    NSDictionary *jsonDictionary = [notification object];
    
    if (![jsonDictionary isStatusSuccessfulINS])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Server return not successful", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setAppSettingsRefreshDateINS:[NSDate date]];
    
    NSDictionary *result = [jsonDictionary objectForKey:@"result"];
    
    // Constants
    
    NSDictionary *constants = [result objectForKey:@"constants"];
    
    NSString *s3URLString = [constants objectForKey:@"S3URL"];
    NSString *s3ImagePreviews = [constants objectForKey:@"S3IMAGEPREVIEWS"];
    NSString *s3Videos = [constants objectForKey:@"S3VIDEOS"];
    NSString *s3LowQualityVideos = [constants objectForKey:@"S3VIDEOSLOW"];
    NSString *s3VideoPreviews = [constants objectForKey:@"S3VIDEOPREVIEWS"];
    
    if ( [s3URLString isNotEmpty] && [s3ImagePreviews isNotEmpty] )
    {
        NSString *serverPath = [s3URLString stringByAppendingString:s3ImagePreviews];
        [userDefaults setServerAvatarImagesFilePathINS:serverPath];
    }
    else
    {
        // Emergency fallback
        [userDefaults setServerAvatarImagesFilePathINS:INSServerFilePathEmergencyUseOnly];
    }
    
    if ( [s3URLString isNotEmpty] && [s3VideoPreviews isNotEmpty])
    {
        NSString *serverPath = [s3URLString stringByAppendingString:s3VideoPreviews];
        [userDefaults setServerVideoThumbnailFilePathINS:serverPath];
    }
    else
    {
        // Emergency fallback
        [userDefaults setServerVideoThumbnailFilePathINS:INSServerFilePathEmergencyUseOnly];
    }
    
    if ( [s3URLString isNotEmpty] && [s3Videos isNotEmpty] )
    {
        NSString *serverPath = [s3URLString stringByAppendingString:s3Videos];
        [userDefaults setServerVideosFilePathINS:serverPath];
    }
    else
    {
        // Emergency fallback
        [userDefaults setServerVideosFilePathINS:INSServerFilePathEmergencyUseOnly];
    }
    
    if ( [s3URLString isNotEmpty] && [s3LowQualityVideos isNotEmpty] )
    {
        NSString *serverPath = [s3URLString stringByAppendingString:s3LowQualityVideos];
        [userDefaults setServerLowQualityVideosFilePathINS:serverPath];
    }
    else
    {
        // Emergency fallback
        [userDefaults setServerLowQualityVideosFilePathINS:INSServerFilePathEmergencyUseOnly];
    }
    
    // Fields
    
    NSDictionary *fields = [result objectForKey:@"fields"];
    
    NSArray *bodyTypes = [fields objectForKey:INSAPIKeyBodyType];
    NSArray *ethnicities = [fields objectForKey:INSAPIKeyEthnicity];
    NSArray *flags = [fields objectForKey:@"flag"];
    NSArray *genders = [fields objectForKey:INSAPIKeyGender];
    NSArray *identities = [fields objectForKey:INSAPIKeyIdentity];
    NSArray *lookingFors = [fields objectForKey:INSAPIKeyLookingFor];
    NSArray *sexualPreferences = [fields objectForKey:INSAPIKeySexualPreference];
    NSArray *smokers = [fields objectForKey:INSAPIKeySmoker];
    
    [userDefaults setAllBodyTypesINS:bodyTypes];
    [userDefaults setAllEthnicitiesINS:ethnicities];
    [userDefaults setAllFlagsINS:flags];
    [userDefaults setAllGendersINS:genders];
    [userDefaults setAllIdentitiesINS:identities];
    [userDefaults setAllLookingForsINS:lookingFors];
    [userDefaults setAllSexualPreferencesINS:sexualPreferences];
    [userDefaults setAllSmokersINS:smokers];
    
    [userDefaults synchronize];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)appSettingsDownloadFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setDownloadingAppSettings:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pingDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // If the message is "Not logged in." something has gone wrong and the user should be logged out
    
    NSDictionary *jsonDictionary = [notification object];
    
    if (![jsonDictionary isStatusSuccessfulINS])
    {
        NSString *message = [jsonDictionary errorMessageINS];
        
        if ([message isEqualToString:@"Not logged in."])
        {
            [self logoutUser];
        }
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Server says user is not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)sideMenuBarButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isPreviewMode])
    {
        [self promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Preview mode", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self sideMenuContainerViewController] toggleLeftSideMenuCompletion:nil];
    
    [[INSVideoDataManager sharedManager] refreshMergeVideoIfNecessary];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Singleton

+ (AppDelegate *)sharedDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

@end
