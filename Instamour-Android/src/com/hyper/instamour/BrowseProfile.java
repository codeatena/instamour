package com.hyper.instamour;

import com.flurry.android.FlurryAgent;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Window;

public class BrowseProfile extends Activity {
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		if (new Function().haveNetworkConnection(getApplicationContext())) {
			setContentView(R.layout.browse_profiles);
		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(
					BrowseProfile.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

	}
	//the directions for FlurryAnalytics call for this
			//(FLJ, 5/6/14)
			 // The Activity's onStart method
			@Override 
			protected void onStart() { // Set up the Flurry session
				super.onStart();
				FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
				
			}
			@Override
			protected void onStop() {
				super.onStop();
				FlurryAgent.onEndSession(this);
			}
}
