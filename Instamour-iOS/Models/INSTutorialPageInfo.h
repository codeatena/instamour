//
//  INSTutorialPageInfo.h
//  Instamour
//
//  Created by Brian Slick on 7/15/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INSTutorialPageInfo : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *imageName;

@end
