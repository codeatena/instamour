//
//  INSChatConversation.h
//  Instamour
//
//  Created by Brian Slick on 8/7/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Classes and Forward Declarations
#import "BTIObject.h"
@class INSUser;
@class INSChatMessage;

// Public Constants

// Protocols

@interface INSChatConversation : BTIObject <NSCoding>

// Public Properties
@property (nonatomic, strong) INSUser *amourUser;
@property (nonatomic, copy) NSString *amourUserID;

@property (nonatomic, strong, readonly) NSMutableArray *chatMessages;
@property (nonatomic, assign, readonly) NSInteger numberOfUnreadMessages;

// Public Methods
- (INSChatMessage *)mostRecentChatMessage;

- (void)addChatMessage:(INSChatMessage *)chatMessage;

- (void)markAllMessagesAsRead;

@end
