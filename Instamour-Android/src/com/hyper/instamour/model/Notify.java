package com.hyper.instamour.model;

public class Notify {
	private String chatname,message,img,chatdatetime;
	private int cnt,jabberid;
	public int getJabberid() {
		return jabberid;
	}
	public void setJabberid(int jabberid) {
		this.jabberid = jabberid;
	}
	public String getChatname() {
		return chatname;
	}
	public void setChatname(String chatname) {
		this.chatname = chatname;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getChatdatetime() {
		return chatdatetime;
	}
	public void setChatdatetime(String chatdatetime) {
		this.chatdatetime = chatdatetime;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

}
