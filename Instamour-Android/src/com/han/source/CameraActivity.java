package com.han.source;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.han.source.CameraView;
import com.han.utility.CameraUtility;
import com.hyper.instamour.R;

import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CameraActivity extends Activity {
	
//	private Camera mCamera;
    private CameraView mPreview;
    private MediaRecorder mMediaRecorder;
    
    private int currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    
    Button btnRecord;
    Button btnSwitchCamera;
    TextView txtBack;
    TextView txtRecordTimer;
    
    Timer myTimer;
    
    int count;
    
    private boolean isRecording = false;

    String TAG = "CameraActivity";
    
    public static Uri videoFileUri = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_video);

        initWidget();
        initValue();
        initEvent();
    }
    
    private void initWidget() {
        // Create an instance of Camera
//        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        btnRecord = (Button) findViewById(R.id.record_button);
        txtBack = (TextView) findViewById(R.id.back_textView);
        btnSwitchCamera = (Button) findViewById(R.id.switch_camera_button);
        txtRecordTimer = (TextView) findViewById(R.id.record_timer_textView);
        
    	if (CameraUtility.checkCameraHardware(this)) {
//            mCamera = CameraUtility.getCameraInstance();
            mPreview = (CameraView) findViewById(R.id.cameraView);
//            preview.addView(mPreview);
    	}
    }
    
    private void initValue() {
    	txtRecordTimer.setVisibility(View.GONE);
    }
    
    private void initEvent() {
    	btnRecord.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            if (isRecording) {
	                // stop recording and release camera
	                mMediaRecorder.stop();  // stop the recording
	                releaseMediaRecorder(); // release the MediaRecorder object
	                mPreview.camera.lock();         // take camera access back from MediaRecorder
	
	                // inform the user that recording has stopped
	                btnRecord.setBackgroundResource(R.drawable.record_camera_nor);
	                isRecording = false;
	                
	                CameraActivity.this.setResult(RESULT_OK);
	                finish();
	            } else {
	                // initialize video camera
	                if (prepareVideoRecorder()) {
	                    // Camera is available and unlocked, MediaRecorder is prepared,
	                    // now you can start recording
	                    mMediaRecorder.start();
	                    count = 0;
	                    txtRecordTimer.setVisibility(View.VISIBLE);
	                    myTimer = new Timer();
	            		myTimer.schedule(new TimerTask() {			
	            			@Override
	            			public void run() {
	            				CameraActivity.this.runOnUiThread(new Runnable() {
	            					public void run() {
	    	            				count++;
	    	            				int sec = count / 10;
	    	            				int milisec = count % 10;
	    	            				txtRecordTimer.setText(Integer.toString(sec) + "." + Integer.toString(milisec) + " sec");
	            					}
	            				});
	            			}
	            		}, 0, 100);
	                    // inform the user that recording has started
//		                btnRecord.setText("Stop");
		                btnRecord.setBackgroundResource(R.drawable.record_camera_sel);

	                    isRecording = true;
	                } else {
	                    // prepare didn't work, release the camera
	                    releaseMediaRecorder();
	                    // inform user
	                }
	            }
    		}
    	});
    	txtBack.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	finish();
	        }
    	});
    	btnSwitchCamera.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	swithCameraFacing();
	        }
    	});
    }
    
    private void swithCameraFacing() {
    	if (mPreview.camera != null) {
    		mPreview.camera.stopPreview();                  
    		mPreview.camera.release();
    		mPreview.camera = null;
    	}

    	if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
    		currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
    	else 
    		currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;         

    	try {                
    		mPreview.camera = Camera.open(currentCameraId);   

    		mPreview.camera.setDisplayOrientation(90);

    		mPreview.camera.setPreviewDisplay(mPreview.getHolder());             

    		mPreview.camera.startPreview();
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private boolean prepareVideoRecorder(){
    	try {
//        mCamera = CameraUtility.getCameraInstance();
	        mMediaRecorder = new MediaRecorder();
	
	        // Step 1: Unlock and set camera to MediaRecorder
	        mPreview.camera.unlock();
	        mMediaRecorder.setCamera(mPreview.camera);
	
	        // Step 2: Set sources
	        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
	        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
//	        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
	        
	        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
	        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
//	        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
//	        profile.videoFrameWidth = 480;
//	        profile.videoFrameHeight = 480;
//	        mMediaRecorder.setProfile(profile);
	        // Step 4: Set output file
	        
	        File videoFile = CameraUtility.getOutputMediaFile(CameraUtility.MEDIA_TYPE_VIDEO);
	        
	        mMediaRecorder.setOutputFile(videoFile.toString());
	        videoFileUri = Uri.fromFile(videoFile);
	        // Step 5: Set the preview output
	        mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());
//	        mMediaRecorder.setVideoSize(480, 480);
        // Step 6: Prepare configured MediaRecorder
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        mPreview.startCamera();
        mPreview.setCameraDisplayOrientation(this);
        
    	swithCameraFacing();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stopCamera();

        releaseMediaRecorder();
        releaseCamera();
    }

    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
//            mCamera.lock();
        }
    }

    private void releaseCamera(){
        if (mPreview.camera != null){
        	mPreview.camera.release();
        	mPreview.camera = null;
        }
    }
}
