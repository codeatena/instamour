//
//  INSCommentTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/31/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSCommentTableCell.h"

@interface INSCommentTableCell ()

// Private Properties

@end

@implementation INSCommentTableCell

- (void)awakeFromNib
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self userNameLabel] setTextColor:[UIColor commentUserNameColorINS]];
    [[self dateLabel] setTextColor:[UIColor commentTimeColorINS]];
    [[self messageLabel] setTextColor:[UIColor commentMessageColorINS]];
    
    UIImageView *imageView = [self avatarImageView];
    
    [imageView setImage:[UIImage placeholderAvatarImageINS]];
    
    CALayer *imageViewLayer = [imageView layer];
    
    [imageViewLayer setShadowColor:[[UIColor blackColor] CGColor]];
    [imageViewLayer setShadowRadius:10.0];
    [imageViewLayer setShadowOffset:CGSizeMake(0.0, 5.0)];
    [imageViewLayer setShadowOpacity:1.0];
    
    [imageViewLayer setMasksToBounds:YES];
    [imageViewLayer setCornerRadius:CGRectGetHeight([imageView frame]) / 2.0];
    
    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
