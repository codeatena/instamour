//
//  INSPendingAmoursTableCell.h
//  Instamour
//
//  Created by Brian Slick on 7/19/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols

@interface INSPendingAmoursTableCell : BTITableViewCell

// Public Properties

@property (nonatomic, strong, readonly) UIImageView *avatarImageView;

// Public Methods

- (void)setUserName:(NSString *)userName;
- (void)setMessage:(NSString *)message;

@end
