//
//  INSChatTableCell.m
//  Instamour
//
//  Created by Brian Slick on 8/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatTableCell.h"

@implementation INSChatTableCell

- (void)awakeFromNib
{
    [[self timeStampLabel] setTextColor:[UIColor colorWithRed:138.0/255.0 green:137.0/255.0 blue:135.0/255.0 alpha:1.0]];
    [[self timeStampLabel] setFont:[UIFont systemFontOfSize:8.0]];
    
    [[self contentView] setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

@end
