//
//  INSTutorialViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/15/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSTutorialViewController.h"

// Models and other global
#import "INSTutorialPageInfo.h"

// Sub-controllers

// Views

// Private Constants

@interface INSTutorialViewController ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *textLabel;
@property (nonatomic, strong) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;

@property (nonatomic, strong) NSArray *pageInfoArray;
@property (nonatomic, assign) NSInteger currentPageIndex;
@property (nonatomic, assign, getter = isCloseButtonHidden) BOOL closeButtonHidden;

@end

@implementation INSTutorialViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewDidLoad];
    
    [[self closeButton] setHidden:[self isCloseButtonHidden]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides


#pragma mark - Notification Handlers


#pragma mark - UI Response Methods

- (IBAction)leftButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger currentIndex = [self currentPageIndex];
    NSInteger nextIndex = currentIndex - 1;
    if (nextIndex < 0)
    {
        nextIndex = 0;
    }
    
    [self setCurrentPageIndex:nextIndex];
    
    [self refreshViewForCurrentPageNumber];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)rightButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger numberOfPages = [[self pageInfoArray] count];
    
    NSInteger currentIndex = [self currentPageIndex];
    NSInteger nextIndex = currentIndex + 1;
    if (nextIndex >= numberOfPages)
    {
        nextIndex = numberOfPages - 1;
    }
    
    [self setCurrentPageIndex:nextIndex];
    
    [self refreshViewForCurrentPageNumber];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)closeButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self delegate] tutorialViewControllerDidFinish:self];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setCloseButtonVisible:(BOOL)isVisible
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setCloseButtonHidden:!isVisible];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setTutorialPageInfos:(NSArray *)contents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setPageInfoArray:contents];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)beginTutorial
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self view];
    
    [self setCurrentPageIndex:0];

    [self refreshViewForCurrentPageNumber];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)refreshViewForCurrentPageNumber
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UILabel *textLabel = [self textLabel];
    
    NSInteger index = [self currentPageIndex];
    
    INSTutorialPageInfo *pageInfo = [[self pageInfoArray] objectAtIndex:index];
    
    [textLabel setText:[pageInfo text]];
    
    UIImage *image = [UIImage imageNamed:[pageInfo imageName]];
    [[self iconImageView] setImage:image];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}



@end
