//
//  ProfileViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIViewController.h"

// Public Constants

// Protocols


@interface INSCurrentUserProfileViewController : BTIViewController

// Public Properties

// Public Methods

@end
