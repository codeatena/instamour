//
//  INSGiftPurchaseViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/5/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSGiftPurchaseViewController.h"

// Models and other global
#import "INSTutorialPageInfo.h"

// Sub-controllers
#import "INSTutorialViewController.h"

// Views

// Private Constants

@interface INSGiftPurchaseViewController ()

// Private Properties
@property (nonatomic, strong) INSTutorialViewController *tutorialViewController;
@property (nonatomic, strong) NSArray *tutorialPages;

@property (nonatomic, strong) IBOutlet UILabel *comingSoonLabel;

@end

@implementation INSGiftPurchaseViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Gifting View"];
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    
    // User Interface

    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self comingSoonLabel] setText:@"Coming Soon\nVersion 2.0 Update"];
    
    NSMutableArray *pageInfos = [NSMutableArray array];
    
    INSTutorialPageInfo *pageInfo = nil;
    
    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Add Filters to your Videos"];
    [pageInfo setImageName:@"filter"];
    
    [pageInfos addObject:pageInfo];
    
    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Add a border to your profile picture"];
    [pageInfo setImageName:@"borderIcon"];
    
    [pageInfos addObject:pageInfo];

    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Add custom ringtones and sounds"];
    [pageInfo setImageName:@"music"];
    
    [pageInfos addObject:pageInfo];

    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Purchase gifts for potential amours"];
    [pageInfo setImageName:@"gifts-icon"];
    
    [pageInfos addObject:pageInfo];

    pageInfo = [[INSTutorialPageInfo alloc] init];
    [pageInfo setText:@"Buy stickers to send in messages or to put in videos"];
    [pageInfo setImageName:@"sticker"];
    
    [pageInfos addObject:pageInfo];

    INSTutorialViewController *tutorialViewController = [[INSTutorialViewController alloc] init];
    [self setTutorialViewController:tutorialViewController];
    [tutorialViewController setCloseButtonVisible:NO];
    [tutorialViewController setTutorialPageInfos:pageInfos];
    
    [tutorialViewController beginTutorial];
    
    [[tutorialViewController view] setFrame:[[self view] bounds]];
    [[self view] addSubview:[tutorialViewController view]];
    
    [[self view] bringSubviewToFront:[self comingSoonLabel]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers


#pragma mark - UI Response Methods


#pragma mark - Misc Methods

@end
