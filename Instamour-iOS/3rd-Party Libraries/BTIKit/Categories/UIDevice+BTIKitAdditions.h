//
//  UIDevice+BTIKitAdditions.h
//  BTIKit
//  v1.1
//
//  Created by Brian Slick in March 2014
//  Copyright (c) 2014 BriTer Ideas LLC. All rights reserved.
//  https://github.com/BriTerIdeas/BTIKit
//

#import <UIKit/UIKit.h>

@interface UIDevice (BTIKitAdditions)

+ (BOOL)isIpadBTI;
+ (BOOL)isIphoneBTI;
+ (BOOL)isRetinaBTI;

@end
