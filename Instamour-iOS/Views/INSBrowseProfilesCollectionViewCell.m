//
//  BrowseProfilesCollectionViewCell.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSBrowseProfilesCollectionViewCell.h"

@implementation INSBrowseProfilesCollectionViewCell

#pragma mark - Dealloc and Memory Management


#pragma mark - Custom Getters and Setters


#pragma mark - Initialization and UI Creation Methods

- (void)awakeFromNib
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    CALayer *layer = [[self avatarImageView] layer];
    
    [layer setCornerRadius:4.0];
    [layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [layer setBorderWidth:1.0];
    [layer setMasksToBounds:YES];

    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
