//
//  INSAmoursViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIArrayTableViewController.h"

// Public Constants

// Protocols

@interface INSAmoursViewController : BTIArrayTableViewController

// Public Properties

// Public Methods

@end
