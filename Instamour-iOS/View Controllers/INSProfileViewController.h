//
//  INSProfileViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIViewController.h"
@class INSUser;

// Public Constants
//typedef NS_ENUM(NSInteger, INSProfileViewControllerDisplayMode) {
//    INSProfileViewControllerDisplayModeBrowseProfiles = 1,
//    INSProfileViewControllerDisplayModeYourAmours,
//    INSProfileViewControllerDisplayModePendingAmours,
//    INSProfileViewControllerDisplayModeHeartsPushed,
//};

// Protocols

@interface INSProfileViewController : BTIViewController

// Public Properties
@property (nonatomic, strong) INSUser *user;
//@property (nonatomic, assign) INSProfileViewControllerDisplayMode mode;

// Public Methods

@end
