//
//  LeftMenuCell.h
//  instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols

@interface INSLeftMenuCell : BTITableViewCell

// Public Properties
@property (nonatomic, strong) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

// Public Methods
- (void)setCount:(NSInteger)count;

@end
