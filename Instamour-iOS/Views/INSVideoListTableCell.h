//
//  INSVideoListTableCell.h
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols
@protocol INSVideoListTableCellDelegate;


@interface INSVideoListTableCell : BTITableViewCell

// Public Properties
@property (nonatomic, strong, readonly) UIImageView *thumbnailImageView;

@property (nonatomic, weak) id<INSVideoListTableCellDelegate> delegate;
//@property (nonatomic, strong) IBOutlet UILabel *testingLabel;

// Public Methods
- (void)setThumbnailImage:(UIImage *)image;

- (void)setRecordButtonEnabled:(BOOL)isEnabled;
- (void)setUploadButtonEnabled:(BOOL)isEnabled;
- (void)setPlayButtonEnabled:(BOOL)isEnabled;
- (void)setDeleteButtonEnabled:(BOOL)isEnabled;

- (void)setVideoInfoState:(INSVideoInfoState)state;

@end



@protocol INSVideoListTableCellDelegate <NSObject>

@required
- (void)videoListTableCellRecordButtonPressed:(INSVideoListTableCell *)cell;
- (void)videoListTableCellUploadButtonPressed:(INSVideoListTableCell *)cell;
- (void)videoListTableCellPlayButtonPressed:(INSVideoListTableCell *)cell;
- (void)videoListTableCellDeleteButtonPressed:(INSVideoListTableCell *)cell;


@end