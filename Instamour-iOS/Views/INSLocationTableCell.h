//
//  INSLocationTableCell.h
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols

@interface INSLocationTableCell : BTITableViewCell

// Public Properties

// Public Methods
- (void)setLocation:(NSString *)location;

@end
