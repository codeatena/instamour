package com.hyper.instamour;

public class MenuItemList {
	String name;

	int id;
	
	
	public MenuItemList(int id,String name) {
		this.id = id;
		this.name = name;
	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
