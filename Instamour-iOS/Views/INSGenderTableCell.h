//
//  INSGenderTableCell.h
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols
@protocol INSGenderTableCellDelegate;

@interface INSGenderTableCell : BTITableViewCell

// Public Properties
@property (nonatomic, weak) id<INSGenderTableCellDelegate> delegate;

// Public Methods
- (void)setGender:(NSString *)gender;

@end

@protocol INSGenderTableCellDelegate <NSObject>

@required
- (void)genderTableCell:(INSGenderTableCell *)cell didSelectGender:(NSString *)gender;

@end