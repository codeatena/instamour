//
//  INSChatDataManager.h
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import <Foundation/Foundation.h>
@class BTITableContentsManager;
@class QBChatMessage;
@class INSUser;
@class INSChatConversation;

// Public Constants

FOUNDATION_EXPORT NSString *const INSChatDataManagerDidChangeAllConversationsNotification;
FOUNDATION_EXPORT NSString *const INSChatDataManagerDidChangeConversationNotification;      // INSChatConversation is in userInfo

FOUNDATION_EXPORT NSString *const INSChatDataManagerNotificationUserInfoConversationKey;

// Protocols

@interface INSChatDataManager : NSObject

// Public Properties

// Full BTITableSectionInfo/BTITableRowInfo structure with INSChatConversation as row info's represented object
@property (nonatomic, strong, readonly) BTITableContentsManager *sortedChatConversations;

@property (nonatomic, copy) NSString *activeChatOtherUserID;

// Public Methods
+ (instancetype)sharedManager;

- (void)refreshSortedConversations;
- (NSInteger)numberOfUnreadMessages;

- (void)didReceiveQuickBloxChatMessage:(QBChatMessage *)message;
- (void)quickBloxChatMessageDidFailToSend:(QBChatMessage *)message;

- (INSChatConversation *)conversationForUserID:(NSString *)userID;

- (void)saveChatConversation:(INSChatConversation *)chatConversation;

- (void)removeChatConversationForUserID:(NSString *)userID;

- (void)sendChatToUser:(INSUser *)toUser
           textMessage:(NSString *)message;
- (void)sendChatToUser:(INSUser *)toUser
                 image:(UIImage *)image;
- (void)sendChatToUser:(INSUser *)toUser
          videoFileURL:(NSURL *)fileURL;

@end
