//
//  INSGenderTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSGenderTableCell.h"

@interface INSGenderTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UIButton *maleButton;
@property (nonatomic, strong) IBOutlet UIButton *femaleButton;

@property (nonatomic, copy) NSString *selectedGender;

@end

@implementation INSGenderTableCell

#pragma mark - UI Response Methods

- (IBAction)maleButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setGender:INSMaleString];
    
    [[self delegate] genderTableCell:self didSelectGender:INSMaleString];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)femaleButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setGender:INSFemaleString];
    
    [[self delegate] genderTableCell:self didSelectGender:INSFemaleString];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setGender:(NSString *)gender
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([[gender lowercaseString] isEqualToString:[INSMaleString lowercaseString]])
    {
        [self setSelectedGender:INSMaleString];
    }
    else if ([[gender lowercaseString] isEqualToString:[INSFemaleString lowercaseString]])
    {
        [self setSelectedGender:INSFemaleString];
    }
    else
    {
        [self setSelectedGender:nil];
    }
    
    [self refreshButtonStatus];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)refreshButtonStatus
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *gender = [self selectedGender];
    UIButton *maleButton = [self maleButton];
    UIButton *femaleButton = [self femaleButton];
    
    if ([gender isEqualToString:INSMaleString])
    {
        [maleButton setBackgroundImage:[UIImage maleButtonImageCheckedINS:YES] forState:UIControlStateNormal];
        [femaleButton setBackgroundImage:[UIImage femaleButtonImageCheckedINS:NO] forState:UIControlStateNormal];
    }
    else if ([gender isEqualToString:INSFemaleString])
    {
        [maleButton setBackgroundImage:[UIImage maleButtonImageCheckedINS:NO] forState:UIControlStateNormal];
        [femaleButton setBackgroundImage:[UIImage femaleButtonImageCheckedINS:YES] forState:UIControlStateNormal];
    }
    else
    {
        [maleButton setBackgroundImage:[UIImage maleButtonImageCheckedINS:NO] forState:UIControlStateNormal];
        [femaleButton setBackgroundImage:[UIImage femaleButtonImageCheckedINS:NO] forState:UIControlStateNormal];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
