package com.han.source;

import java.util.ArrayList;
import java.util.HashMap;

import com.han.network.VideoAsyncTask;
import com.han.source.PlayVideoActivity;
import com.han.utility.CameraUtility;
import com.hyper.instamour.CreateVideos;
import com.hyper.instamour.R;
import com.hyper.instamour.model.CreateVideoData;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

public class CreateVideoDataAdapter extends ArrayAdapter<CreateVideoData> {
	
	ArrayList<CreateVideoData> arrData;
	int row_id;
	Activity parentActivity;
	
    final int INVALID_ID = -1;

    public static final int THUBNAIL_VIDEO_ID = 100;
    public static final int CAMERA_BUTTON_ID = 200;
    public static final int UPLOAD_BUTTON_ID = 300;
    public static final int PLAY_BUTTON_ID = 400;
    public static final int REMOVE_BUTTON_ID = 500;
    
    public int camera_position;
    public int upload_position;
    public int play_position;
    public int remove_position;
    
	public CreateVideoDataAdapter(Context context, int _row_id, ArrayList<CreateVideoData> _arrData) {
		super(context, _row_id, _arrData);
		// TODO Auto-generated constructor stub
		parentActivity = (Activity) context;
		row_id = _row_id;
		arrData = _arrData;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        LayoutInflater inflater = parentActivity.getLayoutInflater();
        row = inflater.inflate(row_id, parent, false);
        
        SmartImageView imgVideoThumbnail = (SmartImageView) row.findViewById(R.id.video_thumbnail_imageView);
        ImageView imgRecordMark = (ImageView) row.findViewById(R.id.record_mark_imageView);
        ImageView imgCameraButton = (ImageView) row.findViewById(R.id.camera_button_imageView);
        ImageView imgUploadButton = (ImageView) row.findViewById(R.id.upload_button_imageView);
        ImageView imgPlayButton = (ImageView) row.findViewById(R.id.play_button_imageView);
        ImageView imgRemoveButton = (ImageView) row.findViewById(R.id.remove_button_imageView);
        
//        rlytVideoThumbnail.setId(THUBNAIL_IMAGE_ID + position);

        imgVideoThumbnail.setId(THUBNAIL_VIDEO_ID + position);
        imgCameraButton.setId(CAMERA_BUTTON_ID + position);
        imgUploadButton.setId(UPLOAD_BUTTON_ID + position);
        imgPlayButton.setId(PLAY_BUTTON_ID + position);
        imgRemoveButton.setId(REMOVE_BUTTON_ID + position);
        
        CreateVideoData data = arrData.get(position);
        if (data.videoUrl == null) {
        	
        	imgVideoThumbnail.setVisibility(View.GONE);
        	imgRecordMark.setVisibility(View.VISIBLE);
        } else {
        	
        	imgVideoThumbnail.setVisibility(View.VISIBLE);
        	imgRecordMark.setVisibility(View.GONE);
        	imgVideoThumbnail.setImageUrl(data.thumbUrl);

//        	if (data.videoFileUri != null) {
//	        	Bitmap bmThumbnail;
//	            bmThumbnail = ThumbnailUtils.createVideoThumbnail(data.videoFileUri.getPath(), Thumbnails.MICRO_KIND);
//	            imgVideoThumbnail.setImageBitmap(bmThumbnail);
//        	}
        }

        imgCameraButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				int position = v.getId() - CAMERA_BUTTON_ID;
				camera_position = position;
				
				Log.e("camera_position button click", Integer.toString(camera_position));
				
				Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
				
		        CreateVideoData videoData = arrData.get(position);

		        videoData.videoFileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_VIDEO);

			    intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
			    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
//			    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
			    intent.putExtra(MediaStore.EXTRA_OUTPUT, videoData.videoFileUri);
			 
//			    parentActivity.startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
			    
			    parentActivity.startActivityForResult(new Intent(parentActivity, CameraActivity.class), CameraUtility.CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
			}
        });
        
        imgUploadButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				int position = v.getId() - UPLOAD_BUTTON_ID;
				
				Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
				//comma-separated MIME types
				mediaChooser.setType("video/*");
				parentActivity.startActivityForResult(mediaChooser, CreateVideos.REQUEST_CODE_CHOOSE_FILE);
				upload_position = position;
			}
        });
        
        imgPlayButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				int position = v.getId() - PLAY_BUTTON_ID;
		        CreateVideoData videoData = arrData.get(position);
		        if (videoData.videoFileUri != null) {
		        	PlayVideoActivity.videoUri = videoData.videoFileUri;
		        	parentActivity.startActivity(new Intent(parentActivity, PlayVideoActivity.class));
		        	return;
		        }
		        if (videoData.videoUrl != null) {
		        	PlayVideoActivity.videoUri = Uri.parse(videoData.videoUrl);
		        	parentActivity.startActivity(new Intent(parentActivity, PlayVideoActivity.class));
		        	return;
		        }
			}
        });
        
        imgRemoveButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				int position = v.getId() - REMOVE_BUTTON_ID;
				long id = CreateVideoDataAdapter.this.getItemId(position);
				
				new VideoAsyncTask(parentActivity).execute(VideoAsyncTask.ACTION_DELETE_VIDEO, Integer.toString(position + 1));
//				Log.e("test getItemId", Long.toString(id));
			}
        });
        
        return row;
	}
}