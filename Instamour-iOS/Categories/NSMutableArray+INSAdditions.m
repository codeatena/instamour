//
//  NSMutableArray+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/19/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "NSMutableArray+INSAdditions.h"

@implementation NSMutableArray (INSAdditions)

- (void)safelyAddPopulatedStringINS:(NSString *)string
{
    if (![string isKindOfClass:[NSString class]])
    {
        return;
    }
    
    if ([string isNotEmpty])
    {
        [self addObject:string];
    }
}

@end
