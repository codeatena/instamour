package com.hyper.instamour.model;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.xbill.DNS.CNAMERecord;

import com.hyper.instamour.model.ChatData;
import com.hyper.instamour.R;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.format.DateFormat;
import android.util.Log;

/**
 * @author ubuntu class for database opertaion
 */
public class DataBaseHelper extends SQLiteOpenHelper {
	private SQLiteDatabase myDataBase;
	private Context myContext;
	int sender_id, receiver_id;
	private String TAG = this.getClass().getSimpleName();

	// String
	// Notification_table="CREATE  TABLE  IF NOT EXISTS "main"."noti" ("chatname" TEXT, "message" TEXT, "img" TEXT, "chatdatetime" TEXT, "cnt" INTEGER DEFAULT 0, "jabberid" INTEGER)";

	public DataBaseHelper(Context context) {

		super(context, context.getResources().getString(R.string.DB_NAME),
				null, 1);
		this.myContext = context;
		try {
			createDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ---Create the database---
	public void createDataBase() throws IOException {
		// ---Check whether database is already created or not---
		boolean dbExist = checkDataBase();
		if (!dbExist) {
			this.getReadableDatabase();
			try {
				// ---If not created then copy the database---
				copyDataBase();
			} catch (IOException e) {
				throw new Error("Error copying database");
			}
		}
	}

	// --- Check whether database already created or not---
	private boolean checkDataBase() {
		try {
			String myPath = String.format(
					myContext.getString(R.string.DB_PATH),
					myContext.getPackageName())
					+ myContext.getString(R.string.DB_NAME);
			File f = new File(myPath);
			if (f.exists())
				return true;
			else
				return false;
		} catch (SQLiteException e) {
			e.printStackTrace();
			return false;
		}

	}

	// --- Copy the database to the output stream---
	private void copyDataBase() throws IOException {

		InputStream myInput = myContext.getAssets().open(
				myContext.getString(R.string.DB_NAME));

		String outFileName = String.format(
				myContext.getString(R.string.DB_PATH),
				myContext.getPackageName())
				+ myContext.getString(R.string.DB_NAME);

		OutputStream myOutput = new FileOutputStream(outFileName);

		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	public void openDataBase() throws SQLException {

		// --- Open the database---
		String myPath = String.format(myContext.getString(R.string.DB_PATH),
				myContext.getPackageName())
				+ myContext.getString(R.string.DB_NAME);

		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READWRITE);

	}

	@Override
	public synchronized void close() {

		if (myDataBase != null && myDataBase.isOpen())
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	/* insert data */

	public String insert_chatdata(int sender_id, int receiver_id,
			String chatcontent, String localPath, int isDownloaded,
			String chatname) {
		this.sender_id = sender_id;
		this.receiver_id = receiver_id;
		openDataBase();
		myDataBase.beginTransaction();
		/*
		 * Calendar c = Calendar.getInstance(); SimpleDateFormat df = new
		 * SimpleDateFormat("MMM dd,yyyy HH:mm"); String formattedDate =
		 * df.format(c.getTime()); String chatdate =
		 * formattedDate.substring(0,formattedDate.lastIndexOf(" ")).trim();
		 * Log.e("current date and time :  ",formattedDate +"date is : " +
		 * chatdate); String chattime =
		 * formattedDate.substring(formattedDate.lastIndexOf(" "),
		 * formattedDate.length()).trim();
		 * Log.e("current date and time :  ",formattedDate +"date is : " +
		 * chatdate + "time is  : " + chattime); String chatdateTime = chatdate
		 * +" "+ chattime;
		 */

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("MMM dd,yyyy");
		String chatdate = df.format(c.getTime());
		// String chatdate =
		// formattedDate.substring(0,formattedDate.lastIndexOf(" ")).trim();
		Log.e("current date :  ", chatdate);
		String time_format;
		if (DateFormat.is24HourFormat(myContext) == false) {
			time_format = "hh:mm aa";
		} else {
			time_format = "HH:mm";
		}
		SimpleDateFormat timeFormat = new SimpleDateFormat(time_format);
		String chattime = timeFormat.format(c.getTime());

		ContentValues values;
		values = new ContentValues();
		values.put("sender_id", sender_id);
		values.put("receiver_id", receiver_id);
		values.put("chatcontent", chatcontent);
		values.put("chatdate", chatdate);
		values.put("chattime", chattime);
		values.put("localpath ", localPath);
		values.put("isDownloaded", isDownloaded);
		values.put("chatname", chatname);
		// this is for the notificatoin
		/*
		 * values.put("chatname", chatname); values.put("chatjid", chatjid);
		 * values.put("chatimg", chatimg); values.put("cnt", cnt);
		 */
		myDataBase.insert("privatechathistory", null, values);
		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		SQLiteDatabase.releaseMemory();
		return chatdate + "  " + chattime;
	}

	public ArrayList<ChatHistoryData> getChatHistory(int sender_id,
			int receiver_id) {
		openDataBase();
		myDataBase.beginTransaction();
		this.sender_id = sender_id;
		this.receiver_id = receiver_id;
		ArrayList<ChatHistoryData> arr = new ArrayList<ChatHistoryData>();
		Cursor c = null;

		c = myDataBase.rawQuery(
				"SELECT * FROM privatechathistory WHERE (sender_id="
						+ sender_id + " AND receiver_id=" + receiver_id
						+ ") OR (" + "sender_id=" + receiver_id
						+ " AND receiver_id=" + sender_id + ") ORDER BY id",
				null);

		if (c != null) {
			if (c.moveToFirst()) {
				do {
					ChatHistoryData privatechathistory = new ChatHistoryData();
					privatechathistory.setId(c.getInt(0));
					privatechathistory.setSenderId(c.getInt(1));
					privatechathistory.setReceiverId(c.getInt(2));
					privatechathistory.setChatContents(c.getString(3));
					privatechathistory.setChatDate(c.getString(4));
					String convertTime = null;
					SimpleDateFormat displayFormat, parseFormat;
					Date date = null;
					if (c.getString(5).contains("P")
							|| c.getString(5).contains("A")) {
						Log.e("hellooooo", "ur input filed is 12 hour format");
						displayFormat = new SimpleDateFormat("hh:mm a");
					} else {
						Log.e("hellooooo", "ur input filed is 24 hour format");
						displayFormat = new SimpleDateFormat("HH:mm");
					}
					try {
						if (DateFormat.is24HourFormat(myContext) == false) {
							date = displayFormat.parse(c.getString(5).trim());
							parseFormat = new SimpleDateFormat("hh:mm a");
							Log.e("Ur Output format of 12 hour is :",
									"------->" + parseFormat.format(date));
							convertTime = parseFormat.format(date);
						} else {
							date = displayFormat.parse(c.getString(5).trim());
							parseFormat = new SimpleDateFormat("HH:mm");
							Log.e("Ur Output format of 24 hour is :",
									"------->" + parseFormat.format(date));
							convertTime = parseFormat.format(date);
						}

						// convertTime = displayFormat.format(date);
						Log.e("time at time of receiver  : ", "------->"
								+ convertTime);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						Log.e("dsdsd", "sdsdsdsd");
						e.printStackTrace();
					}
					privatechathistory.setChatTime(convertTime);
					privatechathistory.setLocalPath(c.getString(6));
					privatechathistory.setIsdownloaded(c.getInt(7));

					// for notification only
					privatechathistory.setChatname(c.getString(8));
					privatechathistory.setChatjid(c.getInt(9));
					privatechathistory.setChatimg(c.getString(10));
					privatechathistory.setCnt(c.getInt(10));

					arr.add(privatechathistory);
				} while (c.moveToNext());
			}
		}
		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		c.close();
		SQLiteDatabase.releaseMemory();
		return arr;
	}

	public ArrayList<ChatHistoryData> getAllChatHistory() {
		openDataBase();
		myDataBase.beginTransaction();
		ArrayList<ChatHistoryData> arr = new ArrayList<ChatHistoryData>();
		Cursor c = null;
		/*
		 * c = myDataBase.rawQuery(
		 * "SELECT * FROM privatechathistory WHERE (sender_id=" + sender_id +
		 * " AND receiver_id=" + receiver_id + ") OR (" + "sender_id=" +
		 * receiver_id + " AND receiver_id=" + sender_id + ") ORDER BY id",
		 * null);
		 */
		c = myDataBase.rawQuery("SELECT * FROM privatechathistory", null);

		if (c != null) {
			if (c.moveToFirst()) {
				do {
					ChatHistoryData privatechathistory = new ChatHistoryData();
					privatechathistory.setId(c.getInt(0));
					privatechathistory.setSenderId(c.getInt(1));
					privatechathistory.setReceiverId(c.getInt(2));
					privatechathistory.setChatContents(c.getString(3));
					privatechathistory.setChatDate(c.getString(4));
					String convertTime = null;
					SimpleDateFormat displayFormat, parseFormat;
					Date date = null;
					if (c.getString(5).contains("P")
							|| c.getString(5).contains("A")) {
						Log.e("hellooooo", "ur input filed is 12 hour format");
						displayFormat = new SimpleDateFormat("hh:mm a");
					} else {
						Log.e("hellooooo", "ur input filed is 24 hour format");
						displayFormat = new SimpleDateFormat("HH:mm");
					}
					try {
						if (DateFormat.is24HourFormat(myContext) == false) {
							date = displayFormat.parse(c.getString(5).trim());
							parseFormat = new SimpleDateFormat("hh:mm a");
							Log.e("Ur Output format of 12 hour is :",
									"------->" + parseFormat.format(date));
							convertTime = parseFormat.format(date);
						} else {
							date = displayFormat.parse(c.getString(5).trim());
							parseFormat = new SimpleDateFormat("HH:mm");
							Log.e("Ur Output format of 24 hour is :",
									"------->" + parseFormat.format(date));
							convertTime = parseFormat.format(date);
						}

						// convertTime = displayFormat.format(date);
						Log.e("time at time of receiver  : ", "------->"
								+ convertTime);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						Log.e("dsdsd", "sdsdsdsd");
						e.printStackTrace();
					}
					privatechathistory.setChatTime(convertTime);
					privatechathistory.setLocalPath(c.getString(6));
					privatechathistory.setIsdownloaded(c.getInt(7));
					arr.add(privatechathistory);
				} while (c.moveToNext());
			}
		}
		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		c.close();
		SQLiteDatabase.releaseMemory();
		return arr;
	}

	public int updateCounter(ChatHistoryData data) {
		openDataBase();
		myDataBase.beginTransaction();
		ContentValues values = new ContentValues();
		values.put("cnt", 0);
		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		int re = myDataBase
				.update("privatechathistory", values, data.getId() + " = ?",
						new String[] { String.valueOf(data.getReceiverId()) });
		myDataBase.close();
		SQLiteDatabase.releaseMemory();
		return re;

	}

	public void updateFriendCount(int count, int jabberId,String Message) {

		openDataBase();
		myDataBase.beginTransaction();
		try {
			myDataBase.execSQL("update noti set cnt=" + count +",message='" + Message + "' where jabberid=" + jabberId + "");
			Log.e("data updated success fully ", "data updated sucess fully");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Log.e("updation error", "updation error");
		}
		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		SQLiteDatabase.releaseMemory();

	}
	
	
	public void ResetFriendCount(int count, int jabberId) {

		openDataBase();
		myDataBase.beginTransaction();
		try {
			myDataBase.execSQL("update noti set cnt=" + count + " where jabberid=" + jabberId + "");
			Log.e("data updated success fully ", "data updated sucess fully");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Log.e("updation error", "updation error");
		}
		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		SQLiteDatabase.releaseMemory();

	}

	public ArrayList<ChatHistoryData> getAllLastHistory() {
		ArrayList<ChatHistoryData> arr_notify = new ArrayList<ChatHistoryData>();
		openDataBase();
		myDataBase.beginTransaction();

		Cursor c = null;
		c = myDataBase
				.rawQuery(
						"Select max(rowid),chatname,chatcontent,chatdate,chattime,localpath from privatechathistory GROUP BY receiver_id ORDER BY rowid ASC ",
						
						
						null);

		if (c != null) {
			if (c.moveToFirst()) {
				do {
					ChatHistoryData privatechathistory = new ChatHistoryData();
				
					privatechathistory.setReceiverId(c.getInt(0));
					privatechathistory.setChatname(c.getString(1));
					privatechathistory.setChatContents(c.getString(2));
					privatechathistory.setChatDate(c.getString(3));
					privatechathistory.setChatTime(c.getString(4));
					privatechathistory.setChatimg(c.getString(5));
					
					arr_notify.add(privatechathistory);
					/*privatechathistory.setChatDate(c.getString(4));
					String convertTime = null;
					SimpleDateFormat displayFormat, parseFormat;
					Date date = null;
					if (c.getString(5).contains("P")
							|| c.getString(5).contains("A")) {
						Log.e("hellooooo", "ur input filed is 12 hour format");
						displayFormat = new SimpleDateFormat("hh:mm a");
					} else {
						Log.e("hellooooo", "ur input filed is 24 hour format");
						displayFormat = new SimpleDateFormat("HH:mm");
					}
					try {
						if (DateFormat.is24HourFormat(myContext) == false) {
							date = displayFormat.parse(c.getString(5).trim());
							parseFormat = new SimpleDateFormat("hh:mm a");
							Log.e("Ur Output format of 12 hour is :",
									"------->" + parseFormat.format(date));
							convertTime = parseFormat.format(date);
						} else {
							date = displayFormat.parse(c.getString(5).trim());
							parseFormat = new SimpleDateFormat("HH:mm");
							Log.e("Ur Output format of 24 hour is :",
									"------->" + parseFormat.format(date));
							convertTime = parseFormat.format(date);
						}

						// convertTime = displayFormat.format(date);
						Log.e("time at time of receiver  : ", "------->"
								+ convertTime);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						Log.e("dsdsd", "sdsdsdsd");
						e.printStackTrace();
					}
					privatechathistory.setChatTime(convertTime);
					privatechathistory.setLocalPath(c.getString(6));
					privatechathistory.setIsdownloaded(c.getInt(7));

					// for notification only
					privatechathistory.setChatname(c.getString(8));*/
				} while (c.moveToNext());
			}
		}

		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		c.close();
		SQLiteDatabase.releaseMemory();
		return arr_notify;

	}
	
	public int getJabberIdNoti(int JabberId)
	{
		openDataBase();
		myDataBase.beginTransaction();
		Cursor cId = null;
		int lastId = 0;
		cId = myDataBase.rawQuery("SELECT jabberid from noti where jabberid=" + JabberId + "", null);

		if (cId != null) {
			if (cId.moveToFirst()) {
				do {
					Log.e("flag before uploaded count is : ", "" + lastId);
					lastId = cId.getCount();
					Log.e("flag after uploaded count is : ", "" + lastId);

				} while (cId.moveToNext());
			}
		}

		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		SQLiteDatabase.releaseMemory();
		return lastId;
		
	}

	public String insertnoti(String chataname, String message, String img,
			String chatdatetime, int cnt, int jabberid) {
		openDataBase();
		myDataBase.beginTransaction();

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("MMM dd,yyyy");
		String chatdate = df.format(c.getTime());
		// String chatdate =
		// formattedDate.substring(0,formattedDate.lastIndexOf(" ")).trim();
		Log.e("current date :  ", chatdate);
		String time_format;
		if (DateFormat.is24HourFormat(myContext) == false) {
			time_format = "hh:mm aa";
		} else {
			time_format = "HH:mm";
		}
		SimpleDateFormat timeFormat = new SimpleDateFormat(time_format);
		String chattime = timeFormat.format(c.getTime());

		ContentValues values;
		values = new ContentValues();
		values.put("chatname", chataname);
		values.put("message", message);
		values.put("img", img);
		values.put("chatdatetime", chatdate + " at " + chattime);
		values.put("cnt", cnt);
		values.put("jabberid", jabberid);

		// this is for the notificatoin
		/*
		 * values.put("chatname", chatname); values.put("chatjid", chatjid);
		 * values.put("chatimg", chatimg); values.put("cnt", cnt);
		 */

		myDataBase.insert("noti", null, values);

		Cursor c1 = null;
		c1 = myDataBase.rawQuery("SELECT * FROM noti", null);

		if (c1 != null) {
			if (c1.moveToFirst()) {
				do {
					Log.e("msgggggggggg reciveddddd is : ", c1.getString(1));
				} while (c1.moveToNext());
			}
		}

		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		c1.close();
		myDataBase.close();
		SQLiteDatabase.releaseMemory();
		Log.e("transaction success", "--" + "notification inserted to local db");

		return chatdate + "  " + chattime;

	}

	public ArrayList<Notify> getnotificationlist() {
		ArrayList<Notify> arr_notify = new ArrayList<Notify>();
		openDataBase();
		myDataBase.beginTransaction();

		Cursor c = null;
		c = myDataBase.rawQuery("SELECT * FROM noti", null);

		if (c != null) {
			if (c.moveToFirst()) {
				do {
					Notify noti = new Notify();
					noti.setChatname(c.getString(0));
					noti.setMessage(c.getString(1));
					noti.setImg(c.getString(2));
					noti.setChatdatetime(c.getString(3));
					noti.setCnt(c.getInt(4));
					noti.setJabberid(c.getInt(5));
					arr_notify.add(noti);
				} while (c.moveToNext());
			}
		}
		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		c.close();
		SQLiteDatabase.releaseMemory();
		return arr_notify;
	}

	public int getChatCount(int jabberID) {
		openDataBase();
		myDataBase.beginTransaction();
		Cursor cId = null;
		int lastId = 0;
		cId = myDataBase.rawQuery("SELECT cnt from noti where jabberid="
				+ jabberID + "", null);

		if (cId != null) {
			if (cId.moveToFirst()) {
				do {
					Log.e("flag before uploaded count is : ", "" + lastId);
					lastId = cId.getInt(0);
					Log.e("flag after uploaded count is : ", "" + lastId);

				} while (cId.moveToNext());
			}
		}

		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		SQLiteDatabase.releaseMemory();
		return lastId;
	}

	public int getTotalCount() {
		openDataBase();
		myDataBase.beginTransaction();
		Cursor cId = null;
		int lastId = 0;
		cId = myDataBase.rawQuery("SELECT sum(cnt) from noti", null);

		if (cId != null) {
			if (cId.moveToFirst()) {
				do {
					Log.e("flag before uploaded count is : ", "" + lastId);
					lastId = cId.getInt(0);
					Log.e("flag after uploaded count is : ", "" + lastId);

				} while (cId.moveToNext());
			}
		}

		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();
		myDataBase.close();
		SQLiteDatabase.releaseMemory();
		return lastId;
	}

	public int countOfflineRecord(String userid, String loginUname) {
		// TODO Auto-generated method stub
		openDataBase();
		myDataBase.beginTransaction();
		int cc = 0;
		try {
			String ss = "SELECT COUNT(*) FROM privatechathistory" + ""
					+ " where read=0 and user like'" + userid
					+ "'and account like '" + loginUname + "';";
			Cursor c = myDataBase.rawQuery(ss, null);
			if (c != null) {
				c.moveToFirst();
				cc = c.getInt(0);
			}
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(cc);
		return cc;
	}

	/********* Read Total Unread message *******************/
	public int countOfflineMessages(String loginUname) {
		// TODO Auto-generated method stub
		openDataBase();
		myDataBase.beginTransaction();
		int cc = 0;
		try {
			String ss = "SELECT COUNT(*) FROM " + "privatechathistory"
					+ " where read=0 and account like '" + loginUname + "';";
			Cursor c = myDataBase.rawQuery(ss, null);
			if (c != null) {
				c.moveToFirst();
				cc = c.getInt(0);
			}
			c.close();
			Log.e("Count msg", "" + cc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(cc);
		return cc;
	}

}