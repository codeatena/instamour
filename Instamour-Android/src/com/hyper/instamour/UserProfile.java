package com.hyper.instamour;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import com.hyper.instamour.SampleActivity.AsyncActionFlag;
import com.hyper.instamour.SampleActivity.LazyAdapter1;
import com.hyper.instamour.SampleActivity.RemoveTask;
import com.hyper.instamour.model.ImageLoader;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class UserProfile extends Activity {

	private Intent intent;
	private ArrayList<NameValuePair> param;
	String uname, photo, height, identity, lookingfor, city, bodytype, aboutme,
			uid, videomerge;
	private TextView txtuname, txtextra, txtaboutme, txtuname1;
	private ImageView cross, gift, uservideopic, heart, img_flag, videoplaybtn;
	private ImageLoader imageLoader;
	VideoView videoview;
	private Function c;

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.uservideoview);
		intent = getIntent();
		uname = intent.getStringExtra("username");
		photo = intent.getStringExtra("photo");

		/*height = intent.getStringExtra("height");
		identity = intent.getStringExtra("identity");

		lookingfor = intent.getStringExtra("lookingfor");
		city = intent.getStringExtra("city");

		bodytype = intent.getStringExtra("bodytype");
		aboutme = intent.getStringExtra("aboutme");*/String data=intent.getStringExtra("userdata");

		uid = intent.getStringExtra("uid");
		videomerge = intent.getStringExtra("videomerge");

		imageLoader = new ImageLoader(getApplicationContext());
		c = new Function();

		txtuname = (TextView) findViewById(R.id.username);
	//	txtuname1 = (TextView) findViewById(R.id.uservideoview_topbar_heading);
		txtextra = (TextView) findViewById(R.id.txtdesc);
		txtaboutme = (TextView) findViewById(R.id.txtaboutme);

		cross = (ImageView) findViewById(R.id.cross);
		gift = (ImageView) findViewById(R.id.gift);
		heart = (ImageView) findViewById(R.id.heart);
		img_flag = (ImageView) findViewById(R.id.flag);
	//	videoplaybtn = (ImageView) findViewById(R.id.video_play);
	//	videoview = (VideoView) findViewById(R.id.uservideoview);

		uservideopic = (ImageView) findViewById(R.id.uservideopic);
		txtuname.setText(uname);
	//	txtuname1.setText(uname);
		VideoSetting setting = new VideoSetting(UserProfile.this);

		String txtDesc = "";
		try {
			if (!height.equalsIgnoreCase("null")
					&& identity.equalsIgnoreCase("null")
					&& bodytype.equalsIgnoreCase("null")
					&& lookingfor.equalsIgnoreCase("null")) {
				txtDesc = height + "/" + identity + "/" + bodytype + "/"
						+ lookingfor + "/" + city;
			} else {
				txtDesc = height + "/" + identity + "/" + bodytype + "/"
						+ lookingfor + "/" + city;
			}
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		txtextra.setText(data);
		txtaboutme.setText(aboutme);
		heart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("uid for the heart", uid);
				Log.e("selected user", String.valueOf(uid));
				if (c.haveNetworkConnection(getApplicationContext())) {
					sendreq(uid, String.valueOf(uid));
					AlertDialog.Builder builder = new AlertDialog.Builder(
							UserProfile.this);
					builder.setTitle("Love is in the air!");
					builder.setMessage("You pushed her heart.")
							.setCancelable(false)
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											heart.setImageResource(R.drawable.heart_sel);
											heart.setEnabled(false);
										}
									});
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					AlertDialog.Builder dialog2 = new AlertDialog.Builder(
							UserProfile.this);
					dialog2.setTitle("Instamour");
					dialog2.setMessage("No Data Connection Avaible");
					dialog2.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							});
					AlertDialog alert2 = dialog2.create();
					alert2.show();
				}

			}
		});

		uservideopic = (ImageView) findViewById(R.id.uservideopic);
		if (c.haveNetworkConnection(getApplicationContext())) {
			imageLoader.DisplayImage(c.dolink + "" + photo, uservideopic);
		} else {
			AlertDialog.Builder dialog2 = new AlertDialog.Builder(
					UserProfile.this);
			dialog2.setTitle("Instamour");
			dialog2.setMessage("No Data Connection Avaible");
			dialog2.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog alert2 = dialog2.create();
			alert2.show();
		}

	}

	@SuppressLint("NewApi")
	private void sendreq(String sender, String receiver) {
		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "friends-status"));
		param.add(new BasicNameValuePair("sessionId", sender));
		param.add(new BasicNameValuePair("uid", receiver));
		param.add(new BasicNameValuePair("status", "pending"));

		JSONObject json = c.makeHttpRequest(c.link, "POST", param);
		Log.e("pushed in heart", "" + json);

	}

	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.menu:
			finish();
			break;

		default:
			break;
		}
	}
}
