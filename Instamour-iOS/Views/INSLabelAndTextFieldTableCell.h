//
//  INSTextFieldTableCell.h
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols

@interface INSLabelAndTextFieldTableCell : BTITableViewCell

// Public Properties
@property (nonatomic, strong) IBOutlet UITextField *textField;

// Public Methods
- (void)setLabelText:(NSString *)text;

@end
