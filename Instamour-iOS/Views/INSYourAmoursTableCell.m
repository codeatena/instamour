//
//  INSYourAmoursTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/19/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSYourAmoursTableCell.h"

@interface INSYourAmoursTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UIButton *chatButton;
@property (nonatomic, strong) IBOutlet UIButton *videoButton;
@property (nonatomic, strong) IBOutlet UIButton *audioButton;

@end

@implementation INSYourAmoursTableCell

#pragma mark - UITableViewCell Overrides

- (void)setEditing:(BOOL)editing
          animated:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super setEditing:editing animated:animated];
    
    [[self chatButton] setHidden:editing];
    [[self videoButton] setHidden:editing];
    [[self audioButton] setHidden:editing];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (IBAction)chatButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self delegate] yourAmoursCellChatButtonPressed:self];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)videoButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self delegate] yourAmoursCellVideoButtonPressed:self];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)audioButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self delegate] yourAmoursCellAudioButtonPressed:self];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setUserName:(NSString *)userName
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self userNameLabel] setText:userName];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setAudioButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self audioButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setVideoButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self videoButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}


@end
