//
//  INSQuickBloxManager.m
//  Instamour
//
//  Created by Brian Slick on 8/1/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSQuickBloxManager.h"

// Models and Other Global
#import <Quickblox/Quickblox.h>

// Private Constants
NSString *const INSQuickBloxManagerChatCallRejectedNotification = @"INSQuickBloxManagerChatCallRejectedNotification";
NSString *const INSQuickBloxManagerChatCallAcceptedNotification = @"INSQuickBloxManagerChatCallAcceptedNotification";
NSString *const INSQuickBloxManagerChatCallDidBeginNotification = @"INSQuickBloxManagerChatCallDidBeginNotification";
NSString *const INSQuickBloxManagerChatCallEndedByOtherUserNotification = @"INSQuickBloxManagerChatCallEndedByOtherUserNotification";
NSString *const INSQuickBloxManagerOtherUserDidNotAnswerNotification = @"INSQuickBloxManagerOtherUserDidNotAnswerNotification";

typedef NS_ENUM(NSInteger, INSQuickBloxLoginScheme) {
    INSQuickBloxLoginSchemeInstamourEmail = 0,
    INSQuickBloxLoginSchemeUserEmail,
    INSQuickBloxLoginSchemeUserName,
};


@interface INSQuickBloxManager ()

// Private Properties
@property (nonatomic, strong) QBUUser *quickBloxUser;

@property (nonatomic, assign, getter = isSessionCreated) BOOL sessionCreated;
@property (nonatomic, weak) NSTimer *presenceTimer;

// Audio/Video Chat
@property (nonatomic, assign) enum QBVideoChatConferenceType callType;
@property (nonatomic, assign) NSInteger incomingCallOpponentID;
@property (nonatomic, copy) NSString *incomingCallSessionID;

@property (nonatomic, strong) NSMutableSet *busyCallersNotified;

@property (nonatomic, assign) INSQuickBloxLoginScheme quickBloxLoginScheme;

@end

@implementation INSQuickBloxManager

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Management


#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        [self setupQuickBlox];
        
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
        [notificationCenter addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    return self;
}

#pragma mark - Custom Getters and Setters

- (NSMutableSet *)busyCallersNotified
{
    if (_busyCallersNotified == nil)
    {
        _busyCallersNotified = [[NSMutableSet alloc] init];
    }
    return _busyCallersNotified;
}

#pragma mark - Notification Handlers

- (void)applicationWillResignActive:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self chatLogout];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)applicationDidBecomeActive:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDate *sessionExpirationDate = [[QBBaseModule sharedModule] tokenExpirationDate];
    NSDate *currentDate = [NSDate date];
    NSTimeInterval interval = [currentDate timeIntervalSinceDate:sessionExpirationDate];
    if (interval > 0)
    {
        [self createSession];
    }
    else
    {
        [self chatLogin];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setupQuickBlox
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setSessionCreated:NO];
    
    [QBSettings setApplicationID:4422];
    [QBSettings setAuthorizationKey:@"yN7L-P-993Dahmm"];
    [QBSettings setAuthorizationSecret:@"YZuhyVsDQDmGQ4a"];
    [QBSettings setAccountKey:@"MKPg6oxNqpggRQyoyHTz"];


#ifdef INS_STORE_BUILD
    
    [QBSettings setLogLevel:QBLogLevelNothing];
    [QBSettings useProductionEnvironmentForPushNotifications:YES];
    
#else
    
    [QBSettings setLogLevel:QBLogLevelDebug];
    [QBSettings useProductionEnvironmentForPushNotifications:NO];
    
#endif
    
    NSMutableDictionary *videoChatConfiguration = [[QBSettings videoChatConfiguration] mutableCopy];
    [videoChatConfiguration setObject:@20 forKey:kQBVideoChatCallTimeout];
    [videoChatConfiguration setObject:AVCaptureSessionPreset352x288 forKey:kQBVideoChatFrameQualityPreset];
    [videoChatConfiguration setObject:@2 forKey:kQBVideoChatVideoFramesPerSecond];

    [QBSettings setVideoChatConfiguration:videoChatConfiguration];
    
    [self createSession];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setRegisteredForNotifications:(BOOL)isRegistered
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (isRegistered)
    {
        if ([[[INSUserManager sharedManager] currentUser] isPushNotificationsEnabled])
        {
            [QBMessages TRegisterSubscriptionWithDelegate:self];
        }
    }
    else
    {
        [QBMessages TUnregisterSubscriptionWithDelegate:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)notificationRegistrationCompletedWithResult:(QBMRegisterSubscriptionTaskResult *)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSLog(@"Registering successful: %@", [result success] ? @"YES" : @"NO");
    NSLog(@"Errors: %@", [result errors]);

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)notificationUnregistrationCompletedWithResult:(QBMUnregisterSubscriptionTaskResult *)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [QBUsers logOutWithDelegate:self];

    NSLog(@"Unregistering successful: %@", [result success] ? @"YES" : @"NO");
    NSLog(@"Errors: %@", [result errors]);

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Create Session

- (void)createSession
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setSessionCreated:NO];
    
    [self setQuickBloxUser:nil];

    [QBAuth createSessionWithDelegate:self];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)createSessionCompletedWithResult:(QBAAuthSessionCreationResult *)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSLog(@"session creation successful: %@", ([result success]) ? @"YES" : @"NO");

    if ([result success])
    {
        [self setSessionCreated:YES];
        
        [self loginCurrentUser];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Login

- (void)loginCurrentUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (![self isSessionCreated])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Session not created yet", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ([self quickBloxUser] != nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    if (![userManager isLoggedIn])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No current user logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *currentUser = [userManager currentUser];
    
    switch ([self quickBloxLoginScheme])
    {
        case INSQuickBloxLoginSchemeInstamourEmail:
        {
            NSLog(@"Attempting log in with Instamour email");
            
            [QBUsers logInWithUserEmail:[NSString stringWithFormat:@"%@@instamourapp.com", [currentUser userID]]
                               password:[currentUser quickBloxPassword]
                               delegate:self];
        }
            break;
        case INSQuickBloxLoginSchemeUserEmail:
        {
            NSLog(@"Attempting log in with user email");
            
            [QBUsers logInWithUserEmail:[currentUser email]
                               password:[currentUser quickBloxPassword]
                               delegate:self];
            
        }
            break;
        case INSQuickBloxLoginSchemeUserName:
        {
            NSLog(@"Attempting log in with user name");
            
            [QBUsers logInWithUserLogin:[currentUser userName]
                               password:[currentUser quickBloxPassword]
                               delegate:self];
        }
        default:
        {
            [self setQuickBloxLoginScheme:INSQuickBloxLoginSchemeInstamourEmail];
        }
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loginCompletedWithResult:(QBUUserLogInResult *)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSLog(@"Login");
    
    if ([result success])
    {
        NSLog(@"Authentication successful");
        
        INSUser *user = [[INSUserManager sharedManager] currentUser];
        
        QBUUser *quickBloxUser = [result user];
        [self setQuickBloxUser:quickBloxUser];
        
        [quickBloxUser setID:[[user quickBloxID] integerValue]];
        [quickBloxUser setPassword:[user quickBloxPassword]];
        
        [self chatLogin];
    }
    else if ([result status] == 401)
    {
        NSLog(@"Not registered!");
        NSLog(@"User Unauthorized. Trying different scheme");
        
        [self setQuickBloxLoginScheme:[self quickBloxLoginScheme] + 1];
        
        [self loginCurrentUser];
    }
    else
    {
        NSLog(@"errors=%@", result.errors);
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Logout

- (void)logoutCurrentUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopPresenceTimer];

    [self setRegisteredForNotifications:NO];
    
    [self chatLogout];
    
    [self setQuickBloxUser:nil];
    
    [self setQuickBloxLoginScheme:INSQuickBloxLoginSchemeInstamourEmail];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)logoutCompletedWithResult:(QBUUserLogOutResult *)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSLog(@"Logout");
    
    if ([result success])
    {
        NSLog(@"LogOut successful.");
    }
    else
    {
        NSLog(@"errors=%@", result.errors);
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Chat

- (void)chatLogin
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([self quickBloxUser] == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No user", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[QBChat instance] setDelegate:self];
    
    [[QBChat instance] loginWithUser:[self quickBloxUser]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatLogout
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[QBChat instance] logout];

    [self stopPresenceTimer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Presence Timer

- (void)startPresenceTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopPresenceTimer];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:30
                                                      target:self
                                                    selector:@selector(presenceTimerFireMethod:)
                                                    userInfo:nil
                                                     repeats:YES];
    [self setPresenceTimer:timer];
    
    [timer fire];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopPresenceTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self presenceTimer] invalidate];
    
    [self setPresenceTimer:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)presenceTimerFireMethod:(NSTimer *)timer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSLog(@"Presence");
    [[QBChat instance] sendPresence];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Audio/Video Support Methods

- (void)cleanupAfterCallEnds
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self busyCallersNotified] removeAllObjects];
    
    [self setIncomingCallOpponentID:NSNotFound];
    [self setIncomingCallSessionID:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - QBActionStatusDelegate Methods

- (void)completedWithResult:(Result *)result
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSLog(@"Result class: %@", NSStringFromClass([result class]));
    
    // Create session
    
    if ([result isKindOfClass:[QBAAuthSessionCreationResult class]])
    {
        [self createSessionCompletedWithResult:(QBAAuthSessionCreationResult *)result];
    }

    // Login
    
    else if ([result isKindOfClass:[QBUUserLogInResult class]])
    {
        [self loginCompletedWithResult:(QBUUserLogInResult *)result];
    }
    
    // Logout
    
    else if ([result isKindOfClass:[QBUUserLogOutResult class]])
    {
        [self logoutCompletedWithResult:(QBUUserLogOutResult *)result];
    }
    
    // Register push notifications
    
    else if ([result isKindOfClass:[QBMRegisterSubscriptionTaskResult class]])
    {
        [self notificationRegistrationCompletedWithResult:(QBMRegisterSubscriptionTaskResult *)result];
    }
    
    // Unregister push notifications
    
    else if ([result isKindOfClass:[QBMUnregisterSubscriptionTaskResult class]])
    {
        [self notificationUnregistrationCompletedWithResult:(QBMUnregisterSubscriptionTaskResult *)result];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - QBChatDelegate Methods

- (void)chatDidLogin
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSLog(@"Chat logged in");
    [self startPresenceTimer];

    [self setRegisteredForNotifications:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatDidNotLogin
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSLog(@"Chat did NOT log in");

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatDidFailWithError:(NSInteger)code
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopPresenceTimer];
    
    if (code == 0)
    {
        //http://stackoverflow.com/questions/19150338/quickblox-how-to-check-qbchat-diddisconnect-to-force-the-user-to-login-again
        // If it was manual logout - code will be equal to 0
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Manual logout", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setQuickBloxUser:nil];
    
    [self loginCurrentUser];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Text Chate Delegate Methods

- (void)chatDidNotSendMessage:(QBChatMessage *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSChatDataManager sharedManager] quickBloxChatMessageDidFailToSend:message];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatDidReceiveMessage:(QBChatMessage *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSChatDataManager sharedManager] didReceiveQuickBloxChatMessage:message];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Video Chat Delegate Methods

- (void)chatDidReceiveCallRequestFromUser:(NSUInteger)userID
                            withSessionID:(NSString *)sessionID
                           conferenceType:(enum QBVideoChatConferenceType)conferenceType
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSLog(@"session ID: %@", sessionID);
    
    // This method will be called multiple times - like once per second - for any incoming call.
    // Repeat calls could be from the same user before the current user has answered the call.
    // Make sure to send the "Busy" notification only once per incoming user if the current user is busy.

    INSUser *otherUser = [[INSUserManager sharedManager] amourWithQuickBloxID:userID];
    
    if ([self incomingCallSessionID] != nil)
    {
        // User might be busy
        
        if (userID == [self incomingCallOpponentID])
        {
            NSLog(@"<<< Leaving  <%p> %s >>> EARLY - Establishing call with this user", self, __PRETTY_FUNCTION__);
            return;
        }
        
        // This is a different user. Only send busy message once.
        
        if ([[self busyCallersNotified] containsObject:@(userID)])
        {
            // User has already been notififed
            NSLog(@"<<< Leaving  <%p> %s >>> EARLY - Busy message already sent to this user", self, __PRETTY_FUNCTION__);
            return;
        }
        
        [[self busyCallersNotified] addObject:@(userID)];
        
        [[INSPushNotificationManager sharedManager] sendBusyCallMessageToUser:otherUser];
        
        NSLog(@"<<< Leaving  <%p> %s >>> EARLY - Sending busy message to user", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setIncomingCallOpponentID:userID];
    [self setIncomingCallSessionID:sessionID];
    [self setCallType:conferenceType];
    
    [[AppDelegate sharedDelegate] showCallingViewForUser:otherUser
                                      quickBloxSessionID:sessionID
                                                callType:conferenceType
                                            incomingCall:YES];
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallUserDidNotAnswer:(NSUInteger)userID
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *otherUser = [[INSUserManager sharedManager] amourWithQuickBloxID:userID];
    
    [[INSPushNotificationManager sharedManager] sendMissedCallMessageToUser:otherUser
                                                                forCallType:[self callType]];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSQuickBloxManagerOtherUserDidNotAnswerNotification];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instamour"
                                                    message:@"User isn't answering. Please try again later."
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallDidAcceptByUser:(NSUInteger)userID
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSQuickBloxManagerChatCallAcceptedNotification];
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallDidRejectByUser:(NSUInteger)userID
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self cleanupAfterCallEnds];

    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSQuickBloxManagerChatCallRejectedNotification];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instamour"
                                                    message:@"User has rejected your call"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallDidStopByUser:(NSUInteger)userID
                       status:(NSString *)status
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self cleanupAfterCallEnds];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSQuickBloxManagerChatCallEndedByOtherUserNotification];
    
    if ([status isEqualToString:kStopVideoChatCallStatus_OpponentDidNotAnswer])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instamour"
                                                        message:@"They did not answer"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([status isEqualToString:kStopVideoChatCallStatus_Manually])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instamour"
                                                        message:@"They hung up"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([status isEqualToString:kStopVideoChatCallStatus_BadConnection])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instamour"
                                                        message:@"Bad network connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallDidStartWithUser:(NSUInteger)userID
                       sessionID:(NSString *)sessionID
{
    NSLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSQuickBloxManagerChatCallDidBeginNotification];
    
    NSLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Singleton

+ (instancetype)sharedManager
{
	static dispatch_once_t pred;
	static INSQuickBloxManager *shared = nil;
    
	dispatch_once(&pred, ^{
        
		shared = [[[self class] alloc] init];
        
	});
    
	return shared;
}

@end
