//
//  INSBrowseProfilesViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/15/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIArrayCollectionViewController.h"

// Public Constants
typedef NS_ENUM(NSInteger, INSBrowseProfilesDisplayMode) {
    INSBrowseProfilesDisplayModeBrowse = 1,
    INSBrowseProfilesDisplayModeCustomSearch,
};

// Protocols

@interface INSBrowseProfilesViewController : BTIArrayCollectionViewController

// Public Properties
@property (nonatomic, assign, readonly) INSBrowseProfilesDisplayMode displayMode;


// Public Methods
- (instancetype)initWithDisplayMode:(INSBrowseProfilesDisplayMode)displayMode;

@end
