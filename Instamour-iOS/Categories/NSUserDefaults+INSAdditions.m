//
//  NSUserDefaults+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/4/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "NSUserDefaults+INSAdditions.h"


@implementation NSUserDefaults (INSAdditions)

#pragma mark - Misc

- (void)safelySetString:(NSString *)string forKey:(NSString *)key
{
    if ([string isNotEmpty])
    {
        [self setObject:string forKey:key];
    }
    else
    {
        [self removeObjectForKey:key];
    }
}

#pragma mark - App Settings Web Service

NSString *const INSUserDefaultsAppSettingsRefreshDateKey = @"INSUserDefaultsAppSettingsRefreshDateKey";

- (NSDate *)appSettingsRefreshDateINS
{
    return [self objectForKey:INSUserDefaultsAppSettingsRefreshDateKey];
}

- (void)setAppSettingsRefreshDateINS:(NSDate *)appSettingsRefreshDateINS
{
    [self setObject:appSettingsRefreshDateINS forKey:INSUserDefaultsAppSettingsRefreshDateKey];
}

NSString *const INSUserDefaultsServerAvatarImagesFilePathKey = @"INSUserDefaultsServerAvatarImagesFilePathKey";

- (NSString *)serverAvatarImagesFilePathINS
{
    return [self stringForKey:INSUserDefaultsServerAvatarImagesFilePathKey];
}

- (void)setServerAvatarImagesFilePathINS:(NSString *)serverAvatarImagesFilePathINS
{
    [self safelySetString:serverAvatarImagesFilePathINS forKey:INSUserDefaultsServerAvatarImagesFilePathKey];
}

NSString *const INSUserDefaultsServerVideoThumbnailImagesFilePathKey = @"INSUserDefaultsServerVideoThumbnailImagesFilePathKey";

- (NSString *)serverVideoThumbnailImagesFilePathINS
{
    return [self stringForKey:INSUserDefaultsServerVideoThumbnailImagesFilePathKey];
}

- (void)setServerVideoThumbnailFilePathINS:(NSString *)serverVideoThumbnailImagesFilePathINS
{
    [self safelySetString:serverVideoThumbnailImagesFilePathINS forKey:INSUserDefaultsServerVideoThumbnailImagesFilePathKey];
}

NSString *const INSUserDefaultsServerVideosFilePathKey = @"INSUserDefaultsServerVideosFilePathKey";

- (NSString *)serverVideosFilePathINS
{
    return [self stringForKey:INSUserDefaultsServerVideosFilePathKey];
}

- (void)setServerVideosFilePathINS:(NSString *)serverVideosFilePathINS
{
    [self safelySetString:serverVideosFilePathINS forKey:INSUserDefaultsServerVideosFilePathKey];
}

NSString *const INSUserDefaultsServerLowQualityVideosFilePathKey = @"INSUserDefaultsServerLowQualityVideosFilePathKey";

- (NSString *)serverLowQualityVideosFilePathINS
{
    return [self stringForKey:INSUserDefaultsServerLowQualityVideosFilePathKey];
}

- (void)setServerLowQualityVideosFilePathINS:(NSString *)serverLowQualityVideosFilePathINS
{
    [self safelySetString:serverLowQualityVideosFilePathINS forKey:INSUserDefaultsServerLowQualityVideosFilePathKey];
}

NSString *const INSUserDefaultsAllBodyTypesKey = @"INSUserDefaultsAllBodyTypesKey";

- (NSArray *)allBodyTypesINS
{
    return [self objectForKey:INSUserDefaultsAllBodyTypesKey];
}

- (void)setAllBodyTypesINS:(NSArray *)allBodyTypesINS
{
    [self setObject:allBodyTypesINS forKey:INSUserDefaultsAllBodyTypesKey];
}

NSString *const INSUserDefaultsAllEthnicitiesKey = @"INSUserDefaultsAllEthnicitiesKey";

- (NSArray *)allEthnicitiesINS
{
    return [self objectForKey:INSUserDefaultsAllEthnicitiesKey];
}

- (void)setAllEthnicitiesINS:(NSArray *)allEthnicitiesINS
{
    [self setObject:allEthnicitiesINS forKey:INSUserDefaultsAllEthnicitiesKey];
}

NSString *const INSUserDefaultsAllFlagsKey = @"INSUserDefaultsAllFlagsKey";

- (NSArray *)allFlagsINS
{
    return [self objectForKey:INSUserDefaultsAllFlagsKey];
}

- (void)setAllFlagsINS:(NSArray *)allFlagsINS
{
    [self setObject:allFlagsINS forKey:INSUserDefaultsAllFlagsKey];
}

NSString *const INSUserDefaultsAllGendersKey = @"INSUserDefaultsAllGendersKey";

- (NSArray *)allGendersINS
{
    return [self objectForKey:INSUserDefaultsAllGendersKey];
}

- (void)setAllGendersINS:(NSArray *)allGendersINS
{
    [self setObject:allGendersINS forKey:INSUserDefaultsAllGendersKey];
}

NSString *const INSUserDefaultsAllIdentitiesKey = @"INSUserDefaultsAllIdentitiesKey";

- (NSArray *)allIdentitiesINS
{
    return [self objectForKey:INSUserDefaultsAllIdentitiesKey];
}

- (void)setAllIdentitiesINS:(NSArray *)allIdentitiesINS
{
    [self setObject:allIdentitiesINS forKey:INSUserDefaultsAllIdentitiesKey];
}

NSString *const INSUserDefaultsAllLookingForsKey = @"INSUserDefaultsAllLookingForsKey";

- (NSArray *)allLookingForsINS
{
    return [self objectForKey:INSUserDefaultsAllLookingForsKey];
}

- (void)setAllLookingForsINS:(NSArray *)allLookingForsINS
{
    [self setObject:allLookingForsINS forKey:INSUserDefaultsAllLookingForsKey];
}

NSString *const INSUserDefaultsAllSexualPreferencesKey = @"INSUserDefaultsAllSexualPreferencesKey";

- (NSArray *)allSexualPreferencesINS
{
    return [self objectForKey:INSUserDefaultsAllSexualPreferencesKey];
}

- (void)setAllSexualPreferencesINS:(NSArray *)allSexualPreferencesINS
{
    [self setObject:allSexualPreferencesINS forKey:INSUserDefaultsAllSexualPreferencesKey];
}

NSString *const INSUserDefaultsAllSmokersKey = @"INSUserDefaultsAllSmokersKey";

- (NSArray *)allSmokersINS
{
    return [self objectForKey:INSUserDefaultsAllSmokersKey];
}

- (void)setAllSmokersINS:(NSArray *)allSmokersINS
{
    [self setObject:allSmokersINS forKey:INSUserDefaultsAllSmokersKey];
}

#pragma mark - Search

NSString *const INSUserDefaultsBrowserProfilesSearchGenderKey = @"INSUserDefaultsBrowserProfilesSearchGenderKey";

- (NSString *)browserProfilesSearchGenderINS
{
    return [self stringForKey:INSUserDefaultsBrowserProfilesSearchGenderKey];
}

- (void)setBrowserProfilesSearchGenderINS:(NSString *)browserProfilesSearchGenderINS
{
    [self safelySetString:browserProfilesSearchGenderINS forKey:INSUserDefaultsBrowserProfilesSearchGenderKey];
}

NSString *const INSUserDefaultsSearchTermUserNameKey = @"INSUserDefaultsSearchTermUserNameKey";

- (NSString *)searchTermUserNameINS
{
    return [self stringForKey:INSUserDefaultsSearchTermUserNameKey];
}

- (void)setSearchTermUserNameINS:(NSString *)searchTermUserNameINS
{
    [self safelySetString:searchTermUserNameINS forKey:INSUserDefaultsSearchTermUserNameKey];
}

NSString *const INSUserDefaultsSearchTermChildrenKey = @"INSUserDefaultsSearchTermChildrenKey";

- (NSString *)searchTermChildrenINS
{
    return [self stringForKey:INSUserDefaultsSearchTermChildrenKey];
}

- (void)setSearchTermChildrenINS:(NSString *)searchTermChildrenINS
{
    [self safelySetString:searchTermChildrenINS forKey:INSUserDefaultsSearchTermChildrenKey];
}

NSString *const INSUserDefaultsSearchTermBodyTypeKey = @"INSUserDefaultsSearchTermBodyTypeKey";

- (NSString *)searchTermBodyTypeINS
{
    return [self stringForKey:INSUserDefaultsSearchTermBodyTypeKey];
}

- (void)setSearchTermBodyTypeINS:(NSString *)searchTermBodyTypeINS
{
    [self safelySetString:searchTermBodyTypeINS forKey:INSUserDefaultsSearchTermBodyTypeKey];
}

NSString *const INSUserDefaultsSearchTermGenderKey = @"INSUserDefaultsSearchTermGenderKey";

- (NSString *)searchTermGenderINS
{
    return [self stringForKey:INSUserDefaultsSearchTermGenderKey];
}

- (void)setSearchTermGenderINS:(NSString *)searchTermGenderINS
{
    [self safelySetString:searchTermGenderINS forKey:INSUserDefaultsSearchTermGenderKey];
}

NSString *const INSUserDefaultsSearchTermMinimumAgeKey = @"INSUserDefaultsSearchTermMinimumAgeKey";

- (NSInteger)searchTermMinimumAgeINS
{
    id object = [self objectForKey:INSUserDefaultsSearchTermMinimumAgeKey];
    
    if (object == nil)
    {
        return NSNotFound;
    }
    
    NSInteger integer = [self integerForKey:INSUserDefaultsSearchTermMinimumAgeKey];
    
    if (integer < 0)
    {
        integer = 0;
    }
    
    return integer;
}

- (void)setSearchTermMinimumAgeINS:(NSInteger)searchTermMinimumAgeINS
{
    [self setInteger:searchTermMinimumAgeINS forKey:INSUserDefaultsSearchTermMinimumAgeKey];
}

NSString *const INSUserDefaultsSearchTermMaximumAgeKey = @"INSUserDefaultsSearchTermMaximumAgeKey";

- (NSInteger)searchTermMaximumAgeINS
{
    id object = [self objectForKey:INSUserDefaultsSearchTermMaximumAgeKey];
    
    if (object == nil)
    {
        return NSNotFound;
    }
    
    NSInteger integer = [self integerForKey:INSUserDefaultsSearchTermMaximumAgeKey];
    
    if (integer < 0)
    {
        integer = 0;
    }
    
    return integer;
}

- (void)setSearchTermMaximumAgeINS:(NSInteger)searchTermMaximumAgeINS
{
    [self setInteger:searchTermMaximumAgeINS forKey:INSUserDefaultsSearchTermMaximumAgeKey];
}

NSString *const INSUserDefaultsSearchTermLookingForKey = @"INSUserDefaultsSearchTermLookingForKey";

- (NSString *)searchTermLookingForINS
{
    return [self stringForKey:INSUserDefaultsSearchTermLookingForKey];
}

- (void)setSearchTermLookingForINS:(NSString *)searchTermLookingForINS
{
    [self safelySetString:searchTermLookingForINS forKey:INSUserDefaultsSearchTermLookingForKey];
}

NSString *const INSUserDefaultsSearchTermVideoCountKey = @"INSUserDefaultsSearchTermVideoCountKey";

- (NSInteger)searchTermVideoCountINS
{
    id object = [self objectForKey:INSUserDefaultsSearchTermVideoCountKey];
    
    if (object == nil)
    {
        return NSNotFound;
    }
    
    NSInteger integer = [self integerForKey:INSUserDefaultsSearchTermVideoCountKey];
    
    if (integer < 0)
    {
        integer = NSNotFound;
    }
    
    return integer;
}

- (void)setSearchTermVideoCountINS:(NSInteger)searchTermVideoCountINS
{
    [self setInteger:searchTermVideoCountINS forKey:INSUserDefaultsSearchTermVideoCountKey];
}

NSString *const INSUserDefaultsSearchTermMilesKey = @"INSUserDefaultsSearchTermMilesKey";

- (NSString *)searchTermMilesINS
{
    return [self stringForKey:INSUserDefaultsSearchTermMilesKey];
}

- (void)setSearchTermMilesINS:(NSString *)searchTermMilesINS
{
    [self safelySetString:searchTermMilesINS forKey:INSUserDefaultsSearchTermMilesKey];
}

NSString *const INSUserDefaultsSearchTermLastOnlineKey = @"INSUserDefaultsSearchTermLastOnlineKey";

- (NSString *)searchTermLastOnlineINS
{
    return [self stringForKey:INSUserDefaultsSearchTermLastOnlineKey];
}

- (void)setSearchTermLastOnlineINS:(NSString *)searchTermLastOnlineINS
{
    [self safelySetString:searchTermLastOnlineINS forKey:INSUserDefaultsSearchTermLastOnlineKey];
}

NSString *const INSUserDefaultsSearchTermEthnicityKey = @"INSUserDefaultsSearchTermEthnicityKey";

- (NSString *)searchTermEthnicityINS
{
    return [self stringForKey:INSUserDefaultsSearchTermEthnicityKey];
}

- (void)setSearchTermEthnicityINS:(NSString *)searchTermEthnicityINS
{
    [self safelySetString:searchTermEthnicityINS forKey:INSUserDefaultsSearchTermEthnicityKey];
}

NSString *const INSUserDefaultsSearchTermSmokerKey = @"INSUserDefaultsSearchTermSmokerKey";

- (NSString *)searchTermSmokerINS
{
    return [self stringForKey:INSUserDefaultsSearchTermSmokerKey];
}

- (void)setSearchTermSmokerINS:(NSString *)searchTermSmokerINS
{
    [self safelySetString:searchTermSmokerINS forKey:INSUserDefaultsSearchTermSmokerKey];
}

NSString *const INSUserDefaultsSearchTermSexualPreferenceKey = @"INSUserDefaultsSearchTermSexualPreferenceKey";

- (NSString *)searchTermSexualPreferenceINS
{
    return [self stringForKey:INSUserDefaultsSearchTermSexualPreferenceKey];
}

- (void)setSearchTermSexualPreferenceINS:(NSString *)searchTermSexualPreferenceINS
{
    [self safelySetString:searchTermSexualPreferenceINS forKey:INSUserDefaultsSearchTermSexualPreferenceKey];
}

#pragma mark - Misc

NSString *const INSUserDefaultsCorporateAlertsEnabledKey = @"INSUserDefaultsCorporateAlertsEnabledKey";

- (BOOL)isCorporateAlertsEnabledINS
{
    return [self boolForKey:INSUserDefaultsCorporateAlertsEnabledKey];
}

- (void)setCorporateAlertsEnabledINS:(BOOL)corporateAlertsEnabled
{
    [self setBool:corporateAlertsEnabled forKey:INSUserDefaultsCorporateAlertsEnabledKey];
}

NSString *const INSUserDefaultsVideoNeededAlertEnabledKey = @"INSUserDefaultsVideoNeededAlertEnabledKey";

- (BOOL)isVideoNeededAlertEnabledINS
{
    return [self boolForKey:INSUserDefaultsVideoNeededAlertEnabledKey];
}

- (void)setVideoNeededAlertEnabledINS:(BOOL)videoNeededAlertEnabledINS
{
    [self setBool:videoNeededAlertEnabledINS forKey:INSUserDefaultsVideoNeededAlertEnabledKey];
}

NSString *const INSUserDefaultsCreateVideo1AlertEnabledKey = @"INSUserDefaultsCreateVideo1AlertEnabledKey";

- (BOOL)isCreateVideo1AlertEnabledINS
{
    return [self boolForKey:INSUserDefaultsCreateVideo1AlertEnabledKey];
}

- (void)setCreateVideo1AlertEnabledINS:(BOOL)createVideo1AlertEnabledINS
{
    [self setBool:createVideo1AlertEnabledINS forKey:INSUserDefaultsCreateVideo1AlertEnabledKey];
}

NSString *const INSUserDefaultsCreateVideo2AlertEnabledKey = @"INSUserDefaultsCreateVideo2AlertEnabledKey";

- (BOOL)isCreateVideo2AlertEnabledINS
{
    return [self boolForKey:INSUserDefaultsCreateVideo2AlertEnabledKey];
}

- (void)setCreateVideo2AlertEnabledINS:(BOOL)createVideo2AlertEnabledINS
{
    [self setBool:createVideo2AlertEnabledINS forKey:INSUserDefaultsCreateVideo2AlertEnabledKey];
}

NSString *const INSUserDefaultsCreateVideo3AlertEnabledKey = @"INSUserDefaultsCreateVideo3AlertEnabledKey";

- (BOOL)isCreateVideo3AlertEnabledINS
{
    return [self boolForKey:INSUserDefaultsCreateVideo3AlertEnabledKey];
}

- (void)setCreateVideo3AlertEnabledINS:(BOOL)createVideo3AlertEnabledINS
{
    [self setBool:createVideo3AlertEnabledINS forKey:INSUserDefaultsCreateVideo3AlertEnabledKey];
}

NSString *const INSUserDefaultsCreateVideo4AlertEnabledKey = @"INSUserDefaultsCreateVideo4AlertEnabledKey";

- (BOOL)isCreateVideo4AlertEnabledINS
{
    return [self boolForKey:INSUserDefaultsCreateVideo4AlertEnabledKey];
}

- (void)setCreateVideo4AlertEnabledINS:(BOOL)createVideo4AlertEnabledINS
{
    [self setBool:createVideo4AlertEnabledINS forKey:INSUserDefaultsCreateVideo4AlertEnabledKey];
}

#pragma mark - Misc Methods

- (void)initializeValuesForNewUserINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self deleteAllValuesINS];
    
    // Misc UI
    [self setCorporateAlertsEnabledINS:YES];
    [self setVideoNeededAlertEnabledINS:YES];
    [self setCreateVideo1AlertEnabledINS:YES];
    [self setCreateVideo2AlertEnabledINS:YES];
    [self setCreateVideo3AlertEnabledINS:YES];
    [self setCreateVideo4AlertEnabledINS:YES];
    
    [self synchronize];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)initializeValuesForLoginINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // Misc Internal
    
    // Misc UI
    [self setVideoNeededAlertEnabledINS:YES];
    [self setCreateVideo1AlertEnabledINS:YES];
    [self setCreateVideo2AlertEnabledINS:YES];
    [self setCreateVideo3AlertEnabledINS:YES];
    [self setCreateVideo4AlertEnabledINS:YES];

    [self synchronize];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteAllValuesINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSArray *miscKeysToPreserve = @[ @"device_Token" ];
    NSArray *serverPathsToPreserve = @[ INSUserDefaultsServerAvatarImagesFilePathKey, INSUserDefaultsServerVideoThumbnailImagesFilePathKey, INSUserDefaultsServerVideosFilePathKey, INSUserDefaultsServerLowQualityVideosFilePathKey ];
    NSArray *appSettingsKeysToPreserve = @[ INSUserDefaultsAppSettingsRefreshDateKey, INSUserDefaultsAllBodyTypesKey, INSUserDefaultsAllEthnicitiesKey, INSUserDefaultsAllFlagsKey, INSUserDefaultsAllGendersKey, INSUserDefaultsAllIdentitiesKey, INSUserDefaultsAllLookingForsKey, INSUserDefaultsAllSexualPreferencesKey, INSUserDefaultsAllSmokersKey ];
    
    NSMutableSet *allKeysToPreserve = [NSMutableSet set];
    
    [allKeysToPreserve addObjectsFromArray:miscKeysToPreserve];
    [allKeysToPreserve addObjectsFromArray:serverPathsToPreserve];
    [allKeysToPreserve addObjectsFromArray:appSettingsKeysToPreserve];
    
    NSMutableDictionary *objectsToPreserve = [NSMutableDictionary dictionary];
    
    for (NSString *key in allKeysToPreserve)
    {
        id object = [self objectForKey:key];
        
        if (object != nil)
        {
            [objectsToPreserve setObject:object forKey:key];
        }
    }
    
    [self removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    
    [self synchronize];
    
    [objectsToPreserve enumerateKeysAndObjectsUsingBlock:^(NSString *key, id object, BOOL *stop) {
        
        [self setObject:object forKey:key];
        
    }];
    
    [self synchronize];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadWithGetUserJSONResultINS:(NSDictionary *)jsonDictionary;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (jsonDictionary == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No JSON Data", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![jsonDictionary isKindOfClass:[NSDictionary class]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - JSON data is not a dictionary", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![jsonDictionary isStatusSuccessfulINS])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - JSON results were not successful", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSVideoDataManager sharedManager] populateWithGetUserJSONDictionary:jsonDictionary];
    
    [self setBrowserGenderSearchCriteriaIfNecessaryINS];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)clearAllSearchCriteriaINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setSearchTermUserNameINS:nil];
    [self setSearchTermChildrenINS:nil];
    [self setSearchTermBodyTypeINS:nil];
    [self setSearchTermGenderINS:nil];
    [self setSearchTermMinimumAgeINS:NSNotFound];
    [self setSearchTermMaximumAgeINS:NSNotFound];
    [self setSearchTermLookingForINS:nil];
    [self setSearchTermVideoCountINS:NSNotFound];
    [self setSearchTermMilesINS:nil];
    [self setSearchTermLastOnlineINS:nil];
    [self setSearchTermEthnicityINS:nil];
    [self setSearchTermSmokerINS:nil];
    [self setSearchTermSexualPreferenceINS:nil];
    
    [self synchronize];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setBrowserGenderSearchCriteriaIfNecessaryINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSString *browserGenderSearch = [self browserProfilesSearchGenderINS];
    
    if ([browserGenderSearch isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already have a term", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    NSString *gender = [user gender];
    
    if (![gender isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No gender specified", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *straight = @"Straight";
    NSString *gay = @"Gay";
    NSString *lesbian = @"Lesbian";
    
    NSString *sexualPreference = [user sexualPreference];
    
    // If no preference is specified, assume opposite gender.
    // If preference is bisexual, don't do anything. An empty field should return both genders from the server. (? - Verify)
    
    if ([sexualPreference isNotEmpty])
    {
        if ([gender isEqualToString:INSMaleString])
        {
            if ([sexualPreference isEqualToString:straight])
            {
                [self setBrowserProfilesSearchGenderINS:INSFemaleString];
            }
            else if ([sexualPreference isEqualToString:gay])
            {
                [self setBrowserProfilesSearchGenderINS:INSMaleString];
            }
        }
        else if ([gender isEqualToString:INSFemaleString])
        {
            if ([sexualPreference isEqualToString:straight])
            {
                [self setBrowserProfilesSearchGenderINS:INSMaleString];
            }
            else if ( [sexualPreference isEqualToString:gay] || [sexualPreference isEqualToString:lesbian] )
            {
                [self setBrowserProfilesSearchGenderINS:INSFemaleString];
            }
        }
    }
    else
    {
        if ([gender isEqualToString:INSMaleString])
        {
            [self setBrowserProfilesSearchGenderINS:INSFemaleString];
        }
        else if ([gender isEqualToString:INSFemaleString])
        {
            [self setBrowserProfilesSearchGenderINS:INSMaleString];
        }
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
