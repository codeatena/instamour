//
//  INSComment.m
//  Instamour
//
//  Created by Brian Slick on 7/18/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSComment.h"

// Private Constants
NSString *const INSCommentIDKey = @"INSCommentIDKey";
NSString *const INSCommentSenderIDKey = @"INSCommentSenderIDKey";
NSString *const INSCommentSenderUserNameKey = @"INSCommentSenderUserNameKey";
NSString *const INSCommentAvatarFileNameKey = @"INSCommentAvatarFileNameKey";
NSString *const INSCommentBodyKey = @"INSCommentBodyKey";
NSString *const INSCommentDateAddedKey = @"INSCommentDateAddedKey";


@interface INSComment ()

// Private Properties
@property (nonatomic, assign, getter = isDownloadingAvatar) BOOL downloadingAvatar;

@end

@implementation INSComment

#pragma mark - Dealloc and Memory Management

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Getters and Setters

- (UIImage *)avatarImage
{
    if (_avatarImage == nil)
    {
        [self loadLocalImage];
        
        if (_avatarImage == nil)
        {
            [self downloadServerImage];
        }
    }
    return _avatarImage;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveMemoryWarningNotification:)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
    }
    return self;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDictionary
{
    self = [self init];
    if (self)
    {
        [self setIdentifier:[jsonDictionary identifierForCommentINS]];
        [self setSenderID:[jsonDictionary senderIDForCommentINS]];
        [self setSenderUserName:[jsonDictionary senderUserNameForCommentINS]];
        [self setAvatarFileName:[jsonDictionary avatarFileNameForCommentINS]];
        [self setBody:[jsonDictionary bodyForCommentINS]];
        [self setDateAdded:[jsonDictionary dateAddedForCommentINS]];
    }
    return self;
}

#pragma mark - NSCoding Methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self identifier] forKey:INSCommentIDKey];
    [aCoder encodeObject:[self senderID] forKey:INSCommentSenderIDKey];
    [aCoder encodeObject:[self senderUserName] forKey:INSCommentSenderUserNameKey];
    [aCoder encodeObject:[self avatarFileName] forKey:INSCommentAvatarFileNameKey];
    [aCoder encodeObject:[self body] forKey:INSCommentBodyKey];
    [aCoder encodeObject:[self dateAdded] forKey:INSCommentDateAddedKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self)
    {
        [self setIdentifier:[aDecoder decodeObjectForKey:INSCommentIDKey]];
        [self setSenderID:[aDecoder decodeObjectForKey:INSCommentSenderIDKey]];
        [self setSenderUserName:[aDecoder decodeObjectForKey:INSCommentSenderUserNameKey]];
        [self setAvatarFileName:[aDecoder decodeObjectForKey:INSCommentAvatarFileNameKey]];
        [self setBody:[aDecoder decodeObjectForKey:INSCommentBodyKey]];
        [self setDateAdded:[aDecoder decodeObjectForKey:INSCommentDateAddedKey]];
    }
    return self;
}

- (void)loadLocalImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[self avatarFileName] isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:[self avatarFileName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No local file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    UIImage *image = [[UIImage alloc] initWithData:data];
    NSLog(@"local image: %@", image);
    
    [self setAvatarImage:image];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadServerImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isDownloadingAvatar])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already downloading", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *fileName = [self avatarFileName];
    
    if (![fileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setDownloadingAvatar:YES];
    
    NSString *urlString = [[[NSUserDefaults standardUserDefaults] serverAvatarImagesFilePathINS] stringByAppendingPathComponent:fileName];
    NSURL *url = [NSURL URLWithString:urlString];
    
    __weak INSComment *weakSelf = self;
    
    [[INSServerAPIManager sharedManager] downloadImageFromURL:url
                                                         success:^(UIImage *image) {
                                                             
                                                             NSLog(@"Downloaded image");
                                                             
                                                             [weakSelf setAvatarImage:image];
                                                             
                                                             if ([weakSelf avatarDownloadCompletionBlock] != nil)
                                                             {
                                                                 [weakSelf avatarDownloadCompletionBlock](image);
                                                                 
                                                                 [weakSelf setAvatarDownloadCompletionBlock:nil];
                                                             }
                                                             
                                                             NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];
                                                             
                                                             NSData *data = UIImagePNGRepresentation(image);
                                                             
                                                             [data writeToURL:fileURL atomically:YES];
                                                             
                                                             [weakSelf setDownloadingAvatar:NO];
                                                             
                                                         }
                                                         failure:^(NSString *errorMessage) {
                                                             
                                                             NSLog(@"Failed to download image");
                                                             NSLog(@"error: %@", errorMessage);
                                                             NSLog(@"URL: %@", url);
                                                             [weakSelf setDownloadingAvatar:NO];
                                                             
                                                         }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)didReceiveMemoryWarningNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setAvatarImage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
