package com.hyper.instamour;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.quickblox.module.videochat.core.service.QBVideoChatService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class EditProfile extends Activity {

	private SharedPreferences preferences;
	private SharedPreferences.Editor editor;
	private String uid;
	private EditText edt_username, edt_aboutme;
	private Spinner spinner_identity, spinner_height, spinner_bodytype,
			spinner_lookingfor, spinner_ethnicity, spinner_smoker, spinner_sex;
	private String uname, identity, height, bodytype, lookingfor, ethnicity,
			sex, smoker, aboutme;
	Global global;
	private int identity_pos, height_pos, bodytype_pos, lookingfor_pos,
			ethnicity_pos, smoker_pos, sex_pos;
	private List<NameValuePair> param;
	private Function c;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			new VideoSetting(EditProfile.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_profile);
		c = new Function();
		global = Global.getInstance();
		preferences = getSharedPreferences("editprofile", MODE_PRIVATE);
		global.preferences = getSharedPreferences("LoginPrefrence",
				MODE_PRIVATE);
		uid = global.preferences.getString("uid", "");
		uname = global.getLoginusername();

		identity = preferences.getString("identity", identity);
		height = preferences.getString("height", height);
		bodytype = preferences.getString("body_type", bodytype);
		lookingfor = preferences.getString("looking_for", lookingfor);
		ethnicity = preferences.getString("ethnicity", ethnicity);
		smoker = preferences.getString("smoker", smoker);
		sex = preferences.getString("sexual_preference", sex);
		aboutme = preferences.getString("about_me", aboutme);

		Log.e("User name", uname + " " + lookingfor);

		global.setFrom_edit(true);
		findViewById();

	}

	public void findViewById() {
		edt_username = (EditText) findViewById(R.id.username_edt);
		edt_aboutme = (EditText) findViewById(R.id.about_me_edt);
		spinner_identity = (Spinner) findViewById(R.id.identity_spinner);
		spinner_height = (Spinner) findViewById(R.id.height_spinner);
		spinner_bodytype = (Spinner) findViewById(R.id.body_type_spinner);
		spinner_lookingfor = (Spinner) findViewById(R.id.lookingfor_spinner);
		spinner_ethnicity = (Spinner) findViewById(R.id.ethnicity_spinner);
		spinner_smoker = (Spinner) findViewById(R.id.smoker_spinner);
		spinner_sex = (Spinner) findViewById(R.id.sex_spinner);

		// setting value
		edt_username.setText(uname);
		edt_aboutme.setText(aboutme);

		ArrayAdapter myAdapter = (ArrayAdapter) spinner_identity.getAdapter();
		identity_pos = myAdapter.getPosition(identity);
		spinner_identity.setSelection(identity_pos);
		Log.e("pos i", Integer.toString(identity_pos));

		ArrayAdapter myAdapter1 = (ArrayAdapter) spinner_height.getAdapter();
		height_pos = myAdapter1.getPosition(height);
		spinner_height.setSelection(height_pos);
		Log.e("pos h", Integer.toString(height_pos));

		ArrayAdapter myAdapter2 = (ArrayAdapter) spinner_bodytype.getAdapter();
		bodytype_pos = myAdapter2.getPosition(bodytype);
		spinner_bodytype.setSelection(bodytype_pos);
		Log.e("pos b", Integer.toString(bodytype_pos));

		ArrayAdapter myAdapter3 = (ArrayAdapter) spinner_lookingfor
				.getAdapter();
		lookingfor_pos = myAdapter3.getPosition(lookingfor);
		spinner_lookingfor.setSelection(lookingfor_pos);
		Log.e("pos l", Integer.toString(lookingfor_pos));

		ArrayAdapter myAdapter4 = (ArrayAdapter) spinner_ethnicity.getAdapter();
		ethnicity_pos = myAdapter4.getPosition(ethnicity);
		spinner_ethnicity.setSelection(ethnicity_pos);
		Log.e("pos e", Integer.toString(ethnicity_pos));

		ArrayAdapter myAdapter5 = (ArrayAdapter) spinner_smoker.getAdapter();
		smoker_pos = myAdapter5.getPosition(smoker);
		spinner_smoker.setSelection(smoker_pos);

		ArrayAdapter myAdapter6 = (ArrayAdapter) spinner_sex.getAdapter();
		sex_pos = myAdapter6.getPosition(sex);
		spinner_sex.setSelection(sex_pos);

		Log.e("pos s", Integer.toString(smoker_pos));
		uname = edt_username.getText().toString();
		Log.e("user name", uname);
		aboutme = edt_aboutme.getText().toString();
		VideoSetting setting = new VideoSetting(EditProfile.this);

	}

	public class UpdateProfileTask extends AsyncTask<Void, Void, Void> {
		public boolean status = false;
		private ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(EditProfile.this);
			pd.setMessage("Updating profile ...");
			pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			editProfile();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pd != null && pd.isShowing()) {
				pd.dismiss();
				Intent intent = new Intent(EditProfile.this, ProfileView.class);
				startActivity(intent);

			}

		}

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signup_cancel:
			finish();
			break;

		case R.id.save:
			uname = edt_username.getText().toString();
			aboutme = edt_aboutme.getText().toString();

			identity = spinner_identity.getSelectedItem().toString();
			identity_pos = spinner_identity.getSelectedItemPosition();

			height = spinner_height.getSelectedItem().toString();
			height_pos = spinner_height.getSelectedItemPosition();

			bodytype = spinner_bodytype.getSelectedItem().toString();
			bodytype_pos = spinner_bodytype.getSelectedItemPosition();

			lookingfor = spinner_lookingfor.getSelectedItem().toString();
			lookingfor_pos = spinner_lookingfor.getSelectedItemPosition();

			ethnicity = spinner_lookingfor.getSelectedItem().toString();
			ethnicity_pos = spinner_lookingfor.getSelectedItemPosition();

			smoker = spinner_smoker.getSelectedItem().toString();
			smoker_pos = spinner_smoker.getSelectedItemPosition();

			sex = spinner_sex.getSelectedItem().toString();
			sex_pos = spinner_sex.getSelectedItemPosition();
			if (c.haveNetworkConnection(getApplicationContext())) {
				new UpdateProfileTask().execute();
			} else {
				AlertDialog.Builder dialog2 = new AlertDialog.Builder(
						EditProfile.this);
				dialog2.setTitle("Instamour");
				dialog2.setMessage("No Data Connection Avaible");
				dialog2.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert2 = dialog2.create();
				alert2.show();
			}
			break;

		}
	}

	@SuppressLint("NewApi")
	public void editProfile() {
		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("m", "user-edit"));
		param.add(new BasicNameValuePair("sessionId", uid));
		param.add(new BasicNameValuePair("height", height));
		param.add(new BasicNameValuePair("identity", identity));
		param.add(new BasicNameValuePair("body_type", bodytype));
		param.add(new BasicNameValuePair("looking_for", lookingfor));
		param.add(new BasicNameValuePair("sexual_preference", sex));

		param.add(new BasicNameValuePair("smoker", smoker));
		param.add(new BasicNameValuePair("about_me", aboutme));

		JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);

		try {
			Log.e("response", "response" + json);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		this.finish();
	}
	//the directions for FlurryAnalytics call for this
			//(FLJ, 5/6/14)
			 // The Activity's onStart method
			@Override 
			protected void onStart() { // Set up the Flurry session
				super.onStart();
				FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
				
			}
			@Override
			protected void onStop() {
				super.onStop();
				FlurryAgent.onEndSession(this);
			}

}
