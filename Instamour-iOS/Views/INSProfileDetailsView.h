//
//  INSProfileDetailsView.h
//  Instamour
//
//  Created by Brian Slick on 7/16/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol INSProfileDetailsViewDelegate;

@interface INSProfileDetailsView : UIView

// Public Properties
@property (nonatomic, weak) id<INSProfileDetailsViewDelegate> delegate;

// Public Methods
+ (instancetype)profileDetailsViewWithDelegate:(id<INSProfileDetailsViewDelegate>)delegate;

- (void)setProfileDescription:(NSString *)profileDescription;
- (void)setNumberOfKisses:(NSInteger)numberOfKisses;
- (void)setComments:(NSArray *)comments;
- (void)setMoreButtonHidden:(BOOL)isHidden;

@end

@protocol INSProfileDetailsViewDelegate <NSObject>

@required

- (void)profileDetailsViewKissButtonPressed:(INSProfileDetailsView *)profileDetailsView;
- (void)profileDetailsViewCommentButtonPressed:(INSProfileDetailsView *)profileDetailsView;
- (void)profileDetailsViewAllCommentsButtonPressed:(INSProfileDetailsView *)profileDetailsView;
- (void)profileDetailsViewMoreButtonPressed:(INSProfileDetailsView *)profileDetailsView;

@end