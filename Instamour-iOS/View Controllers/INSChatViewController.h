//
//  INSChatViewController.h
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTITableViewController.h"
@class INSChatConversation;

// Public Constants

// Protocols

@interface INSChatViewController : BTITableViewController

// Public Properties
@property (nonatomic, strong) INSChatConversation *chatConversation;

// Public Methods

@end
