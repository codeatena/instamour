//
//  INSPushNotificationManager.h
//  Instamour
//
//  Created by Brian Slick on 8/3/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import <Foundation/Foundation.h>
@class INSUser;

// Public Constants

FOUNDATION_EXPORT NSString *const INSPushNotificationComponentMessageTypeKey;
FOUNDATION_EXPORT NSString *const INSPushNotificationComponentSenderIDKey;
FOUNDATION_EXPORT NSString *const INSPushNotificationComponentSenderUserNameKey;

FOUNDATION_EXPORT NSString *const INSPushNotificationTypeSomeoneKissedMe;
FOUNDATION_EXPORT NSString *const INSPushNotificationTypeNewAmour;
FOUNDATION_EXPORT NSString *const INSPushNotificationTypeHeartPush;
FOUNDATION_EXPORT NSString *const INSPushNotificationTypeMyVideoWatched;
FOUNDATION_EXPORT NSString *const INSPushNotificationTypeNewComment;
FOUNDATION_EXPORT NSString *const INSPushNotificationTypeMissedCall;
FOUNDATION_EXPORT NSString *const INSPushNotificationTypeUserBusy;
FOUNDATION_EXPORT NSString *const INSPushNotificationTypeChatMessage;

// Protocols

@interface INSPushNotificationManager : NSObject

// Public Properties

// Public Methods
+ (instancetype)sharedManager;

- (void)sendSomeoneKissedMeMessageToUser:(INSUser *)toUser;

- (void)sendNewAmourMessageToUser:(INSUser *)toUser;

- (void)sendHeartPushedMessageToUser:(INSUser *)toUser;

- (void)sendMyVideoWatchedMessageToUser:(INSUser *)toUser;

- (void)sendNewCommentMessageToUser:(INSUser *)toUser;

- (void)sendMissedCallMessageToUser:(INSUser *)toUser
                        forCallType:(enum QBVideoChatConferenceType)callType;

- (void)sendBusyCallMessageToUser:(INSUser *)toUser;

- (void)sendChatMessageMessageToUser:(INSUser *)toUser;

@end
