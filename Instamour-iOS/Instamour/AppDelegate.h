//
//  AppDelegate.h
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Quickblox/Quickblox.h>

@class UserLogger;
@class MFSideMenuContainerViewController;
@class INSBrowseProfilesViewController;
@class INSUser;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

// Public Properties
@property (nonatomic, strong) UIWindow *window;

@property (nonatomic, strong) MFSideMenuContainerViewController *sideMenuContainerViewController;
@property (nonatomic, strong) UINavigationController *navigationController;

@property (nonatomic, assign, getter = isPreviewMode) BOOL previewMode;

@property (nonatomic, strong, readonly) NSDateFormatter *serverDateFormatter;
@property (nonatomic, strong, readonly) NSDateFormatter *serverBirthdayDateFormatter;

// Public Methods

- (UIBarButtonItem *)sideMenuBarButtonItem;

// Primary View Display Methods

- (void)showLoggedOutScreen;
- (void)showLoggedInHomeScreen;
- (void)showVideoListScreenAfterSignUp;
- (void)showPreviewScreen;
- (void)showViewControllerInCenter:(UIViewController *)viewController closeMenuWhenFinished:(BOOL)isMenuClosed;

- (void)showCallingViewForUser:(INSUser *)user
            quickBloxSessionID:(NSString *)quickBloxSessionID
                      callType:(enum QBVideoChatConferenceType)callType
                  incomingCall:(BOOL)isIncomingCall;
- (void)dismissCallingView;

// Misc Methods

+ (AppDelegate *)sharedDelegate;
+ (BOOL)isiOS7OrNewer;

- (void)promptUserToLogIn;
- (void)logoutUser;
- (void)logoutFacebook;

- (void)showProgressIndicatorWithMessage:(NSString *)message;
- (void)hideProgressIndicator;

- (void)showFailureAlertWithTitle:(NSString *)title
                            error:(NSError *)error
                        orMessage:(NSString *)message;

@end
