//
//  INSChatTextTableCell.m
//  Instamour
//
//  Created by Brian Slick on 8/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatTextTableCell.h"

@implementation INSChatTextTableCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [[self messageLabel] setTextColor:[UIColor whiteColor]];
    [[self messageLabel] setFont:[UIFont systemFontOfSize:12.0]];
}

@end
