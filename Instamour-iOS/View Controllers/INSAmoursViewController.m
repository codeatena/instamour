//
//  INSAmoursViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSAmoursViewController.h"

// Models and other global

// Sub-controllers
#import "INSProfileViewController.h"
#import "INSChatViewController.h"

// Views
#import "INSYourAmoursTableCell.h"
#import "INSHeartsPushedTableCell.h"
#import "INSPendingAmoursTableCell.h"

// Private Constants

typedef NS_ENUM(NSInteger, INSAmoursViewControllerMode) {
    INSAmoursViewControllerModeYourAmours = 1,
    INSAmoursViewControllerModePendingAmours,
    INSAmoursViewControllerModeHeartsPushed
};

@interface INSAmoursViewController () <INSINSYourAmoursTableCellDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIButton *yourAmoursButton;
@property (nonatomic, strong) IBOutlet UIButton *pendingAmoursButton;
@property (nonatomic, strong) IBOutlet UIButton *heartsPushedButton;
@property (nonatomic, strong) IBOutlet UILabel *notificationLabel;

@property (nonatomic, assign) INSAmoursViewControllerMode mode;

@property (nonatomic, strong) INSUser *yourAmourUserToDelete;

@end

@implementation INSAmoursViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];
    
    if ([self mode] == 0)
    {
        [self setMode:INSAmoursViewControllerModeYourAmours];
    }
    
    UILabel *notificationLabel = [self notificationLabel];
    CALayer *notificationLabelLayer = [notificationLabel layer];
    
    [notificationLabelLayer setCornerRadius:3.0];
    [notificationLabelLayer setMasksToBounds:YES];
    [notificationLabelLayer setBorderWidth:1.0];
    [notificationLabelLayer setBorderColor:[[UIColor whiteColor] CGColor]];
    [notificationLabelLayer setShadowColor:[[UIColor clearColor] CGColor]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    [Flurry logEvent:@"Your Amours View"];
    
    // User Interface
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    NSMutableArray *contents = [self mainContents];
    NSMutableArray *pendingAmoursList = [userManager pendingAmoursUsers];
    UILabel *notificationLabel = [self notificationLabel];
    
    UIButton *activeButton = nil;
    UIColor *labelTextColor = nil;
    UIColor *labelBackgroundColor = nil;
    
    [contents removeAllObjects];
    
    switch ([self mode])
    {
        case INSAmoursViewControllerModeYourAmours:
        {
            [self setTitle:@"Your Amours"];
            
            [contents addObjectsFromArray:[userManager yourAmoursUsers]];
            activeButton = [self yourAmoursButton];
            labelTextColor = [UIColor redColor];
            labelBackgroundColor = [UIColor whiteColor];
            
            [self showEditButton];
        }
            break;
        case INSAmoursViewControllerModePendingAmours:
        {
            [self setTitle:@"Pending Amours"];
            
            [contents addObjectsFromArray:pendingAmoursList];
            activeButton = [self pendingAmoursButton];
            labelTextColor = [UIColor whiteColor];
            labelBackgroundColor = [UIColor redColor];
            
            [self showEditButton];
        }
            break;
        case INSAmoursViewControllerModeHeartsPushed:
        {
            [self setTitle:@"Hearts Pushed"];
            
            [contents addObjectsFromArray:[userManager heartsPushedUsers]];
            activeButton = [self heartsPushedButton];
            labelTextColor = [UIColor redColor];
            labelBackgroundColor = [UIColor whiteColor];
            
            [self removeRightBarButtonItem];
        }
            break;
        default:
            break;
    }
    
    [notificationLabel setTextColor:labelTextColor];
    [notificationLabel setBackgroundColor:labelBackgroundColor];
    [notificationLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[pendingAmoursList count]]];
    [notificationLabel setHidden:([pendingAmoursList count] == 0)];
    
    NSArray *allButtons = @[ [self yourAmoursButton], [self pendingAmoursButton], [self heartsPushedButton] ];
    
    for (UIButton *button in allButtons)
    {
        [button setSelected:(button == activeButton)];
    }
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSUserManagerDidChangeYourAmoursNotification
                                   selector:@selector(yourAmoursDidChangeNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSUserManagerDidChangePendingAmoursNotification
                                   selector:@selector(pendingAmoursDidChangeNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSUserManagerDidChangeHeartsPushedAmoursNotification
                                   selector:@selector(heartsPushedDidChangeNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFriendStatusDeletedDidFinishNotification
                                   selector:@selector(friendStatusDeletedDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerFriendStatusDeletedFailedNotification
                                   selector:@selector(friendStatusDeletedFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView setRowHeight:101.0];
    
    [INSYourAmoursTableCell registerNibForTableViewBTI:tableView];
    [INSPendingAmoursTableCell registerNibForTableViewBTI:tableView];
    [INSHeartsPushedTableCell registerNibForTableViewBTI:tableView];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)yourAmoursDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pendingAmoursDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)heartsPushedDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)friendStatusDeletedDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Delete Amour Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Hearts Pushed Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    INSUser *user = [self yourAmourUserToDelete];
    
    NSInteger index = NSNotFound;
    
    switch ([self mode])
    {
        case INSAmoursViewControllerModeYourAmours:
        {
            index = [[userManager yourAmoursUsers] indexOfObject:user];
            
            [[userManager yourAmoursUsers] removeObject:user];
            
            [[self mainContents] removeAllObjects];
            [[self mainContents] addObjectsFromArray:[userManager yourAmoursUsers]];
            
            [userManager saveYourAmoursUsers];
        }
            break;
        case INSAmoursViewControllerModePendingAmours:
        {
            index = [[userManager pendingAmoursUsers] indexOfObject:user];
            
            [[userManager pendingAmoursUsers] removeObject:user];
            
            [[self mainContents] removeAllObjects];
            [[self mainContents] addObjectsFromArray:[userManager pendingAmoursUsers]];
            
            [userManager savePendingAmoursUsers];
        }
        case INSAmoursViewControllerModeHeartsPushed:
        default:
            break;
    }
    
    if (index == NSNotFound)
    {
        [[self tableView] reloadData];
    }
    else
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        [[self tableView] deleteRowsAtIndexPaths:@[ indexPath ]  withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)friendStatusDeletedFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Delete Amour Failed!"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (IBAction)yourAmoursButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSAmoursViewControllerModeYourAmours];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)pendingAmoursButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSAmoursViewControllerModePendingAmours];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)heartsPushedButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSAmoursViewControllerModeHeartsPushed];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)doneButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self tableView] setEditing:NO animated:YES];
    
    [self showEditButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)editButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self tableView] setEditing:YES animated:YES];
    
    [self showDoneButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)showDoneButton
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:doneButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showEditButton
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(editButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:editButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)removeRightBarButtonItem
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self tableView] setEditing:NO animated:YES];
    
    [[self navigationItem] setRightBarButtonItem:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[self mainContents] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITableViewCell *cellToReturn = nil;
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    INSUser *amourUser = [[self mainContents] objectAtIndex:[indexPath row]];
    
    switch ([self mode])
    {
        case INSAmoursViewControllerModeYourAmours:
        {
            INSYourAmoursTableCell *yourAmoursCell = [INSYourAmoursTableCell dequeueCellFromTableViewBTI:tableView];
            [yourAmoursCell setDelegate:self];
            
            BOOL isAudioEnabled = NO;
            BOOL isVideoEnabled = NO;
            
            if ([currentUser isAudioChatEnabled])
            {
                isAudioEnabled = [amourUser isAudioChatEnabled];
            }
            
            if ([currentUser isVideoChatEnabled])
            {
                isVideoEnabled = [amourUser isVideoChatEnabled];
            }
            
            [yourAmoursCell setAudioButtonEnabled:isAudioEnabled];
            [yourAmoursCell setVideoButtonEnabled:isVideoEnabled];
            
            cellToReturn = yourAmoursCell;
        }
            break;
        case INSAmoursViewControllerModePendingAmours:
        {
            INSPendingAmoursTableCell *pendingAmoursCell = [INSPendingAmoursTableCell dequeueCellFromTableViewBTI:tableView];
            
            [pendingAmoursCell setMessage:[NSString pendingAmoursEncouragementMessageForRecipientGenderINS:[amourUser gender]]];
            
            cellToReturn = pendingAmoursCell;
        }
            break;
        case INSAmoursViewControllerModeHeartsPushed:
        {
            INSHeartsPushedTableCell *heartsPushedCell = [INSHeartsPushedTableCell dequeueCellFromTableViewBTI:tableView];
            
            [heartsPushedCell setMessage:[NSString heartsPushedSitTightMessageForRecipientGenderINS:[amourUser gender]]];
            
            cellToReturn = heartsPushedCell;
        }
            break;
        default:
        {
            cellToReturn = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"blankCell"];
        }
            break;
    }
    
    if ([cellToReturn respondsToSelector:@selector(avatarImageView)])
    {
        INSYourAmoursTableCell *cell = (INSYourAmoursTableCell *)cellToReturn;
        
        [cell setUserName:[amourUser userName]];
        
        UIImageView *avatarImageView = [cell avatarImageView];
        
        UIImage *avatarImage = [amourUser avatarImage];
        if (avatarImage == nil)
        {
            [amourUser setAvatarDownloadCompletionBlock:^(UIImage *image){
                
                [avatarImageView setImage:image];
                
            }];
            
            avatarImage = [UIImage placeholderAvatarImageINS];
        }
        else
        {
            [amourUser setAvatarDownloadCompletionBlock:nil];
        }
        
        [avatarImageView setImage:avatarImage];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cellToReturn;
}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ( ([self mode] == INSAmoursViewControllerModeYourAmours) || ([self mode] == INSAmoursViewControllerModePendingAmours) );
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        __weak INSAmoursViewController *weakSelf = self;
        
        RIButtonItem *cancelButton = [RIButtonItem item];
        [cancelButton setLabel:@"Cancel"];
        
        RIButtonItem *deleteButton = [RIButtonItem item];
        [deleteButton setLabel:@"Delete"];
        [deleteButton setAction:^{
            
            INSUser *user = [[weakSelf mainContents] objectAtIndex:[indexPath row]];
            [weakSelf setYourAmourUserToDelete:user];
            
            [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Deleting..."];
            
            [[INSServerAPIManager sharedManager] setFriendStatusDeletedForUserID:[user userID]];
            
        }];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Delete"
                                                        message:@"Are you sure you want to delete this Amour?"
                                               cancelButtonItem:cancelButton
                                               otherButtonItems:deleteButton, nil];
        [alert show];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)tableView:(UITableView *)tableView
canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ([self mode] == INSAmoursViewControllerModeYourAmours);
}

- (void)tableView:(UITableView *)tableView
moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
      toIndexPath:(NSIndexPath *)destinationIndexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    NSMutableArray *mainContents = [self mainContents];
    NSMutableArray *yourAmours = [userManager yourAmoursUsers];
    
    [yourAmours moveObjectAtIndexBTI:[sourceIndexPath row] toIndex:[destinationIndexPath row]];
    
    [mainContents removeAllObjects];
    [mainContents addObjectsFromArray:yourAmours];
    
    [userManager saveYourAmoursUsers];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDelegate Methods

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    INSUser *user = [[self mainContents] objectAtIndex:[indexPath row]];
    
    INSProfileViewController *nextViewController = [[INSProfileViewController alloc] init];
    [nextViewController setUser:user];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSINSYourAmoursTableCellDelegate Methods

- (void)yourAmoursCellChatButtonPressed:(INSYourAmoursTableCell *)yourAmoursCell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:yourAmoursCell];
    INSUser *amourUser = [[self mainContents] objectAtIndex:[indexPath row]];
    
    INSChatConversation *chatConversation = [[INSChatDataManager sharedManager] conversationForUserID:[amourUser userID]];
    
    INSChatViewController *nextViewController = [[INSChatViewController alloc] init];
    [nextViewController setChatConversation:chatConversation];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)yourAmoursCellAudioButtonPressed:(INSYourAmoursTableCell *)yourAmoursCell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:yourAmoursCell];
    INSUser *user = [[self mainContents] objectAtIndex:[indexPath row]];
    
    [[AppDelegate sharedDelegate] showCallingViewForUser:user
                                      quickBloxSessionID:nil
                                                callType:QBVideoChatConferenceTypeAudio
                                            incomingCall:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)yourAmoursCellVideoButtonPressed:(INSYourAmoursTableCell *)yourAmoursCell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:yourAmoursCell];
    INSUser *user = [[self mainContents] objectAtIndex:[indexPath row]];
    
    [[AppDelegate sharedDelegate] showCallingViewForUser:user
                                      quickBloxSessionID:nil
                                                callType:QBVideoChatConferenceTypeAudioAndVideo
                                            incomingCall:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}


@end
