//
//  UINavigationBar+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/23/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "UINavigationBar+INSAdditions.h"

@implementation UINavigationBar (INSAdditions)

- (void)applyGlobalStyleINS
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setBarStyle:UIBarStyleDefault];
    [self setTranslucent:NO];
    [self setOpaque:YES];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
