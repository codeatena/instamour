//
//  INSChatDataManager.m
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatDataManager.h"

// Models and Other Global
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

// Private Constants
NSString *const INSChatDataManagerChatConversationFileExtension = @"data";

NSString *const INSChatDataManagerDidChangeAllConversationsNotification = @"INSChatDataManagerDidChangeAllConversationsNotification";
NSString *const INSChatDataManagerDidChangeConversationNotification = @"INSChatDataManagerDidChangeConversationNotification";

NSString *const INSChatDataManagerNotificationUserInfoConversationKey = @"INSChatDataManagerNotificationUserInfoConversationKey";

NSString *const INSChatDataManagerAttachmentPrefixImage = @"(Attach file)";
NSString *const INSChatDataManagerAttachmentPrefixVideo = @"(Attach video)";

@interface INSChatDataManager ()

// Private Properties
@property (nonatomic, strong) BTITableContentsManager *sortedChatConversations;

@property (nonatomic, strong) NSMutableDictionary *allChatConversations;
// Key = INSUser.userID
// Value = INSChatConversation

@end

@implementation INSChatDataManager

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Management


#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
        [notificationCenter addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    }
    return self;
}

#pragma mark - Custom Getters and Setters

- (BTITableContentsManager *)sortedChatConversations
{
    if (_sortedChatConversations == nil)
    {
        _sortedChatConversations = [[BTITableContentsManager alloc] init];
    }
    return _sortedChatConversations;
}

- (NSMutableDictionary *)allChatConversations
{
    if (_allChatConversations == nil)
    {
        _allChatConversations = [[NSMutableDictionary alloc] init];
    }
    return _allChatConversations;
}

#pragma mark - Notification Handlers

- (void)applicationWillResignActive:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self saveAllChatConversations];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Data Saving/Loading Methods

- (void)saveAllChatConversations
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSArray *allConversations = [[self allChatConversations] allValues];
    
    for (INSChatConversation *chatConversation in allConversations)
    {
        [self saveChatConversation:chatConversation];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveChatConversation:(INSChatConversation *)chatConversation
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *userID = [chatConversation amourUserID];
    
    if (![userID isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No User ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ([[chatConversation chatMessages] count] == 0)
    {
        [self removeChatConversationForUserID:userID];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Empty conversation", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *fileName = [userID stringByAppendingPathExtension:INSChatDataManagerChatConversationFileExtension];
    NSURL *fileURL = [[INSFileUtilities chatDataDirectoryURL] URLByAppendingPathComponent:fileName];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:chatConversation];
    
    [data writeToURL:fileURL atomically:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadAllChatConversations
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSArray *allFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[INSFileUtilities chatDataDirectoryURL] path]
                                                                            error:nil];
    NSArray *chatConversationFileNames = [allFiles pathsMatchingExtensions:@[ INSChatDataManagerChatConversationFileExtension ]];

    for (NSString *path in chatConversationFileNames)
    {
        NSString *fileName = [path lastPathComponent];
        NSString *userID = [fileName stringByDeletingPathExtension];

        [self loadChatConversationForAmourUserID:userID];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loadChatConversationForAmourUserID:(NSString *)amourUserID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (![amourUserID isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No User ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *fileName = [amourUserID stringByAppendingPathExtension:INSChatDataManagerChatConversationFileExtension];
    NSURL *fileURL = [[INSFileUtilities chatDataDirectoryURL] URLByAppendingPathComponent:fileName];

    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    INSChatConversation *chatConversation = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    [self addConversation:chatConversation forUserID:[chatConversation amourUserID]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)removeChatConversationForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (![userID isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No user ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self allChatConversations] removeObjectForKey:userID];
    
    NSString *fileName = [userID stringByAppendingPathExtension:INSChatDataManagerChatConversationFileExtension];
    NSURL *fileURL = [[INSFileUtilities chatDataDirectoryURL] URLByAppendingPathComponent:fileName];

    [[NSFileManager defaultManager] removeItemAtPathBTI:[fileURL path]];

    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSChatDataManagerDidChangeAllConversationsNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Conversation Methods

- (void)refreshSortedConversations
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    BTITableContentsManager *sortedConversations = [self sortedChatConversations];
    [sortedConversations reset];
    
    if ([[self allChatConversations] count] == 0)
    {
        [self loadAllChatConversations];
    }
    
    NSArray *conversations = [[self allChatConversations] allValues];
    
    NSDateFormatter *bucketDateFormatter = [[NSDateFormatter alloc] init];
    [bucketDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [bucketDateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [bucketDateFormatter setDoesRelativeDateFormatting:YES];
    
    NSMutableDictionary *buckets = [NSMutableDictionary dictionary];
    
    for (INSChatConversation *conversation in conversations)
    {
        NSDate *date = [[conversation mostRecentChatMessage] dateSent];
        NSString *bucketKey = [bucketDateFormatter stringFromDate:date];
        
        BTITableSectionInfo *sectionInfo = [buckets objectForKey:bucketKey];
        if (sectionInfo == nil)
        {
            sectionInfo = [sortedConversations dequeueReusableSectionInfo];
            [sectionInfo setHeaderTitle:bucketKey];
            [buckets setObject:sectionInfo forKey:bucketKey];
        }
        
        BTITableRowInfo *rowInfo = [sortedConversations dequeueReusableRowInfo];
        [rowInfo setRepresentedObject:conversation];
        
        [sectionInfo addRowsObject:rowInfo];
        [sectionInfo setRepresentedObject:date];
    }
    
    NSSortDescriptor *bucketSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"representedObject" ascending:NO];
    NSArray *sortedBuckets = [[buckets allValues] sortedArrayUsingDescriptors:@[ bucketSortDescriptor ]];
    
    NSSortDescriptor *conversationSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"representedObject.mostRecentChatMessage.dateSent" ascending:NO];
    
    for (BTITableSectionInfo *sectionInfo in sortedBuckets)
    {
        [sectionInfo sortRowsUsingDescriptors:@[ conversationSortDescriptor ]];
        
        [sortedConversations addSectionInfo:sectionInfo];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSChatDataManagerDidChangeAllConversationsNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (NSInteger)numberOfUnreadMessages
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([[self allChatConversations] count] == 0)
    {
        [self loadAllChatConversations];
    }
    
    NSInteger unreadMessageTotal = 0;
    
    NSArray *allConversations = [[self allChatConversations] allValues];
    
    for (INSChatConversation *conversation in allConversations)
    {
        unreadMessageTotal += [conversation numberOfUnreadMessages];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return unreadMessageTotal;
}

//- (void)loadTestData
//{
//    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
//    
//    INSUser *thor = [[INSUser alloc] init];
//    [thor setUserName:@"Thor"];
//    [thor setUserID:@"12345"];
//    
//    INSUser *captainAmerica = [[INSUser alloc] init];
//    [captainAmerica setUserName:@"CaptainAmerica"];
//    [captainAmerica setUserID:@"23456"];
//    
//    INSUser *ironman = [[INSUser alloc] init];
//    [ironman setUserName:@"IronMan"];
//    [ironman setUserID:@"34567"];
//
//    INSUser *hulk = [[INSUser alloc] init];
//    [hulk setUserName:@"Hulk"];
//    [hulk setUserID:@"45678"];
//    
//    INSChatConversation *conversation = nil;
//    INSChatMessage *chatMessage = nil;
//
//    NSDate *rightNow = [NSDate date];
//    
//    // Thor
//    
//    conversation = [[INSChatConversation alloc] init];
//    [conversation setAmourUser:thor];
//    [conversation setAmourUserID:[thor userID]];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingHours:2]];
//    [chatMessage setMessageBody:@"Have a care how you speak! Loki is beyond reason, but he is of Asgard and he is my brother!"];
//    [chatMessage setUnread:NO];
//    [chatMessage setToCurrentUser:YES];
//
//    [conversation addChatMessage:chatMessage];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingHours:1]];
//    [chatMessage setMessageBody:@"He killed eighty people in two days."];
//    [chatMessage setUnread:NO];
//    [chatMessage setToCurrentUser:NO];
//    
//    [conversation addChatMessage:chatMessage];
//
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:rightNow];
//    [chatMessage setMessageBody:@"He's adopted."];
//    [chatMessage setUnread:NO];
//    [chatMessage setToCurrentUser:YES];
//
//    [conversation addChatMessage:chatMessage];
//
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeImage];
//    [chatMessage setDateSent:[NSDate date]];
//    [chatMessage setToCurrentUser:YES];
//    [chatMessage setUnread:YES];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeImage];
//    [chatMessage setDateSent:[NSDate date]];
//    [chatMessage setToCurrentUser:NO];
//    [chatMessage setUnread:NO];
//    [chatMessage setUploading:YES];
//    
//    [conversation addChatMessage:chatMessage];
//
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeVideo];
//    [chatMessage setDateSent:[NSDate date]];
//    [chatMessage setToCurrentUser:YES];
//    [chatMessage setUnread:YES];
//    [chatMessage setDownloading:YES];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeVideo];
//    [chatMessage setDateSent:[NSDate date]];
//    [chatMessage setToCurrentUser:NO];
//    [chatMessage setUnread:NO];
//    
//    [conversation addChatMessage:chatMessage];
//
//    [self addConversation:conversation forUserID:[conversation amourUserID]];
//    
//    // Captain America
//    
//    conversation = [[INSChatConversation alloc] init];
//    [conversation setAmourUser:captainAmerica];
//    [conversation setAmourUserID:[captainAmerica userID]];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingHours:27]];
//    [chatMessage setMessageBody:@"Always a way out... You know, you may not be a threat, but you better stop pretending to be a hero."];
//    [chatMessage setToCurrentUser:YES];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingHours:26]];
//    [chatMessage setMessageBody:@"A hero? Like you? You're a lab rat, Rogers. Everything special about you came out of a bottle!"];
//    [chatMessage setToCurrentUser:NO];
//    [chatMessage setUnread:NO];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingHours:25]];
//    [chatMessage setMessageBody:@"Put on the suit. Let's go a few rounds."];
//    [chatMessage setToCurrentUser:YES];
//    [chatMessage setUnread:NO];
//    
//    [self addConversation:conversation forUserID:[conversation amourUserID]];
//
//    // Iron Man
//    
//    conversation = [[INSChatConversation alloc] init];
//    [conversation setAmourUser:ironman];
//    [conversation setAmourUserID:[ironman userID]];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingDays:7]];
//    [chatMessage setMessageBody:@"Phil! Come in."];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingDays:6]];
//    [chatMessage setMessageBody:@"\"Phil?\" Uh, his first name is \"Agent.\""];
//    [chatMessage setUnread:YES];
//    [chatMessage setToCurrentUser:YES];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeImage];
//    [chatMessage setToCurrentUser:YES];
//    [chatMessage setUnread:YES];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    [self addConversation:conversation forUserID:[conversation amourUserID]];
//
//    // Hulk
//    
//    conversation = [[INSChatConversation alloc] init];
//    [conversation setAmourUser:hulk];
//    [conversation setAmourUserID:[hulk userID]];
//    
//    chatMessage = [[INSChatMessage alloc] init];
//    [chatMessage setMessageType:INSChatMessageTypeText];
//    [chatMessage setDateSent:[rightNow dateBySubtractingHours:1]];
//    [chatMessage setMessageBody:@"Puny god"];
//    [chatMessage setUnread:YES];
//    [chatMessage setToCurrentUser:YES];
//    
//    [conversation addChatMessage:chatMessage];
//    
//    [self addConversation:conversation forUserID:[conversation amourUserID]];
//
//    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
//}

- (INSChatConversation *)conversationForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSMutableDictionary *allChatConversations = [self allChatConversations];
    
    INSChatConversation *chatConversation = [allChatConversations objectForKey:userID];
    
    if (chatConversation == nil)
    {
        INSUser *amourUser = [[INSUserManager sharedManager] amourWithUserID:userID];
        
        if (amourUser == nil)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Amour not found", self, __PRETTY_FUNCTION__);
            return nil;
        }
        
        chatConversation = [[INSChatConversation alloc] init];
        [chatConversation setAmourUserID:userID];
        [chatConversation setAmourUser:amourUser];
        
        [allChatConversations setObject:chatConversation forKey:userID];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return chatConversation;
}

- (void)addConversation:(INSChatConversation *)conversation
              forUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (conversation == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No Conversation", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self allChatConversations] setObject:conversation forKey:userID];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Incoming Message Methods

- (void)didReceiveQuickBloxChatMessage:(QBChatMessage *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
//    NSLog(@"message: %@", message);
    
//    NSString *messageID = [message ID];
    NSString *messageBody = [message text];
//    NSString *senderID = [NSString stringWithFormat:@"%d", [message senderID]];
//    NSString *recipientID = [NSString stringWithFormat:@"%d", [message recipientID]];
    NSDate *dateStamp = [message datetime];
    
//    NSLog(@"messageID: %@", messageID);
//    NSLog(@"messageBody: %@", messageBody);
//    NSLog(@"senderID: %@", senderID);
//    NSLog(@"recipientID: %@", recipientID);
//    NSLog(@"dateStamp: %@", dateStamp);
    
    if (![messageBody isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No message body", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *amourUser = [[INSUserManager sharedManager] amourWithQuickBloxID:[message senderID]];
    
    if (amourUser == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Unknown sender", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSChatConversation *chatConversation = [self conversationForUserID:[amourUser userID]];
    
    INSChatMessage *chatMessage = [[INSChatMessage alloc] init];
    
    [chatMessage setQuickBloxChatMessage:message];
    [chatMessage setAmourUserID:[amourUser userID]];
    [chatMessage setDateSent:dateStamp];
    [chatMessage setToCurrentUser:YES];
    [chatMessage setUnread:YES];
    
    if ([messageBody hasPrefix:INSChatDataManagerAttachmentPrefixImage])
    {
        NSLog(@"Image Message");
        [chatMessage setMessageType:INSChatMessageTypeImage];

        NSString *urlString = [messageBody stringByReplacingOccurrencesOfString:INSChatDataManagerAttachmentPrefixImage withString:@""];
        NSURL *url = [NSURL URLWithString:urlString];
        
        [chatMessage setImageFileName:[urlString lastPathComponent]];
        [chatMessage setDownloading:YES];

//        NSLog(@"urlString: %@", urlString);
//        NSLog(@"url: %@", url);
        
        INSServerAPIManager *serverManager = [INSServerAPIManager sharedManager];
        
        [serverManager downloadImageFromURL:url
                                    success:^(UIImage *image) {
                                        
                                        NSLog(@"Image downloaded");
                                        
                                        UIImage *thumbnailImage = [image chatThumbnailImageINS];
                                        
                                        [chatMessage setImage:image];
                                        [chatMessage setThumbnailImage:thumbnailImage];
                                        
                                        [chatMessage saveImage];
                                        [chatMessage saveThumbnailImage];
                                        
                                        [chatMessage setDownloading:NO];

                                        [self postRefreshNotificationForChatConversation:chatConversation];

                                    }
                                    failure:^(NSString *errorMessage) {
                                        
                                        NSLog(@"Image download failed");
                                        [chatMessage setDownloading:NO];
                                        
                                        [self postRefreshNotificationForChatConversation:chatConversation];

                                    }];
    }
    else if ([messageBody hasPrefix:INSChatDataManagerAttachmentPrefixVideo])
    {
        NSLog(@"Video Message");
        [chatMessage setMessageType:INSChatMessageTypeVideo];
        
        NSString *urlString = [messageBody stringByReplacingOccurrencesOfString:INSChatDataManagerAttachmentPrefixVideo withString:@""];
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSString *fileName = [urlString lastPathComponent];
        NSURL *targetURL = [[INSFileUtilities videoDirectoryURL] URLByAppendingPathComponent:fileName];
    
        [chatMessage setVideoURL:url];
        [chatMessage setVideoFileName:fileName];
        [chatMessage setDownloading:YES];
        
        INSServerAPIManager *serverManager = [INSServerAPIManager sharedManager];

        [serverManager downloadFileFromURL:url
                                 toFileURL:targetURL
                                   success:^{
                                       
                                       [chatMessage setDownloading:NO];
                                       
                                       [self generateVideoThumbnailForChatMessage:chatMessage];

                                       [self postRefreshNotificationForChatConversation:chatConversation];

                                   }
                                   failure:^(NSString *errorMessage) {
                                       
                                       [chatMessage setDownloading:NO];

                                       [self postRefreshNotificationForChatConversation:chatConversation];

                                   }];
        
    }
    else
    {
        NSLog(@"Text Message");
        [chatMessage setMessageType:INSChatMessageTypeText];

        [chatMessage setMessageBody:messageBody];
    }
    
    [chatConversation addChatMessage:chatMessage];
    NSLog(@"chatMessage: %@", chatMessage);
    
    [self refreshSortedConversations];

    [self postRefreshNotificationForChatConversation:chatConversation];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSChatDataManagerDidChangeAllConversationsNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)postRefreshNotificationForChatConversation:(INSChatConversation *)chatConversation
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = @{ INSChatDataManagerNotificationUserInfoConversationKey : chatConversation };
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSChatDataManagerDidChangeConversationNotification
                                                                     userInfo:userInfo];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSChatDataManagerDidChangeAllConversationsNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)generateVideoThumbnailForChatMessage:(INSChatMessage *)chatMessage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSString *fileName = [chatMessage videoFileName];
    
    NSURL *url = [[INSFileUtilities videoDirectoryURL] URLByAppendingPathComponent:fileName];
    
    [chatMessage setGeneratingThumbnail:YES];

    // http://stackoverflow.com/questions/19105721/thumbnailimageattime-now-deprecated-whats-the-alternative
    
    AVURLAsset *urlAsset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *assetImageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:urlAsset];
    [assetImageGenerator setAppliesPreferredTrackTransform:YES];

    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [assetImageGenerator copyCGImageAtTime:time actualTime:NULL error:&err];
    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];

    UIImage *thumbnailImage = [image chatThumbnailImageINS];
    [chatMessage setThumbnailImage:thumbnailImage];
    
    [chatMessage saveThumbnailImage];

    [chatMessage setGeneratingThumbnail:NO];

    [self postRefreshNotificationForChatConversation:[chatMessage chatConversation]];
        
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Outgoing Message Methods

- (void)quickBloxChatMessageDidFailToSend:(QBChatMessage *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // TODO:
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendChatToUser:(INSUser *)toUser
           textMessage:(NSString *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *toUserID = [toUser userID];
    
    INSChatConversation *chatConversation = [self conversationForUserID:toUserID];
    
    INSChatMessage *chatMessage = [[INSChatMessage alloc] init];
    [chatMessage setAmourUserID:toUserID];
    [chatMessage setDateSent:[NSDate date]];
    [chatMessage setToCurrentUser:NO];
    [chatMessage setMessageType:INSChatMessageTypeText];
    [chatMessage setMessageBody:message];
    [chatMessage setUnread:NO];
    
    [chatConversation addChatMessage:chatMessage];
    
    QBChatMessage *quickBloxChatMessage = [[QBChatMessage alloc] init];
    [quickBloxChatMessage setSenderID:[[[[INSUserManager sharedManager] currentUser] quickBloxID] integerValue]];
    [quickBloxChatMessage setRecipientID:[[toUser quickBloxID] integerValue]];
    [quickBloxChatMessage setText:[chatMessage messageBody]];
    [quickBloxChatMessage setDatetime:[chatMessage dateSent]];
    
    [chatMessage setQuickBloxChatMessage:quickBloxChatMessage];
    
    [[QBChat instance] sendMessage:quickBloxChatMessage];
    [[INSPushNotificationManager sharedManager] sendChatMessageMessageToUser:toUser];
    
    [self playMessageSentSound];
    [self postRefreshNotificationForChatConversation:chatConversation];

    [self saveChatConversation:chatConversation];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendChatToUser:(INSUser *)toUser
                 image:(UIImage *)image
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *thumbnailImage = [image chatThumbnailImageINS];
    
    NSString *toUserID = [toUser userID];
    
    INSChatConversation *chatConversation = [self conversationForUserID:toUserID];
    
    INSChatMessage *chatMessage = [[INSChatMessage alloc] init];
    [chatMessage setAmourUserID:toUserID];
    [chatMessage setDateSent:[NSDate date]];
    [chatMessage setToCurrentUser:NO];
    [chatMessage setMessageType:INSChatMessageTypeImage];
    [chatMessage setImage:image];
    [chatMessage setThumbnailImage:thumbnailImage];
    [chatMessage setUnread:NO];
    [chatMessage setUploading:YES];
    
    [chatConversation addChatMessage:chatMessage];

    [self postRefreshNotificationForChatConversation:chatConversation];

    INSServerAPIManager *serverManager = [INSServerAPIManager sharedManager];
    [serverManager uploadChatImage:image
                           success:^(NSString *urlString) {
                               
                               NSString *fileName = [urlString lastPathComponent];
                               
//                               NSLog(@"urlString: %@", urlString);
//                               NSLog(@"fileName: %@", fileName);
                               
                               [chatMessage setImageFileName:fileName];
                               [chatMessage setUploading:NO];
                               
                               [chatMessage saveImage];
                               [chatMessage saveThumbnailImage];
                               
                               NSString *messageBody = [INSChatDataManagerAttachmentPrefixImage stringByAppendingString:urlString];
                               
//                               NSLog(@"Image upload succeeded: %@", fileName);
//                               NSLog(@"Chat message: %@", messageBody);
                               
                               QBChatMessage *quickBloxChatMessage = [[QBChatMessage alloc] init];
                               [quickBloxChatMessage setSenderID:[[[[INSUserManager sharedManager] currentUser] quickBloxID] integerValue]];
                               [quickBloxChatMessage setRecipientID:[[toUser quickBloxID] integerValue]];
                               [quickBloxChatMessage setText:messageBody];
                               [quickBloxChatMessage setDatetime:[chatMessage dateSent]];
                               
                               [chatMessage setQuickBloxChatMessage:quickBloxChatMessage];
                               
                               [[QBChat instance] sendMessage:quickBloxChatMessage];
                               [[INSPushNotificationManager sharedManager] sendChatMessageMessageToUser:toUser];
                               
                               [self playMessageSentSound];
                               [self postRefreshNotificationForChatConversation:chatConversation];
                               
                               [self saveChatConversation:chatConversation];

                           }
                           failure:^(NSString *errorMessage) {
                               
//                               NSLog(@"Image upload failed: %@", errorMessage);
                               
                               // TODO: Handle upload error
                               [chatMessage setUploading:NO];
                               
                               [self postRefreshNotificationForChatConversation:chatConversation];
                               
                           }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)sendChatToUser:(INSUser *)toUser
          videoFileURL:(NSURL *)fileURL
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *toUserID = [toUser userID];
    
    INSChatConversation *chatConversation = [self conversationForUserID:toUserID];

    INSChatMessage *chatMessage = [[INSChatMessage alloc] init];
    [chatMessage setAmourUserID:toUserID];
    [chatMessage setDateSent:[NSDate date]];
    [chatMessage setToCurrentUser:NO];
    [chatMessage setMessageType:INSChatMessageTypeVideo];
    [chatMessage setUnread:NO];
    [chatMessage setUploading:YES];
    
    [chatConversation addChatMessage:chatMessage];
    
    [self postRefreshNotificationForChatConversation:chatConversation];

    INSServerAPIManager *serverManager = [INSServerAPIManager sharedManager];
    [serverManager uploadChatVideoAtURL:fileURL
                                success:^(NSString *urlString) {
                                    
                                    NSURL *url = [NSURL URLWithString:urlString];
                                    
                                    NSString *fileName = [url lastPathComponent];
                                    
                                    NSURL *newLocalFileURL = [[INSFileUtilities videoDirectoryURL] URLByAppendingPathComponent:fileName];
                                    
                                    [[NSFileManager defaultManager] moveItemAtURL:fileURL
                                                                            toURL:newLocalFileURL
                                                                            error:nil];
                                    
                                    [chatMessage setVideoFileName:fileName];
                                    [chatMessage setVideoURL:url];
                                    [chatMessage setUploading:NO];

                                    NSString *messageBody = [INSChatDataManagerAttachmentPrefixVideo stringByAppendingString:urlString];
                                    
//                                    NSLog(@"Image upload succeeded: %@", urlString);
//                                    NSLog(@"Chat message: %@", messageBody);
                                    
                                    QBChatMessage *quickBloxChatMessage = [[QBChatMessage alloc] init];
                                    [quickBloxChatMessage setSenderID:[[[[INSUserManager sharedManager] currentUser] quickBloxID] integerValue]];
                                    [quickBloxChatMessage setRecipientID:[[toUser quickBloxID] integerValue]];
                                    [quickBloxChatMessage setText:messageBody];
                                    [quickBloxChatMessage setDatetime:[chatMessage dateSent]];
                                    
                                    [chatMessage setQuickBloxChatMessage:quickBloxChatMessage];
                                    
                                    [[QBChat instance] sendMessage:quickBloxChatMessage];
                                    [[INSPushNotificationManager sharedManager] sendChatMessageMessageToUser:toUser];

                                    [self generateVideoThumbnailForChatMessage:chatMessage];

                                    [self playMessageSentSound];
                                    [self postRefreshNotificationForChatConversation:chatConversation];
                                    
                                    [self saveChatConversation:chatConversation];

                                }
                                failure:^(NSString *errorMessage) {
                                    
                                    // TODO: Handle upload error
                                    [chatMessage setUploading:NO];
                                    
                                    [self postRefreshNotificationForChatConversation:chatConversation];

                                }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Sound Methods

- (void)playMessageReceivedSound
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self playSoundWithFileName:@"messageReceived" extension:@"aiff"];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)playMessageSentSound
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self playSoundWithFileName:@"messageSent" extension:@"aiff"];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)playSoundWithFileName:(NSString *)fileName
                    extension:(NSString *)fileExtension
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileExtension];
    
    if (fileURL == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - File not found", self, __PRETTY_FUNCTION__);
        return;
    }
    
    SystemSoundID audioEffect;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)fileURL, &audioEffect);
    AudioServicesPlaySystemSound(audioEffect);

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Singleton

+ (instancetype)sharedManager
{
	static dispatch_once_t pred;
	static INSChatDataManager *shared = nil;
    
	dispatch_once(&pred, ^{
        
		shared = [[[self class] alloc] init];
        
	});
    
	return shared;
}

@end
