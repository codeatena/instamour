//
//  INSImageEditorViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/20/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSImageEditorViewController.h"

// Models and other global

// Sub-controllers

// Views

// Private Constants

@interface INSImageEditorViewController ()

// Private Properties

@end

@implementation INSImageEditorViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.minimumScale = 0.2;
        self.maximumScale = 10;
        self.scaleEnabled = YES;
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return self;
}

#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides


#pragma mark - Notification Handlers


#pragma mark - UI Response Methods


#pragma mark - Misc Methods

@end
