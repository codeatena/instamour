//
//  INSVideoListTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSVideoListTableCell.h"

@interface INSVideoListTableCell ()

// Private Properties
@property (nonatomic, strong) IBOutlet UIImageView *thumbnailImageView;
@property (nonatomic, strong) IBOutlet UIButton *recordButton;
@property (nonatomic, strong) IBOutlet UIButton *uploadButton;
@property (nonatomic, strong) IBOutlet UIButton *playButton;
@property (nonatomic, strong) IBOutlet UIButton *deleteButton;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) IBOutlet UILabel *uploadingLabel;

@end

@implementation INSVideoListTableCell

#pragma mark - UI Response Methods

- (IBAction)recordButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([[self delegate] respondsToSelector:@selector(videoListTableCellRecordButtonPressed:)])
    {
        [[self delegate] videoListTableCellRecordButtonPressed:self];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)uploadButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(videoListTableCellUploadButtonPressed:)])
    {
        [[self delegate] videoListTableCellUploadButtonPressed:self];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)playButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(videoListTableCellPlayButtonPressed:)])
    {
        [[self delegate] videoListTableCellPlayButtonPressed:self];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)deleteButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([[self delegate] respondsToSelector:@selector(videoListTableCellDeleteButtonPressed:)])
    {
        [[self delegate] videoListTableCellDeleteButtonPressed:self];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setThumbnailImage:(UIImage *)image
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (image == nil)
    {
        image = [UIImage imageNamed:@"video-thumbnail"];
    }
    
    [[self thumbnailImageView] setImage:image];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setRecordButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self recordButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setUploadButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self uploadButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setPlayButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self playButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setDeleteButtonEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self deleteButton] setEnabled:isEnabled];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setVideoInfoState:(INSVideoInfoState)state
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    UILabel *statusLabel = [self uploadingLabel];
    UIActivityIndicatorView *activityIndicator = [self activityIndicatorView];
    UIButton *recordButton = [self recordButton];
    UIButton *uploadButton = [self uploadButton];
    UIButton *playButton = [self playButton];
    UIButton *deleteButton = [self deleteButton];
    
    switch (state)
    {
        case INSVideoInfoStateTrimming:
        case INSVideoInfoStateUploading:
        case INSVideoInfoStateDeleting:
        {
            if (state == INSVideoInfoStateTrimming)
            {
                [statusLabel setText:@"Trimming..."];
            }
            else if (state == INSVideoInfoStateUploading)
            {
                [statusLabel setText:@"Uploading..."];
            }
            else if (state == INSVideoInfoStateDeleting)
            {
                [statusLabel setText:@"Deleting..."];
            }
            [activityIndicator setHidden:NO];
            [activityIndicator startAnimating];
            [statusLabel setHidden:NO];
            [recordButton setHidden:YES];
            [uploadButton setHidden:YES];
            [playButton setHidden:YES];
            [deleteButton setHidden:YES];
        }
            break;
        INSVideoInfoStateNormal:
        default:
        {
            [activityIndicator setHidden:YES];
            [activityIndicator stopAnimating];
            [statusLabel setHidden:YES];
            [recordButton setHidden:NO];
            [uploadButton setHidden:NO];
            [playButton setHidden:NO];
            [deleteButton setHidden:NO];
        }
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}



@end
