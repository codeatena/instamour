//
//  INSChatAmoursViewController.h
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTITableViewController.h"

// Public Constants

// Protocols

@interface INSChatAmoursViewController : BTITableViewController

// Public Properties

// Public Methods

@end
