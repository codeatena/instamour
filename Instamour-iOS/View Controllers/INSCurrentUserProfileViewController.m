//
//  ProfileViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/17/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSCurrentUserProfileViewController.h"

// Models and other global
#import <MediaPlayer/MediaPlayer.h>
#import "INSActivityItemProvider.h"

// Sub-controllers
#import "INSSocialMediaViewController.h"
#import "INSEditProfileViewController.h"
#import "INSCommentsViewController.h"
#import "INSImageEditorViewController.h"

// Views
#import "INSProfileDetailsView.h"

@interface INSCurrentUserProfileViewController () <INSProfileDetailsViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UIImageView *cameraIconImageView;
@property (nonatomic, strong) IBOutlet UIButton *playButton;

@property (nonatomic, strong) IBOutlet UIButton *editPhotoButton;
@property (nonatomic, strong) IBOutlet UIButton *editInfoButton;

@property (nonatomic, strong) INSProfileDetailsView *profileDetailsView;

@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;

@end

@implementation INSCurrentUserProfileViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters

- (INSProfileDetailsView *)profileDetailsView
{
    if (_profileDetailsView == nil)
    {
        _profileDetailsView = [INSProfileDetailsView profileDetailsViewWithDelegate:self];
    }
    return _profileDetailsView;
}

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shareButton"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(shareButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:shareButton];
    
    // User Interface
    
    [[self cameraIconImageView] setImage:[UIImage playButtonImageINS]];
    
    [self arrangeSubviews];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    
    
    // User Interface
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidAppear:animated];
    
    [[self scrollView] flashScrollIndicators];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillDisappear:animated];
    
    [[self moviePlayerController] stop];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    [self setTitle:[currentUser userName]];
    
    // Image / Video
    
    // Only download the image once.
    
    UIImage *avatarImage = [currentUser avatarImage];
    if (avatarImage == nil)
    {
        __weak INSCurrentUserProfileViewController *weakSelf = self;
        
        [currentUser setAvatarDownloadCompletionBlock:^(UIImage *image){
            
            [[weakSelf avatarImageView] setImage:image];
            
        }];
        
        avatarImage = [UIImage placeholderAvatarImageINS];
    }
    else
    {
        [currentUser setAvatarDownloadCompletionBlock:nil];
    }
    
    [[self avatarImageView] setImage:avatarImage];
    
    // Details
    
    INSProfileDetailsView *detailsView = [self profileDetailsView];
    
    NSMutableArray *components = [NSMutableArray array];
    
    NSDate *dateOfBirth = [currentUser dateOfBirth];
    if (dateOfBirth != nil)
    {
        NSInteger age = [dateOfBirth numberOfYearsOldINS];
        
        [components addObject:[NSString stringWithFormat:@"%ld", (long)age]];
    }
    
    [components safelyAddPopulatedStringINS:[currentUser height]];
    [components safelyAddPopulatedStringINS:[currentUser identity]];
    [components safelyAddPopulatedStringINS:[currentUser bodyType]];
    [components safelyAddPopulatedStringINS:[currentUser lookingFor]];
    [components safelyAddPopulatedStringINS:[currentUser ethnicity]];
    [components safelyAddPopulatedStringINS:[currentUser sexualPreference]];
    [components safelyAddPopulatedStringINS:[currentUser city]];
    
    NSString *profileDescription = [components componentsJoinedByString:@" \\ "];
    
    NSArray *kisses = [NSArray arrayWithArray:[currentUser kisses]];
    NSArray *comments = [NSArray arrayWithArray:[currentUser comments]];
    
    [detailsView setProfileDescription:profileDescription];
    [detailsView setNumberOfKisses:[kisses count]];
    [detailsView setComments:comments];
    
    
    NSString *instagramName = [currentUser instagramName];
    NSString *vineName = [currentUser vineName];
    
    [detailsView setMoreButtonHidden:( ![instagramName isNotEmpty] && ![vineName isNotEmpty] )];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:MFSideMenuStateNotificationEvent
                                   selector:@selector(sideMenuStateDidChangeNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerKissAddDidFinishNotification
                                   selector:@selector(kissAddDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerKissAddFailedNotification
                                   selector:@selector(kissAddFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)sideMenuStateDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSNumber *stateNumber = [userInfo objectForKey:@"eventType"];
    MFSideMenuStateEvent event = [stateNumber intValue];
    
    if (event == MFSideMenuStateEventMenuWillOpen)
    {
        [[self moviePlayerController] pause];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)kissAddDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Add Kiss Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Kiss Add Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    NSArray *kisses = [currentUser kisses];
    
    [[self profileDetailsView] setNumberOfKisses:[kisses count] + 1];
    
    [[INSPushNotificationManager sharedManager] sendSomeoneKissedMeMessageToUser:currentUser];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Kiss"
                                                    message:@"Kiss added successfully!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)kissAddFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Add Kiss Failed!"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moviePlayBackDidFinish:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    [[player view] removeFromSuperview];
    
    [[self avatarImageView] setHidden:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moviePlaybackStateDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *player = [notification object];
    
    switch ([player playbackState])
    {
        case MPMoviePlaybackStatePlaying:
        {
            [[self cameraIconImageView] setImage:[UIImage pauseButtonImageINS]];
        }
            break;
        default:
        {
            [[self cameraIconImageView] setImage:[UIImage playButtonImageINS]];
        }
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)shareButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    
    if (![[INSVideoDataManager sharedManager] isAnyVideoPopulated])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Create a video so you can share it!"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No image", self, __PRETTY_FUNCTION__);
        return;
    }
    
    UIImage *profileImage = [user avatarImage];
    
    // http://www.instamour.com/profile/UNAME
    
    NSString *userName = [user userName];
    NSString *urlString = [NSString stringWithFormat:@"http://www.instamour.com/profile/%@", userName];
    
    //    NSString *text = @"Check out my profile on Instamour.com!";
    ////    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/instamour/id710173306?mt=8"];
    //    NSURL *url = [NSURL URLWithString:urlString];
    //
    //    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[ text, url, profileImage ]
    //                                                                                         applicationActivities:nil];
    //    [activityViewController setExcludedActivityTypes:@[ UIActivityTypeMessage ]];
    //
    //    [self presentViewController:activityViewController
    //                       animated:YES
    //                     completion:nil];
    
    INSActivityItemProvider *activityItemProvider = [[INSActivityItemProvider alloc] init];
    [activityItemProvider setCheckOutThisThingText:@"Check out my profile on"];
    [activityItemProvider setUrlString:urlString];
    
    NSMutableArray *activityItems = [NSMutableArray array];
    [activityItems addObject:activityItemProvider];
    if (profileImage != nil)
    {
        [activityItems addObject:profileImage];
    }
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                                         applicationActivities:nil];
    
    [self presentViewController:activityViewController
                       animated:YES
                     completion:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)editPhotoButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    __weak INSCurrentUserProfileViewController *weakSelf = self;
    
	void(^showImagePicker)(UIImagePickerControllerSourceType sourceType) = ^(UIImagePickerControllerSourceType sourceType) {
        
		UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
		[imagePicker setMediaTypes:@[(NSString *)kUTTypeImage]];
		[imagePicker setDelegate:weakSelf];
		[imagePicker setSourceType:sourceType];
        [imagePicker setAllowsEditing:NO];
        
        [weakSelf presentViewController:imagePicker
                               animated:YES
                             completion:nil];
        
	};
    
    NSMutableArray *buttons = [NSMutableArray array];
    
	RIButtonItem *buttonItem = nil;
    
	// Pick from library
    
	buttonItem = [RIButtonItem item];
	[buttonItem setLabel:@"Choose Photo"];
	[buttonItem setAction:^{
        
		showImagePicker(UIImagePickerControllerSourceTypePhotoLibrary);
        
	}];
    
	[buttons addObject:buttonItem];
    
	// Pick from camera
    
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
		buttonItem = [RIButtonItem item];
		[buttonItem setLabel:@"Take Photo"];
		[buttonItem setAction:^{
            
			showImagePicker(UIImagePickerControllerSourceTypeCamera);
            
		}];
        
		[buttons addObject:buttonItem];
	}
    
    // Cancel
    
	buttonItem = [RIButtonItem item];
	[buttonItem setLabel:@"Cancel"];
    
	[buttons addObject:buttonItem];
    
	// Action Sheet
    
	UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
											   cancelButtonItem:nil
										  destructiveButtonItem:nil
											   otherButtonItems:nil];
    
	NSInteger cancelIndex = -1;
	for (RIButtonItem *aButton in buttons)
	{
		cancelIndex = [sheet addButtonItem:aButton];
	}
    
	[sheet setCancelButtonIndex:cancelIndex];
    
    [sheet showInView:[self view]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)editInfoButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSEditProfileViewController *nextViewController = [[INSEditProfileViewController alloc] init];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)playButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *moviePlayer = [self moviePlayerController];
    
    if (moviePlayer != nil)
    {
        if ([moviePlayer playbackState] == MPMoviePlaybackStatePlaying)
        {
            [moviePlayer pause];
        }
        else
        {
            [moviePlayer play];
        }
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Movie already loaded", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSVideoInfo *videoInfo = [[INSVideoDataManager sharedManager] mergeVideo];
    if ([videoInfo isPopulated])
    {
        [self playVideoForVideoInfo:videoInfo];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)arrangeSubviews
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // NOTE: The avatar image view is supposed to be square!
    
    UIScrollView *scrollView = [self scrollView];
    UIImageView *avatarImageView = [self avatarImageView];
    INSProfileDetailsView *detailsView = [self profileDetailsView];
    
    CGRect detailsViewFrame = [detailsView frame];
    
    detailsViewFrame.origin.y = CGRectGetMaxY([avatarImageView frame]);
    [detailsView setFrame:detailsViewFrame];
    [scrollView addSubview:detailsView];
    
    [scrollView setContentSize:CGSizeMake(CGRectGetWidth([avatarImageView frame]), CGRectGetMaxY(detailsViewFrame))];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)playVideoForVideoInfo:(INSVideoInfo *)videoInfo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    MPMoviePlayerController *moviePlayer = [self moviePlayerController];
    
    if (moviePlayer != nil)
    {
        [moviePlayer stop];
        [[moviePlayer view] removeFromSuperview];
        [notificationCenter removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
        [notificationCenter removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:moviePlayer];
    }
    
    [[self cameraIconImageView] setHidden:YES];
    
    NSURL *url = [videoInfo serverVideoFileURL];
    
    if (url == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No URL available", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self cameraIconImageView] setHidden:NO];
    
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    [self setMoviePlayerController:moviePlayer];
    
    [notificationCenter addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    [notificationCenter addObserver:self selector:@selector(moviePlaybackStateDidChangeNotification:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    
    [moviePlayer setMovieSourceType:MPMovieSourceTypeFile];
    [moviePlayer setControlStyle:MPMovieControlStyleNone];
    [moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    [moviePlayer setShouldAutoplay:YES];
    
    UIView *movieView = [moviePlayer view];
    UIImageView *avatarImageView = [self avatarImageView];
    
    [movieView setFrame:[avatarImageView frame]];
    [movieView setAutoresizingMask:[avatarImageView autoresizingMask]];
    
    UIScrollView *scrollView = [self scrollView];
    
    [scrollView insertSubview:movieView aboveSubview:avatarImageView];
    [scrollView bringSubviewToFront:[self playButton]];
    [scrollView bringSubviewToFront:[self cameraIconImageView]];
    [avatarImageView setHidden:YES];
    
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSProfileDetailsViewDelegate Methods

- (void)profileDetailsViewKissButtonPressed:(INSProfileDetailsView *)profileDetailsView;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    [[INSServerAPIManager sharedManager] kissAddForUserID:[currentUser userID]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)profileDetailsViewCommentButtonPressed:(INSProfileDetailsView *)profileDetailsView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSCommentsViewController *nextViewController = [[INSCommentsViewController alloc] init];
    [nextViewController setUser:[[INSUserManager sharedManager] currentUser]];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)profileDetailsViewAllCommentsButtonPressed:(INSProfileDetailsView *)profileDetailsView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        [[AppDelegate sharedDelegate] promptUserToLogIn];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSCommentsViewController *nextViewController = [[INSCommentsViewController alloc] init];
    [nextViewController setUser:[[INSUserManager sharedManager] currentUser]];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)profileDetailsViewMoreButtonPressed:(INSProfileDetailsView *)profileDetailsView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self moviePlayerController] pause];
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    
    __weak INSCurrentUserProfileViewController *weakSelf = self;
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    
    RIButtonItem *instagramButton = [RIButtonItem item];
    [instagramButton setLabel:@"View Instagram Profile"];
    [instagramButton setAction:^{
        
        INSSocialMediaViewController *nextViewController = [[INSSocialMediaViewController alloc] init];
        [nextViewController setUser:currentUser];
        [nextViewController setPlatform:INSSocialMediaPlatformInstagram];
        
        [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
        
    }];
    
    RIButtonItem *vineButton = [RIButtonItem item];
    [vineButton setLabel:@"View Vine Profile"];
    [vineButton setAction:^{
        
        INSSocialMediaViewController *nextViewController = [[INSSocialMediaViewController alloc] init];
        [nextViewController setUser:currentUser];
        [nextViewController setPlatform:INSSocialMediaPlatformVine];
        
        [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
        
    }];
    
    NSMutableArray *buttons = [NSMutableArray array];
    
    if ([[currentUser instagramName] isNotEmpty])
    {
        [buttons addObject:instagramButton];
    }
    
    if ([[currentUser vineName] isNotEmpty])
    {
        [buttons addObject:vineButton];
    }
    
    [buttons addObject:cancelButton];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                               cancelButtonItem:nil
                                          destructiveButtonItem:nil
                                               otherButtonItems:nil];
    
    for (RIButtonItem *button in buttons)
    {
        [sheet addButtonItem:button];
    }
    
    [sheet showInView:[self view]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerEditedImage];
	if (selectedImage == nil)
	{
		selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
	}
    
    __weak INSCurrentUserProfileViewController *weakSelf = self;

    void (^processAndUploadImage)(UIImage *uploadImage) = ^(UIImage *uploadImage) {
        
        NSLog(@"Incoming size: %@", NSStringFromCGSize([uploadImage size]));
        
        UIImage *croppedImage = [uploadImage croppedProfileImageINS];
        
        [[[INSUserManager sharedManager] currentUser] setAvatarImage:croppedImage];

        [[INSServerAPIManager sharedManager] editUserProfilePicWithImage:croppedImage
                                                                  orData:nil];
        
    };
    
    if ([selectedImage isSquareINS])
    {
        NSLog(@"Square Image");
        
        processAndUploadImage(selectedImage);
        
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
    }
    else
    {
        NSLog(@"Rectangle Image");
        
        INSImageEditorViewController *nextViewController = [[INSImageEditorViewController alloc] initWithNibName:nil bundle:nil];

        [nextViewController setSourceImage:selectedImage];
        [nextViewController setDoneCallback:^(UIImage *image, BOOL canceled) {
            
            if (canceled || (image == nil) )
            {
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                processAndUploadImage(image);
                
                [weakSelf dismissViewControllerAnimated:YES
                                             completion:nil];
            }
        }];
        
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                     [weakSelf presentViewController:nextViewController
                                                            animated:YES
                                                          completion:nil];
                                     
                                 }];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
	[self dismissViewControllerAnimated:YES
							 completion:nil];
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
