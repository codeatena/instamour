
package com.hyper.instamour;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SignupNext extends Activity {
    private Spinner spinner_sex, spinner_ethnicity;
    private InputStream is;
    private StringBuilder sb;

    private int Jabberid;
    boolean status = false;
    boolean age = false, username = false;

    final Calendar myCalendar = Calendar.getInstance();
    private String result;
    private DatePicker datePicker;
    double latitude = 0;
    double longitude = 0;
    private int identity_pos, height_pos, bodytype_pos, lookingfor_pos,
            ethnicity_pos, smoker_pos, sex_pos;
    private Boolean checkData = false;
    private ProgressDialog pd;
    String selectedImagePath;
    Function c = new Function();
    private ArrayList<NameValuePair> param;
    private TextView txt_getlocation, dob_txt;
    private EditText mUserName;
    private MultipartEntity entity;
    Global global;
    private String sex, ethnicity, city, bday;
    Bundle data;
    private int pYear;
    private int pMonth;
    private int pDay;
    static final int DATE_DIALOG_ID = 0;
    String uname, state;
    String email;
    String pwd;
    String fbid, validsignup;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                || newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
            super.onConfigurationChanged(newConfig);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Runtime.getRuntime().totalMemory();
        Runtime.getRuntime().freeMemory();
        setContentView(R.layout.signupnext);
        global = new Global();
        global.preferences = getSharedPreferences("LoginPrefrence",
                MODE_PRIVATE);
        global.editor = global.preferences.edit();
        entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        spinner_ethnicity = (Spinner) findViewById(R.id.ethnicity_spinner);
        spinner_sex = (Spinner) findViewById(R.id.gender_spinner);
        txt_getlocation = (TextView) findViewById(R.id.location_spinner);
        dob_txt = (TextView) findViewById(R.id.dob_txt);
        mUserName = (EditText) findViewById(R.id.username_edt);
        data = getIntent().getExtras();
        selectedImagePath = data.getString("signupimgpath");
        Log.e("selected image path ", "--" + selectedImagePath);
        pd = new ProgressDialog(SignupNext.this);
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(1996, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)));

        pYear = cal.get(Calendar.YEAR);

        pMonth = cal.get(Calendar.MONTH);
        pDay = cal.get(Calendar.DAY_OF_MONTH);
        uname = data.getString("signupusername");

        email = data.getString("signupemail");
        pwd = data.getString("signuppassword");
        Log.e("pwd", pwd);
        Jabberid = data.getInt("JabberId");
        fbid = data.getString("fbid");
        Log.e("Details from previous page", "-----" + Jabberid + "-->" + uname
                + "-->" + pwd + "-->" + email + "-->" + selectedImagePath);

        if (TextUtils.isEmpty(fbid) || fbid.equalsIgnoreCase("null")) {
            mUserName.setVisibility(View.GONE);
            findViewById(R.id.username).setVisibility(View.GONE);
        } else {
            mUserName.setText(uname);
        }

        if (c.haveNetworkConnection(getApplicationContext())) {
            getLocation();
        } else {
            AlertDialog.Builder dialog2 = new AlertDialog.Builder(SignupNext.this);
            dialog2.setTitle("Instamour");
            dialog2.setMessage("No Data Connection Avaible");
            dialog2.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                        }
                    });
            AlertDialog alert2 = dialog2.create();
            alert2.show();
        }

        /** Display the current date in the TextView */

    }

    private DatePickerDialog.OnDateSetListener pDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            pYear = year;
            pMonth = monthOfYear;
            pDay = dayOfMonth;
            updateDisplay();

        }
    };

    /** Updates the date in the TextView */
    private void updateDisplay() {
        dob_txt.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(pMonth + 1).append("/").append(pDay).append("/")
                .append(pYear).append(" "));
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, pDateSetListener, 1996, pMonth,
                        pDay);

        }
        return null;
    }

    @SuppressLint("NewApi")
    private void getLocation() {
        ProgressDialog pd_location;
        pd_location = new ProgressDialog(SignupNext.this);
        pd_location.setMessage("getting location");
        pd_location.setCancelable(false);
        pd_location.show();

        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        GPSTracker gps;
        gps = new GPSTracker(SignupNext.this);

        if (gps.canGetLocation()) {

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            global.editor.putString("lat", String.valueOf(latitude));
            global.editor.putString("lng", String.valueOf(longitude));

            global.editor.commit();
            // \n is for new line
            try {
                Geocoder gcd = new Geocoder(getApplicationContext(),
                        Locale.getDefault());
                List<Address> addresses = gcd.getFromLocation(latitude,
                        longitude, 1);
                Log.e("address", addresses.toString());
                if (addresses.size() > 0)
                    System.out.println(addresses.get(0).getAddressLine(2));

                city = addresses.get(0).getLocality() + ","
                        + addresses.get(0).getCountryCode();
                Log.e("city", city + "--");
                if (city.equals("") || city.equals(null)) {
                    Toast.makeText(getApplicationContext(),
                            "Enable Gps to get the locaton", Toast.LENGTH_LONG)
                            .show();
                } else {

                    txt_getlocation.setText(city);
                }

            } catch (Exception e) {
                e.printStackTrace();

            }

        } else {

            gps.showSettingsAlert();
            pd_location.dismiss();
        }
        pd_location.dismiss();

    }

    @SuppressWarnings("deprecation")
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.go:

                sex = spinner_sex.getSelectedItem().toString();
                sex_pos = spinner_sex.getSelectedItemPosition();
                ethnicity = spinner_ethnicity.getSelectedItem().toString();
                ethnicity_pos = spinner_ethnicity.getSelectedItemPosition();
                city = txt_getlocation.getText().toString();// Philadelphia, US
                bday = dob_txt.getText().toString();
                if (!TextUtils.isEmpty(fbid) && !fbid.equalsIgnoreCase("null"))
                    uname = mUserName.getText().toString().trim();
                checkData = sex.equals("Choose")
                        || ethnicity.equals("Choose")
                        || TextUtils.isEmpty(uname)
                        || bday.equals("");

                if (!checkData) {
                    boolean isonline = c
                            .haveNetworkConnection(getApplicationContext());
                    if (isonline == true) {
                        new AsyncAction().execute();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "No Data Connection Avaible", Toast.LENGTH_LONG)
                                .show();
                    }

                } else {
                    checkData = false;
                    Toast.makeText(getApplicationContext(),
                            "Some data Should not be blank", Toast.LENGTH_LONG)
                            .show();
                }

                break;

            case R.id.location_spinner:

                getLocation();
                break;
            case R.id.dob_txt:
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.signup_cancel:
                finish();
                break;

        }
    }

    public class AsyncAction extends AsyncTask<Void, Void, Void> {
        public boolean status = false;

        // private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pd.setMessage("Creating Account...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            getinertdata();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            // pd.dismiss();
            validsignup = global.preferences.getString("validsignup", "");

            if (state.equals("fail")) {
                new UserAddAsync().execute();
            } else {

                new UploadImageTask().execute();

            }

        }

    }

    @SuppressLint("NewApi")
    public void uploadImage() {
        int rotate = 0;
        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        try {
            String signupimgpath = data.getString("signupimgpath");
            Log.e("image path to be uploaded", signupimgpath);
            File file = new File(signupimgpath);
            /*
             * try { ExifInterface exif = new
             * ExifInterface(file.getAbsolutePath()); int orientation =
             * exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
             * ExifInterface.ORIENTATION_NORMAL); switch (orientation) { case
             * ExifInterface.ORIENTATION_ROTATE_270: rotate = 270; break; case
             * ExifInterface.ORIENTATION_ROTATE_180: rotate = 180; break; case
             * ExifInterface.ORIENTATION_ROTATE_90: rotate = 90; break; }
             * Log.i("RotateImage", "Exif orientation: " + orientation);
             * Log.i("RotateImage", "Rotate value: " + rotate); } catch
             * (Exception e) { e.printStackTrace(); }
             */

            Log.e("file ", file.toString());
            String uid = "";
            uid = global.preferences.getString("uid", uid);
            Log.e("uid", uid);
            entity.addPart("m", new StringBody("image-add"));
            entity.addPart("sessionId", new StringBody(uid));
            entity.addPart("image", new FileBody(file));
            Log.e("parameter is ", " -->" + entity);

            MultipartEntity nameValuePairs = (MultipartEntity) entity;
            String sResponse = getJSONFromUrl(c.link.trim(), nameValuePairs);

            Log.e("Image upload response is  : ", sResponse);
            global.editor.putString("photo", file + "---------");
            global.editor.commit();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            Log.e("errottt", "---");
        }
        // return rotate;

    }

    public String getJSONFromUrl(String url, MultipartEntity nameValuePairs) {

        InputStream is = null;
        String json = null;
        try {

            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(nameValuePairs);

            HttpResponse response = httpClient.execute(httpPost);
            Log.e("response from post method", "" + httpPost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();

        } catch (UnsupportedEncodingException e) {
            Log.e("errottt", json);

            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.e("errottt", json);
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("errottt", json);
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("errottt", json);
            e.printStackTrace();
        }
        return json;
    }

    @SuppressLint("NewApi")
    private void getinertdata() {

        global.editor.putString("validsignup", "true");
        global.editor.commit();
        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        Date cDate = new Date(pYear - 1900, pMonth, pDay);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fDate = sdf.format(cDate);

        Log.e("selected date is", "-" + fDate);

        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("m", "user-add"));
        param.add(new BasicNameValuePair("uname", uname));
        param.add(new BasicNameValuePair("email", email));
        param.add(new BasicNameValuePair("fbid", fbid));

        param.add(new BasicNameValuePair("pwd", pwd));
        param.add(new BasicNameValuePair("gender", sex));
        param.add(new BasicNameValuePair("dob", fDate));

        param.add(new BasicNameValuePair("ethnicity", ethnicity));
        param.add(new BasicNameValuePair("city", city));
        param.add(new BasicNameValuePair("ltd", Double.toString(latitude)));
        param.add(new BasicNameValuePair("lngd", Double.toString(longitude)));
        param.add(new BasicNameValuePair("chatid", Integer.toString(Jabberid)));
        param.add(new BasicNameValuePair("qbpass", "instamourapp"));

        JSONObject json = c.makeHttpRequest(c.link.trim(), "POST", param);
        Log.e("response from create account", json + "-");
        try {
            state = json.getString("state");
            String sessionId = json.getString("sessionId");

            if (json.get("state").equals("success")) {

                global.editor.putString("validlogin", "true");
                global.editor.putString("uid", sessionId);
                global.editor
                        .putString("session_login", String.valueOf("true"));

                global.editor.putString("uname", uname);
                global.editor.putString("password", pwd);
                global.editor.putString("jabberId", Integer.toString(Jabberid));

                global.setLoginusername(uname);
                global.setPassword(pwd);
                global.setJabberid(Integer.toString(Jabberid));

                global.is_phone_checked = false;
                global.is_video_checked = false;
                global.is_account_checked = true;
                global.editor.commit();
                status = true;

            } else {
                if (json.get("msg").equals("You must be over 18")) {
                    global.editor.putString("validlogin", "false");
                    global.editor.putString("session_login",
                            String.valueOf("false"));
                    global.editor.commit();
                    status = false;
                    age = false;

                } else if (json.get("msg").equals("Username already exists")) {
                    global.editor.putString("validlogin", "false");
                    global.editor.putString("session_login",
                            String.valueOf("false"));
                    global.editor.commit();
                    status = false;
                    username = false;
                }

            }

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public class UpdateLocation extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pd_location;
        GPSTracker gps;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pd_location = new ProgressDialog(SignupNext.this);
            pd_location.setMessage("getting location");
            pd_location.setCancelable(false);
            pd_location.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            getLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pd_location.dismiss();
        }
    }

    private class UploadImageTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            uploadImage();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pd.dismiss();
            if (validsignup.equals("true")) {

                global.editor.putInt("ethnicity_pos", ethnicity_pos);
                global.editor.putInt("sex_pos", sex_pos);
                Log.e("sex positions", sex_pos + "");

                global.editor.commit();
                Intent intent = new Intent(SignupNext.this, CreateVideos.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                global.from_next = true;
                finish();
            }
        }
    }

    public class UserAddAsync extends AsyncTask<Void, Void, Void> {
        public boolean status = false, age = false, username = false;

        // private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("m", "login"));
            param.add(new BasicNameValuePair("fbid", fbid));
            JSONObject json1 = c.makeHttpRequest(c.link.trim(), "POST", param);
            Log.e("state 2", json1 + "");
            try {
                if (json1.get("state").equals("success")) {
                    String sessionID = json1.getString("sessionId");
                    Log.e("Session id ", json1.get("sessionId".toString()) + "");
                    global.from_fb = true;
                    global.editor.putString("validlogin", "true");
                    global.editor.putString("uid", String.valueOf(sessionID));

                    global.editor.putString("uname", uname);
                    Log.e("pass", "--->" + pwd);
                    global.editor.putString("password", pwd);
                    global.editor.putString("jabberId",
                            Integer.toString(Jabberid));

                    global.setLoginusername(uname);
                    global.setPassword(pwd);
                    global.setJabberid(Integer.toString(Jabberid));

                    global.editor.putString("session_login",
                            String.valueOf("true"));
                    global.is_phone_checked = false;
                    global.is_video_checked = false;
                    global.is_account_checked = true;
                    global.editor.commit();

                    status = true;

                } else {
                    if (json1.get("msg").equals("You must be over 18")) {
                        global.editor.putString("validlogin", "false");
                        global.editor.putString("session_login",
                                String.valueOf("false"));
                        global.editor.commit();
                        status = false;
                        age = false;

                    } else if (json1.get("msg").equals(
                            "Username already exists")) {
                        global.editor.putString("validlogin", "false");
                        global.editor.putString("session_login",
                                String.valueOf("false"));
                        global.editor.commit();
                        status = false;
                        username = false;
                    }

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (pd.isShowing()) {
                pd.dismiss();
                if (status == false) {

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            if (!username) {
                                Toast.makeText(getApplicationContext(),
                                        "Username/Email already exitsts",
                                        Toast.LENGTH_LONG).show();
                            } else if (!age) {
                                Toast.makeText(getApplicationContext(),
                                        "You must be over 18",
                                        Toast.LENGTH_LONG).show();
                            }
                        }

                    });

                } else {
                    Intent intent = new Intent(SignupNext.this,
                            CreateVideos.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    global.from_next = true;
                    finish();
                }
                /**/
                Log.e("FaceBook Login : ", "----->" + global.getPassword());
            }

        }

    }

    //the directions for FlurryAnalytics call for this
    //(FLJ, 5/6/14)
    // The Activity's onStart method
    @Override
    protected void onStart() { // Set up the Flurry session
        super.onStart();
        FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");

    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
