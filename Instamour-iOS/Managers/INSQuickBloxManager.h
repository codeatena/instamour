//
//  INSQuickBloxManager.h
//  Instamour
//
//  Created by Brian Slick on 8/1/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import <Foundation/Foundation.h>

// Public Constants
FOUNDATION_EXPORT NSString *const INSQuickBloxManagerChatCallRejectedNotification;
FOUNDATION_EXPORT NSString *const INSQuickBloxManagerChatCallAcceptedNotification;
FOUNDATION_EXPORT NSString *const INSQuickBloxManagerChatCallDidBeginNotification;
FOUNDATION_EXPORT NSString *const INSQuickBloxManagerChatCallEndedByOtherUserNotification;
FOUNDATION_EXPORT NSString *const INSQuickBloxManagerOtherUserDidNotAnswerNotification;

// Protocols

@interface INSQuickBloxManager : NSObject <QBActionStatusDelegate, QBChatDelegate>

// Public Properties

// Public Methods
+ (instancetype)sharedManager;

//- (void)createSession;
- (void)loginCurrentUser;
- (void)logoutCurrentUser;

- (void)setRegisteredForNotifications:(BOOL)isRegistered;

- (void)cleanupAfterCallEnds;

@end
