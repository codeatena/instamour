//
//  INSLocationManager.h
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

// Public Constants
FOUNDATION_EXPORT NSString *const INSLocationManagerDidChangeLocationNotification;
FOUNDATION_EXPORT NSString *const INSLocationManagerFailedNotification;

@interface INSLocationManager : NSObject

// Public Properties
@property (nonatomic, strong, readonly) CLLocationManager *locationManager;
@property (nonatomic, strong, readonly) CLGeocoder *geocoder;

@property (nonatomic, copy, readonly) NSString *city;
@property (nonatomic, copy, readonly) NSString *state;
@property (nonatomic, copy, readonly) NSString *country;
@property (nonatomic, copy, readonly) NSString *latitude;
@property (nonatomic, copy, readonly) NSString *longitude;

// Public Methods
+ (instancetype)sharedManager;

- (void)startTrackingLocation;
- (void)stopTrackingLocation;

@end
