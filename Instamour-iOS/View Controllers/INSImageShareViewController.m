//
//  INSImageShareViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSImageShareViewController.h"

// Models and other global
#import "INSActivityItemProvider.h"

// Sub-controllers

// Views

// Private Constants

@interface INSImageShareViewController ()

// Private Properties
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end

@implementation INSImageShareViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewDidLoad];
    
    // Navigation Bar
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shareButton"]
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(shareButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:shareButton];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
    
    // Navigation Bar
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"instamour_logo_clear"]];
    [[self navigationItem] setTitleView:imageView];
    
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    
    // User Interface
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *image = [[self chatMessage] image];
    NSLog(@"image: %@", image);
    
    [[[self navigationItem] rightBarButtonItem] setEnabled:(image != nil)];
    
    [[self imageView] setImage:image];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers


#pragma mark - UI Response Methods

- (void)shareButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *image = [[self chatMessage] image];
    
    INSActivityItemProvider *activityItemProvider = [[INSActivityItemProvider alloc] init];
    [activityItemProvider setCheckOutThisThingText:@"Check out this photo from"];
    [activityItemProvider setUrlString:@"http://apple.instamour.com"];
    
    NSMutableArray *activityItems = [NSMutableArray array];
    [activityItems addObject:activityItemProvider];
    if (image != nil)
    {
        [activityItems addObject:image];
    }
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                                         applicationActivities:nil];
    
    [self presentViewController:activityViewController
                       animated:YES
                     completion:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

@end
