package com.hyper.instamour.model;

public class ChatHistoryData {

	int id;
	int senderId;
	int receiverId;
	String chatContents;
	String chatDate;
	String chatTime;
	String LocalPath;
	int isdownloaded;

	int cnt;
	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	String chatname;
	String chatimg;
	int chatjid;

	public String getChatname() {
		return chatname;
	}

	public void setChatname(String chatname) {
		this.chatname = chatname;
	}

	public String getChatimg() {
		return chatimg;
	}

	public void setChatimg(String chatimg) {
		this.chatimg = chatimg;
	}

	public int getChatjid() {
		return chatjid;
	}

	public void setChatjid(int chatjid) {
		this.chatjid = chatjid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public String getChatContents() {
		return chatContents;
	}

	public void setChatContents(String chatContents) {
		this.chatContents = chatContents;
	}

	public String getChatDate() {
		return chatDate;
	}

	public void setChatDate(String chatDate) {
		this.chatDate = chatDate;
	}

	public String getChatTime() {
		return chatTime;
	}

	public void setChatTime(String chatTime) {
		this.chatTime = chatTime;
	}

	public String getLocalPath() {
		return LocalPath;
	}

	public void setLocalPath(String localPath) {
		LocalPath = localPath;
	}

	public int getIsdownloaded() {
		return isdownloaded;
	}

	public void setIsdownloaded(int isdownloaded) {
		this.isdownloaded = isdownloaded;
	}

}
