//
//  INSChatAmoursViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatAmoursViewController.h"

// Models and other global

// Sub-controllers
#import "INSChatViewController.h"

// Views
#import "INSChatAmoursTableCell.h"
#import "INSChatTableHeaderView.h"

// Private Constants

@interface INSChatAmoursViewController ()

// Private Properties

@end

@implementation INSChatAmoursViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
    
    // Navigation Bar
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"instamour_logo_clear"]];
    [[self navigationItem] setTitleView:imageView];
    
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    [Flurry logEvent:@"Amours Chat View"];
    
    // Contents
    
    [[INSChatDataManager sharedManager] refreshSortedConversations];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSChatDataManagerDidChangeAllConversationsNotification
                                   selector:@selector(allChatConversationsDidChangeNotification:)
                                     object:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [INSChatAmoursTableCell registerNibForTableViewBTI:tableView];
    
    INSChatAmoursTableCell *cell = [INSChatAmoursTableCell dequeueCellFromTableViewBTI:tableView];
    [tableView setRowHeight:CGRectGetHeight([cell frame])];
    
    [INSChatTableHeaderView registerNibForTableViewBTI:tableView];
    
    INSChatTableHeaderView *headerView = [INSChatTableHeaderView dequeueHeaderFooterViewFromTableViewBTI:tableView];
    [tableView setSectionFooterHeight:CGRectGetHeight([headerView frame])];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)allChatConversationsDidChangeNotification:(NSNotification *)notification;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods


#pragma mark - Misc Methods


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    INSChatDataManager *dataManager = [INSChatDataManager sharedManager];
    
    return [[dataManager sortedChatConversations] numberOfSections];
}

- (UIView *)tableView:(UITableView *)tableView
viewForHeaderInSection:(NSInteger)section
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSChatTableHeaderView *headerView = [INSChatTableHeaderView dequeueHeaderFooterViewFromTableViewBTI:tableView];
    
    INSChatDataManager *dataManager = [INSChatDataManager sharedManager];

    NSString *headerText = [[dataManager sortedChatConversations] headerTitleInSection:section];
    
    [[headerView headerLabel] setText:headerText];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    INSChatDataManager *dataManager = [INSChatDataManager sharedManager];

    return [[dataManager sortedChatConversations] numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSChatAmoursTableCell *cell = [INSChatAmoursTableCell dequeueCellFromTableViewBTI:tableView];
    
    INSChatDataManager *dataManager = [INSChatDataManager sharedManager];
    BTITableRowInfo *rowInfo = [[dataManager sortedChatConversations] rowInfoAtIndexPath:indexPath];
    INSChatConversation *conversation = [rowInfo representedObject];
    
    INSUser *user = [conversation amourUser];
    INSChatMessage *chatMessage = [conversation mostRecentChatMessage];
    
    [[cell userNameLabel] setText:[user userName]];
    [[cell messageLabel] setText:[chatMessage messageBody]];
    [[cell dateLabel] setText:[NSDateFormatter localizedStringFromDate:[chatMessage dateSent] dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle]];
    
    switch ([chatMessage messageType])
    {
        case INSChatMessageTypeImage:
        case INSChatMessageTypeVideo:
        {
            [[cell thumbnailImageView] setHidden:NO];
            [[cell thumbnailImageView] setImage:[chatMessage thumbnailImage]];
            
            [[cell messageLabel] setText:nil];
        }
            break;
        default:
        {
            [[cell thumbnailImageView] setHidden:YES];
            
            [[cell messageLabel] setText:[chatMessage messageBody]];
        }
            break;
    }
    
    [cell setCount:[conversation numberOfUnreadMessages]];
    
    __weak UIImageView *avatarImageView = [cell avatarImageView];
    
    UIImage *avatarImage = [user avatarImage];
    if (avatarImage == nil)
    {
        [user setAvatarDownloadCompletionBlock:^(UIImage *image){
            
            [avatarImageView setImage:image];
            
        }];
        
        avatarImage = [UIImage placeholderAvatarImageINS];
    }
    else
    {
        [user setAvatarDownloadCompletionBlock:nil];
    }
    
    [avatarImageView setImage:avatarImage];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    INSChatDataManager *dataManager = [INSChatDataManager sharedManager];
    BTITableRowInfo *rowInfo = [[dataManager sortedChatConversations] rowInfoAtIndexPath:indexPath];
    INSChatConversation *conversation = [rowInfo representedObject];

    INSChatViewController *nextViewController = [[INSChatViewController alloc] init];
    [nextViewController setChatConversation:conversation];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
