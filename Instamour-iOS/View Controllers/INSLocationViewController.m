//
//  INSLocationViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/25/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSLocationViewController.h"

// Models and other global

// Sub-controllers

// Views
#import "TPKeyboardAvoidingScrollView.h"

// Private Constants

@interface INSLocationViewController () <UITextFieldDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) IBOutlet UIButton *gpsButton;
@property (nonatomic, strong) IBOutlet UITextField *cityTextField;
@property (nonatomic, strong) IBOutlet UITextField *stateTextField;
@property (nonatomic, strong) IBOutlet UITextField *countryTextField;

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *country;

@end

@implementation INSLocationViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods

- (instancetype)initWithCity:(NSString *)city
                       state:(NSString *)state
                     country:(NSString *)country
                    delegate:(id<INSLocationViewControllerDelegate>)delegate
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    self = [self init];
    if (self)
    {
        [self setCity:city];
        [self setState:state];
        [self setCountry:country];
        [self setDelegate:delegate];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return self;
}

#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Location"];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:doneButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    [Flurry logEvent:@"Location View"];
    
    // User Interface
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self cityTextField] setPlaceholder:@"City"];
    [[self cityTextField] setText:[self city]];
    
    [[self stateTextField] setPlaceholder:@"State"];
    [[self stateTextField] setText:[self state]];
    
    [[self countryTextField] setPlaceholder:@"Country"];
    [[self countryTextField] setText:[self country]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSLocationManagerDidChangeLocationNotification
                                   selector:@selector(locationUpdateDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSLocationManagerFailedNotification
                                   selector:@selector(locationUpdateFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)locationUpdateDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    INSLocationManager *locationManager = [INSLocationManager sharedManager];
    
    [self setCity:[locationManager city]];
    [self setState:[locationManager state]];
    [self setCountry:[locationManager country]];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)locationUpdateFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSLocationManager sharedManager] stopTrackingLocation];
    
    [self resignAllFirstResponders];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)doneButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[INSLocationManager sharedManager] stopTrackingLocation];
    
    [self resignAllFirstResponders];
    
    NSString *city = [self city];
    NSString *state = [self state];
    NSString *country = [self country];
    
    BOOL isValidEntry = ( [city isNotEmpty] && [state isNotEmpty] && [country isNotEmpty] );
    
    if (!isValidEntry)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"All fields are required"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Missing fields", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self delegate] locationViewController:self
                              didUpdateCity:[self city]
                                      state:[self state]
                                    country:[self country]];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)gpsButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Getting location..."];
    
    [[INSLocationManager sharedManager] startTrackingLocation];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)backgroundButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self resignAllFirstResponders];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)resignAllFirstResponders
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self cityTextField] resignFirstResponder];
    [[self stateTextField] resignFirstResponder];
    [[self countryTextField] resignFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self scrollView] TPKeyboardAvoiding_scrollToActiveTextField];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *text = [textField text];
    
    if (textField == [self cityTextField])
    {
        [self setCity:text];
    }
    else if (textField == [self stateTextField])
    {
        [self setState:text];
    }
    else if (textField == [self countryTextField])
    {
        [self setCountry:text];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [textField resignFirstResponder];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

@end
