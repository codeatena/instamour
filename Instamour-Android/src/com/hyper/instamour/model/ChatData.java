package com.hyper.instamour.model;

public class ChatData {

	private String userName;
	private String message;
	private String time;
	private boolean isSentMsg = false;
	private boolean isSent = true;
	private String pic="";
	
	public ChatData(String name, String msg, String time, boolean isSentMsg, boolean isSent, String pic){
		this.userName = name;
		this.message = msg;
		this.time = time;
		this.isSentMsg = isSentMsg;
		this.isSent = isSent;
		this.setPic(pic);
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public boolean isSentMsg() {
		return isSentMsg;
	}
	public void setSentMsg(boolean isSentMsg) {
		this.isSentMsg = isSentMsg;
	}

	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}

	public boolean isSent() {
		return isSent;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getPic() {
		return pic;
	}

}
