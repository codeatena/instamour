//
//  NSDate+INSAdditions.m
//  Instamour
//
//  Created by Brian Slick on 7/18/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "NSDate+INSAdditions.h"

@implementation NSDate (INSAdditions)

- (NSInteger)numberOfYearsOldINS
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    
    NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
    
    NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:self];
    
    if ( ([dateComponentsNow month] < [dateComponentsBirth month]) || ( ([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day])) )
    {
        return [dateComponentsNow year] - [dateComponentsBirth year] - 1;
        
    } else {
        
        return [dateComponentsNow year] - [dateComponentsBirth year];
    }
}

@end
