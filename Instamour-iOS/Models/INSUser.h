//
//  INSUser.h
//  Instamour
//
//  Created by Brian Slick on 7/28/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Classes and Forward Declarations
#import "BTIObject.h"

// Public Constants
typedef void (^INSUserAvatarDownloadCompletionBlock)(UIImage *image);

// Protocols

@interface INSUser : BTIObject <NSCoding>

#pragma mark - Public Properties

@property (nonatomic, copy) NSString *sessionID;

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *facebookID;
@property (nonatomic, copy) NSString *quickBloxID;
@property (nonatomic, copy) NSString *quickBloxPassword;        // This probably isn't necessary
@property (nonatomic, copy) NSString *instagramName;
@property (nonatomic, copy) NSString *vineName;

@property (nonatomic, copy) NSDate *dateAdded;

@property (nonatomic, assign, getter = isOnline) BOOL online;
@property (nonatomic, assign, getter = isAccountDisabled) BOOL accountDisabled;

// Demographics

@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSDate *dateOfBirth;
@property (nonatomic, copy) NSString *ethnicity;
@property (nonatomic, copy) NSString *children;
@property (nonatomic, copy) NSString *identity;
@property (nonatomic, copy) NSString *height;
@property (nonatomic, copy) NSString *bodyType;
@property (nonatomic, copy) NSString *sexualPreference;
@property (nonatomic, copy) NSString *lookingFor;
@property (nonatomic, copy) NSString *smoker;

// Avatar

@property (nonatomic, copy) NSString *avatarFileName;
@property (nonatomic, strong) UIImage *avatarImage;
@property (nonatomic, copy) INSUserAvatarDownloadCompletionBlock avatarDownloadCompletionBlock;
@property (nonatomic, copy) NSString *mergeVideoFileName;

// Location

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;

// Notifications

@property (nonatomic, assign, getter = isPushNotificationsEnabled) BOOL pushNotificationsEnabled;
@property (nonatomic, assign, getter = isMyHeartPushedNotificationEnabled) BOOL myHeartPushedNotificationEnabled;
@property (nonatomic, assign, getter = isMyVideoWatchedNotificationEnabled) BOOL myVideoWatchedNotificationEnabled;
@property (nonatomic, assign, getter = isSomeoneKissedMeNotificationEnabled) BOOL someoneKissedMeNotificationEnabled;
@property (nonatomic, assign, getter = isNewCommentNotificationEnabled) BOOL newCommentNotificationEnabled;
@property (nonatomic, assign, getter = isNewAmourNotificationEnabled) BOOL newAmourNotificationEnabled;
@property (nonatomic, assign, getter = isTextChatNotificationEnabled) BOOL textChatNotificationEnabled;
@property (nonatomic, assign, getter = isAudioChatNotificationEnabled) BOOL audioChatNotificationEnabled;
@property (nonatomic, assign, getter = isVideoChatNotificationEnabled) BOOL videoChatNotificationEnabled;
@property (nonatomic, assign, getter = isGiftReceivedNotificationEnabled) BOOL giftReceivedNotificationEnabled;

// Features

@property (nonatomic, assign, getter = isAudioChatEnabled) BOOL audioChatEnabled;
@property (nonatomic, assign, getter = isVideoChatEnabled) BOOL videoChatEnabled;

// Purchased Features

@property (nonatomic, assign, getter = isUnlimitedCommentsEnabled) BOOL unlimitedCommentsEnabled;
@property (nonatomic, assign, getter = isUnlimitedVideosEnabled) BOOL unlimitedVideosEnabled;

// Misc Social

@property (nonatomic, strong) NSMutableArray *kisses;
@property (nonatomic, strong) NSMutableArray *comments;

#pragma mark - Public Methods

- (instancetype)initWithJSONUser:(NSDictionary *)jsonUser;
- (void)loadWithJSONUser:(NSDictionary *)jsonUser;

- (NSString *)formattedCityStateCountry;

@end
