//
//  INSVideoListViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSVideoListViewController.h"

// Models and other global
#import <MediaPlayer/MediaPlayer.h>

// Sub-controllers
#import "INSVideoRecorderViewController.h"

// Views
#import "INSVideoListTableCell.h"

// Private Constants

@interface INSVideoListViewController () <INSVideoListTableCellDelegate, INSVideoRecorderViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIView *upgradeView;

@property (nonatomic, assign, getter = isFullListMode) BOOL fullListMode;

@property (nonatomic, strong) INSVideoInfo *lastSelectedVideoInfo;

@end

@implementation INSVideoListViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Create Video"];
    
    [[self navigationItem] setLeftBarButtonItem:[[AppDelegate sharedDelegate] sideMenuBarButtonItem]];
    
    UIBarButtonItem *tipsButton = [[UIBarButtonItem alloc] initWithTitle:@"Tips"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(tipsButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:tipsButton];
    
    [self setBackBarButtonTitleBTI:@"Create"];
    
    [[self tableView] setEditing:YES];
    
#warning TODO: Load IAP Value
    [self setFullListMode:NO];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Toolbar
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    // Analytics
    
    [Flurry logEvent:@"Create Video View"];
    
    // User Interface
        
    [[INSVideoDataManager sharedManager] loadVideoList];

    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITableView *tableView = [self tableView];
    UIView *upgradeView = [self upgradeView];

    // Upgrade View
    if ([self isFullListMode])
    {
        [upgradeView removeFromSuperview];
        [self setUpgradeView:nil];
        [tableView setScrollEnabled:YES];
    }
    else
    {
        [tableView setScrollEnabled:NO];
        
        CGFloat rowHeight = [tableView rowHeight];
        
        CGRect tableFrame = [tableView frame];
        tableFrame.origin.y = INSMaxNumberOfFreeVideos * rowHeight;
        tableFrame.size.height -= tableFrame.origin.y;
        
        [upgradeView setFrame:tableFrame];
        [[self view] addSubview:upgradeView];
    }

    [tableView reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSVideoDataManagerDidChangeVideoListNotification
                                   selector:@selector(videoListDidChangeNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Methods

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [INSVideoListTableCell registerNibForTableViewBTI:tableView];
    
    INSVideoListTableCell *cell = [INSVideoListTableCell dequeueCellFromTableViewBTI:tableView];
    [tableView setRowHeight:CGRectGetHeight([cell frame])];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)videoListDidChangeNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self tableView] reloadData];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moviePlayerPlaybackDidFinishNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSDictionary *userInfo = [notification userInfo];
    
    NSLog(@"userInfo: %@", userInfo);

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (IBAction)buyButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
//#warning Just for testing
//    [self setFullListMode:YES];
//    [self populateContents];
    
    [Flurry logEvent:@"10 Videos Upgrade Button"];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Coming soon!"
                                                    message:@"Check back later for the ability to unlock all 10 videos!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)tipsButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"create_video_tutorial" withExtension:@"mp4"];
    
    [self playVideoAtURL:url];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)playVideoAtURL:(NSURL *)fileURL
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    MPMoviePlayerViewController *moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:fileURL];
    MPMoviePlayerController *moviePlayer = [moviePlayerViewController moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerPlaybackDidFinishNotification:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [self presentMoviePlayerViewControllerAnimated:moviePlayerViewController];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[[INSVideoDataManager sharedManager] videoInfoList] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSVideoInfo *videoInfo = [[[INSVideoDataManager sharedManager] videoInfoList] objectAtIndex:[indexPath row]];
    
    INSVideoListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:[INSVideoListTableCell reuseIdentifierBTI]];
    [cell setDelegate:self];
    
    [cell setRecordButtonEnabled:YES];
    [cell setUploadButtonEnabled:YES];
    [cell setPlayButtonEnabled:[videoInfo isPopulated]];
    [cell setDeleteButtonEnabled:[videoInfo isPopulated]];
    
    NSLog(@"file name: %@", [videoInfo thumbnailFileName]);
    NSLog(@"URL: %@", [videoInfo serverVideoFileURL]);
    
    [cell setVideoInfoState:[videoInfo state]];
    
    __weak UIImageView *avatarImageView = [cell thumbnailImageView];
    
    UIImage *avatarImage = [videoInfo thumbnailImage];
    if (avatarImage == nil)
    {
        [videoInfo setThumbnailDownloadCompletionBlock:^(UIImage *image){
            
            [avatarImageView setImage:image];
            
        }];
        
        avatarImage = [UIImage imageNamed:@"video-thumbnail"];
    }
    else
    {
        [videoInfo setThumbnailDownloadCompletionBlock:nil];
    }
    
    [avatarImageView setImage:avatarImage];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView
canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSVideoDataManager *videoDataManager = [INSVideoDataManager sharedManager];
    
    BOOL isNotBusyUploading = ![videoDataManager isAnyVideoUploading];
    BOOL isNotBusyReordering = ![videoDataManager isListReordering];
    BOOL isNotBusyDeleting = ![videoDataManager isAnyVideoDeleting];
    BOOL isNotBusyTrimming = ![videoDataManager isAnyVideoTrimming];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return (isNotBusyUploading && isNotBusyReordering && isNotBusyDeleting && isNotBusyTrimming);
}

- (void)tableView:(UITableView *)tableView
moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
      toIndexPath:(NSIndexPath *)destinationIndexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if ([sourceIndexPath isEqual:destinationIndexPath])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Row didn't move", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSVideoDataManager sharedManager] moveVideoAtIndex:[sourceIndexPath row] toIndex:[destinationIndexPath row]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDelegate Methods

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView
shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (NSIndexPath *)tableView:(UITableView *)tableView
targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
       toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    if (![self isFullListMode])
    {
        NSInteger targetRow = [proposedDestinationIndexPath row];
        
        if (targetRow >= INSMaxNumberOfFreeVideos)
        {
            proposedDestinationIndexPath = [NSIndexPath indexPathForRow:INSMaxNumberOfFreeVideos - 1 inSection:0];
        }
    }
    
    return proposedDestinationIndexPath;
}

- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - INSVideoListTableCellDelegate Methods

- (void)videoListTableCellRecordButtonPressed:(INSVideoListTableCell *)cell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    __weak INSVideoListViewController *weakSelf = self;
    
    NSIndexPath *indexPath = [[weakSelf tableView] indexPathForCell:cell];
    INSVideoInfo *videoInfo = [[[INSVideoDataManager sharedManager] videoInfoList] objectAtIndex:[indexPath row]];

    void(^showVideoRecorder)(void) = ^(void){
        
        [weakSelf setLastSelectedVideoInfo:videoInfo];
        
        INSVideoRecorderViewController *nextViewController = [[INSVideoRecorderViewController alloc] init];
        [nextViewController setDelegate:weakSelf];
        [nextViewController setConfirmationButtonTitle:@"Upload"];
        
        [[weakSelf navigationController] pushViewController:nextViewController animated:YES];

    };
    
    if ([videoInfo isPopulated])
    {
        RIButtonItem *cancelButton = [RIButtonItem item];
        [cancelButton setLabel:@"Cancel"];
        
        RIButtonItem *proceedButton = [RIButtonItem item];
        [proceedButton setLabel:@"Replace"];
        [proceedButton setAction:^{
            
            showVideoRecorder();
            
        }];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Replace Video"
                                                        message:@"Are you sure you want to replace this video?"
                                               cancelButtonItem:cancelButton
                                               otherButtonItems:proceedButton, nil];
        [alert show];
    }
    else
    {
        NSInteger row = [indexPath row];
        if (row < 4)
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSString *title = nil;
            NSString *message = nil;
            
            switch (row)
            {
                case 0:
                {
                    if ([userDefaults isCreateVideo1AlertEnabledINS])
                    {
                        title = @"Create an intro Video!";
                        message = @"You get 8 seconds to tell your matches a little about yourself. What you do for work, any hobbies, or what you are looking for in a match!";
                        [userDefaults setCreateVideo1AlertEnabledINS:NO];
                    }
                }
                    break;
                case 1:
                {
                    if ([userDefaults isCreateVideo2AlertEnabledINS])
                    {
                        title = @"Your Pet or Funny Video!";
                        message = @"Show you and your pet, or make a funny video and make your matches laugh!";
                        [userDefaults setCreateVideo2AlertEnabledINS:NO];
                    }
                }
                    break;
                case 2:
                {
                    if ([userDefaults isCreateVideo3AlertEnabledINS])
                    {
                        title = @"Your Favorite Place!";
                        message = @"Show your favorite place. Maybe at a bar, the gym, or favorite store!";
                        [userDefaults setCreateVideo3AlertEnabledINS:NO];
                    }
                }
                    break;
                case 3:
                {
                    if ([userDefaults isCreateVideo4AlertEnabledINS])
                    {
                        title = @"Your Pick!";
                        message = @"You get 8 seconds to show whatever you want. Make it exciting, original, fun, and keep it clean people!";
                        [userDefaults setCreateVideo4AlertEnabledINS:NO];
                    }
                }
                    break;
                default:
                    break;
            }
            
            if (title == nil)
            {
                // No alert was necessary
                showVideoRecorder();
            }
            else
            {
                RIButtonItem *cancelButton = [RIButtonItem item];
                [cancelButton setLabel:@"Cancel"];
                
                RIButtonItem *proceedButton = [RIButtonItem item];
                [proceedButton setLabel:@"OK"];
                [proceedButton setAction:^{
                   
                    showVideoRecorder();
                    
                }];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                                message:message
                                                       cancelButtonItem:cancelButton
                                                       otherButtonItems:proceedButton, nil];
                [alert show];
            }
        }
        else
        {
            showVideoRecorder();
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoListTableCellUploadButtonPressed:(INSVideoListTableCell *)cell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    __weak INSVideoListViewController *weakSelf = self;
    
    NSIndexPath *indexPath = [[weakSelf tableView] indexPathForCell:cell];
    INSVideoInfo *videoInfo = [[[INSVideoDataManager sharedManager] videoInfoList] objectAtIndex:[indexPath row]];

    void(^showImagePicker)(void) = ^(void){
        
        [weakSelf setLastSelectedVideoInfo:videoInfo];
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        [imagePicker setDelegate:weakSelf];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];   //UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        [imagePicker setMediaTypes:@[ (NSString *)kUTTypeMovie ]];
        [imagePicker setAllowsEditing:NO];
        
        [weakSelf presentViewController:imagePicker
                           animated:YES
                         completion:nil];
        
    };
    
    if ([videoInfo isPopulated])
    {
        RIButtonItem *cancelButton = [RIButtonItem item];
        [cancelButton setLabel:@"Cancel"];
        
        RIButtonItem *proceedButton = [RIButtonItem item];
        [proceedButton setLabel:@"Replace"];
        [proceedButton setAction:^{
           
            showImagePicker();
            
        }];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Replace Video"
                                                        message:@"Are you sure you want to replace this video?"
                                               cancelButtonItem:cancelButton
                                               otherButtonItems:proceedButton, nil];
        [alert show];
    }
    else
    {
        showImagePicker();
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoListTableCellPlayButtonPressed:(INSVideoListTableCell *)cell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:cell];
    INSVideoInfo *videoInfo = [[[INSVideoDataManager sharedManager] videoInfoList] objectAtIndex:[indexPath row]];

    NSURL *url = [videoInfo serverVideoFileURL];
    NSLog(@"playing url: %@", url);
    
    [self playVideoAtURL:url];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoListTableCellDeleteButtonPressed:(INSVideoListTableCell *)cell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    __weak INSVideoListViewController *weakSelf = self;
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    
    RIButtonItem *deleteButton = [RIButtonItem item];
    [deleteButton setLabel:@"Delete"];
    [deleteButton setAction:^{
        
        NSIndexPath *indexPath = [[weakSelf tableView] indexPathForCell:cell];
        
        [[INSVideoDataManager sharedManager] deleteVideoAtIndex:[indexPath row]];
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Video"
                                                    message:@"Are you sure you want to delete this video?"
                                           cancelButtonItem:cancelButton
                                           otherButtonItems:deleteButton, nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSVideoRecorderViewControllerDelegate Methods

- (void)videoRecorder:(INSVideoRecorderViewController *)videoRecorder
  didRecordVideoToURL:(NSURL *)fileURL
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self navigationController] popViewControllerAnimated:YES];

    if (fileURL == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Recording Error"
                                                        message:@"No video file"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No video URL", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSInteger serverSlotNumber = [[[self lastSelectedVideoInfo] serverSlotNumber] integerValue];
    
    [[INSVideoDataManager sharedManager] uploadVideoAtURL:fileURL
                                      forServerSlotNumber:serverSlotNumber
                                       withServerRotation:INSServerAPIManagerVideoRotationNone];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
    NSURL *mediaURL = [info objectForKey:UIImagePickerControllerMediaURL];
    
    AVURLAsset *urlAsset = [AVURLAsset assetWithURL:mediaURL];
    CMTime duration = [urlAsset duration];
    
    NSInteger numberOfSeconds = ceil(CMTimeGetSeconds(duration));
    
    // If the video is under the max, copy to new location and upload.
    // If it is over the max, prompt the user to trim the video.
    
    if (numberOfSeconds <= INSMaxVideoLengthInSeconds)
    {
        NSURL *targetFileURL = [INSFileUtilities uniqueNewVideoURL];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSError *error;
        BOOL isCopySuccessful = [fileManager copyItemAtURL:mediaURL toURL:targetFileURL error:&error];
        if (!isCopySuccessful)
        {
            NSLog(@"Error: %@", error);
        }
        else
        {
            NSInteger serverSlotNumber = [[[self lastSelectedVideoInfo] serverSlotNumber] integerValue];
            
            [[INSVideoDataManager sharedManager] uploadVideoAtURL:targetFileURL
                                              forServerSlotNumber:serverSlotNumber
                                               withServerRotation:INSServerAPIManagerVideoRotationNone];
        }
    }
    else
    {
        __weak INSVideoListViewController *weakSelf = self;
        
        RIButtonItem *cancelButton = [RIButtonItem item];
        [cancelButton setLabel:@"Cancel"];
        [cancelButton setAction:^{
           
            [weakSelf setLastSelectedVideoInfo:nil];
            
        }];
        
        RIButtonItem *cropButton = [RIButtonItem item];
        [cropButton setLabel:[NSString stringWithFormat:@"First %ld secs", (long)INSMaxVideoLengthInSeconds]];
        [cropButton setAction:^{
           
            NSInteger serverSlotNumber = [[[weakSelf lastSelectedVideoInfo] serverSlotNumber] integerValue];
            
            [[INSVideoDataManager sharedManager] uploadVideoAtURL:mediaURL
                                              forServerSlotNumber:serverSlotNumber
                                               withServerRotation:INSServerAPIManagerVideoRotationNone];

            
        }];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Too Long"
                                                        message:[NSString stringWithFormat:@"Videos cannot be longer than %ld seconds. Would you like to use the first %ld seconds of this video?", (long)INSMaxVideoLengthInSeconds, (long)INSMaxVideoLengthInSeconds]
                                               cancelButtonItem:cancelButton
                                               otherButtonItems:cropButton, nil];
        [alert show];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setLastSelectedVideoInfo:nil];
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}


@end
