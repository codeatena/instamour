package com.hyper.instamour;

import java.util.ArrayList;

import com.hyper.instamour.messages.C2DMReceiver;
import com.hyper.instamour.model.DataBaseHelper;
import com.hyper.instamour.model.GlobalConsts;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuAdapter extends ArrayAdapter<MenuItemList> {

	Context context;
	int layoutResourceId;
	DataBaseHelper baseHelper;
	Global global;
	ArrayList<MenuItemList> data = new ArrayList<MenuItemList>();
	LayoutInflater inflater;

	public MenuAdapter(Context context, int layoutResourceId,
			ArrayList<MenuItemList> data, LayoutInflater inflaterTmp) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		this.inflater = inflaterTmp;
		baseHelper = new DataBaseHelper(context);
		global = Global.getInstance();
		global.preferences = context.getSharedPreferences("LoginPrefrence", 0);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		//RegardingTypeHolder holder = null;

		if (row == null) {
			row = inflater.inflate(layoutResourceId, parent, false);

			//holder = new RegardingTypeHolder();
			TextView textName = (TextView) row.findViewById(R.id.link);
			ImageView img = (ImageView) row.findViewById(R.id.img_category);

			
			Log.e("xxxx", data.get(position).getName());
			
			if (data.get(position).getName().equals("Instant chat")) {
				TextView instant_noti = (TextView) row
						.findViewById(R.id.instant_noti);
				Log.e("ur inssss", "if condition");
				int cnt = baseHelper.getTotalCount();
				textName.setText(data.get(position).getName());
				img.setImageResource(data.get(position).getId());
				Log.e("cnt", "cnt" + (cnt));
				if (cnt > 0) {
					instant_noti.setVisibility(View.VISIBLE);
					instant_noti.setText("" + cnt);

				} else {
					//if (!InstantChat.listclicked) {
					instant_noti.setVisibility(View.GONE);
					instant_noti.setText("");
					//}

				}

			} else {
				textName.setText(data.get(position).getName());
				img.setImageResource(data.get(position).getId());

			}

			//row.setTag(holder);
		} /*else {
			holder = (RegardingTypeHolder) row.getTag();
		}*/
		
		
		

		return row;

	}

	static class RegardingTypeHolder {

		TextView textName;
		ImageView img;

	}
}
