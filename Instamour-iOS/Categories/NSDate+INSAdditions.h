//
//  NSDate+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/18/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (INSAdditions)

- (NSInteger)numberOfYearsOldINS;

@end
