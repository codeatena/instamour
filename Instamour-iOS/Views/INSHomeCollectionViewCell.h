//
//  INSHomeCollectionViewCell.h
//  Instamour
//
//  Created by Brian Slick on 8/20/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Forward Declarations and Classes
#import "BTICollectionViewCell.h"

// Public Constants

// Protocols

@interface INSHomeCollectionViewCell : BTICollectionViewCell

// Public Properties
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

// Public Methods

@end
