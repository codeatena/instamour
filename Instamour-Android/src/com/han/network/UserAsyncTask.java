package com.han.network;

import com.hyper.instamour.CreateVideos;
import com.hyper.instamour.PushNotificationActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class UserAsyncTask extends AsyncTask<String, Integer, Integer> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public Context parent;
	public ProgressDialog dlgLoading;
	public String strLoading;
	
	public String curAction;
	public int nResult;
	
	public static String ACTION_LOGIN = "action_login";
	public static String ACTION_USER_GET_INF = "action_user_get_inf";
	public static String ACTION_PAID_FEATURE = "action_paid_feature";
	public static String ACTION_SWITCH_PUSH = "action_switch_push";

	public UserAsyncTask(Context _parent) {
		parent = _parent;
		strLoading = "\tPlease wait...";
	}
	
	public UserAsyncTask(Context _parent, String _strLoading) {
		parent = _parent;
		strLoading = _strLoading;
	}
	
	protected Integer doInBackground(String... params) {
		nResult = FAIL;
		
		curAction = params[0];
		if (curAction.equals(ACTION_LOGIN)) {
			nResult = UserWebService.userLogin(params[1], params[2]);
			if (nResult == SUCCESS) {
				nResult = UserWebService.userGetInf();
			}
		}
		if (curAction.equals(ACTION_USER_GET_INF)) {
			nResult = UserWebService.userGetInf();
		}
		if (curAction.equals(ACTION_PAID_FEATURE)) {
			String strFeatureType = params[1];
			String strFeatureStatus = params[2];
			
			nResult = UserWebService.paidFeature(strFeatureType, strFeatureStatus);
			if (nResult == SUCCESS) {
				nResult = UserWebService.userGetInf();
			}
		}
		if (curAction.equals(ACTION_SWITCH_PUSH)) {
			String strPushType = params[1];
			nResult = UserWebService.switchPushNotification(strPushType);
			if (nResult == SUCCESS) {
				nResult = UserWebService.userGetInf();
			}
		}
		
		return nResult;
	}
	
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage(strLoading);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}

	protected void onPostExecute(Integer result) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (curAction.equals(ACTION_LOGIN)) {
			if (nResult == SUCCESS) {
				CreateVideos createVideos = (CreateVideos) parent;
				createVideos.successGetUserInf();
			}
		}
		if (curAction.equals(ACTION_USER_GET_INF)) {
			if (nResult == SUCCESS) {
				CreateVideos createVideos = (CreateVideos) parent;
				createVideos.successGetUserInf();
			}
		}
		if (curAction.equals(ACTION_PAID_FEATURE)) {
			if (nResult == SUCCESS) {
				CreateVideos createVideos = (CreateVideos) parent;
				createVideos.successGetUserInf();
			}
		}
		if (curAction.equals(ACTION_SWITCH_PUSH)) {
			if (nResult == SUCCESS) {
				PushNotificationActivity pushNotificationActivity = (PushNotificationActivity) parent;
				pushNotificationActivity.successSwitchPushNotification();
			}
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
}
