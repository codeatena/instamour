
package com.hyper.instamour;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.easy.facebook.android.apicall.GraphApi;
import com.easy.facebook.android.data.User;
import com.easy.facebook.android.error.EasyFacebookError;
import com.easy.facebook.android.facebook.FBLoginManager;
import com.easy.facebook.android.facebook.Facebook;
import com.easy.facebook.android.facebook.LoginListener;
import com.flurry.android.FlurryAgent;
import com.quickblox.core.QBCallback;
import com.quickblox.core.result.Result;
import com.quickblox.internal.core.exception.BaseServiceException;
import com.quickblox.internal.core.server.BaseService;
import com.quickblox.module.auth.model.QBProvider;
import com.quickblox.module.users.QBUsers;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.users.result.QBUserResult;

public class Signup extends Activity implements LoginListener, QBCallback {

    String largeImagePath = "";
    Uri uriLargeImage;
    Uri uriThumbnailImage;
    private EditText edt_signupusername;
    private EditText edt_signuppassword;
    private Intent pictureActionIntent;
    private static final int GALLERY_PICTURE = 1;
    Bitmap thumbnail = null;
    protected static final int CAMERA_REQUEST = 0;
    private String signup_city;
    Cursor myCursor;
    private ProgressDialog dialog;
    protected static final int CAMERA_PIC_REQUEST = 1337;
    protected static final int CAMERA_IMAGE_CAPTURE = 0;
    private Bitmap bitmap;
    private List<NameValuePair> param;
    private QBUser qbUser;
    private int Jabberid;
    private EditText edt_signupemail;
    String fileSrc, picturePath;
    private String selectedImagePath = "";
    private String username, password, email;
    public final static String KODEFUNFBAPP_ID = "219338071544861";
    Bitmap yourSelectedImage = null;
    private Boolean checkData = false;
    boolean status = false;
    ImageView select_photo;
    private FBLoginManager fbLoginManager;
    Global global;
    byte b[];
    Function c = new Function();
    private ProgressDialog pd_signUp;
    private String facebookAcceessToken;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        int currentOrientation = getResources().getConfiguration().orientation;
        if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                || newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
            super.onConfigurationChanged(newConfig);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("message", largeImagePath);
    }

    @SuppressWarnings("static-access")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Runtime.getRuntime().totalMemory();
        Runtime.getRuntime().freeMemory();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        global = global.getInstance();
        global.preferences = getSharedPreferences("LoginPrefrence",
                MODE_PRIVATE);
        dialog = new ProgressDialog(Signup.this);
        /*
         * dialog.setMessage("please wait"); dialog.setCancelable(false);
         */
        select_photo = (ImageView) findViewById(R.id.signup_choose_photo);
        global.editor = global.preferences.edit();
        edt_signupusername = (EditText) findViewById(R.id.signup_username);
        edt_signuppassword = (EditText) findViewById(R.id.signup_password);
        edt_signupemail = (EditText) findViewById(R.id.signup_email);
        if (savedInstanceState != null) {

            Bitmap Zatang;
            String B1 = savedInstanceState.getString("message");
            selectedImagePath = B1;
            Log.e("selected image", selectedImagePath);
            // Toast.makeText(this, "SavedYeah" + B1, Toast.LENGTH_LONG).show();
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = 4;
            Zatang = BitmapFactory.decodeFile((B1), opts);
            System.gc();
            // selectedImagePath=Zatang.toString();
            if (Zatang != null) {
                /*
                 * Toast.makeText(this, "Success Zatang" + B1,
                 * Toast.LENGTH_LONG) .show();
                 */
                imageCam(Zatang);
            }
        }

    }

    public void imageCam(Bitmap thumbnail) {
        Bitmap photo = thumbnail;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 85, bos);
        b = bos.toByteArray();
        select_photo.setImageBitmap(photo);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup_cancel:

                Intent intent = new Intent(Signup.this, HomeScreen.class).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.signup_go:
                pd_signUp = new ProgressDialog(Signup.this);
                username = edt_signupusername.getText().toString().trim();
                password = edt_signuppassword.getText().toString().trim();
                email = edt_signupemail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Log.e("u r in a ", "emailid");
                    edt_signupemail.setError(getString(R.string.blankEmail));
                    edt_signupemail.requestFocus();
                    checkData = true;

                }
                if (TextUtils.isEmpty(username)) {
                    Log.e("u r in a ", "emailid");
                    edt_signupusername.setError(getString(R.string.blankUsername));
                    edt_signupusername.requestFocus();
                    checkData = true;

                }
                if (TextUtils.isEmpty(password)) {
                    Log.e("u r in a ", "password");
                    edt_signuppassword.setError(getString(R.string.blankPassword));
                    edt_signuppassword.requestFocus();
                    checkData = true;

                }
                /*
                 * if (password.length() <8) { Log.e("u r in a ", "password");
                 * edt_signuppassword.setError(getString(R.string.minimumpwd));
                 * edt_signuppassword.requestFocus(); checkData = true; }
                 */
                if (!checkData) {
                    if (!validEmail(email)) {

                        edt_signupemail.setError(getString(R.string.emailError));
                        edt_signupemail.requestFocus();
                    } else {
                        boolean isonline = c
                                .haveNetworkConnection(getApplicationContext());
                        if (isonline == true) { // new AsyncAction().execute(null,
                            // null, null); Intent
                            pd_signUp.setMessage("Please Wait....");
                            pd_signUp.setCancelable(false);
                            pd_signUp.show();
                            qbUser = new QBUser();
                            qbUser.setPassword(password);
                            qbUser.setEmail(email);
                            qbUser.setLogin(username);

                            QBUsers.signUpSignInTask(qbUser, this);

                        } else {
                            AlertDialog.Builder dialog2 = new AlertDialog.Builder(
                                    Signup.this);
                            dialog2.setTitle("Instamour");
                            dialog2.setMessage("No Data Connection Avaible");
                            dialog2.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                int which) {

                                        }
                                    });
                            AlertDialog alert2 = dialog2.create();
                            alert2.show();

                        }
                        Log.e("Login SucessFully Done",
                                "Login SucessFully Done.......");
                    }

                } else {
                    checkData = false;
                    Log.e("please fill all the data", "please fill all the data");
                }

                break;
            case R.id.signup_facebook:
                pd_signUp = new ProgressDialog(Signup.this);
                if (c.haveNetworkConnection(getApplicationContext())) {
                    myConnectFacebook();
                } else {
                    AlertDialog.Builder dialog2 = new AlertDialog.Builder(
                            Signup.this);
                    dialog2.setTitle("Instamour");
                    dialog2.setMessage("No Data Connection Avaible");
                    dialog2.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {

                                }
                            });
                    AlertDialog alert2 = dialog2.create();
                    alert2.show();

                }

                break;
            case R.id.signup_choose_photo:

                /*
                 * Intent i = new Intent(); i.setType("image/*");
                 * i.setAction(Intent.ACTION_GET_CONTENT);
                 * i.putExtra("return-data", true);
                 * startActivityForResult(Intent.createChooser(i,
                 * "Select Picture"), SELECT_PICTURE);
                 */
                startDialog();

                break;
        }

    }

    private void startDialog() {

        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
        myAlertDialog.setTitle("Change Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        pictureActionIntent = new Intent(
                                Intent.ACTION_GET_CONTENT, null);
                        pictureActionIntent.setType("image/*");
                        pictureActionIntent.putExtra("return-data", true);
                        startActivityForResult(pictureActionIntent,
                                GALLERY_PICTURE);
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        /*
                         * String BX1 = android.os.Build.MANUFACTURER; if
                         * (BX1.equalsIgnoreCase("samsung")) {
                         * Toast.makeText(getApplicationContext(), "Device man"
                         * + BX1, Toast.LENGTH_LONG) .show(); Intent intent =
                         * new Intent( MediaStore.ACTION_IMAGE_CAPTURE);
                         * startActivityForResult(intent, CAMERA_IMAGE_CAPTURE);
                         * } else { Intent cameraIntent = new Intent(
                         * android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                         * startActivityForResult(cameraIntent,
                         * CAMERA_PIC_REQUEST); }
                         */

                        Intent cameraIntent = new Intent(
                                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                });
        myAlertDialog.show();

    }

    @SuppressLint("NewApi")
    public void myConnectFacebook() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String permissions[] = {
                "user_about_me",

                "user_location",

                "email", "user_photos", "user_interests", "user_online_presence",
                "xmpp_login", "offline_access", "publish_actions",
                "user_photo_video_tags", "user_photos",

                "publish_checkins", "publish_stream"
        };

        fbLoginManager = new FBLoginManager(Signup.this, R.layout.signup,
                KODEFUNFBAPP_ID, permissions);

        if (fbLoginManager.existsSavedFacebook()) {
            fbLoginManager.loadFacebook();
        } else {

            fbLoginManager.login();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @SuppressLint("NewApi")
    public void loginSuccess(Facebook facebook) {

        pd_signUp.setMessage("Please Wait....");
        pd_signUp.setCancelable(false);
        pd_signUp.show();
        User user = null;

        try {
            GraphApi graphApi = new GraphApi(facebook);
            user = new User();
            user = graphApi.getMyAccountInfo();

            global.editor.putString("validsignup", "true");

            final String fbid = user.getId();
            final String email = user.getEmail();
            final String uname = user.getFirst_name() + " "
                    + user.getLast_name();
            DownloadImage(fbid);

            Log.e("jdlfjdsjf", "----->" + fbid + "-" + email + "-" + uname);
            try {
                //                QBUser qbuser1 = new QBUser();
                //                qbuser1.setEmail(email);
                //                qbuser1.setFacebookId(fbid);
                //                qbuser1.setPassword("instamourapp");
                QBUsers.signInUsingSocialProvider(QBProvider.FACEBOOK,
                        facebook.getAccessToken(), null, new QBCallback() {

                            @Override
                            public void onComplete(Result result, Object arg1) {

                            }

                            @Override
                            public void onComplete(Result result) {
                                if (result.isSuccess()) {

                                    QBUser qbUserResult = ((QBUserResult) result)
                                            .getUser();
                                    try {
                                        qbUserResult.setPassword(BaseService
                                                .getBaseService().getToken());
                                        qbUserResult.setLogin(qbUserResult
                                                .getLogin());
                                        global.from_fb = true;
                                        global.setFbLoginUsername(qbUserResult
                                                .getLogin());
                                        global.editor.putString("fbuname",
                                                qbUserResult.getLogin());
                                        global.editor.putBoolean("from_fb",
                                                true);

                                        global.editor.commit();

                                    } catch (BaseServiceException e1) {
                                        e1.printStackTrace();
                                    }
                                    Log.e("Passord and login is : ",
                                            "----->"
                                                    + qbUserResult
                                                            .getPassword()
                                                    + "----->"
                                                    + qbUserResult.getLogin());
                                    Jabberid = qbUserResult.getId();

                                    Log.e("Jabber id is  : ", "------->"
                                            + Jabberid);
                                    String session_login = global.preferences
                                            .getString("session_login", "");
                                    if (Jabberid == 0) {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                "Feed action request limit reached",
                                                Toast.LENGTH_SHORT).show();

                                    } else {

                                        pd_signUp.dismiss();
                                        Intent intent = new Intent(Signup.this,
                                                SignupNext.class);
                                        intent.putExtra("fbid", fbid);
                                        intent.putExtra("signupusername", uname);
                                        intent.putExtra("signuppassword",
                                                qbUserResult.getPassword());
                                        intent.putExtra("signupemail", email);
                                        intent.putExtra("signupimgpath",
                                                selectedImagePath);

                                        intent.putExtra("JabberId", Jabberid);
                                        startActivity(intent);

                                    }

                                } else {
                                    pd_signUp.dismiss();
                                    Toast.makeText(Signup.this, result.getErrors().toString(), Toast.LENGTH_SHORT).show();
                                    Log.e("Errors", result.getErrors().toString());
                                }
                            }
                        });

            } catch (Exception e) {
                Toast.makeText(Signup.this,
                        "Sorry ! Unable to login using the facebook ",
                        Toast.LENGTH_LONG).show();
                pd_signUp.dismiss();
            }

        } catch (EasyFacebookError e) {
            e.printStackTrace();
            Toast.makeText(Signup.this,
                    "Sorry ! Unable to login using the facebook ",
                    Toast.LENGTH_LONG).show();
            pd_signUp.dismiss();
        } catch (Exception e) {
            Toast.makeText(Signup.this,
                    "Sorry ! Unable to login using the facebook ",
                    Toast.LENGTH_LONG).show();
            pd_signUp.dismiss();
        }

    }

    @SuppressLint("NewApi")
    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_PICTURE) {

                if (data != null) {

                    try {
                        Uri selectedImageUri = data.getData();
                        Log.e("selected image URI", selectedImageUri.toString());
                        selectedImagePath = getPath(selectedImageUri);
                        Log.e("selected image path", "---" + selectedImagePath);
                        select_photo.setImageURI(selectedImageUri);
                        return;
                    } catch (Exception e) {
                        // TODO: handle exception

                        e.printStackTrace();
                    } catch (OutOfMemoryError e) {
                        // TODO: handle exception
                        Toast.makeText(getApplicationContext(),
                                "this image can't be uploaded",
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
            } else if (requestCode == CAMERA_REQUEST) {
                if (Build.VERSION.SDK_INT < 19) {
                    try {

                        bitmap = (Bitmap) data.getExtras().get("data");
                        saveImage();
                        Uri selectedImageUri = data.getData();
                        // File file = new File(fileSrc);
                        Log.e("fil source", "--" + fileSrc.toString()); // select_photo.set
                                                                        // //
                                                                        // selectedImagePath
                        selectedImagePath = fileSrc.toString(); // =

                        // Bitmap bitmap = //
                        BitmapFactory.decodeFile(fileSrc.toString());
                        select_photo.setImageURI(Uri.parse(fileSrc.toString()));

                    } catch (Exception e) { // TODO: handle exception
                        Toast.makeText(getApplicationContext(),
                                "Sorry, This image can't be uploaded",
                                Toast.LENGTH_LONG).show();
                    } catch (OutOfMemoryError e) {
                        Toast.makeText(getApplicationContext(),
                                "Sorry, This image can't be uploaded",
                                Toast.LENGTH_LONG).show();
                    }
                    return;
                } else {
                    try {
                        Log.e("Above Kitkat ", "Kitkat");
                        Uri selectedImageUri = data.getData();
                        /*
                         * imageStorePath = getPath(selectedImageUri);
                         * Log.e("Image store", "---" + imageStorePath);
                         */
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        String id = selectedImageUri.getLastPathSegment()
                                .split(":")[1];
                        final String[] imageColumns = {
                                MediaStore.Images.Media.DATA
                        };
                        final String imageOrderBy = null;

                        Uri uri = getUri();

                        Cursor imageCursor = managedQuery(uri, imageColumns,
                                MediaStore.Images.Media._ID + "=" + id, null,
                                imageOrderBy);

                        if (imageCursor.moveToFirst()) {
                            selectedImagePath = imageCursor
                                    .getString(imageCursor
                                            .getColumnIndex(MediaStore.Images.Media.DATA));
                        }

                        Bitmap bpm = BitmapFactory
                                .decodeFile(selectedImagePath);
                        select_photo.setImageBitmap(bpm);
                        Log.e("path", "----->" + selectedImagePath);

                        /*
                         * ParcelFileDescriptor parcelFileDescriptor;
                         * parcelFileDescriptor = getContentResolver()
                         * .openFileDescriptor(selectedImageUri, "r");
                         * FileDescriptor fileDescriptor = parcelFileDescriptor
                         * .getFileDescriptor(); Bitmap image = BitmapFactory
                         * .decodeFileDescriptor(fileDescriptor);
                         * parcelFileDescriptor.close();
                         * select_photo.setImageBitmap(image);
                         */

                    } catch (Exception e) {
                        // TODO: handle exception
                        Log.e("Null pointer", "Null Pointer");
                    }
                    return;
                }

            } else if (requestCode == 32665) {
                fbLoginManager.loginSuccess(data);
                return;
            } /*
               * else if (requestCode == CAMERA_IMAGE_CAPTURE) { // Describe the
               * columns you'd like to have returned. Selecting // from the
               * Thumbnails location gives you both the Thumbnail // Image ID,
               * as well as the original image ID String[] projection = {
               * MediaStore.Images.Thumbnails._ID, // The columns we want
               * MediaStore.Images.Thumbnails.IMAGE_ID,
               * MediaStore.Images.Thumbnails.KIND,
               * MediaStore.Images.Thumbnails.DATA }; String selection =
               * MediaStore.Images.Thumbnails.KIND + "=" + // Select // only //
               * mini's MediaStore.Images.Thumbnails.MINI_KIND; String sort =
               * MediaStore.Images.Thumbnails._ID + " DESC"; // At the moment,
               * this is a bit of a hack, as I'm returning ALL // images, and
               * just taking the latest one. There is a better way // to narrow
               * this down I think with a WHERE clause which is // currently the
               * selection variable myCursor = this.managedQuery(
               * MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, projection,
               * selection, null, sort); long imageId = 0l; long
               * thumbnailImageId = 0l; String thumbnailPath = ""; try {
               * myCursor.moveToFirst(); imageId = myCursor .getLong(myCursor
               * .getColumnIndexOrThrow(MediaStore.Images.Thumbnails.IMAGE_ID));
               * thumbnailImageId = myCursor .getLong(myCursor
               * .getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID));
               * thumbnailPath = myCursor .getString(myCursor
               * .getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA)); }
               * finally { myCursor.close(); } // Create new Cursor to obtain
               * the file Path for the large image String[] largeFileProjection
               * = { MediaStore.Images.ImageColumns._ID,
               * MediaStore.Images.ImageColumns.DATA }; String largeFileSort =
               * MediaStore.Images.ImageColumns._ID + " DESC"; myCursor =
               * this.managedQuery(
               * MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
               * largeFileProjection, null, null, largeFileSort); largeImagePath
               * = ""; try { myCursor.moveToFirst(); // This will actually give
               * yo uthe file path location of the // image. largeImagePath =
               * myCursor .getString(myCursor
               * .getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA)); }
               * finally { myCursor.close(); } // These are the two URI's you'll
               * be interested in. They give // you a handle to the actual
               * images uriLargeImage = Uri.withAppendedPath(
               * MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
               * String.valueOf(imageId)); uriThumbnailImage =
               * Uri.withAppendedPath(
               * MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
               * String.valueOf(thumbnailImageId)); // I've left out the
               * remaining code, as all I do is assign the // URI's to my own
               * objects anyways... // Toast.makeText(this, ""+largeImagePath,
               * // Toast.LENGTH_LONG).show(); // Toast.makeText(this,
               * ""+uriLargeImage, // Toast.LENGTH_LONG).show(); //
               * Toast.makeText(this, ""+uriThumbnailImage, //
               * Toast.LENGTH_LONG).show(); if (largeImagePath != null) {
               * Toast.makeText(this, "LARGE YES" + largeImagePath,
               * Toast.LENGTH_LONG).show(); BitmapFactory.Options opts = new
               * BitmapFactory.Options(); opts.inSampleSize = 4; thumbnail =
               * BitmapFactory .decodeFile((largeImagePath), opts); System.gc();
               * if (thumbnail != null) { Toast.makeText(this,
               * "Try Without Saved Instance", Toast.LENGTH_LONG).show();
               * imageCam(thumbnail); } } if (uriLargeImage != null) {
               * Toast.makeText(this, "" + uriLargeImage, Toast.LENGTH_LONG)
               * .show(); } if (uriThumbnailImage != null) {
               * Toast.makeText(this, "" + uriThumbnailImage,
               * Toast.LENGTH_LONG).show(); } return; }
               */

        } else if (resultCode == RESULT_CANCELED) {

            Toast.makeText(getApplicationContext(), "Cancelled",
                    Toast.LENGTH_SHORT).show();
            return;
        }

    }

    @SuppressLint("NewApi")
    public void saveImage() {

        int counting = 0;
        OutputStream fOut = null;
        Uri outputFileUri;

        try {
            File root = new File(Environment.getExternalStorageDirectory()
                    + "/instamour/");
            if (!root.exists()) {
                root.mkdirs();
            }
            String imageName = "image_" + String.valueOf(counting) + ".jpg";

            File sdImageMainDirectory = new File(root, imageName);
            while (sdImageMainDirectory.exists()) {
                counting++;
                imageName = "image_" + String.valueOf(counting) + ".jpg";
                sdImageMainDirectory = new File(root, imageName);
                fileSrc = sdImageMainDirectory.toString();

            }
            Log.e("captured image", sdImageMainDirectory + "");

            outputFileUri = Uri.fromFile(sdImageMainDirectory);
            fOut = new FileOutputStream(sdImageMainDirectory);
        } catch (Exception e) {
            Log.e("Incident Photo", "Error occured. Please try again later.");
        }
        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fOut);

            fOut.flush();
            fOut.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getPath(Uri uri) {

        String[] projection = {
                MediaStore.Images.Media.DATA
        };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public void logoutSuccess() {
        fbLoginManager.displayToast("Logout Success!");
        this.logoutSuccess();

    }

    public void loginFail() {
        fbLoginManager.displayToast("Login Epic Failed!");
        this.loginFail();
    }

    @Override
    public void onComplete(Result result) {
        // TODO Auto-generated method stub
        if (result.isSuccess()) {

            String responseQB = result.getRawBody();

            JSONObject jobj, jobjID;
            try {
                jobj = new JSONObject(responseQB);
                String sucessMsg = jobj.optString("user");
                jobjID = new JSONObject(sucessMsg);
                Jabberid = jobjID.optInt("id");
                Log.e("Jabber id is  : ", "------->" + Jabberid);
                pd_signUp.dismiss();
                Intent intent2 = new Intent(Signup.this, SignupNext.class);
                intent2.putExtra("signupusername", edt_signupusername.getText()
                        .toString());
                intent2.putExtra("signuppassword", edt_signuppassword.getText()
                        .toString());
                intent2.putExtra("signupemail", edt_signupemail.getText()
                        .toString());
                intent2.putExtra("signupimgpath", selectedImagePath);
                intent2.putExtra("JabberId", Jabberid);
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        }

        else {
            Log.e("Result : ", "Failure" + result.getErrors());
            pd_signUp.dismiss();
            Toast.makeText(getApplicationContext(), result.getErrors().toString(), Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onComplete(Result arg0, Object arg1) {
        // TODO Auto-generated method stub

    }

    private void DownloadImage(String fbid) {
        String videoUrlInDownload = null;
        File ReceiveFileVideo = null;
        // downloadFile(params[0]);`
        long startTime = 0;

        try {
            // File root =
            // android.os.Environment.getExternalStorageDirectory() ;
            File dir = new File(Environment.getExternalStorageDirectory()
                    + "/instamour/");
            if (dir.exists() == false) {
                dir.mkdirs();
            }

            startTime = System.currentTimeMillis();
            String fileName = String.valueOf((System.currentTimeMillis()))
                    + ".jpeg";
            URL url = new URL("http://graph.facebook.com/" + fbid
                    + "/picture?type=large"); // you can write here any
            // link
            Log.e("url", url + "");
            // File file = new File(dir, fileName);
            // URL url = new URL ("file://some/path/anImage.png");
            InputStream input = url.openStream();
            try {
                // The sdcard directory e.g. '/sdcard' can be used directly,
                // or
                // more safely abstracted with getExternalStorageDirectory()
                // File storagePath =
                // Environment.getExternalStorageDirectory();
                Log.e("Video is Downloading  : ", "Video is Downloading......");
                ReceiveFileVideo = new File(dir, fileName);
                OutputStream output = new FileOutputStream(ReceiveFileVideo);
                try {
                    byte[] buffer = new byte[5000];
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                } finally {
                    output.close();
                }
            } finally {
                input.close();
            }

        } catch (Exception e) {

            Log.d("Error....", e.toString());
        }

        try {
            selectedImagePath = ReceiveFileVideo.getPath();
            Log.e("Download Image Path is  recieve : ",
                    ReceiveFileVideo.getPath());

            Log.e("Download video path is selected ", selectedImagePath);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    private Uri getUri() {
        String state = Environment.getExternalStorageState();
        if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }

    //the directions for FlurryAnalytics call for this
    //(FLJ, 5/6/14)
    // The Activity's onStart method
    @Override
    protected void onStart() { // Set up the Flurry session
        super.onStart();
        FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");

    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
