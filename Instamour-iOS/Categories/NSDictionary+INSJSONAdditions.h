//
//  NSDictionary+INSJSONAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (INSJSONAdditions)

// Common

- (BOOL)isStatusSuccessfulINS;
- (NSString *)errorMessageINS;
- (NSString *)sessionIdentifierINS;
- (NSString *)chatFileURLStringINS;

- (NSDictionary *)currentUserINS;

- (NSDictionary *)settingsForUserINS;

- (NSString *)identifierForUserINS;
- (NSString *)userNameForUserINS;
- (NSString *)emailForUserINS;
- (NSString *)facebookIDForUserINS;
- (NSString *)chatIDForUserINS;
- (NSString *)quickBloxPasswordForUserINS;
- (NSDate *)dateAddedForUserINS;
- (BOOL)isUserAccountDisabledINS;
- (BOOL)isUserOnlineINS;

- (NSString *)genderForUserINS;
- (NSDate *)dateOfBirthForUserINS;
- (NSString *)ethnicityForUserINS;
- (NSString *)cityForUserINS;
- (NSString *)stateForUserINS;
- (NSString *)countryForUserINS;
- (NSString *)latitudeForUserINS;       // TODO: Make number object?
- (NSString *)longitudeForUserINS;      // TODO: Make number object?
- (NSString *)profileImageNameForUserINS;
- (BOOL)isAudioChatEnabledForUserINS;
- (BOOL)isVideoChatEnabledForUserINS;
- (NSString *)identityForUserINS;
- (NSString *)heightForUserINS;
- (NSString *)bodyTypeForUserINS;
- (NSString *)sexualPreferenceForUserINS;
- (NSString *)lookingForForUserINS;
- (NSString *)smokerForUserINS;
//- (NSString *)aboutMeForUserINS;
- (NSString *)childrenForUserINS;
- (NSString *)instagramForUserINS;
- (NSString *)vineForUserINS;

- (NSString *)fileNameForUserVideoNumberINS:(NSInteger)videoNumber;
- (NSString *)thumbnailFileNameForUserVideoNumberINS:(NSInteger)videoNumber;
- (NSString *)fileNameForUserVideoMergeINS;

#pragma mark - Get User

#pragma mark Videos

- (NSArray *)videosForUserINS;

- (NSString *)identifierForVideoINS;
- (NSNumber *)slotNumberForVideoINS;
- (NSDate *)dateAddedForVideoINS;
- (NSString *)videoFileNameForVideoINS;
- (NSString *)thumbnailFileNameForVideoINS;
- (NSString *)userIDForVideoINS;
- (NSString *)userNameForVideoINS;

#pragma mark Kisses

- (NSArray *)kissesForUserINS;

- (NSString *)identifierForKissINS;
- (NSString *)senderIDForKissINS;
- (NSDate *)dateAddedForKissINS;

#pragma mark Comments

- (NSArray *)commentsForUserINS;

- (NSString *)identifierForCommentINS;
- (NSString *)senderIDForCommentINS;
- (NSString *)senderUserNameForCommentINS;
- (NSString *)avatarFileNameForCommentINS;
- (NSString *)bodyForCommentINS;
- (NSDate *)dateAddedForCommentINS;

#pragma mark Purchased Features

- (NSDictionary *)purchasedFeaturesForUserINS;
- (BOOL)isUnlimitedCommentsEnabledForUserINS;
- (BOOL)isUnlimitedVideosEnabledForUserINS;

#pragma mark Push Notifications

- (NSDictionary *)pushNotificationSettingsForUserINS;

- (BOOL)isPushNotificationsEnabledForUserINS;
- (BOOL)isHeartPushNotificationEnabledForUserINS;
- (BOOL)isWatchedVideoPushNotificationEnabledForUserINS;
- (BOOL)isKissedPushNotificationEnabledForUserINS;
- (BOOL)isNewCommentPushNotificationEnabledForUserINS;
- (BOOL)isNewAmourPushNotificationEnabledForUserINS;
- (BOOL)isChatPushNotificationEnabledForUserINS;
- (BOOL)isVideoPushNotificationEnabledForUserINS;
- (BOOL)isAudioPushNotificationEnabledForUserINS;
- (BOOL)isReceivedGiftPushNotificationEnabledForUserINS;

#pragma mark Search

- (NSArray *)usersINS;

@end
