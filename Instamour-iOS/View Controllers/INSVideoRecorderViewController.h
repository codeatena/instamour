//
//  INSVideoRecorderViewController.h
//  Instamour
//
//  Created by Brian Slick on 7/22/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//


// Libraries

// Forward Declarations and Classes
#import "BTIViewController.h"

// Public Constants

// Protocols
@protocol INSVideoRecorderViewControllerDelegate;


@interface INSVideoRecorderViewController : BTIViewController

// Public Properties
@property (nonatomic, weak) id<INSVideoRecorderViewControllerDelegate> delegate;
@property (nonatomic, copy) NSString *confirmationButtonTitle;

// Public Methods

@end

@protocol INSVideoRecorderViewControllerDelegate <NSObject>

@required
- (void)videoRecorder:(INSVideoRecorderViewController *)videoRecorder didRecordVideoToURL:(NSURL *)fileURL;

@end
