
package com.hyper.instamour;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoCapture extends Activity implements SurfaceHolder.Callback, OnClickListener {

    public static final int MAX_LENGTH = 8;

    public static final String LOGTAG = "VIDEOCAPTURE";

    private MediaRecorder mRecorder;
    private SurfaceHolder holder;
    private CamcorderProfile camcorderProfile;
    private Camera camera;
    private SurfaceView cameraView;
    boolean recording = false, mRecorded = false;
    boolean previewRunning = false;
    private Handler progressHandler;
    private Runnable progressRunnable;
    private ProgressDialog dialog;
    private File outFile;
    boolean uploading = false;
    private int mDuration;
    private int currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
    private ImageView switchCamera, tapToRecord;
    private LinearLayout topView;
    private TextView mDurationView;
    private View mDone, mCancel;
    private VideoView mVideoPlayer;
    private MediaController mController;
    private int mSurfaceHeight, mSurfaceWidth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //        PackageInfo info;
        //        try {
        //            info = getPackageManager().getPackageInfo("com.hyper.instamour", PackageManager.GET_SIGNATURES);
        //            for (Signature signature : info.signatures) {
        //                MessageDigest md;
        //                md = MessageDigest.getInstance("SHA");
        //                md.update(signature.toByteArray());
        //                String something = new String(Base64.encode(md.digest(), 0));
        //                //String something = new String(Base64.encodeBytes(md.digest()));
        //                Log.e("hash key", something);
        //            }
        //        } catch (NameNotFoundException e1) {
        //            Log.e("name not found", e1.toString());
        //        } catch (NoSuchAlgorithmException e) {
        //            Log.e("no such an algorithm", e.toString());
        //        } catch (Exception e) {
        //            Log.e("exception", e.toString());
        //        }
        setContentView(R.layout.record_video);
        bindviews();

    }

    private void bindviews() {
        cameraView = (SurfaceView) findViewById(R.id.videoview);
        switchCamera = (ImageView) findViewById(R.id.switch_camera);
        tapToRecord = (ImageView) findViewById(R.id.tap_to_record);
        mDurationView = (TextView) findViewById(R.id.duration);
        mDone = findViewById(R.id.done);
        mCancel = findViewById(R.id.cancel);
        initializeviews();
    }

    private void initializeviews() {
        mDone.setOnClickListener(this);
        mCancel.setOnClickListener(this);
        switchCamera.setOnClickListener(this);
        tapToRecord.setOnClickListener(this);
        mDone.setVisibility(View.GONE);
        mCancel.setVisibility(View.GONE);
        camcorderProfile = CamcorderProfile.get(currentCameraId, CamcorderProfile.QUALITY_480P);
        camcorderProfile.videoBitRate = 1000000;
        outFile = getOutputMediaFile(String.valueOf(System.currentTimeMillis()));
        holder = cameraView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    private boolean prepareRecorder() {
        mRecorder = new MediaRecorder();

        //        if (!setUpCamera()) {
        //            finish();
        //            return false;
        //        }
        camera.stopPreview();
        camera.unlock();
        mRecorder.setCamera(camera);
        mRecorder.setPreviewDisplay(holder.getSurface());
        mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        //        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //        mRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
        mRecorder.setProfile(camcorderProfile);
        mRecorder.setOutputFile(outFile.getPath());
        mRecorder.setMaxDuration(MAX_LENGTH * 1000);
        setMediaDisplayOrientation();
        try {
            mRecorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            finish();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            finish();
            return false;
        }
        return true;
    }

    @SuppressLint("NewApi")
    private boolean setUpCamera() {
        boolean f = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
        boolean b = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if (!f && !b) {
            Toast.makeText(this, "Camera not available", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!f || !b) {
            currentCameraId = f ? CameraInfo.CAMERA_FACING_FRONT : CameraInfo.CAMERA_FACING_BACK;
            switchCamera.setVisibility(View.GONE);
        }
        camcorderProfile = CamcorderProfile.get(currentCameraId, CamcorderProfile.QUALITY_480P);
        //         camcorderProfile.videoBitRate = 1000000;

        getCameraInstance(currentCameraId);
        if (camera == null) {
            return false;
        }
        Parameters params = camera.getParameters();
        Camera.Size size = getBestPreviewSize(mSurfaceWidth, mSurfaceHeight, params);
        if (size != null) {
            setSurfaceViewPreviewSize(size.width, size.height);
            params.setPreviewSize(size.width, size.height);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                params.setRecordingHint(true);
        }

        camera.setParameters(params);
        setCameraDisplayOrientation();
        camera.startPreview();
        try {
            camera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void startRecorder() {
        prepareRecorder();
        recording = true;
        mRecorder.start();
        tapToRecord.setImageResource(R.drawable.btn_stop_recording);
        switchCamera.setVisibility(View.GONE);
        cameraView.setKeepScreenOn(true);
        initiateProgress();
    }

    private void stopRecorder() {
        cameraView.setKeepScreenOn(false);
        try {
            mRecorder.stop();
            camera.reconnect();
        } catch (RuntimeException e) {
            Toast.makeText(this, "Invalid Video", Toast.LENGTH_SHORT).show();
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
        recording = false;
        mRecorded = true;
        progressHandler.removeCallbacks(progressRunnable);
        tapToRecord.setImageResource(R.drawable.btn1);
        switchCamera.setClickable(true);
        Log.v(LOGTAG, "Recording Stopped");
        mDone.setVisibility(View.VISIBLE);
        mCancel.setVisibility(View.VISIBLE);
        cameraView.setVisibility(View.GONE);
        mDurationView.setVisibility(View.GONE);
        mVideoPlayer = new VideoView(this);
        FrameLayout layout = (FrameLayout) findViewById(R.id.video_container);
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT, Gravity.CENTER);
        layout.removeAllViews();
        layout.addView(mVideoPlayer, params);
        mController = new MediaController(this);
        mVideoPlayer.setMediaController(mController);
        mVideoPlayer.setOnPreparedListener(new OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                mController.show();
            }
        });

        mVideoPlayer.setVideoURI(Uri.fromFile(outFile));
        // Let's prepareRecorder so we can record again
        // prepareRecorder();
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.switch_camera:
                currentCameraId = currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK ? Camera.CameraInfo.CAMERA_FACING_FRONT
                        : Camera.CameraInfo.CAMERA_FACING_BACK;
                setUpCamera();
                break;

            case R.id.tap_to_record:
                if (recording) {
                    stopRecorder();
                    progressHandler.removeCallbacks(progressRunnable);
                    tapToRecord.setImageResource(R.drawable.btn_start_recording);
                    switchCamera.setClickable(true);
                } else if (!mRecorded) {
                    startRecorder();
                    Log.v(LOGTAG, "Recording Started");
                }
                break;

            case R.id.done:
                if (!recording && outFile != null) {
                    Intent i = new Intent();
                    i.setData(Uri.fromFile(outFile));
                    setResult(RESULT_OK, i);
                    finish();
                }

            case R.id.cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }

    }

    public void initiateProgress() {

        progressHandler = new Handler();
        mDuration = 0;
        progressRunnable = new Runnable() {

            @Override
            public void run() {

                if (mDuration == MAX_LENGTH) {
                    stopRecorder();
                } else {
                    if (mDuration < MAX_LENGTH) {
                        mDuration++;
                        mDurationView.setText(mDuration + " secs");
                        progressHandler.postDelayed(progressRunnable, 1000);
                    }
                }

            }
        };
        mDurationView.setText(mDuration + " secs");
        progressHandler.postDelayed(progressRunnable, 1000);
    }

    private void getCameraInstance(int cameraId) {
        if (camera != null) {
            try {
                camera.stopPreview();
                camera.lock();
                camera.release();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
        try {
            camera = Camera.open(cameraId);
        } catch (RuntimeException e) {
            e.printStackTrace();
            Toast.makeText(this, "Can't connect to camera", Toast.LENGTH_SHORT).show();
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.v(LOGTAG, "surfaceCreated");
    }

    private void setSurfaceViewPreviewSize(int width, int height) {

        if (cameraView == null)
            return;
        float ratio;
        if (height >= width)
            ratio = (float) height / (float) width;
        else
            ratio = (float) width / (float) height;
        android.view.ViewGroup.LayoutParams lp = cameraView.getLayoutParams();
        // One of these methods should be used, second method squishes preview
        // slightly

        lp.width = width;
        lp.height = (int) (width * ratio);

        cameraView.setLayoutParams(lp);
        // Get the set dimensions
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.v(LOGTAG, "surfaceChanged");
        mSurfaceHeight = height;
        mSurfaceWidth = width;
        setUpCamera();
        //
        //        if (!recording) {
        //            if (previewRunning) {
        //                camera.stopPreview();
        //            }
        //            try {
        //                camera.lock();
        //                Camera.Parameters p = camera.getParameters();
        //
        //                Camera.Size size = getBestPreviewSize(width, height, p);
        //
        //                //   setSurfaceViewPreviewSize(size.width, size.height);
        //                if (size != null) {
        //                    //   p.setPreviewSize(size.width, size.height);
        //                }
        //                p.setPreviewFrameRate(camcorderProfile.videoFrameRate);
        //                camera.setParameters(p);
        //                camera.setPreviewDisplay(holder);
        //                camera.startPreview();
        //                previewRunning = true;
        //            } catch (IOException e) {
        //                e.printStackTrace();
        //            } catch (RuntimeException e) {
        //                e.printStackTrace();
        //            }
        //
        //            Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        //
        //            switch (display.getRotation()) {
        //                case Surface.ROTATION_0:
        //                    camera.setDisplayOrientation(90);
        //                    break;
        //
        //                case Surface.ROTATION_90:
        //
        //                    break;
        //
        //                case Surface.ROTATION_180:
        //
        //                    break;
        //
        //                case Surface.ROTATION_270:
        //                    camera.setDisplayOrientation(180);
        //                    break;
        //            }
        //        }
    }

    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea <= 720 * 480 && newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }

        return (result);
    }

    private void setCameraDisplayOrientation() {

        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(currentCameraId, info);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;

            case Surface.ROTATION_90:
                degrees = 90;
                break;

            case Surface.ROTATION_180:
                degrees = 180;
                break;

            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.stopPreview();
        camera.setDisplayOrientation(result);
    }

    private void setMediaDisplayOrientation() {

        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(currentCameraId, info);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 + result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        mRecorder.setOrientationHint(result);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.v(LOGTAG, "surfaceDestroyed");
        if (recording) {
            mRecorder.stop();
            mRecorder.release();
            recording = false;
        }

        if (progressHandler != null && progressRunnable != null)
            progressHandler.removeCallbacks(progressRunnable);

        previewRunning = false;
        if (camera != null) {
            camera.stopPreview();
            camera.lock();
            camera.release();
        }
    }

    public static File getOutputMediaFile(String title) {
        if (!hasStorage(true)) {
            return null;
        }
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().toString() + "/Instamour/");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("LearningApp", "failed to create directory");
                return null;
            }
        }
        File mediaFile = new File(mediaStorageDir, title + ".mp4");
        return mediaFile;
    }

    private static boolean hasStorage(boolean requireWriteAccess) {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            if (requireWriteAccess) {
                boolean writable = checkFsWritable();
                return writable;
            } else {
                return true;
            }
        } else if (!requireWriteAccess && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    private static boolean checkFsWritable() {
        String directoryName = Environment.getExternalStorageDirectory().toString() + "/DCIM";
        File directory = new File(directoryName);
        if (!directory.isDirectory()) {
            if (!directory.mkdirs()) {
                return false;
            }
        }
        return directory.canWrite();
    }
}
