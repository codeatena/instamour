//
//  INSServerAPIManager.m
//  Instamour
//
//  Created by Brian Slick on 7/12/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSServerAPIManager.h"

// Models and Other Global

// Private Constants

#pragma mark - API Keys

NSString *const INSAPIKeyServiceCode = @"m";

NSString *const INSAPIKeyAudioChat = @"audio_chat";
NSString *const INSAPIKeyBody = @"body";
NSString *const INSAPIKeyBodyType = @"body_type";
NSString *const INSAPIKeyChatID = @"chatid";
NSString *const INSAPIChatFileURL = @"url";
NSString *const INSAPIKeyChildren = @"children";
NSString *const INSAPIKeyCity = @"city";
NSString *const INSAPIKeyCount = @"count";
NSString *const INSAPIKeyCountry = @"country";
NSString *const INSAPIKeyDateOfBirth = @"dob";
NSString *const INSAPIKeyDistance = @"distance";
NSString *const INSAPIKeyEmail = @"email";
NSString *const INSAPIKeyEthnicity = @"ethnicity";
NSString *const INSAPIKeyFacebookUserID = @"fbid";
NSString *const INSAPIKeyGender = @"gender";
NSString *const INSAPIKeyHeight = @"height";
NSString *const INSAPIKeyIdentity = @"identity";
NSString *const INSAPIKeyIdentifier = @"id";
NSString *const INSAPIKeyInstagram = @"instagram";
NSString *const INSAPIKeyLatitude = @"ltd";
NSString *const INSAPIKeyLongitude = @"lngd";
NSString *const INSAPIKeyLookingFor = @"looking_for";
NSString *const INSAPIKeyMaximumAge = @"max_age";
NSString *const INSAPIKeyMinimumAge = @"min_age";
NSString *const INSAPIKeyNumberOfResults = @"count";
NSString *const INSAPIKeyNumberOfVideos = @"video_count";
NSString *const INSAPIKeyOrder = @"order";
NSString *const INSAPIKeyPageNumber = @"page";
NSString *const INSAPIKeyPassword = @"pwd";
NSString *const INSAPIKeyQuickBloxPassword = @"qbpass";
NSString *const INSAPIKeyReason = @"reason";
NSString *const INSAPIKeyRotate = @"rotate";
NSString *const INSAPIKeySessionID = @"sessionId";
NSString *const INSAPIKeySexualPreference = @"sexual_preference";
NSString *const INSAPIKeySmoker = @"smoker";
NSString *const INSAPIKeySource = @"source";
NSString *const INSAPIKeyState = @"state";
NSString *const INSAPIKeyStatus = @"status";
NSString *const INSAPIKeyType = @"type";
NSString *const INSAPIKeyUserID = @"uid";
NSString *const INSAPIKeyUserName = @"uname";
NSString *const INSAPIKeyVideo = @"video";
NSString *const INSAPIKeyVideoChat = @"video_chat";
NSString *const INSAPIKeyVine = @"vine";

@interface INSServerAPIManager ()

// Private Properties
@property (nonatomic, weak) NSTimer *pingTimer;

@property (nonatomic, strong) NSOperationQueue *operationQueue;

@property (nonatomic, strong) AFHTTPRequestOperationManager *requestOperationManager;

@end

@implementation INSServerAPIManager

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Management


#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
        [notificationCenter addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
        [self setRequestOperationManager:operationManager];
        [operationManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
        
        [[operationManager operationQueue] setMaxConcurrentOperationCount:3];
        
        AFHTTPRequestSerializer *requestSerializer = [operationManager requestSerializer];
        [requestSerializer setTimeoutInterval:180.0];
        [requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];

    }
    return self;
}

#pragma mark - Custom Getters and Setters

- (NSOperationQueue *)operationQueue
{
    if (_operationQueue == nil)
    {
        _operationQueue = [[NSOperationQueue alloc] init];
        [_operationQueue setMaxConcurrentOperationCount:3];
    }
    return _operationQueue;
}

#pragma mark - Notification Handlers

- (void)applicationWillResignActive:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self stopPing];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)applicationDidBecomeActive:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self startPing];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

+ (instancetype)sharedManager
{
	static dispatch_once_t pred;
	static INSServerAPIManager *shared = nil;
    
	dispatch_once(&pred, ^{
        
		shared = [[[self class] alloc] init];
        
	});
    
	return shared;
}

+ (BOOL)isNetworkConnectionAvailable
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
//    return !(networkStatus == NotReachable);
    return [[AFNetworkReachabilityManager sharedManager] isReachable];
}

+ (void)showNoNetworkAlert
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection"
                                                    message:@"You need an internet connection to sign in"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (NSString *)stringFromInteger:(NSInteger)integer
{
    NSString *string = nil;
    
    if (integer != NSNotFound)
    {
        string = [NSString stringWithFormat:@"%ld", (long)integer];
    }
    
    return string;
}

- (void)postServiceWithParameters:(NSMutableDictionary *)parameters
          successNotificationName:(NSString *)successNotificationName
          failureNotificationName:(NSString *)failureNotificationName
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    INSUser *user = [[INSUserManager sharedManager] currentUser];
    NSString *sessionID = [user sessionID];
    
    NSLog(@"SessionID: %@", sessionID);
    [parameters setKeySafelyINS:INSAPIKeySessionID andObject:sessionID];
    
    [[self requestOperationManager] POST:INSServerRequestPath
                              parameters:parameters
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     
                                     NSLog(@"Success: %@", successNotificationName);
                                     [notificationCenter postNotificationNameOnMainThreadBTI:successNotificationName
                                                                                      object:responseObject
                                                                                    userInfo:parameters];
                                     
                                 }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                     
                                     NSLog(@"Fail: %@", failureNotificationName);
                                     [notificationCenter postNotificationNameOnMainThreadBTI:failureNotificationName
                                                                                      object:error
                                                                                    userInfo:parameters];
                                     
                                 }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

+ (NSString *)serverCodeForChildrenStatus:(NSString *)children
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSString *childrenCodeString = nil;
    
    if ([children isEqualToString:INSChildrenStatusNo])
    {
        childrenCodeString = @"0";
    }
    else if ([children isEqualToString:INSChildrenStatusYes])
    {
        childrenCodeString = @"1";
    }
    else if ([children isEqualToString:INSChildrenStatusNotWithMe])
    {
        childrenCodeString = @"2";
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return childrenCodeString;
}

+ (NSString *)childrenStatusForServerCode:(NSString *)serverCode
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (![serverCode isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No code provided", self, __PRETTY_FUNCTION__);
        return nil;
    }
    
    NSString *statusToReturn = nil;

    NSInteger childrenCode = [serverCode integerValue];
    
    if (childrenCode == 0)
    {
        statusToReturn = INSChildrenStatusNo;
    }
    else if (childrenCode == 1)
    {
        statusToReturn = INSChildrenStatusYes;
    }
    else if (childrenCode == 2)
    {
        statusToReturn = INSChildrenStatusNotWithMe;
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return statusToReturn;
}

#pragma mark - Generic Services

- (void)downloadImageFromURL:(NSURL *)url
                     success:(void (^)(UIImage *image))success
                     failure:(void (^)(NSString *errorMessage))failure
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    [requestOperation setResponseSerializer:[AFImageResponseSerializer serializer]];

    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject isKindOfClass:[UIImage class]])
        {
            if (success != nil)
            {
                success((UIImage *)responseObject);
            }
        }
        else
        {
            if (failure != nil)
            {
                failure(@"Did not get an image response from server");
            }
        }
        
    }
                                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                
                                                if (failure != nil)
                                                {
                                                    failure([error localizedDescription]);
                                                }

                                            }];
    
    [[self operationQueue] addOperation:requestOperation];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadFileFromURL:(NSURL *)fromURL
                  toFileURL:(NSURL *)toURL
                    success:(void (^)(void))success
                    failure:(void (^)(NSString *errorMessage))failure
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:fromURL];
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManager];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:
                                         ^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             NSLog(@"File download succeeded");
                                             if (success != nil)
                                             {
                                                 success();
                                             }
                                             
                                         }
                                                                         failure:
                                         ^(AFHTTPRequestOperation *operation, NSError *error) {
                                             
                                             NSLog(@"File download failed");
                                             if (failure != nil)
                                             {
                                                 failure([error localizedDescription]);
                                             }
                                             
                                         }];
    
    NSOutputStream *outputStream = [NSOutputStream outputStreamWithURL:toURL append:NO];
    [operation setOutputStream:outputStream];
    
    [[manager operationQueue] addOperation:operation];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Instamour API Services

#pragma mark App Settings

NSString *const INSServerAPIManagerAppSettingsDidFinishNotification = @"INSServerAPIManagerAppSettingsDidFinishNotification";
NSString *const INSServerAPIManagerAppSettingsFailedNotification = @"INSServerAPIManagerAppSettingsFailedNotification";

//    Parameters:
//    m = 'app-settings'

- (void)appSettings
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"app-settings"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerAppSettingsDidFinishNotification
            failureNotificationName:INSServerAPIManagerAppSettingsFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}


#pragma mark Login

NSString *const INSServerAPIManagerLoginDidFinishNotification = @"INSServerAPIManagerLoginDidFinishNotification";
NSString *const INSServerAPIManagerLoginFailedNotification = @"INSServerAPIManagerLoginFailedNotification";

//    Parameters:
//    m = 'login'
//    email = 'my@email.com'
//    pwd = 'myValidPassword'
//    fbid = '1234567899'
//    source = 'ios'

- (void)loginWithEmail:(NSString *)email
              password:(NSString *)password
            facebookID:(NSString *)facebookID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"login"];
    [parameters setKeySafelyINS:INSAPIKeyEmail andObject:email];
    [parameters setKeySafelyINS:INSAPIKeyPassword andObject:password];
    [parameters setKeySafelyINS:INSAPIKeyFacebookUserID andObject:facebookID];
    [parameters setKeySafelyINS:INSAPIKeySource andObject:@"IOS"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerLoginDidFinishNotification
            failureNotificationName:INSServerAPIManagerLoginFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loginWithEmail:(NSString *)email
              password:(NSString *)password
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self loginWithEmail:email
                password:password
              facebookID:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loginWithFacebookID:(NSString *)facebookID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self loginWithEmail:nil
                password:nil
              facebookID:facebookID];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Logout

NSString *const INSServerAPIManagerLogoutDidFinishNotification = @"INSServerAPIManagerLogoutDidFinishNotification";
NSString *const INSServerAPIManagerLogoutFailedNotification = @"INSServerAPIManagerLogoutFailedNotification";

//    Parameters:
//    m='logout'
//    sessionId='52b947f0e612614d934127d16220bb77'

- (void)logOut;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[[self requestOperationManager] operationQueue] cancelAllOperations];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"logout"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerLogoutDidFinishNotification
            failureNotificationName:INSServerAPIManagerLogoutFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Reset Password

NSString *const INSServerAPIManagerResetPasswordDidFinishNotification = @"INSServerAPIManagerResetPasswordDidFinishNotification";
NSString *const INSServerAPIManagerResetPasswordFailedNotification = @"INSServerAPIManagerResetPasswordFailedNotification";

//    Parameters:
//    m='user-reset-pwd'
//    email='my@email.com'

- (void)resetPasswordForEmail:(NSString *)email
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-reset-pwd"];
    [parameters setKeySafelyINS:INSAPIKeyEmail andObject:email];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerResetPasswordDidFinishNotification
            failureNotificationName:INSServerAPIManagerResetPasswordFailedNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Get User

NSString *const INSServerAPIManagerGetUserDidFinishNotification = @"INSServerAPIManagerGetUserDidFinishNotification";
NSString *const INSServerAPIManagerGetUserFailedNotification = @"INSServerAPIManagerGetUserFailedNotification";

//    Parameters:
//    m='user-get'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    uid='1662'

- (void)getUserWithUserName:(NSString *)userName
                   orUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-get"];
    [parameters setKeySafelyINS:INSAPIKeyUserName andObject:userName];
    [parameters setKeySafelyINS:INSAPIKeyUserID andObject:userID];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerGetUserDidFinishNotification
            failureNotificationName:INSServerAPIManagerGetUserFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Add User

NSString *const INSServerAPIManagerAddUserDidFinishNotification = @"INSServerAPIManagerAddUserDidFinishNotification";
NSString *const INSServerAPIManagerAddUserFailedNotification = @"INSServerAPIManagerAddUserFailedNotification";

//    Parameters:
//    m='user-add'
//    source='android'
//    uname='evan'
//    email='evan@test.com'
//    pwd='validPassword'
//    fbid='12345'
//    gender='male' (male,female)
//    dob='1995-01-31' (yyyy-mm-dd)
//    ethnicity='Caucasian' (Asian,Black,Caucasian,European,Hispanic,Native American,Other)
//    city='Philadelphia'
//    state='Pennsylvania'
//    country='United States'
//    ltd='40.03709712'
//    lngt='-75.03209213'
//    chatid='234234'
//    qbpass='sdfdsf'

- (void)addUserWithUserName:(NSString *)userName
                      email:(NSString *)email
                   password:(NSString *)password
                 facebookID:(NSString *)facebookID
                     gender:(NSString *)gender
                dateOfBirth:(NSDate *)dateOfBirth
                  ethnicity:(NSString *)ethnicity
                  instagram:(NSString *)instagram
                       vine:(NSString *)vine
                       city:(NSString *)city
                      state:(NSString *)state
                    country:(NSString *)country
                   latitude:(NSString *)latitude
                  longitude:(NSString *)longitude
           sexualPreference:(NSString *)sexualPreference
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
        
    NSString *dateOfBirthString = [[[AppDelegate sharedDelegate] serverBirthdayDateFormatter] stringFromDate:dateOfBirth];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-add"];
    [parameters setKeySafelyINS:INSAPIKeySource andObject:@"IOS"];
    [parameters setKeySafelyINS:INSAPIKeyUserName andObject:userName];
    [parameters setKeySafelyINS:INSAPIKeyEmail andObject:email];
    [parameters setKeySafelyINS:INSAPIKeyPassword andObject:password];
    [parameters setKeySafelyINS:INSAPIKeyFacebookUserID andObject:facebookID];
    [parameters setKeySafelyINS:INSAPIKeyGender andObject:gender];
    [parameters setKeySafelyINS:INSAPIKeyDateOfBirth andObject:dateOfBirthString];
    [parameters setKeySafelyINS:INSAPIKeyEthnicity andObject:ethnicity];
    [parameters setKeySafelyINS:INSAPIKeyInstagram andObject:instagram];
    [parameters setKeySafelyINS:INSAPIKeyVine andObject:vine];
    [parameters setKeySafelyINS:INSAPIKeyCity andObject:city];
    [parameters setKeySafelyINS:INSAPIKeyState andObject:state];
    [parameters setKeySafelyINS:INSAPIKeyCountry andObject:country];
    [parameters setKeySafelyINS:INSAPIKeyLatitude andObject:latitude];
    [parameters setKeySafelyINS:INSAPIKeyLongitude andObject:longitude];
    [parameters setKeySafelyINS:INSAPIKeySexualPreference andObject:sexualPreference];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerAddUserDidFinishNotification
            failureNotificationName:INSServerAPIManagerAddUserFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Edit User

NSString *const INSServerAPIManagerEditUserDidFinishNotification = @"INSServerAPIManagerEditUserDidFinishNotification";
NSString *const INSServerAPIManagerEditUserFailedNotification = @"INSServerAPIManagerEditUserFailedNotification";

//    Parameters:
//    m='user-edit'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    height='5'11'
//    identity='insomniac' (Film Buff,Insomniac,Inspirer,etc...)
//    body_type='average' (Average,Athletic,A Little Extra,Big & Tall / BBW,Slim)
//    looking_for='love' (a partner in crime,love,a soulmate,a relationship,friends first,some love)
//    sexual_preference='straight' (straight,lesbian,gay,bisexual,transgender)
//    smoker='no' (yes,no)
//    children='1' (0 = no,1 = yes,2 = Does not live with me)
//    audio_chat='1' (0=disabled,1=enabled)
//    video_chat='1' (0=disabled,1=enabled)

- (void)editUserWithUserName:(NSString *)userName
                      height:(NSString *)height
                    identity:(NSString *)identity
                   ethnicity:(NSString *)ethnicity
                    bodyType:(NSString *)bodyType
                  lookingFor:(NSString *)lookingFor
            sexualPreference:(NSString *)sexualPreference
                      smoker:(NSString *)smoker
                    children:(NSString *)children
                   instagram:(NSString *)instagram
                        vine:(NSString *)vine
            audioChatEnabled:(NSNumber *)isAudioChatEnabled
            videoChatEnabled:(NSNumber *)isVideoChatEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-edit"];
    [parameters setKeySafelyINS:INSAPIKeyUserName andObject:userName];
    [parameters setKeySafelyINS:INSAPIKeyHeight andObject:height];
    [parameters setKeySafelyINS:INSAPIKeyIdentity andObject:identity];
    [parameters setKeySafelyINS:INSAPIKeyEthnicity andObject:ethnicity];
    [parameters setKeySafelyINS:INSAPIKeyBodyType andObject:bodyType];
    [parameters setKeySafelyINS:INSAPIKeyLookingFor andObject:lookingFor];
    [parameters setKeySafelyINS:INSAPIKeySexualPreference andObject:sexualPreference];
    [parameters setKeySafelyINS:INSAPIKeySmoker andObject:smoker];
    [parameters setKeySafelyINS:INSAPIKeyChildren andObject:[INSServerAPIManager serverCodeForChildrenStatus:children]];
    [parameters setKeySafelyINS:INSAPIKeyInstagram andObject:instagram];
    [parameters setKeySafelyINS:INSAPIKeyVine andObject:vine];
    
    if (isAudioChatEnabled != nil)
    {
        [parameters setKeySafelyINS:INSAPIKeyAudioChat andObject:([isAudioChatEnabled boolValue]) ? @"1" : @"0"];
    }
    
    if (isVideoChatEnabled != nil)
    {
        [parameters setKeySafelyINS:INSAPIKeyVideoChat andObject:([isVideoChatEnabled boolValue]) ? @"1" : @"0"];
    }
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerEditUserDidFinishNotification
            failureNotificationName:INSServerAPIManagerEditUserFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Change Email

NSString *const INSServerAPIManagerChangeEmailDidFinishNotification = @"INSServerAPIManagerChangeEmailDidFinishNotification";
NSString *const INSServerAPIManagerChangeEmailFailedNotification = @"INSServerAPIManagerChangeEmailFailedNotification";

//    Parameters:
//    m='user-change-email'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    email='test@email.com'

- (void)changeEmailAddress:(NSString *)newEmailAddress
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-change-email"];
    [parameters setKeySafelyINS:INSAPIKeyEmail andObject:newEmailAddress];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerChangeEmailDidFinishNotification
            failureNotificationName:INSServerAPIManagerChangeEmailFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Change Password

NSString *const INSServerAPIManagerChangePasswordDidFinishNotification = @"INSServerAPIManagerChangePasswordDidFinishNotification";
NSString *const INSServerAPIManagerChangePasswordFailedNotification = @"INSServerAPIManagerChangePasswordFailedNotification";

//    Parameters:
//    m='user-change-pwd'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    pwd='mypass'

- (void)changePassword:(NSString *)newPassword
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-change-pwd"];
    [parameters setKeySafelyINS:INSAPIKeyPassword andObject:newPassword];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerChangePasswordDidFinishNotification
            failureNotificationName:INSServerAPIManagerChangePasswordFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Deactivate User Account

NSString *const INSServerAPIManagerDeactivateUserAccountDidFinishNotification = @"INSServerAPIManagerDeactivateUserAccountDidFinishNotification";
NSString *const INSServerAPIManagerDeactivateUserAccountFailedNotification = @"INSServerAPIManagerDeactivateUserAccountFailedNotification";

//    Parameters:
//    m='user-set-deactive'
//    sessionId='52b947f0e612614d934127d16220bb77'

- (void)deactivateUserAccount
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-set-deactive"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerDeactivateUserAccountDidFinishNotification
            failureNotificationName:INSServerAPIManagerDeactivateUserAccountFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Delete User Account

NSString *const INSServerAPIManagerDeleteUserAccountDidFinishNotification = @"INSServerAPIManagerDeleteUserAccountDidFinishNotification";
NSString *const INSServerAPIManagerDeleteUserAccountFailedNotification = @"INSServerAPIManagerDeleteUserAccountFailedNotification";

//    Parameters:
//    m='user-delete'
//    sessionId='52b947f0e612614d934127d16220bb77'

- (void)deleteUserAccount
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-delete"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerDeleteUserAccountDidFinishNotification
            failureNotificationName:INSServerAPIManagerDeleteUserAccountFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Block User

NSString *const INSServerAPIManagerBlockUserDidFinishNotification = @"INSServerAPIManagerBlockUserDidFinishNotification";
NSString *const INSServerAPIManagerBlockUserFailedNotification = @"INSServerAPIManagerBlockUserFailedNotification";

//    Parameters:
//    m='block-user'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    blockedUid='1127'

- (void)blockUserForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"block-user"];
    [parameters setKeySafelyINS:INSAPIKeyUserID andObject:userID];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerBlockUserDidFinishNotification
            failureNotificationName:INSServerAPIManagerBlockUserFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Edit User Profile Pic

NSString *const INSServerAPIManagerEditUserProfilePicDidFinishNotification = @"INSServerAPIManagerEditUserProfilePicDidFinishNotification";
NSString *const INSServerAPIManagerEditUserProfilePicFailedNotification = @"INSServerAPIManagerEditUserProfilePicFailedNotification";

//    Parameters:
//    m='image-add'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    image=FILE

- (void)editUserProfilePicWithImage:(UIImage *)image
                             orData:(NSData *)data
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ( (image == nil) && (data == nil) )
    {
        // TODO: Error notification
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No image or data", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if ( (data == nil) && (image != nil) )
    {
        data = UIImagePNGRepresentation(image);
        
        if (data == nil)
        {
            // TODO: Error notification
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Could not make data from image", self, __PRETTY_FUNCTION__);
            return;
        }
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"image-add"];
    [parameters setKeySafelyINS:INSAPIKeySessionID andObject:[[[INSUserManager sharedManager] currentUser] sessionID]];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];

    AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
    [requestOperationManager setResponseSerializer:[AFJSONResponseSerializer serializer]];

    [requestOperationManager POST:INSServerRequestPath
                       parameters:parameters
        constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:data
                                        name:@"image"
                                    fileName:@"FILE.png"
                                    mimeType:@"image/png"];
            
        }
                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                              
                              NSLog(@"Image upload succeeded");
                              [notificationCenter postNotificationNameOnMainThreadBTI:INSServerAPIManagerEditUserProfilePicDidFinishNotification
                                                                               object:responseObject
                                                                             userInfo:parameters];
                              
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                              
                              NSLog(@"Image upload succeeded");
                              [notificationCenter postNotificationNameOnMainThreadBTI:INSServerAPIManagerEditUserProfilePicFailedNotification
                                                                               object:error
                                                                             userInfo:parameters];
                              
                          }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Push Notifications

NSString *const INSServerAPIManagerPushNotificationServiceAllNotifications = @"enabled";
NSString *const INSServerAPIManagerPushNotificationServiceMyHeartGetsPushed = @"heart_pushed";
NSString *const INSServerAPIManagerPushNotificationServiceSomeoneWatchesMyVideo = @"watched_video";
NSString *const INSServerAPIManagerPushNotificationServiceSomeoneKissedMe = @"kissed";
NSString *const INSServerAPIManagerPushNotificationServiceNewCommentPosted = @"new_comment";
NSString *const INSServerAPIManagerPushNotificationServiceNewAmourAdded = @"new_amour";
NSString *const INSServerAPIManagerPushNotificationServiceInstantChatRequest = @"instant_chat";
NSString *const INSServerAPIManagerPushNotificationServiceVideoCallRequest = @"video_call";
NSString *const INSServerAPIManagerPushNotificationServiceAudioCallRequest = @"phone_call";
NSString *const INSServerAPIManagerPushNotificationServiceIReceiveAGift = @"received_gift";

NSString *const INSServerAPIManagerTogglePushNotificationsDidFinishNotification = @"INSServerAPIManagerTogglePushNotificationsDidFinishNotification";
NSString *const INSServerAPIManagerTogglePushNotificationsFailedNotification = @"INSServerAPIManagerTogglePushNotificationsFailedNotification";

//    Parameters:
//    m='user-push-notification'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    type='heart_pushed'
//    status = enable or 0      // "Off" value doesn't really matter. Anything other than "enable", "enabled", or "1" will turn off.

- (void)setPushNotificationEnabled:(BOOL)isEnabled
                        forService:(NSString *)serviceType
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"user-push-notification"];
    [parameters setKeySafelyINS:INSAPIKeyStatus andObject:(isEnabled) ? @"enable" : @"0"];
    [parameters setKeySafelyINS:INSAPIKeyType andObject:serviceType];
    
    NSLog(@"parameters: %@", parameters);
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerTogglePushNotificationsDidFinishNotification
            failureNotificationName:INSServerAPIManagerTogglePushNotificationsFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Flag User

//    Parameters:
//    m='flag'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    uid='1200'

NSString *const INSServerAPIManagerFlagUserDidFinishNotification = @"INSServerAPIManagerFlagUserDidFinishNotification";
NSString *const INSServerAPIManagerFlagUserFailedNotification = @"INSServerAPIManagerFlagUserFailedNotification";

- (void)flagUserWithIdentifier:(NSString *)userID
                        reason:(NSString *)reason
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"flag"];
    [parameters setKeySafelyINS:INSAPIKeyUserID andObject:userID];
    [parameters setKeySafelyINS:INSAPIKeyReason andObject:reason];

    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerFlagUserDidFinishNotification
            failureNotificationName:INSServerAPIManagerFlagUserFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Comment Adding

NSString *const INSServerAPIManagerAddCommentDidFinishNotification = @"INSServerAPIManagerAddCommentDidFinishNotification";
NSString *const INSServerAPIManagerAddCommentFailedNotification = @"INSServerAPIManagerAddCommentFailedNotification";

//    Parameters:
//    m='comment-add'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    uid='1127'
//    body='this is a message'

- (void)addComment:(NSString *)comment
          toUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"comment-add"];
    [parameters setKeySafelyINS:INSAPIKeyUserID andObject:userID];
    [parameters setKeySafelyINS:INSAPIKeyBody andObject:comment];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerAddCommentDidFinishNotification
            failureNotificationName:INSServerAPIManagerAddCommentFailedNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Comment Editing

NSString *const INSServerAPIManagerEditCommentDidFinishNotification = @"INSServerAPIManagerEditCommentDidFinishNotification";
NSString *const INSServerAPIManagerEditCommentFailedNotification = @"INSServerAPIManagerEditCommentFailedNotification";

//    Parameters:
//    m='comment-edit'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    id='101'
//    body='this is a edited message'

- (void)editCommentWithID:(NSString *)commentID
                toNewBody:(NSString *)body
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"comment-edit"];
    [parameters setKeySafelyINS:INSAPIKeyIdentifier andObject:commentID];
    [parameters setKeySafelyINS:INSAPIKeyBody andObject:body];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerEditCommentDidFinishNotification
            failureNotificationName:INSServerAPIManagerEditCommentFailedNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Comment Deleting

NSString *const INSServerAPIManagerDeleteCommentDidFinishNotification = @"INSServerAPIManagerDeleteCommentDidFinishNotification";
NSString *const INSServerAPIManagerDeleteCommentFailedNotification = @"INSServerAPIManagerDeleteCommentFailedNotification";

//    Parameters:
//    m='comment-delete'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    id='102'

- (void)deleteCommentWithID:(NSString *)commentID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"comment-delete"];
    [parameters setKeySafelyINS:INSAPIKeyIdentifier andObject:commentID];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerDeleteCommentDidFinishNotification
            failureNotificationName:INSServerAPIManagerDeleteCommentFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Kiss Add

NSString *const INSServerAPIManagerKissAddDidFinishNotification = @"INSServerAPIManagerKissAddDidFinishNotification";
NSString *const INSServerAPIManagerKissAddFailedNotification = @"INSServerAPIManagerKissAddFailedNotification";

//    Parameters:
//    m='kiss-add'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    uid='1127'

- (void)kissAddForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"kiss-add"];
    [parameters setKeySafelyINS:INSAPIKeyUserID andObject:userID];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerKissAddDidFinishNotification
            failureNotificationName:INSServerAPIManagerKissAddFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Kiss Delete

NSString *const INSServerAPIManagerKissDeleteDidFinishNotification = @"INSServerAPIManagerKissDeleteDidFinishNotification";
NSString *const INSServerAPIManagerKissDeleteFailedNotification = @"INSServerAPIManagerKissDeleteFailedNotification";

//    Parameters:
//    m='kiss-delete'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    uid='1127'

- (void)kissDeleteForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"kiss-delete"];
    [parameters setKeySafelyINS:INSAPIKeyUserID andObject:userID];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerKissDeleteDidFinishNotification
            failureNotificationName:INSServerAPIManagerKissDeleteFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Search User

//    Parameters:
//    m='search'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    count='20'
//    page='1'
//    uname='evan'
//    distance='5'
//    min_age='18'
//    max_age='99'
//    video_count='1'
//    gender='female,male'
//    body_type='slim'
//    looking_for='love'
//    sexual_preference='straight' (straight,lesbian,gay,bisexual,transgender)
//    smoker='yes'
//    children='0' (0 = no,1 = yes,2 = Does not live with me)
//    ethnicity='asian'
//    city='Philadelphia'
//    state='PA'
//    country='US'

- (void)searchUserWithPageNumber:(NSInteger)pageNumber
                 numberOfResults:(NSInteger)numberOfResults
                        userName:(NSString *)userName
                        distance:(NSString *)distance
                      minimumAge:(NSInteger)minimumAge
                      maximumAge:(NSInteger)maximumAge
                  numberOfVideos:(NSInteger)numberOfVideos
                          gender:(NSString *)gender
                        bodyType:(NSString *)bodyType
                      lookingFor:(NSString *)lookingFor
                sexualPreference:(NSString *)sexualPreference
                          smoker:(NSString *)smoker
                        children:(NSString *)children
                            city:(NSString *)city
                           state:(NSString *)state
                         country:(NSString *)country
                       instagram:(NSString *)instagram
                            vine:(NSString *)vine
                       ethnicity:(NSString *)ethnicity
         successNotificationName:(NSString *)successNotificationName
         failureNotificationName:(NSString *)failureNotificationName
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
        
    NSString *childrenCode = [INSServerAPIManager serverCodeForChildrenStatus:children];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"search"];
    [parameters setKeySafelyINS:INSAPIKeyPageNumber andObject:[self stringFromInteger:pageNumber]];
    [parameters setKeySafelyINS:INSAPIKeyNumberOfResults andObject:[self stringFromInteger:numberOfResults]];
    [parameters setKeySafelyINS:INSAPIKeyUserName andObject:[userName isNotEmpty] ? userName : nil];
    [parameters setKeySafelyINS:INSAPIKeyDistance andObject:[distance isNotEmpty] ? distance : nil];
    [parameters setKeySafelyINS:INSAPIKeyMinimumAge andObject:[self stringFromInteger:minimumAge]];
    [parameters setKeySafelyINS:INSAPIKeyMaximumAge andObject:[self stringFromInteger:maximumAge]];
    [parameters setKeySafelyINS:INSAPIKeyNumberOfVideos andObject:[self stringFromInteger:numberOfVideos]];
    [parameters setKeySafelyINS:INSAPIKeyGender andObject:[gender isNotEmpty] ? gender : nil];
    [parameters setKeySafelyINS:INSAPIKeyBodyType andObject:[bodyType isNotEmpty] ? bodyType : nil];
    [parameters setKeySafelyINS:INSAPIKeyLookingFor andObject:[lookingFor isNotEmpty] ? lookingFor : nil];
    [parameters setKeySafelyINS:INSAPIKeySexualPreference andObject:[sexualPreference isNotEmpty] ? sexualPreference : nil];
    [parameters setKeySafelyINS:INSAPIKeySmoker andObject:[smoker isNotEmpty] ? smoker : nil];
    [parameters setKeySafelyINS:INSAPIKeyChildren andObject:[childrenCode isNotEmpty] ? childrenCode : nil];
    [parameters setKeySafelyINS:INSAPIKeyCity andObject:[city isNotEmpty] ? city : nil];
    [parameters setKeySafelyINS:INSAPIKeyState andObject:[state isNotEmpty] ? state : nil];
    [parameters setKeySafelyINS:INSAPIKeyCountry andObject:[country isNotEmpty] ? country : nil];
    [parameters setKeySafelyINS:INSAPIKeyInstagram andObject:[instagram isNotEmpty] ? instagram : nil];
    [parameters setKeySafelyINS:INSAPIKeyVine andObject:[vine isNotEmpty] ? vine : nil];
    [parameters setKeySafelyINS:INSAPIKeyEthnicity andObject:[ethnicity isNotEmpty] ? ethnicity : nil];
    
    NSLog(@"Search parameters: \n\n%@\n\n", parameters);
    
    [self postServiceWithParameters:parameters
            successNotificationName:successNotificationName
            failureNotificationName:failureNotificationName];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

NSString *const INSServerAPIManagerBrowserUserDidFinishNotification = @"INSServerAPIManagerBrowserUserDidFinishNotification";
NSString *const INSServerAPIManagerBrowserUserFailedNotification = @"INSServerAPIManagerBrowserUserFailedNotification";

- (void)downloadBrowserUsersForPageNumber:(NSInteger)pageNumber
                          numberOfResults:(NSInteger)numberOfResults
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self searchUserWithPageNumber:pageNumber
                   numberOfResults:numberOfResults
                          userName:nil
                          distance:nil
                        minimumAge:INSAgeRangeMinimum
                        maximumAge:INSAgeRangeMaximum
                    numberOfVideos:NSNotFound
                            gender:[[NSUserDefaults standardUserDefaults] browserProfilesSearchGenderINS]
                          bodyType:nil
                        lookingFor:nil
                  sexualPreference:nil
                            smoker:nil
                          children:nil
                              city:nil
                             state:nil
                           country:nil
                         instagram:nil
                              vine:nil
                         ethnicity:nil
           successNotificationName:INSServerAPIManagerBrowserUserDidFinishNotification
           failureNotificationName:INSServerAPIManagerBrowserUserFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

NSString *const INSServerAPIManagerSearchUserDidFinishNotification = @"INSServerAPIManagerSearchUserDidFinishNotification";
NSString *const INSServerAPIManagerSearchUserFailedNotification = @"INSServerAPIManagerSearchUserFailedNotification";

- (void)downloadSearchUsersForPageNumber:(NSInteger)pageNumber
                         numberOfResults:(NSInteger)numberOfResults
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self searchUserWithPageNumber:pageNumber
                   numberOfResults:numberOfResults
                          userName:[userDefaults searchTermUserNameINS]
                          distance:[userDefaults searchTermMilesINS]
                        minimumAge:[userDefaults searchTermMinimumAgeINS]
                        maximumAge:[userDefaults searchTermMaximumAgeINS]
                    numberOfVideos:[userDefaults searchTermVideoCountINS]
                            gender:[userDefaults searchTermGenderINS]
                          bodyType:[userDefaults searchTermBodyTypeINS]
                        lookingFor:[userDefaults searchTermLookingForINS]
                  sexualPreference:[userDefaults searchTermSexualPreferenceINS]
                            smoker:[userDefaults searchTermSmokerINS]
                          children:[userDefaults searchTermChildrenINS]
                              city:nil
                             state:nil
                           country:nil
                         instagram:nil
                              vine:nil
                         ethnicity:[userDefaults searchTermEthnicityINS]
           successNotificationName:INSServerAPIManagerSearchUserDidFinishNotification
           failureNotificationName:INSServerAPIManagerSearchUserFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Your Amours (Friends List)

NSString *const INSServerAPIManagerYourAmoursDidFinishNotification = @"INSServerAPIManagerYourAmoursDidFinishNotification";
NSString *const INSServerAPIManagerYourAmoursFailedNotification = @"INSServerAPIManagerYourAmoursFailedNotification";

//    Parameters:
//    m='friends-get'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    type='accept'

- (void)downloadYourAmours
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"friends-get"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerYourAmoursDidFinishNotification
            failureNotificationName:INSServerAPIManagerYourAmoursFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Pending Amours (Friends Receiver Pending)

NSString *const INSServerAPIManagerPendingAmoursDidFinishNotification = @"INSServerAPIManagerPendingAmoursDidFinishNotification";
NSString *const INSServerAPIManagerPendingAmoursFailedNotification = @"INSServerAPIManagerPendingAmoursFailedNotification";

//Parameters:
//m='friends-pending-receiver'
//sessionId='52b947f0e612614d934127d16220bb77'

- (void)downloadPendingAmours
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"friends-pending-receiver"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerPendingAmoursDidFinishNotification
            failureNotificationName:INSServerAPIManagerPendingAmoursFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Friends Sender Pending

#pragma mark Hearts Pushed (Friends Sender Pending)

NSString *const INSServerAPIManagerHeartsPushedDidFinishNotification = @"INSServerAPIManagerHeartsPushedDidFinishNotification";
NSString *const INSServerAPIManagerHeartsPushedFailedNotification = @"INSServerAPIManagerHeartsPushedFailedNotification";

//    Parameters:
//    m='friends-pending-sender'
//    sessionId='52b947f0e612614d934127d16220bb77'

- (void)downloadHeartsPushed
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"friends-pending-sender"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerHeartsPushedDidFinishNotification
            failureNotificationName:INSServerAPIManagerHeartsPushedFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Friend Status

//    Parameters:
//    m='friends-status'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    uid='1200'
//    status='pending'

- (void)setFriendStatus:(NSString *)status
              forUserID:(NSString *)userID
successNotificationName:(NSString *)successNotificationName
failureNotificationName:(NSString *)failureNotificationName
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"friends-status"];
    [parameters setKeySafelyINS:INSAPIKeyStatus andObject:status];
    [parameters setKeySafelyINS:INSAPIKeyUserID andObject:userID];
    
    [self postServiceWithParameters:parameters
            successNotificationName:successNotificationName
            failureNotificationName:failureNotificationName];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

NSString *const INSServerAPIManagerFriendStatusAcceptDidFinishNotification = @"INSServerAPIManagerFriendStatusAcceptDidFinishNotification";
NSString *const INSServerAPIManagerFriendStatusAcceptFailedNotification = @"INSServerAPIManagerFriendStatusAcceptFailedNotification";

- (void)setFriendStatusAcceptForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setFriendStatus:@"accept"
                forUserID:userID
  successNotificationName:INSServerAPIManagerFriendStatusAcceptDidFinishNotification
  failureNotificationName:INSServerAPIManagerFriendStatusAcceptFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

NSString *const INSServerAPIManagerFriendStatusPendingDidFinishNotification = @"INSServerAPIManagerFriendStatusPendingDidFinishNotification";
NSString *const INSServerAPIManagerFriendStatusPendingFailedNotification = @"INSServerAPIManagerFriendStatusPendingFailedNotification";

- (void)setFriendStatusPendingForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setFriendStatus:@"pending"
                forUserID:userID
  successNotificationName:INSServerAPIManagerFriendStatusPendingDidFinishNotification
  failureNotificationName:INSServerAPIManagerFriendStatusPendingFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

NSString *const INSServerAPIManagerFriendStatusDeletedDidFinishNotification = @"INSServerAPIManagerFriendStatusDeletedDidFinishNotification";
NSString *const INSServerAPIManagerFriendStatusDeletedFailedNotification = @"INSServerAPIManagerFriendStatusDeletedFailedNotification";

- (void)setFriendStatusDeletedForUserID:(NSString *)userID
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setFriendStatus:@"deleted"
                forUserID:userID
  successNotificationName:INSServerAPIManagerFriendStatusDeletedDidFinishNotification
  failureNotificationName:INSServerAPIManagerFriendStatusDeletedFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Video Upload

NSString *const INSServerAPIManagerVideoUploadDidFinishNotification = @"INSServerAPIManagerVideoUploadDidFinishNotification";
NSString *const INSServerAPIManagerVideoUploadFailedNotification = @"INSServerAPIManagerVideoUploadFailedNotification";

//    Parameters:
//    m='video-add'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    video=UPLOAD FILE
//    count='3'
//    rotate='0'

- (void)uploadVideoFromFileURL:(NSURL *)fileURL
           forServerSlotNumber:(NSNumber *)serverSlotNumber
                      rotation:(INSServerAPIManagerVideoRotation)rotation
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (fileURL == nil)
    {
        // TODO: Error notification
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No URL", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        // TODO: Error notification
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file at URL", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSInteger slotNumber = [serverSlotNumber integerValue];
    
    if ( (serverSlotNumber == nil) || (slotNumber == 0) )
    {
        // TODO: Error notification
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid slot number", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"video-add"];
    [parameters setKeySafelyINS:INSAPIKeySessionID andObject:[[[INSUserManager sharedManager] currentUser] sessionID]];
    [parameters setKeySafelyINS:INSAPIKeyCount andObject:[NSString stringWithFormat:@"%ld", (long)slotNumber]];
    [parameters setKeySafelyINS:INSAPIKeyRotate andObject:[NSString stringWithFormat:@"%d", (int)rotation]];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
//    AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
//    [requestOperationManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    
//    [requestOperationManager POST:ServerRequestPath
//                       parameters:parameters
//        constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//            
//            [formData appendPartWithFileURL:fileURL
//                                       name:INSAPIKeyVideo
//                                      error:nil];
//
//        }
//                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                              
//                              [notificationCenter postNotificationNameOnMainThreadBTI:INSServerAPIManagerVideoUploadDidFinishNotification
//                                                                               object:responseObject
//                                                                             userInfo:parameters];
//                              
//                          }
//                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                              
//                              [notificationCenter postNotificationNameOnMainThreadBTI:INSServerAPIManagerVideoUploadFailedNotification
//                                                                               object:error
//                                                                             userInfo:parameters];
//                              
//                          }];

    // Same thing, but shows progress:
    
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST"
                                                                    URLString:INSServerRequestPath
                                                                   parameters:parameters
                                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                        
                                                        [formData appendPartWithFileURL:fileURL
                                                                                   name:INSAPIKeyVideo
                                                                                  error:nil];

                                                    }
                                                                        error:nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation =
    [manager HTTPRequestOperationWithRequest:request
                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {

                              [notificationCenter postNotificationNameOnMainThreadBTI:INSServerAPIManagerVideoUploadDidFinishNotification
                                                                               object:responseObject
                                                                             userInfo:parameters];
                              
                          }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {

                              [notificationCenter postNotificationNameOnMainThreadBTI:INSServerAPIManagerVideoUploadFailedNotification
                                                                               object:error
                                                                             userInfo:parameters];
                              
                          }];
    
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
        
        if (totalBytesWritten >= totalBytesExpectedToWrite)
        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Test Message"
//                                                            message:@"The entire file size has been uploaded. So if an error happens after this, something doesn't make sense."
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"Ok"
//                                                  otherButtonTitles:nil];
//            [alert show];
        }
        
    }];
    
    [operation start];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Video Delete

NSString *const INSServerAPIManagerVideoDeleteDidFinishNotification = @"INSServerAPIManagerVideoDeleteDidFinishNotification";
NSString *const INSServerAPIManagerVideoDeleteFailedNotification = @"INSServerAPIManagerVideoDeleteFailedNotification";

//    Parameters:
//    m='video-delete'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    count='3'

- (void)deleteVideoForServerSlotNumber:(NSNumber *)serverSlotNumber
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSInteger slotNumber = [serverSlotNumber integerValue];
    
    if ( (serverSlotNumber == nil) || (slotNumber == 0) )
    {
        // TODO: Error notification
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid slot number", self, __PRETTY_FUNCTION__);
        return;
    }

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"video-delete"];
    [parameters setKeySafelyINS:INSAPIKeyCount andObject:[NSString stringWithFormat:@"%ld", (long)slotNumber]];

    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerVideoDeleteDidFinishNotification
            failureNotificationName:INSServerAPIManagerVideoDeleteFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Video Rotate


#pragma mark - Video Sort

NSString *const INSServerAPIManagerVideoSortDidFinishNotification = @"INSServerAPIManagerVideoSortDidFinishNotification";
NSString *const INSServerAPIManagerVideoSortFailedNotification = @"INSServerAPIManagerVideoSortFailedNotification";

- (void)sortVideosWithNewSlotOrder:(NSArray *)slotOrder
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"videos-sort"];
    [parameters setKeySafelyINS:INSAPIKeyOrder andObject:slotOrder];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerVideoSortDidFinishNotification
            failureNotificationName:INSServerAPIManagerVideoSortFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Create Merge Video

NSString *const INSServerAPIManagerVideoCreateMergeDidFinishNotification = @"INSServerAPIManagerVideoCreateMergeDidFinishNotification";
NSString *const INSServerAPIManagerVideoCreateMergeFailedNotification = @"INSServerAPIManagerVideoCreateMergeFailedNotification";

- (void)createMergeVideo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"video-create-merge"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerVideoCreateMergeDidFinishNotification
            failureNotificationName:INSServerAPIManagerVideoCreateMergeFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Feedback

NSString *const INSServerAPIManagerFeedbackDidFinishNotification = @"INSServerAPIManagerFeedbackDidFinishNotification";
NSString *const INSServerAPIManagerFeedbackFailedNotification = @"INSServerAPIManagerFeedbackFailedNotification";

//    Parameters:
//    m='send-feedback'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    body='This is my feedback to the app!'

- (void)sendFeedback:(NSString *)messageBody
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"send-feedback"];
    [parameters setKeySafelyINS:INSAPIKeyBody andObject:messageBody];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerFeedbackDidFinishNotification
            failureNotificationName:INSServerAPIManagerFeedbackFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Chat Files

//    Parameters:
//    m='chat-file-add'
//    sessionId='52b947f0e612614d934127d16220bb77'
//    file=FILE

- (void)uploadChatImage:(UIImage *)image
                success:(void (^)(NSString *urlString))success
                failure:(void (^)(NSString *errorMessage))failure
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSData *data = UIImagePNGRepresentation(image);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"chat-file-add"];
    [parameters setKeySafelyINS:INSAPIKeySessionID andObject:[[[INSUserManager sharedManager] currentUser] sessionID]];
    
    [[self requestOperationManager] POST:INSServerRequestPath
                              parameters:parameters
               constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                   
                   [formData appendPartWithFileData:data
                                               name:@"file"
                                           fileName:@"FILE.png"
                                           mimeType:@"image/png"];
                   
               }
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     
                                     NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
                                     NSLog(@"jsonDictionary: %@", jsonDictionary);
                                     
                                     if ([jsonDictionary isStatusSuccessfulINS])
                                     {
                                         NSString *urlString = [jsonDictionary chatFileURLStringINS];
                                         
                                         if ([urlString isNotEmpty])
                                         {
                                             if (success != nil)
                                             {
                                                 success(urlString);
                                             }
                                         }
                                         else
                                         {
                                             if (failure != nil)
                                             {
                                                 failure(@"No file URL from server");
                                             }
                                         }
                                     }
                                     else
                                     {
                                         if (failure != nil)
                                         {
                                             failure([jsonDictionary errorMessageINS]);
                                         }
                                     }
                                     
                                 }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                     
                                     if (failure != nil)
                                     {
                                         failure([error localizedDescription]);
                                     }
                                     
                                 }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)uploadChatVideoAtURL:(NSURL *)fileURL
                     success:(void (^)(NSString *urlString))success
                     failure:(void (^)(NSString *errorMessage))failure
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"chat-file-add"];
    [parameters setKeySafelyINS:INSAPIKeySessionID andObject:[[[INSUserManager sharedManager] currentUser] sessionID]];
    
    [[self requestOperationManager] POST:INSServerRequestPath
                              parameters:parameters
               constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                   
                   [formData appendPartWithFileURL:fileURL
                                              name:@"file"
                                          fileName:@"FILE.mp4"
                                          mimeType:@"video/mp4"
                                             error:nil];
                   
               }
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     
                                     NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
                                     NSLog(@"jsonDictionary: %@", jsonDictionary);

                                     if ([jsonDictionary isStatusSuccessfulINS])
                                     {
                                         NSString *urlString = [jsonDictionary chatFileURLStringINS];
                                         
                                         if ([urlString isNotEmpty])
                                         {
                                             if (success != nil)
                                             {
                                                 success(urlString);
                                             }
                                         }
                                         else
                                         {
                                             if (failure != nil)
                                             {
                                                 failure(@"No file URL from server");
                                             }
                                         }
                                     }
                                     else
                                     {
                                         if (failure != nil)
                                         {
                                             failure([jsonDictionary errorMessageINS]);
                                         }
                                     }
                                     
                                 }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                     
                                     if (failure != nil)
                                     {
                                         failure([error localizedDescription]);
                                     }
                                     
                                 }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark Ping

- (void)startPing
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopPing];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:10 * 60            // Every 10 minutes
                                                      target:self
                                                    selector:@selector(pingTimerFireMethod:)
                                                    userInfo:nil
                                                     repeats:YES];
    [self setPingTimer:timer];
    
    [timer fire];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopPing
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self pingTimer] invalidate];
    
    [self setPingTimer:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)pingTimerFireMethod:(NSTimer *)timer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[[INSUserManager sharedManager] currentUser] isOnline])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }

    [self ping];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

NSString *const INSServerAPIManagerPingDidFinishNotification = @"INSServerAPIManagerPingDidFinishNotification";
NSString *const INSServerAPIManagerPingFailedNotification = @"INSServerAPIManagerPingFailedNotification";

//    Parameters:
//    m='ping'
//    sessionId='52b947f0e612614d934127d16220bb77'

- (void)ping;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setKeySafelyINS:INSAPIKeyServiceCode andObject:@"ping"];
    
    [self postServiceWithParameters:parameters
            successNotificationName:INSServerAPIManagerPingDidFinishNotification
            failureNotificationName:INSServerAPIManagerPingFailedNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
