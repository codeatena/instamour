//
//  INSHomeViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSHomeViewController.h"

// Models and other global

// Sub-controllers
#import "INSLoginViewController.h"
#import "INSSignUpViewController.h"

// Views
#import "INSHomeCollectionViewCell.h"

// Private Constants


@interface INSHomeViewController () <UICollectionViewDelegateFlowLayout>

// Private Properties
@property (nonatomic, weak) NSTimer *scrollTimer;

@end

@implementation INSHomeViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    UIBarButtonItem *previewButton = [[UIBarButtonItem alloc] initWithTitle:@"Preview"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(previewButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:previewButton];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage navigationBarLogoINS]];
    [[self navigationItem] setTitleView:imageView];
    
    // Misc
    
    [[INSLocationManager sharedManager] startTrackingLocation];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    [Flurry logEvent:@"Home/First/Start Up View"];
    
    // User Interface
    
    [self populateContents];
    
    [self startScrollTimer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillDisappear:animated];
    
    [self stopScrollTimer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self mainContents] removeAllObjects];
    [[self mainContents] addObjectsFromArray:@[ @"screen-1", @"screen-2", @"screen-3", @"screen-4" ]];

    [[self collectionView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTICollectionViewController Overrides

- (void)registerNibsForCollectionView:(UICollectionView *)collectionView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [INSHomeCollectionViewCell registerNibForCollectionViewBTI:collectionView];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers


#pragma mark - UI Response Methods

- (void)previewButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
        
    [[AppDelegate sharedDelegate] showPreviewScreen];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)signUpButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSSignUpViewController *nextViewController = [[INSSignUpViewController alloc] initWithNibName:nil bundle:nil];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)loginButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSLoginViewController *nextViewController = [[INSLoginViewController alloc] init];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)startScrollTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopScrollTimer];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:3
                                                      target:self
                                                    selector:@selector(scrollTimerFireMethod:)
                                                    userInfo:nil
                                                     repeats:YES];
    [self setScrollTimer:timer];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopScrollTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self scrollTimer] invalidate];
    
    [self setScrollTimer:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)scrollTimerFireMethod:(NSTimer *)timer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UICollectionView *collectionView = [self collectionView];
    
    NSArray *visibleCells = [collectionView visibleCells];
    NSIndexPath *indexPath = [collectionView indexPathForCell:[visibleCells lastObject]];
    
    NSInteger row = [indexPath row];
    NSInteger nextRow = row + 1;
    
    if (nextRow >= [[self mainContents] count])
    {
        nextRow = 0;
    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:nextRow inSection:[indexPath section]];
    
    [collectionView scrollToItemAtIndexPath:nextIndexPath
                           atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                   animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopScrollTimer];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    if (!decelerate)
    {
        [self startScrollTimer];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self startScrollTimer];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [[self mainContents] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    INSHomeCollectionViewCell *cell = [INSHomeCollectionViewCell dequeueCellFromCollectionViewBTI:collectionView forIndexPath:indexPath];
    
    NSString *imageName = [[self mainContents] objectAtIndex:[indexPath row]];
    
    [[cell imageView] setImage:[UIImage imageNamed:imageName]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout Methods

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return [collectionView frame].size;
}

@end
