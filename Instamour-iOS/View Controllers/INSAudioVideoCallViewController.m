//
//  INSAudioVideoCallViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/2/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSAudioVideoCallViewController.h"

// Models and other global
#import "PBJVision.h"

// Sub-controllers

// Views

// Private Constants

typedef NS_ENUM(NSInteger, INSAudioVideoCallMode) {
    INSAudioVideoCallModeOffline,
    INSAudioVideoCallModeDialing,
    INSAudioVideoCallModeConnecting,
    INSAudioVideoCallModeChatting,
};

@interface INSAudioVideoCallViewController ()

// Private Properties
@property (nonatomic, strong) IBOutlet UIImageView *opponentVideoContainerView;
@property (nonatomic, strong) IBOutlet UIImageView *currentUserVideoContainerView;
@property (nonatomic, strong) IBOutlet UIButton *callButton;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@property (nonatomic, strong) IBOutlet UIView *timerLabelContainerView;
@property (nonatomic, strong) IBOutlet UILabel *timerLabel;

@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) IBOutlet UIView *videoPreviewContainerView;

@property (nonatomic, assign) INSAudioVideoCallMode mode;

@property (nonatomic, weak) UIAlertView *incomingCallAlertView;
@property (nonatomic, assign) NSInteger incomingCallOpponentID;

@property (nonatomic, strong) QBVideoChat *videoChat;

@property (nonatomic, copy) NSDate *callStartDate;
@property (nonatomic, weak) NSTimer *durationTimer;

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

@end

@implementation INSAudioVideoCallViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:[[self otherUser] userName]];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewWillAppear:animated];
    
    // Analytics
    
    switch ([self callType])
    {
        case QBVideoChatConferenceTypeAudioAndVideo:
            [Flurry logEvent:@"Video Calling View"];
            break;
        case QBVideoChatConferenceTypeAudio:
            [Flurry logEvent:@"Phone Calling View"];
            break;
        default:
            break;
    }
    
    [self setMode:INSAudioVideoCallModeOffline];

    [self populateContents];

    if ([self isIncomingCall])
    {
        [self handleIncomingCallFromQuickBloxUserID:[[[self otherUser] quickBloxID] integerValue]
                                      withSessionID:[self incomingSessionID]
                                           callType:[self callType]];
    }
    
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewWillDisappear:animated];
 
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSLog(@"Populating Contents");
    
    INSUser *currentUser = [[INSUserManager sharedManager] currentUser];
    INSUser *otherUser = [self otherUser];
    
    UIView *mainView = [self view];
    UIImageView *currentUserImageView = [self currentUserVideoContainerView];
    UIImageView *otherUserImageview = [self opponentVideoContainerView];
    UIButton *callButton = [self callButton];
    UILabel *statusLabel = [self statusLabel];
    UIActivityIndicatorView *activityIndicator = [self activityIndicatorView];
    
    [callButton setEnabled:YES];
    
    PBJVision *vision = [PBJVision sharedInstance];
    UIView *videoPreviewContainerView = [self videoPreviewContainerView];

    switch ([self mode])
    {
        case INSAudioVideoCallModeOffline:
        {
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            
            NSLog(@"Offline");
            
            [[self navigationItem] setRightBarButtonItem:nil];
            
            NSString *buttonTitle = nil;
            
            switch ([self callType])
            {
                case QBVideoChatConferenceTypeAudioAndVideo:
                    buttonTitle = @"Start Video Call";
                    break;
                case QBVideoChatConferenceTypeAudio:
                    buttonTitle = @"Start Audio Call";
                    break;
                default:
                    break;
            }
            
            [callButton setTitle:buttonTitle forState:UIControlStateNormal];
            [currentUserImageView setImage:[currentUser avatarImage]];
            [otherUserImageview setImage:[otherUser avatarImage]];
            
            [statusLabel setHidden:YES];
            [activityIndicator stopAnimating];
            
            [self stopDurationTimer];
            
            if ([self callType] == QBVideoChatConferenceTypeAudioAndVideo)
            {
                [[self view] bringSubviewToFront:videoPreviewContainerView];
                
                AVCaptureVideoPreviewLayer *previewLayer = [vision previewLayer];
                [previewLayer removeFromSuperlayer];
                
                [previewLayer setFrame:[videoPreviewContainerView bounds]];
                [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
                [[videoPreviewContainerView layer] addSublayer:previewLayer];

                if ([vision isCameraDeviceAvailable:PBJCameraDeviceFront])
                {
                    [vision setCameraDevice:PBJCameraDeviceFront];
                }
                
                [vision stopPreview];
                [vision cancelVideoCapture];
                
                [vision startPreview];
                [vision unfreezePreview];
            }
            else
            {
                [[self view] sendSubviewToBack:videoPreviewContainerView];
            }
        }
            break;
        case INSAudioVideoCallModeDialing:
        {
            NSLog(@"Dialing");

            [callButton setTitle:@"End Call" forState:UIControlStateNormal];
            [currentUserImageView setImage:nil];
            [otherUserImageview setImage:nil];
            
            [statusLabel setHidden:NO];
            [statusLabel setText:@"Calling..."];
            
            [activityIndicator startAnimating];
            [mainView bringSubviewToFront:activityIndicator];
            
            [vision stopPreview];
            [[self view] sendSubviewToBack:videoPreviewContainerView];
        }
            break;
        case INSAudioVideoCallModeConnecting:
        {
            NSLog(@"Connecting");

            [callButton setTitle:@"End Call" forState:UIControlStateNormal];
            [currentUserImageView setImage:nil];
            [otherUserImageview setImage:nil];
            
            [statusLabel setHidden:NO];
            [statusLabel setText:@"Configuring..."];
            
            [activityIndicator startAnimating];
            [mainView bringSubviewToFront:activityIndicator];
            
            [vision stopPreview];
            [[self view] sendSubviewToBack:videoPreviewContainerView];
        }
            break;
        case INSAudioVideoCallModeChatting:
        {
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];

            NSLog(@"Chatting");

            [self showMuteButton];
            
            [callButton setTitle:@"End Call" forState:UIControlStateNormal];
            [currentUserImageView setImage:nil];
            [otherUserImageview setImage:nil];
            
            [statusLabel setHidden:YES];
            
            [self startDurationTimer];
            
            [activityIndicator stopAnimating];
            
            [vision stopPreview];
            [[self view] sendSubviewToBack:videoPreviewContainerView];
        }
            break;
        default:
            break;
    }
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSQuickBloxManagerChatCallRejectedNotification
                                   selector:@selector(chatCallWasRejectedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSQuickBloxManagerChatCallAcceptedNotification
                                   selector:@selector(chatCallWasAcceptedNotification:)
                                     object:nil];

    [self addVisibleNotificationInfoForName:INSQuickBloxManagerChatCallDidBeginNotification
                                   selector:@selector(chatCallDidBeginNotification:)
                                     object:nil];

    [self addVisibleNotificationInfoForName:INSQuickBloxManagerChatCallEndedByOtherUserNotification
                                   selector:@selector(chatCallEndedByOtherUserNotification:)
                                     object:nil];

    [self addVisibleNotificationInfoForName:INSQuickBloxManagerOtherUserDidNotAnswerNotification
                                   selector:@selector(otherUserDidNotAnswerNotification:)
                                     object:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)chatCallWasRejectedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSAudioVideoCallModeOffline];
    
    [self populateContents];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallWasAcceptedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSAudioVideoCallModeConnecting];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallDidBeginNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setCallStartDate:[NSDate date]];
    
    [self setMode:INSAudioVideoCallModeChatting];
    
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatCallEndedByOtherUserNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMode:INSAudioVideoCallModeOffline];
    
    [self populateContents];
    
    [self cleanupAfterCallEnds];

    [self setMode:INSAudioVideoCallModeOffline];
    
    [self populateContents];

    [[QBChat instance] unregisterVideoChatInstance:[self videoChat]];
    [self setVideoChat:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)otherUserDidNotAnswerNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setMode:INSAudioVideoCallModeOffline];
    
    [self populateContents];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    __weak INSAudioVideoCallViewController *weakSelf = self;

    void(^dismissViewController)(void) = ^{
        
        [[PBJVision sharedInstance] stopPreview];
        
        [[AppDelegate sharedDelegate] dismissCallingView];
        
    };
    
    if ([self mode] == INSAudioVideoCallModeOffline)
    {
        dismissViewController();
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Nothing happening, no need to prompt", self, __PRETTY_FUNCTION__);
        return;
    }
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Cancel"];
    
    RIButtonItem *exitButton = [RIButtonItem item];
    [exitButton setLabel:@"Exit"];
    [exitButton setAction:^{
        
        QBVideoChat *videoChat = [weakSelf videoChat];
        
        [videoChat cancelCall];
        [videoChat finishCall];
        
        [[QBChat instance] unregisterVideoChatInstance:videoChat];
        [weakSelf setVideoChat:nil];
        
        dismissViewController();
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"End Call?"
                                                    message:@"Are you sure you want to exit?"
                                           cancelButtonItem:cancelButton
                                           otherButtonItems:exitButton, nil];
    
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)callButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    switch ([self mode])
    {
        case INSAudioVideoCallModeOffline:
        {
            [self setMode:INSAudioVideoCallModeDialing];
            
            QBVideoChat *videoChat = [[QBChat instance] createAndRegisterVideoChatInstance];
            [self setVideoChat:videoChat];
            
            [self configureVideoChat];
            
            INSUser *otherUser = [self otherUser];
            
            NSLog(@"Other user: %@", otherUser);
            NSLog(@"QuickBlox ID: %ld", (long)[[otherUser quickBloxID] integerValue]);
            NSLog(@"conference type: %d", [self callType]);
            
            [videoChat callUser:[[otherUser quickBloxID] integerValue] conferenceType:[self callType]];
        }
            break;
        case INSAudioVideoCallModeDialing:
        case INSAudioVideoCallModeConnecting:
        {
            [self setMode:INSAudioVideoCallModeOffline];
            
            [[self videoChat] cancelCall];
            
            [[QBChat instance] unregisterVideoChatInstance:[self videoChat]];
        }
            break;
        case INSAudioVideoCallModeChatting:
        {
            [self setMode:INSAudioVideoCallModeOffline];
            
            [[self videoChat] finishCall];
            
            [[QBChat instance] unregisterVideoChatInstance:[self videoChat]];
            
            [self cleanupAfterCallEnds];
            
            if ([self isIncomingCall])
            {
                [self cancelButtonPressed:nil];
                
                return;
            }
        }
            break;
        default:
            break;
    }
    
    [self populateContents];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)muteButtonPressed:(UIBarButtonItem *)button
{
    [self showUnmuteButton];
}

- (void)unmuteButtonPressed:(UIBarButtonItem *)button
{
    [self showMuteButton];
}

#pragma mark - Misc Methods

- (void)denyIncomingCall
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self incomingCallAlertView] dismissWithClickedButtonIndex:[[self incomingCallAlertView] cancelButtonIndex] animated:YES];
    
    [self setIncomingCallAlertView:nil];
    
    // http://quickblox.com/developers/SimpleSample-videochat-ios#Rejecting_the_call
    
    QBVideoChat *videoChat = [[QBChat instance] createAndRegisterVideoChatInstanceWithSessionID:[self incomingSessionID]];
    
    [videoChat rejectCallWithOpponentID:[self incomingCallOpponentID]];
    
    [[QBChat instance] unregisterVideoChatInstance:videoChat];
    
    [self setIncomingCallOpponentID:NSNotFound];
    
    [self setMode:INSAudioVideoCallModeOffline];
    [self populateContents];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)handleIncomingCallFromQuickBloxUserID:(NSInteger)quickBloxID
                                withSessionID:(NSString *)sessionID
                                     callType:(enum QBVideoChatConferenceType)callType
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setIncomingCallOpponentID:quickBloxID];
    [self setIncomingSessionID:sessionID];
    [self setCallType:callType];

    INSUser *otherUser = [[INSUserManager sharedManager] amourWithQuickBloxID:quickBloxID];

    // Play ring tone
    
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"ringing" withExtension:@"wav"];
    
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    [self setAudioPlayer:audioPlayer];
    [audioPlayer setNumberOfLoops:5];
    [audioPlayer setVolume:1.0];
    [audioPlayer play];
    
    [[self callButton] setTitle:@"Incoming..." forState:UIControlStateNormal];
    [[self callButton] setEnabled:NO];
    
    // Alert user
    
    __weak INSAudioVideoCallViewController *weakSelf = self;
    
    RIButtonItem *cancelButton = [RIButtonItem item];
    [cancelButton setLabel:@"Decline"];
    [cancelButton setAction:^{
        
        NSLog(@"Rejecting");
        
        [NSObject cancelPreviousPerformRequestsWithTarget:weakSelf selector:@selector(denyIncomingCall) object:nil];
        
        [weakSelf denyIncomingCall];
        
        [[weakSelf audioPlayer] stop];
        
        [weakSelf cancelButtonPressed:nil];
        
    }];
    
    RIButtonItem *acceptButton = [RIButtonItem item];
    [acceptButton setLabel:@"Answer"];
    [acceptButton setAction:^{
        
        NSLog(@"Answering");
        
        [[weakSelf audioPlayer] stop];

        [NSObject cancelPreviousPerformRequestsWithTarget:weakSelf selector:@selector(denyIncomingCall) object:nil];
        
        [weakSelf setMode:INSAudioVideoCallModeConnecting];
        
        [weakSelf populateContents];
        
        QBVideoChat *videoChat = [[QBChat instance] createAndRegisterVideoChatInstanceWithSessionID:sessionID];
        [weakSelf setVideoChat:videoChat];
        
        [weakSelf setOtherUser:otherUser];
        
        [weakSelf configureVideoChat];
        
        
        [[weakSelf videoChat] acceptCallWithOpponentID:quickBloxID conferenceType:callType];
        
    }];
    
    NSString *title = nil;
    
    switch (callType)
    {
        case QBVideoChatConferenceTypeAudio:
            title = @"Incoming Audio Call";
            break;
        case QBVideoChatConferenceTypeAudioAndVideo:
            title = @"Incoming Video Call";
            break;
        case QBVideoChatConferenceTypeUndefined:
            title = @"Incoming Call";
        default:
            break;
    }
    
    NSString *userName = [otherUser userName];
    if (![userName isNotEmpty])
    {
        userName = @"A friend";
    }
    NSString *message = [NSString stringWithFormat:@"%@ is calling. Would you like to answer?", userName];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                           cancelButtonItem:cancelButton
                                           otherButtonItems:acceptButton, nil];
    [self setIncomingCallAlertView:alert];
    
    [alert show];
    
    [self performSelector:@selector(denyIncomingCall) withObject:nil afterDelay:10.0];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)configureVideoChat
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    QBVideoChat *videoChat = [self videoChat];
    
    if ([self callType] == QBVideoChatConferenceTypeAudioAndVideo)
    {
        [videoChat setUseHeadphone:NO];
        
        [videoChat setUseBackCamera:NO];
        [videoChat setViewToRenderOwnVideoStream:[self currentUserVideoContainerView]];
        [videoChat setViewToRenderOpponentVideoStream:[self opponentVideoContainerView]];
    }
    else
    {
        [videoChat setUseHeadphone:YES];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)cleanupAfterCallEnds
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopDurationTimer];
    
    [[self videoChat] setViewToRenderOpponentVideoStream:nil];
    [[self videoChat] setViewToRenderOwnVideoStream:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)startDurationTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self view] bringSubviewToFront:[self timerLabelContainerView]];
    [[self timerLabelContainerView] setHidden:NO];
    
    [self stopDurationTimer];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                      target:self
                                                    selector:@selector(durationTimerFireMethod:)
                                                    userInfo:nil
                                                     repeats:YES];
    [self setDurationTimer:timer];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopDurationTimer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self durationTimer] invalidate];
    [self setDurationTimer:nil];

    [[self timerLabelContainerView] setHidden:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)durationTimerFireMethod:(NSTimer *)timer
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self view] bringSubviewToFront:[self timerLabelContainerView]];
    [[self timerLabelContainerView] setHidden:NO];
    [[self timerLabel] setHidden:NO];

    NSTimeInterval timeInterval = -[[self callStartDate] timeIntervalSinceNow];
    
    NSInteger hours = timeInterval / 3600;
    NSInteger minutes = ((NSInteger)(timeInterval / 60)) % 60;
    NSInteger seconds = ((NSInteger)timeInterval) % 60;
    
    NSMutableArray *components = [NSMutableArray array];
    
    if (hours > 0)
    {
        [components addObject:[NSString stringWithFormat:@"%ld", (long)hours]];
    }
    
    [components addObject:[NSString stringWithFormat:@"%ld", (long)minutes]];
    
    NSString *secondsString = [NSString stringWithFormat:@"%ld", (long)seconds];
    if ([secondsString length] < 2)
    {
        secondsString = [@"0" stringByAppendingString:secondsString];
    }
    
    [components addObject:secondsString];
    
    NSString *duration = [components componentsJoinedByString:@":"];
    
    [[self timerLabel] setText:duration];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showMuteButton
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self videoChat] setMicrophoneEnabled:YES];
    
    UIBarButtonItem *muteButton = [[UIBarButtonItem alloc] initWithTitle:@"Mute"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(muteButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:muteButton];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showUnmuteButton
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self videoChat] setMicrophoneEnabled:NO];
    
    UIBarButtonItem *unmuteButton = [[UIBarButtonItem alloc] initWithTitle:@"Unmute"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(unmuteButtonPressed:)];
    [[self navigationItem] setRightBarButtonItem:unmuteButton];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
