//
//  INSChatTableCell.h
//  Instamour
//
//  Created by Brian Slick on 8/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// NOTE: This class is not intended to be used standalone. See subclasses.

// Forward Declarations and Classes
#import "BTITableViewCell.h"

// Public Constants

// Protocols

@interface INSChatTableCell : BTITableViewCell

// Public Properties
@property (nonatomic, strong) IBOutlet UILabel *timeStampLabel;

// Public Methods

@end
