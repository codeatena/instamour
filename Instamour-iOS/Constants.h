#pragma mark - Server URLs

FOUNDATION_EXPORT NSString *const INSServerRequestPath;
FOUNDATION_EXPORT NSString *const INSServerFilePathEmergencyUseOnly;

#pragma mark - Misc Values

FOUNDATION_EXPORT NSInteger const INSUserNameMaxLength;

FOUNDATION_EXPORT NSInteger const INSMaxNumberOfFreeVideos;
FOUNDATION_EXPORT NSInteger const INSMaxNumberOfVideos;
FOUNDATION_EXPORT NSInteger const INSAgeRangeMinimum;
FOUNDATION_EXPORT NSInteger const INSAgeRangeMaximum;
FOUNDATION_EXPORT NSInteger const INSNumberOfDownloadedProfilesPerPage;
FOUNDATION_EXPORT NSInteger const INSMaxVideoLengthInSeconds;

FOUNDATION_EXPORT NSString *const INSMaleString;
FOUNDATION_EXPORT NSString *const INSFemaleString;

FOUNDATION_EXPORT NSString *const INSChildrenStatusNo;
FOUNDATION_EXPORT NSString *const INSChildrenStatusYes;
FOUNDATION_EXPORT NSString *const INSChildrenStatusNotWithMe;

FOUNDATION_EXPORT NSInteger const INSMergeVideoServerSlotNumber;

//FOUNDATION_EXPORT NSString *const INSQuickBloxPassword;

#pragma mark - Emojis

FOUNDATION_EXPORT NSString *const INSKissingLipsEmoji;
FOUNDATION_EXPORT NSString *const INSFriendsEmoji;
FOUNDATION_EXPORT NSString *const INSHeartEmoji;
FOUNDATION_EXPORT NSString *const INSVideoCameraEmoji;
FOUNDATION_EXPORT NSString *const INSWarningSignEmoji;
FOUNDATION_EXPORT NSString *const INSPageFacingUpEmoji;
