//
//  INSLoginViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSLoginViewController.h"

// Models and other global

// Sub-controllers

// Views

// Private Constants

@interface INSLoginViewController () <UITextFieldDelegate, FBLoginViewDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UITextField *emailTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;

@property (nonatomic, strong) UIBarButtonItem *cancelButton;
@property (nonatomic, strong) UIBarButtonItem *goButton;

@property (nonatomic, copy) NSString *facebookID;

@end

@implementation INSLoginViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Login"];

    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [self setCancelButton:cancelButton];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    UIBarButtonItem *goButton = [[UIBarButtonItem alloc] initWithTitle:@"Go"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(goButtonPressed:)];
    [self setGoButton:goButton];
    [[self navigationItem] setRightBarButtonItem:goButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    [Flurry logEvent:@"Login View"];
    
    // User Interface
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerLoginDidFinishNotification
                                   selector:@selector(loginDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerLoginFailedNotification
                                   selector:@selector(loginFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerResetPasswordDidFinishNotification
                                   selector:@selector(resetPasswordDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerResetPasswordFailedNotification
                                   selector:@selector(resetPasswordFailedNotification:)
                                     object:nil];
    
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerGetUserDidFinishNotification
                                   selector:@selector(getUserDidSucceedNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:INSServerAPIManagerGetUserFailedNotification
                                   selector:@selector(getUserFailedNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)loginDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [self setUserInterfaceEnabled:YES];
        
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Login Failed"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Login Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSUserManager *userManager = [INSUserManager sharedManager];
    [userManager loginWithSessionID:[jsonDictionary sessionIdentifierINS]];
    
    [[INSServerAPIManager sharedManager] getUserWithUserName:nil     // Nil values will use sessionID (current user)
                                                    orUserID:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loginFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Login Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    [[AppDelegate sharedDelegate] logoutFacebook];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)resetPasswordDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserInterfaceEnabled:YES];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Reset Password Failed"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Reset Password Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Sent!"
                                                    message:@"An email has been sent to recover your password to the above email address."
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)resetPasswordFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Reset Password Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)getUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [self setUserInterfaceEnabled:YES];
        
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"User Details Error"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Get User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    // Save values to user defaults
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    INSUserManager *userManager = [INSUserManager sharedManager];
    
    [userDefaults initializeValuesForLoginINS];
    
    [userDefaults loadWithGetUserJSONResultINS:jsonDictionary];
    [userManager loadUserWithGetUserJSONResult:jsonDictionary];
    
    [userManager refreshPendingAmoursRightNow:YES];
    [userManager refreshYourAmoursRightNow:YES];
    [userManager refreshHeartsPushedRightNow:YES];
    
    AppDelegate *appDelegate = [AppDelegate sharedDelegate];
    [appDelegate hideProgressIndicator];
    
    [appDelegate showLoggedInHomeScreen];
    
    [[INSQuickBloxManager sharedManager] loginCurrentUser];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)getUserFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Log In Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] logoutFacebook];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)goButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([INSServerAPIManager isNetworkConnectionAvailable])
    {
        [self loginUser];
    }
    else
    {
        [INSServerAPIManager showNoNetworkAlert];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)forgotPasswordButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *emailAddress = [[self emailTextField] text];
    
    if ( ![emailAddress isNotEmpty] || ![emailAddress isValidEmailAddress] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:@"Please enter a valid email address!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid email address", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setUserInterfaceEnabled:NO];
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Sending..."];
    
    [[INSServerAPIManager sharedManager] resetPasswordForEmail:emailAddress];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setUserInterfaceEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self cancelButton] setEnabled:isEnabled];
    [[self goButton] setEnabled:isEnabled];
    
    if (isEnabled)
    {
        [[AppDelegate sharedDelegate] hideProgressIndicator];
    }
    else
    {
        [[self emailTextField] resignFirstResponder];
        [[self passwordTextField] resignFirstResponder];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loginUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *emailTextField = [self emailTextField];
    UITextField *passwordTextField = [self passwordTextField];
    
    NSString *username = [emailTextField text];
    NSString *password = [passwordTextField text];
    
    if (![username isNotEmpty] || ![password isNotEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:@"Please fill all the fields"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Empty field", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![username isValidEmailAddress])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:@"Please enter a valid email address"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid email", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setUserInterfaceEnabled:NO];
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Logging in..."];
    
    [[INSServerAPIManager sharedManager] loginWithEmail:username
                                               password:password];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *emailTextField = [self emailTextField];
    UITextField *passwordTextField = [self passwordTextField];
    
    if (textField == emailTextField)
    {
        [passwordTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField)
    {
        [passwordTextField resignFirstResponder];
        
        [self goButtonPressed:nil];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

#pragma mark - FBLoginViewDelegate Methods

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *facebookID = [user objectID];
    
    if (![facebookID isNotEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Error"
                                                        message:@"Facebook data not available. Please try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        [[AppDelegate sharedDelegate] logoutFacebook];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No Facebook ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setUserInterfaceEnabled:NO];
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Logging in..."];
    
    [self setFacebookID:facebookID];
    
    [[INSServerAPIManager sharedManager] loginWithFacebookID:facebookID];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    [[AppDelegate sharedDelegate] logoutFacebook];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
