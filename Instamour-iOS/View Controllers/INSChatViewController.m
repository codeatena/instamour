//
//  INSChatViewController.m
//  Instamour
//
//  Created by Brian Slick on 8/8/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSChatViewController.h"

// Models and other global
#import <MediaPlayer/MediaPlayer.h>

// Sub-controllers
#import "INSImageShareViewController.h"
#import "INSVideoRecorderViewController.h"

// Views
#import "INSChatTextFromMeTableCell.h"
#import "INSChatTextToMeTableCell.h"
#import "INSChatImageFromMeTableCell.h"
#import "INSChatImageToMeTableCell.h"

// Private Constants

@interface INSChatViewController () <UITextFieldDelegate, INSChatImageTableCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, INSVideoRecorderViewControllerDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet UIView *inputContainerView;
@property (nonatomic, strong) IBOutlet UITextField *inputTextField;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *distanceFromBottomConstraint;

@property (nonatomic, strong) INSChatTextTableCell *textMeasurementCell;
@property (nonatomic, assign) CGFloat imageRowHeight;

@end

@implementation INSChatViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
        
    [self showEditButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
    
    // Navigation Bar
    
    INSUser *otherUser = [[self chatConversation] amourUser];
    [self setTitle:[otherUser userName]];
    
    [[INSChatDataManager sharedManager] setActiveChatOtherUserID:[otherUser userID]];
    
    // Analytics
    
    [Flurry logEvent:@"Instant Chat View"];
    
    // User Interface
    
    [self populateContents];
    
    [[self tableView] reloadData];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [super viewDidAppear:animated];
    
    [self scrollToBottomRow];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillDisappear:animated];
    
    [[self chatConversation] markAllMessagesAsRead];
    
    [[INSChatDataManager sharedManager] setActiveChatOtherUserID:nil];
    [[INSChatDataManager sharedManager] saveChatConversation:[self chatConversation]];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides

- (void)populateContents
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [[self chatConversation] markAllMessagesAsRead];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateNotificationInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super populateNotificationInfos];
    
    [self addVisibleNotificationInfoForName:UIKeyboardWillShowNotification
                                   selector:@selector(keyboardWillShowNotification:)
                                     object:nil];
    
    [self addVisibleNotificationInfoForName:UIKeyboardWillHideNotification
                                   selector:@selector(keyboardWillHideNotification:)
                                     object:nil];

    [self addVisibleNotificationInfoForName:INSChatDataManagerDidChangeConversationNotification
                                   selector:@selector(chatConversationDidChangeNotification:)
                                     object:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTITableViewController Overrides

- (void)registerNibsForTableView:(UITableView *)tableView
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // Text Cells
    
    [INSChatTextFromMeTableCell registerNibForTableViewBTI:tableView];
    [INSChatTextToMeTableCell registerNibForTableViewBTI:tableView];
    
    INSChatTextFromMeTableCell *textCell = [INSChatTextFromMeTableCell dequeueCellFromTableViewBTI:tableView];
    [self setTextMeasurementCell:textCell];
    
    // Image Cells
    
    [INSChatImageFromMeTableCell registerNibForTableViewBTI:tableView];
    [INSChatImageToMeTableCell registerNibForTableViewBTI:tableView];
    
    INSChatImageFromMeTableCell *imageCell = [INSChatImageFromMeTableCell dequeueCellFromTableViewBTI:tableView];
    [self setImageRowHeight:CGRectGetHeight([imageCell frame])];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)keyboardWillShowNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
	CGRect keyboardFrameEnd = [(NSValue *)[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	CGRect adjustedKeyboardFrameEnd = [[self view] convertRect:keyboardFrameEnd fromView:nil];
	CGFloat animationDuration = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat animationCurve = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue];
    
    [[self view] layoutIfNeeded];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         
                         [[self distanceFromBottomConstraint] setConstant:CGRectGetHeight(adjustedKeyboardFrameEnd)];
                         [[self view] layoutIfNeeded];
                         
                     }
                     completion:^(BOOL finished) {
                         
//                         NSInteger numberOfRows = [[self mainContents] count];
//                         if (numberOfRows > 0)
//                         {
//                             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:numberOfRows - 1 inSection:0];
//                             [[self tableView] scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
//                         }
                         
                     }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)keyboardWillHideNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    
	CGFloat animationDuration = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat animationCurve = [(NSNumber *)[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue];
    
    [[self view] layoutIfNeeded];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         
                         [[self distanceFromBottomConstraint] setConstant:0];
                         [[self view] layoutIfNeeded];
                         
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)chatConversationDidChangeNotification:(NSNotification *)notification;
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSChatConversation *currentChatConversation = [self chatConversation];
    INSChatConversation *incomingChatConversation = [[notification userInfo] objectForKey:INSChatDataManagerNotificationUserInfoConversationKey];
    
    if (incomingChatConversation != currentChatConversation)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not the current conversation", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self tableView] reloadData];
    
    [self scrollToBottomRow];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moviePlayerPlaybackDidFinishNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self dismissMoviePlayerViewControllerAnimated];
    
    MPMoviePlayerViewController *moviePlayer = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UI Response Methods

- (IBAction)sendButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    UITextField *textField = [self inputTextField];
    
    [textField resignFirstResponder];
    
    NSString *text = [textField text];
    
    if (![text isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No content", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSChatDataManager sharedManager] sendChatToUser:[[self chatConversation] amourUser]
                                           textMessage:text];
    
    [textField setText:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (IBAction)cameraButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    __weak INSChatViewController *weakSelf = self;
    
	void(^showImagePicker)(UIImagePickerControllerSourceType sourceType) = ^(UIImagePickerControllerSourceType sourceType) {
        
		UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
		[imagePicker setMediaTypes:@[(NSString *)kUTTypeImage]];
		[imagePicker setDelegate:weakSelf];
		[imagePicker setSourceType:sourceType];
        [imagePicker setAllowsEditing:YES];
        
        [weakSelf presentViewController:imagePicker
                               animated:YES
                             completion:nil];
        
	};
    
    NSMutableArray *buttons = [NSMutableArray array];
    
	RIButtonItem *buttonItem = nil;
    
	// Pick from library
    
	buttonItem = [RIButtonItem item];
	[buttonItem setLabel:@"Choose Photo"];
	[buttonItem setAction:^{
        
		showImagePicker(UIImagePickerControllerSourceTypePhotoLibrary);
        
	}];
    
	[buttons addObject:buttonItem];
    
	// Pick from camera
    
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
		buttonItem = [RIButtonItem item];
		[buttonItem setLabel:@"Take Photo"];
		[buttonItem setAction:^{
            
			showImagePicker(UIImagePickerControllerSourceTypeCamera);
            
		}];
        
		[buttons addObject:buttonItem];
        
        buttonItem = [RIButtonItem item];
        [buttonItem setLabel:@"Record Video"];
        [buttonItem setAction:^{
           
            INSVideoRecorderViewController *nextViewController = [[INSVideoRecorderViewController alloc] init];
            [nextViewController setDelegate:weakSelf];
            [nextViewController setConfirmationButtonTitle:@"Send"];
            
            [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
            
        }];
        
        [buttons addObject:buttonItem];
	}
    
    // Cancel
    
	buttonItem = [RIButtonItem item];
	[buttonItem setLabel:@"Cancel"];
    
	[buttons addObject:buttonItem];
    
	// Action Sheet
    
	UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
											   cancelButtonItem:nil
										  destructiveButtonItem:nil
											   otherButtonItems:nil];
    
	NSInteger cancelIndex = -1;
	for (RIButtonItem *aButton in buttons)
	{
		cancelIndex = [sheet addButtonItem:aButton];
	}
    
	[sheet setCancelButtonIndex:cancelIndex];
    
    [sheet showInView:[self view]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)doneButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self tableView] setEditing:NO animated:YES];
    
    [self showEditButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)editButtonPressed:(UIButton *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self tableView] setEditing:YES animated:YES];
    
    [self showDoneButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)scrollToBottomRow
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSInteger numberOfMessages = [[[self chatConversation] chatMessages] count];
    
    if ( (numberOfMessages > 0) && ([[self tableView] numberOfRowsInSection:0] > 0) )
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:numberOfMessages - 1 inSection:0];
        
        [[self tableView] scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)playVideoForChatMessage:(INSChatMessage *)chatMessage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSURL *videoURL = [chatMessage videoURL];
    NSString *videoFileName = [chatMessage videoFileName];
    
    if ( (videoURL == nil) && ![videoFileName isNotEmpty] )
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No URL or file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *localURL = [[INSFileUtilities videoDirectoryURL] URLByAppendingPathComponent:videoFileName];
    NSURL *urlToLoad = localURL;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[localURL path]])
    {
        urlToLoad = videoURL;
    }
    
    if (urlToLoad == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No URL", self, __PRETTY_FUNCTION__);
        return;
    }
    
    MPMoviePlayerViewController *moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:urlToLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerPlaybackDidFinishNotification:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [self presentMoviePlayerViewControllerAnimated:moviePlayer];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showDoneButton
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    // TODO: Restore
//    UIBarButtonItem *doneButton = [UIBarButtonItem plainButtonWithTitleINS:@"Done"
//                                                                    target:self
//                                                                    action:@selector(doneButtonPressed:)];
//    [[self navigationItem] setRightBarButtonItem:doneButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)showEditButton
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    // TODO: Restore
//    UIBarButtonItem *editButton = [UIBarButtonItem plainButtonWithTitleINS:@"Edit"
//                                                                    target:self
//                                                                    action:@selector(editButtonPressed:)];
//    [[self navigationItem] setRightBarButtonItem:editButton];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self chatConversation] chatMessages] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSChatMessage *chatMessage = [[[self chatConversation] chatMessages] objectAtIndex:[indexPath row]];

    INSChatTableCell *cellToReturn = nil;

    switch ([chatMessage messageType])
    {
        case INSChatMessageTypeText:
        {
            INSChatTextTableCell *textCell = nil;
            
            if ([chatMessage isToCurrentUser])
            {
                textCell = [INSChatTextToMeTableCell dequeueCellFromTableViewBTI:tableView];
            }
            else
            {
                textCell = [INSChatTextFromMeTableCell dequeueCellFromTableViewBTI:tableView];
            }
            
            [[textCell messageLabel] setText:[chatMessage messageBody]];
            
            cellToReturn = textCell;
        }
            break;
        case INSChatMessageTypeImage:
        case INSChatMessageTypeVideo:
        {
            INSChatImageTableCell *imageCell = nil;
            INSChatMessageType messageType = [chatMessage messageType];
            
            if ([chatMessage isToCurrentUser])
            {
                imageCell = [INSChatImageToMeTableCell dequeueCellFromTableViewBTI:tableView];
            }
            else
            {
                imageCell = [INSChatImageFromMeTableCell dequeueCellFromTableViewBTI:tableView];
            }
            
            [imageCell setDelegate:self];

            UIImage *thumbnailImage = [chatMessage thumbnailImage];
            if (thumbnailImage == nil)
            {
                if (messageType == INSChatMessageTypeImage)
                {
                    thumbnailImage = [UIImage placeholderAvatarImageINS];
                }
                else if (messageType == INSChatMessageTypeVideo)
                {
                    thumbnailImage = [UIImage imageNamed:@"video-thumbnail"];
                }
            }
            
            [[imageCell previewImageView] setImage:thumbnailImage];
            [imageCell setPlayImageHidden:(messageType == INSChatMessageTypeImage)];
            [imageCell setBusy:[chatMessage isBusy]];
            
            cellToReturn = imageCell;
            
        }
            break;
        default:
        {
            cellToReturn = [[INSChatTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"blankCellIdentifier"];
        }
            break;
    }
    
    [[cellToReturn timeStampLabel] setText:[NSDateFormatter localizedStringFromDate:[chatMessage dateSent] dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterShortStyle]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return cellToReturn;
}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        INSChatMessage *chatMessage = [[[self chatConversation] chatMessages] objectAtIndex:[indexPath row]];
        
        // TODO: Anything need to be reported to a server?
        
        [[[self chatConversation] chatMessages] removeObject:chatMessage];
        
        [tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    CGFloat heightToReturn = 0.0;
    
    INSChatMessage *chatMessage = [[[self chatConversation] chatMessages] objectAtIndex:[indexPath row]];
    
    switch ([chatMessage messageType])
    {
        case INSChatMessageTypeText:
        {
            INSChatTextTableCell *textCell = [self textMeasurementCell];
            
            [[textCell messageLabel] setText:[chatMessage messageBody]];
            
            [textCell setNeedsUpdateConstraints];
            [textCell updateConstraintsIfNeeded];
            
            [textCell setNeedsLayout];
            [textCell layoutIfNeeded];
            
            heightToReturn = [textCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        }
            break;
        case INSChatMessageTypeImage:
        case INSChatMessageTypeVideo:
            heightToReturn = [self imageRowHeight];
            break;
        default:
            heightToReturn = 44.0;
            break;
    }
    
    //BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return heightToReturn;
}

- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - INSChatImageTableCellDelegate Methods

- (void)chatImageTableCellShareButtonPressed:(INSChatImageTableCell *)cell
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:cell];
    INSChatMessage *chatMessage = [[[self chatConversation] chatMessages] objectAtIndex:[indexPath row]];

    switch ([chatMessage messageType])
    {
        case INSChatMessageTypeImage:
        {
            INSImageShareViewController *nextViewController = [[INSImageShareViewController alloc] init];
            [nextViewController setChatMessage:chatMessage];
            
            [[self navigationController] pushViewController:nextViewController animated:YES];
        }
            break;
        case INSChatMessageTypeVideo:
        {
            [self playVideoForChatMessage:chatMessage];
        }
            break;
        default:
            break;
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerEditedImage];
	if (selectedImage == nil)
	{
		selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
	}

    UIImage *scaledImage = [selectedImage imageByScalingAspectFitToSizeINS:CGSizeMake(600.0, 800.0)];
    
    [[INSChatDataManager sharedManager] sendChatToUser:[[self chatConversation] amourUser]
                                                 image:scaledImage];
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
	[self dismissViewControllerAnimated:YES
							 completion:nil];
    
	BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - INSVideoRecorderViewControllerDelegate Methods

- (void)videoRecorder:(INSVideoRecorderViewController *)videoRecorder
  didRecordVideoToURL:(NSURL *)fileURL
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    if (fileURL == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Recording Error"
                                                        message:@"No video file"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No video URL", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[INSChatDataManager sharedManager] sendChatToUser:[[self chatConversation] amourUser]
                                          videoFileURL:fileURL];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
