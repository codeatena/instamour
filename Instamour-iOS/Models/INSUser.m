//
//  INSUser.m
//  Instamour
//
//  Created by Brian Slick on 7/28/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSUser.h"

// Private Constants
NSString *const INSUserSessionIDKey = @"INSUserSessionIDKey";
NSString *const INSUserUserNameKey = @"INSUserUserNameKey";
NSString *const INSUserUserIDKey = @"INSUserUserIDKey";
NSString *const INSUserEmailKey = @"INSUserEmailKey";
NSString *const INSUserFacebookIDKey = @"INSUserFacebookIDKey";
NSString *const INSUserQuickBloxIDKey = @"INSUserQuickBloxIDKey";
NSString *const INSUserQuickBloxPasswordKey = @"INSUserQuickBloxPasswordKey";
NSString *const INSUserInstagramNameKey = @"INSUserInstagramNameKey";
NSString *const INSUserVineNameKey = @"INSUserVineNameKey";

NSString *const INSUserDateAddedKey = @"INSUserDateAddedKey";

NSString *const INSUserGenderKey = @"INSUserGenderKey";
NSString *const INSUserDateOfBirthKey = @"INSUserDateOfBirthKey";
NSString *const INSUserEthnicityKey = @"INSUserEthnicityKey";
NSString *const INSUserChildrenKey = @"INSUserChildrenKey";
NSString *const INSUserIdentityKey = @"INSUserIdentityKey";
NSString *const INSUserHeightKey = @"INSUserHeightKey";
NSString *const INSUserBodyTypeKey = @"INSUserBodyTypeKey";
NSString *const INSUserSexualPreferenceKey = @"INSUserSexualPreferenceKey";
NSString *const INSUserLookingForKey = @"INSUserLookingForKey";
NSString *const INSUserSmokerKey = @"INSUserSmokerKey";

NSString *const INSUserAvatarFileNameKey = @"INSUserAvatarFileNameKey";
NSString *const INSUserMergeVideoFileNameKey = @"INSUserMergeVideoFileNameKey";

NSString *const INSUserCityKey = @"INSUserCityKey";
NSString *const INSUserStateKey = @"INSUserStateKey";
NSString *const INSUserCountryKey = @"INSUserCountryKey";
NSString *const INSUserLatitudeKey = @"INSUserLatitudeKey";
NSString *const INSUserLongitudeKey = @"INSUserLongitudeKey";

NSString *const INSUserPushNotificationsEnabledKey = @"INSUserPushNotificationsEnabledKey";
NSString *const INSUserMyHeartPushedNotificationEnabledKey = @"INSUserMyHeartPushedNotificationEnabledKey";
NSString *const INSUserMyVideoWatchedNotificationEnabledKey = @"INSUserMyVideoWatchedNotificationEnabledKey";
NSString *const INSUserSomeoneKissedMeNotificationEnabledKey = @"INSUserSomeoneKissedMeNotificationEnabledKey";
NSString *const INSUserNewCommentNotificationEnabledKey = @"INSUserNewCommentNotificationEnabledKey";
NSString *const INSUserNewAmourNotificationEnabledKey = @"INSUserNewAmourNotificationEnabledKey";
NSString *const INSUserTextChatNotificationEnabledKey = @"INSUserTextChatNotificationEnabledKey";
NSString *const INSUserAudioChatNotificationEnabledKey = @"INSUserAudioChatNotificationEnabledKey";
NSString *const INSUserVideoChatNotificationEnabledKey = @"INSUserVideoChatNotificationEnabledKey";
NSString *const INSUserGiftReceivedNotificationEnabledKey = @"INSUserGiftReceivedNotificationEnabledKey";

NSString *const INSUserAudioChatEnabledKey = @"INSUserAudioChatEnabledKey";
NSString *const INSUserVideoChatEnabledKey = @"INSUserVideoChatEnabledKey";

NSString *const INSUserUnlimitedCommentsEnabledKey = @"INSUserUnlimitedCommentsEnabledKey";
NSString *const INSUserUnlimitedVideosEnabledKey = @"INSUserUnlimitedVideosEnabledKey";

NSString *const INSUserKissesKey = @"INSUserKissesKey";
NSString *const INSUserCommentsKey = @"INSUserCommentsKey";

@interface INSUser ()

@property (nonatomic, assign, getter = isDownloadingAvatar) BOOL downloadingAvatar;

@end


@implementation INSUser

#pragma mark - Dealloc and Memory Management

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Getters and Setters

- (UIImage *)avatarImage
{
    if (_avatarImage == nil)
    {
        [self loadLocalImage];
        
        if (_avatarImage == nil)
        {
            [self downloadServerImage];
        }
    }
    return _avatarImage;
}

- (NSMutableArray *)kisses
{
    if (_kisses == nil)
    {
        _kisses = [[NSMutableArray alloc] init];
    }
    return _kisses;
}

- (NSMutableArray *)comments
{
    if (_comments == nil)
    {
        _comments = [[NSMutableArray alloc] init];
    }
    return _comments;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveMemoryWarningNotification:)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
    }
    return self;
}

- (instancetype)initWithJSONUser:(NSDictionary *)jsonUser
{
    self = [self init];
    if (self)
    {
        [self loadWithJSONUser:jsonUser];
        
        // Handle any default values that might not be in the JSON
    }
    return self;
}

#pragma mark - NSCoding Methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self sessionID] forKey:INSUserSessionIDKey];
    
    [aCoder encodeObject:[self userName] forKey:INSUserUserNameKey];
    [aCoder encodeObject:[self userID] forKey:INSUserUserIDKey];
    [aCoder encodeObject:[self email] forKey:INSUserEmailKey];
    [aCoder encodeObject:[self facebookID] forKey:INSUserFacebookIDKey];
    [aCoder encodeObject:[self quickBloxID] forKey:INSUserQuickBloxIDKey];
    [aCoder encodeObject:[self quickBloxPassword] forKey:INSUserQuickBloxPasswordKey];
    [aCoder encodeObject:[self instagramName] forKey:INSUserInstagramNameKey];
    [aCoder encodeObject:[self vineName] forKey:INSUserVineNameKey];
    
    [aCoder encodeObject:[self dateAdded] forKey:INSUserDateAddedKey];
    
    [aCoder encodeObject:[self gender] forKey:INSUserGenderKey];
    [aCoder encodeObject:[self dateOfBirth] forKey:INSUserDateOfBirthKey];
    [aCoder encodeObject:[self ethnicity] forKey:INSUserEthnicityKey];
    [aCoder encodeObject:[self children] forKey:INSUserChildrenKey];
    [aCoder encodeObject:[self identity] forKey:INSUserIdentityKey];
    [aCoder encodeObject:[self height] forKey:INSUserHeightKey];
    [aCoder encodeObject:[self bodyType] forKey:INSUserBodyTypeKey];
    [aCoder encodeObject:[self sexualPreference] forKey:INSUserSexualPreferenceKey];
    [aCoder encodeObject:[self lookingFor] forKey:INSUserLookingForKey];
    [aCoder encodeObject:[self smoker] forKey:INSUserSmokerKey];
    
    [aCoder encodeObject:[self avatarFileName] forKey:INSUserAvatarFileNameKey];
    [aCoder encodeObject:[self mergeVideoFileName] forKey:INSUserMergeVideoFileNameKey];
    
    [aCoder encodeObject:[self city] forKey:INSUserCityKey];
    [aCoder encodeObject:[self state] forKey:INSUserStateKey];
    [aCoder encodeObject:[self country] forKey:INSUserCountryKey];
    [aCoder encodeObject:[self latitude] forKey:INSUserLatitudeKey];
    [aCoder encodeObject:[self longitude] forKey:INSUserLongitudeKey];
    
    [aCoder encodeBool:[self isPushNotificationsEnabled] forKey:INSUserPushNotificationsEnabledKey];
    [aCoder encodeBool:[self isMyHeartPushedNotificationEnabled] forKey:INSUserMyHeartPushedNotificationEnabledKey];
    [aCoder encodeBool:[self isMyVideoWatchedNotificationEnabled] forKey:INSUserMyVideoWatchedNotificationEnabledKey];
    [aCoder encodeBool:[self isSomeoneKissedMeNotificationEnabled] forKey:INSUserSomeoneKissedMeNotificationEnabledKey];
    [aCoder encodeBool:[self isNewCommentNotificationEnabled] forKey:INSUserNewCommentNotificationEnabledKey];
    [aCoder encodeBool:[self isNewAmourNotificationEnabled] forKey:INSUserNewAmourNotificationEnabledKey];
    [aCoder encodeBool:[self isTextChatNotificationEnabled] forKey:INSUserTextChatNotificationEnabledKey];
    [aCoder encodeBool:[self isAudioChatNotificationEnabled] forKey:INSUserAudioChatNotificationEnabledKey];
    [aCoder encodeBool:[self isVideoChatNotificationEnabled] forKey:INSUserVideoChatNotificationEnabledKey];
    [aCoder encodeBool:[self isGiftReceivedNotificationEnabled] forKey:INSUserGiftReceivedNotificationEnabledKey];
    
    [aCoder encodeBool:[self isAudioChatEnabled] forKey:INSUserAudioChatEnabledKey];
    [aCoder encodeBool:[self isVideoChatEnabled] forKey:INSUserVideoChatEnabledKey];
    
    [aCoder encodeBool:[self isUnlimitedCommentsEnabled] forKey:INSUserUnlimitedCommentsEnabledKey];
    [aCoder encodeBool:[self isUnlimitedVideosEnabled] forKey:INSUserUnlimitedVideosEnabledKey];
    
    [aCoder encodeObject:[self kisses] forKey:INSUserKissesKey];
    [aCoder encodeObject:[self comments] forKey:INSUserCommentsKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self)
    {
        [self setSessionID:[aDecoder decodeObjectForKey:INSUserSessionIDKey]];
        
        [self setUserName:[aDecoder decodeObjectForKey:INSUserUserNameKey]];
        [self setUserID:[aDecoder decodeObjectForKey:INSUserUserIDKey]];
        [self setEmail:[aDecoder decodeObjectForKey:INSUserEmailKey]];
        [self setFacebookID:[aDecoder decodeObjectForKey:INSUserFacebookIDKey]];
        [self setQuickBloxID:[aDecoder decodeObjectForKey:INSUserQuickBloxIDKey]];
        [self setQuickBloxPassword:[aDecoder decodeObjectForKey:INSUserQuickBloxPasswordKey]];
        [self setInstagramName:[aDecoder decodeObjectForKey:INSUserInstagramNameKey]];
        [self setVineName:[aDecoder decodeObjectForKey:INSUserVineNameKey]];
        
        [self setDateAdded:[aDecoder decodeObjectForKey:INSUserDateAddedKey]];
        
        [self setGender:[aDecoder decodeObjectForKey:INSUserGenderKey]];
        [self setDateOfBirth:[aDecoder decodeObjectForKey:INSUserDateOfBirthKey]];
        [self setEthnicity:[aDecoder decodeObjectForKey:INSUserEthnicityKey]];
        [self setChildren:[aDecoder decodeObjectForKey:INSUserChildrenKey]];
        [self setIdentity:[aDecoder decodeObjectForKey:INSUserIdentityKey]];
        [self setHeight:[aDecoder decodeObjectForKey:INSUserHeightKey]];
        [self setBodyType:[aDecoder decodeObjectForKey:INSUserBodyTypeKey]];
        [self setSexualPreference:[aDecoder decodeObjectForKey:INSUserSexualPreferenceKey]];
        [self setLookingFor:[aDecoder decodeObjectForKey:INSUserLookingForKey]];
        [self setSmoker:[aDecoder decodeObjectForKey:INSUserSmokerKey]];
        
        [self setAvatarFileName:[aDecoder decodeObjectForKey:INSUserAvatarFileNameKey]];
        [self setMergeVideoFileName:[aDecoder decodeObjectForKey:INSUserMergeVideoFileNameKey]];
        
        [self setCity:[aDecoder decodeObjectForKey:INSUserCityKey]];
        [self setState:[aDecoder decodeObjectForKey:INSUserStateKey]];
        [self setCountry:[aDecoder decodeObjectForKey:INSUserCountryKey]];
        [self setLatitude:[aDecoder decodeObjectForKey:INSUserLatitudeKey]];
        [self setLongitude:[aDecoder decodeObjectForKey:INSUserLongitudeKey]];
        
        [self setPushNotificationsEnabled:[aDecoder decodeBoolForKey:INSUserPushNotificationsEnabledKey]];
        [self setMyHeartPushedNotificationEnabled:[aDecoder decodeBoolForKey:INSUserMyHeartPushedNotificationEnabledKey]];
        [self setMyVideoWatchedNotificationEnabled:[aDecoder decodeBoolForKey:INSUserMyVideoWatchedNotificationEnabledKey]];
        [self setSomeoneKissedMeNotificationEnabled:[aDecoder decodeBoolForKey:INSUserSomeoneKissedMeNotificationEnabledKey]];
        [self setNewCommentNotificationEnabled:[aDecoder decodeBoolForKey:INSUserNewCommentNotificationEnabledKey]];
        [self setNewAmourNotificationEnabled:[aDecoder decodeBoolForKey:INSUserNewAmourNotificationEnabledKey]];
        [self setTextChatNotificationEnabled:[aDecoder decodeBoolForKey:INSUserTextChatNotificationEnabledKey]];
        [self setAudioChatNotificationEnabled:[aDecoder decodeBoolForKey:INSUserAudioChatNotificationEnabledKey]];
        [self setVideoChatNotificationEnabled:[aDecoder decodeBoolForKey:INSUserVideoChatNotificationEnabledKey]];
        [self setGiftReceivedNotificationEnabled:[aDecoder decodeBoolForKey:INSUserGiftReceivedNotificationEnabledKey]];
        
        [self setAudioChatEnabled:[aDecoder decodeBoolForKey:INSUserAudioChatEnabledKey]];
        [self setVideoChatEnabled:[aDecoder decodeBoolForKey:INSUserVideoChatEnabledKey]];
        
        [self setUnlimitedCommentsEnabled:[aDecoder decodeBoolForKey:INSUserUnlimitedCommentsEnabledKey]];
        [self setUnlimitedVideosEnabled:[aDecoder decodeBoolForKey:INSUserUnlimitedVideosEnabledKey]];
        
        [self setKisses:[aDecoder decodeObjectForKey:INSUserKissesKey]];
        [self setComments:[aDecoder decodeObjectForKey:INSUserCommentsKey]];
    }
    return self;
}

#pragma mark - Misc Methods

- (void)loadWithJSONUser:(NSDictionary *)jsonUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setUserName:[jsonUser userNameForUserINS]];
    [self setUserID:[jsonUser identifierForUserINS]];
    [self setEmail:[jsonUser emailForUserINS]];
    [self setFacebookID:[jsonUser facebookIDForUserINS]];
    [self setQuickBloxID:[jsonUser chatIDForUserINS]];
    [self setQuickBloxPassword:[jsonUser quickBloxPasswordForUserINS]];
    [self setInstagramName:[jsonUser instagramForUserINS]];
    [self setVineName:[jsonUser vineForUserINS]];
    
    [self setDateAdded:[jsonUser dateAddedForUserINS]];
    
    [self setOnline:[jsonUser isUserOnlineINS]];
    [self setAccountDisabled:[jsonUser isUserAccountDisabledINS]];

    [self setGender:[jsonUser genderForUserINS]];
    [self setDateOfBirth:[jsonUser dateOfBirthForUserINS]];
    [self setEthnicity:[jsonUser ethnicityForUserINS]];
    [self setChildren:[jsonUser childrenForUserINS]];
    [self setIdentity:[jsonUser identityForUserINS]];
    [self setHeight:[jsonUser heightForUserINS]];
    [self setBodyType:[jsonUser bodyTypeForUserINS]];
    [self setSexualPreference:[jsonUser sexualPreferenceForUserINS]];
    [self setLookingFor:[jsonUser lookingForForUserINS]];
    [self setSmoker:[jsonUser smokerForUserINS]];
    
    NSString *newImageName = [jsonUser profileImageNameForUserINS];
    NSString *oldImageName = [self avatarFileName];
    if (![oldImageName isEqualToString:newImageName])
    {
        // TODO: Delete old file
        [self setAvatarImage:nil];
        [self setAvatarFileName:newImageName];
    }
    [self setMergeVideoFileName:[jsonUser fileNameForUserVideoMergeINS]];
    
    [self setCity:[jsonUser cityForUserINS]];
    [self setState:[jsonUser stateForUserINS]];
    [self setCountry:[jsonUser countryForUserINS]];
    [self setLatitude:[jsonUser latitudeForUserINS]];
    [self setLongitude:[jsonUser longitudeForUserINS]];
    
    [self setPushNotificationsEnabled:[jsonUser isPushNotificationsEnabledForUserINS]];
    [self setMyHeartPushedNotificationEnabled:[jsonUser isHeartPushNotificationEnabledForUserINS]];
    [self setMyVideoWatchedNotificationEnabled:[jsonUser isVideoPushNotificationEnabledForUserINS]];
    [self setSomeoneKissedMeNotificationEnabled:[jsonUser isKissedPushNotificationEnabledForUserINS]];
    [self setNewCommentNotificationEnabled:[jsonUser isNewCommentPushNotificationEnabledForUserINS]];
    [self setNewAmourNotificationEnabled:[jsonUser isNewAmourPushNotificationEnabledForUserINS]];
    [self setTextChatNotificationEnabled:[jsonUser isChatPushNotificationEnabledForUserINS]];
    [self setAudioChatNotificationEnabled:[jsonUser isAudioPushNotificationEnabledForUserINS]];
    [self setVideoChatNotificationEnabled:[jsonUser isVideoPushNotificationEnabledForUserINS]];
    [self setGiftReceivedNotificationEnabled:[jsonUser isReceivedGiftPushNotificationEnabledForUserINS]];
    
    [self setAudioChatEnabled:[jsonUser isAudioChatEnabledForUserINS]];
    [self setVideoChatEnabled:[jsonUser isVideoChatEnabledForUserINS]];
    
    [self setUnlimitedCommentsEnabled:[jsonUser isUnlimitedCommentsEnabledForUserINS]];
    [self setUnlimitedVideosEnabled:[jsonUser isUnlimitedVideosEnabledForUserINS]];
    
    [[self kisses] removeAllObjects];
    NSArray *jsonKisses = [jsonUser kissesForUserINS];
    for (NSDictionary *jsonKiss in jsonKisses)
    {
        INSKiss *kiss = [[INSKiss alloc] initWithJSONDictionary:jsonKiss];
        
        [[self kisses] addObject:kiss];
    }
    
    [[self comments] removeAllObjects];
    NSArray *jsonComments = [jsonUser commentsForUserINS];
    for (NSDictionary *jsonComment in jsonComments)
    {
        INSComment *comment = [[INSComment alloc] initWithJSONDictionary:jsonComment];
        
        [[self comments] addObject:comment];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (NSString *)formattedCityStateCountry
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSMutableArray *components = [NSMutableArray array];
    
    [components safelyAddPopulatedStringINS:[self city]];
    [components safelyAddPopulatedStringINS:[self state]];
    [components safelyAddPopulatedStringINS:[self country]];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return [components componentsJoinedByString:@", "];
}

- (void)loadLocalImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[self avatarFileName] isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:[self avatarFileName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No local file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    UIImage *image = [[UIImage alloc] initWithData:data];
    NSLog(@"local image: %@", image);
    
    [self setAvatarImage:image];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadServerImage
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([self isDownloadingAvatar])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already downloading", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *fileName = [self avatarFileName];
    
    if (![fileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setDownloadingAvatar:YES];
    
    NSString *urlString = [[[NSUserDefaults standardUserDefaults] serverAvatarImagesFilePathINS] stringByAppendingPathComponent:fileName];
    NSURL *url = [NSURL URLWithString:urlString];

    __weak INSUser *weakSelf = self;
    
    [[INSServerAPIManager sharedManager] downloadImageFromURL:url
                                                         success:^(UIImage *image) {
                                                             
                                                             NSLog(@"Downloaded image");

                                                             [weakSelf setAvatarImage:image];
                                                             
                                                             if ([weakSelf avatarDownloadCompletionBlock] != nil)
                                                             {
                                                                 [weakSelf avatarDownloadCompletionBlock](image);
                                                                 
                                                                 [weakSelf setAvatarDownloadCompletionBlock:nil];
                                                             }
                                                             
                                                             NSURL *fileURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:fileName];
                                                             
                                                             NSData *data = UIImagePNGRepresentation(image);
                                                             
                                                             [data writeToURL:fileURL atomically:YES];
                                                                                                                          
                                                             [weakSelf setDownloadingAvatar:NO];
                                                             
                                                         }
                                                         failure:^(NSString *errorMessage) {
                                                             
                                                             NSLog(@"Failed to download image");
                                                             NSLog(@"error: %@", errorMessage);
                                                             NSLog(@"URL: %@", url);
                                                             [weakSelf setDownloadingAvatar:NO];

                                                         }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Notification Handlers

- (void)didReceiveMemoryWarningNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self setAvatarImage:nil];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
