//
//  INSChatImageTableCell.h
//  Instamour
//
//  Created by Brian Slick on 8/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// NOTE: This class is not intended to be used standalone. See subclasses.

// Forward Declarations and Classes
#import "INSChatTableCell.h"

// Public Constants

// Protocols
@protocol INSChatImageTableCellDelegate;

@interface INSChatImageTableCell : INSChatTableCell

// Public Properties
@property (nonatomic, strong) IBOutlet UIImageView *previewImageView;

@property (nonatomic, weak) id<INSChatImageTableCellDelegate> delegate;

// Public Methods
- (void)setPlayImageHidden:(BOOL)isHidden;
- (void)setBusy:(BOOL)isBusy;

@end

@protocol INSChatImageTableCellDelegate <NSObject>

@required
- (void)chatImageTableCellShareButtonPressed:(INSChatImageTableCell *)cell;

@end