//
//  INSLocationManager.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSLocationManager.h"

// Private Constants
NSString *const INSLocationManagerDidChangeLocationNotification = @"INSLocationManagerDidChangeLocationNotification";
NSString *const INSLocationManagerFailedNotification = @"INSLocationManagerFailedNotification";


@interface INSLocationManager () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLGeocoder *geocoder;

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;

@end

@implementation INSLocationManager

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Management

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_locationManager setDelegate:nil];
}

#pragma mark - Initialization


#pragma mark - Custom Getters and Setters

- (CLLocationManager *)locationManager
{
    if (_locationManager == nil)
    {
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager setDelegate:self];
        [_locationManager setDistanceFilter:kCLDistanceFilterNone];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    }
    return _locationManager;
}

- (CLGeocoder *)geocoder
{
    if (_geocoder == nil)
    {
        _geocoder = [[CLGeocoder alloc] init];
    }
    return _geocoder;
}

#pragma mark - Notification Handlers


#pragma mark - Misc Methods

- (void)startTrackingLocation
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setCity:nil];
    [self setState:nil];
    [self setCountry:nil];
    [self setLatitude:nil];
    [self setLongitude:nil];
    
    [[self locationManager] startUpdatingLocation];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)stopTrackingLocation
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self locationManager] stopUpdatingLocation];
    [[self locationManager] setDelegate:nil];
    
    [self setLocationManager:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)reverseGeocodeLocation:(CLLocation *)location
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (location == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No location", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self geocoder] reverseGeocodeLocation:location
                          completionHandler:^(NSArray *placemarks, NSError *error) {
                              
                              CLPlacemark *placemark = [placemarks firstObject];
                              
                              if (placemark == nil)
                              {
                                  [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSLocationManagerFailedNotification];

                                  return;
                              }
                              
                              CLLocationCoordinate2D coordinate = [location coordinate];
                              
                              NSString *latitudeString = [NSString stringWithFormat:@"%.8f", coordinate.latitude];
                              NSString *longitudeString = [NSString stringWithFormat:@"%.8f", coordinate.longitude];
                              
                              [self setLatitude:latitudeString];
                              [self setLongitude:longitudeString];
                              
                              NSString *city = [placemark locality];
                              NSString *state = [placemark administrativeArea];
                              NSString *country = [placemark country];
                              
                              [self setCity:city];
                              [self setState:state];
                              [self setCountry:country];
                              
                              [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSLocationManagerDidChangeLocationNotification];
                              
                          }];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - CLLocationManagerDelegate Methods

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    CLLocation *mostRecentLocation = [locations lastObject];
    
    if (mostRecentLocation == nil)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No location available", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSTimeInterval locationAge = -[[mostRecentLocation timestamp] timeIntervalSinceNow];
    if (locationAge > 5.0)
	{
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Time stamp is old (cached)", self, __PRETTY_FUNCTION__);
		return;
	}

    if ([mostRecentLocation horizontalAccuracy] < 0)
	{
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid accuracy", self, __PRETTY_FUNCTION__);
		return;
	}

    if ([mostRecentLocation horizontalAccuracy] <= 1000.0)
    {
        [self reverseGeocodeLocation:mostRecentLocation];
        
        [self stopTrackingLocation];
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self stopTrackingLocation];
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Location Error"
                                                      error:error
                                                  orMessage:nil];

    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSLocationManagerFailedNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Singleton

+ (instancetype)sharedManager
{
	static dispatch_once_t pred;
	static INSLocationManager *shared = nil;
    
	dispatch_once(&pred, ^{
        
		shared = [[[self class] alloc] init];
        
	});
    
	return shared;
}



@end
