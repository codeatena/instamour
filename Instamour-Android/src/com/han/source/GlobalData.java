package com.han.source;

import com.hyper.instamour.model.User;

public class GlobalData {
	
	public static String URL_API = "https://www.instamourapp.com/api.v008/";
	
	public static String URL_USER_FILE_PATH = "http://s3.instamourapp.com/user_files/";
	public static String URL_THUMB_VID_FILE_PATH = "http://s3.instamourapp.com/thumb_vid/";

	public static User currentUser = new User();
}