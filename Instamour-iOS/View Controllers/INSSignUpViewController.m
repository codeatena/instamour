//
//  INSSignUpViewController.m
//  Instamour
//
//  Created by Brian Slick on 7/11/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSSignUpViewController.h"

// Models and other global

// Sub-controllers
#import "INSSignUpProfileViewController.h"

// Views
#import "TPKeyboardAvoidingScrollView.h"

// Private Constants

@interface INSSignUpViewController () <UITextFieldDelegate, FBLoginViewDelegate>

// Private Properties
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) IBOutlet FBLoginView *facebookLoginView;

@property (nonatomic, strong) IBOutlet UITextField *usernameTextField;
@property (nonatomic, strong) IBOutlet UITextField *emailTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;

//@property (nonatomic, strong) NSArray *tableContents;
//@property (nonatomic, strong) TextFieldCell *usernameCell;
//@property (nonatomic, strong) TextFieldCell *emailCell;
//@property (nonatomic, strong) TextFieldCell *passwordCell;

@property (nonatomic, strong) UIBarButtonItem *cancelButton;
@property (nonatomic, strong) UIBarButtonItem *goButton;

@end

@implementation INSSignUpViewController

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Methods


#pragma mark - Initialization and UI Creation Methods


#pragma mark - Custom Getters and Setters


#pragma mark - UIViewController Overrides

- (void)viewDidLoad
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // Navigation Bar
    
    [self setTitle:@"Sign Up"];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed:)];
    [self setCancelButton:cancelButton];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
    UIBarButtonItem *goButton = [[UIBarButtonItem alloc] initWithTitle:@"Go"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(goButtonPressed:)];
    [self setGoButton:goButton];
    [[self navigationItem] setRightBarButtonItem:goButton];
    
    // Misc
    
    [[self facebookLoginView] setReadPermissions:@[ @"email", @"user_photos", @"user_birthday", @"user_location", @"user_hometown" ]];
    
//    [[self tableView] setRowHeight:50.0];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [super viewWillAppear:animated];
        
    // Analytics
    
    // User Interface
    
    [self setUserInterfaceEnabled:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - BTIViewController Overrides


#pragma mark - Notification Handlers


#pragma mark - UI Response Methods

- (void)cancelButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)goButtonPressed:(UIBarButtonItem *)button
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if ([INSServerAPIManager isNetworkConnectionAvailable])
    {
        [self signUpUser];
    }
    else
    {
        [INSServerAPIManager showNoNetworkAlert];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)setUserInterfaceEnabled:(BOOL)isEnabled
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self cancelButton] setEnabled:isEnabled];
    [[self goButton] setEnabled:isEnabled];
    
    if (isEnabled)
    {
        [[AppDelegate sharedDelegate] hideProgressIndicator];
    }
    else
    {
        [[self usernameTextField] resignFirstResponder];
        [[self emailTextField] resignFirstResponder];
        [[self passwordTextField] resignFirstResponder];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)signUpUser
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *usernameTextField = [self usernameTextField];
    UITextField *emailTextField = [self emailTextField];
    UITextField *passwordTextField = [self passwordTextField];
    
    NSString *username = [usernameTextField text];
    NSString *email = [emailTextField text];
    NSString *password = [passwordTextField text];
    
    BOOL isValidEntry = ( [username isNotEmpty] && [email isNotEmpty] && [password isNotEmpty] );
    
    if (!isValidEntry)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"Please fill in all of the fields"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - All fields not filled out", self, __PRETTY_FUNCTION__);
        return;
    }
    
    if (![email isValidEmailAddress])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"Please Enter a valid Email Address"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Invalid email address", self, __PRETTY_FUNCTION__);
        return;
    }
    
    INSSignUpProfileViewController *nextViewController = [[INSSignUpProfileViewController alloc] initWithEmail:email
                                                                                                      userName:username
                                                                                                      password:password
                                                                                                    facebookID:nil
                                                                                                   dateOfBirth:nil
                                                                                                        gender:nil
                                                                                                          city:nil
                                                                                                         state:nil
                                                                                                       country:nil];
    
    [[self navigationController] pushViewController:nextViewController animated:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self scrollView] TPKeyboardAvoiding_scrollToActiveTextField];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *usernameTextField = [self usernameTextField];
    UITextField *emailTextField = [self emailTextField];
    UITextField *passwordTextField = [self passwordTextField];
    
    if (textField == usernameTextField)
    {
        [emailTextField becomeFirstResponder];
    }
    else if (textField == emailTextField)
    {
        [passwordTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField)
    {
        [passwordTextField resignFirstResponder];
        
        [self goButtonPressed:nil];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UITextField *usernameTextField = [self usernameTextField];
    
    if (textField == usernameTextField)
    {
        NSString *newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        
        if ([newString length] >= INSUserNameMaxLength)
        {
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User name too long", self, __PRETTY_FUNCTION__);
            return NO;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return YES;
}

#pragma mark - FBLoginViewDelegate Methods

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *facebookID = [user objectID];
    
    if (![facebookID isNotEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Error"
                                                        message:@"Facebook data not available. Please try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        [[AppDelegate sharedDelegate] logoutFacebook];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No Facebook ID", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setUserInterfaceEnabled:NO];
    
    NSString *email = [user objectForKey:@"email"];
    
    NSString *facebookGender = [user objectForKey:@"gender"];
    NSString *gender = nil;
    if ([[facebookGender lowercaseString] isEqualToString:[INSMaleString lowercaseString]])
    {
        gender = INSMaleString;
    }
    else if ([[facebookGender lowercaseString] isEqualToString:[INSFemaleString lowercaseString]])
    {
        gender = INSFemaleString;
    }
    
    NSDictionary *location = [user objectForKey:@"location"];
    NSString *locationString = [location objectForKey:@"name"];
    
    NSString *city = nil;
    NSString *state = nil;
    NSString *country = nil;
    
    NSArray *locationComponents = [locationString componentsSeparatedByString:@","];
    
    if ([locationComponents count] > 0)
    {
        city = [[locationComponents objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    if ([locationComponents count] > 1)
    {
        state = [[locationComponents objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    if ([locationComponents count] > 2)
    {
        country = [[locationComponents objectAtIndex:2] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    // 06/15/1975
    NSString *birthdayString = [user birthday];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *dateOfBirth = [dateFormatter dateFromString:birthdayString];
    
//    NSLog(@"Facebook ID: %@", facebookID);
//    NSLog(@"Email: %@", email);
//    NSLog(@"Gender: %@", gender);
//    NSLog(@"Birthday: %@", dateOfBirth);
//    NSLog(@"City: %@", city);
//    NSLog(@"State: %@", state);
//    NSLog(@"Country: %@", country);
    
    INSSignUpProfileViewController *nextViewController = [[INSSignUpProfileViewController alloc] initWithEmail:email
                                                                                                      userName:nil
                                                                                                      password:nil
                                                                                                    facebookID:facebookID
                                                                                                   dateOfBirth:dateOfBirth
                                                                                                        gender:gender
                                                                                                          city:city
                                                                                                         state:state
                                                                                                       country:country];
    
    // Delay so that user can see what happens when they come back
    
    __weak INSSignUpViewController *weakSelf = self;
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [[weakSelf navigationController] pushViewController:nextViewController animated:YES];
        
    });
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    [[AppDelegate sharedDelegate] logoutFacebook];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
