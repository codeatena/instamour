//
//  INSVideoDataManager.m
//  Instamour
//
//  Created by Brian Slick on 7/21/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSVideoDataManager.h"

#import <MediaPlayer/MediaPlayer.h>

// Private Constants
NSString *const INSVideoDataManagerDidChangeVideoListNotification = @"INSVideoDataManagerDidChangeVideoListNotification";

NSString *const INSVideoInfoFileName = @"VideoInfo.data";


@interface INSVideoDataManager ()

// Private Properties
@property (nonatomic, strong) NSMutableDictionary *allVideoInfos;
@property (nonatomic, strong) NSArray *videoInfoList;

@property (nonatomic, assign, getter = isListReordering) BOOL listReordering;

@property (nonatomic, strong) NSArray *proposedVideoOrder;

@property (nonatomic, assign, getter = isMergeVideoRefreshNecessary) BOOL mergeVideoRefreshNecessary;

@property (nonatomic, strong) INSVideoInfo *videoInfoWaitingForLocalThumbnailGeneration;

@end


@implementation INSVideoDataManager

#pragma mark - Synthesized Properties


#pragma mark - Dealloc and Memory Management


#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
        [notificationCenter addObserver:self
                               selector:@selector(applicationWillEnterForegroundNotification:)
                                   name:UIApplicationWillEnterForegroundNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(getUserDidSucceedNotification:)
                                   name:INSServerAPIManagerGetUserDidFinishNotification
                                 object:nil];

        
        [notificationCenter addObserver:self
                               selector:@selector(videoUploadDidSucceedNotification:)
                                   name:INSServerAPIManagerVideoUploadDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(videoUploadFailedNotification:)
                                   name:INSServerAPIManagerVideoUploadFailedNotification
                                 object:nil];
        

        [notificationCenter addObserver:self
                               selector:@selector(videoSortDidSucceedNotification:)
                                   name:INSServerAPIManagerVideoSortDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(videoSortFailedNotification:)
                                   name:INSServerAPIManagerVideoSortFailedNotification
                                 object:nil];
        
        
        [notificationCenter addObserver:self
                               selector:@selector(videoDeleteDidSucceedNotification:)
                                   name:INSServerAPIManagerVideoDeleteDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(videoDeleteFailedNotification:)
                                   name:INSServerAPIManagerVideoDeleteFailedNotification
                                 object:nil];
        
        
        [notificationCenter addObserver:self
                               selector:@selector(videoMergeDidSucceedNotification:)
                                   name:INSServerAPIManagerVideoCreateMergeDidFinishNotification
                                 object:nil];
        
        [notificationCenter addObserver:self
                               selector:@selector(videoMergeFailedNotification:)
                                   name:INSServerAPIManagerVideoCreateMergeFailedNotification
                                 object:nil];
    }
    return self;
}

#pragma mark - Custom Getters and Setters

- (NSMutableDictionary *)allVideoInfos
{
    if (_allVideoInfos == nil)
    {
        _allVideoInfos = [[NSMutableDictionary alloc] init];
    }
    return _allVideoInfos;
}

- (NSArray *)videoInfoList
{
    if (_videoInfoList == nil)
    {
        NSSortDescriptor *slotNumberSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"serverSlotNumber" ascending:YES];
        NSArray *sortedVideos = [[[self allVideoInfos] allValues] sortedArrayUsingDescriptors:@[ slotNumberSortDescriptor ]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"serverSlotNumber != %d", INSMergeVideoServerSlotNumber];
        _videoInfoList = [sortedVideos filteredArrayUsingPredicate:predicate];        
    }
    return _videoInfoList;
}

#pragma mark - Notification Handlers

- (void)applicationWillEnterForegroundNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    [self purgeUnusedVideoFiles];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)getUserDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
//    NSLog(@"jsonDictionary: %@", jsonDictionary);
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
//        NSLog(@"error: %@", [jsonDictionary errorMessageINS]);
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Get User Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSDictionary *user = [jsonDictionary currentUserINS];
    NSString *incomingID = [user identifierForUserINS];
    NSString *existingID = [[[INSUserManager sharedManager] currentUser] userID];
    
    if (![incomingID isEqualToString:existingID])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Not the current user", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self populateWithGetUserJSONDictionary:jsonDictionary];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoUploadDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSDictionary *jsonDictionary = [notification object];
    NSDictionary *userInfo = [notification userInfo];
    
    NSInteger serverSlotNumber = [(NSString *)[userInfo objectForKey:INSAPIKeyCount] integerValue];
    
    INSVideoInfo *videoInfo = [self videoInfoForServerSlotNumber:serverSlotNumber];
    [videoInfo setState:INSVideoInfoStateNormal];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Video Upload Failure"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Video Upload Failed", self, __PRETTY_FUNCTION__);
        return;
    }

    [self setMergeVideoRefreshNecessary:YES];
    
    [self saveVideoList];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoUploadFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Video Upload Failure"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSInteger serverSlotNumber = [(NSString *)[userInfo objectForKey:INSAPIKeyCount] integerValue];
    
    INSVideoInfo *videoInfo = [self videoInfoForServerSlotNumber:serverSlotNumber];
    [videoInfo setState:INSVideoInfoStateNormal];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoSortDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setListReordering:NO];
    
    [[AppDelegate sharedDelegate] hideProgressIndicator];
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Video Reorder Failure"
                                                          error:nil
                                                      orMessage:[jsonDictionary errorMessageINS]];
        
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Video Reorder Failed", self, __PRETTY_FUNCTION__);
        return;
    }

    [self setMergeVideoRefreshNecessary:YES];
    
    [[self proposedVideoOrder] enumerateObjectsUsingBlock:^(INSVideoInfo *videoInfo, NSUInteger index, BOOL *stop) {
        
        [self setVideoInfo:videoInfo forServerSlotNumber:index + 1];
        
    }];
    
    [self setVideoInfoList:nil];
    [self setProposedVideoOrder:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

    [self saveVideoList];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoSortFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setListReordering:NO];
    [self setProposedVideoOrder:nil];

    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Video Reorder Failure"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoDeleteDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    NSString *serverSlotNumberString = [userInfo objectForKey:INSAPIKeyCount];
    NSInteger serverSlotNumber = [serverSlotNumberString integerValue];
    
    INSVideoInfo *videoInfo = [self videoInfoForServerSlotNumber:serverSlotNumber];
    [videoInfo setState:INSVideoInfoStateNormal];

    NSDictionary *jsonDictionary = [notification object];
//    NSLog(@"jsonDictionary: %@", jsonDictionary);
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

        // Service will fail if the slot was empty. Check message, then decide if we care.
        NSString *message = [jsonDictionary errorMessageINS];
        
        if ([message isEqualToString:@"Cannot find video"])
        {
            // Don't think we care.
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - There was no video to delete", self, __PRETTY_FUNCTION__);
            return;
        }
        
        [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Delete Video Failed"
                                                          error:nil
                                                      orMessage:message];
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Video Delete Failed, Unknown reason", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setMergeVideoRefreshNecessary:YES];

    INSVideoInfo *emptyVideoInfo = [[INSVideoInfo alloc] init];
    [self setVideoInfo:emptyVideoInfo forServerSlotNumber:serverSlotNumber];
    
    [self deleteLocalFilesForVideoInfo:videoInfo];
    
    [self saveVideoList];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoDeleteFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *userInfo = [notification userInfo];
    NSString *serverSlotNumberString = [userInfo objectForKey:INSAPIKeyCount];
    NSInteger serverSlotNumber = [serverSlotNumberString integerValue];
    
    INSVideoInfo *videoInfo = [self videoInfoForServerSlotNumber:serverSlotNumber];
    [videoInfo setState:INSVideoInfoStateNormal];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

    [[AppDelegate sharedDelegate] showFailureAlertWithTitle:@"Delete Video Failed"
                                                      error:[notification object]
                                                  orMessage:nil];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoMergeDidSucceedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *jsonDictionary = [notification object];
    
    BOOL isStatusSuccessful = [jsonDictionary isStatusSuccessfulINS];
    
    if (!isStatusSuccessful)
    {
//        NSLog(@"error message: %@", [jsonDictionary errorMessageINS]);
        // Should only get here if there are no videos to merge.
        // Might need additional handling in the future.
        
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Video Merge Failed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setMergeVideoRefreshNecessary:NO];
    
    [[INSUserManager sharedManager] refreshCurrentUserDataRightNow:YES];
    
    // TODO: Download new merge video
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)videoMergeFailedNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
//    NSDictionary *jsonDictionary = [notification object];
//    NSLog(@"jsonDictionary: %@", jsonDictionary);
    
    // TODO: Not sure what needs to be done here. Try again after delay?
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moviePlayerDidFinishThumbnailGenerationNotification:(NSNotification *)notification
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

#warning TODO: See if local thumbnail generation is worthwhile
//    INSVideoInfo *videoInfo = [self videoInfoWaitingForLocalThumbnailGeneration];
//    [self setVideoInfoWaitingForLocalThumbnailGeneration:nil];
//    
//    NSLog(@"Notification: %@", notification);
//    NSLog(@"Notifcation object: %@", [notification object]);
//    NSLog(@"URL: %@", [(MPMoviePlayerController *)[notification object] contentURL]);
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:MPMoviePlayerThumbnailImageRequestDidFinishNotification
//                                                  object:nil];
//
//    NSDictionary *userInfo = [notification userInfo];
//    
//    if ([userInfo objectForKey:MPMoviePlayerThumbnailErrorKey] != nil)
//    {
//        // There was an error
//        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Image error", self, __PRETTY_FUNCTION__);
//        return;
//    }
//    
//    UIImage *image = [userInfo objectForKey:MPMoviePlayerThumbnailImageKey];
//    
//    if (image == nil)
//    {
//        // No image was created
//        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Image missing", self, __PRETTY_FUNCTION__);
//        return;
//    }
//    
//    NSData *data = UIImagePNGRepresentation(image);
//    
//    NSURL *targetURL = [INSFileUtilities uniqueNewImageURL];
//    
//    [data writeToURL:targetURL atomically:YES];
//    [videoInfo setLocalThumbnailFileName:[targetURL lastPathComponent]];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Data Managing Methods

- (void)loadVideoList
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setVideoInfoList:nil];
    
    if ([[self allVideoInfos] count] > 0)
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Already loaded", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self addRequiredInfosToAllVideoInfos];

    NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSVideoInfoFileName];

    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No data file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:fileURL];
    
    NSMutableDictionary *allVideoInfos = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (![allVideoInfos isKindOfClass:[NSDictionary class]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Data is an old structure", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [[self allVideoInfos] removeAllObjects];
    [[self allVideoInfos] addEntriesFromDictionary:allVideoInfos];
    
    [self addRequiredInfosToAllVideoInfos];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)saveVideoList
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![[INSUserManager sharedManager] isLoggedIn])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - User not logged in", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[self allVideoInfos]];
    
    NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSVideoInfoFileName];
    
    [data writeToURL:fileURL atomically:YES];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteAllData
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    for (INSVideoInfo *videoInfo in [[self allVideoInfos] allValues])
    {
        [self deleteLocalFilesForVideoInfo:videoInfo];
    }
    
    [self setAllVideoInfos:nil];
    [self setVideoInfoList:nil];
    [self setMergeVideoRefreshNecessary:NO];
    
    NSURL *fileURL = [[BTIFileUtilities libraryApplicationSupportDirectoryURL] URLByAppendingPathComponent:INSVideoInfoFileName];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:[fileURL path]])
    {
        [fileManager removeItemAtPathBTI:[fileURL path]];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)populateWithGetUserJSONDictionary:(NSDictionary *)jsonDictionary
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSDictionary *user = [jsonDictionary currentUserINS];
    NSArray *incomingVideos = [user videosForUserINS];
    
    for (NSDictionary *serverVideoDictionary in incomingVideos)
    {
        INSVideoInfo *newVideoInfo = [[INSVideoInfo alloc] initWithJSONDictionary:serverVideoDictionary];
        [self setVideoInfo:newVideoInfo forServerSlotNumber:[[newVideoInfo serverSlotNumber] integerValue]];
    }
    
    [self addRequiredInfosToAllVideoInfos];
    
    // TODO: Handle videos to delete
    
    // TODO: Handle videos to download
    
    [self generateThumbnailsForAllVideos];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)purgeUnusedVideoFiles
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSURL *directoryURL = [INSFileUtilities videoDirectoryURL];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *fileNames = [fileManager contentsOfDirectoryAtPathBTI:[directoryURL path]];
    
    NSArray *videoInfos = [[self allVideoInfos] allValues];
    
    for (NSString *fileName in fileNames)
    {
        BOOL isFileInUse = NO;
        
        for (INSVideoInfo *videoInfo in videoInfos)
        {
            if ([[videoInfo videoFileName] isEqualToString:fileName])
            {
                isFileInUse = YES;
            }
        }
        
        if (!isFileInUse)
        {
            NSString *path = [[directoryURL URLByAppendingPathComponent:fileName] path];
            
            [fileManager removeItemAtPathBTI:path];
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Interrogation Methods

- (INSVideoInfo *)mergeVideo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self loadVideoList];
    
    INSVideoInfo *mergeVideo = nil;
    
    for (INSVideoInfo *videoInfo in [[self allVideoInfos] allValues])
    {
        if ([videoInfo isMergedVideo])
        {
            mergeVideo = videoInfo;
            break;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return mergeVideo;
}

- (BOOL)isAnyVideoUploading
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BOOL isUploading = NO;
    
    for (INSVideoInfo *videoInfo in [[self allVideoInfos] allValues])
    {
        if ([videoInfo state] == INSVideoInfoStateUploading)
        {
            isUploading = YES;
            break;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return isUploading;
}

- (BOOL)isAnyVideoPopulated
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BOOL isPopulated = NO;
    
    for (INSVideoInfo *videoInfo in [[self allVideoInfos] allValues])
    {
        if ([videoInfo isPopulated])
        {
            isPopulated = YES;
            break;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return isPopulated;
}

- (BOOL)isAnyVideoDeleting
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BOOL isDeleting = NO;
    
    for (INSVideoInfo *videoInfo in [[self allVideoInfos] allValues])
    {
        if ([videoInfo state] == INSVideoInfoStateDeleting)
        {
            isDeleting = YES;
            break;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return isDeleting;
}

- (BOOL)isAnyVideoTrimming
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    BOOL isTrimming = NO;
    
    for (INSVideoInfo *videoInfo in [[self allVideoInfos] allValues])
    {
        if ([videoInfo state] == INSVideoInfoStateTrimming)
        {
            isTrimming = YES;
            break;
        }
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
    return isTrimming;
}

#pragma mark - Server Call Methods

- (void)uploadVideoAtURL:(NSURL *)fileURL
     forServerSlotNumber:(NSInteger)serverSlotNumber
      withServerRotation:(INSServerAPIManagerVideoRotation)serverRotation
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    INSVideoInfo *videoInfo = [self videoInfoForServerSlotNumber:serverSlotNumber];

    void(^cancelAndShowAlert)(void) = ^{
        
        [videoInfo setState:INSVideoInfoStateNormal];
        
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Crop Error"
                                                        message:@"Could not shorten the video"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
	};

    void(^uploadVideo)(NSURL *url) = ^(NSURL *url){
        
        [videoInfo setState:INSVideoInfoStateUploading];
        
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];
        
        [[INSServerAPIManager sharedManager] uploadVideoFromFileURL:url
                                                forServerSlotNumber:@(serverSlotNumber)
                                                           rotation:serverRotation];

    };
    
    // Determine if video is too long. Trim if so.
    
    AVURLAsset *urlAsset = [AVURLAsset assetWithURL:fileURL];
    CMTime duration = [urlAsset duration];
    
    NSInteger numberOfSeconds = ceil(CMTimeGetSeconds(duration));
    
    if (numberOfSeconds <= INSMaxVideoLengthInSeconds)
    {
        uploadVideo(fileURL);
    }
    else
    {
        [videoInfo setState:INSVideoInfoStateTrimming];
        
        [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

        AVAsset *asset = [[AVURLAsset alloc] initWithURL:fileURL options:nil];
        
        NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
        
        if (![compatiblePresets containsObject:AVAssetExportPresetMediumQuality])
        {
            cancelAndShowAlert();
            
            BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Export preset not available", self, __PRETTY_FUNCTION__);
            return;
        }

        NSURL *outputURL = [INSFileUtilities uniqueNewVideoURL];
        
        // http://stackoverflow.com/questions/12714938/trim-video-without-displaying-uivideoeditorcontroller

        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:asset
                                                                              presetName:AVAssetExportPresetMediumQuality];
        
        [exportSession setOutputURL:outputURL];
        [exportSession setOutputFileType:AVFileTypeMPEG4];
        
        CMTime startTime = CMTimeMakeWithSeconds(0, 1);
        CMTime duration = CMTimeMakeWithSeconds(INSMaxVideoLengthInSeconds, 1);
        
        CMTimeRange timeRange = CMTimeRangeMake(startTime, duration);
        [exportSession setTimeRange:timeRange];

        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([exportSession status])
            {
                case AVAssetExportSessionStatusCompleted:
                {
                    uploadVideo(outputURL);
                }
                    break;
                case AVAssetExportSessionStatusFailed:
                default:
                {
                    cancelAndShowAlert();
                }
                    break;
            }
            
        }];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)moveVideoAtIndex:(NSInteger)fromIndex
                 toIndex:(NSInteger)toIndex
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [self setMergeVideoRefreshNecessary:YES];
    
    NSMutableArray *proposedOrder = [[self videoInfoList] mutableCopy];
    
    [proposedOrder moveObjectAtIndexBTI:fromIndex toIndex:toIndex];
    
    [self setProposedVideoOrder:[NSArray arrayWithArray:proposedOrder]];
    
    NSMutableArray *serverSlotsNumbers = [NSMutableArray arrayWithCapacity:[proposedOrder count]];
    
    for (INSVideoInfo *videoInfo in proposedOrder)
    {
        NSNumber *slotNumber = [videoInfo serverSlotNumber];
        
        NSString *slotNumberString = [NSString stringWithFormat:@"%ld", (long)[slotNumber integerValue]];
        
        [serverSlotsNumbers addObject:slotNumberString];
    }
        
    [[AppDelegate sharedDelegate] showProgressIndicatorWithMessage:@"Please wait..."];
    
    [self setListReordering:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];
    
    [[INSServerAPIManager sharedManager] sortVideosWithNewSlotOrder:serverSlotsNumbers];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteVideoAtIndex:(NSInteger)index
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSInteger serverSlotNumber = index + 1;
    
    INSVideoInfo *videoInfo = [self videoInfoForServerSlotNumber:serverSlotNumber];
    [videoInfo setState:INSVideoInfoStateDeleting];

    [[INSServerAPIManager sharedManager] deleteVideoForServerSlotNumber:@(serverSlotNumber)];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)refreshMergeVideoIfNecessary
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    if (![self isMergeVideoRefreshNecessary])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - Refresh not needed", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setMergeVideoRefreshNecessary:NO];
    
    [[INSServerAPIManager sharedManager] createMergeVideo];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Misc Methods

- (void)addRequiredInfosToAllVideoInfos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    // Make sure there are video infos for 10 videos + 1 merge video

    for (NSInteger serverSlotNumber = 0; serverSlotNumber <= INSMaxNumberOfVideos; serverSlotNumber++)
    {
        INSVideoInfo *videoInfo = [self videoInfoForServerSlotNumber:serverSlotNumber];
        
        if (videoInfo == nil)
        {
            videoInfo = [[INSVideoInfo alloc] init];
            [self setVideoInfo:videoInfo forServerSlotNumber:serverSlotNumber];
        }
    }
    
    [self saveVideoList];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)deleteLocalFilesForVideoInfo:(INSVideoInfo *)videoInfo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([videoInfo videoFileName] != nil)
    {
        NSURL *url = [[INSFileUtilities videoDirectoryURL] URLByAppendingPathComponent:[videoInfo videoFileName]];
        
        if ([fileManager fileExistsAtPath:[url path]])
        {
            [fileManager removeItemAtPathBTI:[url path]];
        }
    }
    
    if ([videoInfo thumbnailFileName] != nil)
    {
        NSURL *url = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:[videoInfo thumbnailFileName]];
        
        if ([fileManager fileExistsAtPath:[url path]])
        {
            [fileManager removeItemAtPathBTI:[url path]];
        }
    }

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setVideoInfo:(INSVideoInfo *)videoInfo
 forServerSlotNumber:(NSInteger)serverSlotNumber
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSNumber *key = @(serverSlotNumber);
    
    [videoInfo setServerSlotNumber:key];
    
    [[self allVideoInfos] setObject:videoInfo forKey:key];

    [self setVideoInfoList:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];

    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (INSVideoInfo *)videoInfoForServerSlotNumber:(NSInteger)serverSlotNumber
{
    NSNumber *key = @(serverSlotNumber);

    return [[self allVideoInfos] objectForKey:key];
}

- (void)generateThumbnailsForAllVideos
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSArray *allVideos = [[self allVideoInfos] allValues];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *imageDirectoryURL = [INSFileUtilities imageDirectoryURL];
    
    for (INSVideoInfo *videoInfo in allVideos)
    {
        // 1. Look for locally cached image file
        // 2. Look for server file info and download
        // 3. Create one from local video file
        // 4. ??? Create one from server video? Maybe better to download video first.
        
        NSString *localImageFileName = [videoInfo thumbnailFileName];
        
        if ([localImageFileName isNotEmpty])
        {
            NSURL *localFileURL = [imageDirectoryURL URLByAppendingPathComponent:localImageFileName];
            
            if ([fileManager fileExistsAtPath:[localFileURL path]])
            {
                // Already have a thumbnail image, no action needed
                continue;
            }
        }
        
        NSString *serverFileName = [videoInfo thumbnailFileName];
        
        if (serverFileName != nil)
        {
            // Download existing thumbnail
            [self downloadThumbnailForVideoInfo:videoInfo];
            
            continue;
        }
        
        [self generateThumbnailFromLocalVideoForVideoInfo:videoInfo];
    }
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)downloadThumbnailForVideoInfo:(INSVideoInfo *)videoInfo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);

    NSString *serverFileName = [videoInfo thumbnailFileName];

    if (![serverFileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No server filename", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *serverURLString = [[[NSUserDefaults standardUserDefaults] serverVideoThumbnailImagesFilePathINS] stringByAppendingPathComponent:serverFileName];
    NSURL *url = [NSURL URLWithString:serverURLString];
    
    INSServerAPIManager *serverManager = [INSServerAPIManager sharedManager];
    
    [serverManager downloadImageFromURL:url
                                success:^(UIImage *image) {
                                    
                                    NSLog(@"Success");
                                    
                                    NSData *data = UIImagePNGRepresentation(image);
                                    
                                    NSURL *targetURL = [[INSFileUtilities imageDirectoryURL] URLByAppendingPathComponent:serverFileName];
                                    
                                    [data writeToURL:targetURL atomically:YES];
                                    [videoInfo setThumbnailFileName:serverFileName];
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationNameOnMainThreadBTI:INSVideoDataManagerDidChangeVideoListNotification];
                                    
                                }
                                failure:^(NSString *errorMessage) {
                                    
                                    NSLog(@"Fail: %@", errorMessage);
                                    
                                }];
        
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)generateThumbnailFromLocalVideoForVideoInfo:(INSVideoInfo *)videoInfo
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    NSString *localVideoFileName = [videoInfo videoFileName];
    
    if (![localVideoFileName isNotEmpty])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No local file name", self, __PRETTY_FUNCTION__);
        return;
    }
    
    NSURL *localFileURL = [[INSFileUtilities videoDirectoryURL] URLByAppendingPathComponent:localVideoFileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[localFileURL path]])
    {
        BTITrackingLog(@"<<< Leaving  <%p> %s >>> EARLY - No local file", self, __PRETTY_FUNCTION__);
        return;
    }
    
    [self setVideoInfoWaitingForLocalThumbnailGeneration:videoInfo];
    
    MPMoviePlayerController *moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:localFileURL];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerDidFinishThumbnailGenerationNotification:)
                                                 name:MPMoviePlayerThumbnailImageRequestDidFinishNotification
                                               object:nil];
    
    [moviePlayer requestThumbnailImagesAtTimes:@[ @1 ] timeOption:MPMovieTimeOptionNearestKeyFrame];
    
    [moviePlayer stop];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

#pragma mark - Singleton

+ (instancetype)sharedManager
{
	static dispatch_once_t pred;
	static INSVideoDataManager *shared = nil;
    
	dispatch_once(&pred, ^{
        
		shared = [[[self class] alloc] init];
        
	});
    
	return shared;
}


@end
