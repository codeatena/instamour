//
//  INSPendingAmoursTableCell.m
//  Instamour
//
//  Created by Brian Slick on 7/19/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import "INSPendingAmoursTableCell.h"

@interface INSPendingAmoursTableCell ()

// Private Propertes
@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UILabel *messageLabel;

@end

@implementation INSPendingAmoursTableCell

#pragma mark - Misc Methods

- (void)setUserName:(NSString *)userName
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self userNameLabel] setText:userName];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

- (void)setMessage:(NSString *)message
{
    BTITrackingLog(@">>> Entering <%p> %s <<<", self, __PRETTY_FUNCTION__);
    
    [[self messageLabel] setText:message];
    
    BTITrackingLog(@"<<< Leaving  <%p> %s >>>", self, __PRETTY_FUNCTION__);
}

@end
