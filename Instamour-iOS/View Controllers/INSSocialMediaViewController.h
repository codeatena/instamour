//
//  INSSocialMediaViewController.h
//  Instamour
//
//  Created by Brian Slick on 8/5/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIWebViewController.h"
@class INSUser;

// Public Constants

typedef NS_ENUM(NSInteger, INSSocialMediaPlatform) {
    INSSocialMediaPlatformInstagram,
    INSSocialMediaPlatformVine,
};

// Protocols

@interface INSSocialMediaViewController : BTIWebViewController

// Public Properties
@property (nonatomic, strong) INSUser *user;
@property (nonatomic, assign) INSSocialMediaPlatform platform;

// Public Methods

@end
