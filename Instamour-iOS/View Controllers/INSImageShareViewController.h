//
//  INSImageShareViewController.h
//  Instamour
//
//  Created by Brian Slick on 8/9/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

// Libraries

// Forward Declarations and Classes
#import "BTIViewController.h"
@class INSChatMessage;

// Public Constants

// Protocols

@interface INSImageShareViewController : BTIViewController

// Public Properties
@property (nonatomic, strong) INSChatMessage *chatMessage;

// Public Methods

@end
