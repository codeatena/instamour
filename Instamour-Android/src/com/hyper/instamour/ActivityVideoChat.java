package com.hyper.instamour;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.service.QBVideoChatService;
import com.quickblox.module.videochat.core.service.ServiceInteractor;
import com.quickblox.module.videochat.model.listeners.OnCameraViewListener;
import com.quickblox.module.videochat.model.listeners.OnQBVideoChatListener;
import com.quickblox.module.videochat.model.objects.CallState;
import com.quickblox.module.videochat.model.objects.CallType;
import com.quickblox.module.videochat.views.CameraView;
import com.quickblox.module.videochat.views.OpponentView;
import com.quickblox.module.videochat.model.objects.VideoChatConfig;

public class ActivityVideoChat extends Activity {
	private Global global;
	private Function c;
	private String uname;
	private TextView txt_uname;
	private Intent data;
	String currentusername, loginusername;
	String jabberid, loginjabberid;
	Intent intent;
	String password;

	private QBUser me, friend;
	private VideoChatConfig videoChatConfig;
	private CameraView cameraView;
	private OpponentView opponentSurfaceView;
	private ProgressBar opponentImageLoadingPb;
	private ImageView img_end;

	// private VideoChatConfig videoChatConfig;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_chat_layout);
		global = Global.getInstance();
		initViews();

	}
	//the directions for FlurryAnalytics call for this
			//(FLJ, 5/6/14)
			 // The Activity's onStart method
			@Override 
			protected void onStart() { // Set up the Flurry session
				super.onStart();
				FlurryAgent.onStartSession(this, "R6ZN6QFQGS385Z5RSSXP");
				
			}
			@Override
			protected void onStop() {
				super.onStop();
				FlurryAgent.onEndSession(this);
			}
	
	
	

	@Override
	public void onDestroy() {
		try {
			QBVideoChatService.getService().finishVideoChat(
					videoChatConfig.getSessionId());
		} catch (Exception e) {
			/* IGNORE */
		}
		super.onDestroy();
	}

	private void initViews() {
		// Debugger.logConnection("initViews");

		// Setup UI
		//
		if (global.from_audio) {
			intent = getIntent();
			Log.e("audio Chat Activity : ", "Activity");
			txt_uname = (TextView) findViewById(R.id.topbar_heading);
			opponentSurfaceView = (OpponentView) findViewById(R.id.camera_preview);
			cameraView = (CameraView) findViewById(R.id.opponentSurfaceView);
			opponentImageLoadingPb = (ProgressBar) findViewById(R.id.opponentImageLoading);
			opponentSurfaceView.setVisibility(View.INVISIBLE);
			cameraView.setVisibility(View.INVISIBLE);
			opponentImageLoadingPb.setVisibility(View.INVISIBLE);
			img_end = (ImageView) findViewById(R.id.img_end);
			videoChatConfig = intent.getParcelableExtra(VideoChatConfig.class
					.getCanonicalName());
			/*
			 * currentusername = intent.getStringExtra("selectedusername");
			 * Log.e("selected user name from the amours", currentusername);
			 */

			// videoChatConfig = YourAmours.videoChatConfig;
			// videoChatConfig = getIntent().getStringExtra("VideoChatAct");
			try {

				QBVideoChatService.getService().startVideoChat(videoChatConfig);

				txt_uname.setText(intent.getStringExtra("selectedusername"));
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Error in start Video chat : ", "Error");
			}
		} else if (global.from_audio == false) {
			intent = getIntent();
			Log.e("Video Chat Activity : ", "Activity");
			// txt_uname = (TextView) findViewById(R.id.video_txt_username);
			opponentSurfaceView = (OpponentView) findViewById(R.id.camera_preview);
			cameraView = (CameraView) findViewById(R.id.opponentSurfaceView);
			opponentImageLoadingPb = (ProgressBar) findViewById(R.id.opponentImageLoading);
			img_end = (ImageView) findViewById(R.id.img_end);
			videoChatConfig = intent.getParcelableExtra(VideoChatConfig.class
					.getCanonicalName());
			/*
			 * currentusername = intent.getStringExtra("selectedusername");
			 * Log.e("selected user name from the amours", currentusername);
			 * txt_uname.setText(currentusername);
			 */

			// videoChatConfig = YourAmours.videoChatConfig;
			// videoChatConfig = getIntent().getStringExtra("VideoChatAct");
			try {

				QBVideoChatService.getService().startVideoChat(videoChatConfig);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Error in start Video chat : ", "Error");
			}
		}

		img_end.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				QBVideoChatService.getService().finishVideoChat(
						videoChatConfig.getSessionId());

				finish();

			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		QBVideoChatService.getService().finishVideoChat(
				videoChatConfig.getSessionId());
		finish();
	}

	@Override
	public void onResume() {

		Log.e("Video Chat Activity Resume : ", "Activity Resume");
		if (!global.from_audio) {
			try {
				QBVideoChatService.getService().setQbVideoChatListener(
						qbVideoChatListener);

				cameraView.setQBVideoChatListener(qbVideoChatListener);
				cameraView.setOnCameraViewListener(new OnCameraViewListener() {
					@Override
					public void onCameraInit(
							List<Camera.Size> cameraPreviewSizes,
							List<Integer> listFps) {
						cameraView.setCameraPreviewSizeImageQualityCameraFps(
								findMinimalSize(cameraPreviewSizes), 100,
								findMinimalFPS(listFps));
					}
				});
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Error", "Error");

			}
		} else if (global.from_audio) {
			cameraView.setVisibility(View.INVISIBLE);
			opponentSurfaceView.setVisibility(View.INVISIBLE);
			opponentImageLoadingPb.setVisibility(View.INVISIBLE);
		}
		super.onResume();
	}

	private int findMinimalFPS(List<Integer> fpSs) {
		return (fpSs.get(fpSs.size() - 1) > fpSs.get(0)) ? fpSs.get(0) : fpSs
				.get(fpSs.size() - 1);
	}

	private Camera.Size findMinimalSize(List<Camera.Size> previewSizes) {
		return (previewSizes.get(previewSizes.size() - 1).width > previewSizes
				.get(0).width) ? previewSizes.get(0) : previewSizes
				.get(previewSizes.size() - 1);
	}

	OnQBVideoChatListener qbVideoChatListener = new OnQBVideoChatListener() {
		@Override
		public void onCameraDataReceive(byte[] videoData, byte value) {
			if (videoChatConfig.getCallType() != CallType.VIDEO_AUDIO) {
				return;
			}
			ServiceInteractor.INSTANCE.sendVideoData(ActivityVideoChat.this,
					videoData, value);
		}

		@Override
		public void onMicrophoneDataReceive(byte[] audioData) {
			ServiceInteractor.INSTANCE.sendAudioData(ActivityVideoChat.this,
					audioData);
		}

		@Override
		public void onOpponentVideoDataReceive(byte[] videoData) {
			opponentSurfaceView.setData(videoData);
		}

		@Override
		public void onOpponentAudioDataReceive(byte[] audioData) {
			QBVideoChatService.getService().playAudio(audioData);
		}

		@Override
		public void onProgress(boolean progress) {
			opponentImageLoadingPb.setVisibility(progress ? View.VISIBLE
					: View.GONE);
		}

		@Override
		public void onVideoChatStateChange(CallState callState,
				VideoChatConfig chat) {

			switch (callState) {
			case ON_CALL_START:
				Toast.makeText(getBaseContext(),
						getString(R.string.call_start_txt), Toast.LENGTH_SHORT)
						.show();

				break;
			case ON_CANCELED_CALL:
				Toast.makeText(getBaseContext(),
						getString(R.string.call_canceled_txt),
						Toast.LENGTH_SHORT).show();
				finish();
				break;
			case ON_CALL_END:
				Toast.makeText(getBaseContext(), "call end", Toast.LENGTH_SHORT)
						.show();
				finish();
				break;
			}
		}
	};

}
