//
//  NSMutableDictionary+INSAdditions.h
//  Instamour
//
//  Created by Brian Slick on 7/12/14.
//  Copyright (c) 2014 Instamour Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (INSAdditions)

- (void)setObjectSafelyINS:(id)anObject forKey:(id < NSCopying >)aKey;
- (void)setKeySafelyINS:(id < NSCopying >)aKey andObject:(id)anObject;

@end
